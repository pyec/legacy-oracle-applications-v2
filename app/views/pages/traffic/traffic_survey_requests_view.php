<div class="row">
	<div class="col-md-6">
		<h2>Taffic Survey Requests</h2>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<form action="<?php echo base_url(); ?>traffic/TSR_search" method="POST">
    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <label for="record_number">Record #</label>
                <input type="text" name="record_number" class="form-control" title="Record Number" value="<?php echo set_value('record_number') ?>" tabindex="1" placeholder="Record Number" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
                <button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="traffic_atr_form_reset()">Clear</button>
            </div>
        </div>
    </div>
</form>
<legend class="phonebook_legend"></legend>

<div class="row">
	<div class="col-md-12">
		<?php if (!empty($TSR_studies)): ?>
			<?php if (count($TSR_studies)): ?>
				<h2 class="count"><?php echo count($TSR_studies) ?> studies or more</h2>
			<?php else: ?>
				<h2 class="count"><?php echo count($TSR_studies) ?> studies</h2>
			<?php endif ?>
		<?php endif ?>
	</div>
	<div class="col-sm-12" style="margin-top: 30px;">
		<?php if (!empty($TSR_studies) && count($TSR_studies) == 500): ?>
			<P class="">Refine your search for more specific results.</P>			
		<?php endif ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>traffic/add_TSR" class="btn btn-success pull-right">Add Survey</a>
	</div>
</div>

<?php if (!empty($TSR_studies)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Record #</th>
				<th>Comments</th>
				<th>Created On</th>
				<th>Delete</th>
			</thead>
			<tbody class="tbody">
					<?php foreach ($TSR_studies as $study): ?>
						<tr>
							<td><a href="<?= base_url(); ?>traffic/update_TSR/<?= $study['REQ_NUM'] ?>"><?= $study['REQ_NUM'] ?></a></td>
							<td><?php echo $study['COMMENTS'] ?></td>
							<td><?php echo $study['CREATE_DATE'] ?></td>
							<td>
								<form action="<?php echo base_url(); ?>traffic/delete_TSR" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger delete_survey"><i class="fa fa-minus-square"></i></button>
									<input type="hidden" name="REQ_NUM" value="<?= $study['REQ_NUM'] ?>" />
								</form>
							</td>
						</tr>
						
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>