<div class="row">
	<div class="col-md-6">
		<h2>Traffic Volume Database</h2>
		<p>Select a survey category</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<ul>
			<li><a href="<?php echo base_url(); ?>traffic/ATR">24 Hour Automated traffic Recorder Studies</a></li>
			<li><a href="<?php echo base_url(); ?>traffic/LTI">Left Turn Investigation</a></li>
			<li><a href="<?php echo base_url(); ?>traffic/MTMTC">Manual Turning Movement Traffic Counts</a></li>
			<li><a href="<?php echo base_url(); ?>traffic/PS">Pedestrian Studies</a></li>
			<li><a href="<?php echo base_url(); ?>traffic/RSS">Radar Spot Speed Studies</a></li>
			<li><a href="<?php echo base_url(); ?>traffic/TSW">Traffic Signal Warrants</a></li>
			<li><a href="<?php echo base_url(); ?>traffic/TSR">Traffic Survey Requests</a></li>
			<li><a href="<?php echo base_url(); ?>traffic/TR">Traffic Regulations</a></li>
		</ul>
	</div>
</div>
