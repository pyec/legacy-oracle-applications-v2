<div class="row">
	<div class="col-md-12">
		<h2>Update Manual Turning Movement Traffic Counts Survey</h2>
		<p class="lead">Update</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?php echo base_url(); ?>traffic/update_MTMTC" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="REC_CODE">Record #</label>
						<input type="text" name="REC_CODE" class="form-control" title="Record Number" value="<?= $survey['REC_CODE'] ?>" readonly />
					</div>
					<div class="col-sm-2">
						<label for="S_AM" class="no_label_fix">AM</label>
						<?php if ($survey['S_AM'] == 'T'): ?>
							<input checked="checked" type="checkbox" name="S_AM" class="" />
						<?php else : ?>
							<input type="checkbox" name="S_AM" class="" />
						<?php endif ?>
					</div>
					<div class="col-sm-2">
						<label for="S_PM" class="no_label_fix">PM</label>
						<?php if ($survey['S_PM'] == 'T'): ?>
							<input checked="checked" type="checkbox" name="S_PM" class="" />
						<?php else : ?>
							<input type="checkbox" name="S_PM" class="" />
						<?php endif ?>
					</div>
					<div class="col-sm-3">
						<label for="S_OP" class="no_label_fix" title="Off Peak" >OP</label>
						<?php if ($survey['S_OP'] == 'T'): ?>
							<input checked="checked" type="checkbox" name="S_OP" class="" />
						<?php else : ?>
							<input type="checkbox" name="S_OP" class="" />
						<?php endif ?>
					</div>
					<div class="col-sm-2">
						<label for="CREATE_DATE">Created On</label>
						<input type="text" name="CREATE_DATE" class="form-control date_picker" title="Created On" value="<?= $survey['CREATE_DATE'] ?>" readonly />
					</div>
				</div>
			</div>

			<legend>Intersection</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR_CODE" class="form-control street_code" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">Street Name</label>
						<input type="text"  class="form-control street_name" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">Type</label>
						<input type="text" class="form-control street_type" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">Community</label>
						<input type="text" class="form-control community" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR2_CODE" class="form-control no_label_fix 2_street_code" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">At</label>
						<input type="text"  class="form-control 2_street_name" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 2_street_type" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 2_community" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR3_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR3_CODE" class="form-control 3_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix 3_street_name" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 3_street_type" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 3_community" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR4_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR4_CODE" class="form-control 4_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix 4_street_name" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 4_street_type" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 4_community" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR5_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR5_CODE" class="form-control 5_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix 5_street_name" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 5_street_type" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 5_community" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>

			<legend>Location</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="SURVEY_DATE">Survey Date</label>
						<input type="text" name="SURVEY_DATE" class="form-control date_picker" title="Survey Date" value="<?= $survey['SURVEY_DATE'] ?>" readonly />
					</div>
					<div class="col-sm-2">
						<label for="STR_REG">Region</label>
						<select name="STR_REG" class="form-control" title="Region" maxlength="3" >
							<option value="">select..</option>
							<option value="EST">Eastern</option>
							<option value="WST">Western</option>
							<option value="CNT">Central</option>
							<?php if (!empty($survey['STR_REG'])): ?>
								<option selected value="<?= $survey['STR_REG'] ?>"><?php echo $value = ($survey['STR_REG'] == 'EST' ? 'Eastern' : ($survey['STR_REG'] == 'WST' ? 'Western' : ($survey['STR_REG'] == 'CNT' ? 'Central' : 'Select..'))); ?></option>
							<?php endif ?>
						</select>
					</div>
					<div class="col-sm-3">
						<label for="X_COORD">GPS X</label>
						<input type="text" name="X_COORD" class="form-control" title="GPS X" value="<?= $survey['X_COORD'] ?>" maxlength="10" />
					</div>
					<div class="col-sm-3">
						<label for="Y_COORD">GPS Y</label>
						<input type="text" name="Y_COORD" class="form-control" title="GPS Y" value="<?= $survey['Y_COORD'] ?>" maxlength="10" />
					</div>
				</div>
			</div>

			

			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="PEAK_HV">Peak Hour Volume</label>
						<input type="text" name="PEAK_HV" class="form-control" title="Peak Hour Volume" value="<?= $survey['PEAK_HV'] ?>" maxlength="5" />
					</div>
					<div class="col-sm-2">
						<label for="SURVEYOR">Surveyor</label>
							<input type="text" name="SURVEYOR" class="form-control pull-right surveyor_input" title="Surveyor" maxlength="3" value="<?= $survey['SURVEYOR'] ?>" />
					</div>

					<div class="col-sm-6">
						<label for="COMMENT_BOX">Comments</label>
						<textarea name="COMMENT_BOX" class="form-control" title="Comments" maxlength="200" ><?= $survey['COMMENT_BOX'] ?></textarea>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-primary btn-md pull-right" value="Update Survey" />
						</form>
					</div>
				</div>
			</div>

			<legend><?= $attachment_count ?> - Attachment</legend>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<?php echo form_open_multipart('traffic/do_upload');?>

					<input type="file" name="userfile" size="20" class="form-control" />

					<br /><br />
					<input type="hidden" name="REC_CODE" value="<?= $survey['REC_CODE'] ?>" />
					<input type="hidden" name="FORM" value="MTMTC" />

				</div>
				<div class="col-sm-2">
					<input type="submit" value="Upload" class="btn btn-md btn-primary" />
					</form>
					
				</div>
				<div class="col-sm-4">
					<input type="button" class="btn btn-md btn-primary open_attachments pull-right" value="Open attachments" id="MTMTC" />
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<a href="<?php echo base_url(); ?>traffic/delete_attachments/MTMTC/<?= $survey['REC_CODE'] ?>" class="btn btn-md btn-default pull-right delete_attachments">Delete Attachments</a>
				</div>
			</div>
		</div>

			

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<a href="<?php echo base_url(); ?>traffic/MTMTC" class="btn btn-default btn-md">Back to MTMTC</a>
					</div>
				</div>
			</div>
		</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title">Street Look Up</h4>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="col-sm-5">
									<div class="modal_search">
										<label for="search_box">Street Name</label>
										<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
										<p class="text-muted">(Select the name of the record you would like to use.)</p>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<table class="table table-bordered">
							<thead>
								<th>Street</th>
							</thead>
							<tbody class="modal_tbody">
								<tr>
									
								</tr>
							</tbody>	
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->