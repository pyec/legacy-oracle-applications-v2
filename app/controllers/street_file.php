<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	  Class : Street File Update System

Description : This system allows users to update the information on file for each
			street in HRM.

  Functions : index(), 
  			search(),
  			street( $id ),
  			add(), 
  			update(), 
  			delete()

=========================================================================================================================*/
class Street_file extends CI_Controller 
{	
	/*           
	============================================================================
	index() - The landing function for the app, displays the home page.
	----------------------------------------------------------------------------
	N/A
	----------------------------------------------------------------------------
	Return values:
	N/A
	============================================================================
	*/
	
	public function index( $offset = 0 )
	{
		//Load Model 
		$this->load->model('street_file_model');

		$data['communities'] = $this->street_file_model->get_cities();
		$data['street_types'] = $this->street_file_model->get_street_file_types();
		$data['content'] = "pages/street_file/home_view";
		$this->load->view('master', $data);
	}

	/*           
	============================================================================
	search() - This function takes the search string from the form and retrieves
	a result set based on that string.
	----------------------------------------------------------------------------
	N/A
	----------------------------------------------------------------------------
	Return values:
	N/A
	============================================================================
	*/
	
	public function search()
	{
		//Load Model 
		$this->load->model('street_file_model');

		// Set up validation rules
		$this->form_validation->set_rules('code', 'Street Code', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('name', 'Street Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('communities', 'Communities', 'trim|xss_clean');

		if ($this->form_validation->run() == true)
		{
			// if the validation passes, grab the POST variables
			$post = $this->input->post(null, true);

			// if there is a string to search on..
			if (!empty($post))
			{
				// hand it off to the model and retrieve the record set.
				$data['communities'] = $this->street_file_model->get_cities();
				$data['streets'] = $this->street_file_model->search_streets($post);
				$data['street_types'] = $this->street_file_model->get_street_file_types();


				if (count($data['streets']) > 500){$data['streets'] = array_slice($data['streets'], 0, 500);}
				$data['content'] = "pages/street_file/home_view";
				$this->load->view('master', $data);
			}
			else
			{
				// else just send the user back to home which displays all records.
				$this->index();
			}
		}
		else
		{
			$this->index();
		}
	}

	/*           
	============================================================================
	street( $id = null )
	----------------------------------------------------------------------------
	(int) $id - The id of the street that the user wants to view.
	----------------------------------------------------------------------------
	Return values:
	N/A
	============================================================================
	*/
	
	public function street( $id = null )
	{
		//Load Model 
		$this->load->model('street_file_model');

		// if no id is being passed then load the add street view.
		if ($id == null)
		{
			$data['street_types'] = $this->street_file_model->get_street_file_types();
			$data['content'] = "pages/street_file/add_street_file_view";

			$this->load->view('master', $data);
		}
		// else there is an id and the user wants to update that record.
		else
		{
			$data['street'] = $this->street_file_model->get_street_file_by_id($id);

			// $key = $this->street_file_model->get_next_key();
			$data['street_types'] = $this->street_file_model->get_street_file_types();
			$data['content'] = "pages/street_file/update_street_file_view";

			$this->load->view('master', $data);
		}

	}

	/*           
	============================================================================
	add() - This function inserts a record into the table.
	----------------------------------------------------------------------------
	N/A
	----------------------------------------------------------------------------
	Return values:
	N/Ao
	============================================================================
	*/
	
	public function add()
	{
		//Load Model 
		$this->load->model('street_file_model');

		// validate the input fields
		$this->form_validation->set_rules('STNAME', 'Street Name', 'trim|max_length[20]|required|xss_clean');
		$this->form_validation->set_rules('SUFFIX', 'Street Type', 'trim|xss_clean');
		$this->form_validation->set_rules('CITY', 'City', 'trim|max_length[50]|required|xss_clean');
		$this->form_validation->set_rules('PROVINCE', 'Province', 'trim|max_length[2]|required|xss_clean');
		$this->form_validation->set_rules('LSTNOLO', 'Odd Low', 'trim|max_length[5]|required|xss_clean');
		$this->form_validation->set_rules('LSTNOHI', 'Odd High', 'trim|max_length[5]|required|xss_clean');
		$this->form_validation->set_rules('RSTNOLO', 'Even Low', 'trim|max_length[5]|required|xss_clean');
		$this->form_validation->set_rules('RSTNOHI', 'Even High', 'trim|max_length[5]|required|xss_clean');
		$this->form_validation->set_rules('SDE_STNAME', 'Full Street Name', 'trim|max_length[50]|required|xss_clean');
		$this->form_validation->set_rules('SDE_CITY', 'Full City Name', 'trim|max_length[50]|required|xss_clean');
		
		// get POST data.
		$post = $this->input->post(null, true);

		$post['MODBY'] = $this->session->userdata('samaccountname');
		$post['MODDTTM'] = date('y-m-d');

		$post['ADDBY'] = $this->session->userdata('samaccountname');
		$post['ADDDTTM'] = date('y-m-d');


		foreach ($post as $key => $value) 
		{
			if (is_string($value) )
			{
				$post[$key] = strtoupper($value);
			}
		}


		if ($this->form_validation->run() == false)
		{
			// if the validation fails send them back tot he street they were viewing with an error message.
			$this->street();
		}
		else
		{	
			$post['STR_CODE'] = $this->street_file_model->get_next_val();

			//Else attempt an update
			if ($this->street_file_model->insert_street_file($post))
			{
				// Success
				$this->session->set_flashdata('message', $post['STNAME']." ".$post['SUFFIX']." has been successfully inserted.");
				redirect('street_file/street/'.$post['STR_CODE']);
			}
			else
			{
				// Fail
				$this->session->set_flashdata('error', $post['STNAME']." ".$post['SUFFIX']." could not be insterted, please try again.");
				redirect('street_file');
			}
		}
	}

	/*           
	============================================================================
	update()
	----------------------------------------------------------------------------
	N/A
	----------------------------------------------------------------------------
	Return values:
	N/A
	============================================================================
	*/
	
	public function update()
	{	
		// Set validation rules
		$this->form_validation->set_rules('PROVINCE', 'Province', 'trim|max_length[2]|xss_clean');
		// Get POST variables
		$post_data = $this->input->post(null, true);
	
		// Add last updated by and time
		$post_data['MODBY'] = $this->session->userdata('samaccountname');
		$post_data['MODDTTM'] = date('Y-m-d');

		if (empty($post_data['EXP_FLAG']))
		{
			$post_data['EXP_FLAG'] = null;
		}

		foreach ($post_data as $key => $value) 
		{
			if (is_string($value) )
			{
				$post_data[$key] = strtoupper($value);
			}
		}
	
		// Load Model
		$this->load->model('street_file_model');

		// validate the input fields
		$this->form_validation->set_rules('STNAME', 'Street Name', 'trim|max_length[20]|required|xss_clean');
		$this->form_validation->set_rules('SUFFIX', 'Street Type', 'trim|xss_clean');
		$this->form_validation->set_rules('CITY', 'City', 'trim|max_length[50]|required|xss_clean');
		$this->form_validation->set_rules('PROVINCE', 'Province', 'trim|max_length[2]|required|xss_clean');
		$this->form_validation->set_rules('LSTNOLO', 'Odd Low', 'trim|max_length[5]|required|xss_clean');
		$this->form_validation->set_rules('LSTNOHI', 'Odd High', 'trim|max_length[5]|required|xss_clean');
		$this->form_validation->set_rules('RSTNOLO', 'Even Low', 'trim|max_length[5]|required|xss_clean');
		$this->form_validation->set_rules('RSTNOHI', 'Even High', 'trim|max_length[5]|required|xss_clean');
		$this->form_validation->set_rules('SDE_STNAME', 'Full Street Name', 'trim|max_length[50]|required|xss_clean');
		$this->form_validation->set_rules('SDE_CITY', 'Full City Name', 'trim|max_length[50]|required|xss_clean');

		if ($this->form_validation->run() == false)
		{
			// if the validation fails send them back tot he street they were viewing with an error message.
			$this->session->set_flashdata('error', validation_errors());
			redirect('street_file/street/'.$post_data['STR_CODE']);
		}
		else
		{	
			// Else attempt an update
			if ($this->street_file_model->update_street_file($post_data))
			{
				// Success
				$this->session->set_flashdata('message', $post_data['STNAME']." ".$post_data['SUFFIX']." has been successfully updated.");
				redirect('street_file/street/'.$post_data['STR_CODE']);
			}
			else
			{
				// Fail
				$this->session->set_flashdata('error', $post_data['STNAME']." ".$post_data['SUFFIX']." could not be updated, please try again.");
				redirect('street_file/street/'.$post_data['STR_CODE']);
			}
		}
	}

	/*           
	============================================================================
	delete()
	----------------------------------------------------------------------------
	N/A
	----------------------------------------------------------------------------
	Return values:
	N/A
	============================================================================
	*/
	
	// public function delete()
	// {
	// 	//Load Model 
	// 	$this->load->model('street_file_model');

	// 	$post_data = $this->input->post(null, true);


	// 	if ($this->street_file_model->remove_street_file($post_data['STR_CODE']))
	// 	{
	// 		$this->session->set_flashdata('message', 'The record has been removed.');
	// 		redirect('street_file');
	// 	}
	// 	else
	// 	{ 
	// 		$this->session->set_flashdata('error', 'The record could not be removed, please try again.');
	// 		redirect('street_file');
	// 	}
	// }

}

/* End of file street_file.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/street_file.php */