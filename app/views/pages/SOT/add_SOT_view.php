<div class="row">
	<div class="col-md-12">
		<h2>Add summary offence ticket</h2>
		<p class="lead">This form will allow you to add a new SOT to the system.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php elseif (validation_errors()): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo validation_errors(); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>

<legend>Summary Offence Ticket <p style="font-size: 12px;" class="text-danger">Required *</p></legend>
<div class="row">
	<div class="col-md-12">
	<form action="<?php echo base_url(); ?>SOT/add_SOT" method="post" >
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="SOT_NUM">SOT #<i class="text-danger">*</i></label>
					<input type="text" name="SOT_NUM" class="form-control" title="SOT Number" value="<?php echo set_value('SOT_NUM'); ?>"  />
				</div>
				<div class="col-sm-3">
					<label for="ISSUE_DTE">Issue Date<i class="text-danger">*</i></label>
					<input type="text" name="ISSUE_DTE" class="form-control date_picker" title="Issue Date" value="<?php echo set_value('ISSUE_DTE'); ?>" readonly />
				</div>
				<div class="col-sm-3">
					<label for="SYSTEM_NME">System Name</label>
					<select name="SYSTEM_NME" class="form-control" title="" value="<?php echo set_value('SYSTEM_NME'); ?>">
						<option value="null">Select a system</option>
						<option value="HANSEN">Hansen</option>
						<option value="BCT">BCT</option>
						<option value="ACS">ACS</option>
						<option value="FDM">FDM</option>
						<option value="OTHER">Other</option>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="CASE_NUM">Case Number</label>
					<input type="text" name="CASE_NUM" class="form-control" title="Case Number" value="<?php echo set_value('CASE_NUM'); ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-2 no_label_fix">
					<button type="button" value="ISSUE_ID" class="btn btn-primary btn-sm modal_btn" data-toggle="modal" data-target="#myModal">Select Officer</button>
				</div>
				<div class="col-sm-2">
					<label for="ISSUE_ID">Officer Id</label>
					<input autocomplete="off" type="text" name="ISSUE_ID" class="form-control ISSUE_ID" title="Officer" value="<?php echo set_value('ISSUE_ID'); ?>" maxlength="10" disabled />
					<input type="hidden" name="ISSUE_ID" class="form-control ISSUE_ID"  value="<?php echo set_value('ISSUE_ID'); ?>"  />
				</div>
				<div class="col-sm-4">
					<label for="officer_name">Officer Name</label>
					<input type="text" name="officer_name" class="form-control officer_name" title="Officer Name" value="<?php echo set_value('officer_name'); ?>" disabled />
					<input type="hidden" name="officer_name" class="form-control officer_name" value="<?php echo set_value('officer_name'); ?>" />
				</div>
				<div class="col-sm-4">
					<label for="AGENCY_NME">Business Unit/Resp</label>
					<input type="text" name="AGENCY_NME" class="form-control AGENCY_NME" title="Business Unit" value="<?php echo set_value('AGENCY_NME'); ?>" disabled />
					<input type="hidden" name="AGENCY_NME" class="form-control AGENCY_NME" value="<?php echo set_value('AGENCY_NME'); ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="BYLAW_CDE">Bylaw Code<i class="text-danger">*</i></label>
					<select name="BYLAW_CDE" class="form-control AJAX-BYLAW_CDE">
						<option value="">Select..</option>
						<?php foreach ($bylaws as $value): ?>
							<option value="<?= $value['BYLAW_CDE'] ?>"><?= $value['BYLAW_CDE'] ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="SECTION_CDE" onchange="get_desc(this)">Section Code</label>
					<select name="SECTION_CDE" class="form-control AJAX-SECTION_CDE" value="<?php echo set_value('SECTION_CDE'); ?>">
						<option value="null">Select a Bylaw..</option>
					</select>
				</div>
				<div class="col-sm-7">
					<label for="bylaw_description">Bylaw</label>
					<textarea readonly name="bylaw_description" class="form-control AJAX-BYLAW_DSC" title="Bylaw" maxlength="1000"><?php echo set_value('bylaw_description'); ?></textarea>
				</div>
			</div>
		</div>

		<legend>Contact Address</legend>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2 no_label_fix">
					<button type="button" value="CONTACT_NUM" class="btn btn-primary btn-sm modal_btn" data-toggle="modal" data-target="#myModal_contact">Select Contact</button>
				</div>
				<div class="col-sm-3">
					<label for="CONTACT_NUM">Contact # <i class="text-danger">*</i></label>
					<input disabled autocomplete="off" type="text" name="CONTACT_NUM" class="form-control" title="Contact Number" value="<?php echo set_value('CONTACT_NUM'); ?>" />
					<input type="hidden" name="CONTACT_NUM" value="<?php echo set_value('CONTACT_NUM'); ?>" />
				</div>
				<div class="col-sm-1">
					<label for="TITLE_DSC">Title</label>
					<input disabled type="text" name="TITLE_DSC" class="form-control" title="Title" value="<?php echo set_value('TITLE_DSC'); ?>" />
					<input type="hidden" name="TITLE_DSC" value="<?php echo set_value('TITLE_DSC'); ?>" />
				</div>
				<div class="col-sm-3">
					<label for="FIRST_NME">First Name</label>
					<input disabled type="text" name="FIRST_NME" class="form-control" title="First Name" value="<?php echo set_value('FIRST_NME'); ?>" />
					<input type="hidden" name="FIRST_NME" value="<?php echo set_value('FIRST_NME'); ?>" />
				</div>
				<div class="col-sm-3">
					<label for="LAST_NME">Last Name</label>
					<input disabled type="text" name="LAST_NME" class="form-control" title="Last Name" value="<?php echo set_value('LAST_NME'); ?>" />
					<input type="hidden" name="LAST_NME" value="<?php echo set_value('LAST_NME'); ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<label for="CIVIC_ADDR">Civic Address</label>
					<input disabled type="text" name="CIVIC_ADDR" class="form-control" title="Civic Address" value="<?php echo set_value('CIVIC_ADDR'); ?>" />
					<input type="hidden" name="CIVIC_ADDR" value="<?php echo set_value('CIVIC_ADDR'); ?>" />
				</div>
				<div class="col-sm-6">
					<label for="MAIL_ADDR">Mailing Address</label>
					<input disabled type="text" name="MAIL_ADDR" class="form-control" title="Mailing Address" value="<?php echo set_value('MAIL_ADDR'); ?>" />
					<input type="hidden" name="MAIL_ADDR" value="<?php echo set_value('MAIL_ADDR'); ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="country">Country</label>
					<input disabled type="text" name="COUNTRY_DSC" class="form-control" title="Country" value="<?php echo set_value('COUNTRY_DSC'); ?>" />
					<input type="hidden" name="COUNTRY_DSC" value="<?php echo set_value('COUNTRY_DSC'); ?>" />
				</div>
				<div class="col-sm-4">
					<label for="provence">Prov.</label>
					<input disabled type="text" name="PROV_DSC" class="form-control" title="Province" value="<?php echo set_value('PROV_DSC'); ?>" />
					<input type="hidden" name="PROV_DSC" value="<?php echo set_value('PROV_DSC'); ?>" />
				</div>
				<div class="col-sm-3">
					<label for="potal_code">Postal Code</label>
					<input disabled type="text" name="POSTAL_CDE" class="form-control" title="Postal Code" value="<?php echo set_value('POSTAL_CDE'); ?>" />
					<input type="hidden" name="POSTAL_CDE" value="<?php echo set_value('POSTAL_CDE'); ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<label for="AREA_CDE">Area</label>
					<input disabled type="text" name="AREA_CDE" class="form-control" title="Area Code" value="<?php echo set_value('AREA_CDE'); ?>" />
					<input type="hidden" name="AREA_CDE" value="<?php echo set_value('AREA_CDE'); ?>" />
				</div>
				<div class="col-sm-3">
					<label for="PHONE_NUM">Home Phone</label>
					<input disabled type="text" name="PHONE_NUM" class="form-control" title="Home Phone" value="<?php echo set_value('PHONE_NUM'); ?>" />
					<input type="hidden" name="PHONE_NUM" value="<?php echo set_value('PHONE_NUM'); ?>" />
				</div>
				<div class="col-sm-1">
					<label for="EXT_CDE">Ext.</label>
					<input disabled type="text" name="EXT_CDE" class="form-control" title="Extention" value="<?php echo set_value('EXT_CDE'); ?>" />
					<input type="hidden" name="EXT_CDE" value="<?php echo set_value('EXT_CDE'); ?>" />
				</div>
				<div class="col-sm-1">
					<label for="PHONE_FLG">Type</label>
					<input disabled type="text" name="PHONE_FLG" class="form-control" title="Type" value="<?php echo set_value('PHONE_FLG'); ?>" />
					<input type="hidden" name="PHONE_FLG" value="<?php echo set_value('PHONE_FLG'); ?>" />
				</div>
			</div>
		</div>

		<legend>Summary Offence Information</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="CIVIC_NUM">Civic #</label>
					<input autocomplete="off" type="text" name="CIVIC_NUM" class="form-control" title="Civic Number"   value="<?php echo set_value('CIVIC_NUM'); ?>" />
				</div>
				<div class="col-sm-1">
					<button type="button" value="CONTACT_NUM" class="btn btn-primary btn-sm modal_btn no_label_fix" data-toggle="modal" data-target="#myModal_street">Select Street</button>
				</div>
				<div class="col-sm-6">
					<label for="CIVIC_ADDRESS">Civic Address of Offence</label>
					<input disabled type="text" name="CIVIC_ADDRESS" class="form-control CIVIC_ADDRESS" title="Civic Address Of Offence" value="<?php echo set_value('CIVIC_ADDRESS'); ?>" />
					<input type="hidden" name="CIVIC_ADDRESS" class="form-control CIVIC_ADDRESS" value="<?php echo set_value('CIVIC_ADDRESS'); ?>" />
				</div>
				<div class="col-sm-1">
					<label for="APT_NUM">Apt.</label>
					<input type="text" name="APT_NUM" class="form-control" title="Apartment" value="<?php echo set_value('APT_NUM'); ?>" />
				</div>
				<div class="col-sm-2">
					<label for="DISTRICT_NUM">District</label>
					<input type="text" name="DISTRICT_NUM" class="form-control" title="District" value="<?php echo set_value('DISTRICT_NUM'); ?>" />
				</div>
			</div>
		</div>
		

		<div class="form-group">
			<div class="row">
				<div class="col-sm-8">
					<label for="OFFENCELOC_DSC">Offence Location</label>
					<input type="text" name="OFFENCELOC_DSC" class="form-control" title="Office Location" value="<?php echo set_value('OFFENCELOC_DSC'); ?>" />
				</div>
				<div class="col-sm-4">
					<label for="ARRAIGN_DTE">Arraign Date</label>
					<input type="text" name="ARRAIGN_DTE" class="form-control date_picker" title="Arrain Date" value="<?php echo set_value('ARRAIGN_DTE'); ?>" readonly />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="COURT_DTE">Trial Date</label>
					<input type="text" name="COURT_DTE" class="form-control date_picker" title="Trial Date" value="<?php echo set_value('COURT_DTE'); ?>" readonly />
				</div>
				<div class="col-sm-4">
					<label for="OUTCOME_DSC">Outcome</label>
					<select name="OUTCOME_DSC" class="form-control" title="Outcome" value"<?php echo set_value('OUTCOME_DSC'); ?>">
						<option selected value=" ">Select..</option>
						<option value="GUILTY">Guilty</option>
						<option value="NOT GUILTY">Not Guilty</option>
						<option value="DROP CHARGE">Drop Charge</option>
						<option value="VOL PYMNT">Vol. Payment</option>
						<option value="IN ABSENCE">In Absence</option>
						<option value="WITHDRAWAL">Withdrawn</option>
						<option value="CHANGE OF PLEA">Change of Plea</option>
					</select>
				</div>
				<div class="col-sm-4">
					<label for="FINE_AMT">Fine Amount</label>
					<input type="text" name="FINE_AMT" class="form-control" title="Fine Amount" value="<?php echo set_value('FINE_AMT'); ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<label for="NOTES_DSC">Notes</label>
					<textarea name="NOTES_DSC" class="form-control" title="Notes" maxlength="1000"><?php echo set_value('NOTES_DSC'); ?></textarea>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_SOT pull-right" value="Add SOT" />
					<a href="<?php echo base_url(); ?>SOT/summary_offence_tickets" class="btn btn-md btn-default return_SOT">Back to SOTs</a>
				</div>
			</div>
		</div>
	</form>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Officer Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-3">
							<div class="modal_search">
								<label for="employee_search_box">First Name</label>
								<input type="text" name="officer_first_search_box" id="" class="form-control AJAX-ISSUE_ID" title="Name" autofocus maxlength="" />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="modal_search">
								<label for="employee_search_box">Last Name</label>
								<input type="text" name="officer_last_search_box" id="" class="form-control AJAX-ISSUE_ID" title="Name" autofocus maxlength="" />
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Name</th>
					</thead>
					<tbody class="modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" id="myModal_contact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Contact Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<div class="modal_search">
								<label for="search_box">Last Name</label>
								<input type="text" name="search_box" id="" class="form-control AJAX-CONTACT_NUM" title="ID" autofocus maxlength="" />
								<p class="text-muted">(Select the name of the record you would like to use.)</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Name</th>
					</thead>
					<tbody class="modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="myModal_street" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Street Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<div class="modal_search">
								<label for="search_box">Name</label>
								<input type="text" name="search_box" id="" class="form-control AJAX-STREET_NAME" title="ID" autofocus maxlength="" />
								<p class="text-muted">(Select the name of the record you would like to use.)</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Street</th>
					</thead>
					<tbody class="street_modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->