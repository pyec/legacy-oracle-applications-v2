<div class="row">
	<div class="col-md-6">
		<h2>Update Business Unit</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>BU_update/update_BU" method="POST">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<input type="hidden" name="BUSUNIT_CDE" class="form-control" title="Unit ID" readonly maxlength="3" value="<?= $business_unit[0]['BUSUNIT_CDE'] ?>" />
						<label for="BUSUNIT_DSC">District Name</label>
						<input type="text" name="BUSUNIT_DSC" class="form-control" title="District Name" maxlength="40" value="<?= $business_unit[0]['BUSUNIT_DSC'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="CURRENT_FLG">Active?</label>
						<select name="CURRENT_FLG" class="form-control">
							<?php if ($business_unit[0]['CURRENT_FLG'] == 'Y'): ?>
								<option value="Y" selected>Yes</option>
								<option value="N" >No</option>
							<?php else: ?>
								<option value="N" selected>No</option>
								<option value="Y">Yes</option>
							<?php endif ?>
						</select>
					</div>
					<div class="col-sm-2">
						<input type="submit" class="btn btn-success btn-md no_label_fix" value="Update Business Unit" />
					</div>
				</div>
			</div>
		</form>
		<legend></legend>

		<div class="form-group">
			<div class="row">
				 <div class="col-sm-12">
					<a href="<?php echo base_url(); ?>BU_update" class="btn btn-default">Back to Business Units</a>
				 </div>
			</div>
		</div>
	</div>
</div>