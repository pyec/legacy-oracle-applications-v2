<div class="row">
	<div class="col-md-6">
		<h2>Add Source Types</h2>
	</div>
</div>

<!-- Display messages back to the user if there set. -->
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<form action="<?php echo base_url(); ?>MCT/add_source_type" method="post">
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-sm-4">
					<label for="source_type_name">Source Type Name</label>
					<input type="text" name="MCT_SOURCE_DESC" class="form-control" />
				</div>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-success btn-md add_source_type" value="Add Source Type" />
			</div>
		</div>	
	</form>
</div>