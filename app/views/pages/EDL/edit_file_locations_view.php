<?php
// determine if the user is an admin -- redirect if not
$is_admin = false;
$is_readwrite = false;

$groups = $this->session->userdata('groups');

foreach ($groups as $group)
{
    if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite')
    {
        $is_readwrite = true;
    }

    if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
    {
        $is_admin = true;
    }
}

if (!($is_admin) && !($is_readwrite)) {
    redirect('EDL/');
}
?>

<div class="row">
	<div class="col-md-12">
		<h2>Manage Locations</h2>
		<p class="lead">Use this form to add/remove a location record for file # : <?= $FILE_ID ?></p>
		<!-- <div class="alert alert-warning">
		    <strong>Please Note : </strong> To update a location record it must be deleted and then re-added.
		</div> -->
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<?php if (!empty($locations)): ?>
		<?php if (!empty($locations)): ?>
        <?php $locationCounter = 0; ?>
			<?php foreach ($locations as $loc): ?>
            <?php $locationCounter++; ?>
            <?php if ($locationCounter > 1): ?>
                <hr style="margin-top: 30px; ">
            <?php endif ?>
<form action="<?php echo base_url(); ?>EDL/update_delete_file_location" method="POST">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="PROCESS">Process</label>
                        <select name="PROCESS" class="form-control" title="Process">
                            <option value="">Select..</option>
                            <?php foreach ($process as $pro): ?>
                                <?php if ($loc['PROCESS'] == $pro['PROCESS_CODE']): ?>
                                    <option selected value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                                <?php else : ?>
                                    <option value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
					</div>
					<div class="col-sm-2">
						<label for="DEVICE">Device</label>
                        <select name="DEVICE" class="form-control" title="Device">
                            <option value="">Select..</option>
                            <?php foreach ($devices as $device): ?>
                                <?php if ($loc['DEVICE'] == $device['DEVICE_CODE']): ?>
                                    <option selected value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                                <?php else : ?>
                                    <option value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
					</div>
					<div class="col-sm-2">
						<label for="STATUS">Status</label>
                        <select name="STATUS" class="form-control" title="Status">
                            <option value="">Select..</option>
                            <?php foreach ($statuses as $status): ?>
                                <?php if ($loc['STATUS'] == $status['STATUS_CODE']): ?>
                                    <option selected value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                                <?php else : ?>
                                    <option value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
					</div>
					<div class="col-sm-2">
						<label for="DRAWN">Drawn By</label>
                        <select name="DRAWN" class="form-control" title="Drawn">
                            <option value="">Select..</option>
                            <?php foreach ($drawn_by as $drawn): ?>
                                <?php if ($loc['DRAWN'] == $drawn['DRAWN_CODE']): ?>
                                    <option selected value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                                <?php else : ?>
                                    <option value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
					</div>
                    <?php if ($locationCounter == 1) : ?>
                    <div class="col-sm-4">
                        <label for="ACTION" class="pull-right" style="padding-right: 15px;" >Action</label>
                    </div>
                    <?php endif ?>
				</div>
            </div>
                <div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="DATETIME">Date</label>
                        <input type="text" id="DATETIME<?= $loc['ID'] ?>" name="DATETIME" class="form-control date_picker_new" title="Date" maxlength="" readonly
                               value="<?= $loc['DATETIME'] ?>"/>
					</div>
					<div class="col-sm-4">
						<label for="FILE_LOC">File Location</label>
						<input type="text" name="FILE_LOC" class="form-control" title="File Location" value="<?= $loc['FILE_LOC'] ?>"  />
					</div>
					<div class="col-sm-5">
						<label for="PHYSICAL_DESC">Physical Description</label>
						<input type="text" name="PHYSICAL_DESC" class="form-control" title="Physical Description" maxlength="" value="<?= $loc['PHYSICAL_DESC'] ?>"  />
					</div>
 					<div class="col-sm-1">
						<input type="hidden" name="FILE_ID" value="<?= $FILE_ID ?>" />
                        <input type="hidden" name="ID" value="<?= $loc['ID'] ?>" />

					</div>
				</div>
			</div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12 container text-right">
                        <input type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-sm btn-success no_label_fix" title="Update" value="Update">
                        <input type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-sm btn-danger delete_location no_label_fix" title="Delete" value="Delete">
                    </div>
                </div>
            </div>
</form>

			<?php endforeach ?>
		<?php endif ?>
<?php endif ?>

<hr style="margin-top: 30px; ">
<form action="<?php echo base_url(); ?>EDL/add_file_locations" method="POST">
    <div class="form-group">
        <div class="row">
            <div class="col-sm-2">
                <label for="PROCESS">Process</label>
                <select name="PROCESS" class="form-control" title="Process">
                    <option value="">Select..</option>
                    <?php foreach ($process as $pro): ?>
                        <?php if ($add_location['PROCESS'] == $pro['PROCESS_CODE']): ?>
                            <option selected value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                        <?php else : ?>
                            <option value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                        <?php endif ?>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-sm-2">
                <label for="DEVICE">Device</label>
                <select name="DEVICE" class="form-control" title="Device">
                    <option value="">Select..</option>
                    <?php foreach ($devices as $device): ?>
                        <?php if ($add_location['DEVICE'] == $device['DEVICE_CODE']): ?>
                            <option selected value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                        <?php else : ?>
                            <option value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                        <?php endif ?>
                    <?php endforeach ?>
                </select>
            </div>

            <div class="col-sm-2">
                <label for="STATUS">Status</label>
                <select name="STATUS" class="form-control" title="Status">
                    <option value="">Select..</option>
                    <?php foreach ($statuses as $status): ?>
                        <?php if ($add_location['STATUS'] == $status['STATUS_CODE']): ?>
                            <option selected value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                        <?php else : ?>
                            <option value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                        <?php endif ?>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-sm-2">
                <label for="DRAWN">Drawn By</label>
                <select name="DRAWN" class="form-control" title="Drawn">
                    <option value="">Select..</option>
                    <?php foreach ($drawn_by as $drawn): ?>
                        <?php if ($add_location['DRAWN'] == $drawn['DRAWN_CODE']): ?>
                            <option selected value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                        <?php else : ?>
                            <option value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                        <?php endif ?>
                    <?php endforeach ?>
                </select>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-2">
                <label for="DATETIME">Date</label>
                <?php $value = isset($DATETIME) ? $DATETIME : ''; ?>
                <input type="text" id="DATETIME" name="DATETIME" class="form-control date_picker_new" title="Date" maxlength="" readonly
                       value="<?= $add_location['DATETIME'] ?>"/>
            </div>
            <div class="col-sm-4">
                <label for="FILE_LOC">File Location</label>
                <input type="text" name="FILE_LOC" class="form-control" title="File Location" maxlength=""
                       value="<?= $add_location['FILE_LOC'] ?>"/>
            </div>
            <div class="col-sm-5">
                <label for="PHYSICAL_DESC">Physical Description</label>
                <input type="text" name="PHYSICAL_DESC" class="form-control" title="Physical Description" maxlength=""
                       value="<?= $add_location['PHYSICAL_DESC'] ?>"/>
            </div>
            <div class="col-sm-1 container text-right">
<!--                <label for="FILE_ID">Add</label>-->
                <input type="hidden" name="FILE_ID" value="<?= $FILE_ID ?>"/>
                <button type="submit" class="btn btn-sm btn-success no_label_fix" title="Add">Add</button>
            </div>
        </div>
    </div>
</form>


<hr>
<div class="form-group">
	<div class="row">
		<div class="col-sm-12 ">
			<a href="<?php echo base_url(); ?>EDL/" class="btn btn-md btn-default ">Back to EDL Lookup</a>
			<a class="btn btn-md btn-primary pull-right" href="<?php echo base_url(); ?>EDL/update_location/<?= $FILE_ID ?>">Back to Record</a>
		</div>
	</div>
</div>
</form>