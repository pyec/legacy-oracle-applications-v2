<div class="row">
	<div class="col-md-12">
		<h2>Update Course</h2>
		<p class="lead">This form will allow you to update a course in the system.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<form action="<?php echo base_url(); ?>TPW/update_employee_course" method="post" >
				<input type="hidden" name="EMPL_ID" value="<?= $this->uri->segment(3); ?>" />
				<div class="col-sm-2">
					<label for="COURSE_NUM">Course #</label>
					<input type="text" name="COURSE_NUM" class="form-control" title="Course Number" value="<?= $course['COURSE_NUM'] ?>" readonly />
				</div>

				<div class="col-sm-2">
					<label for="COURSE_DTE">Course date</label>
					<input type="text" name="COURSE_DTE" class="form-control date_picker_new" title="Course Date" value="<?= $course['COURSE_DTE'] ?>" readonly />
				</div>

				<div class="col-sm-2">
					<label for="EXPIRY_DTE">Expiry date</label>
					<input type="text" name="EXPIRY_DTE" class="form-control date_picker_new" title="Expiry Date" value="<?= $course['EXPIRY_DTE'] ?>" readonly/>
				</div>
				
				<div class="col-sm-2 no_label_fix">
					<label for="RECERT_FLG">Recert Flag</label>
					<input type="checkbox" name="RECERT_FLG" class="" title="Recert Flag" <?php echo $checked = ($course['RECERT_FLG'] == 'Y' ? 'checked':''); ?> />
				</div>

				<div class="col-sm-2 no_label_fix">
					<label for="PASS_FLG">Pass Flag</label>
					<input type="checkbox" name="PASS_FLG" class="" title="Recert Flag" <?php echo $checked = ($course['PASS_FLG'] == 'Y' ? 'checked':''); ?> />
				</div>

		</div>
			<div class="form-group">
				<div class="col-sm-8">
					<label for="COURSE_COMMENTS">Comments</label>
					<textarea class="form-control" name="COURSE_COMMENTS" ><?= $course['COURSE_COMMENTS'] ?></textarea>
				</div>
			</div>

		<div class="form-group">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_course pull-right" value="Update" />
					<a href="<?php echo base_url(); ?>TPW/view_employee/<?= $course['EMPL_ID'] ?>" class="btn btn-md btn-default return">Back to Employee Courses</a>
				</div>
			</form>
		</div>
	</div>
</div>