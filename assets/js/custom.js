$('.delete_surrounding').click(function(event) {	
	if (!confirm('Are you sure you want to DELETE this surrounding street?')) {
		event.preventDefault();
	};
});

$('.delete_location').click(function(event) {
    if (!confirm('Are you sure you want to DELETE this location?')) {
        event.preventDefault();
    };
});

$('.delete_drawing').click(function(event) {
    if (!confirm('Are you sure you want to DELETE this drawing?')) {
        event.preventDefault();
    };
});

$('.update_to_complete').click(function(event) {	
	if (!confirm('Are you sure you want to update all the employees in this course to complete as of today?')) {
		event.preventDefault();
	};
});

$('.tbody').delegate('.delete_emp', 'click', function(event) {	
	if (!confirm('Are you sure you want to remove this employee?')) {
		event.preventDefault();
	};
});
$('.delete_attachments').click(function(event) {	
	if (!confirm('Are you sure you want to delete this files attachments?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_SOT', 'click', function(event) {	
	if (!confirm('Are you sure you want to remove this SOT?')) {
		event.preventDefault();
	};
});

$('.tbody').delegate('.delete_business_unit', 'click', function(event) {	
	if (!confirm('Are you sure you want to remove this business unit?')) {
		event.preventDefault();
	};
});


$('.tbody').delegate('.delete_endorsement', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this endorsement?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_condition', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this condition?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_course', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this course?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_date', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this course date?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_file', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this location record?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_survey', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this survey record?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_atr', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this survey record?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_MTMTC', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this survey record?')) {
		event.preventDefault();
	};
});
$('.tbody').delegate('.delete_plan', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this plan record?')) {
		event.preventDefault();
	};
});	

$('.tbody').delegate('.delete_table_maintenance', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this record?')) {
		event.preventDefault();
	};
});

$('.tbody').delegate('.delete_call', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this call record?')) {
		event.preventDefault();
	};
});	

$('.tbody').delegate('.delete_source_type', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this source type record?')) {
		event.preventDefault();
	};
});	

$('.tbody').delegate('.delete_type', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this call type?')) {
		event.preventDefault();
	};
});	


$('.tbody').delegate('.delete_councillor', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this Councillor record?')) {
		event.preventDefault();
	};
});	

$('.tbody').delegate('.delete_cemetery_name', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this cemetery?')) {
		event.preventDefault();
	};
});	

$('.tbody').delegate('.delete_owners', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this owner?')) {
		event.preventDefault();
	};
});

$('.tbody').delegate('.delete_interred', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this interred?')) {
		event.preventDefault();
	};
});	

$('.tbody').delegate('.delete_contact', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this contact?')) {
		event.preventDefault();
	};
});	

$('.tbody').delegate('.delete_employee', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this employee?')) {
		event.preventDefault();
	};
});

$('.tbody').delegate('.delete_bylaw', 'click', function(event) {	
	if (!confirm('Are you sure you want to delete this bylaw?')) {
		event.preventDefault();
	};
});	

$('.clear_incomplete_interred_date').click(function(){
	$("input[name='INT_INCOMPDTE']").val('');
});

function asset_form_reset()
{
	var name = $("input[name='name']");
	name.attr('value', '');

	var phone_number = $("input[name='phone_number']");
	phone_number.attr('value', '');

	var civic_number = $("input[name='civic_number']");
	civic_number.attr('value', '');

	var street_name = $("input[name='street_name']");
	street_name.attr('value', '');

	var business_unit = $("select[name='business_unit']");
	business_unit.prop('selectedIndex', 0);
}

function plan_index_form_reset()
{
	var record_number = $("input[name='plan_number']");
	record_number.attr('value', '');
}

function traffic_atr_form_reset()
{
	var record_number = $("input[name='record_number']");
	record_number.attr('value', '');
}

function edl_files_form_reset()
{
	var project = $("select[name='project_type']");
	project.prop('selectedIndex', 0);

	var orig = $("select[name='originator']");
	orig.prop('selectedIndex', 0);

	var file_number = $("input[name='file_number']");
	file_number.attr('value', '');

	var first_name = $("input[name='street_number']");
	first_name.attr('value', '');
	
	var last_name = $("input[name='street_name']");
	last_name.attr('value', '');

	var signed = $("input[name='signed_date']");
	signed.attr('value', '');

	var tender = $("input[name='tender_number']");
	tender.attr('value', '');

	var description = $("input[name='description']");
	description.attr('value', '');

	var street_1 = $("input[name='street_1']");
	street_1.attr('value', '');

	var street_2 = $("input[name='street_2']");
	street_2.attr('value', '');
}

function mct_call_form_reset()
{
	var call_number = $("input[name='call_number']");
	call_number.attr('value', '');

	var first_name = $("input[name='first_name']");
	first_name.attr('value', '');
	
	var last_name = $("input[name='last_name']");
	last_name.attr('value', '');
}

function sot_employee_form_reset()
{
	var employee_number = $("input[name='employee_number']");
	employee_number.attr('value', '');

	var first_name = $("input[name='first_name']");
	first_name.attr('value', '');
	
	var last_name = $("input[name='last_name']");
	last_name.attr('value', '');
}

function sot_contact_form_reset()
{
	var contact_number = $("input[name='contact_number']");
	contact_number.attr('value', '');

	var first_name = $("input[name='first_name']");
	first_name.attr('value', '');
	
	var last_name = $("input[name='last_name']");
	last_name.attr('value', '');
}

function sot_bylaw_form_reset()
{
	var bylaw_code = $("input[name='bylaw_code']");
	bylaw_code.attr('value', '');
	
	var description = $("input[name='description']");
	description.attr('value', '');

	var section_code = $("input[name='section_code']");
	section_code.attr('value', '');
}

function sot_tickets_form_reset()
{
	var ticket_number = $("input[name='ticket_number']");
	ticket_number.attr('value', '');

	var officer = $("input[name='officer']");
	officer.attr('value', '');

	var location = $("input[name='location']");
	location.attr('value', '');
}

function HIRS_form_reset()
{
	var course_number = $("input[name='course_number']");
	course_number.attr('value', '');
	
	var course_title = $("input[name='course_title']");
	course_title.attr('value', '');
}

function pwt_eployees_form_reset()
{
	var reference_number = $("input[name='employee_number']");
	reference_number.attr('value', '');

	var first_name = $("input[name='first_name']");
	first_name.attr('value', '');
	
	var last_name = $("input[name='last_name']");
	last_name.attr('value', '');

	var middle_name = $("input[name='middle_name']");
	middle_name.attr('value', '');
}

function pwt_courses_form_reset()
{
	var reference_number = $("input[name='course_number']");
	reference_number.attr('value', '');

	var first_name = $("input[name='course_name']");
	first_name.attr('value', '');
}

function cem_owners_form_reset()
{
	var reference_number = $("input[name='reference_number']");
	reference_number.attr('value', '');

	var first_name = $("input[name='first_name']");
	first_name.attr('value', '');
	
	var last_name = $("input[name='last_name']");
	last_name.attr('value', '');

	var middle_name = $("input[name='middle_name']");
	middle_name.attr('value', '');

    var location = $("input[name='location']");
    location.attr('value', '');

 	$('#dvCount').html('');
    $('#dvResults').html('');
}

function phonebook_form_reset()
{
	// get reference to all the form attributes and clear there values.
	var first_name = $("input[name='first_name']");
	first_name.attr('value', '');
	
	var last_name = $("input[name='last_name']");
	last_name.attr('value', '');

	var position = $("input[name='position']");
	position.attr('value', '');

	var section = $("input[name='section']");
	section.attr('value', '');

	var email = $("input[name='email']");
	email.attr('value', '');

	var office_phone = $("input[name='office_phone']");
	office_phone.attr('value', '');

	var mobile_phone = $("input[name='mobile_phone']");
	mobile_phone.attr('value', '');
}

function street_file_form_reset()
{

	var name = $("input[name='name']");
	name.attr('value', '');
	$('.code').attr('value', '');

	$(".community").val(' ');
}

$(window).on("scroll", function() {
	var scroll = window.pageYOffset;
	if (scroll > 0) {$(".app_nav").css('margin-top', 0)}
		else{$(".app_nav").css('margin-top', 52)};
});

$(document).ready(function() {
	var scroll = window.pageYOffset;
	if (scroll > 0) 
	{
		$(".app_nav").css('margin-top', 0);
	}
	else
	{
		$(".app_nav").css('margin-top', 52);
	};
});

$('.date_picker-y-M-d').datetimepicker({
	format: 'd-M-y',
	timepicker: false,
	closeOnWithoutClick: true,
	defaultSelect: false
});

$('.date_picker-y-m-d').datetimepicker({
	format: 'd-m-y',
	timepicker: false,
	closeOnWithoutClick: true,
	defaultSelect: false
});

$('.clear_dates').click(function(event) {
	var date_inputs = $('.date_picker_new');

	for(var i = 0; i < date_inputs.length; i++)
	{
		date_inputs[i].value = '';
	}
});

$('.street_file_search_submit').click(function(event){
	if ($('.code').val() == '' && $('.community').val() == '' && $('.name').val() == '') 
	{
		event.preventDefault();
	}
});

$('.add_suffix').change(function(){
	$('.official_street_name').val($('.add_street_name').val()+' '+$('.add_suffix').val());
});

$('.add_street_name').keyup(function(e){
	$('.official_street_name').val(this.value+' '+$('.add_suffix').val());
});

$('.add_community_name').keyup(function(e){
	$('.official_community_name').val(this.value);
});

$('#update_street_file_form :input').bind('input', function() { 
   document.getElementById("street_file_update_button").disabled = false;
});

$("input[name='EXP_FLAG']").change(function(e){
	document.getElementById("street_file_update_button").disabled = false;
});

$(document).ready(function() {
	// Get the current url from the window obj.
	var url = window.location.href;
	url = url.split("/");

	if (url[5] == 'search_assets') 
	{
		$('#phonebook_tab').prop('class', ' ');
		$('#assets_tab').prop('class', 'active');

		$('#phonebook').prop('class', 'tab-pane');
		$('#assets').prop('class', 'tab-pane active');
	}

    $('[data-toggle="tooltip"]').tooltip();
});
