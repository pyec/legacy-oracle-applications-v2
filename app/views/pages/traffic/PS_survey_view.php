<div class="row">
	<div class="col-md-12">
		<h2>Pedestrian Studies Survey</h2>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?php echo base_url(); ?>traffic/add_PS" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="REC_CODE">Record #</label>
						<input type="text" name="REC_CODE" class="form-control" title="Record Number" maxlength="10" value="<?php echo set_value('REC_CODE') ?>" />
					</div>
					<div class="col-sm-2">
						<label for="TIME_AM" class="no_label_fix">AM</label>
						<input type="checkbox" name="TIME_AM" class="" <?php echo set_checkbox('TIME_AM', 'on'); ?> />
					</div>
					<div class="col-sm-2">
						<label for="TIME_PM" class="no_label_fix">PM</label>
						<input type="checkbox" name="TIME_PM" class="" <?php echo set_checkbox('TIME_PM', 'on'); ?> />
					</div>
					<div class="col-sm-2">
						<label for="TIME_OP" class="no_label_fix" title="Off Peak" >OP</label>
						<input type="checkbox" name="TIME_OP" class="" title="Off Peak" <?php echo set_checkbox('TIME_OP', 'on'); ?> />
					</div>
					<div class="col-sm-2">
						<label for="CREATE_DATE">Created On</label>
						<input type="text" name="CREATE_DATE" class="form-control date_picker" title="Created On" readonly value="<?php echo set_value('REC_CODE') ?>" />
					</div>
				</div>
			</div>

			<legend>Intersection</legend>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STR_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STR_CODE" class="form-control no_label_fix street_code" readonly value="<?php echo set_value('STR_CODE') ?>" />
							</div>
							<div class="col-sm-3">
								<label for="">Street Name</label>
								<input type="text"  class="form-control street_name" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<label for="">Type</label>
								<input type="text" class="form-control street_type" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<label for="">Community</label>
								<input type="text" class="form-control community" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STR2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STR2_CODE" class="form-control 2_street_code" readonly value="<?php echo set_value('STR2_CODE') ?>"/>
							</div>
							<div class="col-sm-3">
								<label for="">At</label>
								<input type="text"  class="form-control 2_street_name" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix 2_street_type" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix 2_community" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STR3_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STR3_CODE" class="form-control 3_street_code no_label_fix" readonly value="<?php echo set_value('STR3_CODE') ?>"/>
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix 3_street_name" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix 3_street_type" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix 3_community" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STR4_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STR4_CODE" class="form-control 4_street_code no_label_fix" readonly value="<?php echo set_value('STR4_CODE') ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix 4_street_name" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix 4_street_type" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix 4_community" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STR5_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STR5_CODE" class="form-control 5_street_code no_label_fix" readonly value="<?php echo set_value('STR5_CODE') ?>"/>
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix 5_street_name" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix 5_street_type" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix 5_community" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>

			<legend>Mid Block</legend>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STRB2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STRB2_CODE" class="form-control no_label_fix B2_street_code" readonly value="<?php echo set_value('STRB2_CODE') ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix B2_street_name" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix B2_street_type" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix B2_community" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STRB3_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<label for="STRB3_CODE">Between</label>
								<input type="hidden" name="STRB3_CODE" class="form-control B3_street_code" readonly value="<?php echo set_value('STRB3_CODE') ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix B3_street_name" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix B3_street_type" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix B3_community" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>

			<legend>Additional Information</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="X_COORD">X Coord</label>
						<input type="text" name="X_COORD" class="form-control PS_X_COORD" title="" value="<?php echo set_value('X_COORD') ?>" />
					</div>
					<div class="col-sm-2">
						<label for="Y_COORD">Y Coord</label>
						<input type="text" name="Y_COORD" class="form-control PS_Y_COORD" title="" value="<?php echo set_value('Y_COORD') ?>" />
					</div>
					<div class="col-sm-1">
						<label for="CIV_ADDR">Civic #</label>
						<input type="text" name="CIV_ADDR" class="form-control" title="" value="<?php echo set_value('CIV_ADDR') ?>" />
					</div>
					<div class="col-sm-2">
						<label for="SURVEY_DATE">Completion Date</label>
						<input type="text" name="SURVEY_DATE" class="date_picker form-control" title="" readonly value="<?php echo set_value('SURVEY_DATE') ?>" />
					</div>
					<div class="col-sm-2">
						<label for="region">Region</label>
						<select name="STR_REG" class="form-control" title="Region" maxlength="3" >
							<option <?php echo set_select('STR_REG', '', true); ?> value="">select..</option>
							<option <?php echo set_select('STR_REG', 'EST'); ?> value="EST">Eastern</option>
							<option <?php echo set_select('STR_REG', 'WST'); ?> value="WST">Western</option>
							<option <?php echo set_select('STR_REG', 'CNT'); ?> value="CNT">Central</option>
						</select>
					</div>

					<div class="col-sm-2">
						<label for="SURVEYOR">Surveyor</label>
							<input type="text" name="SURVEYOR" class="form-control pull-right surveyor_input" title="Surveyor" maxlength="3" />
					</div>
				</div>
			</div>

			

			<div class="form-group">
				<div class="row">
					<div class="col-sm-3 survey_categories">
						<label class="title">Survey Category</label><br />
						<input type="checkbox" name="SIGNAL" <?php echo set_checkbox('SIGNAL', 'on'); ?> /><label class="categories_label" for="SIGNAL">Signal</label>
						<input type="checkbox" name="CONT" <?php echo set_checkbox('CONT', 'on'); ?> /><label class="categories_label" for="CONT">Stop Controlled</label>
						<input type="checkbox" name="RA5" <?php echo set_checkbox('RA5', 'on'); ?> /><label class="categories_label" for="RA5">RA-5</label>
						<input type="checkbox" name="UNCONT" <?php echo set_checkbox('UNCONT', 'on'); ?> /><label class="categories_label" for="UNCONT">Uncontrolled Crosswalk</label>
						<input type="checkbox" name="HALFSIGNAL" <?php echo set_checkbox('HALFSIGNAL', 'on'); ?> /><label class="categories_label" for="HALFSIGNAL">Half Signal Intersection</label>
					</div>
					<div class="col-sm-2 survey_reuqested">
						<label>Requested</label><br />
						<input type="checkbox" name="SIGNAL_REQ" class="" <?php echo set_checkbox('SIGNAL_REQ', 'on'); ?> />
						<input type="checkbox" name="CONT_REQ" class="" <?php echo set_checkbox('CONT_REQ', 'on'); ?> />
						<input type="checkbox" name="RA5_REQ" class="" <?php echo set_checkbox('RA5_REQ', 'on'); ?> />
						<input type="checkbox" name="UNCONT_REQ" class="" <?php echo set_checkbox('UNCONT_REQ', 'on'); ?> />
						<input type="checkbox" name="HALFSIGNAL_REQ" class="" <?php echo set_checkbox('HALFSIGNAL_REQ', 'on'); ?> />
					</div>
					<div class="fom-group">
						<div class="col-sm-7">
							<label for="COMMENT_BOX">Comments</label>
							<textarea name="COMMENT_BOX" class="form-control" title="" ><?php echo set_value('COMMENT_BOX') ?></textarea>
						</div>
					</div>
				</div>
			</div>

			

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-primary btn-md pull-right" value="Add Survey" />
						<a href="<?php echo base_url(); ?>traffic/PS" class="btn btn-default btn-md">Back to Pedestrian Studies</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Street Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<div class="modal_search">
								<label for="search_box">Street Name</label>
								<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
								<p class="text-muted">(Select the name of the record you would like to use.)</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Street</th>
					</thead>
					<tbody class="modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->