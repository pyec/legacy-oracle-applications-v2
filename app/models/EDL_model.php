<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Engineer Drawing Location System Model

Description : This is the data layer for the EDL system.

  Functions : get_files( ),
  			get_file_by_id( $id = null ),
            get_files_by_id( $id = null ),
  			insert_file( $data = null ),
  			update_file( $data = null ),
  			remove_file( $data  = null),
  			get_departments( ),
  			update_departments( ),
  			get_devices( ),
  			update_devices( ),
  			get_paper_classifications( ),
  			update_paper_classifications( ),
  			get_electronic_classifications( ),
  			update_electronic_classifications( ),
  			get_drawn( ),
  			update_drawn( ),
  			get_process( ),
  			update_process( ),
  			get_originator( ),
  			update_originator( ),
  			get_project_type( ),
  			update_project_type( ),
  			get_locations( ),
  			update_locations( ),
  			get_surveyor( ),
  			update_surveyor( ),
  			get_survey_method( ),
  			update_survey_method( ),
  			get_software_version( ),
  			update_software_version( ),
  			get_status( ),
  			update_status( ),
  			get_surrounding_streets( ),
  			get_surrounding_street_by_id( $id = null ),
  			insert_surrounding_street( $data = null ),
  			update_surrounding_street( $data = null ),
  			remove_surrounding_street( $data  = null),
  			get_plans( ),
  			get_plan_by_id( $id = null ),
  			insert_plan( $data = null ),
  			update_plan( $data = null ),
  			remove_plan( $data  = null),
            delete_drawing($id = null )

=========================================================================================================================*/
class EDL_model extends CI_Model
{
    //Properties
    public $table_files = null;
    public $id_files = null;

    public $table_departments = null;
    public $id_departments = null;

    public $table_devices = null;
    public $id_devices = null;

    public $table_paper_class = null;
    public $id_paper_class = null;

    public $table_electronic_class = null;
    public $id_electronic_class = null;

    public $table_drawn = null;
    public $id_drawn = null;

    public $table_process = null;
    public $id_process = null;

    public $table_originator = null;
    public $id_originator = null;

    public $table_project_type = null;
    public $id_project_type = null;

    public $table_locations = null;
    public $id_locations = null;

    public $table_surveyor = null;
    public $id_surveyor = null;

    public $table_survey_method = null;
    public $id_survey_method = null;

    public $table_software_version = null;
    public $id_software_version = null;

    public $table_status = null;
    public $id_status = null;

    public $table_surrounding_streets = null;
    public $id_surrounding_streets = null;
    public $table_edl_loc = null;

    public $table_plans = null;
    public $id_plans = null;
    public $hrm_street = null;

    public $oracle_db = null; // The reference to the database

    //Constructor
    public function __construct()
    {
        parent::__construct();
        //Load Dependencies
        $this->table_files = 'EDL_MAIN';
        $this->id_files = 'FILE_ID';

        $this->table_departments = 'EDL_DEPT';
        $this->id_departments = 'DEPT_CODE';

        $this->table_devices = 'EDL_DEVICE';
        $this->id_devices = 'DEVICE_CODE';

        $this->table_paper_class = 'EDL_P_CLASSIFICATION';
        $this->id_paper_class = 'CLASS_CODE';

        $this->table_electronic_class = 'EDL_E_CLASSIFICATION';
        $this->id_electronic_class = 'CLASS_CODE';

        $this->table_drawn = 'EDL_DRAWN';
        $this->id_drawn = 'DRAWN_CODE';

        $this->table_process = 'EDL_PROCESS';
        $this->id_process = 'PROCESS_CODE';

        $this->table_originator = 'EDL_ORIGINATOR';
        $this->id_originator = 'ORIGINATOR_CODE';

        $this->table_project_type = 'EDL_PROJTYP';
        $this->id_project_type = 'PROJTYP_CODE';

        $this->table_locations = 'EDL_LOCATION';
        $this->id_locations = 'LOCATION_CODE';

        $this->table_surveyor = 'EDL_SURVEYOR';
        $this->id_surveyor = 'SURVEYOR_CODE';

        $this->table_survey_method = 'EDL_SURVMETH';
        $this->id_survey_method = 'SURVMETH_CODE';

        $this->table_software_version = 'EDL_SWVER';
        $this->id_software_version = 'SWVER_CODE';

        $this->table_status = 'EDL_STATUS';
        $this->id_status = 'STATUS_CODE';

        $this->table_surrounding_streets = 'EDL_STREET';
        $this->id_surrounding_streets = 'FILE_ID';
        $this->table_edl_loc = 'EDL_LOC';

        $this->table_plans = 'EDL_DBASE';
        $this->id_plans = 'PLAN_NUM';
        $this->hrm_street = 'HRM_STREET';

        $this->oracle_db = $this->load->database('oracle_EDL', TRUE);

        $this->oracle_db->query("alter session set nls_date_format = 'DD-MON-YYYY'");

        // impersonate the EDLADM schema
        $this->oracle_db->query("ALTER SESSION SET CURRENT_SCHEMA=EDLADM");
    }

// ------------------------------------------------------------------------
    //Util methods

    public function convert_date($date)
    {
        if (is_array($date))
        {
            $date_string = '';

            // Build the string up from the array.
            foreach ($date as $key => $value)
            {
                $date_string .= $value.'-';
            }

            // Remove the last character.
            $length = strlen($date_string);
            $clean_string = substr($date_string, 0, --$length);

            return $clean_string;
        }
        else
        {
            if (!empty($date))
            {
                $result_array = explode('-', $date);
                return $result_array;
            }
            else
            {
                return false;
            }
        }
    }

    public function get_table_fields($table_name)
    {
        switch ($table_name)
        {
            case 'departments':
                $table = $this->table_departments;
                break;

            case 'devices':
                $table = $this->table_devices;
                break;

            case 'paper_classifications':
                $table = $this->table_paper_class;
                break;

            case 'electronic_classifications':
                $table = $this->table_electronic_class;
                break;

            case 'drawn':
                $table = $this->table_drawn;
                break;

            case 'processes':
                $table = $this->table_process;
                break;

            case 'originators':
                $table = $this->table_originator;
                break;

            case 'project_types':
                $table = $this->table_project_type;
                break;

            case 'locations':
                $table = $this->table_locations;
                break;

            case 'surveyors':
                $table = $this->table_surveyor;
                break;

            case 'survey_methods':
                $table = $this->table_survey_method;
                break;

            case 'software_versions':
                $table = $this->table_software_version;
                break;

            case 'statuses':
                $table = $this->table_status;
                break;

            default:
                redirect('EDL/table_maintenance');
                break;
        }

        $fields = $this->oracle_db->list_fields($table);

        return $fields;
    }

    public function get_field_meta($table_name, $field_name)
    {
        switch ($table_name)
        {
            case 'departments':
                $table = $this->table_departments;
                break;

            case 'devices':
                $table = $this->table_devices;
                break;

            case 'paper_classifications':
                $table = $this->table_paper_class;
                break;

            case 'electronic_classifications':
                $table = $this->table_electronic_class;
                break;

            case 'drawn':
                $table = $this->table_drawn;
                break;

            case 'processes':
                $table = $this->table_process;
                break;

            case 'originators':
                $table = $this->table_originator;
                break;

            case 'project_types':
                $table = $this->table_project_type;
                break;

            case 'locations':
                $table = $this->table_locations;
                break;

            case 'surveyors':
                $table = $this->table_surveyor;
                break;

            case 'survey_methods':
                $table = $this->table_survey_method;
                break;

            case 'software_versions':
                $table = $this->table_software_version;
                break;

            case 'statuses':
                $table = $this->table_status;
                break;

            default:
                redirect('EDL/table_maintenance');
                break;
        }


        $fields = $this->oracle_db->field_data($table);

        foreach ($fields as $field)
        {
            if ($field->name == $field_name)
            {
                return $field;
            }
        }
    }

    /*
    ============================================================================
    get_files
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    boolean - false, array[string] - result set
    ============================================================================
    */

    public function get_files()
    {
        $this->oracle_db->select("FILE_ID, STREET_NUM, SIGNED, SURVEYOR, NOTES");
        $query = $this->oracle_db->get($this->table_files);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    public function search_files( $data = null,  $limit, $start)
    {
        $query = null;
        $results = null;

        $this->oracle_db->limit($limit, $start);

        $this->oracle_db->select('EDL_MAIN.FILE_ID,EDL_MAIN.STREET_NUM,EDL_MAIN.NOTES,EDL_MAIN.PROJTYP_CDE,EDL_MAIN.TO_PLAN,EDL_MAIN.FROM_PLAN,EDL_MAIN.ARCHIVE_DATE,EDL_MAIN.SIGNED,EDL_PROJTYP.PROJTYP_DSC');
        $this->oracle_db->join('EDL_PROJTYP', 'EDL_PROJTYP.PROJTYP_CODE = EDL_MAIN.PROJTYP_CDE', 'left');

        if (!empty($data['surrounding_street_1'])) {
            $this->oracle_db->join($this->table_surrounding_streets, 'EDL_STREET.FILE_ID = EDL_MAIN.FILE_ID');
            $this->oracle_db->where('EDL_STREET.STREET_NUM', $data['surrounding_street_1']);
        }

        // If the user is searching on a street include it in the search clause
        if (!empty($data['street'])) {
            $this->oracle_db->where('EDL_MAIN.STREET_NUM', $data['street']);
        }

        // now that were returning files by street name limit the results set by the other fields if the data is there.
        if (!empty($data['file_number'])) {
            $this->oracle_db->like('LOWER(EDL_MAIN.FILE_ID)', strtolower($data['file_number']));
        }
        if (!empty($data['project_type'])) {
            $this->oracle_db->where('LOWER(EDL_MAIN.PROJTYP_CDE)', strtolower($data['project_type']));
        }
        if (!empty($data['originator'])) {
            $this->oracle_db->where('LOWER(EDL_MAIN.ORIG_CDE)', strtolower($data['originator']));
        }
        if (!empty($data['surveyor'])) {
            $this->oracle_db->where('LOWER(EDL_MAIN.SURVEYOR)', strtolower($data['surveyor']));
        }
        if (!empty($data['signed_date'])) {
            $this->oracle_db->where('EDL_MAIN.SIGNED', $data['signed_date']);
        }
        if (!empty($data['description'])) {
            $this->oracle_db->like('LOWER(EDL_MAIN.NOTES)', strtolower($data['description']));
        }
        if (!empty($data['tender_number'])) {
            $this->oracle_db->like('EDL_MAIN.TENDER_NUM', $data['tender_number']);
        }
        if (!empty($data['from_plan'])) {
            $this->oracle_db->like('LOWER(EDL_MAIN.FROM_PLAN)', strtolower($data['from_plan']));
        }
        if (!empty($data['to_plan'])) {
            $this->oracle_db->like('LOWER(EDL_MAIN.TO_PLAN)', strtolower($data['to_plan']));
        }

        $this->oracle_db->order_by('EDL_MAIN.FILE_ID', 'asc');
        // return the result set and store it in $results
        $query = $this->oracle_db->get($this->table_files);

        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        } else {
            return false;
        }

        foreach ($results as $key => $result) {
            $surrounding_results = $this->get_surrounding_streets($result['FILE_ID']);

            if (!empty($surrounding_results)) {
                if (!empty($surrounding_results[0])) {
                    $surrounding_street_1 = $this->get_street_by_id($surrounding_results[0]['STREET_NUM']);

                    if ($surrounding_street_1 != null) {
                        $results[$key]['surrounding_street_1'] = $surrounding_street_1['STREET'];
                    }
                }
                if (!empty($surrounding_results[1])) {
                    $surrounding_street_2 = $this->get_street_by_id($surrounding_results[1]['STREET_NUM']);

                    if ($surrounding_street_2 != null) {
                        $results[$key]['surrounding_street_2'] = $surrounding_street_2['STREET'];
                    }
                }
            }
        }
        return $results;
    }

    public function search_files_count( $data = null)
    {
        $query = null;
        $results = null;

        $this->oracle_db->select('EDL_MAIN.FILE_ID,EDL_MAIN.STREET_NUM,EDL_MAIN.NOTES,EDL_MAIN.PROJTYP_CDE,EDL_MAIN.TO_PLAN,EDL_MAIN.FROM_PLAN,EDL_MAIN.ARCHIVE_DATE,EDL_MAIN.SIGNED,EDL_PROJTYP.PROJTYP_DSC');
        $this->oracle_db->join('EDL_PROJTYP', 'EDL_PROJTYP.PROJTYP_CODE = EDL_MAIN.PROJTYP_CDE', 'left');

        if (!empty($data['surrounding_street_1'])) {
            $this->oracle_db->join($this->table_surrounding_streets, 'EDL_STREET.FILE_ID = EDL_MAIN.FILE_ID');
            $this->oracle_db->where('EDL_STREET.STREET_NUM', $data['surrounding_street_1']);
        }

        // If the user is searching on a street include it in the search clause
        if (!empty($data['street'])) {
            $this->oracle_db->where('EDL_MAIN.STREET_NUM', $data['street']);
        }

        // now that were returning files by street name limit the results set by the other fields if the data is there.
        if (!empty($data['file_number'])) {
            $this->oracle_db->like('LOWER(EDL_MAIN.FILE_ID)', strtolower($data['file_number']));
        }
        if (!empty($data['project_type'])) {
            $this->oracle_db->where('LOWER(EDL_MAIN.PROJTYP_CDE)', strtolower($data['project_type']));
        }
        if (!empty($data['originator'])) {
            $this->oracle_db->where('LOWER(EDL_MAIN.ORIG_CDE)', strtolower($data['originator']));
        }
        if (!empty($data['surveyor'])) {
            $this->oracle_db->where('LOWER(EDL_MAIN.SURVEYOR)', strtolower($data['surveyor']));
        }
        if (!empty($data['signed_date'])) {
            $this->oracle_db->where('EDL_MAIN.SIGNED', $data['signed_date']);
        }
        if (!empty($data['description'])) {
            $this->oracle_db->like('LOWER(EDL_MAIN.NOTES)', strtolower($data['description']));
        }
        if (!empty($data['tender_number'])) {
            $this->oracle_db->like('EDL_MAIN.TENDER_NUM', $data['tender_number']);
        }
        if (!empty($data['from_plan'])) {
            $this->oracle_db->like('LOWER(EDL_MAIN.FROM_PLAN)', strtolower($data['from_plan']));
        }
        if (!empty($data['to_plan'])) {
            $this->oracle_db->like('LOWER(EDL_MAIN.TO_PLAN)', strtolower($data['to_plan']));
        }


        $this->oracle_db->order_by('EDL_MAIN.FILE_ID', 'asc');
        // return the result set and store it in $results
        $query = $this->oracle_db->get($this->table_files);

        if ($query->num_rows() > 0) {
            $results = $query->result_array();
        } else {
            return false;
        }

        return $results;
    }


    public function get_surrounding_streets_by_id( $file_number = null, $street_number = null )
    {
        $this->oracle_db->where('FILE_ID', $file_number);
        $this->oracle_db->where('STREET_NUM', $street_number);

        $query = $this->oracle_db->get('EDL_STREET');

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    public function get_surrounding_streets_by_street_number( $street_number = null )
    {
        $this->oracle_db->where('STREET_NUM', $street_number);

        $query = $this->oracle_db->get('EDL_STREET');

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    public function get_streets_by_name( $name = null )
    {
        if ($name != null)
        {
            $this->oracle_db->like('STREET', $name);
            $query = $this->oracle_db->get($this->hrm_street);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

//    public function get_street_name_by_id( $id = null )
//    {
//        if ($id != null)
//        {
//            $street = [];
//
//            try {
//                $site = GIS_STREET_BY_ID_SEARCH_URL;
//                $url = str_replace("STREET_ID", $id, $site);
//                //$url = "http://www.google.ca";
//                $proxy = PROXY_URL;
//
//                $ch = curl_init();
//                // Check if initialization had gone wrong*
//                if ($ch === false) {
//                    throw new Exception('failed to initialize');
//                }
//
//                curl_setopt($ch, CURLOPT_URL, $url);
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                curl_setopt($ch, CURLOPT_PROXY, $proxy);
//
//                $data = curl_exec($ch);
//
//                if ($data === false) {
//                    throw new Exception(curl_error($ch), curl_errno($ch));
//                }
//
//                $jsonArray = json_decode($data, true);
//                $features = $jsonArray["features"];
//                $attributes = $features[0]["attributes"];
//
//                curl_close($ch);
//
//                $street['STREET_CDE'] = $attributes['STR_CODE_L'];
//                $street['STREET'] = $attributes['STR_NAME'];
//                $street['STREET_TYP'] = $attributes['STR_TYPE'];
//                $street['COMMUNITY'] = $attributes['GSA_LEFT'];
//
//                //return $attributes['STREET'].' '.$attributes['STR_TYPE'].', '.$attributes['GSA_LEFT'];
//
//                return $street;
//            } catch(Exception $e) {
//
//                trigger_error(sprintf(
//                    'Curl failed with error #%d: %s',
//                    $e->getCode(), $e->getMessage()),
//                    E_USER_ERROR);
//
//            }

//            $this->oracle_db->where('STREET_CDE', $id);
//            //$query = $this->oracle_db->get('hrmadm.hrm_street');
//            $query = $this->oracle_db->get($this->hrm_street);
//
//            if ($query->num_rows() > 0)
//            {
//                return $query->row_array()['STREET'].' '.$query->row_array()['STREET_TYP'].', '.$query->row_array()['COMMUNITY'];
//            }
//            else
//            {
//                return false;
//            }
//        }
//        else
//        {
//            return false;
//        }
//    }

    public function get_street_id_by_name( $name = null )
    {
        if ($name != null)
        {
            $this->oracle_db->like('STREET', $name);
            $query = $this->oracle_db->get($this->hrm_street);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    /*
    ============================================================================
    get_files_by_id
    ----------------------------------------------------------------------------
    integer $id = The id that we want to retrieve from the db.
    ----------------------------------------------------------------------------
    boolean - false, array[string] - result set
    ============================================================================
    */
    public function get_files_by_id( $id = null )
    {
        // $this->oracle_db->select("FILE_ID, STREET_NUM, SIGNED, SURVEYOR, NOTES");
        $this->oracle_db->like("FILE_ID", $id);
        $this->oracle_db->limit(501);
        $query = $this->oracle_db->get($this->table_files);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    /*
    ============================================================================
    get_file_by_id
    ----------------------------------------------------------------------------
    integer $id = The id that we want to retrieve from the db.
    ----------------------------------------------------------------------------
    boolean - false, array[string] - result set
    ============================================================================
    */
    public function get_file_by_id( $id = null )
    {
        $this->oracle_db->where("FILE_ID", $id);
        $query = $this->oracle_db->get($this->table_files);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    /*
    ============================================================================
    get_files_by_street_id
    ----------------------------------------------------------------------------
    integer $id = The id that we want to retrieve from the db.
    ----------------------------------------------------------------------------
    boolean - false, array[string] - result set
    ============================================================================
    */

    public function get_files_by_street_id( $id = null )
    {
        // $this->oracle_db->select("FILE_ID, STREET_NUM, SIGNED, SURVEYOR, NOTES");
        $this->oracle_db->where("STREET_NUM", $id);
        $this->oracle_db->limit(501);
        $query = $this->oracle_db->get($this->table_files);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    /*
    ============================================================================
    insert_file
    ----------------------------------------------------------------------------
    array[string] - $data - The data that we want to insert into the db.
    ----------------------------------------------------------------------------
    boolean - false - If the insert fails return false to the controller.
    ============================================================================
    */

    public function insert_file( $data = null, $locations_array = null, $loc_data_exists = null )
    {
        if ($data == null)
        {
            return false;
        }
        else
        {

            //start the transaction
            $this->oracle_db->trans_strict(TRUE);
            $this->oracle_db->trans_begin();

            $results = $this->oracle_db->insert($this->table_files, $data);


            if ($loc_data_exists && $results)
            {
                // loop though the whole array
                for ($i=0; $i < count($locations_array); $i++)
                {
                    // The first three elements will be the surronding street arrays
                    if ($i < 3)
                    {
                        // as long as there is a street code present then we can write the surrounding street to the db.
                        if (!empty($locations_array[$i]['STREET_CODE']) && $results)
                        {
                            $surrounding_street = array();
                            $surrounding_street['FILE_ID'] = $data['FILE_ID'];
                            $surrounding_street['STREET_NUM'] = $locations_array[$i]['STREET_CODE'];


                            $results = $this->EDL_model->insert_surrounding_street($surrounding_street);
                        }
                    }
                    // any element above the third will be drawing location data.
                    else
                    {
                        if ($results) {
                            if (!empty($locations_array[$i]['PROCESS']) || !empty($locations_array[$i]['DEVICE']) || !empty($locations_array[$i]['FILE_LOC']) || !empty($locations_array[$i]['DATETIME']) || !empty($locations_array[$i]['STATUS']) || !empty($locations_array[$i]['DRAWN']) || !empty($locations_array['PHYSICAL_DESC'])) {
                                $results = $this->EDL_model->insert_locs($locations_array[$i], $data['FILE_ID']);
                            }
                        }
                    }
                }
            }

            //make transaction complete
            $this->oracle_db->trans_complete();

            //check if transaction status TRUE or FALSE
            if ($this->oracle_db->trans_status() === FALSE || (!$results)) {
                //if something went wrong, rollback everything
                $this->oracle_db->trans_rollback();
                return FALSE;
            } else {
                //if everything went right, commit the data to the database
                $this->oracle_db->trans_commit();
                return TRUE;
            }

            return $results;
        }
    }
    /*
    ============================================================================
    update_file
    ----------------------------------------------------------------------------
    array[string] - $data - The data that we want to insert into the db.
    ----------------------------------------------------------------------------
    boolean - false - If the update fails return false to the controller.
    ============================================================================
    */

    public function update_file( $id = null, $data = null )
    {
        $result = false;

        if ($data == null)
        {
            return false;
        }
        else
        {
            // if the incoming file_id is not the same
            // need to update any existing edl_loc and edl_street records
            // with the new id
            if ($id != null && ($id != $data['FILE_ID'])) {
                //start the transaction
                $this->oracle_db->trans_strict(TRUE);
                $this->oracle_db->trans_begin();

                //update any surrounding streets and locations
                $updateData = array(
                  'FILE_ID' => $data['FILE_ID']
                );

                $this->oracle_db->where($this->id_files, $id);
                $result = $this->oracle_db->update($this->table_surrounding_streets, $updateData);

                if ($result) {
                    $this->oracle_db->where($this->id_files, $id);
                    $result = $this->oracle_db->update($this->table_edl_loc, $updateData);
                }

                // finally update the edl_main table
                if ($result) {
                    $this->oracle_db->where($this->id_files, $id);
                    $result = $this->oracle_db->update($this->table_files, $data);
                }

                //make transaction complete
                $this->oracle_db->trans_complete();

                //check if transaction status TRUE or FALSE
                if ($this->oracle_db->trans_status() === FALSE || (!$result)) {
                    //if something went wrong, rollback everything
                    $this->oracle_db->trans_rollback();
                    $result = FALSE;
                } else {
                    //if everything went right, commit the data to the database
                    $this->oracle_db->trans_commit();
                    $result = TRUE;
                }
            }else{
                $this->oracle_db->where($this->id_files, $id);
                $result = $this->oracle_db->update($this->table_files, $data);
            }

            return $result;
        }
    }
    /*
    ============================================================================
    remove_file
    ----------------------------------------------------------------------------
    integer - $id - The id of the record we want to remove from the table.
    ----------------------------------------------------------------------------
    boolean - false - if the id is null return false.
    ============================================================================
    */

    public function delete_drawing( $id  = null)
    {
        if ($id == null)
        {
            return false;
        }
        else
        {
            //start the transaction
            $this->oracle_db->trans_strict(TRUE);
            $this->oracle_db->trans_begin();

            // Delete streets
            $results = $this->delete_streets($id);

            // Delete locations
            if ($results) {
                $results = $this->delete_locs($id);
            }

            if ($results) {
                $this->oracle_db->where($this->id_files, $id);
                $results = $this->oracle_db->delete($this->table_files);
            }

            //make transaction complete
            $this->oracle_db->trans_complete();

            //check if transaction status TRUE or FALSE
            if ($this->oracle_db->trans_status() === FALSE || (!$results)) {
                //if something went wrong, rollback everything
                $this->oracle_db->trans_rollback();
                return FALSE;
            } else {
                //if everything went right, commit the data to the database
                $this->oracle_db->trans_commit();
                return TRUE;
            }
        }
    }

    /*-----------------------------------------------------------------------------------
        Table Maintenance functions (theres a lot of em :) )
    ------------------------------------------------------------------------------------*/
    public function add_to_maintenance_table($data)
    {
        $table_name = $data['table_name'];
        unset($data['table_name']);

        switch ($table_name)
        {
            case 'departments':
                $table = $this->table_departments;
                break;

            case 'devices':
                $table = $this->table_devices;
                break;

            case 'paper_classifications':
                $table = $this->table_paper_class;
                break;

            case 'electronic_classifications':
                $table = $this->table_electronic_class;
                break;

            case 'drawn':
                $table = $this->table_drawn;
                break;

            case 'processes':
                $table = $this->table_process;
                break;

            case 'originators':
                $table = $this->table_originator;
                break;

            case 'project_types':
                $table = $this->table_project_type;
                break;

            case 'locations':
                $table = $this->table_locations;
                break;

            case 'surveyors':
                $table = $this->table_surveyor;
                break;

            case 'survey_methods':
                $table = $this->table_survey_method;
                break;

            case 'software_versions':
                $table = $this->table_software_version;
                break;

            case 'statuses':
                $table = $this->table_status;
                break;

            default:
                redirect('EDL/table_maintenance');
                break;
        }

        $results = $this->oracle_db->insert($table, $data);

        return $results;
    }

    public function delete_from_maintenance_table($data)
    {
        switch ($data['table_name'])
        {
            case 'departments':
                $table = $this->table_departments;
                $id = $this->id_departments;
                break;

            case 'devices':
                $table = $this->table_devices;
                $id = $this->id_devices;
                break;

            case 'paper_classifications':
                $table = $this->table_paper_class;
                $id = $this->id_paper_class;
                break;

            case 'electronic_classifications':
                $table = $this->table_electronic_class;
                $id = $this->id_electronic_class;
                break;

            case 'drawn':
                $table = $this->table_drawn;
                $id = $this->id_drawn;
                break;

            case 'processes':
                $table = $this->table_process;
                $id = $this->id_process;
                break;

            case 'originators':
                $table = $this->table_originator;
                $id = $this->id_originator;
                break;

            case 'project_types':
                $table = $this->table_project_type;
                $id = $this->id_project_type;
                break;

            case 'locations':
                $table = $this->table_locations;
                $id = $this->id_locations;
                break;

            case 'surveyors':
                $table = $this->table_surveyor;
                $id = $this->id_surveyor;
                break;

            case 'survey_methods':
                $table = $this->table_survey_method;
                $id = $this->id_survey_method;
                break;

            case 'software_versions':
                $table = $this->table_software_version;
                $id = $this->id_software_version;
                break;

            case 'statuses':
                $table = $this->table_status;
                $id = $this->id_status;
                break;

            default:
                redirect('EDL/table_maintenance');
                break;
        }

        if ($data == null)
        {
            return false;
        }
        else
        {
            $this->oracle_db->where($id, $data[$id]);
            $results = $this->oracle_db->delete($table);
            return $results;
        }
    }

    public function update_maintenance_table($data)
    {

        $table_name = $data['table_name'];
        unset($data['table_name']);

        switch ($table_name)
        {
            case 'departments':
                $table = $this->table_departments;
                $id = $this->id_departments;
                break;

            case 'devices':
                $table = $this->table_devices;
                $id = $this->id_devices;
                break;

            case 'paper_classifications':
                $table = $this->table_paper_class;
                $id = $this->id_paper_class;
                break;

            case 'electronic_classifications':
                $table = $this->table_electronic_class;
                $id = $this->id_electronic_class;
                break;

            case 'drawn':
                $table = $this->table_drawn;
                $id = $this->id_drawn;
                break;

            case 'processes':
                $table = $this->table_process;
                $id = $this->id_process;
                break;

            case 'originators':
                $table = $this->table_originator;
                $id = $this->id_originator;
                break;

            case 'project_types':
                $table = $this->table_project_type;
                $id = $this->id_project_type;
                break;

            case 'locations':
                $table = $this->table_locations;
                $id = $this->id_locations;
                break;

            case 'surveyors':
                $table = $this->table_surveyor;
                $id = $this->id_surveyor;
                break;

            case 'survey_methods':
                $table = $this->table_survey_method;
                $id = $this->id_survey_method;
                break;

            case 'software_versions':
                $table = $this->table_software_version;
                $id = $this->id_software_version;
                break;

            case 'statuses':
                $table = $this->table_status;
                $id = $this->id_status;
                break;

            default:
                redirect('EDL/table_maintenance');
                break;
        }

        $this->oracle_db->where($id, $data[$id]);
        $results = $this->oracle_db->update($table, $data);

        return $results;
    }
// ---------------------------------------------------------------------------------

    public function get_departments( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_departments, $id);
            $this->oracle_db->order_by('DEPT_CODE asc');
            $query = $this->oracle_db->get($this->table_departments);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('DEPT_CODE', 'ASC');
            $query = $this->oracle_db->get($this->table_departments);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }

    }
// ---------------------------------------------------------------------------------

    public function get_devices( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_devices, $id);
            $query = $this->oracle_db->get($this->table_devices);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(DEVICE_DSC)', 'ASC');
            $query = $this->oracle_db->get($this->table_devices);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------

    public function get_paper_classifications( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_paper_class, $id);

            $query = $this->oracle_db->get($this->table_paper_class);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('CLASS_CODE', 'ASC');
            $query = $this->oracle_db->get($this->table_paper_class);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------
    public function get_electronic_classifications( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_electronic_class, $id);

            $query = $this->oracle_db->get($this->table_electronic_class);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('CLASS_CODE', 'ASC');
            $query = $this->oracle_db->get($this->table_electronic_class);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------
    public function get_drawn( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_drawn, $id);

            $query = $this->oracle_db->get($this->table_drawn);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(DRAWN_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_drawn);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------
    public function get_process( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_process, $id);

            $query = $this->oracle_db->get($this->table_process);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(PROCESS_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_process);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------
    public function get_originator( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_originator, $id);

            $query = $this->oracle_db->get($this->table_originator);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(ORIGINATOR_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_originator);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------
    public function get_project_type( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_project_type, $id);

            $query = $this->oracle_db->get($this->table_project_type);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(PROJTYP_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_project_type);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }

// ---------------------------------------------------------------------------------
    public function get_locations( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_locations, $id);

            $query = $this->oracle_db->get($this->table_locations);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(LOCATION_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_locations);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------
    public function get_surveyor( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_surveyor, $id);

            $query = $this->oracle_db->get($this->table_surveyor);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(SURVEYOR_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_surveyor);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }

    }
// ---------------------------------------------------------------------------------
    public function get_survey_method( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_survey_method, $id);

            $query = $this->oracle_db->get($this->table_survey_method);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(SURVMETH_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_survey_method);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------
    public function get_software_version( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_software_version, $id);

            $query = $this->oracle_db->get($this->table_software_version);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(SWVER_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_software_version);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }
// ---------------------------------------------------------------------------------
    public function get_status( $id = null )
    {
        if ($id != null)
        {
            $this->oracle_db->where($this->id_status, $id);

            $query = $this->oracle_db->get($this->table_status);

            if ($query->num_rows() > 0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $this->oracle_db->order_by('lower(STATUS_DSC)', 'asc');
            $query = $this->oracle_db->get($this->table_status);

            if ($query->num_rows() > 0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
        }
    }

// ---------------------------------------------------------------------------------
    /*-----------------------------------------------------------------------------------
        Surrounding Streets retrieval function
    ------------------------------------------------------------------------------------*/
    public function get_surrounding_streets( $file_id = null )
    {
        $this->oracle_db->where('FILE_ID', $file_id);
        $query = $this->oracle_db->get($this->table_surrounding_streets);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
    public function get_surrounding_street_by_id( $id = null )
    {
        $this->oracle_db->where($this->id_surrounding_streets, $id);
        $query = $this->oracle_db->get($this->table_surrounding_streets);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    public function get_files_with_surrounding_streets( $street_name = null )
    {
        if ($street_name != null)
        {

        }
    }
    /*-----------------------------------------------------------------------------------
        Surrounding Streets insertion function
    ------------------------------------------------------------------------------------*/
    public function insert_surrounding_street( $data = null )
    {
        if ($data == null)
        {
            return false;
        }
        else
        {

            $results = $this->oracle_db->insert($this->table_surrounding_streets, $data);

            return $results;
        }
    }

    /*-----------------------------------------------------------------------------------
        Surrounding Streets update function
    ------------------------------------------------------------------------------------*/
    public function update_surrounding_street( $data = null )
    {
        if ($data == null)
        {
            return false;
        }
        else
        {
            $this->oracle_db->where($this->id_surrounding_streets, $data[$this->id_surrounding_streets]);
            $this->oracle_db->update($this->table_surrounding_streets, $data);
        }
    }

    public function update_surrounding_streeets_by_id($id = null, $data = null)
    {
        if ($data == null)
        {
            return false;
        }
        else
        {
            $this->oracle_db->where($this->id_surrounding_streets, $id);
            $this->oracle_db->where('STREET_NUM', $data['STREET_NUM']);
            $this->oracle_db->update($this->table_surrounding_streets, $data);
        }
    }

    /*-----------------------------------------------------------------------------------
        Surrounding Street removal function
    ------------------------------------------------------------------------------------*/
    public function remove_surrounding_street( $data = null )
    {
        if ($data == null)
        {
            return false;
        }
        else
        {

            $this->oracle_db->where('FILE_ID', $data['FILE_ID']);
            $this->oracle_db->where('STREET_NUM', $data['STREET_CDE']);
            $results = $this->oracle_db->delete('EDL_STREET');

            return $results;
        }
    }

    /*-----------------------------------------------------------------------------------
        Plan Index retrieval functions
    ------------------------------------------------------------------------------------*/
    public function get_plans( )
    {
        $query = $this->oracle_db->query("SELECT PLAN_NUM, STR_NUMBER, STR_NAME, SURVEY_BY, DESC1 FROM EDL_DBASE");

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
    public function get_plan_by_id( $id = null )
    {
        $this->oracle_db->where($this->id_plans, $id);
        $query = $this->oracle_db->get($this->table_plans);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    public function get_plan_like_id( $id = null )
    {
        $this->oracle_db->like($this->id_plans, $id);
        $query = $this->oracle_db->get($this->table_plans);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    /*-----------------------------------------------------------------------------------
        Plan Index insertion function
    ------------------------------------------------------------------------------------*/
    public function insert_plan( $data = null )
    {
        if ($data == null)
        {
            return false;
        }
        else
        {
            $results = $this->oracle_db->insert($this->table_plans, $data);

            return $results;
        }
    }

    /*-----------------------------------------------------------------------------------
        Plan Index update function
    ------------------------------------------------------------------------------------*/
    public function update_plan( $data = null )
    {
        if ($data == null)
        {
            return false;
        }
        else
        {
            $this->oracle_db->where($this->id_plans, $data[$this->id_plans]);
            $result = $this->oracle_db->update($this->table_plans, $data);

            return $result;
        }
    }

    /*-----------------------------------------------------------------------------------
        Plan Index removal function
    ------------------------------------------------------------------------------------*/
    public function remove_plan( $id  = null)
    {
        if ($id == null)
        {
            return false;
        }
        else
        {
            $this->oracle_db->where($this->id_plans, $id);
            $results = $this->oracle_db->delete($this->table_plans);

            return $results;
        }
    }

    public function get_subdivisions()
    {
        $query = $this->oracle_db->get('EDL_SUBDIV');

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    public function get_subjects()
    {
        $query = $this->oracle_db->get('EDL_SUBJ');

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }


    public function get_streets($name)
    {
        // check if the first character is a %
        // if so both, otherwise after
        if ($name[0] == '%'){
            $name = ltrim($name, '%');
            $this->oracle_db->like('STREET', $name, 'both');
        }else {
            $this->oracle_db->like('STREET', $name, 'after');
        }

        $this->oracle_db->order_by('STREET', 'asc');
        $this->oracle_db->order_by('STREET_TYP', 'asc');
        $this->oracle_db->order_by('COMMUNITY', 'asc');

        //$query = $this->oracle_db->get('hrmadm.hrm_street', 15);
        $query = $this->oracle_db->get($this->hrm_street, 25);

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

//    public function get_streets($name)
//    {
//        $streets = [];
//
//        try {
//            $site = GIS_STREET_LIKE_SEARCH_URL;
//            $fields = 'STR_CODE_L,STR_NAME,STR_TYPE,GSA_LEFT';
//            $url = str_replace("STREET_NAME", $name, $site);    // replace the search string
//            $url = str_replace('FIELDS', $fields, $url);    // replace the fields
//            //$proxy = PROXY_URL;
//
//            $ch = curl_init();
//            // Check if initialization had gone wrong*
//            if ($ch === false) {
//                throw new Exception('failed to initialize');
//            }
//
//            curl_setopt($ch, CURLOPT_URL, $url);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//            //curl_setopt($ch, CURLOPT_PROXY, $proxy);
//
//            $data = curl_exec($ch);
//
//            if ($data === false) {
//                throw new Exception(curl_error($ch), curl_errno($ch));
//            }
//
//            $jsonArray = json_decode($data, true);
//            $features = $jsonArray["features"];
//            //$attributes = $features[0]["attributes"];
//
//            curl_close($ch);
//
//            if (count($features) == 0) {
//                return false;
//            } else {
//
//                foreach ($features as $street) {
//
//                    //$arraySearchResults = array_search($street['attributes']['STR_CODE_L'], array_column($streets, 'STREET_CDE'));
//
//                    //if (array_search($street['attributes']['STR_CODE_L'], array_column($streets, 'STREET_CDE')) != 'false'){
//                        $streetArray['STREET_CDE'] = $street['attributes']['STR_CODE_L'];
//                        $streetArray['STREET'] = $street['attributes']['STR_NAME'];
//                        $streetArray['STREET_TYP'] = $street['attributes']['STR_TYPE'];
//                        $streetArray['COMMUNITY'] = $street['attributes']['GSA_LEFT'];
//
//                        $streets[] = $streetArray;
//                    //}
//                }
//
//                return $streets;
//            }
//        } catch (Exception $e) {
//
//            trigger_error(sprintf(
//                'Curl failed with error #%d: %s',
//                $e->getCode(), $e->getMessage()),
//                E_USER_ERROR);
//        }
//    }

//    public function get_street_by_id($id)
//    {
//        $street = [];
//
//        if ($id != null) {
//
//            try {
//                $site = GIS_STREET_BY_ID_SEARCH_URL;
//                $fields = 'STR_CODE_L,STR_NAME,STR_TYPE,GSA_LEFT';
//                $url = str_replace("STREET_ID", $id, $site);
//                $url = str_replace('FIELDS', $fields, $url);    // replace the fields
//                //$proxy = PROXY_URL;
//
//                $ch = curl_init();
//                // Check if initialization had gone wrong*
//                if ($ch === false) {
//                    throw new Exception('failed to initialize');
//                }
//
//                curl_setopt($ch, CURLOPT_URL, $url);
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//                //curl_setopt($ch, CURLOPT_PROXY, $proxy);
//
//                $data = curl_exec($ch);
//
//                if ($data === false) {
//                    throw new Exception(curl_error($ch), curl_errno($ch));
//                }
//
//                $jsonArray = json_decode($data, true);
//                $features = $jsonArray["features"];
//                $attributes = $features[0]["attributes"];
//
//                curl_close($ch);
//
//                $street['STREET_CDE'] = $attributes['STR_CODE_L'];
//                $street['STREET'] = $attributes['STR_NAME'];
//                $street['STREET_TYP'] = $attributes['STR_TYPE'];
//                $street['COMMUNITY'] = $attributes['GSA_LEFT'];
//
//                return $street;
//            } catch (Exception $e) {
//
//                trigger_error(sprintf(
//                    'Curl failed with error #%d: %s',
//                    $e->getCode(), $e->getMessage()),
//                    E_USER_ERROR);
//
//            }
//        }else{
//            return false;
//        }
//    }


    public function get_street_by_id($id)
    {
        $this->oracle_db->where('STREET_CDE', $id);

        //$query = $this->oracle_db->get('hrmadm.hrm_street');
        $query = $this->oracle_db->get($this->hrm_street);

        if ($query->num_rows() > 0)
        {
            return $query->row_array();
        }
        else
        {
            return false;
        }
    }

    public function delete_streets($id)
    {
        $this->oracle_db->where('FILE_ID', $id);
        $results = $this->oracle_db->delete('EDL_STREET');

        return $results;
    }

    public function get_location_by_id($id)
    {
        $this->oracle_db->where('LOCATION_CODE', $id);
        $results = $this->oracle_db->get('EDL_LOCATION');

        if ($results->num_rows() > 0)
        {
            return $results->result_array();
        }
        else
        {
            return false;
        }
    }

    public function get_originator_by_id($id)
    {
        $this->oracle_db->where('ORIGINATOR_CODE', $id);
        $results = $this->oracle_db->get('EDL_ORIGINATOR');

        if ($results->num_rows() > 0)
        {
            return $results->result_array();
        }
        else
        {
            return false;
        }
    }

    public function get_project_by_id($id)
    {
        $this->oracle_db->where('PROJTYP_CODE', $id);
        $results = $this->oracle_db->get('EDL_PROJTYP');

        if ($results->num_rows() > 0)
        {
            return $results->result_array();
        }
        else
        {
            return false;
        }
    }

    public function get_locs($id)
    {
        $this->oracle_db->where('FILE_ID', $id);
        $results = $this->oracle_db->get('EDL_LOC');

        if ($results->num_rows() > 0)
        {
            return $results->result_array();
        }
        else
        {
            return false;
        }
    }

    public function insert_locs($data, $id)
    {
        if (empty($data))
        {
            return false;
        }
        else
        {
            $data['FILE_ID'] = $id;
            $results = $this->oracle_db->insert('EDL_LOC', $data);

            return $results;
        }
    }


    public function update_locs($data, $id)
    {
        if (empty($data))
        {
            return false;
        }
        else
        {

            foreach ($data as $rec)
            {
                $this->oracle_db->where('FILE_ID', $id);
                $this->oracle_db->where('PROCESS', $rec['PROCESS']);
                unset($rec['PROCESS']);
                $results = $this->oracle_db->update('EDL_LOC', $rec);

            }
            return $results;
        }
    }

    public function delete_locs($id)
    {
        $this->oracle_db->where('FILE_ID', $id);
        $results = $this->oracle_db->delete('EDL_LOC');
        return $results;
    }

    public function delete_file_location($data = null)
    {
        $this->oracle_db->where('FILE_ID', $data['FILE_ID']);

//        if ($data['FILE_LOC'] != null) {
//            $this->oracle_db->where('FILE_LOC', $data['FILE_LOC']);
//        }
        $this->oracle_db->where('ID', $data['ID']);
//        $this->oracle_db->where('STATUS', $data['STATUS']);
//        $this->oracle_db->where('DRAWN', $data['DRAWN']);

        $results = $this->oracle_db->delete('EDL_LOC');
        return $results;
    }

    public function update_file_location($data = null)
    {
        $this->oracle_db->where('FILE_ID', $data['FILE_ID']);
        $this->oracle_db->where('ID', $data['ID']);
        unset($data['ID']);

        $results = $this->oracle_db->update($this->table_edl_loc, $data);

        return $results;
    }
}

/* End of file EDL_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/EDL_model.php */