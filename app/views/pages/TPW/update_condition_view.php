<div class="row">
	<div class="col-md-12">
		<h2>Update License Condition</h2>
		<p class="lead">This form will allow you to update a licence condition.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?php echo base_url(); ?>TPW/update_condition/<?= $condition['CONDITION_CDE'] ?>" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-sm-2">
					<label for="CONDITION_CDE">Condition Code</label><span class="text-danger">*</span>
					<input type="text" name="CONDITION_CDE" class="form-control" title="Condition Code" maxlength="3" value="<?= $condition['CONDITION_CDE'] ?>" readonly />
				</div>

				<div class="col-sm-6">
					<label for="CONDITION_DSC">Course Name</label><span class="text-danger">*</span>
					<input type="text" name="CONDITION_DSC" class="form-control" title="Course Name" maxlength="70" value="<?= $condition['CONDITION_DSC'] ?>" />
				</div>
			
				<div class="col-sm-12"> 
					<input type="submit" class="btn btn-md btn-primary update_course pull-right no_label_fix" value="Update Condition" />
					<a href="<?php echo base_url(); ?>TPW/licence_conditions" class="btn btn-md btn-default return">Back to Conditions</a>
				</div>
			</div>
		</div>
	</form>
</div>