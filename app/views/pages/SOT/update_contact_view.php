<div class="row">
	<div class="col-md-6">
		<h2>Update Contact Information</h2>
		<p>Select a contact to view more information about them.</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>

<form action="<?php echo base_url(); ?>SOT/update_contact" method="POST" >
<legend>Contact Info</legend>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="CONTACT_NUM">Contact #</label>
					<input readonly type="text" name="CONTACT_NUM" class="form-control" title="Contact ID" value="<?= $contact[0]['CONTACT_NUM'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="BIRTH_DTE">Birth Date</label>
					<input type="text" name="BIRTH_DTE" class="form-control date_picker" title="Birth Date" value="<?= $contact[0]['BIRTH_DTE'] ?>" readonly />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="TITLE_DSC">Title</label>
					<input type="text" name="TITLE_DSC" class="form-control" title="Title" value="<?= $contact[0]['TITLE_DSC'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="FIRST_NME">First Name</label>
					<input type="text" name="FIRST_NME" class="form-control" title="First Name" value="<?= $contact[0]['FIRST_NME'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="MIDDLE_NME">Middle Name</label>
					<input type="text" name="MIDDLE_NME" class="form-control" title="Middle Name" value="<?= $contact[0]['MIDDLE_NME'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="LAST_NME">Last Name</label>
					<input type="text" name="LAST_NME" class="form-control" title="Last Name" value="<?= $contact[0]['LAST_NME'] ?>" />
				</div>
			</div>
		</div>

		<legend>Addresses</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<label for="CIVIC_NUM">Civic #</label>
					<input type="text" name="C-CIVIC_NUM" class="form-control" title="Civic Number" value="<?= $civic[0]['CIVIC_NUM'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="CIVIC_ADDR">Civic Address</label>
					<input  type="text" name="C-CIVIC_ADDR" class="form-control" title="Civic Address" value="<?= $address[0]['CIVIC_ADDR'] ?>" />
				</div>
				<div class="col-sm-1">
					<label for="APT_NUM">Apt #</label>
					<input type="text" name="C-APT_NUM" class="form-control" title="Apartment Number" value="<?= $civic[0]['APT_NUM'] ?>" />
				</div>
				<div class="col-sm-1">
					<label for="DISTRICT_NUM">District</label>
					<input type="text" name="C-DISTRICT_NUM" class="form-control" title="District" value="<?= $civic[0]['DISTRICT_NUM'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="ADDRESS_TYP">Type (H - Home, W - Work, O - Other)</label>
					<input type="text" name="C-ADDRESS_TYP" class="form-control" title="Residence Type" value="<?= $civic[0]['ADDRESS_TYP'] ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="MAILADDR1_DSC">Mailing Address</label>
					<input type="text" name="C-MAILADDR1_DSC" class="form-control" title="Mailing Address" value="<?= $civic[0]['MAILADDR1_DSC'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="COUNTRY_DSC">Country</label>
					<input type="text" name="C-COUNTRY_DSC" class="form-control" title="Country" value="<?= $civic[0]['COUNTRY_DSC'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="POSTAL_CDE">Postal Code</label>
					<input type="text" name="C-POSTAL_CDE" class="form-control" title="Postal Code" value="<?= $civic[0]['POSTAL_CDE'] ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<label for="PROV_DSC">Prov.</label>
					<input type="text" name="C-PROV_DSC" class="form-control" title="Province" value="<?= $civic[0]['PROV_DSC'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="FROM_DTE">From</label>
					<input type="text" name="C-FROM_DTE" class="form-control date_picker" title="From" value="<?= $civic[0]['FROM_DTE'] ?>" readonly />
				</div>
				<div class="col-sm-3">
					<label for="TO_DTE">To</label>
					<input type="text" name="C-TO_DTE" class="form-control date_picker" title="To" value="<?= $civic[0]['TO_DTE'] ?>" readonly />
				</div>
				<div class="col-sm-5">
					<label for="ADDRESS_NOTES">Address Notes</label>
					<textarea name="C-ADDRESS_NOTES" class="form-control" title="Address Notes" maxlength="1000"><?= $civic[0]['ADDRESS_NOTES'] ?></textarea>
				</div>
			</div>
		</div>

		<legend>Phone Numbers</legend>
		<?php if (!empty($phone_numbers)): ?>
			<?php $count = 0; ?>
			<?php foreach ($phone_numbers as $number): ?>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-1">
							<input type="hidden" name="<?= $count ?>-PHONEREC_NUM" value="<?= $number['PHONEREC_NUM']?>" />
							<label for="AREA_CDE">Area</label>
							<input type="text" name="<?= $count ?>-AREA_CDE" class="form-control" title="Area Code" value="<?= $number['AREA_CDE'] ?>" maxlength="3" />
						</div>
						<div class="col-sm-2">
							<label for="PHONE_NUM">Number</label>
							<input type="text" name="<?= $count ?>-PHONE_NUM" class="form-control" title="Number" value="<?= $number['PHONE_NUM'] ?>" maxlength="7" />
						</div>
						<div class="col-sm-1">
							<label for="EXT_CDE">Ext</label>
							<input type="text" name="<?= $count ?>-EXT_CDE" class="form-control" title="Extention" value="<?= $number['EXT_CDE'] ?>" />
						</div>
						<div class="col-sm-1">
							<label for="PHONE_FLG">Type</label>
							<input type="text" name="<?= $count ?>-PHONE_FLG" class="form-control" title="Type" value="<?= $number['PHONE_FLG'] ?>" />
						</div>
						<div class="col-sm-1">
							<label for="PRIMARY_FLG">Primary?</label>
							<?php if ($number['PRIMARY_FLG'] == 'Y'): ?>
								<input type="checkbox" name="<?= $count ?>-PRIMARY_FLG" title="Primary?" checked />
							<?php else : ?>
								<input type="checkbox" name="<?= $count ?>-PRIMARY_FLG" title="Primary?" />
							<?php endif ?>
						</div>
						<div class="col-sm-3">
							<label for="FROM_DTE">From</label>
							<input type="text" name="<?= $count ?>-FROM_DTE" class="form-control date_picker" title="From" value="<?= $number['FROM_DTE'] ?>" readonly />
						</div>
						<div class="col-sm-3">
							<label for="TO_DTE">To</label>
							<input type="text" name="<?= $count ?>-TO_DTE" class="form-control date_picker" title="To" value="<?= $number['TO_DTE'] ?>" readonly />
						</div>
					</div>
				</div>
				<?php ++$count; ?>
			<?php endforeach ?>
		<?php endif ?>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<a class="no_label_fix pull-right" href="<?php echo base_url(); ?>SOT/add_contact_phone/<?= $contact[0]['CONTACT_NUM'] ?>">Add Phone Number</a>
				</div>
			</div>
		</div>
		<legend>Contact Notes</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<textarea name="NOTES_DSC" class="form-control" title="Contact Notes" maxlength="1000"><?= $contact[0]['NOTES_DSC'] ?></textarea>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-12 no_label_fix">
					<input type="submit" class="btn btn-md btn-primary pull-right" value="Update Contact" />
					<a href="<?php echo base_url(); ?>SOT/contact_information" class="btn btn-md btn-default">Back to Contacts</a>
				</div>
			</div>
		</div>
	</div>
</div>
</form>