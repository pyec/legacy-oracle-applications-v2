<div class="row">
	<div class="col-6">
		<h2>Add Staff</h2>
		<p class="lead">Use this form to add a staff member to the system.</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="col-sm-4">
			<label for="first_name">First Name</label>
			<input type="text" name="first_name" class="form-control" title="First Name" />
		</div>
		<div class="col-sm-4">
			<label for="last_name">Last Name</label>
			<input type="text" name="last_name" class="form-control" title="Last Name" />
		</div>
		<div class="col-sm-4">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" title="Title" />
		</div>
	</div>

	<legend></legend>

	<div class="col-md-12">
		<div class="col-sm-4">
			<label for="email_address">Email Address</label>
			<input type="text" name="email_address" class="form-control" title="Email Address" />
		</div>
		<div class="col-sm-2">
			<label for="employee_number">Employee #</label>
			<input type="text" name="employee_number" class="form-control" title="Employee Number" />
		</div>
		<div class="col-sm-4 col-sm-offset-2">
			<label for="section">Section</label>
			<input type="text" name="section" class="form-control" title="Section" />
		</div>
	</div>

	<legend></legend>

	<div class="col-md-12">
		<div class="col-sm-6">
			<label for="location">Location</label>
			<input type="text" name="location" class="form-control" title="Location" />
		</div>
		<div class="col-sm-6">
			<label for="BU_select">Business Unit</label>
			<select name="BU_select" class="form-control" title="Business Unit">
				<option vlaue="">BU's here</option>
			</select>
		</div>
	</div>

	<legend></legend>

	<div class="col-md-12">
		<div class="col-sm-6">
			<div class="col-sm-2">
				<label for="civic_address_number">Civic #</label>
				<input type="text" name="civic_address_number" class="form-control civic_address_number" title="Civic Number" />
			</div>
			<div class="col-sm-10">
				<label for="civic_address">Civic Address</label>
				<input type="text" name="civic_address" class="form-control" title="Civic Address" />
			</div>
		</div>
		<div class="col-sm-3">
			<label for="floor_suite">Floor/Suite</label>
			<input type="text" name="floor_suite" class="form-control" title="Floor/Suite" />
		</div>
	</div>

	<legend></legend>

	<div class="col-md-12">
		<div class="col-sm-6">
			<label for="work_phone">Work Phone</label>
			<input type="text" name="work_phone" class="form-control" title="Work Phone" />
		</div>
		<div class="col-sm-6">
			<label for="cell_phone">Cell Phone</label>
			<input type="text" name="cell_phone" class="form-control" title="Cell Phone" />
		</div>
	</div>

	<legend></legend>

	<div class="col-md-12">
		<div class="col-sm-6">
			<label for="fax">Fax</label>
			<input type="text" name="fax" class="form-control" title="Fax" />
		</div>
		<div class="col-sm-6">
			<label for="pager">Pager</label>
			<input type="text" name="pager" class="form-control" title="Pager" />
		</div>
	</div>

	<legend></legend>

	<div class="col-md-12">
		<div class="col-sm-4">
			<input type="submit" class="btn btn-lg btn-success add_staff" />
			<a href="<?php echo base_url(); ?>phonebook" class="btn btn-lg btn-primary return_phonebook">Back to phonebook</a>
		</div>
	</div>
</div>