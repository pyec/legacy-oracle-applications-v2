<div class="row">
	<div class="col-md-6">
		<h2>Summary Offence Bylaws</h2>
		<p class="lead">Search bylaws by bylaw code, section code or description.</p>
		<p>Select a bylaw by clicking on it's bylaw code to edit it's information.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>
<!-- <div class="row"> -->
	<form action="<?php echo base_url(); ?>SOT/bylaw_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="section_code">Section Code</label>
					<input type="text" name="section_code" class="form-control" title="Section Code" value="<?php echo set_value('section_code') ?>" tabindex="1" placeholder="Section Code" />
				</div>
				<div class="col-sm-2">
					<label for="bylaw_code">Bylaw Code</label>
					<input type="text" name="bylaw_code" class="form-control" title="Bylaw Code" value="<?php echo set_value('bylaw_code') ?>" tabindex="1" placeholder="Bylaw Code" />
				</div>
				<div class="col-sm-4">
					<label for="description">Description</label>
					<input type="text" name="description" class="form-control" title="Description" value="<?php echo set_value('description') ?>" tabindex="1" placeholder="Description" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="sot_bylaw_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
	
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($bylaws)): ?>
			<h2 class="count"><?php echo count($bylaws) ?> bylaws</h2>
		<?php endif ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<a href="<?php echo base_url(); ?>SOT/add_bylaw" class="btn btn-success">Add Bylaw</a>
		</div>
	</div>
</div>

<!-- Echo out the list of bylaws -->

<?php if (!empty($bylaws)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Type</th>
				<th>Section Code</th>
				<th>Bylaw Code</th>
				<th>Bylaw Description</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
					<?php foreach ($bylaws as $bylaw): ?>
						<tr>				
							<td><?php echo $bylaw['TYPE_CDE'] ?></td>
							<form action="<?php echo base_url(); ?>SOT/update_bylaw" method="post">
							<td><button type="submit" class="bylaw_update_btn"><?php echo $bylaw['SECTION_CDE'] ?></button></td>
									<input type="hidden" name="H-SECTION_CDE" value="<?= $bylaw['SECTION_CDE'] ?>" />
									<input type="hidden" name="H-BYLAW_CDE" value="<?= $bylaw['BYLAW_CDE'] ?>" />
							</form>
								<td><?php echo $bylaw['BYLAW_CDE'] ?></td>
							<td><?php echo ucfirst(strtolower($bylaw['BYLAW_DSC'])) ?></td>
							<td>
							<form action="<?php echo base_url(); ?>SOT/remove_bylaw" method="post">
								<button type="submit" class="delete_bylaw btn btn-sm btn-danger" ><i class="fa fa-minus-square"></i></button>
								<input type="hidden" name="SECTION_CDE" value="<?= $bylaw['SECTION_CDE'] ?>" />
								<input type="hidden" name="BYLAW_CDE" value="<?= $bylaw['BYLAW_CDE'] ?>" />
							</form>
							</td>
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>