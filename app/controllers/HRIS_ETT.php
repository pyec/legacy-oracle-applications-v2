<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : HRIS Employee Training Tracking System

Description : This system allows users to add new courses to the HRIS training 
			schedule and then tie emoloyees to the courses.

=========================================================================================================================*/
class HRIS_ETT extends CI_Controller 
{

	public function index()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		$data['content'] = "pages/HRIS_ETT/home_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function search()
	{
		// Load model
		$this->load->model('HRIS_ETT_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation Rules
		$this->form_validation->set_rules('course_number', 'Course Number', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('course_title', 'Course Title', 'trim|max_length[50]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['course_number']) && empty($search_array['course_title']))
			{
				// Search on course number.
				$results = $this->HRIS_ETT_model->search_course_by_id($search_array['course_number']);
			}
			else if(!empty($search_array['course_title']) && empty($search_array['course_number']))
			{
				// Search on course title.
				$results = $this->HRIS_ETT_model->get_course_by_name(strtoupper($search_array['course_title']));
			}
			elseif (!empty($search_array['course_title']) && !empty($search_array['course_number'])) 
			{
				// Search on both.
				$results = $this->HRIS_ETT_model->search_course_by_id($search_array['course_number']);
			}
			else if(empty($search_array['course_title']) && empty($search_array['course_number']))
			{
				// empty seach params return initial query.
				redirect('HRIS_ETT');
			}
		}
		else
		{
			// Validation failed, return errors.	
		}

		$data['courses'] = $results;
		$data['content'] = "pages/HRIS_ETT/home_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_course
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_course()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		// Get post
		$post = $this->input->post(null, true);

		// If post is not empty
		if (!empty($post))
		{
			// Get course number from POST variables
			$course_number = $this->input->post('COURSE_NO');
			
			$this->form_validation->set_rules('COURSE_NO', 'Course Number', 'required');
			$this->form_validation->set_rules('COURSE_CLASS', 'Category', 'required');

			// If validation passes
			if ($this->form_validation->run() == true)
			{
				// test to see if the course number they entered already exist.
				if (!$this->HRIS_ETT_model->does_course_id_exist($course_number))
				{
					foreach ($post as $key => $value) 
					{
						if (is_string($value))
						{
							$post[$key] = strtoupper($value);
						}
					}
					$post['COURSE_TITLE'] = strtoupper($post['COURSE_TITLE']);
					// Insert the record
					if ($this->HRIS_ETT_model->insert_course($post))
					{
						$this->session->set_flashdata('message', 'The course was added successfully.');
						redirect('HRIS_ETT/');
					}
					else
					{
						// An error such as db connection drop or something else unforseen.
						$this->session->set_flashdata('error', 'An error has occured, please try again.');
						redirect('HRIS_ETT/add_course');
					}
				}
				else
				{
					//Redirect back to add course with error message
					$this->session->set_flashdata('error', 'This course number already exists.');
					redirect('HRIS_ETT/add_course');
				}	
			}
			else
			{
				// Redirect back to add course with validation errors.
				$this->session->set_flashdata('error', validation_errors());
				redirect('HRIS_ETT/add_course');
			}
		}
		else
		{
			// error: no data in post.
			$data['content'] = "pages/HRIS_ETT/add_course_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
delete_course
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_course()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		// Grab the id number for the course to delete.
		$number = $this->input->post('COURSE_NO');

        // Attempt to delete the course.
		if ($this->HRIS_ETT_model->delete_course($number))
		{
			$this->session->set_flashdata('message', 'Course: '.$number.' has been deleted.');
			redirect('HRIS_ETT/');
		}
		else
		{
			$this->session->set_flashdata('message', 'Course: '.$number.' has been deleted.');
			redirect('HIRS_ETT/');
		}
	}

/*           
============================================================================
update_course
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_course()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		$post_data = $this->input->post(null, true);
		$id_to_update = $this->uri->segment(3); 

		if (empty($post))
		{
			// Get a list of course dates back from the course_date table, if theres more than one date,
			// let the user choose before
			$data['course_dates'] = $this->HRIS_ETT_model->get_course_dates($id_to_update);
			$data['course_info'] = $this->HRIS_ETT_model->get_course_by_id($id_to_update);
			
			$data['course_no'] = $id_to_update;

			$data['content'] = "pages/HRIS_ETT/course_dates_view";
			$this->load->view('master', $data);
		}
	}

	public function add_course_date()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		// Get the post variables
		$post = $this->input->post(null, true);

		// Get the course number from the URL
		$course_number = $this->uri->segment(3);

		if (empty($post))
		{
			// If the post is empty then load the new course view.
			$data['course_info'] = $this->HRIS_ETT_model->get_course_by_id($course_number);
			$data['content'] = "pages/HRIS_ETT/add_course_date_view";
			$this->load->view('master', $data);
		}
		else
		{
			// Else if it's set then the user has submitted a new course from and we need to process it and add it to the db.
			$post['ADDBY'] = $this->session->userdata('samaccountname');
            $post['ADDDTE'] = date('Y-m-d');

			// Set the validation rules.
			$this->form_validation->set_rules('COURSE_CLASS', 'Category', 'required');
			$this->form_validation->set_rules('COURSE_NO', 'Course Number', 'required');
			$this->form_validation->set_rules('COURSE_SCHED', 'Schedule', 'required');

			// Validate the form.
			if ($this->form_validation->run() == true)
			{
				foreach ($post as $key => $value) 
				{
					if (is_string($value))
					{
						$post[$key] = strtoupper($value);
 					}
				}
				if ($this->HRIS_ETT_model->insert_course_date($post))
				{
					$this->session->set_flashdata('message', 'The course date was added, selected it from the table to add employees to it.');
					redirect('HRIS_ETT/update_course/'.$course_number);
				}
				else
				{
					$this->session->set_flashdata('error', 'The course date could not be added, please try again.');
					redirect('HRIS_ETT/add_course_date/'.$course_number);
				}

			}
			else
			{
				$data['course_info'] = $this->HRIS_ETT_model->get_course_by_id($course_number);
				$data['content'] = "pages/HRIS_ETT/add_course_date_view";
				$this->load->view('master', $data);
			}
		}
	}

	public function update_course_date()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		// Get course and schedule information out of the URL
		$course_number = $this->uri->segment(3);
		$course_sched = $this->uri->segment(4);

		$post = $this->input->post(null, true);

		// if the POST data is empty that means the user has requested a record to up date so get the record info and load the view
		if (empty($post))
		{
			$data['course'] = $this->HRIS_ETT_model->get_course_by_id($course_number);
			$data['schedule'] = $this->HRIS_ETT_model->get_course_schedule($course_number, $course_sched);
			$data['employees'] = $this->HRIS_ETT_model->get_course_employees($course_number, $course_sched);

			$data['content'] = "pages/HRIS_ETT/update_course_view";
			$this->load->view('master', $data);
		}
		else
		{
			$post['MODDTE'] = date('Y-m-d');
			

			if ($this->HRIS_ETT_model->update_course_date($post))
			{
				$this->session->set_flashdata('message', 'The course schedule was updated successfully.');
				redirect('HRIS_ETT/update_course_date/'.$course_number.'/'.$course_sched);
			}
			else
			{
				$this->session->set_flashdata('error', 'The course schedule could not be updated, please try again.');
				redirect('HRIS_ETT/add_course_date/'.$course_number.'/'.$course_sched);
			}
		}
	}

	public function delete_course_date()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		// Get course information from the POST to identify which record to delete, it's safer this way,
		$number = $this->input->post('COURSE_NO');
		$schedule = $this->input->post('COURSE_SCHED');

		// Because each course date has a list of employees that are attending, if we need to delete a course date 
		// then we need to delete all of the employees attached.

		if ($this->HRIS_ETT_model->delete_course_date($number, $schedule))
		{
			// Redirect with a success message.
			$this->session->set_flashdata('message', 'Schedule: '.$schedule.' has been deleted.');
			redirect('HRIS_ETT/update_course/'.$number);
		}
		else
		{
			$this->session->set_flashdata('message', 'Schedule: '.$schedule.' has been deleted.');
			redirect('HIRS_ETT/'.$number);
		}
	}


	public function add_course_employee()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		// get basic coruse info from the URL
		$course_number = $this->uri->segment(3);
		$course_schedule = $this->uri->segment(4);
		$emp_no = $this->uri->segment(5);

		// Get the employees record so we can prep the data for insert.
		$employee = $this->HRIS_ETT_model->get_employees_by_id($emp_no);
		// Get the course record for the additional info that isn't in the URL.
		$course = $this->HRIS_ETT_model->get_course_by_id($course_number);
		// Get the course schedule info.
		$schedule = $this->HRIS_ETT_model->get_course_schedule($course_number, $course_schedule);

		//retrieve the employee cost center and division from the emp_trn_master table
		$employeeInfo = $this->HRIS_ETT_model->get_employees_by_id($emp_no);

		// Set up the initial record for the employee using the information gather above.
		$data['EMP_NO'] = $employee['EMP_NO'];
		$data['COURSE_CLASS'] = $course[0]['COURSE_CLASS'];
		$data['COURSE_NO'] = $course[0]['COURSE_NO'];
		$data['COURSE_SCHED'] = $schedule[0]['COURSE_SCHED'];
		$data['SURNAME'] = $employee['SURNAME'];
		$data['GIVEN_NAME1'] = $employee['GIVEN_NAME1'];
        $data['DIVISION_NAME'] = $employeeInfo['DIVISION_NAME'];
        $data['CC_CODE'] = $employeeInfo['CC_CODE'];
        $data['CC_NAME'] = $employeeInfo['CC_NAME'];
		$data['COURSE_STATUS'] = 'O';
		$data['SKILL_UPDATED'] = 'N';
		$data['DATE_COMPLETED'] = '';
		$data['COURSE_START'] = $schedule[0]['COURSE_START'];
		$data['COURSE_END'] = $schedule[0]['COURSE_END'];
		$data['COST_COURSE'] = $schedule[0]['COST_COURSE'];
		$data['COST_EXPENSES'] = 0;
		$data['COURSE_RESULT'] = '';
		$data['COURSE_COMMENT'] = '';
		$data['COURSE_MARK'] = '';
		$data['DURATION_DAYS'] = $schedule[0]['DURATION_DAYS'];
		$data['DURATION_HRS'] = $schedule[0]['DURATION_HRS'];
		$data['ADDBY'] = $this->session->userdata('rdn');
        $data['ADDDTE'] = date('Y-m-d');
		$data['MODDTE'] = '';

		// Run the insert.
		if ($this->HRIS_ETT_model->insert_course_employee($data))
		{
			$this->session->set_flashdata('message', 'Employee : '.$data['GIVEN_NAME1'].' '.$data['SURNAME'].' has been added to the course schedule.');
		}
		else
		{
			$this->session->set_flashdata('error', 'Employee : '.$data['GIVEN_NAME1'].' '.$data['SURNAME'].' could not be added due to an internal error, please try again.');
		}

		// Redirect back to the courses schedule.
		redirect('HRIS_ETT/update_course_date/'.$course_number.'/'.$course_schedule);
	}

	public function update_course_employee()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		// Get POST info.
		$post = $this->input->post(null, true);
		// Get the course info from the URL.
		$course_number = $this->uri->segment(3);
		$schedule = $this->uri->segment(4);

        $post['MODDTE'] = date('Y-m-d');

        if ($post['COURSE_STATUS'] == 'C') {
            if ($post['DATE_COMPLETED'] == null) {
                $post['DATE_COMPLETED'] = date('Y-m-d');
            }else {
                $dtDateCompleted = strtotime($post['DATE_COMPLETED']);
                $post['DATE_COMPLETED'] = date('Y-m-d', $dtDateCompleted);
            }
        }
        else{
            $post['DATE_COMPLETED'] = null;
        }

		// Run the update and redirect with an appropriate message.
		if ($this->HRIS_ETT_model->update_course_employee($course_number, $schedule, $post['EMP_NO'], $post))
		{
			$this->session->set_flashdata('message', 'Employee: '.$post['EMP_NO'].' has been updated successfully.');
			redirect('HRIS_ETT/update_course_date/'.$course_number.'/'.$schedule);
		}
		else
		{
			$this->session->set_flashdata('error', 'The employee could not be updated, please try again.');
			redirect('HRIS_ETT/update_course_date/'.$course_number.'/'.$schedule);
		}
	}

	public function remove_course_employee()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		// Get course info from POST.
		$course_number = $this->input->post('COURSE_NO');
		$schedule = $this->input->post('COURSE_SCHED');
		$emp_no = $this->input->post('EMP_NO');

		// Attempted the delete.
		//$results = $this->HRIS_ETT_model->delete_course_employee($course_number, $schedule, $emp_no);

        //do a soft delete instead of a hard
        $data = array("DELETED"=>"1");
        $results = $this->HRIS_ETT_model->update_course_employee($course_number, $schedule, $emp_no, $data);

		// based on the results redirect with and success or error message.
		if ($results)
		{
			$this->session->set_flashdata('message', 'Employee: '.$emp_no.' has been removed from the course schedule.');
			redirect('HRIS_ETT/update_course_date/'.$course_number.'/'.$schedule);
		}
		else
		{
			$this->session->set_flashdata('error', 'The employee could not be removed, please try again.');
			redirect('HRIS_ETT/update_course_date/'.$course_number.'/'.$schedule);
		}
	}

	public function update_to_complete()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');


		// Get course and schedule information out of the URL
		$course_number = $this->uri->segment(3);
		$course_sched = $this->uri->segment(4);

		$employees = $this->HRIS_ETT_model->get_course_employees($course_number, $course_sched);

		foreach ($employees as $employee) 
		{
			$employee['COURSE_STATUS'] = 'C';
            $employee['DATE_COMPLETED'] = date('Y-m-d');
            $employee['MODDTE'] = date('Y-m-d');
			$results = $this->HRIS_ETT_model->update_course_employee($course_number, $course_sched, $employee['EMP_NO'], $employee);

		}


		$this->session->set_flashdata('message', 'All employees have been updated.');
		redirect('HRIS_ETT/update_course_date/'.$course_number.'/'.$course_sched);
	}

// ------------------------------------------------------------------------
	// Ajax functions 

	// These AJAX function are pretty straight forward, load the model, get the string to search on from POST,
	// query the database for the records and then echo them in json format back to the browser.
// ------------------------------------------------------------------------

	public function ajax_get_employees_by_id()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		$id = $this->input->post('id');

		$results = $this->HRIS_ETT_model->get_like_employees_by_id($id);

		echo json_encode($results);
	}

	public function ajax_get_employees_by_first_name()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

        $lastName = $this->input->post('lastName');
        $firstName = $this->input->post('firstName');

		$results = $this->HRIS_ETT_model->get_like_employees_by_first_name($lastName, $firstName);

		echo json_encode($results);
	}

	public function ajax_get_employees_by_last_name()
	{
		//Load Model
		$this->load->model('HRIS_ETT_model');

		$lastName = $this->input->post('lastName');
        $firstName = $this->input->post('firstName');

		$results = $this->HRIS_ETT_model->get_like_employees_by_last_name($lastName, $firstName);

		echo json_encode($results);
	}

}




/* End of file HRIS_ETT.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/HRIS_ETT.php */