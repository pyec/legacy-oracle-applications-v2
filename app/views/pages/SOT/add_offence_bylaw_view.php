<div class="row">
	<div class="col-md-12">
		<h2>Add summary offence Bylaw</h2>
		<p class="lead">This form will allow you to add a new bylaw to the system.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>
<div class="row">
	<form action="<?php echo base_url(); ?>SOT/add_bylaw" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="TYPE_CDE">Type</label>
						<input type="text" name="TYPE_CDE" class="form-control" title="Type" maxlength="4" />
					</div>
					<div class="col-sm-4">
						<label for="BYLAW_CDE">Bylaw Code</label><span class="text-danger">*</span>
						<input type="text" name="BYLAW_CDE" class="form-control" title="Bylaw Code" maxlength="12" />
					</div>
					<div class="col-sm-4">
						<label for="SECTION_CDE">Section Code</label><span class="text-danger">*</span>
						<input type="text" name="SECTION_CDE" class="form-control" title="Section Code" maxlength="15" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="BYLAW_DSC">Bylaw Description</label>
						<textarea name="BYLAW_DSC" class="form-control" title="Bylay Description" maxlength="1000" ></textarea>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-md btn-primary add_SOT pull-right" value="Add Bylaw" />
						<a href="<?php echo base_url(); ?>SOT/offence_bylaws" class="btn btn-md btn-default return_SOT">Back to Offence Bylaws</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>