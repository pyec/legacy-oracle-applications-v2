<!-- Page title -->
<div class="row">
	<div class="col-md-12">
		<h2>Update Street File</h2>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<!-- Error reporting -->
		<?php if ($this->session->flashdata('error')): ?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p><?php echo $this->session->flashdata('error'); ?></p>
			</div>
		<?php elseif ($this->session->flashdata('message')): ?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p><?php echo $this->session->flashdata('message'); ?></p>
			</div>
		<?php endif ?>
		<!-- Beginning of update input fields row 1 -->
		<form action="<?php echo base_url(); ?>street_file/update" method="POST" class="" id="update_street_file_form">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="STR_CODE">Street Code</label>
					<input type="text" name="STR_CODE" class="form-control" title="Street Code" value="<?php echo $street[0]['STR_CODE'] ?>" readonly />
				</div>
				<div class="col-sm-4">
					<label for="STNAME">Street Name</label>
					<input type="text" name="STNAME" class="form-control add_street_name" title="Street Name" value="<?php echo $street[0]['STNAME'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="SUFFIX">Street Type</label>
					<select name="SUFFIX" class="form-control add_suffix" title="Street Type">
						<option value="<?php echo $street[0]['SUFFIX'] ?>" selected><?php echo $street[0]['SUFFIX'] ?></option>
						<?php foreach ($street_types as $type): ?>
							<option value="<?php echo $type['SUFFIX'] ?>"><?php echo $type['SUFFIX'] ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-2">
					<label for="CITY">Community</label>
					<input type="text" name="CITY" class="form-control add_community_name" title="Community" value="<?php echo $street[0]['CITY'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="PROVINCE">Province</label>
					<input type="text" name="PROVINCE" class="form-control" title="Province" value="<?php echo $street[0]['PROVINCE'] ?>" disabled />
					<input type="hidden" name="PROVINCE" value="<?php echo $street[0]['PROVINCE'] ?>" />
				</div>
			</div>
		</div>

		<legend>Additional Information</legend>

		<!-- Update input fields row 4 -->
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="LSTNOLO">Odd Low</label>
					<input type="text" name="LSTNOLO" class="form-control" title="Odd Low" value="<?php echo $street[0]['LSTNOLO'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="LSTNOHI">Odd High</label>
					<input type="text" name="LSTNOHI" class="form-control" title="Odd High" value="<?php echo $street[0]['LSTNOHI'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="RSTNOLO">Even Low</label>
					<input type="text" name="RSTNOLO" class="form-control" title="Even Low" value="<?php echo $street[0]['RSTNOLO'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="RSTNOHI">Even High</label>
					<input type="text" name="RSTNOHI" class="form-control" title="Even High" value="<?php echo $street[0]['RSTNOHI'] ?>" />
				</div>
			</div>
		</div>

		<!-- Update input fields row 5 -->
		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<label for="SDE_STNAME">Official Street Name</label>
					<input type="text" name="SDE_STNAME" class="form-control official_street_name" title="Official Street Name" value="<?php echo $street[0]['SDE_STNAME'] ?>" disabled />
					<input type="hidden" name="SDE_STNAME" class="form-control official_street_name" title="Official Street Name" value="<?php echo $street[0]['SDE_STNAME'] ?>" />
				</div>
				<div class="col-sm-6">
					<label for="SDE_CITY">Official City Name</label>
					<input type="text" name="SDE_CITY" class="form-control official_community_name" title="Official City Name" value="<?php echo $street[0]['SDE_CITY'] ?>" disabled />
					<input type="hidden" name="SDE_CITY" class="form-control official_community_name" title="Official City Name" value="<?php echo $street[0]['SDE_CITY'] ?>" />
				</div>
			</div>
		</div>

		<!-- Update input fields row 3 -->
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="MODBY">Last Update By</label>
					<input type="text" name="MODBY" class="form-control" title="Last Update By" value="<?php echo $street[0]['MODBY'] ?>" readonly />
				</div>
				<div class="col-sm-2">
					<input type="text" name="MODDTTM" class="form-control no_label_fix" title="Last Updated Date" value="<?php echo $street[0]['MODDTTM'] ?>" readonly />
				</div>

				<div class="col-sm-2">
					<label class="no_label_fix" for="expired">Expired?</label>
					<?php if ($street[0]['EXP_FLAG'] == 'Y'): ?>
						<input checked type="checkbox" name="EXP_FLAG" title="Expired?" value="Y" />
					<?php else : ?>
						<input type="checkbox" name="EXP_FLAG" title="Expired?" value="Y" />
					<?php endif ?>
				</div>
			</div>
		</div>

		<!-- Submit and back buttons -->
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="pull-right btn btn-primary btn-md" value="Update Street" id="street_file_update_button" disabled="true" />
					<a class="btn btn-default btn-md" href="<?php echo base_url(); ?>street_file">Back to Street File</a>
				</div>
			</div>
		</div>
	</form>
	</div>
</div>