<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Mayor Call Traking System Model

Description : This is the data layer for the mayor's call traking system.

  Functions : get_calls( $offset = 0, $limit = 0 ),
  			get_call_by_id( $id = null ),
  			insert_call( $data = null ),
  			update_call( $data = null ),
  			get_resolved_calls( $offset = 0, $limit = 0 ),
  			get_resolved_call_by_id( $id = null ),
  			insert_resolved_call( $data = null ),
  			update_resolved_call( $data = null ),
  			remove_resolved_call( $id = null ),
  			get_call_types ( $offset = 0, $limit = 0 ),
  			get_call_types_by_id ( $id = null ),
  			insert_call_type( $data = null ),
  			update_call_type( $data = null ),
  			remove_call_type( $id = null ),
  			get_district_councillors( $offset = 0, $limit = 0 ),
  			get_district_councillors_by_id( $id = null ),
  			get_district_councillors_by_name( $name = null ),
  			insert_district_councillor( $data = null ),
  			update_district_councillor( $data = null ),
  			remove_district_councillor( $id = null ),
  			get_source_types( $offset = 0, $limit = 0 ),
  			insert_source_types( $data = null ),
  			update_source_type( $data = null ),
  			remove_source_type( $id = null )

=========================================================================================================================*/
class MCT_model extends CI_Model 
{
	//Properties
	public $table_calls = null;
	public $id_calls = null;
	public $first_name_calls = null;
	public $last_name_calls = null;

	public $table_resolved_calls = null;
	public $id_resolved_calls = null;

	public $table_call_types = null;
	public $id_call_types = null;

	public $table_district_councillors = null;
	public $id_district_councillors = null;
	public $first_name_district_councillors = null;
	public $name_district_councillors = null;

	public $table_source_types = null;
	public $id_source_types = null;

	public $oracle_db = null; // The reference to the database


	//Constructor
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies

		$this->table_resolved_calls = 'MCT_MASTER';
		$this->id_resolved_calls = 'MCT_NBR';

		$this->table_calls = "MCT_MASTER";
		$this->id_calls = "MCT_NBR";
		$this->first_name_calls = 'MCT_FST_NME';
		$this->last_name_calls = 'MCT_LST_NME';

		$this->table_call_types = 'MCT_TYPE';
		$this->id_call_types = 'MCT_TYPE_DESC';

		$this->table_district_councillors = 'MCT_COUNCIL';
		$this->id_district_councillors = 'MCT_DISTRICT';
		$this->name_district_councillors = 'MCT_COUNCILLOR';

		$this->table_source_types = 'MCT_SOURCE';
		$this->id_source_types = 'MCT_SOURCE_DESC';

		$this->oracle_db = $this->load->database('oracle_MCT', TRUE);

        $this->oracle_db->query("alter session set nls_date_format = 'DD-MON-YYYY'");
	}

/*           
============================================================================
get_next_val
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
int - The next id in the table.
============================================================================
*/

	public function get_next_val()
	{
		// Query for the max value in the id field.
		$query = $this->oracle_db->query("SELECT MAX(MCT_NBR) FROM MCT_MASTER");

		// In one expression return the result sets value, cast it to an integer 
		// and store it in the variable $id.
		$id = (int)$query->result_array()[0]["MAX(MCT_NBR)"];

		// Increment by 1 and return the id so we can add a new record to the table.
		return ++$id;
	}

	public function get_civic_address_key($civic_name = null, $civic_number = null)
	{
		$this->oracle_db->where('STNO', (int)$civic_number);
		$this->oracle_db->where('STNAME', $civic_name);
		$query = $this->oracle_db->get('ACTADM.ADDR2_VW');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_parcel_key($addrkey = null)
	{
		$this->oracle_db->select('PRCLKEY');
		$this->oracle_db->where('ADDRKEY', $addrkey);
		$query = $this->oracle_db->get('ACTADM.ADDRPRCL_VW');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

/*-----------------------------------------------------------------------------------
	Open Call retrieval functions
------------------------------------------------------------------------------------*/
	public function get_open_calls()
	{
		$this->oracle_db->where('MCT_RESOLVE_FLG', 'N');
		$query = $this->oracle_db->get($this->table_resolved_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function search_resolved_calls( $data = null )
	{
		$this->oracle_db->where('MCT_RESOLVE_FLG', 'Y');

		if (!empty($data['first_name'])){$this->oracle_db->like('MCT_FST_NME', ucwords(strtolower($data['first_name'])));}
		if (!empty($data['last_name'])){$this->oracle_db->like('MCT_LST_NME', ucwords(strtolower($data['last_name'])));}
		if (!empty($data['call_number'])){$this->oracle_db->like('MCT_NBR', $data['call_number']);}

		$query = $this->oracle_db->get($this->table_resolved_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function search_open_calls( $data = null )
	{
		$this->oracle_db->where('MCT_RESOLVE_FLG', 'N');

		if (!empty($data['first_name'])){$this->oracle_db->like('MCT_FST_NME', ucwords(strtolower($data['first_name'])));}
		if (!empty($data['last_name'])){$this->oracle_db->like('MCT_LST_NME', ucwords(strtolower($data['last_name'])));}
		if (!empty($data['call_number'])){$this->oracle_db->like('MCT_NBR', $data['call_number']);}

		$query = $this->oracle_db->get($this->table_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_call_by_id( $id = null, $resolved = null )
	{
		$this->oracle_db->where($this->id_calls, $id);

		if ($resolved != null)
		{
			$this->oracle_db->where('MCT_RESOLVE_FLG', $resolved);
		}

		$query = $this->oracle_db->get($this->table_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_call_like_id( $id = null, $resolved = null )
	{
		$this->oracle_db->like($this->id_calls, $id);

		if ($resolved != null)
		{
			$this->oracle_db->like('MCT_RESOLVE_FLG', $resolved);
		}

		$query = $this->oracle_db->get($this->table_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_call_by_first_name( $first_name = null, $resolved = null )
	{
		$this->oracle_db->like($this->first_name_calls, $first_name);

		if ($resolved != null)
		{
			$this->oracle_db->where('MCT_RESOLVE_FLG', $resolved);
		}
		
		$query = $this->oracle_db->get($this->table_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_call_by_last_name( $last_name = null, $resolved = null )
	{
		$this->oracle_db->like($this->last_name_calls, $last_name);

		if ($resolved != null)
		{
			$this->oracle_db->where('MCT_RESOLVE_FLG', $resolved);
		}

		$query = $this->oracle_db->get($this->table_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_call_by_name( $name = null, $resolved = null )
	{
		$this->oracle_db->like($this->first_name_calls, $name['first_name']);
		$this->oracle_db->like($this->last_name_calls, $name['last_name']);

		if ($resolved != null)
		{
			$this->oracle_db->where('MCT_RESOLVE_FLG', $resolved);
		}

		$query = $this->oracle_db->get($this->table_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}


/*-----------------------------------------------------------------------------------
	Call insertion function
------------------------------------------------------------------------------------*/
	public function insert_call( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
//			$data['MCT_ENTER_DTE'] = "to_date('".$data['MCT_ENTER_DTE']."','YYYY-mm-dd')";
//			$data['MCT_REF_DTE'] = "to_date('".$data['MCT_REF_DTE']."','YYYY-mm-dd')";
//			$data['MCT_DUE_DTE'] = "to_date('".$data['MCT_DUE_DTE']."','YYYY-mm-dd')";

//			$this->oracle_db->set("MCT_ENTER_DTE", $data['MCT_ENTER_DTE'], false);
//			$this->oracle_db->set("MCT_REF_DTE", $data['MCT_REF_DTE'], false);
//			$this->oracle_db->set("MCT_DUE_DTE", $data['MCT_DUE_DTE'], false);
			$this->oracle_db->set("MCT_CVC_NUM", (int)$data['MCT_CVC_NUM'], false);
			$this->oracle_db->set("STREET_CDE", (int)$data['STREET_CDE'], false);
			$this->oracle_db->set("MCT_PRIORITY", (int)$data['MCT_PRIORITY'], false);
			$this->oracle_db->set("MCT_DISTRICT", (int)$data['MCT_DISTRICT'], false);

//			unset($data['MCT_ENTER_DTE']);
//			unset($data['MCT_REF_DTE']);
//			unset($data['MCT_DUE_DTE']);
			
			unset($data['MCT_CVC_NUM']);
			unset($data['STREET_CDE']);
			unset($data['MCT_PRIORITY']);
			unset($data['MCT_DISTRICT']);

			$results = $this->oracle_db->insert($this->table_calls, $data);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Call update function 
------------------------------------------------------------------------------------*/
	public function update_open_call( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
//			$data['MCT_ENTER_DTE'] = "to_date('".$data['MCT_ENTER_DTE']."','YYYY-mm-dd')";
//			$data['MCT_REF_DTE'] = "to_date('".$data['MCT_REF_DTE']."','YYYY-mm-dd')";
//			$data['MCT_DUE_DTE'] = "to_date('".$data['MCT_DUE_DTE']."','YYYY-mm-dd')";

//			$this->oracle_db->set("MCT_ENTER_DTE", $data['MCT_ENTER_DTE'], false);
//			$this->oracle_db->set("MCT_REF_DTE", $data['MCT_REF_DTE'], false);
//			$this->oracle_db->set("MCT_DUE_DTE", $data['MCT_DUE_DTE'], false);
			$this->oracle_db->set("MCT_CVC_NUM", (int)$data['MCT_CVC_NUM'], false);
			$this->oracle_db->set("STREET_CDE", (int)$data['STREET_CDE'], false);
			$this->oracle_db->set("MCT_PRIORITY", (int)$data['MCT_PRIORITY'], false);
			$this->oracle_db->set("MCT_DISTRICT", (int)$data['MCT_DISTRICT'], false);

//			unset($data['MCT_ENTER_DTE']);
//			unset($data['MCT_REF_DTE']);
//			unset($data['MCT_DUE_DTE']);
			
			unset($data['MCT_CVC_NUM']);
			unset($data['STREET_CDE']);
			unset($data['MCT_PRIORITY']);
			unset($data['MCT_DISTRICT']);

			$this->oracle_db->where($this->id_calls, $data[$this->id_calls]);
			$results = $this->oracle_db->update($this->table_calls, $data);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Call removal function 
------------------------------------------------------------------------------------*/
	public function remove_open_call( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_calls, $id);
			$results = $this->oracle_db->delete($this->table_calls);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Resolved Call retrieval functions
------------------------------------------------------------------------------------*/
	public function get_resolved_calls()
	{
		$this->oracle_db->where('MCT_RESOLVE_FLG', 'Y');
		$query = $this->oracle_db->get($this->table_resolved_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_resolved_call_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_resolved_calls, $id);
		$this->oracle_db->where('MCT_RESOLVE_FLG', 'Y');
		$query = $this->oracle_db->get($this->table_resolved_calls);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

// -----------------------------------------------------------------------------------
// 	Resolved Call insertion function
// ------------------------------------------------------------------------------------
// 	public function insert_resolved_call( $data = null )
// 	{
// 		if ($data == null)
// 		{
// 			return false;
// 		}
// 		else
// 		{
// 			$this->oracle_db->insert($this->table_resolved_calls, $data);
// 		}
// 	}

/*-----------------------------------------------------------------------------------
	Resolved Call update function 
------------------------------------------------------------------------------------*/
	public function update_resolved_call( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['MCT_ENTER_DTE'] = "to_date('".$data['MCT_ENTER_DTE']."','YYYY-mm-dd')";
			$data['MCT_REF_DTE'] = "to_date('".$data['MCT_REF_DTE']."','YYYY-mm-dd')";
			$data['MCT_DUE_DTE'] = "to_date('".$data['MCT_DUE_DTE']."','YYYY-mm-dd')";

			$this->oracle_db->set("MCT_ENTER_DTE", $data['MCT_ENTER_DTE'], false);
			$this->oracle_db->set("MCT_REF_DTE", $data['MCT_REF_DTE'], false);
			$this->oracle_db->set("MCT_DUE_DTE", $data['MCT_DUE_DTE'], false);
			$this->oracle_db->set("MCT_CVC_NUM", (int)$data['MCT_CVC_NUM'], false);
			$this->oracle_db->set("STREET_CDE", (int)$data['STREET_CDE'], false);
			$this->oracle_db->set("MCT_PRIORITY", (int)$data['MCT_PRIORITY'], false);
			$this->oracle_db->set("MCT_DISTRICT", (int)$data['MCT_DISTRICT'], false);

			unset($data['MCT_ENTER_DTE']);
			unset($data['MCT_REF_DTE']);
			unset($data['MCT_DUE_DTE']);
			
			unset($data['MCT_CVC_NUM']);
			unset($data['STREET_CDE']);
			unset($data['MCT_PRIORITY']);
			unset($data['MCT_DISTRICT']);

			$this->oracle_db->where($this->id_calls, $data[$this->id_calls]);
			$results = $this->oracle_db->update($this->table_calls, $data);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Resolved Call removal function
------------------------------------------------------------------------------------*/
	public function remove_resolved_call( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_resolved_calls, $id);
			$results = $this->oracle_db->delete($this->table_resolved_calls);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Call Type retrieval function
------------------------------------------------------------------------------------*/
	public function get_call_types()
	{
		$this->oracle_db->order_by('MCT_TYPE_DESC ASC');
		$query = $this->oracle_db->get($this->table_call_types);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_call_types_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_call_types, $id);
		$query = $this->oracle_db->get($this->table_call_types);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function search_types( $name = null )
	{
		if ($name != null)
		{
			$this->oracle_db->like('MCT_TYPE_DESC', $name);
			$query = $this->oracle_db->get($this->table_call_types);

			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}
		}
	}

	public function search_sources( $name = null )
	{
		if ($name != null)
		{
			$this->oracle_db->like('MCT_SOURCE_DESC', $name);
			$query = $this->oracle_db->get($this->table_source_types);

			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}
		}
	}

/*-----------------------------------------------------------------------------------
	Call Type insertion function
------------------------------------------------------------------------------------*/
	public function insert_call_type( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$call_type_check = $this->get_call_types_by_id($data[$this->id_call_types]);
			if (!$call_type_check)
			{
				$results = $this->oracle_db->insert($this->table_call_types, $data);
				return $results;
			}
			else
			{
				$this->session->set_flashdata('error', 'This call type already exists.');
				redirect('MCT/call_types');
			}	
		}
	}

/*-----------------------------------------------------------------------------------
	Call Type update function 
------------------------------------------------------------------------------------*/
	public function update_call_type( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_call_types, $data[$this->id_call_types]);
			$this->oracle_db->update($this->table_call_types, $data);
		}
	}

/*-----------------------------------------------------------------------------------
	Call Type removal function
------------------------------------------------------------------------------------*/
	public function remove_call_type( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_call_types, $id);
			$results = $this->oracle_db->delete($this->table_call_types);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	District Coucillor retrieval functions
------------------------------------------------------------------------------------*/
// 	public function get_district_councillors()
// 	{
// 		$this->oracle_db->order_by('MCT_DISTRICT ASC');
// 		$query = $this->oracle_db->get($this->table_district_councillors);

// 		if ($query->num_rows() > 0)
// 		{ 
// 			return $query->result_array();
// 		}
// 		else 
// 		{
// 			return false;
// 		}
// 	}

// 	public function get_district_councillor_by_id( $id = null )
// 	{
// 		$this->oracle_db->where($this->id_district_councillors, $id);
// 		$query = $this->oracle_db->get($this->table_district_councillors);

// 		if ($query->num_rows() > 0)
// 		{ 
// 			return $query->result_array();
// 		}
// 		else 
// 		{
// 			return false;
// 		}
// 	}

// 	public function get_district_councillor_by_name( $name = null )
// 	{
// 		if ($name == null)
// 		{
// 			return false;
// 		}
// 		else
// 		{
// 			$this->oracle_db->where($this->name_district_councillors, $name);
// 			$query = $this->oracle_db->get($this->table_district_councillors);

// 			if ($query->num_rows() > 0)
// 			{
// 				return $query->row_result();
// 			}
// 			else
// 			{
// 				return false;
// 			}
// 		}
// 	}

// /*-----------------------------------------------------------------------------------
// 	District Coucillor insertion function
// ------------------------------------------------------------------------------------*/
// 	public function insert_district_councillor( $data = null )
// 	{
// 		if ($data == null)
// 		{
// 			return false;
// 		}
// 		else
// 		{
// 			$results = $this->oracle_db->insert($this->table_district_councillors, $data);

// 			return $results;
// 		}
// 	}

// /*-----------------------------------------------------------------------------------
// 	District Coucillor update function 
// ------------------------------------------------------------------------------------*/
// 	public function update_district_councillor( $data = null )
// 	{				
// 		if ($data == null)
// 		{
// 			return false;
// 		}
// 		else
// 		{
// 			$this->oracle_db->set("MCT_DISTRICT", $data['MCT_DISTRICT']);
// 			$this->oracle_db->set("MCT_DIST_DESC", $data['MCT_DIST_DESC']);
// 			$this->oracle_db->set("MCT_COUNCILLOR", $data['MCT_COUNCILLOR']);
// 			$this->oracle_db->where($this->id_district_councillors, $data[$this->id_district_councillors]);
// 			$results = $this->oracle_db->update($this->table_district_councillors);

// 			return $results;
// 		}
// 	}

// /*-----------------------------------------------------------------------------------
// 	District Coucillor removal function
// ------------------------------------------------------------------------------------*/
// 	public function remove_district_councillor( $id = null )
// 	{
// 		if ($id == null)
// 		{
// 			return false;
// 		}
// 		else
// 		{
// 			$this->oracle_db->where($this->id_district_councillors, $id);
// 			$results = $this->oracle_db->delete($this->table_district_councillors);
// 			return $results;
// 		}
// 	}

/*-----------------------------------------------------------------------------------
	Source Types retrieval function 
------------------------------------------------------------------------------------*/
	public function get_source_types()
	{
		$query = $this->oracle_db->get($this->table_source_types);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}



/*-----------------------------------------------------------------------------------
	Source Types insertion function
------------------------------------------------------------------------------------*/
	public function insert_source_types( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_source_types, $data);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Source Types update function
------------------------------------------------------------------------------------*/
	public function update_source_type( $data = null )
	{
		if ($data = null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_source_types, $data[$this->id_source_types]);
			$this->oracle_db->update($this->table_source_types, $data);
		}
	}

/*-----------------------------------------------------------------------------------
	Source Types removal function 
------------------------------------------------------------------------------------*/
	public function remove_source_type( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_source_types, $id);
			$results = $this->oracle_db->delete($this->table_source_types);
			return $results;
		}
	}

}

/* End of file MCT_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/MCT_model.php */