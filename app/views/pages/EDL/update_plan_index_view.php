<?php 
// determine if the user is an admin 
	$is_admin = false;
	$read_only = false;

	$groups = $this->session->userdata('groups');

	foreach ($groups as $group) 
	{
		if ($group == 'AppHRMAppsEngineeringDrawingLocationReadOnly')
		{
			$read_only = true;
		}

		if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
		{
			$is_admin = true;
			$read_only = false;
		}
	}
 ?>
<div class="row">
	<div class="col-6-md">
		<h2>Update Plan Index</h2>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php 

		$plan_date = $this->EDL_model->convert_date(array($plan[0]['PLAN_DAY'], $plan[0]['PLAN_MONTH'], $plan[0]['PLAN_YR']));
		$alter_date = $this->EDL_model->convert_date(array($plan[0]['ALTER_DAY'], $plan[0]['ALTER_MTH'], $plan[0]['ALTER_YR']));

 		?>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>EDL/update_plan_index" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="PLAN_NUM">Plan #</label>
					<?php if ($is_admin): ?>
						<input type="text" name="PLAN_NUM" class="form-control" title="Plan Number" value="<?= $plan[0]['PLAN_NUM'] ?>" />
					<?php else: ?>
						<input type="text" name="PLAN_NUM" class="form-control" title="Plan Number" value="<?= $plan[0]['PLAN_NUM'] ?>" disabled/>
						<input type="hidden" name="PLAN_NUM" value="<?= $plan[0]['PLAN_NUM'] ?>" readonly/>
					<?php endif ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<?php if (!$read_only): ?>
						<button type="button" value="street" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select</button>
					<?php endif ?>
				</div>
				<div class="col-sm-3">
					<label for="STR_NAME">Street Name</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="STR_NAME" class="form-control" title="Street Name" value="<?= $plan[0]['STR_NAME'] ?>" />
					<?php else: ?>
						<input type="text" name="STR_NAME" class="form-control" title="Street Name" readonly value="<?= $plan[0]['STR_NAME'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<?php if ($read_only): ?>
						<input disabled type="text" name="STR_TYPE" class="form-control no_label_fix" title="Street Type" value="<?= $plan[0]['STR_TYPE'] ?>" />
					<?php else: ?>
						<input type="text" name="STR_TYPE" class="form-control no_label_fix" title="Street Type" readonly value="<?= $plan[0]['STR_TYPE'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<?php if ($read_only): ?>
						<input disabled type="text" name="STR_NUMBER" class="form-control no_label_fix" title="Street Number" value="<?= $plan[0]['STR_NUMBER'] ?>" />
					<?php else: ?>
						<input type="text" name="STR_NUMBER" class="form-control no_label_fix" title="Street Number" readonly value="<?= $plan[0]['STR_NUMBER'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-2">
					<label for="STR_SIDE">Side of Street</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="STR_SIDE" class="form-control" title="Side of Street" value="<?= $plan[0]['STR_SIDE'] ?>" />
					<?php else: ?>
						<input type="text" name="STR_SIDE" class="form-control" title="Side of Street" value="<?= $plan[0]['STR_SIDE'] ?>" />
					<?php endif ?>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<?php if (!$read_only): ?>
						<button type="button" value="limit_from" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select</button>
					<?php endif ?>
				</div>
				<div class="col-sm-3">
					<?php if ($read_only): ?>
						<input disabled type="text" name="LIMIT_FROM" class="form-control no_label_fix" title="Limit From Description" value="<?= $plan[0]['LIMIT_FROM'] ?>" />
					<?php else: ?>
						<input type="text" name="LIMIT_FROM" class="form-control no_label_fix" title="Limit From Description" readonly value="<?= $plan[0]['LIMIT_FROM'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<?php if ($read_only): ?>
						<input disabled type="text" name="LIM_FRM_T" class="form-control no_label_fix" title="Limit From Abbriviation" value="<?= $plan[0]['LIM_FRM_T'] ?>" />
					<?php else: ?>
						<input type="text" name="LIM_FRM_T" class="form-control no_label_fix" title="Limit From Abbriviation" readonly value="<?= $plan[0]['LIM_FRM_T'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-3">
					<label for="plan_date">Plan Date</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="plan_date" class="form-control date_picker" title="Plan Date" value="<?= $plan_date ?>" />
					<?php else: ?>
						<input type="text" name="plan_date" class="form-control date_picker" title="Plan Date" readonly value="<?= $plan_date ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-3">
					<label for="altered_date">Altered Date</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="altered_date" class="form-control date_picker" title="Altered Date" value="<?= $alter_date ?>" />
					<?php else: ?>
						<input type="text" name="altered_date" class="form-control date_picker" title="Altered Date" readonly value="<?= $alter_date ?>" />
					<?php endif ?>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<?php if (!$read_only): ?>
						<button type="button" value="limit_to" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select</button>
					<?php endif ?>
				</div>
				<div class="col-sm-3">
					<?php if ($read_only): ?>
						<input disabled type="text" name="LIMIT_TO" class="form-control no_label_fix" title="Limit To Description" value="<?= $plan[0]['LIMIT_TO'] ?>" />
					<?php else: ?>
						<input type="text" name="LIMIT_TO" class="form-control no_label_fix" title="Limit To Description" readonly value="<?= $plan[0]['LIMIT_TO'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<?php if ($read_only): ?>
						<input disabled type="text" name="LIM_TO_T" class="form-control no_label_fix" title="Limit To Abbriviation" value="<?= $plan[0]['LIM_TO_T'] ?>" />
					<?php else: ?>
						<input type="text" name="LIM_TO_T" class="form-control no_label_fix" title="Limit To Abbriviation" readonly value="<?= $plan[0]['LIM_TO_T'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-4">
					<label for="SUB_NUMBER">Subject</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="" class="form-control" title=""  value="<?= $plan[0]['SUB_NUMBER'] ?>" />
					<?php else: ?>
						<select name="SUB_NUMBER" class="form-control" title="Subject">
							<option value="">Select..</option>
							<?php foreach ($subjects as $sub): ?>
								<?php if ($plan[0]['SUB_NUMBER'] == $sub['SUBJ_NUM']): ?>
									<option selected value="<?= $sub['SUBJ_NUM'].','.$sub['SUBJ_NAME'] ?>"><?= $sub['SUBJ_NUM'].' - '.$sub['SUBJ_NAME'] ?></option>
							 	<?php else : ?>
									<option value="<?= $sub['SUBJ_NUM'].','.$sub['SUBJ_NAME'] ?>"><?= $sub['SUBJ_NUM'].' - '.$sub['SUBJ_NAME'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					<?php endif ?>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="PARCEL">Parcel</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="PARCEL" class="form-control" title="Parcel" value="<?= $plan[0]['PARCEL'] ?>" />
					<?php else: ?>
						<input type="text" name="PARCEL" class="form-control" title="Parcel" value="<?= $plan[0]['PARCEL'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-4">
					<label for="CIVIC_NUM">Civic #</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="CIVIC_NUM" class="form-control" title="Civic Number" value="<?= $plan[0]['CIVIC_NUM'] ?>" />
					<?php else: ?>
						<input type="text" name="CIVIC_NUM" class="form-control" title="Civic Number" value="<?= $plan[0]['CIVIC_NUM'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-4">
					<label for="SURVEY_BY">Survey By</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="SURVEY_BY" class="form-control" title="Survey By" value="<?= $plan[0]['SURVEY_BY'] ?>" />
					<?php else: ?>
						<input type="text" name="SURVEY_BY" class="form-control" title="Survey By" value="<?= $plan[0]['SURVEY_BY'] ?>" />
					<?php endif ?>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="S_DIV_NUM">Subdivision</label>
					<?php if ($read_only): ?>
						<input type="text" name="" class="form-control" title="" value="<?= $plan[0]['S_DIV_NUM'] ?>"  disabled />
					<?php else: ?>
						<select name="S_DIV_NUM" class="form-control" title="Subdivision">
							<option value="">Select...</option>
							<?php foreach ($subdivision as $subd): ?>
								<?php if ($plan[0]['S_DIV_NUM'] == $subd['SD_NUM']): ?>
								<option selected value="<?= $subd['SD_NUM'] ?>"><?= $subd['SD_NAME'] ?></option>
								<?php else : ?>
								<option value="<?= $subd['SD_NUM'] ?>"><?= $subd['SD_NUM'].' - '.$subd['SD_NAME'] ?></option>	
								<?php endif ?>
							<?php endforeach ?>
						</select>
					<?php endif ?>
				</div>
				<div class="col-sm-2">
					<label for="BLOCK_NUM">Block #</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="BLOCK_NUM" class="form-control" title="Block Number" value="<?= $plan[0]['BLOCK_NUM'] ?>" />
					<?php else: ?>
						<input type="text" name="BLOCK_NUM" class="form-control" title="Block Number" value="<?= $plan[0]['BLOCK_NUM'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-2">
					<label for="LOT_NUMBER">Lot #</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="LOT_NUMBER" class="form-control" title="Lot Number" value="<?= $plan[0]['LOT_NUMBER'] ?>" />
					<?php else: ?>
						<input type="text" name="LOT_NUMBER" class="form-control" title="Lot Number" value="<?= $plan[0]['LOT_NUMBER'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-2">
					<label for="ISSING" class="no_label_fix">Missing</label>
					<?php if ($plan[0]['ISSING'] == 'X'): ?>
						<input type="checkbox" name="ISSING" class="" title="Missing" value="<?= $plan[0]['ISSING'] ?>" checked/>
					<?php else : ?>
						<input type="checkbox" name="ISSING" class="" title="Missing" value="<?= $plan[0]['ISSING'] ?>" />
					<?php endif ?>
				</div>
			</div>
		</div>

		<legend>Options</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<label for="REC" title="Recreation">Rec</label>
					<?php if (!empty($plan[0]['REC'])): ?>
						<input checked type="checkbox" name="REC" title="REC" value="<?= $plan[0]['REC'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="REC" title="REC" value="<?= $plan[0]['REC'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="AS_BUILTS" title="As Built">As Blt</label>
					<?php if (!empty($plan[0]['AS_BUILTS'])): ?>
						<input checked type="checkbox" name="AS_BUILTS" title="AS_BUILTS" value="<?= $plan[0]['AS_BUILTS'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="AS_BUILTS" title="AS_BUILTS" value="<?= $plan[0]['AS_BUILTS'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="PLAN_PROF" title="Plan Prof">PP</label>
					<?php if (!empty($plan[0]['PLAN_PROF'])): ?>
						<input checked type="checkbox" name="PLAN_PROF" title="PLAN_PROF" value="<?= $plan[0]['PLAN_PROF'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="PLAN_PROF" title="PLAN_PROF" value="<?= $plan[0]['PLAN_PROF'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="EX_PRO_COD" title="Ex Pro Cod">ExPC</label>
					<?php if (!empty($plan[0]['EX_PRO_COD'])): ?>
						<input checked type="checkbox" name="EX_PRO_COD" title="EX_PRO_COD" value="<?= $plan[0]['EX_PRO_COD'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="EX_PRO_COD" title="EX_PRO_COD" value="<?= $plan[0]['EX_PRO_COD'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="SUBD">Subd</label>
					<?php if (!empty($plan[0]['SUBD'])): ?>
						<input checked type="checkbox" name="SUBD" title="SUBD" value="<?= $plan[0]['SUBD'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="SUBD" title="SUBD" value="<?= $plan[0]['SUBD'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="REC_DRAWS" title="Rec Draws">Rec D</label>
					<?php if (!empty($plan[0]['REC_DRAWS'])): ?>
						<input checked type="checkbox" name="REC_DRAWS" title="REC_DRAWS" value="<?= $plan[0]['REC_DRAWS'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="REC_DRAWS" title="REC_DRAWS" value="<?= $plan[0]['REC_DRAWS'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="CONV" title="Conv">Conv</label>
					<?php if (!empty($plan[0]['CONV'])): ?>
						<input checked type="checkbox" name="CONV" title="CONV" value="<?= $plan[0]['CONV'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="CONV" title="CONV" value="<?= $plan[0]['CONV'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1" title="Ease">
					<label for="EASE">Ease</label>
					<?php if (!empty($plan[0]['EASE'])): ?>
						<input checked type="checkbox" name="EASE" title="EASE" value="<?= $plan[0]['EASE'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="EASE" title="EASE" value="<?= $plan[0]['EASE'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="AQUIRED" title="Aquired">Aqu.</label>
					<?php if (!empty($plan[0]['AQUIRED'])): ?>
						<input checked type="checkbox" name="AQUIRED" title="AQUIRED" value="<?= $plan[0]['AQUIRED'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="AQUIRED" title="AQUIRED" value="<?= $plan[0]['AQUIRED'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="ST_LINE" title="St Line">St L</label>
					<?php if (!empty($plan[0]['ST_LINE'])): ?>
						<input checked type="checkbox" name="ST_LINE" title="ST_LINE" value="<?= $plan[0]['ST_LINE'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="ST_LINE" title="ST_LINE" value="<?= $plan[0]['ST_LINE'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="R_O_W" title="Row">ROW</label>
					<?php if (!empty($plan[0]['R_O_W'])): ?>
						<input checked type="checkbox" name="R_O_W" title="R_O_W" value="<?= $plan[0]['R_O_W'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="R_O_W" title="R_O_W" value="<?= $plan[0]['R_O_W'] ?>" />
					<?php endif ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<label for="ST_CLOSURE" title="street closure">St C</label>
					<?php if (!empty($plan[0]['ST_CLOSURE'])): ?>
						<input checked type="checkbox" name="ST_CLOSURE" title="ST_CLOSURE" value="<?= $plan[0]['ST_CLOSURE'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="ST_CLOSURE" title="ST_CLOSURE" value="<?= $plan[0]['ST_CLOSURE'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="CONSOL" title="consol">Cons.</label>
					<?php if (!empty($plan[0]['CONSOL'])): ?>
						<input checked type="checkbox" name="CONSOL" title="CONSOL" value="<?= $plan[0]['CONSOL'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="CONSOL" title="CONSOL" value="<?= $plan[0]['CONSOL'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="ZONING" title="Zoning">Zon.</label>
					<?php if (!empty($plan[0]['ZONING'])): ?>
						<input checked type="checkbox" name="ZONING" title="ZONING" value="<?= $plan[0]['ZONING'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="ZONING" title="ZONING" value="<?= $plan[0]['ZONING'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="GAS" title="Gas">Gas</label>
					<?php if (!empty($plan[0]['GAS'])): ?>
						<input checked type="checkbox" name="GAS" title="GAS" value="<?= $plan[0]['GAS'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="GAS" title="GAS" value="<?= $plan[0]['GAS'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="LOCAL_IMP" title="Local Imp">Loc I</label>
					<?php if (!empty($plan[0]['LOCAL_IMP'])): ?>
						<input checked type="checkbox" name="LOCAL_IMP" title="LOCAL_IMP" value="<?= $plan[0]['LOCAL_IMP'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="LOCAL_IMP" title="LOCAL_IMP" value="<?= $plan[0]['LOCAL_IMP'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="NSPC">NSPC</label>
					<?php if (!empty($plan[0]['NSPC'])): ?>
						<input checked type="checkbox" name="NSPC" title="NSPC" value="<?= $plan[0]['NSPC'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="NSPC" title="NSPC" value="<?= $plan[0]['NSPC'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="PSC">PSC</label>
					<?php if (!empty($plan[0]['PSC'])): ?>
						<input checked type="checkbox" name="PSC" title="PSC" value="<?= $plan[0]['PSC'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="PSC" title="PSC" value="<?= $plan[0]['PSC'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="EX_PRO" title="Ex Pro">ExPro</label>
					<?php if (!empty($plan[0]['EX_PRO'])): ?>
						<input checked type="checkbox" name="EX_PRO" title="EX_PRO" value="<?= $plan[0]['EX_PRO'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="EX_PRO" title="EX_PRO" value="<?= $plan[0]['EX_PRO'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="MT_T">Mt T</label>
					<?php if (!empty($plan[0]['MT_T'])): ?>
						<input checked type="checkbox" name="MT_T" title="MT_T" value="<?= $plan[0]['MT_T'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="MT_T" title="MT_T" value="<?= $plan[0]['MT_T'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="REZONING" title="Rezoning">Rez</label>
					<?php if (!empty($plan[0]['REZONING'])): ?>
						<input checked type="checkbox" name="REZONING" title="REZONING" value="<?= $plan[0]['REZONING'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="REZONING" title="REZONING" value="<?= $plan[0]['REZONING'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="F_A" title="FA">FA</label>
					<?php if (!empty($plan[0]['F_A'])): ?>
						<input checked type="checkbox" name="F_A" title="F_A" value="<?= $plan[0]['F_A'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="F_A" title="F_A" value="<?= $plan[0]['F_A'] ?>" />
					<?php endif ?>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<label for="PAVING" title="Paving">Pav.</label>
					<?php if (!empty($plan[0]['PAVING'])): ?>
						<input checked type="checkbox" name="PAVING" title="PAVING" value="<?= $plan[0]['PAVING'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="PAVING" title="PAVING" value="<?= $plan[0]['PAVING'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="STAND_DRAW" title="Stand Draw">SD</label>
					<?php if (!empty($plan[0]['STAND_DRAW'])): ?>
						<input checked type="checkbox" name="STAND_DRAW" title="STAND_DRAW" value="<?= $plan[0]['STAND_DRAW'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="STAND_DRAW" title="STAND_DRAW" value="<?= $plan[0]['STAND_DRAW'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="SEWERS" title="Sewers">Sew.</label>
					<?php if (!empty($plan[0]['SEWERS'])): ?>
						<input checked type="checkbox" name="SEWERS" title="SEWERS" value="<?= $plan[0]['SEWERS'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="SEWERS" title="SEWERS" value="<?= $plan[0]['SEWERS'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="S_W" title="SW">SW</label>
					<?php if (!empty($plan[0]['S_W'])): ?>
						<input checked type="checkbox" name="S_W" title="S_W" value="<?= $plan[0]['S_W'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="S_W" title="S_W" value="<?= $plan[0]['S_W'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="STIL" title="Still">Still</label>
					<?php if (!empty($plan[0]['STIL'])): ?>
						<input checked type="checkbox" name="STIL" title="STIL" value="<?= $plan[0]['STIL'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="STIL" title="STIL" value="<?= $plan[0]['STIL'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="T_L" title="TL">TL</label>
					<?php if (!empty($plan[0]['T_L'])): ?>
						<input checked type="checkbox" name="T_L" title="T_L" value="<?= $plan[0]['T_L'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="T_L" title="T_L" value="<?= $plan[0]['T_L'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="R_WALL">R W</label>
					<?php if (!empty($plan[0]['R_WALL'])): ?>
						<input checked type="checkbox" name="R_WALL" title="R_WALL" value="<?= $plan[0]['R_WALL'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="R_WALL" title="R_WALL" value="<?= $plan[0]['R_WALL'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="CTV">CTV</label>
					<?php if (!empty($plan[0]['CTV'])): ?>
						<input checked type="checkbox" name="CTV" title="CTV" value="<?= $plan[0]['CTV'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="CTV" title="CTV" value="<?= $plan[0]['CTV'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="LIGHTS">Lights</label>
					<?php if (!empty($plan[0]['LIGHTS'])): ?>
						<input checked type="checkbox" name="LIGHTS" title="LIGHTS" value="<?= $plan[0]['LIGHTS'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="LIGHTS" title="LIGHTS" value="<?= $plan[0]['LIGHTS'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="C_G">CG</label>
					<?php if (!empty($plan[0]['C_G'])): ?>
						<input checked type="checkbox" name="C_G" title="C_G" value="<?= $plan[0]['C_G'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="C_G" title="C_G" value="<?= $plan[0]['C_G'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="XSECTIONS">X Sec</label>
					<?php if (!empty($plan[0]['XSECTIONS'])): ?>
						<input checked type="checkbox" name="XSECTIONS" title="XSECTIONS" value="<?= $plan[0]['XSECTIONS'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="XSECTIONS" title="XSECTIONS" value="<?= $plan[0]['XSECTIONS'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-2">
					<label for="PROFILE">Profile</label>
					<?php if (!empty($plan[0]['PROFILE'])): ?>
						<input checked type="checkbox" name="PROFILE" title="PROFILE" value="<?= $plan[0]['PROFILE'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="PROFILE" title="PROFILE" value="<?= $plan[0]['PROFILE'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-1">
					<label for="PLAN">Plan</label>
					<?php if (!empty($plan[0]['PLAN'])): ?>
						<input checked type="checkbox" name="PLAN" title="Plan" value="<?= $plan[0]['PLAN'] ?>" />
					<?php else :  ?>
						<input type="checkbox" name="PLAN" title="Plan" value="<?= $plan[0]['PLAN'] ?>" />
					<?php endif ?>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="CHECKED_BY">Checked By</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="CHECKED_BY" class="form-control" title="Checked By" value="<?= $plan[0]['CHECKED_BY'] ?>" />
					<?php else: ?>
						<input type="text" name="CHECKED_BY" class="form-control" title="Checked By" value="<?= $plan[0]['CHECKED_BY'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-3">
					<label for="CHECK_DATE">Check Date</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="CHECK_DATE" class="form-control date_picker" title="Check Date" value="<?= $plan[0]['CHECK_DATE'] ?>" />
					<?php else: ?>
						<input type="text" name="CHECK_DATE" class="form-control date_picker" title="Check Date" readonly value="<?= $plan[0]['CHECK_DATE'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-3">
					<label for="ENTERED_BY">Entered By</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="ENTERED_BY" class="form-control" title="Entered By" value="<?= $plan[0]['ENTERED_BY'] ?>" />
					<?php else: ?>
						<input type="text" name="ENTERED_BY" class="form-control" title="Entered By" value="<?= $plan[0]['ENTERED_BY'] ?>" />
					<?php endif ?>
				</div>
				<div class="col-sm-3">
					<label for="ENTER_DATE">Enter Date</label>
					<?php if ($read_only): ?>
						<input disabled type="text" name="ENTER_DATE" class="form-control date_picker" title="Enter Date" value="<?= $plan[0]['ENTER_DATE'] ?>" />
					<?php else: ?>
						<input type="text" name="ENTER_DATE" class="form-control date_picker" title="Enter Date" readonly value="<?= $plan[0]['ENTER_DATE'] ?>" />
					<?php endif ?>
				</div>
			</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-sm-8">
					<label for="DESC1">Description</label>
					<?php if ($read_only): ?>
						<textarea disabled name="DESC1" class="form-control" title="Description"><?= $plan[0]['DESC1'] ?></textarea>
					<?php else: ?>
						<textarea name="DESC1" class="form-control" title="Description"><?= $plan[0]['DESC1'] ?></textarea>
					<?php endif ?>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<?php if (!$read_only): ?>
						<input type="submit" class="btn btn-primary btn-md no_label_fix pull-right" value="Update Record" />	
					<?php endif ?>
					<a href="<?php echo base_url(); ?>EDL/plan_index_query" class="btn btn-default btn-md no_label_fix">Back to plan index query</a>
				</div>	
			</div>
		</div>		
	</form>
	</div>

	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title">Street Look Up</h4>
				</div>
				<div class="modal-body">
					<div class="container">
						<div class="row">
							<div class="col-sm-5">
								<div class="modal_search">
									<label for="search_box">Street Name</label>
									<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" />
									<p class="text-muted">(Select the name of the record you would like to use.)</p>
								</div>
							</div>
						</div>
					</div>
					<hr>
					<table class="table table-bordered">
						<thead>
							<th>Name</th>
							<th>Type</th>
							<th>Community</th>
							<th>#</th>
						</thead>
						<tbody class="modal_tbody">
							<tr>
								
							</tr>
						</tbody>	
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</div>




