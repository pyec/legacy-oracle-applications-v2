<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Business Unit Update Model

Description : This is the data layer for the BU_update system.

  Functions : get_business_units( $offset = 0 ),
  			insert_business_unit(),
  			update_business_unit( $id = NULL ),
  			remove_business_unit( $id = NULL )

  	  Notes: For an unknown reason we had to run all of the queries using straight sql.
  	  instead of active directories. 
=========================================================================================================================*/
class Business_unit_update_model extends CI_Controller 
{
	//Properties
	public $table = null;
	public $row_id = null;
	public $oracle_db = null; // The reference to the database

	//Constructor
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies

		$table = 'HRM_BUSUNIT';
		$row_id = 'BUSUNIT_CDE';
		$this->oracle_db = $this->load->database('oracle_business_unit', TRUE);
	}

/*           
============================================================================
get_next_val
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
int - The next id in the table.
============================================================================
*/

	public function get_next_val()
	{
		// Query for the max value in the id field.
		$query = $this->oracle_db->query("SELECT MAX(BUSUNIT_CDE) FROM HRM_BUSUNIT");

		// In one expression return the result sets value, cast it to an integer 
		// and store it in the variable $id.
		$id = (int)$query->result_array()[0]["MAX(BUSUNIT_CDE)"];

		// Increment by 1 and return the id so we can add a new record to the table.
		return ++$id;
	}

/*           
============================================================================
get_business_units
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
array - anonymous - The result set
boolean - if the query fails.
============================================================================
*/

	public function get_business_units()
	{
		$query = $this->oracle_db->query("SELECT * FROM HRM_BUSUNIT ORDER BY BUSUNIT_CDE DESC");
		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	/*           
============================================================================
get_business_unit_by_id
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
array - anonymous - The result set
boolean - if the query fails.
============================================================================
*/

	public function get_business_unit_by_id( $id )
	{
		$this->oracle_db->where('BUSUNIT_CDE', $id);
		$query = $this->oracle_db->get("HRM_BUSUNIT");
		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

/*           
============================================================================
insert_business_unit	
----------------------------------------------------------------------------
array - data - contains the data to insert.
----------------------------------------------------------------------------
boolean - On whether or not the query was successful.
============================================================================
*/
	public function insert_business_unit( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$id = $this->get_next_val();

			if ($this->oracle_db->query("INSERT INTO HRM_BUSUNIT (BUSUNIT_CDE, BUSUNIT_DSC, CURRENT_FLG) VALUES ('".$id."', '".$data['add_business_unit_name']."', 'Y')"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

/*           
============================================================================
update_business_unit	
----------------------------------------------------------------------------
array - data - contains the data to insert.
----------------------------------------------------------------------------
NULL
============================================================================
*/
	public function update_business_unit( $data = NULL )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('BUSUNIT_CDE', $data['BUSUNIT_CDE']);
			$results = $this->oracle_db->update('HRM_BUSUNIT', $data);

			return $results;
		}
	}

/*           
============================================================================
delete_business_unit	
----------------------------------------------------------------------------
integer - id - contains the id to delete.
----------------------------------------------------------------------------
boolean - On whether or not the query was successful.
============================================================================
*/
	public function delete_business_unit( $id = NULL )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->query("DELETE FROM HRM_BUSUNIT WHERE BUSUNIT_CDE = '".$id."'");
			return $results;
		}
	}
}

/* End of file business_unit_update_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/business_unit_update_model.php */
