
 <div class="row">
	<div class="col-md-12">
		<h2>Add Employee</h2>
		<p class="lead">This form will allow you to update an employee.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?php echo base_url(); ?>TPW/add_employee/" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="EMPL_ID">Employee ID</label>
						<input type="text" name="EMPL_ID" class="form-control" title="Employee ID" maxlength="10" value="<?php echo set_value('EMPL_ID'); ?>"   />
					</div>
					<div class="col-sm-3">
						<label for="FIRST_NME">First Name</label><span class="text-danger">*</span>
						<input type="text" name="FIRST_NME" class="form-control" title="First Name" maxlength="40"  value="<?php echo set_value('FIRST_NME'); ?>" />
					</div>

					<div class="col-sm-3">
						<label for="MIDDLE_NME">Middle Initial</label><span class="text-danger">*</span>
						<input type="text" name="MIDDLE_NME" class="form-control" title="Middle Name" maxlength="3"  value="<?php echo set_value('MIDDLE_NME'); ?>" />
					</div>

					<div class="col-sm-3">
						<label for="LAST_NME">Last Name</label><span class="text-danger">*</span>
						<input type="text" name="LAST_NME" class="form-control" title="Last Name" maxlength="40"  value="<?php echo set_value('LAST_NME'); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="POSITION_NME">Position Name</label>
						<input type="text" name="POSITION_NME" class="form-control" title="Position Name" maxlength="40"  value="<?php echo set_value('POSITION_NME'); ?>" />
					</div>

					<div class="col-sm-4">
						<label for="SECTION_NME">Section Name</label>
						<input type="text" name="SECTION_NME" class="form-control" title="Section Name" maxlength="40"  value="<?php echo set_value('SECTION_NME'); ?>" />
					</div>

					<div class="col-sm-4">
						<label for="AREA_CDE">Area/District</label>
						<input type="text" name="AREA_CDE" class="form-control" title="Area/District" maxlength="10"  value="<?php echo set_value('AREA_CDE'); ?>" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
					<button type="button" value="course" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Supervisor</button>
				</div>
					<div class="col-sm-2">
						<label for="SUPER_ID">Supervisor ID</label>
						<input type="text" name="SUPER_ID" class="form-control SUPER_ID" title="Supervisor ID" maxlength="40" value="<?php echo set_value('SUPER_ID'); ?>" disabled  />
						<input type="hidden" name="SUPER_ID" class="form-control SUPER_ID" value="<?php echo set_value('SUPER_ID'); ?>"  />
					</div>

					<div class="col-sm-3">
						<label for="SUPER_NAME">Supervisor Name</label>
						<input type="text" name="SUPER_NAME" class="form-control SUPER_NAME" title="Supervisor Name" maxlength="40" value=""  disabled />
					</div>

					<div class="col-sm-4">
						<label for="POSITION_NME">Supervisor Position</label> 
						<input type="text" name="POSITION_NME" class="form-control POSITION_NME" title="Supervisor Position" maxlength="10" value="" disabled />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="DRIVELIC_FLG">Drivers Licence on File?</label>
						<select class="form-control" name="DRIVELIC_FLG">
							<option value="">Select..</option>
							<option value="Y" >Yes</option>
							<option value="N" >No</option>
						</select>
					</div>
					<div class="col-sm-3">
						<label for="DRIVEABS_FLG">Drivers abstract on File?</label>
						<select class="form-control" name="DRIVEABS_FLG">
							<option value="">Select..</option>
							<option value="Y" >Yes</option>
							<option value="N" >No</option>
						</select>
					</div>
					<div class="col-sm-2">
						<label for="CLASS_CDE">Class</label>
						<select class="form-control" name="CLASS_CDE">
							<option value="">Select..</option>
							<option value="1" <?php echo set_select('CLASS_CDE', '1'); ?> >1</option>
							<option value="2" <?php echo set_select('CLASS_CDE', '2'); ?> >2</option>
							<option value="3" <?php echo set_select('CLASS_CDE', '3'); ?> >3</option>
							<option value="4" <?php echo set_select('CLASS_CDE', '4'); ?> >4</option>
							<option value="5" <?php echo set_select('CLASS_CDE', '5'); ?> >5</option>
							<option value="6" <?php echo set_select('CLASS_CDE', '6'); ?> >6</option>
							<option value="7" <?php echo set_select('CLASS_CDE', '7'); ?> >7</option>
							<option value="8" <?php echo set_select('CLASS_CDE', '8'); ?> >8</option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="ENDORSEMENT_CDE">Endorsement Code</label>
						<select class="form-control" name="ENDORSEMENT_CDE" >
							<option value="">Select..</option>
							<?php foreach ($endorsements as $endorsement): ?>
								<option value="<?= $endorsement['ENDORSEMENT_CDE'] ?>" ><?= $endorsement['ENDORSEMENT_CDE'] .' - '. $endorsement['ENDORSEMENT_DSC'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-sm-4">
						<label for="CONDITION_CDE">Condition Code</label>
						<select class="form-control" name="CONDITION_CDE" >
							<option value="">Select..</option>
							<?php foreach ($conditions as $condition): ?>
								<option value="<?= $condition['CONDITION_CDE'] ?>" ><?= $condition['CONDITION_CDE'] .' - '. $condition['CONDITION_DSC'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12"> 
						<input type="submit" class="btn btn-md btn-primary update_course pull-right add_employee" value="Add Employee" />
						<a href="<?php echo base_url(); ?>TPW/employees" class="btn btn-md btn-default return">Back to Employees</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Supervisor Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-3">
							<div class="modal_search">
								<label for="FIRST_NME">First Name</label>
								<input type="text" name="FIRST_NME" id="" class="form-control supervisor_first_search_box" title="Supervisor First Name" autofocus="true" maxlength="" autocomplete="off" />
							</div>
						</div>
						<div class="col-sm-3">
							<div class="modal_search">
								<label for="LAST_NME">Last Name</label>
								<input type="text" name="LAST_NME" id="" class="form-control supervisor_last_search_box" title="Supervisor Last Name"  maxlength="" autocomplete="off" />
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Employee</th>
						<th>Position</th>
					</thead>
					<tbody class="supervisor_modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->