<?php
// This is the list of apps that are contained in the site,
// and is a common list among each header so we write the list once
// and just point to it in the header files.
?>

<?php
$groups = $this->session->userdata('groups');

$app_list['employee_directory'] = '<li><a href="'.base_url().'phonebook">Employee Directory</a></li>';
$app_list['BU_update'] = '<li class="disabled"><a href="">Business Unit Update</a></li>';
$app_list['cemetery'] = '<li class="disabled"><a href="">Cemetery System</a></li>';
$app_list['EDL'] = '<li class="disabled"><a href="">Engineering Drawing Location (EDL) System</a></li>';
$app_list['HRIS_ETT'] = '<li class="disabled"><a href="">HRIS Employee Training Tracking</a></li>';
$app_list['MCT'] = '<li class="disabled"><a href="">Mayor Correspondence Tracking System</a></li>';
$app_list['street_file'] = '<li class="disabled"><a href="">Street File Update </a></li>';
$app_list['SOT'] = '<li class="disabled"><a href="">Summary Offense Tickets System</a></li>';
$app_list['TPW'] = '<li class="disabled"><a href="">TPW Employee Training Tracking</a></li>';
$app_list['traffic'] = '<li class="disabled"><a href="">Traffic Volume Database</a></li>';

foreach ($groups as $group)
{
    switch ($group)
    {
        case 'AppHRMAppsBusinessUnitUpdate':
            $app_list['BU_update'] = '<li><a href="'.base_url().'BU_update">Business Unit Update</a></li>';
            break;
        case 'AppHRMAppsCemetery':
            $app_list['cemetery'] = '<li><a href="'.base_url().'cemetery/owners">Cemetery System</a></li>';
            break;
        case 'AppHRMAppsEngineeringDrawingLocationReadOnly':
            $app_list['EDL'] = '<li><a href="'.base_url().'EDL">Engineering Drawing Location (EDL) System</a></li>';
            break;
        case 'AppHRMAppsEngineeringDrawingLocationReadWrite':
            $app_list['EDL'] = '<li><a href="'.base_url().'EDL">Engineering Drawing Location (EDL) System</a></li>';
            break;
        case 'AppHRMAppsEngineeringDrawingLocationAdmin':
            $app_list['EDL'] = '<li><a href="'.base_url().'EDL">Engineering Drawing Location (EDL) System</a></li>';
            break;
        case 'AppHRMAppsHRISTraining':
            $app_list['HRIS_ETT'] = '<li><a href="'.base_url().'HRIS_ETT">HRIS Employee Training Tracking</a></li>';
            break;
        case 'AppHRMAppsMayorCallTracking':
            $app_list['MCT'] = '<li><a href="'.base_url().'MCT/add">Mayor Correspondence Tracking System</a></li>';
            break;
        case 'AppHRMAppsStreetFileUpdate':
            $app_list['street_file'] = '<li><a href="'.base_url().'street_file">Street File Update </a></li>';
            break;
        case 'AppHRMAppsSummaryOffenseTickets':
            $app_list['SOT'] = '<li><a href="'.base_url().'SOT">Summary Offense Tickets System</a></li>';
            break;
        case 'AppHRMAppsTPWTraining':
            $app_list['TPW'] = '<li><a href="'.base_url().'TPW">TPW Employee Training Tracking</a></li>';
            break;
        case 'AppHRMAppsTrafficVolumes':
            $app_list['traffic'] = '<li><a href="'.base_url().'traffic">Traffic Volume Database</a></li>';
            break;
        case 'AppHRMAppsAdmin':
            $app_list['BU_update'] = '<li><a href="'.base_url().'BU_update">Business Unit Update</a></li>';
            $app_list['cemetery'] = '<li><a href="'.base_url().'cemetery/owners">Cemetery System</a></li>';
            $app_list['EDL'] = '<li><a href="'.base_url().'EDL">Engineering Drawing Location (EDL) System</a></li>';
            $app_list['HRIS_ETT'] = '<li><a href="'.base_url().'HRIS_ETT">HRIS Employee Training Tracking</a></li>';
            $app_list['MCT'] = '<li><a href="'.base_url().'MCT/add">Mayor Correspondence Tracking System</a></li>';
            $app_list['street_file'] = '<li><a href="'.base_url().'street_file">Street File Update </a></li>';
            $app_list['SOT'] = '<li><a href="'.base_url().'SOT">Summary Offense Tickets System</a></li>';
            $app_list['TPW'] = '<li><a href="'.base_url().'TPW">TPW Employee Training Tracking</a></li>';
            $app_list['traffic'] = '<li><a href="'.base_url().'traffic">Traffic Volume Database</a></li>';
            break;
        default:
            # code...
            break;
    }
}
?>

<ul class="dropdown-menu">

    <?php

    foreach ($app_list as $key => $value)
    {
        echo $value;
    }

    // 	$disabled_app_item = '<li class="disabled" title="Contact the ICT Service Desk if you require access to this application."><a href="">';

    // 	if (!empty($groups))
    // 	{
    // 		foreach ($groups as $group)
    // 		{

    // 			// Business Unit Update
    // 			if($group == 'AppHRMAppsBusinessUnitUpdate' || $group == 'AppHRMAppsAdmin')	echo '<li><a href="'.base_url().'BU_update">';
    // 			else echo $disabled_app_item;
    // 			echo 'Business Unit Update</a></li>';

    // 			// Cemetery System
    // 			if($group == 'AppHRMAppsCemetery' || $group == 'AppHRMAppsAdmin') echo '<li><a href="'.base_url().'cemetery">';
    // 			else echo $disabled_app_item;
    // 			echo 'Cemetery System</a></li>';

    // 			// Employee Directory
    // 			echo '<li><a href="'.base_url().'phonebook">Employee Directory</a></li>';

    // 			// Engineering Drawing Location (EDL) System
    // 			if(
    // 				$group == 'AppHRMAppsEngineeringDrawingLocationReadOnly' ||
    // 				$group == 'AppHRMAppsEngineeringDrawingLocationReadWrite' ||
    // 				$group == 'AppHRMAppsEngineeringDrawingLocationAdmin'  ||
    // 				$group == 'AppHRMAppsAdmin'
    // 				)
    // 				echo '<li><a href="'.base_url().'EDL">';
    // 			else echo $disabled_app_item;
    // 			echo 'Engineering Drawing Location (EDL) System</a></li>';

    // 			// HRIS Employee Training Tracking
    // 			if($group == 'AppHRMAppsHRISTraining' || $group == 'AppHRMAppsAdmin')
    // 				echo '<li><a href="'.base_url().'HRIS_ETT">';
    // 			else echo $disabled_app_item;
    // 			echo 'HRIS Employee Training Tracking</a></li>';

    // 			// Mayor Call Tracking System
    // 			if($group == 'AppHRMAppsMayorCallTracking' || $group == 'AppHRMAppsAdmin')
    // 				echo '<li><a href="'.base_url().'MCT">';
    // 			else echo $disabled_app_item;
    // 			echo 'Mayor Call Tracking System</a></li>';

    // 			// Street File Update
    // 			if($group == 'AppHRMAppsStreetFileUpdate' || $group == 'AppHRMAppsAdmin')
    // 				echo '<li><a href="'.base_url().'street_file">';
    // 			else echo $disabled_app_item;
    // 			echo 'Street File Update</a></li>';

    // 			// Summary Offense Tickets System
    // 			if($group == 'AppHRMAppsSummaryOffenseTickets' || $group == 'AppHRMAppsAdmin')
    // 				echo '<li><a href="'.base_url().'SOT">';
    // 			else echo $disabled_app_item;
    // 			echo 'Summary Offense Tickets System</a></li>';

    // 			// TPW Employee Training Tracking
    // 			if($group == 'AppHRMAppsTPWTraining' || $group == 'AppHRMAppsAdmin')
    // 				echo '<li><a href="'.base_url().'TPW">';
    // 			else echo $disabled_app_item;
    // 			echo 'TPW Employee Training Tracking</a></li>';

    // 			// Traffic Volume Database
    // 			if($group == 'AppHRMAppsTrafficVolumes' || $group == 'AppHRMAppsAdmin')
    // 				echo '<li><a href="'.base_url().'traffic">';
    // 			else echo $disabled_app_item;
    // 			echo 'Traffic Volume Database</a></li>';

    // 		}
    // 	}
    ?>



</ul>