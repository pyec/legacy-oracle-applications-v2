<div class="row">
	<div class="col-6-md">
		<h2>Add Plan Index</h2>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
			<div class="alert alert-success alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
				<p><?php echo $this->session->flashdata('message'); ?></p>
			</div>
		<?php elseif(validation_errors()) : ?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
				<p><?php echo validation_errors()?></p>
			</div>
		<?php endif ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>EDL/add_plan_index" method="POST">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="PLAN_NUM">Plan #</label>
						<input type="text" name="PLAN_NUM" class="form-control" title="Plan Number" maxlength="15" value="<?php echo set_value('PLAN_NUM') ?>"  />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="street" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select</button>
					</div>
					<div class="col-sm-3">
						<label for="STR_NAME">Street Name</label>
						<input type="text" name="STR_NAME" class="form-control" title="Street Name" readonly value="<?php echo set_value('STR_NAME') ?>"  />
					</div>
					<div class="col-sm-1">
						<input type="text" name="STR_TYPE" class="form-control no_label_fix" title="Street Type" readonly value="<?php echo set_value('STR_TYPE') ?>"  />
					</div>
					<div class="col-sm-1">
						<input type="text" name="STR_NUMBER" class="form-control no_label_fix" title="Street Number" readonly value="<?php echo set_value('STR_NUMBER') ?>"  />
					</div>
					<div class="col-sm-2">
						<label for="STR_SIDE">Side of Street</label>
						<input type="text" name="STR_SIDE" class="form-control" title="Side of Street" maxlength="11" value="<?php echo set_value('STR_SIDE') ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="limit_from" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select</button>
					</div>
					<div class="col-sm-3">
						<label for="LIMIT_FROM">Limit From</label>
						<input type="text" name="LIMIT_FROM" class="form-control" title="Limit From Description" readonly value="<?php echo set_value('LIMIT_FROM') ?>" />
					</div>
					<div class="col-sm-1">
						<input type="text" name="LIM_FRM_T" class="form-control no_label_fix" title="Limit From Abbriviation" readonly value="<?php echo set_value('LIM_FRM_T') ?>" />
						<input type="hidden" name="L_F_NUMBER" value="" />
					</div>
					<div class="col-sm-3">
						<label for="plan_date">Plan Date</label>
						<input type="text" name="plan_date" class="form-control date_picker" title="Plan Date" readonly value="<?php echo set_value('plan_date') ?>" />
					</div>
					<div class="col-sm-3">
						<label for="altered_date">Altered Date</label>
						<input type="text" name="altered_date" class="form-control date_picker" title="Altered Date" readonly value="<?php echo set_value('altered_date') ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="limit_to" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select</button>
					</div>
					<div class="col-sm-3">
						<label for="LIMIT_TO">Limit To</label>
						<input type="text" name="LIMIT_TO" class="form-control " title="Limit To Description" readonly value="<?php echo set_value('LIMIT_TO') ?>" />
					</div>
					<div class="col-sm-1">
						<input type="text" name="LIM_TO_T" class="form-control no_label_fix" title="Limit To Abbriviation" readonly  value="<?php echo set_value('LIM_TO_T') ?>"/>
						<input type="hidden" name="L_T_NUMBER" value="" />
					</div>
					<div class="col-sm-4">
						<label for="SUB_NUMBER">Subject</label>
						<select name="SUB_NUMBER" class="form-control" title="Subject">
							<option value="">Select..</option>
							<?php foreach ($subjects as $sub): ?>
								<?php if ($plan[0]['SUB_NUMBER'] == $sub['SUBJ_NUM']): ?>
									<option selected value="<?= $sub['SUBJ_NUM'].','.$sub['SUBJ_NAME'] ?>"><?= $sub['SUBJ_NUM'].' - '.$sub['SUBJ_NAME'] ?></option>
							 	<?php else : ?>
									<option value="<?= $sub['SUBJ_NUM'].','.$sub['SUBJ_NAME'] ?>"><?= $sub['SUBJ_NUM'].' - '.$sub['SUBJ_NAME'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="PARCEL">Parcel</label>
						<input type="text" name="PARCEL" class="form-control" title="Parcel" maxlength="6" value="<?php echo set_value('PARCEL') ?>" />
					</div>
					<div class="col-sm-4">
						<label for="CIVIC_NUM">Civic #</label>
						<input type="text" name="CIVIC_NUM" class="form-control" title="Civic Number" maxlength="21" value="<?php echo set_value('CIVIC_NUM') ?>" />
					</div>
					<div class="col-sm-4">
						<label for="SURVEY_BY">Survey By</label>
						<input type="text" name="SURVEY_BY" class="form-control" title="Survey By" maxlength="21" value="<?php echo set_value('SURVEY_BY') ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-5">
						<label for="S_DIV_NUM">Subdivision</label>
						<select name="S_DIV_NUM" class="form-control" title="Subdivision">
							<option value="">Select...</option>
							<?php foreach ($subdivision as $subd): ?>
								<?php if ($plan[0]['S_DIV_NUM'] == $subd['SD_NUM']): ?>
								<option selected value="<?= $subd['SD_NUM'] ?>"><?= $subd['SD_NAME'] ?></option>
								<?php else : ?>
								<option value="<?= $subd['SD_NUM'] ?>"><?= $subd['SD_NUM'].' - '.$subd['SD_NAME'] ?></option>	
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
					
					<div class="col-sm-2">
						<label for="BLOCK_NUM">Block #</label>
						<input type="text" name="BLOCK_NUM" class="form-control" title="Block Number" maxlength="11" value="<?php echo set_value('BLOCK_NUM') ?>" />
					</div>
					<div class="col-sm-2">
						<label for="LOT_NUMBER">Lot #</label>
						<input type="text" name="LOT_NUMBER" class="form-control" title="Lot Number" maxlength="12" value="<?php echo set_value('LOT_NUMBER') ?>" />
					</div>
					<div class="col-sm-2">
						<label for="ISSING" class="no_label_fix">Missing</label>
						<input type="checkbox" name="ISSING" class="" title="Missing" value="<?php echo set_checkbox('ISSING', 'on') ?>" />
					</div>
				</div>
			</div>

			<legend>Options</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label for="REC" title="Recreation">Rec</label>
						<input type="checkbox" name="REC" title="Recreation"  />
					</div>
					<div class="col-sm-1">
						<label for="AS_BUILTS" title="As Built">As Blt</label>
						<input type="checkbox" name="AS_BUILTS" title="As Built"  />
					</div>
					<div class="col-sm-1">
						<label for="PLAN_PROF" title="Plan Prof">PP</label>
						<input type="checkbox" name="PLAN_PROF" title="Plan Prof"  />
					</div>
					<div class="col-sm-1">
						<label for="EX_PRO_COD" title="Ex Pro Cod">ExPC</label>
						<input type="checkbox" name="EX_PRO_COD" title="Ex Pro Cod"  />
					</div>
					<div class="col-sm-1">
						<label for="SUBD">Subd</label>
						<input type="checkbox" name="SUBD" title=""  />
					</div>
					<div class="col-sm-1">
						<label for="REC_DRAWS" title="Rec Draws">Rec D</label>
						<input type="checkbox" name="REC_DRAWS" title="Rec Draws"  />
					</div>
					<div class="col-sm-1">
						<label for="CONV" title="Conv">Conv</label>
						<input type="checkbox" name="CONV" title="Convo"  />
					</div>
					<div class="col-sm-1" title="Ease">
						<label for="EASE">Ease</label>
						<input type="checkbox" name="EASE" title="Ease"  />
					</div>
					<div class="col-sm-1">
						<label for="AQUIRED" title="Aquired">Aqu.</label>
						<input type="checkbox" name="AQUIRED" title="Aquired"  />
					</div>
					<div class="col-sm-1">
						<label for="ST_LINE" title="St Line">St L</label>
						<input type="checkbox" name="ST_LINE" title="St Line"  />
					</div>
					<div class="col-sm-1">
						<label for="R_O_W" title="Row">ROW</label>
						<input type="checkbox" name="R_O_W" title="Row"  />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label for="ST_CLOSURE" title="street closure">St C</label>
						<input type="checkbox" name="ST_CLOSURE" title="St Closure"  />
					</div>
					<div class="col-sm-1">
						<label for="CONSOL" title="consol">Cons.</label>
						<input type="checkbox" name="CONSOL" title="consol"  />
					</div>
					<div class="col-sm-1">
						<label for="ZONING" title="Zoning">Zon.</label>
						<input type="checkbox" name="ZONING" title="Zoning"  />
					</div>
					<div class="col-sm-1">
						<label for="GAS" title="Gas">Gas</label>
						<input type="checkbox" name="GAS" title="Gas"  />
					</div>
					<div class="col-sm-1">
						<label for="LOCAL_IMP" title="Local Imp">Loc I</label>
						<input type="checkbox" name="LOCAL_IMP" title="Local Imp"  />
					</div>
					<div class="col-sm-1">
						<label for="NSPC">NSPC</label>
						<input type="checkbox" name="NSPC" title=""  />
					</div>
					<div class="col-sm-1">
						<label for="PSC">PSC</label>
						<input type="checkbox" name="PSC" title=""  />
					</div>
					<div class="col-sm-1">
						<label for="EX_PRO" title="Ex Pro">ExPro</label>
						<input type="checkbox" name="EX_PRO" title="Ex Pro"  />
					</div>
					<div class="col-sm-1">
						<label for="MT_T">Mt T</label>
						<input type="checkbox" name="MT_T" title=""  />
					</div>
					<div class="col-sm-1">
						<label for="REZONING" title="Rezoning">Rez</label>
						<input type="checkbox" name="REZONING" title="Rezoning"  />
					</div>
					<div class="col-sm-1">
						<label for="F_A" title="FA">FA</label>
						<input type="checkbox" name="F_A" title="FA"  />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label for="PAVING" title="Paving">Pav.</label>
						<input type="checkbox" name="PAVING" title="Paving"  />
					</div>
					<div class="col-sm-1">
						<label for="STAND_DRAW" title="Stand Draw">SD</label>
						<input type="checkbox" name="STAND_DRAW" title="Stand Draw"  />
					</div>
					<div class="col-sm-1">
						<label for="SEWERS" title="Sewers">Sew.</label>
						<input type="checkbox" name="SEWERS" title="Sewers"  />
					</div>
					<div class="col-sm-1">
						<label for="S_W" title="SW">SW</label>
						<input type="checkbox" name="S_W" title="SW"  />
					</div>
					<div class="col-sm-1">
						<label for="STIL" title="Still">Still</label>
						<input type="checkbox" name="STIL" title="Still"  />
					</div>
					<div class="col-sm-1">
						<label for="T_L" title="TL">TL</label>
						<input type="checkbox" name="T_L" title="TL"  />
					</div>
					<div class="col-sm-1">
						<label for="R_WALL">R W</label>
						<input type="checkbox" name="R_WALL" title="R W"  />
					</div>
					<div class="col-sm-1">
						<label for="CTV">CTV</label>
						<input type="checkbox" name="CTV" title="CTV"  />
					</div>
					<div class="col-sm-1">
						<label for="LIGHTS">Lights</label>
						<input type="checkbox" name="LIGHTS" title="Lights"  />
					</div>
					<div class="col-sm-1">
						<label for="C_G">CG</label>
						<input type="checkbox" name="C_G" title="CG"  />
					</div>
					<div class="col-sm-1">
						<label for="XSECTIONS">X Sec</label>
						<input type="checkbox" name="XSECTIONS" title="Cross Section"  />
					</div>
					<div class="col-sm-2">
						<label for="PROFILE">Profile</label>
						<input type="checkbox" name="PROFILE" title="Profile"  />
					</div>
					<div class="col-sm-1">
						<label for="PLAN">Plan</label>
						<input type="checkbox" name="PLAN" title="Plan"  />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="CHECKED_BY">Checked By</label>
						<input type="text" name="CHECKED_BY" class="form-control" title="Checked By" maxlength="12" value="<?php echo set_value('CHECKED_BY') ?>" />
					</div>
					<div class="col-sm-3">
						<label for="CHECK_DATE">Check Date</label>
						<input type="text" name="CHECK_DATE" class="form-control date_picker" title="Check Date" readonly maxlength="12" value="<?php echo set_value('CHECK_DATE') ?>" />
					</div>
					<div class="col-sm-3">
						<label for="ENTERED_BY">Entered By</label>
						<input type="text" name="ENTERED_BY" class="form-control" title="Entered By" maxlength="12" value="<?php echo set_value('ENTERED_BY') ?>" />
					</div>
					<div class="col-sm-3">
						<label for="ENTER_DATE">Enter Date</label>
						<input type="text" name="ENTER_DATE" class="form-control date_picker" title="Enter Date" readonly maxlength="12" value="<?php echo set_value('ENTER_DATE') ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="DESC1">Description</label>
						<textarea name="DESC1" class="form-control" title="Description" maxlength="62" ></textarea>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-primary btn-md no_label_fix pull-right" value="submit" />
						<a href="<?php echo base_url(); ?>EDL/plan_index_query" class="btn btn-default btn-md no_label_fix">Back to PIQ</a>
					</div>
				</div>
			</div>
		</div>
		</form>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title">Street Look Up</h4>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="col-sm-5">
									<div class="modal_search">
										<label for="search_box">Street Name</label>
										<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
										<p class="text-muted">(Select the name of the record you would like to use.)</p>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<table class="table table-bordered">
							<thead>
								<th>Name</th>
							</thead>
							<tbody class="modal_tbody">
								<tr>
									
								</tr>
							</tbody>	
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
	</div>
</div>




