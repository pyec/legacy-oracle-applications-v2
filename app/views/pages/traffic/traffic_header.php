
<nav class="navbar navbar-default navbar-fixed-top app_nav" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="nav_items">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle app_header" data-toggle="collapse" data-target=".navbar-ex2-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php echo base_url(); ?>traffic" class="navbar-brand">Traffic Volume Database</a>
			</div>
		
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex2-collapse">
				<?php if ($this->uri->segment(2) == "TSR"): ?>
					<ul class="nav navbar-nav">
						<!-- <li><a href="<?php echo base_url(); ?>traffic/TSR_file_attachments">File Attachments</a></li> -->
					</ul>
				<?php elseif($this->uri->segment(2) == "TSR_file_attachments"): ?>
					<ul class="nav navbar-nav">
						<!-- <li><a href="<?php echo base_url(); ?>traffic/TSR">traffic Survey Requests</a></li> -->
					</ul>
				<?php endif ?>
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Surveys <b class="caret"></b></a>
						<!-- Here is where we include or app list resource. -->
						<ul class="dropdown-menu">
							<li><a href="<?php echo base_url(); ?>traffic/ATR">24 Hour Automated Traffic Recorder Studies</a></li>
							<li><a href="<?php echo base_url(); ?>traffic/LTI">Left Turn Investigation</a></li>
							<li><a href="<?php echo base_url(); ?>traffic/MTMTC">Manual Turning Movement Traffic Counts</a></li>
							<li><a href="<?php echo base_url(); ?>traffic/PS">Pedestrian Studies</a></li>
							<li><a href="<?php echo base_url(); ?>traffic/RSS">Radar Spot Speed Studies</a></li>
							<li><a href="<?php echo base_url(); ?>traffic/TSW">Traffic Signal Warrants</a></li>
							<li><a href="<?php echo base_url(); ?>traffic/TSR">Traffic Survey Requests</a></li>
							<li><a href="<?php echo base_url(); ?>traffic/TR">Traffic Regulations</a></li>
						</ul>
					</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</div>
</nav>


