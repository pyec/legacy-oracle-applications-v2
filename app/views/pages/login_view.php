
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sign in</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= base_url() ?>assets/css/signin.css" rel="stylesheet">

</head>

<body>
<div class="container">

    <form action="<?= base_url() ?>login/validate" method="POST" class="form-signin">

        <h1 class="text-center">HRM Applications</h1>

        <?php if ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger alert-dismissable">
                <p><?= $this->session->flashdata('error'); ?></p>
            </div>
        <?php endif ?>

        <label for="user_name" style="padding-top:5px;">Username</label>
        <input type="text" class="form-control" placeholder="Username (eg. smithj)" name="user_name" autofocus tabindex="1" autocomplete="off" />
        <label for="Password" style="padding-top:5px;">Password</label>
        <input type="password" class="form-control" placeholder="Password" name="password" tabindex="2" />
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>

        <br>

        <div class="alert alert-info">
            <p><strong>Application access has changed</strong><br>You now login using your HRM Active Directory credentials. For assistance please contact the <a href="mailto:ictsd@halifax.ca?subject=HRM Applications (https://apps.halifax.ca/HRMApplications)">ICT Service Desk</a>.</p>
        </div>

    </form>

</div> <!-- /container -->
</body>
</html>
