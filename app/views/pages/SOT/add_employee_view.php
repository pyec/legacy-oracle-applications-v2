<div class="row">
	<div class="col-md-6">
		<h2>Add Officer Information</h2>
		<p>Select a officer to view more information about them.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php elseif (validation_errors()): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo validation_errors(); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>SOT/add_employee" method="POST">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="EMPL_ID">Officer ID</label><span class="text-danger">*</span>
						<input type="text" name="EMPL_ID" class="form-control" title="Employee ID"  maxlength="10" value="<?php echo set_value('EMPL_ID'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="FIRST_NME">First Name</label><span class="text-danger">*</span>
						<input type="text" name="FIRST_NME" class="form-control" title="First Name"  maxlength="30" value="<?php echo set_value('FIRST_NME'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MIDDLE_NME">Middle Name</label>
						<input type="text" name="MIDDLE_NME" class="form-control" title="Middle Name"  maxlength="3" value="<?php echo set_value('MIDDLE_NME'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="LAST_NME">Last Name</label><span class="text-danger">*</span>
						<input type="text" name="LAST_NME" class="form-control" title="Last Name"  maxlength="30" value="<?php echo set_value('LAST_NME'); ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="AGENCY_NME">Agency</label>
						<input type="text" name="AGENCY_NME" class="form-control" title="Agencys"  maxlength="40" value="<?php echo set_value('AGENCY_NME'); ?>" />
					</div>
					<div class="col-sm-4">
						<label for="EMAIL_DSC">Email Address</label>
						<input type="email" name="EMAIL_DSC" class="form-control" title="Email Address"  maxlength="50" value="<?php echo set_value('EMAIL_DSC'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="WORK_LOC">Work Location</label>
						<input type="text" name="WORK_LOC" class="form-control" title="Work Location"  maxlength="15" value="<?php echo set_value('WORK_LOC'); ?>" />
					</div>
					<div class="col-sm-1">
						<label for="INACTIVE_FLG">Active?</label>
						<input type="checkbox" name="INACTIVE_FLG" class="" title="Active?" />
					</div>
				</div>
			</div>

			<legend>Phone Numbers</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="P-PHONE_NUM">Number</label>
						<input type="text" name="P-PHONE_NUM" class="form-control" title="Number"  maxlength="15" value="<?php echo set_value('P-PHONE_NUM'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="P-PHONE_FLG">Type (H-home, W-work)</label>
						<input type="text" name="P-PHONE_FLG" class="form-control" title="Type"  maxlength="1" value="<?php echo set_value('P-PHONE_FLG'); ?>" />
					</div>
					<div class="col-sm-1">
						<label for="P-PRIMARY_FLG">Primary?</label>
						<input type="checkbox" name="P-PRIMARY_FLG" title="Primary?"  />
					</div>
				</div>
			</div>

			<legend>Notes</legend>

			<div class="form-group">
				<div class="row">
					<textarea name="NOTES_DSC" class="form-control" title="Employee Notes" maxlength="1000" ></textarea>	
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<input type="submit" class="btn btn-md btn-primary pull-right" value="Add Officer" />
					<a href="<?php echo base_url(); ?>SOT/employee_information" class="btn btn-md btn-default">Back to Officers</a>
				</div>
			</div>
		</form>
	</div>
</div>