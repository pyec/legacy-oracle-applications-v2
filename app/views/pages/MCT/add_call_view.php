<div class="row">
	<div class="col-md-12">
		<h2>Add Correspondence</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<!-- <div class="row"> -->
	<div class="col=md-12">
		<form action="<?php echo base_url(); ?>MCT/add" method="post">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="MCT_SOURCE">Source</label><span class="text-danger">*</span>
						<select name="MCT_SOURCE" class="form-control" title="Source">
							<option value="">Source types here...</option>
							<?php foreach ($source_types as $source): ?>
								<option value="<?= $source['MCT_SOURCE_DESC'] ?>"><?= $source['MCT_SOURCE_DESC'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-sm-3">
						<label for="MCT_TYPE_DESC">Type</label><span class="text-danger">*</span>
						<select name ="MCT_TYPE_DESC" class="form-control" title="Type">
							<option value="">Call types here...</option>
							<?php foreach ($call_types as $type): ?>
								<option value="<?= $type['MCT_TYPE_DESC'] ?>"><?= $type['MCT_TYPE_DESC'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-sm-3">
						<label for="MCT_FST_NME">First Name</label>
						<input type="text" name="MCT_FST_NME"  class="form-control" title="First Name" maxlength="35" value="<?php echo set_value('MCT_FST_NME'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_LST_NME">Last Name</label><span class="text-danger">*</span>
						<input type="text" name="MCT_LST_NME" class="form-control" title="Last Name" maxlength="35" value="<?php echo set_value('MCT_LST_NME'); ?>" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="MCT_CVC_NUM">Civic #</label>
						<input type="text" name="MCT_CVC_NUM" class="form-control MCT_CVC_NUM" title="Civic Number" maxlength="5" value="<?php echo set_value('MCT_CVC_NUM'); ?>" />
					</div>
					<div class="col-sm-1">
						<button type="button" value="" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-4">
						<label for="STREET_CDE">Street</label>
						<input type="text"  class="form-control street_name" title="Street Code" maxlength="5" value="" disabled />
						<input type="hidden" name="STREET_CDE" class="form-control street_code" title="Street Code" value="<?php echo set_value('STREET_CDE'); ?>" />
					</div>
					<div class="col-sm-1">
						<label for="MCT_DISTRICT">District</label>
						<input type="text" name="MCT_DISTRICT" class="form-control MCT_DISTRICT" value="<?php echo set_value('MCT_DISTRICT'); ?>"  />
					</div>
					<div class="col-sm-1">
						<label for="MCT_CVC_EXT_CDE">Ext.</label>
						<input type="text" name="MCT_CVC_EXT_CDE" class="form-control" title="Extention" maxlength="3" value="<?php echo set_value('MCT_CVC_EXT_CDE'); ?>" />
					</div>
					
					<div class="col-sm-3">
						<label for="MCT_UNIT_DSC">Unit Desc.</label>
						<input type="text" name="MCT_UNIT_DSC" class="form-control" title="Unit Description" maxlength="5" value="<?php echo set_value('MCT_UNIT_DSC'); ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="MCT_UNIT_NUM">Unit #</label>
						<input type="text" name="MCT_UNIT_NUM" class="form-control" title="Unit Number" maxlength="5" value="<?php echo set_value('MCT_UNIT_NUM'); ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_POSTAL_CDE">Postal Code</label>
						<input type="text" name="MCT_POSTAL_CDE" class="form-control" title="Postal Code" maxlength="7" value="<?php echo set_value('MCT_POSTAL_CDE'); ?>" />
					</div>

					<div class="col-sm-2">
						<label for="MCT_FILE_NUM">File #</label>
						<input type="text" name="MCT_FILE_NUM" class="form-control" title="File Number" maxlength="15" value="<?php echo set_value('MCT_FILE_NUM'); ?>" />
					</div>

					<div class="col-sm-2">
						<label for="MCT_PRIORITY">Priority</label>
						<select name="MCT_PRIORITY" class="form-control" title="Priority">
							<option value="2" <?php echo set_select('MCT_PRIORITY', '2'); ?> >high</option>
							<option value="1" <?php echo set_select('MCT_PRIORITY', '1'); ?> >Med</option>
							<option value="0" <?php echo set_select('MCT_PRIORITY', '0'); ?> >Low</option>
						</select>
					</div>
					<div class="col-sm-2">
						<label for="MCT_ENTER_DTE">Entered On</label><span class="text-danger">*</span>
						<input type="text" name="MCT_ENTER_DTE" class="form-control date_picker" title="Entered On" maxlength="" readonly value="<?php echo set_value('MCT_ENTER_DTE'); ?>" />
					</div>
				</div>
			</div>
 

			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="MCT_HPHONE">Home Phone</label>
						<input type="text" name="MCT_HPHONE" class="form-control" title="Home Phone" maxlength="14" value="<?php echo set_value('MCT_HPHONE'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_WPHONE">Work Phone</label>
						<input type="text" name="MCT_WPHONE" class="form-control" title="Work Phone" maxlength="14" value="<?php echo set_value('MCT_WPHONE'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_CPHONE">Cell Phone</label>
						<input type="text" name="MCT_CPHONE" class="form-control" title="Cell Phone" maxlength="14" value="<?php echo set_value('MCT_CPHONE'); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_FPHONE">Fax</label>
						<input type="text" name="MCT_FPHONE" class="form-control" title="Fax" maxlength="14" value="<?php echo set_value('MCT_FPHONE'); ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="MCT_DESC">Description</label>
						<textarea name="MCT_DESC" cols="6" class="form-control mct_description" title="Description" maxlength="2000"><?php echo set_value() ?></textarea>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="MCT_REF_NME">Referred To</label>
						<input type="text" name="MCT_REF_NME" class="form-control" title="Referred To" maxlength="40" value="<?php echo set_value('MCT_REF_NME'); ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_REF_DPT">Department</label>
						<input type="text" name="MCT_REF_DPT" class="form-control" title="Department" maxlength="30" value="<?php echo set_value('MCT_REF_DPT'); ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_REF_PHONE">Phone</label>
						<input type="text" name="MCT_REF_PHONE" class="form-control" title="Phone" maxlength="14" value="<?php echo set_value('MCT_REF_PHONE'); ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_REF_DTE">Referred On</label>
						<input type="text" name="MCT_REF_DTE" class="form-control date_picker" title="Referred On" maxlength="" readonly value="<?php echo set_value('MCT_REF_DTE'); ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_DUE_DTE">Due On</label><span class="text-danger">*</span>
						<input type="text" name="MCT_DUE_DTE" class="form-control date_picker" title="Due On" maxlength="" readonly value="<?php echo set_value('MCT_DUE_DTE'); ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_RESOLVE_FLG">Resolved?</label>
						<select name="MCT_RESOLVE_FLG" class="form-control">
							<option value="N" <?php echo set_select('MCT_RESOLVE_FLG', 'N'); ?> >No</option>
							<option value="Y" <?php echo set_select('MCT_RESOLVE_FLG', 'Y'); ?> >Yes</option>
						</select>
					</div>
				</div>
			</div>

			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-md btn-primary pull-right" value="Add" />
						<a href="<?php echo base_url(); ?>MCT/open" class="btn btn-md btn-default">Back to Open Correspondence</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Street Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<div class="modal_search">
								<label for="MCT_search_box">Street Name</label>
								<input type="text" name="MCT_search_box" id="" class="form-control MCT_search_box" title="Street Name" autofocus maxlength="" />
								<p class="text-muted">(Select the name of the record you would like to use.)</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Name</th>
					</thead>
					<tbody class="modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->