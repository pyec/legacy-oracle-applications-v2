
		<nav class="navbar navbar-default navbar-fixed-top app_nav" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="nav_items">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle app_header" data-toggle="collapse" data-target=".navbar-ex2-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="<?php echo base_url(); ?>SOT" class="navbar-brand">SOT</a>
					</div>
				
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex2-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url(); ?>SOT/summary_offence_tickets">Summary Offence Tickets</a></li>
							<li><a href="<?php echo base_url(); ?>SOT/offence_bylaws">Bylaw Offences</a></li>
							<li><a href="<?php echo base_url(); ?>SOT/contact_information">Contact Info</a></li>
							<li><a href="<?php echo base_url(); ?>SOT/employee_information">Officer Info</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div>
			</div>
		</nav>

