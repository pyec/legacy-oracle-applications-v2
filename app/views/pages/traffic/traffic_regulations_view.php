<div class="row">
	<div class="col-md-12">
		<h2>Traffic Regulations</h2>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<!-- <div class="row"> -->
	<form action="<?php echo base_url(); ?>traffic/TR_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="record_number">Record #</label>
					<input type="text" name="record_number" class="form-control" title="Record Number" value="<?php echo set_value('record_number') ?>" tabindex="1" placeholder="Record Number" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="traffic_atr_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
	
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($TR_studies)): ?>
			<?php if (count($TR_studies)): ?>
				<h2 class="count"><?php echo count($TR_studies) ?> studies or more</h2>
			<?php else: ?>
				<h2 class="count"><?php echo count($TR_studies) ?> studies</h2>
			<?php endif ?>
		<?php endif ?>
	</div>
	<div class="col-sm-12" style="margin-top: 30px;">
		<?php if (!empty($TSR_studies) && count($TSR_studies) == 500): ?>
			<P class="">Refine your search for more specific results.</P>			
		<?php endif ?>
	</div>
</div>

	<div class="row">
		<div class="col-md-12">
			<a class="btn btn-success btn-md pull-right" href="<?php echo base_url(); ?>traffic/add_TR" >Add Survey</a>		
		</div>
	</div>

<?php if (!empty($TR_studies)): ?>
	<div class="row">
		<div class="col-sm-12">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<th>Regulation #</th>
					<th>Issue Date</th>
					<th>Completion Date</th>
					<th>Comments</th>
					<th>Delete</th>
				</thead>
				<tbody class="tbody">
						<?php foreach ($TR_studies as $study): ?>
							<tr>
								<td><a href="<?= base_url(); ?>traffic/update_TR/<?= $study['REG_NUM'] ?>"><?= $study['REG_NUM'] ?></a></td>
								<td><?php echo $study['ISSUE_DATE'] ?></td>
								<td><?php echo $study['C_DATE'] ?></td>
								<td><?php echo $study['COMMENTS'] ?></td>
								<td>
									<form action="<?php echo base_url(); ?>traffic/delete_TR" method="POST" >
										<button type="submit" class="btn btn-sm btn-danger delete_survey"><i class="fa fa-minus-square"></i></button>
										<input type="hidden" name="REG_NUM" value="<?= $study['REG_NUM'] ?>" />
									</form>
								</td>
							</tr>
						<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php endif ?>