<div class="row">
	<div class="col-md-12">
		<h2>Employee courses</h2>
		<p class="lead">Select an employee to view/update their course list.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
	<form action="<?= base_url(); ?>TPW/employee_course_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="EMPL_ID">Employee #</label>
					<input type="text" name="EMPL_ID" class="form-control" title="Employee Number" value="<?php echo set_value('EMPL_ID') ?>" tabindex="1" placeholder="Employee Number" />
				</div>
				<div class="col-sm-3">
					<label for="FIRST_NME">First Name</label>
					<input type="text" name="FIRST_NME" class="form-control" title="First Name" value="<?php echo set_value('FIRST_NME') ?>" tabindex="2" placeholder="First Name" />
				</div>
				<div class="col-sm-3">
					<label for="LAST_NME">Last Name</label>
					<input type="text" name="LAST_NME" class="form-control" title="Last Name" value="<?php echo set_value('LAST_NME') ?>" tabindex="3" placeholder="Last Name" />
				</div>
				<div class="col-sm-3">
					<label for="POSITION_NME">Position</label>
					<input type="text" name="POSITION_NME" class="form-control" title="Position" value="<?php echo set_value('POSITION_NME') ?>" tabindex="4" placeholder="Position" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right no_label_fix phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right no_label_fix" tabindex="9" onclick="pwt_eployees_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
<div class="row">
	<div class="form-group">
		<div class="col-md-12">
			<?php if (!empty($employees)): ?>
				<?php if (count($employees) == 500): ?>
					<h2 class="count"><?= count($employees) ?> employees or more</h2>
				<?php endif ?>
				<h2 class="count"><?= count($employees) ?> employees</h2>
			<?php endif ?>
		</div>
	</div>
</div>

<div class="row">
	<div class="form-group">
		<div class="col-sm-12" style="margin-top: 20px;">
			<?php if (!empty($employees) && count($employees) == 500): ?>
				<P class="">Refine your search for more specific results.</P>			
			<?php endif ?>
		</div>
	</div>
</div>


<?php if (!empty($employees)): ?>
<div class="row no_label_fix">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Employee ID</th>
				<th>Name</th>
				<th>Position</th>
			</thead>
			<tbody class="tbody">
					<?php foreach ($employees as $emp): ?>
						<tr>
							<td><a href="<?= base_url(); ?>TPW/view_employee/<?= $emp['EMPL_ID'] ?>" ><?= $emp['EMPL_ID'] ?></a></td>
							<td><?= ucwords(strtolower($emp['LAST_NME'].', '.$emp['FIRST_NME'].' '.$emp['MIDDLE_NME'])) ?></td>
							<td><?= ucwords(strtolower($emp['POSITION_NME'])) ?></td>
						</tr>
					<?php endforeach ?>
			</tbody>	
		</table>
	</div>
</div>
<?php endif ?>