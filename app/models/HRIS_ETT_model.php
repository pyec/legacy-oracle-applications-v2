<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class HRIS_ETT_model extends CI_Model 
{
/*========================================================================================================================
	   Name : HRIS Employee Training Tracking model

Description : This is the data layer for the HRIS ETT system.

  Functions : get_courses( $offset = 0 ),
			get_course_by_id( $id = null ),
			get_course_by_name( $name = null ),
			insert_course( $data = null ),
			update_course( $id = null ),
			remove_course( $id = null )
=========================================================================================================================*/
	//Properties
	public $table_courses = null;
	public $id_courses = null;
	public $name_courses = null;
	public $oracle_db = null; // The reference to the database

	//Constructor
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies

		$this->table_courses = 'COURSES';
		$this->id_courses = 'COURSE_NO';
		$this->name_courses = 'COURSE_TITLE';

		$this->table_course_dates = 'COURSE_DATE';

		$this->oracle_db = $this->load->database('oracle_HRIS', TRUE); // The reference to the database

        $this->oracle_db->query("alter session set nls_date_format = 'DD-MON-YYYY'");
	}

/*           
============================================================================
get_courses
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
boolean - false, array[string] - result set
============================================================================
*/

	public function get_courses()
	{
		$query = $this->oracle_db->get($this->table_courses);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function does_course_id_exist($id)
	{
		$this->oracle_db->where($this->id_courses, $id);
		$query = $this->oracle_db->get($this->table_courses);


		if ($query->num_rows() > 0)
		{ 
			return true;
		}
		else 
		{
			return false;
		}
	}

/*           
============================================================================
get_clouse_by_id
----------------------------------------------------------------------------
integer - $id - The id of the record that we want to 
----------------------------------------------------------------------------
boolean - false, array[string] - result set
============================================================================
*/

	public function get_course_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_courses, $id);
		$query = $this->oracle_db->get($this->table_courses);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function search_course_by_id( $id = null )
	{
		$this->oracle_db->like($this->id_courses, $id);
        $this->oracle_db->where('DELETED', 0);
		$query = $this->oracle_db->get($this->table_courses);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

/*           
============================================================================
get_course_by_name
----------------------------------------------------------------------------
string - $name - The name of the course that we want to look up
----------------------------------------------------------------------------
boolean - false, array[string] - result set
============================================================================
*/

	public function get_course_by_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->name_courses, $name);
            $this->oracle_db->where('DELETED', 0);
			$query = $this->oracle_db->get($this->table_courses);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

/*           
============================================================================
get_course_by_info
----------------------------------------------------------------------------
array[integer, string]  - $info - THe title and number of the course, used 
to look up a course by other fields simultaneous.
----------------------------------------------------------------------------
boolean - false, array[string] - result set
============================================================================
*/

	public function get_course_by_info($info)
	{
		if ($info == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->name_courses, $info['course_title']);
			$this->oracle_db->like($this->id_courses, $info['course_number']);
			$query = $this->oracle_db->get($this->table_courses);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

/*           
============================================================================
get_course_employees()
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
array - a list of empoyee records tied to 
============================================================================
*/

	public function get_course_employees($course_id, $schedule_id)
	{
		if ($course_id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('COURSE_NO', $course_id);
			$this->oracle_db->where('COURSE_SCHED', $schedule_id);
            $this->oracle_db->where('DELETED', 0);
			$query = $this->oracle_db->get('EMP_COURSE');

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	/*           
============================================================================
get_course_dates()
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
array - a list of empoyee records tied to 
============================================================================
*/

	public function get_course_dates($course_id)
	{
		if ($course_id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('COURSE_NO', $course_id);
            $this->oracle_db->where('DELETED', 0);
			$query = $this->oracle_db->get('COURSE_DATES');

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function get_course_schedule($course_id, $schedule_id)
	{
		if ($course_id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('COURSE_NO', $course_id);
			$this->oracle_db->where('COURSE_SCHED', $schedule_id);
			$query = $this->oracle_db->get('COURSE_DATES');

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

/*           
============================================================================
insert_course
----------------------------------------------------------------------------
array[integer, string] - $data - The array of data that will make up the row
that were inserting.	
----------------------------------------------------------------------------
boolean - false - if the param is null.
============================================================================
*/

	public function insert_course( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_courses, $data);
			return $results;
		}
	}

/*           
============================================================================
update_course
----------------------------------------------------------------------------
integer - $id - The key of the record that we want to update.
----------------------------------------------------------------------------
boolean - false - if the param is null.
============================================================================
*/

	public function update_course( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_courses, $data[$this->id_courses]);
			$results = $this->oracle_db->update($this->table_courses, $data);
			return $results;
		}
	}

/*           
============================================================================
remove_course
----------------------------------------------------------------------------
integer - $id - The key of the record that we want to remove from the table.
----------------------------------------------------------------------------
boolean - false - if the param is null.
============================================================================
*/

//	public function delete_course( $id = null )
//	{
//		if ($id == null)
//		{
//			return false;
//		}
//		else
//		{
//			$this->oracle_db->where($this->id_courses, $id);
//			$results = $this->oracle_db->delete($this->table_courses);
//			return $results;
//		}
//	}

    public function delete_course( $id = null )
    {
        if ($id == null)
        {
            return false;
        }
        else
        {
            $dataEmployee = array(
                "DELETED" => "1"
            );

            $dataCourse = array(
                "DELETED" => "1",
                "COURSE_NO" => $id
            );

            $this->oracle_db->trans_strict(TRUE);
            $this->oracle_db->trans_begin();

            $courseSchedules = $this->get_course_dates($id);

            foreach ($courseSchedules as $courseSchedule) {
                $courseEmployees = $this->get_course_employees($id, $courseSchedule['COURSE_SCHED']);

                foreach ($courseEmployees as $courseEmployee){
                    // Soft delete the record
                    $this->update_course_employee($id, $courseSchedule['COURSE_SCHED'], $courseEmployee['EMP_NO'], $dataEmployee);
                }

                $dataSchedule = array(
                    "DELETED" => "1",
                    "MODDTE" => date('y-m-d'),
                    "COURSE_NO" => $id,
                    "COURSE_SCHED" => $courseSchedule['COURSE_SCHED']
                );

                // Soft delete the course schedule
                $this->update_course_date($dataSchedule);
            }

            // finally soft delete the course
            $this->update_course($dataCourse);

            //make transaction complete
            $this->oracle_db->trans_complete();

            //check if transaction status TRUE or FALSE
            if ($this->oracle_db->trans_status() === FALSE) {
                //if something went wrong, rollback everything
                $this->oracle_db->trans_rollback();
                return FALSE;
            } else {
                //if everything went right, commit the data to the database
                $this->oracle_db->trans_commit();
                return TRUE;
            }
        }
    }

	/*           
============================================================================
insert_course_date
----------------------------------------------------------------------------
array[integer, string] - $data - The array of data that will make up the row
that were inserting.	
----------------------------------------------------------------------------
boolean - false - if the param is null.
============================================================================
*/

	public function insert_course_date( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
            $data['ADDDTE'] = "to_date('".$data['ADDDTE']."','YY-MM-dd')";
            $this->oracle_db->set("ADDDTE", $data['ADDDTE'], false);
            unset($data['ADDDTE']);

			$results = $this->oracle_db->insert('COURSE_DATES', $data);
			return $results;
		}
	}

	public function update_course_date($data = null)
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			//INC0057168
			// $data['COURSE_START'] = "to_date('".$data['COURSE_START']."','dd-MON-YY')";
			// $data['COURSE_END'] = "to_date('".$data['COURSE_END']."','dd-MON-YY')";
			$data['MODDTE'] = "to_date('".$data['MODDTE']."','YY-MM-dd')";
			//
			$this->oracle_db->set("MODDTE", $data['MODDTE'], false);
			unset($data['MODDTE']);
			$this->oracle_db->where('COURSE_NO', $data['COURSE_NO']);
			$this->oracle_db->where('COURSE_SCHED', $data['COURSE_SCHED']);
			$results = $this->oracle_db->update('COURSE_DATES', $data);
			return $results;
		}
	}

/*   
============================================================================
delete_course_date
----------------------------------------------------------------------------

----------------------------------------------------------------------------
boolean - false - if the param is null.
============================================================================
*/

//	public function delete_course_date( $number = null, $schedule = null )
//	{
//		if ($number == null || $schedule == null)
//		{
//			return false;
//		}
//		else
//		{
//			$this->oracle_db->where('COURSE_NO', $number);
//			$this->oracle_db->where('COURSE_SCHED', $schedule);
//
//			$results = $this->oracle_db->delete('COURSE_DATES');
//			return $results;
//		}
//	}

    public function delete_course_date( $number = null, $schedule = null )
    {
        if ($number == null || $schedule == null)
        {
            return false;
        }
        else
        {
            $dataEmployee = array(
                "DELETED" => "1"
            );

            $this->oracle_db->trans_strict(TRUE);
            $this->oracle_db->trans_begin();

            $courseEmployees = $this->get_course_employees($number, $schedule);

            foreach ($courseEmployees as $courseEmployee){
                // Soft delete the record
                $this->update_course_employee($number, $schedule, $courseEmployee['EMP_NO'], $dataEmployee);
            }

            $dataSchedule = array(
                "DELETED" => "1",
                "MODDTE" => date('y-m-d'),
                "COURSE_NO" => $number,
                "COURSE_SCHED" => $schedule
            );

            // Soft delete the course schedule
            $this->update_course_date($dataSchedule);


            //make transaction complete
            $this->oracle_db->trans_complete();

            //check if transaction status TRUE or FALSE
            if ($this->oracle_db->trans_status() === FALSE) {
                //if something went wrong, rollback everything
                $this->oracle_db->trans_rollback();
                return FALSE;
            } else {
                //if everything went right, commit the data to the database
                $this->oracle_db->trans_commit();
                return TRUE;
            }
        }
    }

	/*   
============================================================================
get_employees_by_id
----------------------------------------------------------------------------

----------------------------------------------------------------------------
boolean - false - if the param is null.
============================================================================
*/

	public function get_employees_by_id( $id = null )
	{
		if ($id == null )
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('EMP_NO', $id);
			$query = $this->oracle_db->get('EMP_TRN_MASTER');
			
			if ($query->num_rows() > 0)
			{ 
				return $query->row_array();
			}
			else 
			{
				return false;
			}
			
		}
	}

	/*   
============================================================================
get_like_employees_by_id
----------------------------------------------------------------------------

----------------------------------------------------------------------------
boolean - false - if the param is null.
============================================================================
*/

	public function get_like_employees_by_id( $id = null )
	{
		if ($id == null )
		{
			return false;
		}
		else
		{
			$this->oracle_db->like('EMP_NO', $id);
			$this->oracle_db->limit(25);
			$query = $this->oracle_db->get('EMP_TRN_MASTER');
			
			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}
			
		}
	}

	public function get_like_employees_by_first_name($lastName, $firstName)
	{
		if ($firstName == null )
		{
			return false;
		}
		else
		{
            $firstNameUpper = strtoupper($firstName);
            $lastNameUpper = strtoupper($lastName);

			$this->oracle_db->like('UPPER(GIVEN_NAME1)', $firstNameUpper);
            if ($lastNameUpper != null) {
                $this->oracle_db->like('UPPER(SURNAME)', $lastNameUpper);
            }

			$this->oracle_db->limit(25);
			$query = $this->oracle_db->get('EMP_TRN_MASTER');
			
			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}
			
		}
	}

	public function get_like_employees_by_last_name($lastName, $firstName)
	{
		if ($lastName == null )
		{
			return false;
		}
		else
		{
			$lastNameUpper = strtoupper($lastName);
			$firstNameUpper = strtoupper($firstName);

			$this->oracle_db->like('UPPER(SURNAME)', $lastNameUpper);
			if ($firstNameUpper != null) {
                $this->oracle_db->like('UPPER(GIVEN_NAME1)', $firstNameUpper);
            }
			$this->oracle_db->limit(25);
			$query = $this->oracle_db->get('EMP_TRN_MASTER');
			
			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}
			
		}
	}

	/*           
	============================================================================
	get_employee_position()
	----------------------------------------------------------------------------
	$employee_id
	----------------------------------------------------------------------------
	The employees posistion record.
	============================================================================
	*/
	public function get_employee_position( $id = null )
	{
		if ($id == null )
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('EMP_NO', $id);
			$query = $this->oracle_db->get('POS_EMP');
			
			if ($query->num_rows() > 0)
			{ 
				if ($query->num_rows() > 1)
				{ 

					$results = $query->result_array();
					$end = count($results) -1;

					for ($i=0; $i <= $end; $i++) 
					{ 
						$current_date = $results[$i]['DATE_POS_START'];

						if ($current_date > $results[$i]['DATE_POS_START'])
						{
							$final_results = $results[$i]; 
							continue;
						}
						else
						{
							$current_date = $results[$i++]['DATE_POS_START'];
							$final_results = $results[$i++];
						}
					}

					return $final_results;
				}
				else
				{
					return $query->row_array();
				}
			}
			else 
			{
				return false;
			}
			
		}
	}

	public function get_employee_division( $division_code = null )
	{
		if ($division_code == null )
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('DIVISION_CODE', $division_code);
			$query = $this->oracle_db->get('DIVISION');
			
			if ($query->num_rows() > 0)
			{
				$results = $query->row_array();
				return $results['DIVISION_NAME'];
			}
			else 
			{
				return false;
			}
			
		}
	}
	
	public function get_cost_center_name( $cc_code = null )
	{
		if ($cc_code == null )
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('CC_CODE', $cc_code);
			$query = $this->oracle_db->get('COST_CENTER');
			
			if ($query->num_rows() > 0)
			{ 
				$results = $query->row_array();

				return $results['CC_NAME'];
			}
			else 
			{
				return false;
			}		
		}
	}

	public function insert_course_employee($data)
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
            $data['ADDDTE'] = "to_date('".$data['ADDDTE']."','YY-MM-dd')";
            $this->oracle_db->set("ADDDTE", $data['ADDDTE'], false);
            unset($data['ADDDTE']);

			$results = $this->oracle_db->insert('EMP_COURSE', $data);
			return $results;
		}
	}

	public function update_course_employee($course_number, $schedule, $emp_no, $data)
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
		    if ($data['DATE_COMPLETED'] != null) {
                $data['DATE_COMPLETED'] = "to_date('" . $data['DATE_COMPLETED'] . "','YY-MM-dd')";
                $this->oracle_db->set("DATE_COMPLETED", $data['DATE_COMPLETED'], false);
                unset($data['DATE_COMPLETED']);
            }

            $data['MODDTE'] = "to_date('" . $data['MODDTE'] . "','YY-MM-dd')";
            $this->oracle_db->set("MODDTE", $data['MODDTE'], false);
            unset($data['MODDTE']);

			$this->oracle_db->where('EMP_NO', $emp_no);
			$this->oracle_db->where('COURSE_NO', $course_number);
			$this->oracle_db->where('COURSE_SCHED', $schedule);
			$results = $this->oracle_db->update('EMP_COURSE', $data);
			return $results;
		}
	}

	public function delete_course_employee($course_number, $schedule, $emp_no)
	{
		if ($course_number == null || $schedule == null || $emp_no == null)
		{
			return false;
		}
		else
		{

			$this->oracle_db->where('COURSE_NO', $course_number);
			$this->oracle_db->where('COURSE_SCHED', $schedule);
			$this->oracle_db->where('EMP_NO', $emp_no);

			$results = $this->oracle_db->delete('EMP_COURSE');
			return $results;
		}
	}

}

/* End of file HRIS_ETT_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/HRIS_ETT_model.php */