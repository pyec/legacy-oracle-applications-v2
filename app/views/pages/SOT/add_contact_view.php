<div class="row">
	<div class="col-md-6">
		<h2>Add Contact Information</h2>
		<p>Select a contact to view more information about them.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php elseif (validation_errors()): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo validation_errors(); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>
<legend>Contact Info</legend>
<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>SOT/add_contact" method="POST">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label for="TITLE_DSC">Title</label>
						<input type="text" name="TITLE_DSC" class="form-control" title="Title" value="<?php echo set_value('TITLE_DSC') ?>" maxlength="5" />
					</div>
					<div class="col-sm-3">
						<label for="FIRST_NME">First Name</label>
						<input type="text" name="FIRST_NME" class="form-control" title="First Name" value="<?php echo set_value('FIRST_NME') ?>" maxlength="30" />
					</div>
					<div class="col-sm-2">
						<label for="MIDDLE_NME">Middle Name</label>
						<input type="text" name="MIDDLE_NME" class="form-control" title="Middle Name" value="<?php echo set_value('MIDDLE_NME') ?>" maxlength="3" />
					</div>
					<div class="col-sm-3">
						<label for="LAST_NME">Last Name</label><span class="text-danger">*</span>
						<input type="text" name="LAST_NME" class="form-control" title="Last Name" value="<?php echo set_value('LAST_NME') ?>" maxlength="30" />
					</div>
					<div class="col-sm-3">
						<label for="BIRTH_DTE">Birth Date</label>
						<input type="text" name="BIRTH_DTE" class="form-control date_picker" title="Birth Date" value="<?php echo set_value('BIRTH_DTE') ?>" readonly />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="AGENCY_NME">Agency Name</label>
						<input type="text" name="AGENCY_NME" class="form-control" title="Agency Name" value="<?php echo set_value('AGENCY_NME') ?>" maxlength="40" />
					</div>
					<div class="col-sm-4">
						<label for="EMAIL_DSC">Email Address</label>
						<input type="text" name="EMAIL_DSC" class="form-control" title="Email Address" value="<?php echo set_value('MIDDLE_NME') ?>" maxlength="50" />
					</div>
				</div>
			</div>

			<legend>Addresses</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label for="CIVIC_NUM">Civic #</label>
						<input type="text" name="A-CIVIC_NUM" class="form-control" title="Civic Number" value="<?php echo set_value('A-CIVIC_NUM') ?>" maxlength="6" />
					</div>
					<div class="col-sm-4">
						<label for="CIVIC_ADDR">Civic Address</label>
						<input type="text" name="D-CIVIC_ADDR" class="form-control" title="Civic Address" value="<?php echo set_value('D-CIVIC_ADDR') ?>" maxlength="100" />
					</div>
					<div class="col-sm-1">
						<label for="A-APT_NUM">Apt #</label>
						<input type="text" name="A-APT_NUM" class="form-control" title="Apartment Number" value="<?php echo set_value('A-APT_NUM') ?>" maxlength="5" />
					</div>
					<div class="col-sm-1">
						<label for="DISTRICT_NUM">District</label>
						<input type="text" name="A-DISTRICT_NUM" class="form-control" title="District" value="<?php echo set_value('A-DISTRICT_NUM') ?>" maxlength="4" />
					</div>
					<div class="col-sm-1">
						<label for="ADDRESS_TYP">Type</label>
						<input type="text" name="A-ADDRESS_TYP" class="form-control" title="Residence Type" value="<?php echo set_value('A-ADDRESS_TYP') ?>" maxlength="1" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="MAIL_ADDR">Mailing Address</label>
						<input type="text" name="D-MAIL_ADDR" class="form-control" title="Mailing Address" value="<?php echo set_value('D-MAIL_ADDR') ?>" maxlength="200" />
					</div>
					<div class="col-sm-3">
						<label for="MAILCITY_DSC">City</label>
						<input type="text" name="A-MAILCITY_DSC" class="form-control" title="City" value="<?php echo set_value('A-MAILCITY_DSC') ?>" maxlength="25" />
					</div>
					<div class="col-sm-3">
						<label for="COUNTRY_DSC">Country</label>
						<input type="text" name="A-COUNTRY_DSC" class="form-control" title="Country" value="<?php echo set_value('A-COUNTRY_DSC') ?>" maxlength="25" />
					</div>
					<div class="col-sm-2">
						<label for="POSTAL_CDE">Postal Code</label>
						<input type="text" name="A-POSTAL_CDE" class="form-control" title="Postal Code" value="<?php echo set_value('A-POSTAL_CDE') ?>" maxlength="7" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label for="PROV_DSC">Prov.</label>
						<input type="text" name="A-PROV_DSC" class="form-control" title="Province" value="<?php echo set_value('A-PROV_DSC') ?>" maxlength="4" />
					</div>
					<div class="col-sm-3">
						<label for="A-FROM_DTE">From</label>
						<input type="text" name="A-FROM_DTE" class="form-control date_picker" title="From" value="<?php echo set_value('A-FROM_DTE') ?>" readonly />
					</div>
					<div class="col-sm-3">
						<label for="A-TO_DTE">To</label>
						<input type="text" name="A-TO_DTE" class="form-control date_picker" title="To" value="<?php echo set_value('A-TO_DTE') ?>" readonly />
					</div>
					<div class="col-sm-5">
						<label for="ADDRESS_NOTES">Address Notes</label>
						<textarea name="A-ADDRESS_NOTES" class="form-control" title="Address Notes" maxlength="1000"><?php echo set_value('A-ADDRESS_NOTES') ?></textarea>
					</div>
				</div>
			</div>

			<legend>Phone Numbers</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<label for="AREA_CDE">Area</label>
						<input type="text" name="P-AREA_CDE" class="form-control" title="Area" value="<?php echo set_value('P-AREA_CDE') ?>" maxlength="3" />
					</div>
					<div class="col-sm-2">
						<label for="PHONE_NUM">Number</label>
						<input type="text" name="P-PHONE_NUM" class="form-control" title="Number" value="<?php echo set_value('P-PHONE_NUM') ?>" maxlength="7" />
					</div>
					<div class="col-sm-1">
						<label for="EXT_CDE">Ext</label>
						<input type="text" name="P-EXT_CDE" class="form-control" title="Extention" value="<?php echo set_value('P-EXT_CDE') ?>" maxlength="4" />
					</div>
					<div class="col-sm-1">
						<label for="PHONE_FLG">Type</label>
						<input type="text" name="P-PHONE_FLG" class="form-control" title="Type" value="<?php echo set_value('P-PHONE_FLG') ?>" maxlength="1" />
					</div>
					<div class="col-sm-1">
						<label for="PRIMARY_FLG">Primary?</label>
						<input type="checkbox" name="P-PRIMARY_FLG" title="Primary?"/>
					</div>
					<div class="col-sm-3">
						<label for="FROM_DTE">From</label>
						<input type="text" name="P-FROM_DTE" class="form-control date_picker" title="From" value="<?php echo set_value('P-FROM_DTE') ?>" readonly />
					</div>
					<div class="col-sm-3">
						<label for="TO_DTE">To</label>
						<input type="text" name="P-TO_DTE" class="form-control date_picker" title="To" value="<?php echo set_value('P-TO_DTE') ?>" readonly />
					</div>
				</div>
			</div>

			<legend>Contact Notes</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<textarea name="NOTES_DSC" class="form-control" title="Contact Notes" maxlength="1000"><?php echo set_value('NOTES_DSC') ?></textarea>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12 no_label_fix">
						<input type="submit" class="btn btn-md btn-primary pull-right" value="Add Contact" />
						<a href="<?php echo base_url(); ?>SOT/contact_information" class="btn btn-md btn-default">Back to Contacts</a>
					</div>
				</div>
			</div>
	</form>
	</div>
</div>