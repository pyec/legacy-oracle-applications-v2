<div class="row" style="margin-top: 50px;">
	<div class="col-sm-12">
		<div class="alert alert-warning text-center">
		    <p><strong>This application is currently not availible for production release.</strong></p>
		    <p>Please continue to use the old application until the new one is availible.</p>
		    <p>The old application can be found at <a href="http://hrmforms:7777/servlet/f60servlet?config=edl" target="_blank">http://hrmforms:7777/servlet/f60servlet?config=edl</a></p> 
		    <hr>
		    <p>To test the current version in development, please visit the link below.</p>
		    <p><strong>Please note : This development version uses test data and should not be used for anything but testing purposes.</strong></p>
		    <p><a href="https://appsdev.halifax.ca/hrmapplications">https://appsdev.halifax.ca/hrmapplications</a></p>
	</div>
</div>