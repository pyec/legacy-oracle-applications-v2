<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Phonebook System

Description : This system contains contact information for HRM employees for
			quick reference.

  Functions : index(), 
  			add_staff(), 
  			delete_staff(), 
  			update_staff()

=========================================================================================================================*/
class Phonebook extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		// ENVIRONMENT is set in index.php at root of application
		if(ENVIRONMENT == 'production') 
		{
			$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
			$base = PROD_LDAP_BASE;
		}
		// dev environment
		else
		{
			$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
			$base = DEV_LDAP_BASE;
		}

		// Set up the config array for the LDAP library
		$config = array(
			'port' => LDAP_PORT,
			'host_name' => $host_name, 
			'base' => $base, 
			'rdn' => $this->session->userdata('rdn'), 
			'password' => $this->encrypt->decode($this->session->userdata('password')), 
			);

		// Load LDAP and pass it the configuration array.
		$this->load->library('ldap', $config);

		$data['departments'] = $this->ldap->get_deparments();
		$data['content'] = 'pages/phonebook/home_view';
		$this->load->view('master', $data);	
	}
/*           
============================================================================
get_expired
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - Gets all the expired staff members.
============================================================================
*/

	public function get_expired()
	{
		// ENVIRONMENT is set in index.php at root of application
		if(ENVIRONMENT == 'production') 
		{
			$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
			$base = PROD_LDAP_BASE;
		}
		// dev environment
		else
		{
			$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
			$base = DEV_LDAP_BASE;
		}

		// Set up the config array for the LDAP library
		$config = array(
			'port' => LDAP_PORT,
			'host_name' => $host_name, 
			'base' => $base, 
			'rdn' => $this->session->userdata('rdn'), 
			'password' => $this->encrypt->decode($this->session->userdata('password')), 
			);

		// Load LDAP and pass it the configuration array.
		$this->load->library('ldap', $config);

		$expired = $this->ldap->expired();
	}

/*           
============================================================================
search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles validation and setting up the fiter to query the 
ldap server with.
============================================================================
*/

	public function search()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('position', 'Position', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('section', 'Section', 'trim|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('office_phone', 'Office Phone', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('mobile_phone', 'Mobile Phone', 'trim|max_length[50]|xss_clean');
		if ($this->form_validation->run() == true)
		{
			$data = $this->input->post(null, true);
			if (!empty($data)) 
			{
				# code...
				$filter = "(&";

				if (!empty($data['first_name']) && !empty($data['last_name']))
				{
					$filter = $filter."(name=".$data['last_name']."* ".$data['first_name']."*)";	
				}
				elseif(!empty($data['first_name']) && empty($data['last_name']))
				{
					$filter = $filter."(name=*".$data['first_name']."*)";
				}
				elseif(empty($data['first_name']) && !empty($data['last_name']))
				{
					$filter = $filter."(name=*".$data['last_name']."*)";
				}

				if (!empty($data['position']))
				{
					$filter = $filter."(title=*".$data['position']."*)";
				}

				if ($data['section'])
				{
					$filter = $filter."(Division=*".$data['section']."*)";
				}

				if (!empty($data['email']))
				{
					$filter = $filter."(mail=*".$data['email']."*)";
				}

				if (!empty($data['office_phone']))
				{
					$filter = $filter."(telephonenumber=*".$data['office_phone']."*)";
				}

				if (!empty($data['mobile_phone']))
				{
					$filter = $filter."(mobile=*".$data['mobile_phone']."*)";
				}

				$filter = $filter.")";

				// ENVIRONMENT is set in index.php at root of application
				if(ENVIRONMENT == 'production') 
				{
					$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
					$base = PROD_LDAP_BASE;
				}
				// dev environment
				else
				{
					$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
					$base = DEV_LDAP_BASE;
				}

				// Set up the config array for the LDAP library
				$config = array(
					'port' => LDAP_PORT,
					'host_name' => $host_name, 
					'base' => $base, 
					'rdn' => $this->session->userdata('rdn'), 
					'password' => $this->encrypt->decode($this->session->userdata('password')), 
					);

				// Load LDAP and pass it the configuration array.
				$this->load->library('ldap', $config);
				$data['staff'] = $this->ldap->phonebook_search($filter);
			}

			$data['departments'] = $this->ldap->get_deparments();
			$data['content'] = "pages/phonebook/home_view";
			$this->load->view('master', $data);
		}
	}


/*           
============================================================================
staff
----------------------------------------------------------------------------
string $name = The string containing the same of the staff member that you
want to return.
----------------------------------------------------------------------------
No return - gets the record by name.
============================================================================
*/

	public function staff( $name )
	{
		// ENVIRONMENT is set in index.php at root of application
		if(ENVIRONMENT == 'production') 
		{
			$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
			$base = PROD_LDAP_BASE;
		}
		// dev environment
		else
		{
			$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
			$base = DEV_LDAP_BASE;
		}

		// Set up the config array for the LDAP library
		$config = array(
			'port' => LDAP_PORT,
			'host_name' => $host_name, 
			'base' => $base, 
			'rdn' => $this->session->userdata('rdn'), 
			'password' => $this->encrypt->decode($this->session->userdata('password')), 
			);

		// Load LDAP and pass it the configuration array.
		$this->load->library('ldap', $config);

		// Set up the filter for the ldap search.
		$filter = '(samaccountname='.$name.')';

		$data['employee'] = $this->ldap->get_staff_by_accountname($filter);
		
		$data['employee'][0]['name'] = explode(',', $data['employee'][0]['name']);

		$data['content'] = "pages/phonebook/staff_details_view";
		$this->load->view('master', $data);	
	}

//--------------------------------------------------------------------------
	/*
		These functions aren't needed because all of the modification of 
		data will be handled through the help desk.
	*/

/*           
============================================================================
add_Staff
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles preping the data for insert.
============================================================================
*/

// 	public function add_staff()
// 	{
// 		//Load Model
// 		// $this->load->model('phonebook_model');

// 		$post_data = $this->input->post(null, true);

// 		if (!empty($post_data))
// 		{
// 			// if ($this->phonebook_model->insert_employee($post_data))
// 			// {
// 			// 	// Return a success message.
// 			// 	$this->index();
// 			// }
// 			// else
// 			// {
// 			// 	// Return an error message.
// 			// }
// 		}
// 		else
// 		{
// 			// error: no data in post.
// 			$data['content'] = "pages/phonebook/add_staff_view";
// 			$this->load->view('master', $data);
// 		}
// 	}

// /*           
// ============================================================================
// delete_staff
// ----------------------------------------------------------------------------
// NO PARAMS
// ----------------------------------------------------------------------------
// No return - handles deleting staff.
// ============================================================================
// */

// 	public function delete_staff()
// 	{
// 		//Load Model
// 		// $this->load->model('phonebook_model');

// 		$post_data = $this->input->post(null, true);

// 		// if ($this->phonebook_model->remove_employee($post_data['id']))
// 		// {
// 		// 	// Return a success message.
// 		// }
// 		// else
// 		// {
// 		// 	// Return an error message.
// 		// }
// 		$this->index();
// 	}

// /*           
// ============================================================================
// update_staff
// ----------------------------------------------------------------------------
// NO PARAMS
// ----------------------------------------------------------------------------
// No return - handles passing the data to the model for an update.
// ============================================================================
// */

// 	public function update_staff()
// 	{
// 		//Load Model
// 		// $this->load->model('phonebook_model');

// 		$post_data = $this->input->post(null, true);

// 		// if ($this->phonebook_model->update_employee($post_data['id']))
// 		// {
// 		// 	// Return a success message.
// 		// }
// 		// else
// 		// {
// 		// 	// Return an error message.
// 		// }

// 		$this->index();
// 	}

	public function search_assets()
	{
		// Load Model
		$this->load->model('phonebook_model');
		$post = $this->input->post(null, true);

		if (!empty($post))
		{

			$this->form_validation->set_rules('name', 'Asset Name', 'trim|xss_clean');
			$this->form_validation->set_rules('business_unit', 'Business Unit', 'trim|xss_clean');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|xss_clean');
			$this->form_validation->set_rules('civic_number', 'Civic Number', 'trim|xss_clean');
			$this->form_validation->set_rules('street_name', 'Street Name', 'trim|xss_clean');

			if ($this->form_validation->run() == true)
			{
				$data['assets'] = $this->phonebook_model->search_assets($post);

				// ENVIRONMENT is set in index.php at root of application
				if(ENVIRONMENT == 'production') 
				{
					$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
					$base = PROD_LDAP_BASE;
				}
				// dev environment
				else
				{
					$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
					$base = DEV_LDAP_BASE;
				}

				// Set up the config array for the LDAP library
				$config = array(
					'port' => LDAP_PORT,
					'host_name' => $host_name, 
					'base' => $base, 
					'rdn' => $this->session->userdata('rdn'), 
					'password' => $this->encrypt->decode($this->session->userdata('password')), 
					);

				// Load LDAP and pass it the configuration array.
				$this->load->library('ldap', $config);
				$data['departments'] = $this->ldap->get_deparments();
				$data['content'] = "pages/phonebook/home_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
				$base = PROD_LDAP_BASE;
			}
			// dev environment
			else
			{
				$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
				$base = DEV_LDAP_BASE;
			}

			// Set up the config array for the LDAP library
			$config = array(
				'port' => LDAP_PORT,
				'host_name' => $host_name, 
				'base' => $base, 
				'rdn' => $this->session->userdata('rdn'), 
				'password' => $this->encrypt->decode($this->session->userdata('password')), 
				);

			// Load LDAP and pass it the configuration array.
			$this->load->library('ldap', $config);
			$data['departments'] = $this->ldap->get_deparments();
			$data['content'] = "pages/phonebook/home_view";
			$this->load->view('master', $data);
		}
	}

	public function add_asset()
	{
		// Load Model
		$this->load->model('phonebook_model');

		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			$this->form_validation->set_rules('name', 'Asset Name', 'trim|max_length[50]|required');
			$this->form_validation->set_rules('section', 'Section Name', 'trim|max_length[25]');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|max_length[15]');
			$this->form_validation->set_rules('fax_number', 'Fax Number', 'trim|max_length[15]');
			$this->form_validation->set_rules('address_description', 'Address Description', 'trim|max_length[50]');
			$this->form_validation->set_rules('civic_number', 'Civic Number', 'trim|max_length[10]');
			$this->form_validation->set_rules('street_name', 'Street Name', 'trim|max_length[50]');

			if ($this->form_validation->run() == true)
			{
				if ($this->phonebook_model->add_asset($post))
				{
					$this->session->set_flashdata('message', 'The asset was added.');
					redirect('phonebook/search_assets');
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an issue inserting the record, please try again.');
					redirect('phonebook/search_assets');
				}
			}
			else
			{
				// ENVIRONMENT is set in index.php at root of application
				if(ENVIRONMENT == 'production') 
				{
					$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
					$base = PROD_LDAP_BASE;
				}
				// dev environment
				else
				{
					$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
					$base = DEV_LDAP_BASE;
				}

				// Set up the config array for the LDAP library
				$config = array(
					'port' => LDAP_PORT,
					'host_name' => $host_name, 
					'base' => $base, 
					'rdn' => $this->session->userdata('rdn'), 
					'password' => $this->encrypt->decode($this->session->userdata('password')), 
					);
				// Load LDAP and pass it the configuration array.
				$this->load->library('ldap', $config);

				
				$data['departments'] = $this->ldap->get_deparments();
				$data['content'] = "pages/phonebook/add_asset_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
				$base = PROD_LDAP_BASE;
			}
			// dev environment
			else
			{
				$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
				$base = DEV_LDAP_BASE;
			}

			// Set up the config array for the LDAP library
			$config = array(
				'port' => LDAP_PORT,
				'host_name' => $host_name, 
				'base' => $base, 
				'rdn' => $this->session->userdata('rdn'), 
				'password' => $this->encrypt->decode($this->session->userdata('password')), 
				);

			// Load LDAP and pass it the configuration array.
			$this->load->library('ldap', $config);

			
			$data['departments'] = $this->ldap->get_deparments();
			$data['content'] = "pages/phonebook/add_asset_view";
			$this->load->view('master', $data);
		}
	}

	public function update_asset()
	{	
		// Load Model
		$this->load->model('phonebook_model');
		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			$this->form_validation->set_rules('name', 'Asset Name', 'trim|max_length[50]');
			$this->form_validation->set_rules('section', 'Section Name', 'trim|max_length[25]');
			$this->form_validation->set_rules('phone_number', 'Phone Number', 'trim|max_length[15]');
			$this->form_validation->set_rules('fax_number', 'Fax Number', 'trim|max_length[15]');
			$this->form_validation->set_rules('address_description', 'Address Description', 'trim|max_length[50]');
			$this->form_validation->set_rules('civic_number', 'Civic Number', 'trim|max_length[10]');
			$this->form_validation->set_rules('street_name', 'Street Name', 'trim|max_length[50]');

			if ($this->form_validation->run() == true)
			{
				if ($this->phonebook_model->update_asset($post))
				{
					$this->session->set_flashdata('message', 'The asset was updated.');
					redirect('phonebook/search_assets');
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an issue updating the record, please try again.');
					redirect('phonebook/search_assets');
				}
			}
			else
			{
				// ENVIRONMENT is set in index.php at root of application
				if(ENVIRONMENT == 'production') 
				{
					$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
					$base = PROD_LDAP_BASE;
				}
				// dev environment
				else
				{
					$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
					$base = DEV_LDAP_BASE;
				}

				// Set up the config array for the LDAP library
				$config = array(
					'port' => LDAP_PORT,
					'host_name' => $host_name, 
					'base' => $base, 
					'rdn' => $this->session->userdata('rdn'), 
					'password' => $this->encrypt->decode($this->session->userdata('password')), 
					);
				// Load LDAP and pass it the configuration array.
				$this->load->library('ldap', $config);

				
				$data['departments'] = $this->ldap->get_deparments();
				$data['content'] = "pages/phonebook/update_asset_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			// ENVIRONMENT is set in index.php at root of application
			if(ENVIRONMENT == 'production') 
			{
				$host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
				$base = PROD_LDAP_BASE;
			}
			// dev environment
			else
			{
				$host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
				$base = DEV_LDAP_BASE;
			}

			// Set up the config array for the LDAP library
			$config = array(
				'port' => LDAP_PORT,
				'host_name' => $host_name, 
				'base' => $base, 
				'rdn' => $this->session->userdata('rdn'), 
				'password' => $this->encrypt->decode($this->session->userdata('password')), 
				);
			// Load LDAP and pass it the configuration array.
			$this->load->library('ldap', $config);

			
			$data['departments'] = $this->ldap->get_deparments();
			$data['asset'] = $this->phonebook_model->get_asset_by_id($id_to_update);
			$data['content'] = "pages/phonebook/update_asset_view";
			$this->load->view('master', $data);
		}
	}

	public function view_asset()
	{	
		// Load Model
		$this->load->model('phonebook_model');
		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		// ENVIRONMENT is set in index.php at root of application
		if(ENVIRONMENT == 'production') 
		{
            $host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
            $base = PROD_LDAP_BASE;
		}
		// dev environment
		else
		{
            $host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
            $base = DEV_LDAP_BASE;
		}
		
		// Set up the config array for the LDAP library
		$config = array(
            'port' => LDAP_PORT,
			'host_name' => $host_name, 
			'base' => $base, 
			'rdn' => $this->session->userdata('rdn'), 
			'password' => $this->encrypt->decode($this->session->userdata('password')), 
			);
		// Load LDAP and pass it the configuration array.
		$this->load->library('ldap', $config);

		
		$data['departments'] = $this->ldap->get_deparments();
		$data['asset'] = $this->phonebook_model->get_asset_by_id($id_to_update);
		$data['content'] = "pages/phonebook/view_asset_view";
		$this->load->view('master', $data);
}

	public function delete_asset()
	{
		// Load Model
		$this->load->model('phonebook_model');
	}

}

/* End of file phonebook.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/phonebook.php */