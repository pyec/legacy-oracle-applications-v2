<div class="row">
	<div class="col-md-6">
		<h2>Automated Traffic Recorder</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>


<form action="<?php echo base_url(); ?>traffic/ATR_search" method="POST">
    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <label for="record_number">Record #</label>
                <input type="text" name="record_number" class="form-control" title="Record Number" value="<?php echo set_value('record_number') ?>" tabindex="1" placeholder="Record Number" />
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
                <button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="traffic_atr_form_reset()">Clear</button>
            </div>
        </div>
    </div>
</form>
<legend class="phonebook_legend"></legend>
	
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($ATR_studies)): ?>
			<?php if (count($ATR_studies) == 500): ?>
				<h2 class="count"><?php echo count($ATR_studies) ?> studies or more</h2>
			<?php else: ?>
				<h2 class="count"><?php echo count($ATR_studies) ?> studies</h2>
			<?php endif ?>
		<?php endif ?>
	</div>
	<div class="col-sm-12" style="margin-top: 30px;">
		<?php if (!empty($ATR_studies) && count($ATR_studies) == 500): ?>
			<P class="">Refine your search for more specific results.</P>			
		<?php endif ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<a href="<?= base_url(); ?>traffic/add_ATR" class="btn btn-success pull-right">Add Survey</a>
	</div>
</div>

<?php if (!empty($ATR_studies)): ?>

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Record #</th>
				<th>Civic #</th>
				<th>Surveyor</th>
				<th>Comments</th>
				<th>Created On</th>
				<th>Delete</th>
			</thead>
			<tbody class="tbody">
					<?php foreach ($ATR_studies as $study): ?>
						<tr>
							<td><a href="<?= base_url(); ?>traffic/update_ATR/<?= $study['REC_CODE'] ?>"><?= $study['REC_CODE'] ?></a></td>
							<td><?= $study['CIV_ADDR']  ?></td>
							<td><?= $study['SURVEYOR']  ?></td>
							<td><?= $study['COMMENT_BOX'] ?></td>
							<td><?= $study['CREATE_DATE'] ?></td>
							<td>
								<form action="<?php echo base_url(); ?>traffic/delete_ATR" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger delete_atr"><i class="fa fa-minus-square"></i></button>
									<input type="hidden" name="REC_CODE" value="<?= $study['REC_CODE'] ?>" />
								</form>
							</td>	
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>
