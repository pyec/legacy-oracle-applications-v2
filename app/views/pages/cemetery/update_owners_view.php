<?php
$BILink = '';

if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'production':
            $BILink = BI_PROD_URL;
            break;
        default:
            $BILink = BI_QA_URL;
    }
}
?>

<div class="row">
	<div class="col-md-9">
		<h2>Update Owner</h2>
	</div>
	<div class="col-sm-3" style="margin-bottom: 1em">
        <a target="_blank" href="<?php echo $BILink ?>/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=Ae_D7jwgtzBIjI7fYkALJiQ&sIDType=CUID&sType=rpt&sRefresh=N&lsSp%5Fcemnum=<?php echo $owner[0]['CEM_NBR']; ?>&lsSp%5Fownnum=<?php echo $owner[0]['CEM_REFNBR'] ?>&lsSp%5Fparm3=1&lsSp%5Fintnum=-1&lsScomments=A" class="btn btn-md btn-primary add_interred pull-right" style="margin-right: 5px;" >Print Record</a>
	</div>
</div>

 <div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php elseif (validation_errors()): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo validation_errors(); ?></p>
		</div>
	<?php endif ?>
    <?php if ($errorMessage): ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <p><?php echo $errorMessage; ?></p>
        </div>
    <?php endif ?>
	</div>
</div>

<div class="row">
	<form action="<?php echo base_url(); ?>cemetery/update_owner/<?php echo $owner[0]['CEM_REFNBR'] ?>/<?php echo $owner[0]['CEM_NBR']; ?>" method="POST" >
		<!-- <input type="hidden" name="CEM_REFNBR" value="<?php echo $owner[0]['CEM_REFNBR'] ?>" /> -->
		<div class="col-md-12">
			<!-- <div class="form-group">
				<div class="col-sm-3">
					<label for="CEM_NBR">Cemetery Name</label>
					<select name="CEM_NBR" class="form-control">
						<?php foreach ($cemeteries as $cem): ?>
							<?php if ($cem['CEM_NBR'] == $owner[0]['CEM_NBR']): ?>
								<option selected value="<?php echo $cem['CEM_NBR'] ?>"><?php echo $cem['CEM_NAME'] ?></option>
							<?php else : ?>
								<option value="<?php echo $cem['CEM_NBR'] ?>"><?php echo $cem['CEM_NAME'] ?></option>
							<?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
			</div> -->
 
			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<!-- div class="col-sm-3">
						<label for="CEM_TTL">Title</label>
						<input type="text" name="CEM_TTL" class="form-control" title="Title" value="<?php echo $owner[0]['CEM_TTL'] ?>" maxlength="15"/>
						<input type="hidden" name="CEM_REFNBR" value="<?php echo $owner[0]['CEM_REFNBR']; ?>" />
					</div> -->
                    <input type="hidden" name="CEM_REFNBR" value="<?php echo $owner[0]['CEM_REFNBR']; ?>" />
					<div class="col-sm-3">
						<label for="CEM_NBR">Cemetery Name</label>
						<select name="CEM_NBR" class="form-control">
							<?php foreach ($cemeteries as $cem): ?>
								<?php if ($cem['CEM_NBR'] == $owner[0]['CEM_NBR']): ?>
									<option selected value="<?php echo $cem['CEM_NBR'] ?>"><?php echo $cem['CEM_NAME'] ?></option>
								<?php else : ?>
									<option value="<?php echo $cem['CEM_NBR'] ?>"><?php echo $cem['CEM_NAME'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
				
					<div class="col-sm-3">
						<label for="CEM_LNAME">Last Name</label>
						<input type="text" name="CEM_LNAME" class="form-control" title="Last Name" value="<?php echo $owner[0]['CEM_LNAME'] ?>" maxlength="30"/>	
					</div>
					<div class="col-sm-3">
						<label for="CEM_FNAME">First Name</label>
						<input type="text" name="CEM_FNAME" class="form-control" title="First Name" value="<?php echo $owner[0]['CEM_FNAME'] ?>" maxlength="20"/>	
					</div>
					<div class="col-sm-3">
						<label for="CEM_MNAME">Middle Name</label>
						<input type="text" name="CEM_MNAME" class="form-control" title="Middle Name" value="<?php echo $owner[0]['CEM_MNAME'] ?>" maxlength="20"/>	
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="CEM_OWNERS">Owner Information</label>
						<textarea row="10" type="text" name="CEM_OWNERS" class="form-control" title="Owner Information"><?php echo $owner[0]['CEM_OWNERS'] ?></textarea>	
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="CEM_LOTNBR">Lot Number</label>
						<input type="text" name="CEM_LOTNBR" class="form-control" title="Lot Numbers" value="<?php echo $owner[0]['CEM_LOTNBR'] ?>" maxlength="30"/>	
					</div>
					<div class="col-sm-1">
						<label for="CEM_SIZE">Lot Size</label>
						<input type="text" name="CEM_SIZE" class="form-control" title="Lot Size" value="<?php echo $owner[0]['CEM_SIZE'] ?>" maxlength="75"/>	
					</div>
					<div class="col-sm-1">
						<label for="CEM_NBRGR">Graves</label>
						<input type="text" name="CEM_NBRGR" class="form-control" title="Nubmer of Graves" value="<?php echo $owner[0]['CEM_NBRGR'] ?>" maxlength="4"/>	
					</div>
					<div class="col-sm-3">
						<label for="CEM_SCTN">Location</label>
						<input type="text" name="CEM_SCTN" class="form-control" title="Location" value="<?php echo $owner[0]['CEM_SCTN'] ?>" maxlength="4"/>	
					</div>
					<div class="col-sm-1">
						<label for="CEM_SHEET">Sheet</label>
						<input type="text" name="CEM_SHEET" class="form-control" title="Sheet" value="<?php echo $owner[0]['CEM_SHEET'] ?>" maxlength="4"/>	
					</div>
					<div class="col-sm-3" title="Incomplete Purchase Date">
						<label for="CEM_INCOMPURDTE">Incomplete Purchase Date</label>
						<input type="text" name="CEM_INCOMPURDTE" class="form-control" maxlength="" value="<?php echo $owner[0]['CEM_INCOMPURDTE'] ?>" />	
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="CEM_PURDTE">Purchase Date</label>
						<input type="text" name="CEM_PURDTE" class="form-control date_picker_cem" title="Purchase Date" value="<?php echo $owner[0]['CEM_PURDTE'] ?>" readonly maxlength=""/>
					</div>
					<div class="col-sm-2">
						<label for="CEM_PURAMT">Purchase Amount</label>
						<input type="text" name="CEM_PURAMT" class="form-control" title="Purchase Amount" value="<?php echo $owner[0]['CEM_PURAMT'] ?>" maxlength="11" />	
					</div>
					<div class="col-sm-2">
						<label for="CEM_CHGNBR">Charge Number</label>
						<input type="text" name="CEM_CHGNBR" class="form-control" title="Charge Number" value="<?php echo $owner[0]['CEM_CHGNBR'] ?>" maxlength="6"/>	
					</div>
					<div class="col-sm-2">
						<label for="CEM_NBRMON"># Monuments</label>
						<input type="text" name="CEM_NBRMON" class="form-control" title="Number of Monuments" value="<?php echo $owner[0]['CEM_NBRMON'] ?>" maxlength="10"/>	
					</div>
					<div class="col-sm-2">
						<label for="CEM_MONSIZE">Monument Size</label>
						<input type="text" name="CEM_MONSIZE" class="form-control" title="Number of Monuments" value="<?php echo $owner[0]['CEM_MONSIZE'] ?>" maxlength="7"/>	
					</div>
					<div class="col-sm-1">
						<label for="CEM_MARKER">Marker</label>
						<input type="text" name="CEM_MARKER" class="form-control" title="Marker" value="<?php echo $owner[0]['CEM_MARKER'] ?>" maxlength="15"/>	
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<!-- <div class="col-sm-3">
						<label for="CEM_CLDATE">Close Date</label>
						<input type="text" name="CEM_CLDATE" class="form-control date_picker" title="Close Date" value="<?php echo $owner[0]['CEM_CLDATE'] ?>" readonly maxlength=""/>	
					</div>
					<div class="col-sm-2 no_label_fix">
						<label for="CEM_CLFLAG">Close Flag</label>
						<?php if (!empty($owner[0]['CEM_CLFLAG'])): ?>
							<?php if ($owner[0]['CEM_CLFLAG'] == 'Y'): ?>
								<input type="checkbox" name="CEM_CLFLAG" title="Close Flag" value="on" maxlength=""/>	
							<?php else : ?>
								<input type="checkbox" name="CEM_CLFLAG" title="Close Flag" value="off" maxlength=""/>	
							<?php endif ?>
						<?php else : ?>
							<input type="checkbox" name="CEM_CLFLAG" title="Close Flag" value="off" maxlength=""/>	
						<?php endif ?>
					</div> -->
					<div class="col-sm-3">
						<label for="CEM_MONVEND">Supplier</label>
						<input type="text" name="CEM_MONVEND" class="form-control" title="Supplier" value="<?php echo $owner[0]['CEM_MONVEND'] ?>" maxlength="30"/>	
					</div>
					<div class="col-sm-2">
						<label for="CEM_OWNYR">Year</label>
						<input type="text" name="CEM_OWNYR" class="form-control" title="Year" value="<?php echo $owner[0]['CEM_OWNYR'] ?>" maxlength="4"/>	
					</div>
					<div class="col-sm-2">
						<label for="CEM_PRPCARE">Perpetual Care</label>
						<select name="CEM_PRPCARE" class="form-control" title="Perpetual Care">
							<?php if (!empty($owner[0]['CEM_PRPCARE'])): ?>
								<?php if ($owner[0]['CEM_PRPCARE'] == 1): ?>
									<option value="1" selected >Yes</option>
									<option value="0" >No</option>
								<?php else : ?>
									<option value="1" >Yes</option>
									<option value="0" selected >No</option>
								<?php endif ?>
							<?php else: ?>
								<option value="1" >Yes</option>
								<option value="0" selected >No</option>
							<?php endif ?>
						</select>	
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<!-- <div class="form-group">
				<div class="col-sm-3">
					<label for="CEM_MICR_ROLL">Micro Roll</label>
					<input type="text" name="CEM_MICR_ROLL" class="form-control" title="Micro Roll" value="<?php echo $owner[0]['CEM_MICR_ROLL'] ?>" maxlength="4"/>
				</div>
				<div class="col-sm-3">
					<label for="CEM_MICR_FRM">Micro Frame</label>
					<input type="text" name="CEM_MICR_FRM" class="form-control" title="Micro Frame" value="<?php echo $owner[0]['CEM_MICR_FRM'] ?>" maxlength="4"/>
				</div>
			</div> -->

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="CEM_CMNT">Comments</label>
						<textarea name="CEM_CMNT" class="form-control" title="Comments"><?php echo $owner[0]['CEM_CMNT'] ?></textarea>
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="CEM_SPC_CMNT">Special Comments</label>
						<textarea name="CEM_SPC_CMNT" class="form-control" title="Special Comments"><?php echo $owner[0]['CEM_SPC_CMNT'] ?></textarea>
					</div>
				</div>
			</div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <input type="submit" class="btn btn-md btn-primary pull-right" value="Update Owner" />
                        <a href="<?php echo base_url(); ?>cemetery/owners" class="btn btn-md btn-default">Back to Owners</a>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 2em">
                <div class="col-md-9">
                    <h3>Interred</h3>
                </div>
            </div>

			<?php if (!empty($interred)): ?>
				<table class="table table-bordered table-striped table-hover"> 
					<thead>
						<th>Interred</th>
						<th>Date</th>
						<th>Inc. Date</th>
						<th>Refnbr</th>
						<th>Type</th>
						<th>Marker</th>
						<th>Location</th>
						<th>Comments</th>
						<th>Spc. Comments</th>
						<!-- <th>Action</th> -->
					</thead>
					<tbody>
						<?php foreach ($interred as $int): ?>
							<tr>
						 		<td>
						 			<a href="<?php echo base_url(); ?>cemetery/update_interred/<?php echo $int['CEM_NBR'] ?>/<?= $int['CEM_REFNBR'] ?>/<?= $int['INT_REFNBR'] ?>">
						 				<?php echo ucwords(strtolower($int['INT_LNAME'].', '.$int['INT_FNAME'].' '.$int['INT_MNAME'])) ?>
						 			</a>
						 		</td>
						 		<td><?php echo $int['INT_DTE'] ?></td>
						 		<td><?php echo $int['INT_INCOMPDTE'] ?></td>
						 		<td><?php echo $int['INT_REFNBR'] ?></td>
						 		<td><?php echo $int['INT_TYP'] ?></td>
						 		<td><?php echo $int['INT_MARKER'] ?></td>
						 		<td><?php echo $int['INT_LOC'] ?></td>
						 		<td><?php echo $int['INT_CMNT'] ?></td>
						 		<td><?php echo $int['INT_SPC_CMNT'] ?></td>
						 		<!-- <td>
									<a href="<?php echo base_url(); ?>cemetery/delete_owners_interred/<?= $int['CEM_REFNBR'] ?>/<?= $int['CEM_NBR'] ?>/<?= $int['INT_REFNBR'] ?>" class="btn btn-sm btn-danger delete_owners">Delete</a>
						 		</td> -->
						 	</tr>
						<?php endforeach ?>
					</tbody>	
				</table>
			<?php endif ?>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <a href="<?php echo base_url(); ?>cemetery/add_interred/<?php echo $owner[0]['CEM_REFNBR'].'/'.$owner[0]['CEM_NBR'] ?>" class="btn btn-success btn-md pull-right">Add Interred</a>
                    </div>
                </div>
            </div>

		</div>
	</form>
</div>