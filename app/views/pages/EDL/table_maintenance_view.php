<?php
// determine if the user is an admin -- redirect if not
$is_admin = false;

$groups = $this->session->userdata('groups');

foreach ($groups as $group)
{
    if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
    {
        $is_admin = true;
    }
}

if (!($is_admin)) {
    redirect('EDL/');
}
?>

<div class="row">
	<div class="col-md-12">
		<?php if (empty($title)): ?>
			<h2>Reference Table Update</h2>
		<?php else : ?>
			<?php $header = str_replace('_', ' ', $title); ?>
			<h2><?= ucwords($header) ?></h2>
		<?php endif ?>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
		<?php if (empty($data)): ?>
			<ul>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/departments">Departments</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/devices">Devices</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/paper_classifications">Paper Classifications</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/electronic_classifications">Electronic Classifications</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/drawn">Drawn</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/processes">Processes</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/originators">Originators</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/project_types">Project Types</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/locations">Locations</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/surveyors">Surveyors</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/survey_methods">Survey Methods</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/software_versions">Software Versions</a></li>
				<li><a href="<?php echo base_url(); ?>EDL/table_maintenance/statuses">Statuses</a></li>
			</ul>
		<?php else : ?>
			<a href="<?php echo base_url(); ?>EDL/add_table_maintenance/<?= $title ?>" class="btn btn-sm btn-success pull-right no_label_fix" style="margin-bottom: 5px;" >Add Record</a>
			 <table class="table table-bordered table-striped table-hover" >
			 	<thead>
			 		<?php 
		 				$keys = array_keys($data[0]);
		 			 ?>
			 			
		 			<?php foreach ($keys as $key): ?>
		 				<?php 
		 					$key = strtolower($key); 
		 					$key = str_replace('_', ' ', $key);
		 					$key = ucwords($key);
		 				?>
		 				<th><?= $key ?></th>
		 			<?php endforeach ?>
			 	</thead>
			 	<tbody class="tbody">
			 		<?php foreach ($data as $row): ?>
			 			<tr>
				 			<?php $count = 0; ?>
				 			<?php foreach ($row as $key => $field): ?>
				 				<?php 
				 				if ($count == 0): ?>
				 					<td><a href="<?php echo base_url(); ?>EDL/update_table_maintenance/<?= $title.'/'.$field ?>"><?= $field ?></a></td>
				 				<?php else: ?>	
				 					<td><?= $field ?></td>
				 				<?php endif ?>
				 			<?php 
				 				$count++; 
				 			endforeach ?>
				 			
				 			<?php
					 			reset($row);
					 			$first_key = key($row);
				 			 ?>
				 			<!-- <td>
				 				<form action="<?php echo base_url(); ?>EDL/delete_table_maintenance" method="POST" >
				 					<button class="btn btn-sm btn-danger delete_table_maintenance" >Delete</button>
				 					<input type="hidden" name="<?= $keys[0] ?>" value="<?= $row[$first_key] ?>" />
				 					<input type="hidden" name="table_name" value="<?= $title ?>" />
				 				</form>
			 				</td> -->
				 		</tr>
			 		<?php endforeach ?>
			 	</tbody>	
			 </table>
		<?php endif ?>
	</div>
</div>



