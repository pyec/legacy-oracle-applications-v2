<div class="row">
	<div class="col-md-6">
		<h2>Add Phone Record</h2>
		<p>Select a contact to view more information about them.</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>

<form action="<?php echo base_url(); ?>SOT/add_employee_phone" method="POST">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <label for="PHONE_NUM">Number</label>
                        <input type="text" name="PHONE_NUM" class="form-control" title="Number"
                               value="<?php set_value('PHONE_NUM') ?>" maxlength="15"/>
                    </div>
                    <div class="col-sm-1">
                        <label for="PHONE_FLG">Type</label>
                        <input type="text" name="PHONE_FLG" class="form-control" title="Type"
                               value="<?php set_value('PHONE_FLG') ?>" maxlength="1"/>
                    </div>
                    <div class="col-sm-1">
                        <label for="PRIMARY_FLG">Primary?</label>
                        <input type="checkbox" name="PRIMARY_FLG" title="Primary?"
                               value="<?php set_value('PRIMARY_FLG') ?>"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
		<div class="col-sm-12 no_label_fix">
			<input type="hidden" name="EMPL_ID"   value="<?= $EMPL_ID ?>" />
			<input type="submit" class="btn btn-md btn-primary pull-right" value="Add Phone" />
			<a href="<?php echo base_url(); ?>SOT/update_employee/<?= $EMPL_ID ?>" class="btn btn-md btn-default">Back to Officer</a>
		</div>
	</div>
</form>