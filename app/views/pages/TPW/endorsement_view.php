<div class="row">
	<div class="col-md-12">
		<h2>Endorsement</h2>
		<p class="lead">Select an endorsement to view/update their information.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>
<!-- <div class="row"> -->

	<div class="form-group">
		<div class="row">
			<div class="col-sm-12">
				<a href="<?php echo base_url(); ?>/TPW/add_endorsement" class="btn btn-success btn-md pull-right no_label_fix" title="Add">Add Endorsement</a>
			</div>
		</div>
	</div>


<!-- </div> -->
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Endorsement</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
				<?php foreach ($endorsements as $end): ?>
					<tr>
						<td><a href="<?= base_url(); ?>TPW/update_endorsement/<?= $end['ENDORSEMENT_CDE'] ?>"><?= $end['ENDORSEMENT_CDE'].' - '.$end['ENDORSEMENT_DSC'] ?></a></td>
						<td>
							<form action="<?php echo base_url(); ?>TPW/delete_endorsement" method="POST" >
								<button type="submit" class="btn btn-sm btn-danger delete_endorsement"><i class="fa fa-minus-square"></i></button>
								<input type="hidden" name="ENDORSEMENT_CDE" value="<?= $end['ENDORSEMENT_CDE'] ?>" />
							</form>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>	
		</table>
	</div>
</div>