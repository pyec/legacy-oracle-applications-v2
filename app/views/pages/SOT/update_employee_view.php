<div class="row">
	<div class="col-md-6">
		<h2>Update Officer Information</h2>
		<p>Select a officer to view more information about them.</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>

<form action="<?php echo base_url(); ?>SOT/update_employee" method="POST" >
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="EMPL_ID">Officer ID</label>
					<input readonly type="text" name="EMPL_ID" class="form-control" title="Officer ID" value="<?= $employee[0]['EMPL_ID'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="FIRST_NME">First Name</label>
					<input type="text" name="FIRST_NME" class="form-control" title="First Name" value="<?= $employee[0]['FIRST_NME'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="MIDDLE_NME">Middle Initials</label>
					<input type="text" name="MIDDLE_NME" class="form-control" title="Middle Name" value="<?= $employee[0]['MIDDLE_NME'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="LAST_NME">Last Name</label>
					<input type="text" name="LAST_NME" class="form-control" title="Last Name" value="<?= $employee[0]['LAST_NME'] ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="AGENCY_NME">Agency</label>
					<input type="text" name="AGENCY_NME" class="form-control" title="Agency" value="<?= $employee[0]['AGENCY_NME'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="EMAIL_DSC">Email Address</label>
					<input type="text" name="EMAIL_DSC" class="form-control" title="Email Address" value="<?= $employee[0]['EMAIL_DSC'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="WORK_LOC">Work Location</label>
					<input type="text" name="WORK_LOC" class="form-control" title="Work Location" value="<?= $employee[0]['WORK_LOC'] ?>" />
				</div>
				<div class="col-sm-1">

					<?php if ($employee[0]['INACTIVE_FLG'] == 'Y'): ?>
						<label for="INACTIVE_FLG">Active?</label>
						<input type="checkbox" name="INACTIVE_FLG" class="" title="Active?" />
					<?php else : ?>
						<label for="INACTIVE_FLG">Active?</label>
						<input type="checkbox" name="INACTIVE_FLG" class="" title="Active?" checked />
					<?php endif ?>
				</div>
			</div>
		</div>

		<legend>Phone Numbers</legend>

		<?php if (!empty($phone_numbers)): ?>
			<?php $count = 0; ?>
			<?php foreach ($phone_numbers as $number): ?>
				<div class="row">
					<input type="hidden" name="<?= $count ?>-PHONEREC_NUM" value="<?= $number['PHONEREC_NUM'] ?>"/>
					<div class="col-sm-2">
						<label for="PHONE_NUM">Number</label>
						<input type="text" name="<?= $count ?>-PHONE_NUM" class="form-control" title="Number" value="<?= $number['PHONE_NUM'] ?>" />
					</div>
					<div class="col-sm-1">
						<label for="PHONE_FLG">Type</label>
						<input type="text" name="<?= $count ?>-PHONE_FLG" class="form-control" title="Type" value="<?= $number['PHONE_FLG'] ?>" />
					</div>
					<div class="col-sm-1">
						<label for="PRIMARY_FLG">Primary?</label>
						<?php if (!empty($number['PRIMARY_FLG'])): ?>
							<?php if ($number['PRIMARY_FLG'] == 'Y'): ?>
								<input type="checkbox" name="<?= $count ?>-PRIMARY_FLG" title="Primary?" checked />
							<?php else : ?>
								<input type="checkbox" name="<?= $count ?>-PRIMARY_FLG" title="Primary?" />
							<?php endif ?>
						<?php else : ?>
							<input type="checkbox" name="<?= $count ?>-PRIMARY_FLG" title="Primary?" />
						<?php endif ?>
					</div>
				</div>
			<?php ++$count;  ?>
			<?php endforeach ?>
		<?php endif ?>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<a class="no_label_fix pull-right" href="<?php echo base_url(); ?>SOT/add_employee_phone/<?= $employee[0]['EMPL_ID'] ?>">Add Phone Number</a>
				</div>
			</div>
		</div>
		<legend>Notes</legend>

		<div class="form-group">
			<div class="row">
				<textarea name="NOTES_DSC" class="form-control" title="Officer Notes" maxlength="1000"><?= $employee[0]['NOTES_DSC'] ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="no_label_fix">
					<input type="submit" class="btn btn-md btn-primary pull-right" value="Update Officer" />
					<a href="<?php echo base_url(); ?>SOT/employee_information" class="btn btn-md btn-default">Back to Officers</a>
				</div>
			</div>
		</div>
	</div>
</div>
</form>