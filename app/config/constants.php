<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
define('PROXY_URL', 'http://proxy.halifax.ca:8002');
//define('GIS_STREET_LIKE_SEARCH_URL', 'https://services2.arcgis.com/11XBiaBYA9Ep0yNJ/arcgis/rest/services/Street_Name_Routes/FeatureServer/0/query?where=FULL_NAME+like+%27STREET_NAME%25%27&outFields=*&returnGeometry=false&f=pjson');
//define('GIS_STREET_BY_ID_SEARCH_URL', 'https://services2.arcgis.com/11XBiaBYA9Ep0yNJ/arcgis/rest/services/Street_Name_Routes/FeatureServer/0/query?where=STR_CODE_L%3DSTREET_ID&outFields=*&returnGeometry=false&f=pjson');

define('GIS_STREET_LIKE_SEARCH_URL', 'https://msgiswebp01.hrm.halifax.ca/hrm/rest/services/HRM/StreetCut/MapServer/0/query?where=FULL_NAME+like+%27STREET_NAME%25%27&outFields=FIELDS&returnDistinctValues=true&returnGeometry=false&f=pjson');
define('GIS_STREET_BY_ID_SEARCH_URL', 'https://msgiswebp01.hrm.halifax.ca/hrm/rest/services/HRM/StreetCut/MapServer/0/query?where=STR_CODE_L%3DSTREET_ID&outFields=FIELDS&returnDistinctValues=true&returnGeometry=false&f=pjson');

define('PROD_LDAP_HOST_NAME','71.7.141.100');
define('PROD_LDAP_BASE','DC=hrm,DC=halifax,DC=ca');

define('DEV_LDAP_HOST_NAME','MSDC05.hrm.halifax.ca');
define('DEV_LDAP_BASE','OU=HRM Users,OU=Halifax Regional Municipality,DC=hrm,DC=halifax,DC=ca');
define('DEV_HW_LDAP_BASE','OU=Halifax Water,DC=hrm,DC=halifax,DC=ca');

define('LDAP_PORT','389');

define('BI_QA_URL','https://msbiappq02.hrm.halifax.ca');
define('BI_PROD_URL','https://hrmreports.hrm.halifax.ca');

/* End of file constants.php */
/* Location: ./application/config/constants.php */