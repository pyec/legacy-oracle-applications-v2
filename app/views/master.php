
<?php

	// If content is a path to the login page go ahead a load the login
	if ($content == 'pages/login_view')
	{
		$this->load->view($content);
	}
	else
	{
		$this->load->view('templates/header');
		$this->load->view($content);
		$this->load->view('templates/footer');
	}
 ?>