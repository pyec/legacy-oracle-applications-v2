<div class="row" style="margin-bottom: 2em">
    <div class="col-sm-6 col-xs-6">
        <img src="<?php echo base_url(); ?>/assets/img/logo.png" width="245" height="60" border="0" alt="HALIFAX" title="HALIFAX">
    </div>
    <div class="col-sm-6 col-xs-6">
        <h4 class="text-center">HRM Cemetery System</h4>
        <h5 class="text-center">Date : <?= date('d-m-Y h:i:s') ?></h5>
    </div>
</div>

<div class="row">
	<div class="col-xs-4">
		<p><strong>Cemetery :</strong> <?= $cemetery[0]['CEM_NAME'] ?></p>
		<p><strong>Owner :</strong> <?= $owner[0]['CEM_LNAME'].', '.$owner[0]['CEM_FNAME'].' '.$owner[0]['CEM_MNAME'] ?></p>
		<p><strong>Interred :</strong> <?= $interred[0]['INT_LNAME'].', '.$interred[0]['INT_FNAME'].' '.$interred[0]['INT_MNAME'] ?></p>
		<p><strong>Date	:</strong> <?= $interred[0]['INT_DTE'] ?></p>
		<p><strong>Age :</strong> <?= $interred[0]['INT_AGE'] ?></p>
	</div>
	<div class="col-xs-4">
		<p><strong>Type	:</strong> <?= $interred[0]['INT_TYP'] ?></p>
		<p><strong>Marker :</strong> <?= $interred[0]['INT_MARKER'] ?></p>
		<p><strong>Location :</strong> <?= $interred[0]['INT_LOC'] ?></p>
		<p><strong>Comments :</strong> <?= $interred[0]['INT_CMNT'] ?></p>
		<p><strong>Special Comments :</strong>  <?= $interred[0]['INT_SPC_CMNT'] ?></p>
	</div>
	<div class="col-xs-2">
		<p><strong>Interred Date :</strong> <?= $interred[0]['INT_INCOMPDTE'] ?></p>
	</div>
</div>
