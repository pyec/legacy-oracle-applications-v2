<div class="row">
	<div class="col-md-6">
		<h2>Update Asset</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>phonebook/update_asset" method="POST">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="name">Name</label>
						<input type="text" name="name" class="form-control" title="Asset Name" maxlength="50" value="<?= $asset[0]['name'] ?>" />
						<input type="hidden" name="id" value="<?= $asset[0]['id'] ?>" />
					</div>
					<div class="col-sm-4">
						<label for="business_unit">Business Unit</label>
						<input type="text" name="business_unit" class="form-control" title="Business Unit" maxlength="50" value="<?= $asset[0]['business_unit'] ?>" />
					</div>
					<div class="col-sm-4">
						<label for="section">Section</label>
						<input type="text" name="section" class="form-control" title="Section" maxlength="25" value="<?= $asset[0]['section'] ?>" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="phone_number">Phone Number</label>
						<input type="text" name="phone_number" class="form-control" title="Phone Number" maxlength="15" value="<?= $asset[0]['phone_number'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="fax_number">Fax Number</label>
						<input type="text" name="fax_number" class="form-control" title="Fax Number" maxlength="15" value="<?= $asset[0]['fax_number'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="civic_number">Civic Number</label>
						<input type="text" name="civic_number" class="form-control" title="Civic Number" maxlength="10" value="<?= $asset[0]['civic_number'] ?>" />
					</div>
					<div class="col-sm-4">
						<label for="address_description">Address Description</label>
						<input type="text" name="address_description" class="form-control" title="Address Description" maxlength="50" value="<?= $asset[0]['address_description'] ?>" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-5">
						<label for="street_name">Street Name</label>
						<input type="hidden" name="address_id" class="address_id" value="" value="<?= $asset[0]['address_id'] ?>" />
						<input type="text" name="street_name" class="form-control street_name" title="Street Name" maxlength="50" value="<?= $asset[0]['street_name'] ?>" />
					</div>
				</div>
			</div>
		<legend></legend>

		<div class="form-group">
			<div class="row">
				 <div class="col-sm-12">
					<a href="<?php echo base_url(); ?>phonebook/" class="btn btn-default no_label_fix">Back to phonebook</a>
					<input type="submit" class="btn btn-primary add_councillor pull-right" value="Update Asset" />
				 </div>
			</div>
		</div>
	</div>
</div>
</form>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Street Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<div class="modal_search">
								<label for="ASSET_search_box">Street Name</label>
								<input type="text" name="ASSET_search_box" id="" class="form-control ASSET_search_box" title="Street Name" autofocus maxlength="" />
								<p class="text-muted">(Select the name of the record you would like to use.)</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Name</th>
					</thead>
					<tbody class="modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->