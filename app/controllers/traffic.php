<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : traffic Signal System

Description : This system contains a host of surveys and information gathering 
			forms that have to do with traffic and motor vehicle safty in HRM.

  Functions : index(), 
  			ATR(), 
  			add_ATR(), 
  			update_ATR(),
  			delete_ATR()
  			LTI(), 
  			add_LTI(), 
  			update_LTI(),
  			delete_LTI()
  			MTMTC(), 
  			add_MTMTC(),
  			update_MTMTC(),
  			delete_MTMTC()
  			PS(), 
  			add_PS(), 
  			update_PS(),
  			delete_PS()
  			RSS(), 
  			add_RSS(), 
  			update_RSS(),
  			delete_RSS()
  			TSW(), 
  			add_TSW(), 
  			update_TSW(),
  			delete_TSW()
  			TSR(),
  			add_TSR(), 
  			update_TSR(),
  			delete_TSR(),
  			TSR_file_attachments(), 
  			traffic_regluations(),
  			add_traffic_regulation()

=========================================================================================================================*/
/*
	NOTE: the ATR system functions will be fully commented. The rest
*/
/*=========================================================================================================================*/
class Traffic extends CI_Controller 
{
/*-----------------------------------------------------------------------------------
	Home screen that gives you a list of surveys 
------------------------------------------------------------------------------------*/
	public function index()
	{
		$data['content'] = "pages/traffic/home_view";
		$this->load->view('master', $data);	
	}

/*           
============================================================================
ATR
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function ATR()
	{
		//Load Model
		$this->load->model('traffic_model');


		// this is the rel path to the view that we are going to load.
		$data['content'] = "pages/traffic/automated_traffic_recorder_view";
		// we load the master view and pass it data, the master view looks inside data to find what view to load and loads it between the header and footer.
		$this->load->view('master', $data);
	}

/*           
============================================================================
ATR_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function ATR_search()
	{
		// Load model
		$this->load->model('traffic_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation rules
		$this->form_validation->set_rules('record_number', 'Record Number', 'trim|max_length[15]|xss_clean|required');

		//Execute the validation, if true..
		if ($this->form_validation->run() == true)
		{
			// Confirm a record number exists in the post data.
			if (!empty($search_array['record_number']))
			{
				// use the model to search on the record number.
				$data['ATR_studies'] = $this->traffic_model->search_ATR_like_id(strtoupper($search_array['record_number']));
				if (!empty($data['ATR_studies'])){$data['ATR_studies'] = array_slice($data['ATR_studies'], 0, 500);}


			}
			// if there was no number to search on/
			else
			{
				// send the user back to the main ATR page.
				redirect('traffic/ATR');
			}
		}
		else
		{
			// if the validation fails send the user back to the main ATR page,
			redirect('traffic/ATR');
		}

		// If the search is successfully made then the execution won't reach a redirect and we can load the view with the records.
		$data['content'] = "pages/traffic/automated_traffic_recorder_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_ATR
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_ATR()
	{
		//Load Model
		$this->load->model('traffic_model');

		// Retrieve the post input.
		$post = $this->input->post(null, true);

		// If the post is empty then we know the user is looking to add a record 
		// so display the add record view.
		if (empty($post) || validation_errors())
		{
			$data['content'] = "pages/traffic/ATR_survey_view";
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('ATRS');

			$this->load->view('master', $data);
		}
		// ELse we know the form has been submitted so go ahead and process the post data.
		else
		{
			// We need to convert the "on" values of the checkboxes to T/F
			$post['ONE_WAY'] = (isset($post['ONE_WAY']) && $post['ONE_WAY'] == 'on' ? 'T':'F');
			$post['TWO_WAY'] = (isset($post['TWO_WAY']) && $post['TWO_WAY'] == 'on' ? 'T':'F');
			$post['SP_SURVEY'] = (isset($post['SP_SURVEY']) && $post['SP_SURVEY'] == 'on' ? 'T':'F');
			$post['ISIN_IND_PARK'] = (isset($post['ISIN_IND_PARK']) && $post['ISIN_IND_PARK'] == 'on' ? 'T':'F');

			// Define the forms validation rules.
			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required|max_length[10]');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Bwtween street', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'And street', 'numeric');
			$this->form_validation->set_rules('CIV_ADDR', 'Civic Number', 'numeric');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('AAWT1', 'AAWT One Way', 'numeric');
			$this->form_validation->set_rules('AAWT1OP', 'AAWT One Way OP', 'numeric');
			$this->form_validation->set_rules('AAWT2', 'Survey Date', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			
			if ($this->form_validation->run())
			{
				// If the validation passes seperate the file upload data from the survey. 
				$file_data = array();
				foreach ($post as $key => $value) 
				{
					$exploded_key = explode('_', $key);

					if ($exploded_key[0] == 'FILE')
					{
						$file_data[$key] = $value;
						unset($post[$key]);
					}
				}

				// attempt to insert the record.
				if($this->traffic_model->insert_ATR_study($post))
				{
					// If successful set a success message in flash data and redirect to the update view of that newly inserted record.
					$this->session->set_flashdata('message', 'The survey was added successfully');
					redirect('traffic/update_ATR/'.$post['REC_CODE']);
				}
				else
				{
					// If it fails, set an error message and redirect.
					$this->session->set_flashdata('error', 'The survey could not be added, please try again.');
					redirect('traffic/add_ATR');
				}
			}
			else
			{
				$this->add_ATR();
			}

		}
	}

/*           
============================================================================
update_ATR
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_ATR()
	{
		//Load Model
		$this->load->model('traffic_model');
		$this->load->model('EDL_model');
		$id_to_update = $this->uri->segment(3);

		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['survey'] = $this->traffic_model->get_ATR_study_by_id($id_to_update);

			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR2_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR3_CODE']);
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('ATRS');

			$data['attachment_count'] = $this->get_attachment_count($id_to_update, 'ATR');
			$data['content'] = 'pages/traffic/update_ATR_view';

			$this->load->view('master', $data);	
		}
		else
		{
			// If the validation passes seperate the file upload data from the survey. 
			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$post['ONE_WAY'] = (isset($post['ONE_WAY']) && $post['ONE_WAY'] == 'on' ? 'T':'F');
			$post['TWO_WAY'] = (isset($post['TWO_WAY']) && $post['TWO_WAY'] == 'on' ? 'T':'F');
			$post['SP_SURVEY'] = (isset($post['SP_SURVEY']) && $post['SP_SURVEY'] == 'on' ? 'T':'F');
			$post['ISIN_IND_PARK'] = (isset($post['ISIN_IND_PARK']) && $post['ISIN_IND_PARK'] == 'on' ? 'T':'F');

			if($this->traffic_model->update_ATR_study($post))
			{
				$this->session->set_flashdata('message', 'The survey record has been updated successfully.');
				redirect('traffic/update_ATR/'.$post['REC_CODE']);
			}
			else
			{
				$this->session->set_flashdata('error', 'The survey record could not be updated, please try again.');
				redirect('traffic/update_ATR/'.$post['REC_CODE']);
			}
		}
	}

/*           
============================================================================
delete_ATR
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_ATR()
	{
		//Load Model
		$this->load->model('traffic_model');
		$id_to_delete = $this->input->post('REC_CODE', true);

		if ($this->traffic_model->remove_ATR_study($id_to_delete))
		{
			$this->session->set_flashdata('message', 'The survey record has been removed successfully.');
			redirect('traffic/ATR');
		}
		else
		{
			$this->session->set_flashdata('error', 'The survey record coiuls not be removed, please try again.');
			redirect('traffic/ATR');
		}
	}

/*           
============================================================================
LTI
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function LTI()
	{
		//Load Model
		$this->load->model('traffic_model');
		$data['content'] = "pages/traffic/left_turn_investigation_view";
		$this->load->view('master', $data);
	}

	public function LTI_search()
	{
		// Load model
		$this->load->model('traffic_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('record_number', 'Record Number', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['record_number']))
			{
				$data['LTI_studies'] = $this->traffic_model->search_LTI_like_id(strtoupper($search_array['record_number']));
				if (!empty($data['LTI_studies'])){$data['LTI_studies'] = array_slice($data['LTI_studies'], 0, 500);}
			}
			else
			{
				redirect('traffic/LTI');
			}
		}
		else
		{
			redirect('traffic/LTI');
		}

		$data['content'] = "pages/traffic/left_turn_investigation_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_LTI
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_LTI()
	{
		//Load Model
		$this->load->model('traffic_model');
		$post = $this->input->post(null, true);

		if (empty($post) || validation_errors())
		{
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('LTIS');
			$data['content'] = "pages/traffic/LTI_survey_view";
			$this->load->view('master', $data);
		}
		else
		{
			$post['TIME_AM'] = (isset($post['TIME_AM']) && $post['TIME_AM'] == 'on' ? 'T':'F');
			$post['TIME_PM'] = (isset($post['TIME_PM']) && $post['TIME_PM'] == 'on' ? 'T':'F');
			$post['TIME_OP'] = (isset($post['TIME_OP']) && $post['TIME_OP'] == 'on' ? 'T':'F');

			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STR4_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STR5_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->insert_LTI_study($post))
				{
					$this->session->set_flashdata('message', 'The survey was added successfully.');
					redirect('traffic/update_LTI/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('message', 'There was an error adding the survey record, please try again.');
					redirect('traffic/add_LTI');
				}
			}
			else
			{
				$this->add_LTI();
			}
		}
	}

/*           
============================================================================
update_LTI
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_LTI()
	{
		//Load Model
		$this->load->model('traffic_model');
		$this->load->model('EDL_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['survey'] = $this->traffic_model->get_LTI_study_by_id($id_to_update);
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('LTIS');

			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR2_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR3_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR4_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR5_CODE']);

			$data['attachment_count'] = $this->get_attachment_count($id_to_update, 'LTI');
			
			$data['content'] = 'pages/traffic/update_LTI_view';
			$this->load->view('master', $data);
		}
		else
		{
			$post['TIME_AM'] = (isset($post['TIME_AM']) && $post['TIME_AM'] == 'on' ? 'T':'F');
			$post['TIME_PM'] = (isset($post['TIME_PM']) && $post['TIME_PM'] == 'on' ? 'T':'F');
			$post['TIME_OP'] = (isset($post['TIME_OP']) && $post['TIME_OP'] == 'on' ? 'T':'F');

			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STR4_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STR5_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->update_LTI_study($post))
				{
					$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' has been updated.');
					redirect('traffic/update_LTI/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('traffic/update_LTI/'.$post['REC_CODE']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/update_LTI/'.$post['REC_CODE']);
			}
		}
	}

/*           
============================================================================
delete_LTI
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_LTI()
	{
		//Load Model
		$this->load->model('traffic_model');

		$id_to_delete = $this->input->post('REC_CODE');

		if (!empty($id_to_delete))
		{
			if ($this->traffic_model->remove_LTI_study($id_to_delete))
			{
				$this->session->set_flashdata('message', 'The survey was successfully deleted.');
				redirect('traffic/LTI');
			}
			else
			{
				$this->session->set_flashdata('error', 'The survey could not be deleted.');
				redirect('traffic/LTI');
			}
		}
		else
		{
			redirect('traffic/LTI');
		}
	}

/*           
============================================================================
MTMTC
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function MTMTC()
	{
		//Load Model
		$this->load->model('traffic_model');

	
		$data['content'] = "pages/traffic/manual_turning_movement_traffic_counts_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
MTMTC_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function MTMTC_search()
	{
		// Load model
		$this->load->model('traffic_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('record_number', 'Record Number', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['record_number']))
			{
				$data['MTMTC_studies'] = $this->traffic_model->search_MTMTC_study_like_id(strtoupper($search_array['record_number']));
				if (!empty($data['MTMTC_studies'])){$data['MTMTC_studies'] = array_slice($data['MTMTC_studies'], 0, 500);}
			}
			else
			{
				redirect('traffic/MTMTC');
			}
		}
		else
		{
			redirect('traffic/MTMTC');
		}

		$data['content'] = "pages/traffic/manual_turning_movement_traffic_counts_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_MTMTC
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_MTMTC()
	{
		//Load Model
		$this->load->model('traffic_model');
		$post = $this->input->post(null, true);

		if (empty($post) || validation_errors())
		{
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('MTMT');
			$data['content'] = "pages/traffic/MTMTC_survey_view";
			$this->load->view('master', $data);
		}
		else
		{
			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$post['S_AM'] = (isset($post['S_AM']) && $post['S_AM'] == 'on' ? 'T':'F');
			$post['S_PM'] = (isset($post['S_PM']) && $post['S_PM'] == 'on' ? 'T':'F');
			$post['S_OP'] = (isset($post['S_OP']) && $post['S_OP'] == 'on' ? 'T':'F');

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STR4_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STR5_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('PEAK_HV', 'Peak Hour Volume', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->insert_MTMTC_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been added successfull added.');
					redirect('traffic/update_MTMTC/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error adding the record, please try again.');
					redirect('traffic/add_MTMTC');
				}
			}
			else
			{
				$this->add_MTMTC();
			}
		}
		
	}

/*           
============================================================================
update_MTMTC
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_MTMTC()
	{
		//Load Model
		$this->load->model('traffic_model');
		$this->load->model('EDL_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('MTMT');

			$data['survey'] = $this->traffic_model->get_MTMTC_study_by_id($id_to_update);

			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR2_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR3_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR4_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR5_CODE']);

			$data['attachment_count'] = $this->get_attachment_count($id_to_update, 'MTMTC');
			
			$data['content'] = 'pages/traffic/update_MTMTC_view';
			$this->load->view('master', $data);
		}
		else
		{
			$post['S_AM'] = (isset($post['S_AM']) && $post['S_AM'] == 'on' ? 'T':'F');
			$post['S_PM'] = (isset($post['S_PM']) && $post['S_PM'] == 'on' ? 'T':'F');
			$post['S_OP'] = (isset($post['S_OP']) && $post['S_OP'] == 'on' ? 'T':'F');

			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STR4_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STR5_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('PEAK_HV', 'Peak Hour Volume', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->update_MTMTC_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been updated successfully.');
					redirect('traffic/update_MTMTC/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('traffic/update_MTMTC/'.$post['REC_CODE']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/update_MTMTC/'.$post['REC_CODE']);
			}
		}
	}

/*           
============================================================================
delete_MTMTC
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_MTMTC()
	{
		//Load Model
		$this->load->model('traffic_model');

		$id_to_delete = $this->input->post('REC_CODE');

		if (!empty($id_to_delete))
		{
			if ($this->traffic_model->remove_MTMTC_study($id_to_delete))
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' has been removed.');
				redirect('traffic/MTMTC');
			}
			else
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' could not be removed, please try again.');
				redirect('traffic/MTMTC');
			}
		}
	}

/*           
============================================================================
PS
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function PS()
	{
		//Load Model
		$this->load->model('traffic_model');
		$data['content'] = "pages/traffic/pedestrian_studies_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
PS_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function PS_search()
	{
		// Load model
		$this->load->model('traffic_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('record_number', 'Record Number', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['record_number']))
			{
				$data['PS_studies'] = $this->traffic_model->search_PS_study_like_id(strtoupper($search_array['record_number']));
				if (!empty($data['PS_studies'])){$data['PS_studies'] = array_slice($data['PS_studies'], 0, 500);}

			}
			else
			{
				redirect('traffic/PS');
			}
		}
		else
		{
			redirect('traffic/PS');
		}

		$data['content'] = "pages/traffic/pedestrian_studies_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_PS
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_PS()
	{
		//Load Model
		$this->load->model('traffic_model');
		$post = $this->input->post(null, true);

		if (empty($post) || validation_errors())
		{
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('PEDS');

			$data['content'] = "pages/traffic/PS_survey_view";
			$this->load->view('master', $data);
		}
		else
		{
			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$post['TIME_AM'] = (isset($post['TIME_AM']) && $post['TIME_AM'] == 'on' ? 'T':'F');
			$post['TIME_PM'] = (isset($post['TIME_PM']) && $post['TIME_PM'] == 'on' ? 'T':'F');
			$post['TIME_OP'] = (isset($post['TIME_OP']) && $post['TIME_OP'] == 'on' ? 'T':'F');

			$post['SIGNAL'] = (isset($post['SIGNAL']) && $post['SIGNAL'] == 'on' ? 'T':'F');
			$post['UNCONT'] = (isset($post['UNCONT']) && $post['UNCONT'] == 'on' ? 'T':'F');
			$post['CONT'] = (isset($post['CONT']) && $post['CONT'] == 'on' ? 'T':'F');
			$post['RA5'] = (isset($post['RA5']) && $post['RA5'] == 'on' ? 'T':'F');
			$post['HALFSIGNAL'] = (isset($post['HALFSIGNAL']) && $post['HALFSIGNAL'] == 'on' ? 'T':'F');

			$post['SIGNAL_REQ'] = (isset($post['SIGNAL_REQ']) && $post['SIGNAL_REQ'] == 'on' ? 'T':'F');
			$post['UNCONT_REQ'] = (isset($post['UNCONT_REQ']) && $post['UNCONT_REQ'] == 'on' ? 'T':'F');
			$post['CONT_REQ'] = (isset($post['CONT_REQ']) && $post['CONT_REQ'] == 'on' ? 'T':'F');
			$post['RA5_REQ'] = (isset($post['RA5_REQ']) && $post['RA5_REQ'] == 'on' ? 'T':'F');
			$post['HALFSIGNAL_REQ'] = (isset($post['HALFSIGNAL_REQ']) && $post['HALFSIGNAL_REQ'] == 'on' ? 'T':'F');

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STR4_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STR5_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');
			$this->form_validation->set_rules('STRB2_CODE', 'Street B 1', 'numeric');
			$this->form_validation->set_rules('STRB3_CODE', 'Street B 2', 'numeric');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->insert_PS_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been added successfull added.');
					redirect('traffic/update_PS/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error adding the record, please try again.');
					redirect('traffic/add_PS');
				}
			}
			else
			{
				$this->add_PS();
			}
		}
		
	}

/*           
============================================================================
update_PS
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_PS()
	{
		//Load Model
		$this->load->model('traffic_model');
		$this->load->model('EDL_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('PEDS');
			$data['survey'] = $this->traffic_model->get_PS_study_by_id($id_to_update);

			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR2_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR3_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR4_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR5_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STRB2_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STRB3_CODE']);

			$data['attachment_count'] = $this->get_attachment_count($id_to_update, 'PS');

			$data['content'] = 'pages/traffic/update_PS_view';
			$this->load->view('master', $data);
		}
		else
		{
			$post['TIME_AM'] = (isset($post['TIME_AM']) && $post['TIME_AM'] == 'on' ? 'T':'F');
			$post['TIME_PM'] = (isset($post['TIME_PM']) && $post['TIME_PM'] == 'on' ? 'T':'F');
			$post['TIME_OP'] = (isset($post['TIME_OP']) && $post['TIME_OP'] == 'on' ? 'T':'F');

			$post['SIGNAL'] = (isset($post['SIGNAL']) && $post['SIGNAL'] == 'on' ? 'T':'F');
			$post['UNCONT'] = (isset($post['UNCONT']) && $post['UNCONT'] == 'on' ? 'T':'F');
			$post['CONT'] = (isset($post['CONT']) && $post['CONT'] == 'on' ? 'T':'F');
			$post['RA5'] = (isset($post['RA5']) && $post['RA5'] == 'on' ? 'T':'F');
			$post['HALFSIGNAL'] = (isset($post['HALFSIGNAL']) && $post['HALFSIGNAL'] == 'on' ? 'T':'F');

			$post['SIGNAL_REQ'] = (isset($post['SIGNAL_REQ']) && $post['SIGNAL_REQ'] == 'on' ? 'T':'F');
			$post['UNCONT_REQ'] = (isset($post['UNCONT_REQ']) && $post['UNCONT_REQ'] == 'on' ? 'T':'F');
			$post['CONT_REQ'] = (isset($post['CONT_REQ']) && $post['CONT_REQ'] == 'on' ? 'T':'F');
			$post['RA5_REQ'] = (isset($post['RA5_REQ']) && $post['RA5_REQ'] == 'on' ? 'T':'F');
			$post['HALFSIGNAL_REQ'] = (isset($post['HALFSIGNAL_REQ']) && $post['HALFSIGNAL_REQ'] == 'on' ? 'T':'F');

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STR4_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STR5_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');
			$this->form_validation->set_rules('STRB2_CODE', 'Street B 1', 'numeric');
			$this->form_validation->set_rules('STRB3_CODE', 'Street B 2', 'numeric');

			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}


			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->update_PS_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been updated successfull added.');
					redirect('traffic/update_PS/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('traffic/update_PS/'.$post['REC_CODE']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/update_PS/'.$post['REC_CODE']);
			}
		}
	}

/*           
============================================================================
delete_PS
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_PS()
	{
		//Load Model
		$this->load->model('traffic_model');

		$id_to_delete = $this->input->post('REC_CODE');

		if (!empty($id_to_delete))
		{
			if ($this->traffic_model->remove_PS_study($id_to_delete))
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' has been removed.');
				redirect('traffic/PS');
			}
			else
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' could not be removed, please try again.');
				redirect('traffic/PS');
			}
		}
	}

/*           
============================================================================
RSS
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function RSS()
	{
		//Load Model
		$this->load->model('traffic_model');
		$data['content'] = "pages/traffic/radar_spot_speed_view";
		$this->load->view('master', $data);
	}
/*           
============================================================================
RSS_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function RSS_search()
	{
		// Load model
		$this->load->model('traffic_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('record_number', 'Record Number', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['record_number']))
			{
				$data['RSS_studies'] = $this->traffic_model->search_RSS_study_like_id(strtoupper($search_array['record_number']));
				if(!empty($data['RSS_studies'])) {$data['RSS_studies'] = array_slice($data['RSS_studies'], 0, 500);}

			}
			else
			{
				redirect('traffic/RSS');
			}
		}
		else
		{
			redirect('traffic/RSS');
		}

		$data['content'] = "pages/traffic/radar_spot_speed_view";
		$this->load->view('master', $data);
	}
/*           
============================================================================
add_RSS
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_RSS()
	{
		//Load Model
		$this->load->model('traffic_model');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('RSSS');
			$data['content'] = "pages/traffic/RSS_survey_view";
			$this->load->view('master', $data);
		}
		else
		{
			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$post['TIME_AM'] = (isset($post['TIME_AM']) && $post['TIME_AM'] == 'on' ? 'T':'F');
			$post['TIME_PM'] = (isset($post['TIME_PM']) && $post['TIME_PM'] == 'on' ? 'T':'F');
			$post['TIME_OP'] = (isset($post['TIME_OP']) && $post['TIME_OP'] == 'on' ? 'T':'F');

			$post['ONE_WAY'] = (isset($post['ONE_WAY']) && $post['ONE_WAY'] == 'on' ? 'T':'F');
			$post['TWO_WAY'] = (isset($post['TWO_WAY']) && $post['TWO_WAY'] == 'on' ? 'T':'F');

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');
			$this->form_validation->set_rules('AVG_SPEED', 'Average Speed', 'numeric');
			$this->form_validation->set_rules('PER85TH', '85% Speed', 'numeric');
			$this->form_validation->set_rules('CIV_ADDR', 'Civic Address', 'numeric');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->insert_RSS_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been added successfull added.');
					redirect('traffic/update_RSS/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error adding the record, please try again.');
					redirect('traffic/add_RSS');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/add_RSS');
			}
		}
	}

/*           
============================================================================
update_RSS
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_RSS()
	{
		//Load Model
		$this->load->model('traffic_model');
		$this->load->model('EDL_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['surveyor_list'] = $this->traffic_model->get_surveyor_list('RSSS');

			$data['survey'] = $this->traffic_model->get_RSS_study_by_id($id_to_update);

			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR2_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR3_CODE']);

			$data['attachment_count'] = $this->get_attachment_count($id_to_update, 'RSS');

			$data['content'] = 'pages/traffic/update_RSS_view';
			$this->load->view('master', $data);
		}
		else
		{
			$post['TIME_AM'] = (isset($post['TIME_AM']) && $post['TIME_AM'] == 'on' ? 'T':'F');
			$post['TIME_PM'] = (isset($post['TIME_PM']) && $post['TIME_PM'] == 'on' ? 'T':'F');
			$post['TIME_OP'] = (isset($post['TIME_OP']) && $post['TIME_OP'] == 'on' ? 'T':'F');

			$post['ONE_WAY'] = (isset($post['ONE_WAY']) && $post['ONE_WAY'] == 'on' ? 'T':'F');
			$post['TWO_WAY'] = (isset($post['TWO_WAY']) && $post['TWO_WAY'] == 'on' ? 'T':'F');

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');
			$this->form_validation->set_rules('AVG_SPEED', 'Average Speed', 'numeric');
			$this->form_validation->set_rules('PER85TH', '85% Speed', 'numeric');
			$this->form_validation->set_rules('CIV_ADDR', 'Civic Address', 'numeric');


			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}


			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->update_RSS_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been updated successfull added.');
					redirect('traffic/update_RSS/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('traffic/update_RSS/'.$post['REC_CODE']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/update_RSS/'.$post['REC_CODE']);
			}
		}
	}

/*           
============================================================================
delete_RSS
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_RSS()
	{
		//Load Model
		$this->load->model('traffic_model');

		$id_to_delete = $this->input->post('REC_CODE');

		if (!empty($id_to_delete))
		{
			if ($this->traffic_model->remove_RSS_study($id_to_delete))
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' has been removed.');
				redirect('traffic/RSS');
			}
			else
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' could not be removed, please try again.');
				redirect('traffic/RSS');
			}
		}
	}

/*           
============================================================================
TSW
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function TSW()
	{
		//Load Model
		$this->load->model('traffic_model');

		$data['content'] = "pages/traffic/traffic_signal_warrants_view";
		$this->load->view('master', $data);
	}
	public function TSW_search()
	{
		// Load model
		$this->load->model('traffic_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('record_number', 'Record Number', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['record_number']))
			{
				$data['TSW_studies'] = $this->traffic_model->search_TSW_study_like_id(strtoupper($search_array['record_number']));
				if(!empty($data['TSW_studies'])) {$data['TSW_studies'] = array_slice($data['TSW_studies'], 0, 500);}

			}
			else
			{
				redirect('traffic/TSW');
			}
		}
		else
		{
			redirect('traffic/TSW');
		}

		$data['content'] = "pages/traffic/traffic_signal_warrants_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_TSW
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_TSW()
	{
		//Load Model
		$this->load->model('traffic_model');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['content'] = "pages/traffic/TSW_survey_view";
			$this->load->view('master', $data);
		}
		else
		{
			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STR4_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STR5_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('PRI_PTS', 'Pri Pts', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->insert_TSW_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been added successfull added.');
					redirect('traffic/update_TSW/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error adding the record, please try again.');
					redirect('traffic/add_TSW');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/add_TSW');
			}
		}
	}

/*           
============================================================================
update_TSW
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_TSW()
	{
		//Load Model
		$this->load->model('traffic_model');
		$this->load->model('EDL_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['survey'] = $this->traffic_model->get_TSW_study_by_id($id_to_update);

			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR2_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR3_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR4_CODE']);
			$data['streets'][] = $this->EDL_model->get_street_by_id($data['survey']['STR5_CODE']);

			$data['attachment_count'] = $this->get_attachment_count($id_to_update, 'TSW');

			$data['content'] = 'pages/traffic/update_TSW_view';
			$this->load->view('master', $data);
		}
		else
		{

			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REC_CODE', 'Record ID', 'required');
			$this->form_validation->set_rules('SURVEY_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('STR_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STR2_CODE', 'Street 1', 'numeric');
			$this->form_validation->set_rules('STR3_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STR4_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STR5_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('PRI_PTS', 'Pri Pts', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->update_TSW_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been updated successfull added.');
					redirect('traffic/update_TSW/'.$post['REC_CODE']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('traffic/update_TSW/'.$post['REC_CODE']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/update_TSW/'.$post['REC_CODE']);
			}
		}
	}

/*           
============================================================================
delete_TSW
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_TSW()
	{
		//Load Model
		$this->load->model('traffic_model');

		$id_to_delete = $this->input->post('REC_CODE');

		if (!empty($id_to_delete))
		{
			if ($this->traffic_model->remove_TSW_study($id_to_delete))
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' has been removed.');
				redirect('traffic/TSW');
			}
			else
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' could not be removed, please try again.');
				redirect('traffic/TSW');
			}
		}
	}

/*           
============================================================================
TSR
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function TSR()
	{
		//Load Model
		$this->load->model('traffic_model');

		$data['content'] = "pages/traffic/traffic_survey_requests_view";
		$this->load->view('master', $data);
	}

public function TSR_search()
	{
		// Load model
		$this->load->model('traffic_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('record_number', 'Record Number', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['record_number']))
			{
				$data['TSR_studies'] = $this->traffic_model->search_TSR_study_like_id(strtoupper($search_array['record_number']));
				if(!empty($data['TSR_studies'])) {$data['TSR_studies'] = array_slice($data['TSR_studies'], 0, 500);}

			}
			else
			{
				redirect('traffic/TSR');
			}
		}
		else
		{
			redirect('traffic/TSR');
		}

		$data['content'] = "pages/traffic/traffic_survey_requests_view";
		$this->load->view('master', $data);
	}
/*           
============================================================================
add_TSR
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_TSR()
	{
		//Load Model
		$this->load->model('traffic_model');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['content'] = "pages/traffic/add_TSR_view";
			$this->load->view('master', $data);
		}
		else
		{
			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REQ_NUM', 'Record ID', 'required');
			$this->form_validation->set_rules('REQ_DATE', 'Request Date', 'required');
			$this->form_validation->set_rules('PRI_RANK', 'Priority', 'required|numeric');
			$this->form_validation->set_rules('MAIN_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STRA_CODE', 'Street A', 'numeric');
			$this->form_validation->set_rules('STRA2_CODE', 'Street A1', 'numeric');
			$this->form_validation->set_rules('STRA3_CODE', 'Street A2', 'numeric');
			$this->form_validation->set_rules('STRA4_CODE', 'Street A3', 'numeric');
			$this->form_validation->set_rules('STRB_CODE', 'Street A4', 'numeric');
			$this->form_validation->set_rules('STRB_CODE', 'Street B', 'numeric');
			$this->form_validation->set_rules('STRB2_CODE', 'Street B2', 'numeric');
			$this->form_validation->set_rules('CIV_ADDR', 'Civic Address', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			$post['B2WC'] = (isset($post['B2WC']) && $post['B2WC'] == 'on' ? 'T':'F');
			$post['B12WC'] = (isset($post['B12WC']) && $post['B12WC'] == 'on' ? 'T':'F');
			$post['PEDS'] = (isset($post['PEDS']) && $post['PEDS'] == 'on' ? 'T':'F');
			$post['RSSS'] = (isset($post['RSSS']) && $post['RSSS'] == 'on' ? 'T':'F');
			$post['SVS'] = (isset($post['SVS']) && $post['SVS'] == 'on' ? 'T':'F');
			$post['CLASS'] = (isset($post['CLASS']) && $post['CLASS'] == 'on' ? 'T':'F');
			$post['GAPS'] = (isset($post['GAPS']) && $post['GAPS'] == 'on' ? 'T':'F');
			$post['SIG_WAR'] = (isset($post['SIG_WAR']) && $post['SIG_WAR'] == 'on' ? 'T':'F');
			$post['CONT_WAR'] = (isset($post['CONT_WAR']) && $post['CONT_WAR'] == 'on' ? 'T':'F');
			$post['LTIS'] = (isset($post['LTIS']) && $post['LTIS'] == 'on' ? 'T':'F');
			$post['LTHL'] = (isset($post['LTHL']) && $post['LTHL'] == 'on' ? 'T':'F');
			$post['SIG_INT'] = (isset($post['SIG_INT']) && $post['SIG_INT'] == 'on' ? 'T':'F');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->insert_TSR_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been added successfull added.');
					redirect('traffic/update_TSR/'.$post['REQ_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error adding the record, please try again.');
					redirect('traffic/add_TSR');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/add_TSR');
			}
		}
	}

/*           
============================================================================
update_TSR
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_TSR()
	{
		//Load Model
		$this->load->model('traffic_model');
		$this->load->model('EDL_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['survey'] = $this->traffic_model->get_TSR_study_by_id($id_to_update);

			$data['streets'][] = (!empty($data['survey']['MAIN_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['MAIN_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRA_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRA_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRA2_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRA2_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRA3_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRA3_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRA4_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRA4_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRB_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRB_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRB2_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRB2_CODE']):'');

			$data['attachment_count'] = $this->get_attachment_count($id_to_update, 'TSR');

			$data['content'] = 'pages/traffic/update_TSR_view';
			$this->load->view('master', $data);
		}
		else
		{

			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REQ_NUM', 'Record ID', 'required');
			$this->form_validation->set_rules('REQ_DATE', 'Survey Date', 'required');
			$this->form_validation->set_rules('PRI_RANK', 'Street', 'required|numeric');
			$this->form_validation->set_rules('MAIN_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STRA_CODE', 'Street A', 'numeric');
			$this->form_validation->set_rules('STRA2_CODE', 'Street A1', 'numeric');
			$this->form_validation->set_rules('STRA3_CODE', 'Street A2', 'numeric');
			$this->form_validation->set_rules('STRA4_CODE', 'Street A3', 'numeric');
			$this->form_validation->set_rules('STRB_CODE', 'Street A4', 'numeric');
			$this->form_validation->set_rules('STRB_CODE', 'Street B', 'numeric');
			$this->form_validation->set_rules('STRB2_CODE', 'Street B2', 'numeric');
			$this->form_validation->set_rules('CIV_ADDR', 'Civic Address', 'numeric');
			$this->form_validation->set_rules('X_COORD', 'X Coord', 'numeric');
			$this->form_validation->set_rules('Y_COORD', 'Y Coord', 'numeric');

			$post['B2WC'] = (isset($post['B2WC']) && $post['B2WC'] == 'on' ? 'T':'F');
			$post['B12WC'] = (isset($post['B12WC']) && $post['B12WC'] == 'on' ? 'T':'F');
			$post['PEDS'] = (isset($post['PEDS']) && $post['PEDS'] == 'on' ? 'T':'F');
			$post['RSSS'] = (isset($post['RSSS']) && $post['RSSS'] == 'on' ? 'T':'F');
			$post['SVS'] = (isset($post['SVS']) && $post['SVS'] == 'on' ? 'T':'F');
			$post['CLASS'] = (isset($post['CLASS']) && $post['CLASS'] == 'on' ? 'T':'F');
			$post['GAPS'] = (isset($post['GAPS']) && $post['GAPS'] == 'on' ? 'T':'F');
			$post['SIG_WAR'] = (isset($post['SIG_WAR']) && $post['SIG_WAR'] == 'on' ? 'T':'F');
			$post['CONT_WAR'] = (isset($post['CONT_WAR']) && $post['CONT_WAR'] == 'on' ? 'T':'F');
			$post['LTIS'] = (isset($post['LTIS']) && $post['LTIS'] == 'on' ? 'T':'F');
			$post['LTHL'] = (isset($post['LTHL']) && $post['LTHL'] == 'on' ? 'T':'F');
			$post['SIG_INT'] = (isset($post['SIG_INT']) && $post['SIG_INT'] == 'on' ? 'T':'F');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->update_TSR_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been updated successfull updated.');
					redirect('traffic/update_TSR/'.$post['REQ_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('traffic/update_TSR/'.$post['REQ_NUM']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/update_TSR/'.$post['REQ_NUM']);
			}
		}
	}

/*           
============================================================================
delete_TSR
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_TSR()
	{
		//Load Model
		$this->load->model('traffic_model');

		$id_to_delete = $this->input->post('REQ_NUM');

		if (!empty($id_to_delete))
		{
			if ($this->traffic_model->remove_TSR_study($id_to_delete))
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' has been removed.');
				redirect('traffic/TSR');
			}
			else
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' could not be removed, please try again.');
				redirect('traffic/TSR');
			}
		}
	}

/*           
============================================================================
TSR_file_attachments
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function TSR_file_attachments()
	{
		//Load Model
		// $this->load->model('traffic_model');
		$data['content'] = "pages/traffic/TSR_file_attachments_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
TR
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function TR()
	{
		//Load Model
		$this->load->model('traffic_model');

		$data['content'] = "pages/traffic/traffic_regulations_view";
		
		$this->load->view('master', $data);
	}

	public function TR_search()
	{
		// Load model
		$this->load->model('traffic_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('record_number', 'Record Number', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['record_number']))
			{
				$data['TR_studies'] = $this->traffic_model->search_TR_study_like_id(strtoupper($search_array['record_number']));
				if(!empty($data['TR_studies'])) {$data['TR_studies'] = array_slice($data['TR_studies'], 0, 500);}
			}
			else
			{
				redirect('traffic/TR');
			}
		}
		else
		{
			redirect('traffic/TR');
		}

		$data['content'] = "pages/traffic/traffic_regulations_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_TR
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_TR()
	{
		//Load Model
		$this->load->model('traffic_model');
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['content'] = "pages/traffic/add_TR_view";
			$this->load->view('master', $data);
		}
		else
		{
			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REG_NUM', 'Regulation ID', 'required');
			$this->form_validation->set_rules('ISSUE_DATE', 'Issue Date', 'required');
			$this->form_validation->set_rules('STR_REG', 'Region', 'required');
			$this->form_validation->set_rules('MAIN_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STRA_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STRA2_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STRA3_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STRA4_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('STRB_CODE', 'Street B', 'numeric');
			$this->form_validation->set_rules('STRB2_CODE', 'Street B2', 'numeric');
			$this->form_validation->set_rules('CIV_ADDR', 'Civic Address', 'numeric');

			$post['SIGNS'] = (isset($post['SIGNS']) && $post['SIGNS'] == 'on' ? 'T':'F');
			$post['SIGNALS'] = (isset($post['SIGNALS']) && $post['SIGNALS'] == 'on' ? 'T':'F');
			$post['TRANSIT'] = (isset($post['TRANSIT']) && $post['TRANSIT'] == 'on' ? 'T':'F');
			$post['MARKINGS'] = (isset($post['MARKINGS']) && $post['MARKINGS'] == 'on' ? 'T':'F');
			$post['METERS'] = (isset($post['METERS']) && $post['METERS'] == 'on' ? 'T':'F');
			$post['TAXI'] = (isset($post['TAXI']) && $post['TAXI'] == 'on' ? 'T':'F');
			$post['OTHER'] = (isset($post['OTHER']) && $post['OTHER'] == 'on' ? 'T':'F');

			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			if ($this->form_validation->run() == true)
			{
				
				if ($this->traffic_model->insert_TR_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been added successfull added.');
					redirect('traffic/update_TR/'.$post['REG_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error adding the record, please try again.');
					redirect('traffic/add_TR');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/add_TR');
			}
		}
	}

/*           
============================================================================
update_TR
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_TR()
	{
		//Load Model
		$this->load->model('traffic_model');
		$this->load->model('EDL_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['survey'] = $this->traffic_model->get_TR_study_by_id($id_to_update);

			$data['streets'][] = (!empty($data['survey']['MAIN_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['MAIN_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRA_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRA_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRA2_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRA2_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRA3_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRA3_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRA4_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRA4_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRB_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRB_CODE']):'');
			$data['streets'][] = (!empty($data['survey']['STRB2_CODE']) ? $this->EDL_model->get_street_by_id($data['survey']['STRB2_CODE']):'');

			$data['attachment_count'] = $this->get_attachment_count($id_to_update, 'TR');

			$data['content'] = 'pages/traffic/update_TR_view';
			
			$this->load->view('master', $data);
		}
		else
		{

			$file_data = array();

			foreach ($post as $key => $value) 
			{
				$exploded_key = explode('_', $key);

				if ($exploded_key[0] == 'FILE')
				{
					$file_data[$key] = $value;
					unset($post[$key]);
				}
			}

			$this->form_validation->set_rules('REG_NUM', 'Regulation ID', 'required');
			$this->form_validation->set_rules('ISSUE_DATE', 'Issue Date', 'required');
			$this->form_validation->set_rules('STR_REG', 'Region', 'required');
			$this->form_validation->set_rules('MAIN_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STRA_CODE', 'Street', 'required|numeric');
			$this->form_validation->set_rules('STRA2_CODE', 'Street 2', 'numeric');
			$this->form_validation->set_rules('STRA3_CODE', 'Street 3', 'numeric');
			$this->form_validation->set_rules('STRA4_CODE', 'Street 4', 'numeric');
			$this->form_validation->set_rules('STRB_CODE', 'Street B', 'numeric');
			$this->form_validation->set_rules('STRB2_CODE', 'Street B2', 'numeric');
			$this->form_validation->set_rules('CIV_ADDR', 'Civic Address', 'numeric');

			$post['SIGNS'] = (isset($post['SIGNS']) && $post['SIGNS'] == 'on' ? 'T':'F');
			$post['SIGNALS'] = (isset($post['SIGNALS']) && $post['SIGNALS'] == 'on' ? 'T':'F');
			$post['TRANSIT'] = (isset($post['TRANSIT']) && $post['TRANSIT'] == 'on' ? 'T':'F');
			$post['MARKINGS'] = (isset($post['MARKINGS']) && $post['MARKINGS'] == 'on' ? 'T':'F');
			$post['METERS'] = (isset($post['METERS']) && $post['METERS'] == 'on' ? 'T':'F');
			$post['TAXI'] = (isset($post['TAXI']) && $post['TAXI'] == 'on' ? 'T':'F');
			$post['OTHER'] = (isset($post['OTHER']) && $post['OTHER'] == 'on' ? 'T':'F');

			if ($this->form_validation->run() == true)
			{
				if ($this->traffic_model->update_TR_study($post))
				{
					$this->session->set_flashdata('message', 'The survey record has been updated successfull added.');
					redirect('traffic/update_TR/'.$post['REG_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('traffic/update_TR/'.$post['REG_NUM']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('traffic/update_TR/'.$post['REG_NUM']);
			}
		}
	}

/*           
============================================================================
delete_TR
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_TR()
	{
		//Load Model
		$this->load->model('traffic_model');

		$id_to_delete = $this->input->post('REG_NUM');

		if (!empty($id_to_delete))
		{
			if ($this->traffic_model->remove_TR_study($id_to_delete))
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' has been removed.');
				redirect('traffic/TR');
			}
			else
			{
				$this->session->set_flashdata('message', 'Survey '.$id_to_delete.' could not be removed, please try again.');
				redirect('traffic/TR');
			}
		}
	}

	public  function do_upload()
	{
		$id = $this->input->post('REC_CODE');
		$form = $this->input->post('FORM');
		$config['upload_path'] = './assets/uploads/Traffic_Survey_System/'.$form;
		$config['file_name'] = $id.'_';
		$config['allowed_types'] = 'pdf';
		$config['max_size']	= '1000';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$this->session->set_flashdata('error', $this->upload->display_errors());
	
			redirect('traffic/update_'.$form.'/'.$id);
		}
		else
		{
			$this->session->set_flashdata('message', 'The file upload was successful.');

			redirect('traffic/update_'.$form.'/'.$id);
		}
	}

	// ------------------------------------------------------------------------
	// AJAX get attachment
	public function ajax_does_survey_have_attachments()
	{
		//Load Model
		$this->load->model('traffic_model');

		$id = $this->input->post('id');
		$form = $this->input->post('form');

		$count = 0;
		$loop = true;
		while($loop)
		{
			if ($count > 0)
			{
				if (file_exists('assets/uploads/Traffic_Survey_System/'.$form.'/'.$id.'_'.$count.'.pdf'))
				{	
					$count++;
				}
				else
				{
					$loop = false;
				}
			}
			else
			{
				if (file_exists('assets/uploads/Traffic_Survey_System/'.$form.'/'.$id.'_.pdf'))
				{	
					$count++;
				}
				else
				{
					$loop = false;				 
				}
			}
		}
		
		echo json_encode(--$count);
	}

	public function get_attachment_count($id, $form)
	{
		$count = 0;
		$loop = true;
		while($loop)
		{
			if ($count > 0)
			{
				if (file_exists('assets/uploads/Traffic_Survey_System/'.$form.'/'.$id.'_'.$count.'.pdf'))
				{	
					$count++;
				}
				else
				{
					$loop = false;
				}
			}
			else
			{
				if (file_exists('assets/uploads/Traffic_Survey_System/'.$form.'/'.$id.'_.pdf'))
				{	
					$count++;
				}
				else
				{
					$loop = false;				 
				}
			}
		}

		return $count;
	}

	public function delete_attachments()
	{
		$form = $this->uri->segment(3);
		$id = $this->uri->segment(4);

		// Using a loop calculate how many files are attached to the record.
		$count = 0;
		$loop = true;
		while($loop)
		{
			if ($count > 0)
			{
				if (file_exists('assets/uploads/Traffic_Survey_System/'.$form.'/'.$id.'_'.$count.'.pdf'))
				{	
					$count++;
				}
				else
				{
					$loop = false;
				}
			}
			else
			{
				if (file_exists('assets/uploads/Traffic_Survey_System/'.$form.'/'.$id.'_.pdf'))
				{	
					$count++;
				}
				else
				{
					$loop = false;			
				}
			}
		}

		if ($count == 0)
		{
			$this->session->set_flashdata('message', 'There were no attachments to delete.');
			redirect('traffic/update_'.$form.'/'.$id);
		}
		else
		{
			// using another loop unlink() (delete) the files attached
			for ($i=0; $i < $count; $i++) 
			{ 
				if ($i == 0)
				{
					unlink('assets/uploads/Traffic_Survey_System/'.$form.'/'.$id.'_.pdf');
				}
				else
				{
					unlink('assets/uploads/Traffic_Survey_System/'.$form.'/'.$id.'_'.$i.'.pdf');
				}
			}

			$this->session->set_flashdata('message', $count.' attachments deleted.');
			redirect('traffic/update_'.$form.'/'.$id);
		}
	}

	public function ajax_get_mtmtc_streets()
	{
		$street_1 = $this->input->post('street_1');
		$street_2 = $this->input->post('street_2');

		$this->load->model('traffic_model');

		$results = $this->traffic_model->get_MTMTC_by_streets($street_1, $street_2);

		echo json_encode($results);

	}

	public function ajax_get_ps_streets()
	{
		$street_1 = $this->input->post('street_1');
		$street_2 = $this->input->post('street_2');

		$this->load->model('traffic_model');

		$results = $this->traffic_model->get_PS_by_streets($street_1, $street_2);

		echo json_encode($results);

	}
}




/* End of file trafic.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/trafic.php */