<div class="row">
	<div class="col-md-6">
		<h2>HRIS Employee Training Tracking</h2>
		<p class="lead">Managing HRIS employee training.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<form action="<?php echo base_url(); ?>HRIS_ETT/search" method="POST">
    <div class="form-group">
        <div class="row">
            <div class="col-sm-2">
                <label for="course_number">Course #</label>
                <input type="text" name="course_number" class="form-control" title="Course Number" value="<?php echo set_value('course_number') ?>" tabindex="1" placeholder="Course Number" />
            </div>
            <div class="col-sm-4">
                <label for="course_title">Course Title</label>
                <input type="text" name="course_title" class="form-control" title="Course Title" value="<?php echo set_value('course_title') ?>" tabindex="2" placeholder="Course Title" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
                <button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="HIRS_form_reset()">Clear</button>
            </div>
        </div>
    </div>
</form>

<legend class="phonebook_legend"></legend>

<div class="row">
	<div class="col-md-12">
		<?php if (!empty($courses)): ?>
			<h2 class="count"><?php echo count($courses) ?> courses</h2>
		<?php endif ?>
		<a href="<?php echo base_url(); ?>HRIS_ETT/add_course" class="btn btn-success pull-right">Add Course</a>
	</div>
</div>

<?php if (!empty($courses)): ?>
<div class="row">
	<div class="col-sm-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Course Number</th>
				<th>Course Title</th>
				<th>Delete</th>
			</thead>
			<tbody class="tbody">
					<?php foreach ($courses as $course): ?>
						<tr>
							<td><a href="<?php echo base_url(); ?>HRIS_ETT/update_course/<?= $course['COURSE_NO'] ?>"><?php echo $course['COURSE_NO'] ?></a></td>
							<td><?php echo ucwords(strtolower($course['COURSE_TITLE'])) ?></td>
							<td>
								<form action="<?php echo base_url(); ?>HRIS_ETT/delete_course" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger delete_course">Delete</button>
									<input type="hidden" name="COURSE_NO" value="<?= $course['COURSE_NO'] ?>" />
								</form>
							</td>
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>