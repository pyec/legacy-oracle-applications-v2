
<?php if(!defined('BASEPATH')) exit('No direct script access allowed.');
/*========================================================================================================================
	   Name : Ldap

Description : This class will interface with an LDAP server to get basic CRUD functionallity
				 for the phone book and validate users. This library, like other CI libraries is initialized with 
				 by passing a config array to $this->load->library('ldap', $config). That array should look like this.

 $config = array(

	'port' => '', 			- Port to the ldap/AD server. (default is 389)
	'host_name' => '', 		- Host name/connection string to the server. (HRM AD = msaddns02.hrm.halifax.ca)	
	'base' => '',			- The place in the LDAP tree that you start when seaching for records. (path to HRM = OU=HRM Users,OU=Halifax Regional Municipality,DC=hrm,DC=halifax,DC=ca)
	'rdn' => '', 			- The username that you use to connect to the server with. (For HRM you can connect with any employee AD account name. e.x. morinb )
	'password' => '', 		- The password you use to connect to the server with. (For HRM this is you AD password)
	);

  Functions : initialize_config( $config = array ),
  				connect(),
  				bind( $conn, annonymous = false ),
				disconnect( $conn ),
				validate_user()

=========================================================================================================================*/
class Ldap 
{
	//Properties
	protected $CI;				// This object is needed to instantiate LDAP as a library class.
	protected $error_array;		// Used to store error messages that will be sent back to the user. (Not implemented yet.)

	//Config variables
	protected $host_name;
	protected $port;
	protected $base;

	//Authentication Varibles 
	protected $rdn; // ldap rdn or dn
	protected $password; 

	function __construct( $config = array() )
	{
		/*
			IMPORTANT - The $CI object is huge and already in memory at this point, 
			however we want it's functionality in this class so we copy by ref.
			We need this object to treat this class as a library.
		*/
		$this->CI =& get_instance();

		// If no config options are set, throw an error.
		if (!empty($config))
		{
			// Otherwise call the initialize_config function and pass along our congof array.
			$this->initialize_config($config);
		}
		else
		{
			$this->error_array[] = 'No configuration values set.';
		}

	}

/*-----------------------------------------------------------------------------------
	This function takes in the config array from the constructor and assigns 
		values to their matching property.
------------------------------------------------------------------------------------*/
	public function initialize_config( $config )
	{
		// For each element in the config array.
		foreach ($config as $key => $value) 
		{
			// Check to see if that elements key matches the name of a configuation property.
			if (empty($this->$key))
			{
				// If there is a match assign that value of that element to the property.
				$this->$key = $value;
			}
		}
	}

/*-----------------------------------------------------------------------------------
	Creates a connection to an ldap server based on the cofig proerties.
------------------------------------------------------------------------------------*/
	public function connect()
	{
		// Check to make sure a host name and port were set before attempting the connection.
		if (!empty($this->host_name) && !empty($this->port))
		{
			/*
				This function doesn't actally connect us to the server rather than set up the connection 
				variable and prepare the system for connection. The ldap_bind() function is the one 
				that establishes the connection. ldap_connect(), if successful will return a resource id#
				that is required for binding and querying the server.
			*/
			$conn = ldap_connect($this->host_name, $this->port) or die('Could not establish connection ');

			// This forces the OPT Protocol to be in version 3, needed for working with LDAP in php.
			ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, 3);

			return $conn;
		}
	}

/*-----------------------------------------------------------------------------------
	Closes a connection with an ldap server.
------------------------------------------------------------------------------------*/
	public function disconnect( $conn )
	{
		ldap_close($conn);
	}

/*-----------------------------------------------------------------------------------
	Bind to the server establishing our connection, some servers will allow for an
	annonymous bind (i.e. no rdn or password). The second parameter of this function 
	is a boolean that triggers whether or not the bind will be annonymous. If not the
	function attempts to bind with the config properties.
------------------------------------------------------------------------------------*/
	public function bind( $conn, $annonymous = false )
	{
		if ($annonymous)
		{
			ldap_bind($conn);	
		}
		else
		{
			// Check to see if the config properties were set before attempting the bind.
			if (!empty($this->rdn) && !empty($this->password))
			{
				/*
					ldap_bind will return a true or false.				
				*/				
				$bind = ldap_bind($conn, $this->rdn, $this->password);
				return $bind;
			}
		}
	}

/*-----------------------------------------------------------------------------------
	This function will retrieve the data accociated with the user logging in, if the bind 
	doesn't work then we know it's an invalid user.
------------------------------------------------------------------------------------*/
	public function validate_user( $filter , $base)
	{
		// Get the resource id.
		$conn = $this->connect();

		// Create the bind.
		$valid = $this->bind($conn, false);

		// If the bind isn't valid that mean the credentials were incorrect.
		if (!$valid) {return false;}


		// Retrieves the result object.
//        $result_object = ldap_search($conn, $this->base, $filter);
		$result_object = ldap_search($conn, $base, $filter);
		$results = ldap_get_entries($conn, $result_object);

		// Here we're only expecting one record, but we need to pick and choose which field we want to return.
		for ($i=0; $i < $results["count"]; $i++) 
		{
			$user_data = array(
				'name' => $results[$i]['name'][0], 
				'title' => $results[$i]['title'][0], 
				'department' => $results[$i]['department'][0],  
				'member_of' => $results[$i]['memberof'], 
				'samaccountname' => $results[$i]['samaccountname'][0], 
				'telephonenumber' => '', 
				'mobile' => '', 
				);
			
			
			// 	Not ever person in Active Directories has phone numbers attached to their account
			// 	so we perfom a check to see if those fields are set first as to avoid errors.
			if (!empty($results[$i]['telephonenumber'])) {$user_data['telephonenumber'] = $results[$i]['telephonenumber'][0];}
			if (!empty($results[$i]['mobile'])) {$user_data['mobile'] = $results[$i]['mobile'][0];}
			if (!empty($results[$i]['mail'])) {$user_data['mail'] = $results[$i]['mail'][0];}
		}

		// Destroy the connection to the LDAP server. 
		$this->disconnect($conn);
		// Return the data back to the controller to be stored in session.
		return $user_data;
	}

/*-----------------------------------------------------------------------------------
	This function will return a list of departments.
------------------------------------------------------------------------------------*/

	public function get_deparments()
	{
		// Get the resource id.
		$conn = $this->connect();

		// Create the bind.
		$valid = $this->bind($conn, false);

		// If the bind isn't valid that mean the credentials were incorrect.
		if (!$valid) {return false;}


		// Retrieves the result object.
		// ldap_set_option($conn, LDAP_OPT_SIZELIMIT, 0);
		$result_object = @ldap_search($conn, $this->base, '(Division=*)', array('Division'));
		$results = ldap_get_entries($conn, $result_object);

		$departments = array();

		for ($i=0; $i < $results['count']; $i++) 
		{ 
			//added a trim around the division as there were some ladap entries with whitespace showing as duplicates in selection criteria SB 2018-07-19
			$departments[] = trim($results[$i]['division'][0]);
		}

		$departments = array_unique($departments);
		sort($departments);

		$this->disconnect($conn);
		
		return $departments;
	}

/*-----------------------------------------------------------------------------------
	This function takes in an ldap filter determined by the phonebook search form
	and returns the records that match the filter.
------------------------------------------------------------------------------------*/
	public function phonebook_search( $filter )
	{
		// Get the resource id.
		$conn = $this->connect();

		// Create the bind.
		$valid = $this->bind($conn, false);

		// If the bind isn't valid that mean the credentials were incorrect.
		if (!$valid) {return false;}

		// ldap search will either return a link identifier to the records or return false if there is no records to return.
		// $result_object = @ldap_search($conn, $this->base, $filter, array('samaccountname', 'name', 'title', 'mail', 'department', 'physicaldeliveryofficename', 'telephonenumber', 'mobile', 'givenname'));
		$result_object = @ldap_search($conn, $this->base, $filter);

		// If there are records.
		if($result_object != false)
		{
			// Store the records in $results
			$results = ldap_get_entries($conn, $result_object);

			for ($i=0; $i < $results['count']; $i++) 
			{ 
				if (isset($results[$i]['samaccountname'])){$staff[$i]['samaccountname'] = $results[$i]['samaccountname'][0];}
				if (isset($results[$i]['name'])){$staff[$i]['name'] = $results[$i]['name'][0];}
				if (isset($results[$i]['title'])){$staff[$i]['title'] = $results[$i]['title'][0];}
				if (isset($results[$i]['mail'])){$staff[$i]['mail'] = $results[$i]['mail'][0];}
				if (isset($results[$i]['department'])){$staff[$i]['department'] = $results[$i]['department'][0];}
				if (isset($results[$i]['physicaldeliveryofficename'])){$staff[$i]['physicaldeliveryofficename'] = $results[$i]['physicaldeliveryofficename'][0];}
				if (isset($results[$i]['mobile'])){$staff[$i]['mobile'] = $results[$i]['mobile'][0];}
				if (isset($results[$i]['telephonenumber'])){$staff[$i]['telephonenumber'] = $results[$i]['telephonenumber'][0];}
			}
			
			$this->disconnect($conn);
		
			if ($filter == "(&)") {unset($staff[0]);}
		
			if (!empty($staff))
			{
				return $staff;	
			}
			else
			{
				return false;
			}
		}
		return false;
	}


	public function get_staff_by_accountname( $filter )
	{
		// Get the resource id.
		$conn = $this->connect();

		// Create the bind.
		$valid = $this->bind($conn, false);

		// If the bind isn't valid that mean the credentials were incorrect.
		if (!$valid) {return false;}

		// ldap search will either return a link identifier to the records or return false if there is no records to return.
		$result_object = @ldap_search($conn, $this->base, $filter);

		// If there are records.
		if($result_object != false)
		{
			// Store the records in $results
			$results = ldap_get_entries($conn, $result_object);

			for ($i=0; $i < $results['count']; $i++) 
			{ 
				if (isset($results[$i]['samaccountname'])){$staff[$i]['samaccountname'] = $results[$i]['samaccountname'][0];}
				if (isset($results[$i]['name'])){$staff[$i]['name'] = $results[$i]['name'][0];}
				if (isset($results[$i]['title'])){$staff[$i]['title'] = $results[$i]['title'][0];}
				if (isset($results[$i]['mail'])){$staff[$i]['mail'] = $results[$i]['mail'][0];}
				if (isset($results[$i]['mail'])){$staff[$i]['mail'] = $results[$i]['mail'][0];}
				if (isset($results[$i]['department'])){$staff[$i]['department'] = $results[$i]['department'][0];}
				if (isset($results[$i]['physicaldeliveryofficename'])){$staff[$i]['physicaldeliveryofficename'] = $results[$i]['physicaldeliveryofficename'][0];}
				if (isset($results[$i]['streetaddress'])){$staff[$i]['streetaddress'] = $results[$i]['streetaddress'][0];}
				if (isset($results[$i]['mobile'])){$staff[$i]['mobile'] = $results[$i]['mobile'][0];}
				if (isset($results[$i]['telephonenumber'])){$staff[$i]['telephonenumber'] = $results[$i]['telephonenumber'][0];}
				if (isset($results[$i]['facsimiletelephonenumber'])){$staff[$i]['fax'] = $results[$i]['facsimiletelephonenumber'][0];}
			}
			
			$this->disconnect($conn);
			return $staff;
		}
		return false;

		$this->disconnect($conn);
	}

	private function expired()
	{
		// Get the resource id.
		$conn = $this->connect();

		// Create the bind.
		$valid = $this->bind($conn, false);

		// If the bind isn't valid that mean the credentials were incorrect.
		if (!$valid) {return false;}

		// ldap search will either return a link identifier to the records or return false if there is no records to return.
		$result_object = @ldap_search($conn, $this->base, "(&(useraccountcontrol=514))");

		$results = ldap_get_entries($conn, $result_object);

		echo '<pre>';
		print_r($results);
		echo '</pre>';

	}
}

