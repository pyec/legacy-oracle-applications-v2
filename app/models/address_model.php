<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Address_model extends CI_Model 
{
	public $oracle_db = null; // The reference to the database

	//Constructor
	public function __construct()
	{
		parent::__construct();
		$this->oracle_db = $this->load->database('oracle_address_db', TRUE);
	}


	public function get_district($prclkey = null)
	{	
		$this->oracle_db->where('PRCLKEY', (int)$prclkey);
		$query = $this->oracle_db->get('PARCEL');

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

}

/* End of file address_model.php */
/* Location: .//Z/HRMApplications_2/app/models/address_model.php */