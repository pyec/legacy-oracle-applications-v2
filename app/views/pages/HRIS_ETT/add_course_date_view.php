<div class="row">
	<div class="col-md-12">
		<h2>Add Course Date</h2>
		<p class="lead">This form will allow you to add a new course to the system.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<form action="<?php echo base_url(); ?>HRIS_ETT/add_course_date/<?= $course_info[0]['COURSE_NO'] ?>" method="post" >
		<div class="col-md-12">
			<legend>Course</legend>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="COURSE_NO">Course Number</label>
						<input type="text" name="COURSE_NO" class="form-control" title="Course Number" maxlength="6" value="<?= $course_info[0]['COURSE_NO'] ?>" disabled />
						<input type="hidden" name="COURSE_NO"  value="<?= $course_info[0]['COURSE_NO'] ?>"  />
					</div>

					<div class="col-sm-4">
						<label for="COURSE_TITLE">Course Name</label>
						<input type="text" class="form-control" title="Course Name" maxlength="30" value="<?= $course_info[0]['COURSE_TITLE'] ?>" disabled />
					</div>
					
					<div class="col-sm-2">
						<label for="COURSE_CLASS">Course Category</label>
						<input type="text" name="COURSE_CLASS" class="form-control" title="Category" value="<?= $course_info[0]['COURSE_CLASS'] ?>" disabled />
						<input type="hidden" name="COURSE_CLASS"value="<?= $course_info[0]['COURSE_CLASS'] ?>"  />
					</div>
				</div>
			</div>

			<legend>Date Info</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="COURSE_SCHED">Course Schedule</label><span class="text-danger">*</span>
						<input type="text" name="COURSE_SCHED" class="form-control" title="Schedule" value="" maxlength="4" value="<?php echo set_value('COURSE_SCHED'); ?>" />
					</div>	

					<div class="col-sm-3">
						<label for="SCHOOL_LOCN">Location</label>
						<input type="text" name="SCHOOL_LOCN" class="form-control" title="Location" value="" maxlength="30" value="<?php echo set_value('SCHOOL_LOCN'); ?>" />
					</div>

					<div class="col-sm-3">
						<label for="INSTRUCTOR">Instructor</label>
						<input type="text" name="INSTRUCTOR" class="form-control" title="Instructor" value="" maxlength="30" value="<?php echo set_value('INSTRUCTOR'); ?>" />
					</div>

					<div class="col-sm-2">
						<label for="COST_COURSE">Course Cost</label>
						<input type="text" name="COST_COURSE" class="form-control" title="Cost" value="" maxlength="8" value="<?php echo set_value('COST_COURSE'); ?>" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="COURSE_START">Start Date</label>
						<input type="text" name="COURSE_START" class="form-control date_picker_new" title="Schedule" value="" maxlength="" value="<?php echo set_value('COURSE_START'); ?>" readonly />
					</div>	

					<div class="col-sm-3">
						<label for="COURSE_END">End Date</label>
						<input type="text" name="COURSE_END" class="form-control date_picker_new" title="Location" value="" maxlength="" value="<?php echo set_value('COURSE_END'); ?>" readonly />
					</div>

					<div class="col-sm-2">
						<label for="DURATION_DAYS">Duration Days</label>
						<input type="text" name="DURATION_DAYS" class="form-control" title="Instructor" value="" maxlength="2" value="<?php echo set_value('DURATION_DAYS'); ?>" />
					</div>

					<div class="col-sm-2">
						<label for="DURATION_HRS">Duration Hours</label>
						<input type="text" name="DURATION_HRS" class="form-control" title="Cost" value="" maxlength="4" value="<?php echo set_value('DURATION_HRS'); ?>" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-md btn-primary add_course pull-right" value="Add Course Date" />
						<a href="<?php echo base_url(); ?>HRIS_ETT/update_course/<?= $course_info[0]['COURSE_NO'] ?>" class="btn btn-md btn-default return">Back to Course</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>