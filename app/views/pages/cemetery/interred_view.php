<div class="row">
	<div class="col-md-6">
		<h2>Interred</h2>
		<!-- <p class="lead">Information on the interred of HRM.</p> -->
	</div>
</div>
 <div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>

<form action="<?php echo base_url(); ?>cemetery/interred_search" method="POST">
    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <label for="cemetery">Cemetery</label>
                <select name="cemetery" class="form-control" title="Cemetery"
                        value="<?php echo set_select('cemetery') ?>" tabindex="1">
                    <option value="">All</option>
                    <?php foreach ($cemeteries as $cem): ?>
                        <option value="<?= $cem['CEM_NBR'] ?>" <?php echo set_select('cemetery', $cem['CEM_NBR']); ?> ><?php echo ucwords(strtolower($cem['CEM_NAME'])) ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="col-sm-3">
                <label for="last_name">Last Name</label>
                <input type="text" name="last_name" class="form-control" title="Last Name"
                       value="<?php echo set_value('last_name') ?>" tabindex="4" placeholder="Last Name"/>
            </div>
            <div class="col-sm-3">
                <label for="first_name">First Name</label>
                <input type="text" name="first_name" class="form-control" title="First Name"
                       value="<?php echo set_value('first_name') ?>" tabindex="2" placeholder="First Name"/>
            </div>
            <div class="col-sm-3">
                <label for="middle_name">Middle Name</label>
                <input type="text" name="middle_name" class="form-control" title="Middle Name"
                       value="<?php echo set_value('middle_name') ?>" tabindex="3" placeholder="Middle Name"/>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <label for="location">Location</label>
                <input type="text" name="location" class="form-control" title="Location"
                       value="<?php echo set_value('location') ?>" tabindex="3" placeholder="Location"/>
            </div>
        </div>
    </div>
        <div class="row">
            <div class="col-sm-12">
                <button type="submit" class="btn btn-primary btn-md pull-right no_label_fix phonebook_search_submit"
                        tabindex="8"><i class="fa fa-search">&nbsp;</i>Search
                </button>
                <button type="reset" class="btn btn-default btn-md pull-right no_label_fix" tabindex="9"
                        onclick="cem_owners_form_reset()">Clear
                </button>
            </div>
        </div>
</form>


<!--<legend class="phonebook_legend"></legend>-->

<div id="dvCount" class="row">
	<div class="col-md-12">
		<?php if (!empty($interred)): ?>
			<h2><?php echo count($interred) ?> Interred</h2>
		<?php endif ?>
		<!--<a href="<?php echo base_url(); ?>cemetery/add_interred" class="btn btn-success pull-right">Add Interred</a>-->
	</div>
	<div class="col-md-12">
		<?php if (!empty($interred) && count($interred) >= 500): ?>
			<P class="">Refine your search for more specific results.</P>
		<?php endif ?>
	</div>
</div>
<?php if (!empty($interred)): ?>
<div id="dvResults" class="row">
	<div class="col-md-12">
		<table id="tblResults" class="table table-bordered table-striped table-hover">
			<thead>
				<th>Cemetery</th>
				<th>Name</th>
				<th>Age</th>
				<th>Interred Date</th>
                <th>Location</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
				<?php foreach ($interred as $value): ?>
				 	<tr>
				 		<td><?php echo ucfirst(strtolower($value['CEM_NAME'])) ?></a></td>
				 		<td>
				 			<a href="<?php echo base_url(); ?>cemetery/update_interred/<?php echo $value['CEM_NBR'] ?>/<?php echo $value['CEM_REFNBR'] ?>/<?php echo $value['INT_REFNBR'] ?>">
				 				<?php echo ucwords(strtolower($value['INT_LNAME'].', '.$value['INT_FNAME'].' '.$value['INT_MNAME'])) ?>
				 			</a>
				 		</td>
				 		<td><?php echo $value['INT_AGE'] ?></td>
				 		<td><?php echo $value['INT_DTE'] ?></td>
                        <td><?php echo $value['INT_LOC'] ?></td>
				 		<td>
					 		<form action="<?php echo base_url(); ?>cemetery/delete_interred" method="post">
							<button type="submit" class="delete_interred btn btn-sm btn-danger" ><i class="fa fa-minus-square"></i></button>
							<input type="hidden" name="INT_REFNBR" value="<?= $value['INT_REFNBR'] ?>" />
							<input type="hidden" name="CEM_NBR" value="<?= $value['CEM_NBR'] ?>" />
							<input type="hidden" name="CEM_REFNBR" value="<?= $value['CEM_REFNBR'] ?>" />
							</form>
						</td>
				 	</tr>
				 <?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>
