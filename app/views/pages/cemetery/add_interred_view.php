<div class="row">
	<div class="col-md-12">
		<h2>Add Interred</h2>
		<p class="lead">This form will allow you to add a new interred record to the system.</p>
	</div>
</div>
 <div class="row">
     <div class="col-md-12">
         <?php if ($this->session->flashdata('message')): ?>
             <div class="alert alert-success alert-dismissable">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 <p><?php echo $this->session->flashdata('message'); ?></p>
             </div>
         <?php elseif ($this->session->flashdata('error')): ?>
             <div class="alert alert-danger alert-dismissable">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 <p><?php echo $this->session->flashdata('error'); ?></p>
             </div>
         <?php elseif (validation_errors()): ?>
             <div class="alert alert-danger alert-dismissable">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 <p><?php echo validation_errors(); ?></p>
             </div>
         <?php endif ?>
        <?php if ($errorMessage): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p><?php echo $errorMessage; ?></p>
            </div>
        <?php endif ?>
    </div>
</div>

<form action="<?php echo base_url(); ?>cemetery/add_interred/<?php echo $owner_id ?>/<?php echo $cem_id ?>" method="post">
<div class="row">
		<div class="col-md-12">
            <div class="form-group">
			    <div class="row">
					<div class="col-sm-4">
						<label for="CEM_NBR">Cemetery</label>
						<select class="form-control" name="CEM_NBR" disabled>
							<option selected value="null">Select a cemetery</option>
							<?php foreach ($cemeteries as $cem): ?>
								<?php if (!empty($cem_id)): ?>
									<?php if ($cem_id == $cem['CEM_NBR']): ?>
										<option selected value="<?= $cem['CEM_NBR'] ?>" <?php echo set_select('CEM_NBR', $cem['CEM_NBR']); ?>><?= $cem['CEM_NAME'] ?></option>
									<?php else: ?>
										<option value="<?= $cem['CEM_NBR'] ?>" <?php echo set_select('CEM_NBR', $cem['CEM_NBR']); ?>><?= $cem['CEM_NAME'] ?></option>
									<?php endif ?>
                                <?php else: ?>
<!--                                    <option value="--><?//= $cem['CEM_NBR'] ?><!--" --><?php //echo set_select('CEM_NBR', $cem['CEM_NBR']); ?><!-->--><?//= $cem['CEM_NAME'] ?><!--</option>-->
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
                    <!--
					<div class="col-sm-1">
						<button type="button" class="modal_btn btn btn-primary btn-sm no_label_fix" value="location" data-toggle="modal" data-target="#myModal">Select Owner</button>
					</div>-->
					<div class="col-sm-3">
						<label for="CEM_REFNBR">Owner Ref #</label><span class="text-danger">*</span>
						<input type="text" name="CEM_REFNBR" class="form-control owner_ref" title="Owner Reference" value="<?php if(!empty($owner_id)){ echo $owner_id; }else{echo set_value('CEM_REFNBR');} ?>" readonly />
						<!--<input type="hidden" name="CEM_REFNBR" class="form-control owner_ref" title="Owner Reference" value="<?php if(!empty($owner_id)){ echo $owner_id; }else{echo set_value('CEM_REFNBR');} ?>" />-->
					</div>
					<div class="col-sm-4">
						<label for="owner_name">Owner Name</label>
						<input type="text" class="form-control owner_name" name="OwnerName" title="Owner Name"  value="<?php echo $owner_name; ?>" disabled />
					</div>
				</div>
			</div>
            <input type="hidden" name="CEM_NBR" value="<?php echo $cem_id ?>" />

		<!-- <legend></legend> -->

            <div class="form-group">
			    <div class="row">
					<div class="col-sm-4">
						<label for="INT_FNAME">First Name</label>
						<input type="text" name="INT_FNAME" class="form-control" title="First Name" value="<?php echo set_value('INT_FNAME'); ?>" maxlength="20" />
					</div>
					<div class="col-sm-4">
						<label for="INT_MNAME">Middle Name</label>
						<input type="text" name="INT_MNAME" class="form-control" title="Middle Name" value="<?php echo set_value('INT_MNAME') ?>" maxlength="20" />
					</div>
					<div class="col-sm-4">
						<label for="INT_LNAME">Last Name</label>
						<input type="text" name="INT_LNAME" class="form-control" title="Last Name" value="<?php echo set_value('INT_LNAME') ?>" maxlength="30" />
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="INT_TITLE">Title</label>
						<input type="text" name="INT_TITLE" class="form-control" title="Title" value="<?php echo set_value('INT_TITLE') ?>" maxlength="15"/>
					</div>
					<div class="col-sm-4">
						<label for="INT_DTE">Interred Date</label>
						<input type="text" name="INT_DTE" class="form-control date_picker_past_cem" title="Interred Date" value="<?php echo set_value('INT_DTE') ?>" readonly />
					</div>
					<div class="col-sm-2">
						<label for="INT_AGE">Age</label>
						<input type="text" name="INT_AGE" class="form-control" title="Age" value="<?php echo set_value('INT_AGE') ?>" maxlength="10" />
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="INT_TYP">Grave Type</label>
						<input type="text" name="INT_TYP" class="form-control" title="Grave Type" value="<?php echo set_value('INT_TYP') ?>" maxlength="10" />
					</div>
					<div class="col-sm-2">
						<label for="INT_MARKER">Marker</label>
						<input type="text" name="INT_MARKER" class="form-control" title="Marker" value="<?php echo set_value('INT_MARKER') ?>" maxlength="1" />
					</div>
					<div class="col-sm-4">
						<label for="INT_LOC">Location</label>
						<input type="text" name="INT_LOC" class="form-control" title="Location" value="<?php echo set_value('INT_LOC') ?>" maxlength="5" />
					</div>
					<div class="col-sm-4">
						<label for="INT_INCOMDTE">Incomplete Interred Date</label>
						<input type="text" name="INT_INCOMPDTE" class="form-control" title="Incomplete Interred Date" value="<?php echo set_value('INT_INCOMDTE') ?>" readonly />
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="INT_CMNT">Comments</label>
						<textarea name="INT_CMNT" class="form-control" title="Comments" maxlength="256" ><?php echo set_value('INT_CMNT') ?></textarea>
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="INT_SPC_CMNT">Special Comments</label>
						<textarea name="INT_SPC_CMNT" class="form-control" title="Special Comments" maxlength="256" ><?php echo set_value('INT_SPC_CMNT') ?></textarea>
					</div>
				</div>
			</div>
    		<div class="form-group" style="margin-top: 2em">
    				<input type="submit" class="btn btn-md btn-primary pull-right" value="Add Interred" />
					<a href="<?php echo base_url(); ?>cemetery/update_owner/<?php echo $owner_id ?>/<?php echo $cem_id ?>" class="btn btn-md btn-default">Back to Owner</a>
			</div>
	</div>
</div>
</form>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Owner Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="modal_search">
							<div class="col-sm-4">
								<label for="first_name">First Name</label>
								<input type="text" name="owner_first_name" id="owner_first_name" class="form-control owner_first_name_search_box" title="First Name" autofocus maxlength="" placeholder="First Name" autocomplete="false" />
							</div>
							<div class="col-sm-4">
								<label for="last_name">Last Name</label>
								<input type="text" name="owner_last_name" id="owner_last_name" class="form-control owner_last_name_search_box" title="Last Name" maxlength="" placeholder="Last Name" autocomplete="false" />
							</div>
						</div>
					</div>
					<div class="row">
						<p class="text-muted">(Select the name of the record you would like to use.)</p>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Name</th>
					</thead>
					<tbody class="cem_modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->