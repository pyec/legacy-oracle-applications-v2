<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Engineering Drawing Location System

Description : This system allows users to locate the electronic and paper copies
				of engineer drawing in HRM.

  Functions : index(),
			add_location(),
			table_maintenance(),
			surrounding_street_query()
  			add_surrounding_street(),
  			plan_index_query(),
  			add_plan_index(),
            ajax_file_id()

=========================================================================================================================*/
class EDL extends CI_Controller
{

    public function index()
	{
        $this->session->set_userdata('file_number', null);
        $this->session->set_userdata('project_type', null);
        $this->session->set_userdata('originator', null);
        $this->session->set_userdata('surveyor', null);
        $this->session->set_userdata('signed_date', null);
        $this->session->set_userdata('tender_number', null);
        $this->session->set_userdata('description', null);
        $this->session->set_userdata('STREET-0', null);
        $this->session->set_userdata('STREET_TYPE-0', null);
        $this->session->set_userdata('STREET_CODE-0', null);
        $this->session->set_userdata('COMMUNITY-0', null);
        $this->session->set_userdata('STREET-1', null);
        $this->session->set_userdata('STREET_TYPE-1', null);
        $this->session->set_userdata('STREET_CODE-1', null);
        $this->session->set_userdata('COMMUNITY-1', null);

        $this->session->set_userdata('searchCount', null);
        $this->session->set_userdata('searchTerm', null);
        $this->session->set_userdata('from_plan', null);
        $this->session->set_userdata('to_plan', null);

//        if (defined('ENVIRONMENT'))
//        {
//           switch (ENVIRONMENT)
//           {
//               case 'testing':
//                   //Load Model
//                   // $this->load->model('EDL_model');
//
//                   // $data['originators'] = $this->EDL_model->get_originator();
//                   // $data['project_types'] = $this->EDL_model->get_project_type();
//                   $data['content'] = "templates/no_access_edl";
//                   $this->load->view('master', $data);
//                   break;
//               default:
                    //Load Model
                    $this->load->model('EDL_model');

                    $data['originators'] = $this->EDL_model->get_originator();
                    $data['surveyors'] = $this->EDL_model->get_surveyor();
                    $data['project_types'] = $this->EDL_model->get_project_type();
                    $data['content'] = "pages/EDL/home_view";
                    $data['files'] = null;
                    $this->load->view('master', $data);
//            }
//        }
	}

    public function searchTerm_handler($searchTerm)
    {
        if($searchTerm)
        {
            $this->session->set_userdata('searchTerm', $searchTerm);
            return $searchTerm;
        }
        elseif($this->session->userdata('searchTerm'))
        {
            $searchTerm = $this->session->userdata('searchTerm');
            return $searchTerm;
        }
        else
        {
            $searchTerm ="";
            return $searchTerm;
        }
    }

    /*
    ============================================================================
    file_search
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    No return - Handles validation and setting up the search string.
    ============================================================================
    */
    public function file_search()
    {
        // Load model
        $this->load->helper("url");
        $this->load->model('EDL_model');
        $this->load->library("pagination");

        // Get search params from the form.
        $search_array = $this->input->post(null, true);

        $searchParameters = false;

        $searchTerm =$this->searchTerm_handler($search_array, TRUE);

        foreach ($search_array as $key => $value) {
            $this->session->set_userdata($key, $value);

            if ($value != '' && $searchParameters == false) {
                $searchParameters = true;
            }

            if (strpos($key, '-')) {
                if ($key == 'STREET_CODE-0') {
                    $search_array['street'] = $value;
                    unset($search_array[$key]);
                } elseif ($key == 'STREET_CODE-1') {
                    $search_array['surrounding_street_1'] = $value;
                    unset($search_array[$key]);
                }
//                    elseif($key == 'STREET_CODE-2')
//                    {
//                        $search_array['surrounding_street_2'] = $value;
//                        unset($search_array[$key]);
//                    }
                else {
                    unset($search_array[$key]);
                }
            }
        }

        if ($searchParameters || array_filter($searchTerm) != null) {
            $config = array();
            $config["base_url"] = base_url() . "EDL/file_search";

            if ($search_array) {
                $resultsCount = $this->EDL_model->search_files_count($search_array);
                $searchCount = count($resultsCount);
                $this->session->set_userdata('searchCount', $searchCount);
                $config["total_rows"] = $searchCount;
            } else {
                $searchCount = $this->session->userdata('searchCount');
                $config["total_rows"] = $searchCount;
            }

            $config["per_page"] = 25;
            $config["uri_segment"] = 3;
            $config["num_links"] = 10;

            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

            $searchTerm = $this->searchterm_handler($search_array, TRUE);

            $data['files'] = $this->EDL_model->search_files($searchTerm, $config["per_page"], $page);


            $this->pagination->initialize($config);

            $data["links"] = $this->pagination->create_links();
        }

        if (!empty($data['files']))
        {
            foreach ($data['files'] as $key => $value)
            {
                $data['files'][$key]["STREET"] = $this->EDL_model->get_street_by_id($value['STREET_NUM'])['STREET'];
            }
        }


        $data['originators'] = $this->EDL_model->get_originator();
        $data['surveyors'] = $this->EDL_model->get_surveyor();
        $data['project_types'] = $this->EDL_model->get_project_type();
        $data['content'] = "pages/EDL/home_view";
        $this->load->view('master', $data);
    }

    public function surrounding_search()
    {
        // Load model
        $this->load->model('EDL_model');

        // Get search params from the form.
        $search_array = $this->input->post(null, true);

        // Validation
        $this->form_validation->set_rules('file_number', 'File Number', 'trim|max_length[15]|xss_clean');
        $this->form_validation->set_rules('street_number', 'Street Number', 'trim|max_length[15]|xss_clean');
        $this->form_validation->set_rules('street_name', 'Street Name', 'trim|max_length[15]|xss_clean');

        if ($this->form_validation->run() == true)
        {
            if (!empty($search_array['file_number']))
            {
                $data['streets'] = $this->EDL_model->get_files_by_id($search_array['file_number']);
            }
            else if (!empty($search_array['street_number']))
            {
                $data['streets'] = $this->EDL_model->get_files_by_street_id($search_array['street_number']);
            }
            else
            {
                redirect('EDL/surrounding_streets_query');
            }
        }
        else
        {
            redirect('EDL/surrounding_streets_query');
        }

        if (!empty($data['streets'])){$data['streets'] = array_slice($data['streets'], 0, 500);}
        $data['content'] = "pages/EDL/surrounding_streets_query_view";
        $this->load->view('master', $data);
    }

    /*
    ============================================================================
    add_location
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    No return - handles adding owner record to the table.
    ============================================================================
    */
    public function add_location()
    {
        //Load Model
        $this->load->model('EDL_model');

        // Get the POST variables
        $post = $this->input->post(null, true);

        if (!empty($post))
        {
            // Set validation rules
            $this->form_validation->set_rules('FILE_ID', 'File ID', 'required');
            $this->form_validation->set_rules('PROJ_NUM', 'Project #', 'trim|regex_match[^\d{2}-\d{5}$|xss_clean');
            $this->form_validation->set_rules('TENDER_NUM', 'Tender #', 'trim|regex_match[^\d{4}-\d{3}$|xss_clean');
            $this->form_validation->set_rules('FROM_PLAN', 'From Plan', 'trim|max_length[11]|xss_clean');
            $this->form_validation->set_rules('TO_PLAN', 'To Plan', 'trim|max_length[11]|xss_clean');
            $this->form_validation->set_rules('CIVIC_NUM', 'Civic #', 'trim|max_length[5]|xss_clean');
            $this->form_validation->set_rules('EXT', 'Ext', 'trim|max_length[4]|xss_clean');
            $this->form_validation->set_rules('SIDE', 'Side', 'trim|max_length[3]|xss_clean');
            //$this->form_validation->set_rules('PROJTYP_CDE', 'Project', 'required|trim|xss_clean');
            $this->form_validation->set_rules('NOTES', 'Title/Description', 'trim|max_length[500]|xss_clean');
            $this->form_validation->set_rules('RELFILE_ID', 'Related File ID', 'trim|max_length[9]|xss_clean');
            $this->form_validation->set_rules('ROLL_NUM', 'Roll #', 'trim|max_length[10]|xss_clean');
            $this->form_validation->set_rules('ARCHIVE_DATE', 'Date Note', 'trim|max_length[50]|xss_clean');

            // Validate the form
            if ($this->form_validation->run() == true)
            {
                // check if the file ID already exists
                if (!$this->EDL_model->get_file_by_id(strtoupper($post['FILE_ID'])))
                {
                    $post['FILE_ID'] = strtoupper($post['FILE_ID']);
                    $post['TENDER_NUM'] = str_replace('-','',$post['TENDER_NUM']);
                    $post['PROJ_NUM'] = str_replace('-','',$post['PROJ_NUM']);
                    $post['PROJTYP_CDE'] = trim($post['PROJTYP_CDE'], ';');     // ; being added to PK&GR
                    $post['LAST_UPDATE'] = date('d-M-Y');
                    $post['LAST_UPDATE_BY'] = $this->session->userdata('samaccountname');

                    unset($post['street_name']);
                    unset($post['area_name']);
                    unset($post['street_type']);

                    // location names and surrounding streets have the _ in the name attr so thats how we identify which post vaiable belong to the location/surrounding street data
                    $locations_array = array();
                    foreach ($post as $key => $value)
                    {
                        $split_key = explode('-', $key);
                        if (is_numeric(end($split_key)))
                        {
                            $locations_array[end($split_key)][$split_key[0]] = $value;

                            unset($post[$key]);
                        }
                    }

                    $loc_data_exists = false;
                    foreach ($locations_array as $record)
                    {
                        if ($loc_data_exists)
                        {
                            continue;
                        }
                        foreach ($record as $key => $field)
                        {
                            if (!empty($field))
                            {
                                $loc_data_exists = true;
                            }
                        }
                    }

                    if ($this->EDL_model->insert_file($post, $locations_array, $loc_data_exists))
                    {
                        $this->session->set_flashdata('message', 'File '.$post['FILE_ID'].' was successfully added.');
                        redirect('EDL/update_location/'.$post['FILE_ID']);
                    }
                    else
                    {
                        $this->session->set_flashdata('error', 'The record could not be added please try again.');
                        // error: no data in post.
                        $data['locations'] = $this->EDL_model->get_locations();
                        $data['originators'] = $this->EDL_model->get_originator();
                        $data['projects'] = $this->EDL_model->get_project_type();
                        $data['survey_methods'] = $this->EDL_model->get_survey_method();
                        $data['surveyors'] = $this->EDL_model->get_surveyor();
                        $data['software_versions'] = $this->EDL_model->get_software_version();
                        $data['departments'] = $this->EDL_model->get_departments();
                        $data['electronic_class_numbers'] = $this->EDL_model->get_electronic_classifications();
                        $data['paper_class_numbers'] = $this->EDL_model->get_paper_classifications();

                        $data['process'] = $this->EDL_model->get_process();
                        $data['devices'] = $this->EDL_model->get_devices();
                        $data['statuses'] = $this->EDL_model->get_status();
                        $data['drawn_by'] = $this->EDL_model->get_drawn();

                        $data['content'] = "pages/EDL/add_location_view";
                        $this->load->view('master', $data);
                    }
                }
                else
                {
                    $this->session->set_userdata('error', 'A record with this file ID already exists.');
                    // error: no data in post.
                    $data['locations'] = $this->EDL_model->get_locations();
                    $data['originators'] = $this->EDL_model->get_originator();
                    $data['projects'] = $this->EDL_model->get_project_type();
                    $data['survey_methods'] = $this->EDL_model->get_survey_method();
                    $data['surveyors'] = $this->EDL_model->get_surveyor();
                    $data['software_versions'] = $this->EDL_model->get_software_version();
                    $data['departments'] = $this->EDL_model->get_departments();
                    $data['electronic_class_numbers'] = $this->EDL_model->get_electronic_classifications();
                    $data['paper_class_numbers'] = $this->EDL_model->get_paper_classifications();

                    $data['process'] = $this->EDL_model->get_process();
                    $data['devices'] = $this->EDL_model->get_devices();
                    $data['statuses'] = $this->EDL_model->get_status();
                    $data['drawn_by'] = $this->EDL_model->get_drawn();

                    $data['content'] = "pages/EDL/add_location_view";
                    $this->load->view('master', $data);
                }
            }
            else
            {
                // error: no data in post.
                $data['locations'] = $this->EDL_model->get_locations();
                $data['originators'] = $this->EDL_model->get_originator();
                $data['projects'] = $this->EDL_model->get_project_type();
                $data['survey_methods'] = $this->EDL_model->get_survey_method();
                $data['surveyors'] = $this->EDL_model->get_surveyor();
                $data['software_versions'] = $this->EDL_model->get_software_version();
                $data['departments'] = $this->EDL_model->get_departments();
                $data['electronic_class_numbers'] = $this->EDL_model->get_electronic_classifications();
                $data['paper_class_numbers'] = $this->EDL_model->get_paper_classifications();

                $data['process'] = $this->EDL_model->get_process();
                $data['devices'] = $this->EDL_model->get_devices();
                $data['statuses'] = $this->EDL_model->get_status();
                $data['drawn_by'] = $this->EDL_model->get_drawn();

                $data['content'] = "pages/EDL/add_location_view";
                $this->load->view('master', $data);
            }
        }
        else
        {
            // error: no data in post.
            $data['locations'] = $this->EDL_model->get_locations();
            $data['originators'] = $this->EDL_model->get_originator();
            $data['projects'] = $this->EDL_model->get_project_type();
            $data['survey_methods'] = $this->EDL_model->get_survey_method();
            $data['surveyors'] = $this->EDL_model->get_surveyor();
            $data['software_versions'] = $this->EDL_model->get_software_version();
            $data['departments'] = $this->EDL_model->get_departments();
            $data['electronic_class_numbers'] = $this->EDL_model->get_electronic_classifications();
            $data['paper_class_numbers'] = $this->EDL_model->get_paper_classifications();

            $data['process'] = $this->EDL_model->get_process();
            $data['devices'] = $this->EDL_model->get_devices();
            $data['statuses'] = $this->EDL_model->get_status();
            $data['drawn_by'] = $this->EDL_model->get_drawn();

            $data['content'] = "pages/EDL/add_location_view";
            $this->load->view('master', $data);
        }
    }

    /*
    ============================================================================
    delete_location
    ----------------------------------------------------------------------------
    NO PARAM
    ----------------------------------------------------------------------------
    No return - hadles passign the id to delete to the model to run the query.
    ============================================================================
    */
    public function delete_drawing()
    {
        $is_admin = false;

        $groups = $this->session->userdata('groups');

        foreach ($groups as $group) {
            if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin') {
                $is_admin = true;
            }
        }

        if ($is_admin) {

            //Load Model
            $this->load->model('EDL_model');

            $file_id = $this->uri->segment(3);

            $results = $this->EDL_model->delete_drawing($file_id);

            if ($results) {
                $this->session->set_flashdata('message', 'The record ' . $file_id . ' was deleted successfully.');
                redirect('EDL/');
            } else {
                $data['file'] = $this->EDL_model->get_file_by_id($file_id);

                $data['streets'] = $this->EDL_model->get_street_by_id($data['file']['STREET_NUM']);
                $data['locations'] = $this->EDL_model->get_locations();
                $data['originators'] = $this->EDL_model->get_originator();
                $data['projects'] = $this->EDL_model->get_project_type();
                $data['survey_methods'] = $this->EDL_model->get_survey_method();
                $data['surveyors'] = $this->EDL_model->get_surveyor();
                $data['software_versions'] = $this->EDL_model->get_software_version();
                $data['departments'] = $this->EDL_model->get_departments();
                $data['electronic_class_numbers'] = $this->EDL_model->get_electronic_classifications();
                $data['paper_class_numbers'] = $this->EDL_model->get_paper_classifications();

                $data['surrounding_street_records'] = $this->EDL_model->get_surrounding_street_by_id($file_id);

                if (!empty($data['surrounding_street_records'])) {
                    foreach ($data['surrounding_street_records'] as $street) {
                        $data['surrounding_streets'][] = $this->EDL_model->get_street_by_id($street['STREET_NUM']);
                    }
                }

                $data['process'] = $this->EDL_model->get_process();
                $data['devices'] = $this->EDL_model->get_devices();
                $data['statuses'] = $this->EDL_model->get_status();
                $data['drawn_by'] = $this->EDL_model->get_drawn();

                $data['locs'] = $this->EDL_model->get_locs($file_id);
                $this->session->set_flashdata('error', 'The record ' . $file_id . ' could not be deleted, please try again.');
                $data['content'] = "pages/EDL/update_location_view";
                $this->load->view('master', $data);
            }
        }
        else{
            redirect('EDL/');
        }
    }

    /*
    ============================================================================
    update_location
    ----------------------------------------------------------------------------
    NO PARAM
    ----------------------------------------------------------------------------
    No return - handles passing the data to model to update the specified record.
    ============================================================================
    */
    public function update_location()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);
        $id_to_update = $this->uri->segment(3);

        if (empty($post))
        {
            $data['file'] = $this->EDL_model->get_file_by_id($id_to_update);
            $data['streets'] = $this->EDL_model->get_street_by_id($data['file'][0]['STREET_NUM']);

            $data['location_name'] = '';
            $data['originator_name'] = '';
            $data['project_name'] = '';

            if (!empty($data['file'][0]['LOCATION'])){ $data['location_name'] = $this->EDL_model->get_location_by_id($data['file'][0]['LOCATION'])[0]['LOCATION_DSC']; }
            if (!empty($data['file'][0]['ORIG_CDE'])){ $data['originator_name'] = $this->EDL_model->get_originator_by_id($data['file'][0]['ORIG_CDE'])[0]['ORIGINATOR_DSC']; }
            if (!empty($data['file'][0]['PROJTYP_CDE'])){ $data['project_name'] = $this->EDL_model->get_project_by_id($data['file'][0]['PROJTYP_CDE'])[0]['PROJTYP_DSC']; }

            $data['locations'] = $this->EDL_model->get_locations();
            $data['originators'] = $this->EDL_model->get_originator();
            $data['projects'] = $this->EDL_model->get_project_type();
            $data['survey_methods'] = $this->EDL_model->get_survey_method();
            $data['surveyors'] = $this->EDL_model->get_surveyor();
            $data['software_versions'] = $this->EDL_model->get_software_version();
            $data['departments'] = $this->EDL_model->get_departments();
            $data['electronic_class_numbers'] = $this->EDL_model->get_electronic_classifications();
            $data['paper_class_numbers'] = $this->EDL_model->get_paper_classifications();

            $data['surrounding_street_records'] = $this->EDL_model->get_surrounding_street_by_id($id_to_update);

            if (!empty($data['surrounding_street_records']))
            {
                foreach ($data['surrounding_street_records'] as $street)
                {
                    $data['surrounding_streets'][] = $this->EDL_model->get_street_by_id($street['STREET_NUM']);
                }
            }

            $data['process'] = $this->EDL_model->get_process();
            $data['devices'] = $this->EDL_model->get_devices();
            $data['statuses'] = $this->EDL_model->get_status();
            $data['drawn_by'] = $this->EDL_model->get_drawn();

            $data['locs'] = $this->EDL_model->get_locs($id_to_update);

            $data['content'] = "pages/EDL/update_location_view";
            $this->load->view('master', $data);
        }
        else
        {
            // Set validation rules
            $this->form_validation->set_rules('FILE_ID', 'File ID', 'required');
            $this->form_validation->set_rules('PROJ_NUM', 'Project #', 'trim|regex_match[^\d{2}-\d{5}$|xss_clean');
            $this->form_validation->set_rules('TENDER_NUM', 'Tender #', 'trim|regex_match[^\d{4}-\d{3}$|xss_clean');
            $this->form_validation->set_rules('FROM_PLAN', 'From Plan', 'trim|max_length[11]|xss_clean');
            $this->form_validation->set_rules('TO_PLAN', 'To Plan', 'trim|max_length[11]|xss_clean');
            $this->form_validation->set_rules('CIVIC_NUM', 'Civic #', 'trim|max_length[5]|xss_clean');
            $this->form_validation->set_rules('SIDE', 'Side', 'trim|max_length[3]|xss_clean');
            //$this->form_validation->set_rules('PROJTYP_CDE', 'Project', 'required|trim|xss_clean');
            $this->form_validation->set_rules('NOTES', 'Title/Description', 'trim|max_length[500]|xss_clean');
            $this->form_validation->set_rules('RELFILE_ID', 'Related File ID', 'trim|max_length[9]|xss_clean');
            $this->form_validation->set_rules('ROLL_NUM', 'Roll #', 'trim|max_length[10]|xss_clean');
            $this->form_validation->set_rules('ARCHIVE_DATE', 'Date Note', 'trim|max_length[50]|xss_clean');

            if ($this->form_validation->run() == true) {
                $post['TENDER_NUM'] = str_replace('-','',$post['TENDER_NUM']);
                $post['PROJ_NUM'] = str_replace('-','',$post['PROJ_NUM']);
                $post['PROJTYP_CDE'] = trim($post['PROJTYP_CDE'], ';');     // ; being added to PK&GR
                $post['LAST_UPDATE'] = date('d-M-Y');
                $post['LAST_UPDATE_BY'] = $this->session->userdata('samaccountname');
                if ($this->EDL_model->update_file($id_to_update, $post)) {
                    $this->session->set_flashdata('message', 'The location record had been updated successfully.');
                    redirect('EDL/update_location/' . $post['FILE_ID']);
                } else {
                    $this->session->set_flashdata('error', 'The location record could not be updated, please try again.');
                    redirect('EDL/update_location/' . $post['FILE_ID']);
                }
            }else{
                $data['streets'] = $this->EDL_model->get_street_by_id($post['STREET_NUM']);
                $data['locations'] = $this->EDL_model->get_locations();
                $data['originators'] = $this->EDL_model->get_originator();
                $data['projects'] = $this->EDL_model->get_project_type();
                $data['survey_methods'] = $this->EDL_model->get_survey_method();
                $data['surveyors'] = $this->EDL_model->get_surveyor();
                $data['software_versions'] = $this->EDL_model->get_software_version();
                $data['departments'] = $this->EDL_model->get_departments();
                $data['electronic_class_numbers'] = $this->EDL_model->get_electronic_classifications();
                $data['paper_class_numbers'] = $this->EDL_model->get_paper_classifications();

                $data['surrounding_street_records'] = $this->EDL_model->get_surrounding_street_by_id($id_to_update);

                if (!empty($data['surrounding_street_records']))
                {
                    foreach ($data['surrounding_street_records'] as $street)
                    {
                        $data['surrounding_streets'][] = $this->EDL_model->get_street_by_id($street['STREET_NUM']);
                    }
                }

                $data['process'] = $this->EDL_model->get_process();
                $data['devices'] = $this->EDL_model->get_devices();
                $data['statuses'] = $this->EDL_model->get_status();
                $data['drawn_by'] = $this->EDL_model->get_drawn();

                $data['locs'] = $this->EDL_model->get_locs($id_to_update);
                $data['file'][0] = $post;
                $data['content'] = "pages/EDL/update_location_view";
                $this->load->view('master', $data);
            }
        }
    }


    public function update_file_locations()
    {
        $this->load->model('EDL_model');

        $file_id = $this->uri->segment(3);

        $data['locations'] = $this->EDL_model->get_locs($file_id);

        $data['FILE_ID'] = $file_id;
        $data['process'] = $this->EDL_model->get_process();
        $data['devices'] = $this->EDL_model->get_devices();
        $data['statuses'] = $this->EDL_model->get_status();
        $data['drawn_by'] = $this->EDL_model->get_drawn();
        $data['DATETIME'] = Date('d-M-Y');
        $data['content'] = 'pages/EDL/edit_file_locations_view';

        $this->load->view('master', $data);
    }

    public function update_delete_file_location()
    {
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);

        $action = $post['btnSubmit'];
        unset($post['btnSubmit']);

        if ($action == 'Delete') {
        $results = $this->EDL_model->delete_file_location($post);

            if ($results)
            {
                $this->session->set_flashdata('message', 'Location removed successfully.');
                redirect('EDL/update_file_locations/'.$post['FILE_ID']);
            }
        }
        if($action == 'Update') {

            $this->form_validation->set_rules('PROCESS', 'Process', 'required');
            $this->form_validation->set_rules('FILE_LOC', 'File Location', 'required');
            $this->form_validation->set_rules('DEVICE', 'Device', 'required');
            $this->form_validation->set_rules('DATETIME', 'Date', 'required');
            $this->form_validation->set_rules('STATUS', 'Status', 'required');
            $this->form_validation->set_rules('DRAWN', 'Drawn By', 'required');
            $this->form_validation->set_rules('FILE_LOC', 'File Location', 'trim|max_length[100]|xss_clean');
            $this->form_validation->set_rules('PHYSICAL_DESC', 'Physical Description', 'trim|max_length[255]|xss_clean');

            if ($this->form_validation->run()) {
                $results = $this->EDL_model->update_file_location($post);

                if ($results) {
                    $this->session->set_flashdata('message', 'Location updated successfully.');
                    redirect('EDL/update_file_locations/' . $post['FILE_ID']);
                }
            }else
            {
                $data['locations'] = $this->EDL_model->get_locs($post['FILE_ID']);

                // find the one being updated and reset its data to
                // what was posted
                foreach ($data['locations'] as $key => $location) {
                    if ($location['ID'] == $post['ID']) {
                        $data['locations'][$key]['PROCESS'] = $post['PROCESS'];
                        $data['locations'][$key]['DEVICE'] = $post['DEVICE'];
                        $data['locations'][$key]['STATUS'] = $post['STATUS'];
                        $data['locations'][$key]['DRAWN'] = $post['DRAWN'];
                        $data['locations'][$key]['DATETIME'] = $post['DATETIME'];
                        $data['locations'][$key]['FILE_LOC'] = $post['FILE_LOC'];
                        $data['locations'][$key]['PHYSICAL_DESC'] = $post['PHYSICAL_DESC'];

                        break;
                    }
                }

                unset($_POST['ID']);
                unset($_POST['PROCESS']);
                unset($_POST['DEVICE']);
                unset($_POST['STATUS']);
                unset($_POST['DRAWN']);
                unset($_POST['DATETIME']);
                unset($_POST['FILE_LOC']);
                unset($_POST['PHYSICAL_DESC']);

                $data['FILE_ID'] = $post['FILE_ID'];
                $data['process'] = $this->EDL_model->get_process();
                $data['devices'] = $this->EDL_model->get_devices();
                $data['statuses'] = $this->EDL_model->get_status();
                $data['drawn_by'] = $this->EDL_model->get_drawn();
                $data['content'] = 'pages/EDL/edit_file_locations_view';

                $this->load->view('master', $data);
            }
        }
//
//        if ($results)
//        {
//            $this->session->set_flashdata('message', 'Location removed successfully.');
//            redirect('EDL/update_file_locations/'.$post['FILE_ID']);
//        }
    }

    public function add_file_locations()
    {
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);

        $this->form_validation->set_rules('PROCESS', 'Process', 'required');
        $this->form_validation->set_rules('FILE_LOC', 'File Location', 'required');
        $this->form_validation->set_rules('DEVICE', 'Device', 'required');
        $this->form_validation->set_rules('DATETIME', 'Date', 'required');
        $this->form_validation->set_rules('STATUS', 'Status', 'required');
        $this->form_validation->set_rules('DRAWN', 'Drawn By', 'required');
        $this->form_validation->set_rules('FILE_LOC', 'File Location', 'trim|max_length[100]|xss_clean');
        $this->form_validation->set_rules('PHYSICAL_DESC', 'Physical Description', 'trim|max_length[255]|xss_clean');

        if ($this->form_validation->run())
        {
            $results = $this->EDL_model->insert_locs($post, $post['FILE_ID']);

            if ($results)
            {
                $this->session->set_flashdata('message', 'Location added successfully.');
                redirect('EDL/update_file_locations/'.$post['FILE_ID']);
            }
        }
        else
        {
            $data['locations'] = $this->EDL_model->get_locs($post['FILE_ID']);

            $data['add_location'] = $post;
            $data['FILE_ID'] = $post['FILE_ID'];
            $data['process'] = $this->EDL_model->get_process();
            $data['devices'] = $this->EDL_model->get_devices();
            $data['statuses'] = $this->EDL_model->get_status();
            $data['drawn_by'] = $this->EDL_model->get_drawn();
            $data['content'] = 'pages/EDL/edit_file_locations_view';

            $this->load->view('master', $data);
        }
    }

    /*
    ============================================================================
    table_maintenance
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    No return - this function sets up the data for the view.
    ============================================================================
    */
    public function table_maintenance()
    {

//        if (defined('ENVIRONMENT'))
//        {
//           switch (ENVIRONMENT)
//           {
//               case 'production':
//                   //Load Model
//                   // $this->load->model('EDL_model');
//
//                   // $data['originators'] = $this->EDL_model->get_originator();
//                   // $data['project_types'] = $this->EDL_model->get_project_type();
//                   $data['content'] = "templates/no_access_edl";
//                   $this->load->view('master', $data);
//                   return;
//                   break;
//               default:
//            }
//        }

        //Load Model
        $this->load->model('EDL_model');

        $table = $this->uri->segment(3);

        $data['title'] = $table;

        switch ($table)
        {
            case 'departments':
                $data['data'] = $this->EDL_model->get_departments();
                break;

            case 'devices':
                $data['data'] = $this->EDL_model->get_devices();
                break;

            case 'paper_classifications':
                $data['data'] = $this->EDL_model->get_paper_classifications();
                break;

            case 'electronic_classifications':
                $data['data'] = $this->EDL_model->get_electronic_classifications();
                break;

            case 'drawn':
                $data['data'] = $this->EDL_model->get_drawn();
                break;

            case 'processes':
                $data['data'] = $this->EDL_model->get_process();
                break;

            case 'originators':
                $data['data'] = $this->EDL_model->get_originator();
                break;

            case 'project_types':
                $data['data'] = $this->EDL_model->get_project_type();
                break;

            case 'locations':
                $data['data'] = $this->EDL_model->get_locations();
                break;

            case 'surveyors':
                $data['data'] = $this->EDL_model->get_surveyor();
                break;

            case 'survey_methods':
                $data['data'] = $this->EDL_model->get_survey_method();
                break;

            case 'software_versions':
                $data['data'] = $this->EDL_model->get_software_version();
                break;

            case 'statuses':
                $data['data'] = $this->EDL_model->get_status();
                break;

            default:
                break;
        }
        $data['content'] = "pages/EDL/table_maintenance_view";
        $this->load->view('master', $data);
    }

    public function add_table_maintenance()
    {
        //Load Model
        $this->load->model('EDL_model');

        $table = $this->uri->segment(3);

        $post = $this->input->post(null, true);

        if (empty($post))
        {
            switch ($table)
            {
                case 'departments':
                    $data['data'] = $this->EDL_model->get_departments();
                    break;

                case 'devices':
                    $data['data'] = $this->EDL_model->get_devices();
                    break;

                case 'paper_classifications':
                    $data['data'] = $this->EDL_model->get_paper_classifications();
                    break;

                case 'electronic_classifications':
                    $data['data'] = $this->EDL_model->get_electronic_classifications();
                    break;

                case 'drawn':
                    $data['data'] = $this->EDL_model->get_drawn();
                    break;

                case 'processes':
                    $data['data'] = $this->EDL_model->get_process();
                    break;

                case 'originators':
                    $data['data'] = $this->EDL_model->get_originator();
                    break;

                case 'project_types':
                    $data['data'] = $this->EDL_model->get_project_type();
                    break;

                case 'locations':
                    $data['data'] = $this->EDL_model->get_locations();
                    break;

                case 'surveyors':
                    $data['data'] = $this->EDL_model->get_surveyor();
                    break;

                case 'survey_methods':
                    $data['data'] = $this->EDL_model->get_survey_method();
                    break;

                case 'software_versions':
                    $data['data'] = $this->EDL_model->get_software_version();
                    break;

                case 'statuses':
                    $data['statuses'] = $this->EDL_model->get_status();
                    break;

                default:
                    redirect('EDL/table_maintenance');
                    break;
            }

            $data['fields'] = $this->EDL_model->get_table_fields($table);
            $data['title'] = $table;
            $data['content'] = "pages/EDL/add_table_maintenance_view";
            $this->load->view('master', $data);
        }
        else
        {
            $first_key = key($post);
            $key_description = str_replace('_', ' ', $first_key);

            $field_meta = $this->EDL_model->get_field_meta($post['table_name'], $first_key);

            $this->form_validation->set_rules($first_key, $key_description, 'required|max_length['.$field_meta->max_length.']');

            if ($this->form_validation->run() == true)
            {
                if ($this->EDL_model->add_to_maintenance_table($post))
                {
                    $this->session->set_flashdata('message', 'The record has been added successfully.');
                    redirect('EDL/table_maintenance/'.$post['table_name']);
                }
                else
                {
                    $this->session->set_flashdata('error', 'The record was not added, please try again.');
                    redirect('EDL/table_maintenance/'.$post['table_name']);
                }
            }
            else
            {
                $this->session->set_flashdata('error', validation_errors());
                redirect('EDL/table_maintenance/'.$post['table_name']);
            }
        }

    }

    public function update_table_maintenance()
    {
        //Load Model
        $this->load->model('EDL_model');

        $table = $this->uri->segment(3);
        $id = $this->uri->segment(4);

        $post = $this->input->post(null, true);

        if (empty($post))
        {
            switch ($table)
            {
                case 'departments':
                    $data['data'] = $this->EDL_model->get_departments($id);
                    break;

                case 'devices':
                    $data['data'] = $this->EDL_model->get_devices($id);
                    break;

                case 'paper_classifications':
                    $data['data'] = $this->EDL_model->get_paper_classifications($id);
                    break;

                case 'electronic_classifications':
                    $data['data'] = $this->EDL_model->get_electronic_classifications($id);
                    break;

                case 'drawn':
                    $data['data'] = $this->EDL_model->get_drawn($id);
                    break;

                case 'processes':
                    $data['data'] = $this->EDL_model->get_process($id);
                    break;

                case 'originators':
                    $data['data'] = $this->EDL_model->get_originator($id);
                    break;

                case 'project_types':
                    $data['data'] = $this->EDL_model->get_project_type($id);
                    break;

                case 'locations':
                    $data['data'] = $this->EDL_model->get_locations($id);
                    break;

                case 'surveyors':
                    $data['data'] = $this->EDL_model->get_surveyor($id);
                    break;

                case 'survey_methods':
                    $data['data'] = $this->EDL_model->get_survey_method($id);
                    break;

                case 'software_versions':
                    $data['data'] = $this->EDL_model->get_software_version($id);
                    break;

                case 'statuses':
                    $data['data'] = $this->EDL_model->get_status($id);
                    break;

                default:
                    redirect('EDL/table_maintenance');
                    break;
            }

            $data['title'] = $table;
            $data['content'] = "pages/EDL/update_table_maintenance_view";

            $this->load->view('master', $data);
        }
        else
        {

            $first_key = key($post);
            $key_description = str_replace('_', ' ', $first_key);

            $field_meta = $this->EDL_model->get_field_meta($post['table_name'], $first_key);

            $this->form_validation->set_rules($first_key, $key_description, 'required|max_length['.$field_meta->max_length.']');

            if ($this->form_validation->run() == true)
            {

                if ($this->EDL_model->update_maintenance_table($post))
                {
                    $this->session->set_flashdata('message', 'The record has been updated successfully.');
                    redirect('EDL/table_maintenance/'.$post['table_name']);
                }
                else
                {
                    $this->session->set_flashdata('error', 'The record was not updated, please try again.');
                    redirect('EDL/table_maintenance/'.$post['table_name']);
                }
            }
            else
            {
                $this->session->set_flashdata('error', validation_errors());
                redirect('EDL/table_maintenance/'.$post['table_name']);
            }
        }
    }

    public function delete_table_maintenance()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);

        if ($this->EDL_model->delete_from_maintenance_table($post))
        {
            $this->session->set_flashdata('message', 'The record has been deleted.');
            redirect('EDL/table_maintenance/'.$post['table_name']);
        }
    }

    /*
    ============================================================================
    surrounding_streets_query
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    No return - this function sets up the data for the view.
    ============================================================================
    */
    public function surrounding_streets_query()
    {
        //Load Model
        $this->load->model('EDL_model');

        $data['content'] = "pages/EDL/surrounding_streets_query_view";

        $this->load->view('master', $data);
    }

    /*
    ============================================================================
    add_surrounding_street
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    No return - handles adding owner record to the table.
    ============================================================================
    */
    public function add_surrounding_street()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);

        if (!empty($post['STREET_CODE_ADD']))
        {
            $data['FILE_ID'] = $post['FILE_ID'];
            $data['STREET_NUM'] = $post['STREET_CODE_ADD'];

            if ($this->EDL_model->insert_surrounding_street($data))
            {
                $this->session->set_flashdata('message', 'Surrounding street added successfully.');
                redirect('EDL/update_surrounding_streets/'.$post['FILE_ID']);
            }
            else
            {
                $this->session->set_flashdata('error', 'The surrounding street could not be added, please try again.');
                redirect('EDL/update_surrounding_streets/'.$post['FILE_ID']);
            }
        }
        else
        {
            redirect('EDL/update_surrounding_streets/'.$post['FILE_ID']);
        }

    }

    /*
    ============================================================================
    delete_surrounding_street
    ----------------------------------------------------------------------------
    NO PARAM
    ----------------------------------------------------------------------------
    No return - hadles passign the id to delete to the model to run the query.
    ============================================================================
    */
    public function delete_surrounding_street()
    {
        //Load Model
        $this->load->model('EDL_model');

        $data = $this->input->post(null, true);

        if ($this->EDL_model->remove_surrounding_street($data))
        {
            $this->session->set_flashdata('message', 'Surrounding street removed successfully.');
            redirect('EDL/update_surrounding_streets/'.$data['FILE_ID']);
        }
        else
        {
            $this->session->set_flashdata('error', 'The surrounding street could not be removed, please try again.');
            redirect('EDL/update_surrounding_streets/'.$data['FILE_ID']);
        }
    }

    /*
    ============================================================================
    update_surrounding_street
    ----------------------------------------------------------------------------
    NO PARAM
    ----------------------------------------------------------------------------
    No return - handles passing the data to model to update the specified record.
    ============================================================================
    */
    public function update_surrounding_streets()
    {
        //Load Model
        $this->load->model('EDL_model');

        $id_to_update = $this->uri->segment(3);


        // Get streets
        $temp = $this->EDL_model->get_surrounding_street_by_id($id_to_update);

        $data['streets'] = array();
        if (!empty($temp))
        {
            foreach ($temp as $street)
            {
                $results =	$this->EDL_model->get_street_by_id($street['STREET_NUM']);

                array_push($data['streets'], $results);
            }
        }

        $data['FILE_ID'] = $id_to_update;
        $data['content'] = 'pages/EDL/edit_surrounding_street_view';

        $this->load->view('master', $data);
    }



    /*
    ============================================================================
    plan_index_query
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    No return - this function sets up the data for the plan index view.
    ============================================================================
    */
    public function plan_index_query()
    {
        //Load Model
        $this->load->model('EDL_model');

        $data['content'] = "pages/EDL/plan_index_query_view";
        $this->load->view('master', $data);
    }

    /*
    ============================================================================
    plan_search
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    No return - this function retrieve's data based on the search params.
    ============================================================================
    */
    public function plan_search()
    {
        //Load Model
        $this->load->model('EDL_model');

        $search_string = $this->input->post('plan_number', true);

        if (!empty($search_string))
        {
            $this->form_validation->set_rules('plan_number', 'Plan Number', 'max_length[15]|xss_clean');
            if ($this->form_validation->run() == true)
            {
                $data['plans'] = $this->EDL_model->get_plan_like_id(strtoupper($search_string));
                if (!empty($data['plans'])){$data['plans'] = array_slice($data['plans'], 0, 500);}
                $data['content'] = "pages/EDL/plan_index_query_view";
                $this->load->view('master', $data);
            }
            else
            {
                $this->session->set_flashdata('error', validation_errors());
                redirect('EDL/plan_index_query');
            }
        }
        else
        {
            $data['content'] = "pages/EDL/plan_index_query_view";
            $this->load->view('master', $data);
        }
    }

    /*
    ============================================================================
    add_plan_index
    ----------------------------------------------------------------------------
    NO PARAMS
    ----------------------------------------------------------------------------
    No return - handles adding owner record to the table.
    ============================================================================
    */
    public function add_plan_index()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);


        if (!empty($post))
        {

            $this->form_validation->set_rules('PLAN_NUM', 'Plan Number', 'required');

            if ($this->form_validation->run() == true)
            {

                // Convert our dates into 3 separate variables.
                if (!empty($post['plan_date']))
                {
                    $plan_date = $this->EDL_model->convert_date($post['plan_date']);

                    $post['PLAN_DAY'] = $plan_date[0];
                    $post['PLAN_MONTH'] = $plan_date[1];
                    $post['PLAN_YR'] = $plan_date[2];
                }

                if (!empty($post['altered_date']))
                {
                    $altered_date = $this->EDL_model->convert_date($post['altered_date']);

                    $post['ALTER_DAY'] = $altered_date[0];
                    $post['ALTER_MTH'] = $altered_date[1];
                    $post['ALTER_YR'] = $altered_date[2];
                }

                unset($post['plan_date']);
                unset($post['altered_date']);

                // Loop through the post variables, anything with a value of 'on' is a check box and the
                // value needs to be converted to 'X' for the DB.
                foreach ($post as $key => $value)
                {
                    if ($value == 'on')
                    {
                        $post[$key] = 'X';
                    }
                    // Split the subject number and name into 2 variables.
                    if ($key == 'SUB_NUMBER' && !empty($value))
                    {
                        $subject = explode(',', $value);
                        $post['SUB_NUMBER'] = $subject[0];
                        $post['SUB_TITLE'] = $subject[1];
                    }
                }

                foreach ($post as $key => $value)
                {
                    $post[$key] = strtoupper($value);
                }

                if ($this->EDL_model->insert_plan($post))
                {
                    $this->session->set_flashdata('message', 'The plan was successfully added.');
                    redirect('EDL/plan_index_query');
                }
                else
                {
                    $this->session->set_flashdata('error', 'The plan could not be added, please try again.');
                    redirect('EDL/plan_index_query');
                }
            }
            else
            {
                $data['subdivision'] = $this->EDL_model->get_subdivisions();
                $data['subjects'] = $this->EDL_model->get_subjects();
                $data['content'] = "pages/EDL/add_plan_index_view";
                $this->load->view('master', $data);
            }

        }
        else
        {
            // error: no data in post.
            $data['subdivision'] = $this->EDL_model->get_subdivisions();
            $data['subjects'] = $this->EDL_model->get_subjects();
            $data['content'] = "pages/EDL/add_plan_index_view";
            $this->load->view('master', $data);
        }

    }

    /*
    ============================================================================
    delete_plan_index
    ----------------------------------------------------------------------------
    NO PARAM
    ----------------------------------------------------------------------------
    No return - hadles passign the id to delete to the model to run the query.
    ============================================================================
    */
    public function delete_plan_index()
    {
        //Load Model
        $this->load->model('EDL_model');

        $id_to_delete = $this->input->post('PLAN_NUM', true);

        if ($this->EDL_model->remove_plan($id_to_delete))
        {
            $this->session->set_flashdata('message', 'The plan was successfully deleted.');
            redirect('EDL/plan_index_query');
        }
        else
        {
            $this->session->set_flashdata('error', 'The plan could not be deleted, please try again.');
        }
    }

    /*
    ============================================================================
    update_plan_index
    ----------------------------------------------------------------------------
    NO PARAM
    ----------------------------------------------------------------------------
    No return - handles passing the data to model to update the specified record.
    ============================================================================
    */
    public function update_plan_index()
    {
        //Load Model
        $this->load->model('EDL_model');

        $id_to_update = $this->uri->segment(3);
        $post = $this->input->post(null, true);

        if (empty($post))
        {
            $data['subdivision'] = $this->EDL_model->get_subdivisions();
            $data['subjects'] = $this->EDL_model->get_subjects();
            $data['plan'] = $this->EDL_model->get_plan_by_id($id_to_update);
            $data['content'] = "pages/EDL/update_plan_index_view";
            $this->load->view('master', $data);
        }
        else
        {
            // Convert our dates into 3 separate variables.
            if (!empty($post['plan_date']))
            {
                $plan_date = $this->EDL_model->convert_date($post['plan_date']);

                $post['PLAN_DAY'] = $plan_date[0];
                $post['PLAN_MONTH'] = $plan_date[1];
                $post['PLAN_YR'] = $plan_date[2];
            }

            if (!empty($post['altered_date']))
            {
                $altered_date = $this->EDL_model->convert_date($post['altered_date']);

                $post['ALTER_DAY'] = $altered_date[0];
                $post['ALTER_MTH'] = $altered_date[1];
                $post['ALTER_YR'] = $altered_date[2];
            }

            unset($post['plan_date']);
            unset($post['altered_date']);

            // Loop through the post variables, anything with a value of 'on' is a check box and the
            // value needs to be converted to 'X' for the DB.
            foreach ($post as $key => $value)
            {
                if ($value == 'on')
                {
                    $post[$key] = 'X';
                }
                // Split the subject number and name into 2 variables.
                if ($key == 'SUB_NUMBER' && !empty($value))
                {
                    $subject = explode(',', $value);
                    $post['SUB_NUMBER'] = $subject[0];
                    $post['SUB_TITLE'] = $subject[1];
                }
            }

            foreach ($post as $key => $value)
            {
                $post[$key] = strtoupper($value);
            }


            if ($this->EDL_model->update_plan($post))
            {
                $this->session->set_flashdata('message', 'The plan was successfully updated.');
                redirect('EDL/update_plan_index/'.$post['PLAN_NUM']);
            }
            else
            {
                $this->session->set_flashdata('error', 'The plan could not me updated, please try again.');
                redirect('EDL/update_plan_index/'.$post['PLAN_NUM']);
            }
        }
    }

    public function ajax_street_name()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);

        $results = $this->EDL_model->get_streets(strtoupper($post['id']));

        echo json_encode($results);
    }

    public function ajax_street_id()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);

        $results = $this->EDL_model->get_street_by_id(strtoupper($post['id']));

        echo json_encode($results);
    }

    /*
     * searches for a file id
     */
    public function ajax_file_id()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post(null, true);

        $results = $this->EDL_model->get_file_by_id(strtoupper($post['id']));

        echo json_encode($results);
    }

    public function location_by_id()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post('id');
        $results = $this->EDL_model->get_location_by_id($post);
        echo json_encode($results);
    }

    public function originator_by_id()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post('id');
        $results = $this->EDL_model->get_originator_by_id($post);
        echo json_encode($results);
    }

    public function project_by_id()
    {
        //Load Model
        $this->load->model('EDL_model');

        $post = $this->input->post('id');
        $results = $this->EDL_model->get_project_by_id($post);
        echo json_encode($results);
    }
}

/* End of file EDL.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/EDL.php */