<div class="row">
	<div class="col-md-6">
		<h2>View Asset</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="name">Name</label>
						<input type="text" name="name" class="form-control" title="Asset Name" maxlength="50" value="<?= $asset[0]['name'] ?>" disabled />
						<input type="hidden" name="id" value="<?= $asset[0]['id'] ?>" />
					</div>
					<div class="col-sm-4">
						<label for="business_unit">Business Unit</label>
						<input type="text" name="business_unit" class="form-control" title="Business Unit" maxlength="50" value="<?= $asset[0]['business_unit'] ?>" disabled />
					</div>
					<div class="col-sm-4">
						<label for="section">Section</label>
						<input type="text" name="section" class="form-control" title="Section" maxlength="25" value="<?= $asset[0]['section'] ?>" disabled />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="phone_number">Phone Number</label>
						<input type="text" name="phone_number" class="form-control" title="Phone Number" maxlength="15" value="<?= $asset[0]['phone_number'] ?>" disabled />
					</div>
					<div class="col-sm-2">
						<label for="fax_number">Fax Number</label>
						<input type="text" name="fax_number" class="form-control" title="Fax Number" maxlength="15" value="<?= $asset[0]['fax_number'] ?>" disabled />
					</div>
					<div class="col-sm-2">
						<label for="civic_number">Civic Number</label>
						<input type="text" name="civic_number" class="form-control" title="Civic Number" maxlength="10" value="<?= $asset[0]['civic_number'] ?>" disabled />
					</div>
					<div class="col-sm-4">
						<label for="address_description">Address Description</label>
						<input type="text" name="address_description" class="form-control" title="Address Description" maxlength="50" value="<?= $asset[0]['address_description'] ?>" disabled />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-5">
						<label for="street_name">Street Name</label>
						<input type="hidden" name="address_id" class="address_id" value="" value="<?= $asset[0]['address_id'] ?>"  />
						<input type="text" name="street_name" class="form-control street_name" title="Street Name" maxlength="50" value="<?= $asset[0]['street_name'] ?>" disabled />
					</div>
				</div>
			</div>
		<legend></legend>

		<div class="form-group">
			<div class="row">
				 <div class="col-sm-12">
					<a href="<?php echo base_url(); ?>phonebook/" class="btn btn-default no_label_fix">Back to phonebook</a>
				 </div>
			</div>
		</div>
	</div>
</div>
