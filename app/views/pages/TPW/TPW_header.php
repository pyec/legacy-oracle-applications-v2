
		<nav class="navbar navbar-default navbar-fixed-top app_nav" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="nav_items">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle app_header" data-toggle="collapse" data-target=".navbar-ex2-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="<?php echo base_url(); ?>TPW" class="navbar-brand">TPW</a>
					</div>
				
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex2-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url(); ?>TPW/employee_courses">Employee Courses</a></li>
							<li><a href="<?php echo base_url(); ?>TPW/course_list">Course List</a></li>
							<li><a href="<?php echo base_url(); ?>TPW/employees">Employees</a></li>
							<li><a href="<?php echo base_url(); ?>TPW/endorsement">Endorsements</a></li>
							<li><a href="<?php echo base_url(); ?>TPW/licence_conditions">License Conditions</a></li>
						</ul>
					</div><!-- /.navbar-collapse -->
				</div>
			</div>
		</nav>

