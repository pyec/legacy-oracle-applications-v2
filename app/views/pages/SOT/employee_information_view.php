<div class="row">
	<div class="col-md-6">
		<h2>Officer Information</h2>
		<p>Select a officer to view more information about them.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php elseif (validation_errors()): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo validation_errors(); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>
<!-- <div class="row"> -->
	<form action="<?php echo base_url(); ?>SOT/employee_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="employee_number">Officer #</label>
					<input type="text" name="employee_number" class="form-control" title="Officer Number" value="<?php echo set_value('employee_number') ?>" tabindex="1" placeholder="Officer Number" />
				</div>
				<div class="col-sm-3">
					<label for="last_name">Last Name</label>
					<input type="text" name="last_name" class="form-control" title="Last Name" value="<?php echo set_value('last_name') ?>" tabindex="1" placeholder="Last Name" />
				</div>
				<div class="col-sm-3">
					<label for="first_name">First Name</label>
					<input type="text" name="first_name" class="form-control" title="First Name" value="<?php echo set_value('first_name') ?>" tabindex="1" placeholder="First Name" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="sot_employee_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
	
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($employees)): ?>
			<h2 class="count"><?php echo count($employees) ?> employees</h2>
		<?php endif ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<a href="<?php echo base_url(); ?>SOT/add_employee" class="btn btn-success">Add Officer</a>
		</div>
	</div>
</div>

<?php if (!empty($employees)): ?>
<div class="row">
	<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<th>Officer Id</th>
						<th>Name</th>
						<th>Agency</th>
						<th>Action</th>
					</thead>
					<tbody class="tbody">
						<?php foreach ($employees as $employee): ?>
							<tr>
								<td><a href="<?php echo base_url(); ?>SOT/update_employee/<?= $employee['EMPL_ID'] ?>"><?php echo $employee['EMPL_ID'] ?></a></td>
								<td><?php echo ucwords(strtolower($employee['LAST_NME'].', '.$employee['FIRST_NME'].' '.$employee['MIDDLE_NME'])); ?></td>
								<td><?php echo ucwords(strtolower($employee['AGENCY_NME'])); ?></td>
								<td>
									<form action="<?php echo base_url(); ?>SOT/delete_employee" method="post">
										<button type="submit" class="delete_employee btn btn-sm btn-danger" ><i class="fa fa-minus-square"></i></button>
										<input type="hidden" name="EMPL_ID" value="<?= $employee['EMPL_ID'] ?>" />
									</form>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
	</div>
</div>
<?php endif ?>