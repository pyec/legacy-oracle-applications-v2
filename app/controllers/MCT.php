<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Mayor Call Tracking System

Description : This system tracks all of the messages that the mayor recieves from
			HRM residents and allows users to keep track of the calls status.

  Functions : index(), 
  		add_call(), 
  		update_call(),
		delete_call(),
  		resolved_calls(), 
  		add_resolved_call(),
  		update_resolved_call(),
  		delete_resolved_call(),
  		call_types(),
  		add_call_type(),
  		update_call_types(),
  		delete_call_type(),
  		district_councillors(), 
  		add_councillor(), 
  		update_district_councillor(),
  		delete_district_councillor(),
  		source_types(),
  		add_source_type(),
  		update_source_types(),
  		delete_source_types()

=========================================================================================================================*/
class MCT extends CI_Controller 	
{

	public function index()
	{
		$data['content'] = "pages/MCT/home_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
call_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function resolved_call_search()
	{
		// Load model
		$this->load->model('MCT_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('call_number', 'Call Number', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array))
			{
				
				$data['resolved_calls'] = $this->MCT_model->search_resolved_calls($search_array);
			}
			else
			{
				redirect('MCT/resolved_calls');
			}
		}
		else
		{
			redirect('MCT/resolved_calls');
		}

		if (!empty($data['resolved_calls'])) {$data['resolved_calls'] = array_slice($data['resolved_calls'], 0, 500);}
		$data['content'] = "pages/MCT/resolved_calls_view";
		$this->load->view('master', $data);
	}

	public function open_call_search()
	{
		// Load model
		$this->load->model('MCT_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('call_number', 'Call Number', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array))
			{
				$data['open_calls'] = $this->MCT_model->search_open_calls($search_array);
			}
			else
			{
				redirect('MCT/open_calls');
			}
		}
		else
		{
			redirect('MCT/open_calls');
		}

		if (isset($data['open_calls'])) {$data['open_calls'] = array_slice($data['open_calls'], 0, 500);}

		$data['content'] = "pages/MCT/open_calls_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
open_calls
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function open()
	{
		//Load Model 
		$this->load->model('MCT_model');

		$data['content'] = "pages/MCT/open_calls_view";
		$this->load->view('master', $data);
	}

	public function print_view()
	{

		//Load Model 
		$this->load->model('MCT_model');

		$id_to_update = $this->uri->segment(3);
		$resolved = $this->uri->segment(4);

		// Get the call data via ID 
		$data['call'] = $this->MCT_model->get_call_by_id($id_to_update, $resolved);

		$body = '';

		foreach ($data['call'][0] as $key => $value) 
		{

			$current_label = explode('_', $key);
			$label = '';

			foreach ($current_label as $val) 
			{
				if ($val == 'MCT')
				{
					continue;
				}
				else
				{
					$label .= ucfirst(strtolower($val)); 
				}
			}

			$body .= $label." : ".$value."<br>";
		}
		echo $body;
	}

	public function ajax_district_lookup()
	{
		//Load Model 
		$this->load->model('MCT_model');
		$this->load->model('address_model');

		$civic_name = $this->input->post('civic_name');
		$civic_number = $this->input->post('civic_number');

		// $addrkey = $this->MCT_model->get_civic_address_key('CIRCLE', '39')[0]['ADDRKEY'];
		$addrkey = $this->MCT_model->get_civic_address_key($civic_name, $civic_number)[0]['ADDRKEY'];
		$prclkey = $this->MCT_model->get_parcel_key($addrkey)[0]['PRCLKEY'];

		$district = $this->address_model->get_district($prclkey)[0]['DISTRICT'];

		echo json_encode($district);
	}

/*           
============================================================================
add_call
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add()
	{
		//Load Model
		$this->load->model('MCT_model');

		$post_data = $this->input->post(null, true);

		if (!empty($post_data))
		{
			$post_data['MCT_NBR'] = $this->MCT_model->get_next_val();
			$post_data['MCT_USER'] = $this->session->userdata('samaccountname');
			

			$this->form_validation->set_rules('MCT_SOURCE', 'Call Source', 'required');
			$this->form_validation->set_rules('MCT_TYPE_DESC', 'Call Type', 'required');
			$this->form_validation->set_rules('MCT_LST_NME', 'Last Name', 'required');
			$this->form_validation->set_rules('MCT_PRIORITY', 'Priority', 'required');
			$this->form_validation->set_rules('MCT_ENTER_DTE', 'Entered Date', 'required');
			$this->form_validation->set_rules('MCT_DUE_DTE', 'Due Date', 'required');

			if ($this->form_validation->run() == true)
			{
				if ($this->MCT_model->insert_call($post_data))
				{
					$this->session->set_flashdata('message', 'The call was successfully added.');
					redirect('MCT/update_open/'.$post_data['MCT_NBR']);
				}
				else
				{
					$this->session->set_flashdata('error', 'The call could not be added, please try again.');
					redirect('MCT/add_call');
				}	
			}
			else
			{
				// $data['districts'] = $this->MCT_model->get_district_councillors();
				$data['source_types'] = $this->MCT_model->get_source_types();
				$data['call_types'] = $this->MCT_model->get_call_types();
				$data['content'] = "pages/MCT/add_call_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			// $data['districts'] = $this->MCT_model->get_district_councillors();
			$data['source_types'] = $this->MCT_model->get_source_types();
			$data['call_types'] = $this->MCT_model->get_call_types();
			$data['content'] = "pages/MCT/add_call_view";
			$this->load->view('master', $data);
		}
	}

	/*           
============================================================================
update_open_call
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_open()
	{
		//Load Model
		$this->load->model('MCT_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		
		if (!empty($post))
		{
			$this->form_validation->set_rules('MCT_SOURCE', 'Call Source', 'required');
			$this->form_validation->set_rules('MCT_TYPE_DESC', 'Call Type', 'required');
			$this->form_validation->set_rules('MCT_LST_NME', 'Last Name', 'required');
			$this->form_validation->set_rules('MCT_PRIORITY', 'Priority', 'required');
			$this->form_validation->set_rules('MCT_ENTER_DTE', 'Entered Date', 'required');
			$this->form_validation->set_rules('MCT_DUE_DTE', 'Due Date', 'required');
			$post['MCT_USER'] = $this->session->userdata('samaccountname');

			if ($this->form_validation->run() == true)
			{
				if ($this->MCT_model->update_open_call($post))
				{
					if ($post['MCT_RESOLVE_FLG'] == 'Y')
					{
						$this->session->set_flashdata('message', 'The call was successfully updated.');
						redirect('MCT/update_resolved/'.$post['MCT_NBR']);
					}
					else
					{
						$this->session->set_flashdata('message', 'The call was successfully updated.');
						redirect('MCT/update_open/'.$post['MCT_NBR']);
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'The call could not be updated, please try again.');
					redirect('MCT/update_open/'.$post['MCT_NBR']);
				}	
			}
			else
			{
				redirect('MCT/update_open/'.$post['MCT_NBR']);
			}
			
		}
		else
		{
			// Get the call data via ID 
			$data['call'] = $this->MCT_model->get_call_by_id($id_to_update, "N");

			// We need to create a mailto link on the update page, the subject of the email will be defined in the view
			// however we need to progmatically create the body string to echo into the link on the view.

			$email_body = '';

			// Loop through each field in the array separating the keys and values so we can work with both.
			foreach ($data['call'][0] as $key => $value) 
			{
				// The $key represents the label for the piece of data in the email and the field name in the DB.
				// This is currently in all capitals and separated with underscores and this isn't readable so... (Example MCT_FST_NME)

				// split the $key on underscores making an array of words
				$current_label = explode('_', $key);
				$label = '';

				// loop through the words to craete a new label that will be included in the email.
				foreach ($current_label as $val) 
				{
					// if the current word we are looking at is 'MCT' (every field but one starts with this) skip it becuase the user doesn't need it in the email
					if ($val == 'MCT')
					{
						continue;
					}
					else
					{
						// else make the string lower case then upper case the first letter and add a encoded character space at the end.
						$label .= ucfirst(strtolower($val)).'%20'; 
					}
				}

				// once the label is properly formatted create another string with the current key : value pairs using url character encoding for the space and new lines.
				$email_body .= $label.'%20:%20'.$value.'%0A';
			}

			// $data['districts'] = $this->MCT_model->get_district_councillors();

			$this->load->model('EDL_model');
			$data['street_name'] = $this->EDL_model->get_street_by_id($data['call'][0]['STREET_CDE'])['STREET'];

			$data['source_types'] = $this->MCT_model->get_source_types();
			$data['call_types'] = $this->MCT_model->get_call_types();
			$data['email_body'] = $email_body;
			$data['content'] = "pages/MCT/update_open_call_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
delete_open_call
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_open()
	{
		//Load Model
		$this->load->model('MCT_model');

		$post = $this->input->post(null, true);

		if ($this->MCT_model->remove_open_call($post['MCT_NBR']))
		{
			$this->session->set_flashdata('message', 'The call was delete.');
			redirect('MCT/open');
		}
		else
		{
			$this->session->set_flashdata('error', 'The call could not be removed, please try again.');
			redirect('MCT/open');
		}
	}
/*           
============================================================================
resolved_calls
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function resolved()
	{
		//Load Model 
		$this->load->model('MCT_model');

		$data['content'] = "pages/MCT/resolved_calls_view";
		$this->load->view('master', $data);
	}


/*           
============================================================================
update_resolved_call
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_resolved()
	{
		//Load Model
		$this->load->model('MCT_model');

		$id_to_update = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			$post['MCT_USER'] = $this->session->userdata('samaccountname');

			if ($this->MCT_model->update_open_call($post))
			{
				if ($post['MCT_RESOLVE_FLG'] == 'N')
				{
					$this->session->set_flashdata('message', 'The call was successfully updated.');
					redirect('MCT/update_open/'.$id_to_update);
				}
				else
				{
					$this->session->set_flashdata('message', 'The call was successfully updated.');
					redirect('MCT/update_resolved/'.$id_to_update);
				}
			}
			else
			{
				$this->session->set_flashdata('error', 'The call could not be updated, please try again.');
				redirect('MCT/update_resolved/'.$id_to_update);
			}	
		}
		else
		{
			// Get the call data via ID 
			$data['call'] = $this->MCT_model->get_call_by_id($id_to_update, "Y");

			// We need to create a mailto link on the update page, the subject of the email will be defined in the view
			// however we need to progmatically create the body string to echo into the link on the view.

			$email_body = '';

			// Loop through each field in the array separating the keys and values so we can work with both.
			foreach ($data['call'][0] as $key => $value) 
			{
				// The $key represents the label for the piece of data in the email and the field name in the DB.
				// This is currently in all capitals and separated with underscores and this isn't readable so... (Example MCT_FST_NME)

				// split the $key on underscores making an array of words
				$current_label = explode('_', $key);
				$label = '';

				// loop through the words to craete a new label that will be included in the email.
				foreach ($current_label as $val) 
				{
					// if the current word we are looking at is 'MCT' (every field but one starts with this) skip it becuase the user doesn't need it in the email
					if ($val == 'MCT')
					{
						continue;
					}
					else
					{
						// else make the string lower case then upper case the first letter and add a encoded character space at the end.
						$label .= ucfirst(strtolower($val)).'%20'; 
					}
				}

				// once the label is properly formatted create another string with the current key : value pairs using url character encoding for the space and new lines.
				$email_body .= $label.'%20:%20'.$value.'%0A';
			}

			// $data['districts'] = $this->MCT_model->get_district_councillors();
			$data['source_types'] = $this->MCT_model->get_source_types();
			$data['call_types'] = $this->MCT_model->get_call_types();
			$data['email_body'] = $email_body;
			$data['content'] = "pages/MCT/update_resolved_call_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
delete_resolved_call
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_resolved()
	{
		//Load Model
		$this->load->model('MCT_model');

		$post = $this->input->post(null, true);

		if ($this->MCT_model->remove_resolved_call($post['MCT_NBR']))
		{
			$this->session->set_flashdata('message', 'The call was deleted.');
			redirect('MCT/resolved');
		}
		else
		{
			$this->session->set_flashdata('error', 'The call could not be removed, please try again.');
			redirect('MCT/resolved');
		}
	}

/*           
============================================================================
call_types
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function types()
	{
		//Load Model 
		$this->load->model('MCT_model');

		$data['call_types'] = $this->MCT_model->get_call_types();
		$data['content'] = "pages/MCT/call_types_view";
		$this->load->view('master', $data);
	}

	public function type_search()
	{
		//Load Model 
		$this->load->model('MCT_model');

		$post = $this->input->post(null, true);

		if ($post['type_name'] != '')
		{
			$data['call_types'] = $this->MCT_model->search_types($post['type_name']);
			$data['content'] = "pages/MCT/call_types_view";
			$this->load->view('master', $data);
		}
		else
		{
			$data['call_types'] = $this->MCT_model->get_call_types();
			$data['content'] = "pages/MCT/call_types_view";
			$this->load->view('master', $data);
		}
	}
/*           
============================================================================
add_call_type
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_type()
	{
		//Load Model
		$this->load->model('MCT_model');

		$post_data = $this->input->post(null, true);


		if (!empty($post_data))
		{
			if ($this->MCT_model->insert_call_type($post_data))
			{
				$this->session->set_flashdata('message', 'The call type was added.');
				redirect('MCT/types');
			}
			else
			{
				$this->session->set_flashdata('error', 'There was an error adding the call type, please try again.');
				redirect('MCT/types');
			}
		}
		else
		{
			$data['content'] = "pages/MCT/add_call_type_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
update_call_types
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_types()
	{
		//Load Model
		// $this->load->model('MCT_model');

		$post_data = $this->input->post(null, true);

		// if ($this->MCT_model->update_call_type($post_data['id']))
		// {
		// 	// Return a success message.
		// }
		// else
		// {
		// 	// Return an error message.
		// }

		$this->call_types();
	}

/*           
============================================================================
delete_call_type
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_type()
	{
		//Load Model
		$this->load->model('MCT_model');

		$post_data = $this->input->post(null, true);

		if ($this->MCT_model->remove_call_type($post_data['MCT_TYPE_DESC']))
		{
			$this->session->set_flashdata('message', 'The call type was removed successfully.');
			redirect('MCT/types');
		}
		else
		{
			$this->session->set_flashdata('error', 'There was an error removing the call type, please try again.');
			redirect('MCT/types');
		}
	}

/*           
============================================================================
district_councillors
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	// public function district_councillors()
	// {
	// 	//Load Model 
	// 	$this->load->model('MCT_model');

	// 	$data['district_councillors'] = $this->MCT_model->get_district_councillors();
	// 	$data['content'] = "pages/MCT/district_councillors_view";
	// 	$this->load->view('master', $data);
	// }

/*           
============================================================================
add_councillor
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	// public function add_councillor()
	// {
	// 	//Load Model
	// 	$this->load->model('MCT_model');

	// 	$post_data = $this->input->post(null, true);

	// 	$this->form_validation->set_rules('MCT_DISTRICT', 'District Number', 'required');

	// 	if (!empty($post_data))
	// 	{
	// 		if ($this->form_validation->run() == true)
	// 		{
	// 			if ($this->MCT_model->insert_district_councillor($post_data))
	// 			{
	// 				$this->session->set_flashdata('message', 'The district councillor was successfully added.');
	// 				redirect('MCT/district_councillors');
	// 			}
	// 			else
	// 			{
	// 				$this->session->set_flashdata('error', 'The district councillor could not be added.');
	// 				redirect('MCT/add_councillor');
	// 			}
	// 		}
	// 		else
	// 		{
	// 			$this->session->set_flashdata('error', validation_errors());
	// 			redirect('MCT/add_councillor');
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$data['content'] = "pages/MCT/add_councillor_view";
	// 		$this->load->view('master', $data);
	// 	}
	// }

/*           
============================================================================
update_councillors
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	// public function update_councillor()
	// {	
	// 	//Load Model
	// 	$this->load->model('MCT_model');

	// 	$id_to_update = $this->uri->segment(3);

	// 	$post = $this->input->post(null, true);

	// 	if (empty($post))
	// 	{
	// 		$data['councillor'] = $this->MCT_model->get_district_councillor_by_id($id_to_update);
			
	// 		$data['content'] = "pages/MCT/update_councillor_view";
	// 		$this->load->view('master', $data);
	// 	}
	// 	else
	// 	{
	// 		if ($this->MCT_model->update_district_councillor($post))
	// 		{
	// 			$this->session->set_flashdata('message', 'The district councillor was successfully updated.');
	// 			redirect('MCT/district_councillors');
	// 		}
	// 		else
	// 		{
	// 			$this->session->set_flashdata('error', 'The district councillor could not be updated, please try again.');
	// 			redirect('MCT/district_councillors');
	// 		}
	// 	}
	// }

/*           
============================================================================
delete_councillor
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	// public function delete_councillor()
	// {
	// 	//Load Model
	// 	$this->load->model('MCT_model');

	// 	$post_data = $this->input->post(null, true);

	// 	if (!empty($post_data))
	// 	{
	// 		if ($this->MCT_model->remove_district_councillor($post_data['MCT_DISTRICT']))
	// 		{
	// 			$this->session->set_flashdata('message', 'The district councillor was successfully deleted.');
	// 			redirect('MCT/district_councillors');
	// 		}
	// 		else
	// 		{
	// 			$this->session->set_flashdata('error', 'The district councillor could not be deleted, please try again.');
	// 			redirect('MCT/district_councillors');
	// 		}
	// 	}
	// 	else
	// 	{
	// 		$this->session->set_flashdata('error', 'The district councillor could not be deleted, please try again.');
	// 		redirect('MCT/district_councillors');
	// 	}
	// }

/*           
============================================================================
source_types
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function source_types()
	{
		//Load Model 
		$this->load->model('MCT_model');

		$data['source_types'] = $this->MCT_model->get_source_types();
		$data['content'] = "pages/MCT/source_types_view";
		$this->load->view('master', $data);
	}

	public function source_search()
	{
		//Load Model 
		$this->load->model('MCT_model');

		$post = $this->input->post(null, true);

		if ($post['source_name'] != '')
		{
			$data['source_types'] = $this->MCT_model->search_sources($post['source_name']);
			$data['content'] = "pages/MCT/source_types_view";
			$this->load->view('master', $data);
		}
		else
		{
			$data['source_types'] = $this->MCT_model->get_source_types();
			$data['content'] = "pages/MCT/source_types_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
add_source_type
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_source_type()
	{
		//Load Model
		$this->load->model('MCT_model');

		$post_data = $this->input->post(null, true);


		if (!empty($post_data['MCT_SOURCE_DESC']))
		{
			if ($this->MCT_model->insert_source_types($post_data))
			{
				$this->session->set_flashdata('message', 'The source type was successfully added.');
				redirect('MCT/source_types');
			}
			else
			{
				$this->session->set_flashdata('error', 'There was an error adding the source type to the list.');
				redirect('MCT/source_types');
			}
		}
		else
		{
			$data['content'] = "pages/MCT/add_source_type_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
update_source_types
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_source_types()
	{
		//Load Model
		// $this->load->model('MCT_model');

		$post_data = $this->input->post(null, true);

		// if ($this->MCT_model->update_source_type($post_data['id']))
		// {
		// 	// Return a success message.
		// }
		// else
		// {
		// 	// Return an error message.
		// }

		$this->source_types();
	}

/*           
============================================================================
delete_source_type
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_source_type()
	{
		//Load Model
		$this->load->model('MCT_model');

		$post_data = $this->input->post(null, true);

		if (!empty($post_data['MCT_SOURCE_DESC']))
		{
			if ($this->MCT_model->remove_source_type($post_data['MCT_SOURCE_DESC']))
			{
				$this->session->set_flashdata('message', 'The source type was deleted successfully.');
				redirect('MCT/source_types');
			}
			else
			{
				$this->session->set_flashdata('error', 'There was an error deleting the source type from the list.');
				redirect('MCT/source_types');
			}	
		}
		else
		{
			$this->session->set_flashdata('error', 'There was an error deleting the source type from the list.');
			redirect('MCT/source_types');
		}
	}

}

/* End of file MCT.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/MCT.php */