<div class="row">
	<div class="col-md-12">
		<h2>Course List</h2>
		<p class="lead">Select an course to view/update their courses.</p>
	</div>
</div>
	<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif($this->session->flashdata('error')) : ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	<form action="<?= base_url(); ?>TPW/search_courses" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="COURSE_NUM">Course #</label>
					<input type="text" name="COURSE_NUM" class="form-control" title="Course Number" value="<?= set_value('COURSE_NUM') ?>" tabindex="1" placeholder="Course Number" />
				</div>
				<div class="col-sm-3">
					<label for="COURSE_NME">Course Name</label>
					<input type="text" name="COURSE_NME" class="form-control" title="Course Name" value="<?= set_value('COURSE_NME') ?>" tabindex="2" placeholder="Course Name" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right  phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right " tabindex="9" onclick="pwt_courses_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
	<div class="row">
		<div class="col-md-12">
			<?php if (!empty($courses)): ?>
				<h2 class="count count_spacing"><?= count($courses) ?> courses</h2>
			<?php endif ?>
			<a href="<?= base_url(); ?>TPW/add_course" class="btn btn-md btn-success pull-right">Add Course</a>
		</div>
	</div>
<!-- </div> -->
<?php if (!empty($courses)): ?>
<div class="row no_label_fix">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Course</th>
				<th>Recert Required</th>
				<th>Recert Period (yrs)</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
					<?php foreach ($courses as $course): ?>
						<tr>
							<td><a href="<?= base_url(); ?>TPW/update_course/<?= $course['COURSE_NUM'] ?>"><?= $course['COURSE_NUM'] ?> - <?= ucwords(strtolower($course['COURSE_NME'])) ?></a></td>
							<td><?= $flag = ($course['RECERT_FLG'] == 'Y' ? 'Yes':'No'); ?></td>
							<td><?= $course['RECERT_PERIOD'] ?></td>
							<td>
								<form action="<?= base_url(); ?>TPW/delete_course" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger delete_course"><i class="fa fa-minus-square"></i></button>
									<input type="hidden" name="COURSE_NUM" value="<?= $course['COURSE_NUM'] ?>" />
								</form>
							</td>
						</tr>
					<?php endforeach ?>
			</tbody>	
		</table>
	</div>
</div>
<?php endif ?>