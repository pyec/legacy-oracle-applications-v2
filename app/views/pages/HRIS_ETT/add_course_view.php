<div class="row">
	<div class="col-md-12">
		<h2>Add Course</h2>
		<p class="lead">This form will allow you to add a new course to the system.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?php echo base_url(); ?>HRIS_ETT/add_course" method="post" >
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="COURSE_NO">Course Number</label><span class="text-danger">*</span>
						<input type="text" name="COURSE_NO" class="form-control" title="Course Number" maxlength="6" />
					</div>

					<div class="col-sm-4">
						<label for="COURSE_TITLE">Course Name</label><span class="text-danger">*</span>
						<input type="text" name="COURSE_TITLE" class="form-control" title="Course Name" maxlength="30" />
					</div>
					
					<div class="col-sm-2">
						<label for="COURSE_CLASS">Course Category</label>
						<select name="COURSE_CLASS" class="form-control" title="Course Category" >
							<option value="C">C</option>
							<option value="H">H</option>
							<option value="S">S</option>
							<option value="T">T</option>
						</select>
					</div>
				</div>
			</div>

			<legend></legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-md btn-primary add_course pull-right" value="Add Course" />
						<a href="<?php echo base_url(); ?>HRIS_ETT" class="btn btn-md btn-default return">Back to Courses</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>