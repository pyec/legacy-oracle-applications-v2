<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Business Unit Update

Description : This is the controller class for the Business unit update form.
				This form updates whether or not 

  Functions : index(), 
  			 add_BU(), 
  		  delete_BU(),
  		  update_BU()

=========================================================================================================================*/
class BU_update extends CI_Controller 
{
	public function index()
	{
		//Load Model
		$this->load->model('business_unit_update_model');

		$data['business_units'] = $this->business_unit_update_model->get_business_units();
		$data['content'] = "pages/BU_update/home_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_BU
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return but upon a success or fail on insert, set a flash message for the 
user.
============================================================================
*/

	public function add_BU()
	{
		//Load Model
		$this->load->model('business_unit_update_model');

		// Get all of the POST variables.
		$post_data = $this->input->post(null, true);

		$this->form_validation->set_rules('add_business_unit_name', 'Business Unit Name', 'required');

		if ($this->form_validation->run() == true)
		{
			// Attempt an insert and redirect accordingly.
			if ($this->business_unit_update_model->insert_business_unit($post_data))
			{
				$this->session->set_flashdata('message', 'Business unit added successfully.');
				redirect('BU_update');
			}
			else
			{
				$this->session->set_flashdata('error', 'The business unit could not be added, please try again.');
				redirect('BU_update');
			}	
		}
		else
		{
			$data['business_units'] = $this->business_unit_update_model->get_business_units();
			$data['content'] = "pages/BU_update/home_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
delete_BU
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return but set flash message on success or fail of record deletion.
============================================================================
*/

	public function remove_BU()
	{
		//Load Model
		$this->load->model('business_unit_update_model');

		// Get the id to delete in the post
		$id = $this->input->post('BUSUNIT_CDE', true);

		// Attempt the delete and redirect accordingly
		if ($this->business_unit_update_model->delete_business_unit($id))
		{
			$this->session->set_flashdata('message', 'Business unit removed successfully.');
			redirect('BU_update');
		}
		else
		{
			$this->session->set_flashdata('error', 'The business unit could not be removed, please try again.');
			redirect('BU_update');
		}
	}

/*           
============================================================================
update_BU
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return but set a flash message after record update.
============================================================================
*/

	public function update_BU()
	{
		//Load Model
		$this->load->model('business_unit_update_model');

		$unit_id = $this->uri->segment(3);
		$post_data = $this->input->post(null, true);

		if (!empty($post_data))
		{
			$this->form_validation->set_rules('BUSUNIT_DSC', 'Business Unit Description', 'max_length[40]');

			if ($this->form_validation->run() == true)
			{
				if ($this->business_unit_update_model->update_business_unit($post_data))
				{
					$this->session->set_flashdata('message', 'The business unit was successfully updated');
					redirect('BU_update');
				}
			}
			else
			{
				$data['business_units'] = $this->business_unit_update_model->get_business_units();
				$data['content'] = "pages/BU_update/home_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			$data['business_unit'] = $this->business_unit_update_model->get_business_unit_by_id($unit_id);
			$data['content'] = "pages/BU_update/update_view";
			$this->load->view('master', $data);
		}
	}	

}

/* End of file BU_update.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/BU_update.php */