<?php 
	if (!$this->session->userdata('samaccountname'))
	{
		$this->session->set_userdata(array('url' => current_url()));
		redirect('login');
	}

	// Bool to track if the url was requested by user that should hav acces to that page.
	$valid_user = true;
	$groups = $this->session->userdata('groups');
	$app = $this->uri->segment(1);

	// Switch on the app name that the user is trying to access
	// for every case loop through the logged in users groups, if the user has the appropriate group then give them access.
	switch ($app) {
		case 'BU_update':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsBusinessUnitUpdate')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'HRIS_ETT':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsHRISTraining')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'TPW':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsTPWTraining')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'SOT':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsSummaryOffenseTickets')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'EDL':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsEngineeringDrawingLocation' || $group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsEngineeringDrawingLocationReadOnly' || $group == 'AppHRMAppsEngineeringDrawingLocationReadWrite')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'MCT':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsMayorCallTracking')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'traffic':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsTrafficVolumes')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'street_file':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsStreetFileUpdate')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'cemetery':
			foreach ($groups as $group) 
			{
				if ($group == 'AppHRMAppsCemetery')
				{
					$valid_user = true;	
				}
			}
			break;

		case 'welcome':
			$valid_user = true;	
			break;

		case 'phonebook':
			$valid_user = true;	
			break;

		default:
			
			break;
			}

	// do a check to see if the user is a system admin.
	if (!empty($groups))
	{
		foreach ($groups as $group) 
		{
			if ($group == 'AppHRMAppsAdmin')
			{
				$valid_user = true;	
			}
		}
	}

	// If the user isn't valid send them back to the welcome screen.
	if (!$valid_user)
	{
		$this->session->set_flashdata('error', 'You do not have access to this application.');
		redirect('welcome');
	}
 ?>
<!DOCTYPE HTML>
<html lang="en">
	<head>
		<?php 
			// Switch on the app title to determine the title tag.
			$app = $this->uri->segment(1);
			$title = "";
			switch ($app) {
				case 'BU_update':
					$title = "HRM - Business Units";
					break;
				case 'phonebook':
					$title = "HRM - Contacts";
					break;
				case 'HRIS_ETT':
					$title = "HRM - HRIS Training";
					break;
				case 'TPW':
					$title = "HRM - TPW Training";
					break;
				case 'SOT':
					$title = "HRM - Summary Offence Tickets";
					break;
				case 'EDL':
					$title = "HRM - Engineer Drawing Location";
					break;
				case 'MCT':
					$title = "HRM - Mayor Call Tracking";
					break;
				case 'traffic':
					$title = "HRM - Traffic Surveys";
					break;
				case 'street_file':
					$title = "HRM - Street File Update";
					break;
				case 'cemetery':
					$title = "HRM - Cemetery";
					break;
				default:
					$title = "HRM - Apps";
					break;
			}
		 ?>
		<title><?php echo $title ?></title>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name="keywords" content="badass" />
		<meta name="description" content="" />
		<meta name="author" content="Gian Pompilio, Branden Morin" />
		<!-- Favicon -->
		<link rel="icon" type="image/png" href="">
		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.datetimepicker.css">
		<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.custom.min.css">-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/custom.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/print.css" type="text/css" media="print"/> 

		<!--[if lte IE 8]>
			<script src="<?php echo base_url(); ?>assets/js/respond.js"></script>
		<![endif]-->

		<!--[if lte IE 9]>
			<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/IE9.css">
		<![endif]-->


	</head>
<!-- This is the main header and all application headers look simillar to this except
		for navigation items that are app specific. -->		


	<body>
		<nav class="navbar navbar-inverse main_nav" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="nav_items">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="<?php echo base_url(); ?>" >HRM Applications</a>
					</div>
				
					<!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <!-- And app specific Nav items would be here in the form of a <ul> -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Application list <b class="caret"></b></a>
                                <!-- Here is where we include or app list resource. -->
                                <?php include_once("app/views/templates/app_list.php"); ?>
                            </li>
                            <li class="dropdown">
                                <!-- Navbar floated to the right for user options and the app list -->
                                <a href="<?= base_url() ?>login/logout">Logout <?= $this->session->userdata('first_name') ?></a>
                                <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $this->session->userdata('name') ?> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo base_url(); ?>login/logout">Logout</a></li>
								</ul> -->
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
				</div>
			</div>
		</nav>

		<?php 
			switch ($app) {
				case 'SOT':
					include_once("app/views/pages/SOT/SOT_header.php");
					break;
				case 'EDL':
					include_once("app/views/pages/EDL/EDL_header.php");
					break;
				case 'MCT':
					include_once("app/views/pages/MCT/MCT_header.php");
					break;
				case 'traffic':
					include_once("app/views/pages/traffic/traffic_header.php");
					break;
				case 'cemetery':
					include_once("app/views/pages/cemetery/cemetery_header.php");
					break;
				case 'TPW':
					include_once("app/views/pages/TPW/TPW_header.php");
					break;
				default:
					break;
			}
		 ?>

		<!-- end of alert pop up -->
		<div class="wrap">
            <div class="container clear-top" style="padding-bottom: 20px;">

