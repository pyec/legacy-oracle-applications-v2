<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Summary Offence Ticket Model

Description : This is the data layer for the SOT system.

  Functions : __construct
				get_next_val
				get_SOTs
				get_SOT_by_id
				insert_SOT
				update_SOT
				remove_SOT
				get_bylaws
				get_bylaw_by_id
				get_bylaw_by_section
				get_bylaw_by_desc
				get_bylaw_for_update
				check_if_bylaw_exsists
				check_if_description_exsists
				get_bylaw_codes
				get_section_codes
				insert_bylaw
				update_bylaw
				remove_bylaw
				get_contacts
				get_contact_by_id
				get_contacts_like_id
				get_contact_by_first_name
				get_contact_by_last_name
				get_contact_by_name
				get_civic_by_like_id
				get_civic_info
				get_contact_civic_info
				get_contact_address
				get_contact_phone
				insert_contact
				insert_contact_phone
				insert_contact_description
				insert_contact_address
				update_contact
				update_contact_phone
				update_contact_civic
				remove_contact
				remove_contact_phone
				remove_contact_desc
				remove_contact_address
				get_employees
				get_employee_by_id
				check_employee_id
				get_employee_phone
				get_employee_by_first_name
				get_employee_by_last_name
				get_employee_by_name
				insert_employee
				insert_employee_phone
				update_employee
				update_employee_phone
				remove_employee
				remove_employee_phone
=========================================================================================================================*/
class SOT_model extends CI_Model 
{
	//Properies
	public $table_SOT = null;
	public $id_SOT = null;

	public $table_bylaw = null;
	public $name_bylaw = null;
	public $id_bylaw = null;

	public $table_contact = null;
	public $id_contact = null;
	public $first_name_contact = null;
	public $last_name_contact = null;

	public $table_employee = null;
	public $id_employee = null;
	public $first_name_employee = null;
	public $last_name_employee = null;

	public $oracle_db = null; // The reference to the database

	//Constructor
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->table_SOT = 'ACT_SOT';
		$this->id_SOT = 'SOT_NUM';

		$this->table_bylaw = 'ACT_SOT_OFFENCE';
		$this->name_bylaw = 'BYLAW_DSC';
		$this->id_bylaw = "BYLAW_CDE";

		$this->table_contact = 'ACT_CONTACT';
		$this->id_contact = 'CONTACT_NUM';
		$this->first_name_contact = 'FIRST_NME';
		$this->last_name_contact = 'LAST_NME';

		$this->table_employee = 'ACT_SOT_EMPLOYEE';
		$this->id_employee = 'EMPL_ID';
		$this->first_name_employee = 'FIRST_NME';
		$this->last_name_employee = 'LAST_NME';

		$this->oracle_db = $this->load->database('oracle_SOT', TRUE);

        $this->oracle_db->query("alter session set nls_date_format = 'DD-MON-YYYY'");
	}
	

	/*           
============================================================================
get_next_val
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
int - The next id in the table.
============================================================================
*/

	public function get_next_val( $table_name, $id )
	{
		// Query for the max value in the id field.
		$query = $this->oracle_db->query("SELECT MAX(".$id.") FROM ".$table_name);

		// In one expression return the result sets value, cast it to an integer 
		// and store it in the variable $id.
		$id = (int)$query->result_array()[0]["MAX(".$id.")"];

		// Increment by 1 and return the id so we can add a new record to the table.
		return 1 + $id;
	}

/*-----------------------------------------------------------------------------------
	Summary Offence Tickets retrieval functions
------------------------------------------------------------------------------------*/
	public function get_SOTs( )
	{
		$this->oracle_db->select("SOT_NUM, ISSUE_DTE, ISSUE_ID, OFFENCELOC_DSC, OUTCOME_DSC");
		$query = $this->oracle_db->get($this->table_SOT);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_SOT_by_id( $id = null )
	{
	
		$this->oracle_db->where('SOT_NUM', $id);
		$query = $this->oracle_db->get('ACT_SOT');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_SOT_like_id( $id = null )
	{
	
		$this->oracle_db->like('SOT_NUM', $id);
		$query = $this->oracle_db->get('ACT_SOT');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_SOT_like_officer_name( $officer = null )
	{
		$this->oracle_db->like('ISSUE_ID', $officer);
		$query = $this->oracle_db->get('ACT_SOT');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_SOT_like_location_name( $location = null )
	{
		$this->oracle_db->like('OFFENCELOC_DSC', $location);
		$query = $this->oracle_db->get('ACT_SOT');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_streets_by_name( $name = null )
	{
		$this->oracle_db->like('STNAME', strtoupper($name));
        $query = $this->oracle_db->get('ADDRST2_VW');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_streets_by_id( $id = null )
	{
		$this->oracle_db->where('STR_CODE', $id);
        $query = $this->oracle_db->get('ADDRST2_VW');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

/*-----------------------------------------------------------------------------------
	Summary Offence Ticket insertion function
------------------------------------------------------------------------------------*/
	public function insert_SOT( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->set("SOT_NUM", $data['SOT_NUM']);
			$this->oracle_db->set("CASE_NUM", (int)$data['CASE_NUM']);
			$this->oracle_db->set("ISSUE_DTE", $data['ISSUE_DTE'], false);
			$this->oracle_db->set("ISSUE_ID", $data['ISSUE_ID']);
			$this->oracle_db->set("BYLAW_CDE", $data['BYLAW_CDE']);
			$this->oracle_db->set("SECTION_CDE", $data['SECTION_CDE']);
			$this->oracle_db->set("CONTACT_NUM", (int)$data['CONTACT_NUM']);
			$this->oracle_db->set("CIVIC_NUM", $data['CIVIC_NUM']);
			$this->oracle_db->set("APT_NUM", $data['APT_NUM']);
			$this->oracle_db->set("DISTRICT_NUM", $data['DISTRICT_NUM']);
			$this->oracle_db->set("OFFENCELOC_DSC", strtoupper($data['OFFENCELOC_DSC']));
			$this->oracle_db->set("ARRAIGN_DTE", $data['ARRAIGN_DTE'], false);
			$this->oracle_db->set("SYSTEM_NME", $data['SYSTEM_NME']);
			$this->oracle_db->set("COURT_DTE", $data['COURT_DTE'], false);
			$this->oracle_db->set("OUTCOME_DSC", $data['OUTCOME_DSC']);
			$this->oracle_db->set("FINE_AMT", $data['FINE_AMT']);
			$this->oracle_db->set("NOTES_DSC", strtoupper($data['NOTES_DSC']));
			$this->oracle_db->set("ADD_BY", $data['ADD_BY']);
			$this->oracle_db->set("ADD_DTE", $data['ADD_DTE'], false);

			// $sql = "INSERT INTO SET ACT_SOT ('SOT_NUM', 'CASE_NUM', 'ISSUE_DTE', 'ISSUE_ID', 'BYLAW_CDE', 'SECTION_CDE', 'CONTACT_NUM', 'CIVIC_NUM', 'APT_NUM', 'DISTRICT_NUM', 'OFFENCELOC_DSC', 'ARRAIGN_DTE', 'COURT_DTE', 'OUTCOME_DSC', 'FINE_AMT', 'NOTES_DSC', 'ADD_BY', ADD_DTE) VALUES (50111208, 123123, '14-03-01', 'HAMERD', 'A-300', '8(1)', 703050, '268', '1', '12', 'TEST', '14-03-05', '14-03-26', 'GUILTY', 123, 'TEST', 'morinb', to_date('2014-03-10 10:19:11','YYYY-mm-dd HH24:MI:SS'))";
			$results = $this->oracle_db->insert('ACT_SOT');
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Summary Offence Ticket update function
------------------------------------------------------------------------------------*/
	public function update_SOT( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{	
			
			$this->oracle_db->set("SOT_NUM", $data['SOT_NUM']);
			$this->oracle_db->set("ADD_BY", $data['ADD_BY']);
			$this->oracle_db->set("ADD_DTE", $data['ADD_DTE']);
			$this->oracle_db->set("MOD_BY", $data['MOD_BY']);
			$this->oracle_db->set("MOD_DTE", $data['MOD_DTE'], false);
			$this->oracle_db->set("ISSUE_ID", $data['ISSUE_ID']);
			$this->oracle_db->set("ISSUE_DTE", $data['ISSUE_DTE']);
			$this->oracle_db->set("CASE_NUM", $data['CASE_NUM']);
			$this->oracle_db->set("CONTACT_NUM", $data['CONTACT_NUM']);
			$this->oracle_db->set("COURT_DTE", $data['COURT_DTE']);
			$this->oracle_db->set("OUTCOME_DSC", $data['OUTCOME_DSC']);
			$this->oracle_db->set("ARRAIGN_DTE", $data['ARRAIGN_DTE']);
			$this->oracle_db->set("FINE_AMT", $data['FINE_AMT']);
			$this->oracle_db->set("OFFENCELOC_DSC", strtoupper($data['OFFENCELOC_DSC']));
			$this->oracle_db->set("BYLAW_CDE", $data['BYLAW_CDE']);
			$this->oracle_db->set("SECTION_CDE", $data['SECTION_CDE']);
			$this->oracle_db->set("TYPE_CDE", $data['TYPE_CDE']);
			$this->oracle_db->set("REF_NUM", $data['REF_NUM']);
			$this->oracle_db->set("SYSTEM_NME", $data['SYSTEM_NME']);
			$this->oracle_db->set("BUSUNIT_CDE", $data['BUSUNIT_CDE']);
			$this->oracle_db->set("CIV_ID", $data['CIV_ID']);
			$this->oracle_db->set("APT_NUM", $data['APT_NUM']);
			$this->oracle_db->set("DISTRICT_NUM", $data['DISTRICT_NUM']);
			$this->oracle_db->set("CIVIC_NUM", $data['CIVIC_NUM']);
			$this->oracle_db->set("CIVIC_EXT", $data['CIVIC_EXT']);

			$this->oracle_db->set("NOTES_DSC", strtoupper($data['NOTES_DSC']));
			

			$this->oracle_db->where($this->id_SOT, $data[$this->id_SOT]);
			$this->oracle_db->update($this->table_SOT);
		}
	}

/*-----------------------------------------------------------------------------------
	Summary Offence Ticket removal function
------------------------------------------------------------------------------------*/
	public function remove_SOT( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_SOT, $id);
			$results = $this->oracle_db->delete($this->table_SOT);

			return $results;
		}
	} 

/*-----------------------------------------------------------------------------------
	Bylaw retrieval functions
------------------------------------------------------------------------------------*/
	public function get_bylaws( $params = false )
	{	
		if (!$params)
		{
			$this->oracle_db->distinct();
			$this->oracle_db->select('BYLAW_CDE');
			$query = $this->oracle_db->get($this->table_bylaw);
			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}	
		}
		else
		{
			$this->oracle_db->like($this->id_bylaw, strtoupper($params['bylaw_code']));
			$this->oracle_db->like($this->name_bylaw, strtoupper($params['description']));
			$query = $this->oracle_db->get($this->table_bylaw);
			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}	
		}
	}

	public function get_bylaw_by_id( $id = null )
	{
		$this->oracle_db->like($this->id_bylaw, strtoupper($id));
		$query = $this->oracle_db->get($this->table_bylaw);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_bylaw_by_section( $section )
	{
		$this->oracle_db->like("SECTION_CDE", $section);
		$query = $this->oracle_db->get($this->table_bylaw);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_bylaw_description_by_codes( $bylaw_code = null, $section_code = null )
	{
		$this->oracle_db->where('BYLAW_CDE', $bylaw_code);
		$this->oracle_db->where('SECTION_CDE', $section_code);

		$query = $this->oracle_db->get($this->table_bylaw);
		if ($query->num_rows() > 0)
		{ 
			return $query->row_array()['BYLAW_DSC'];
		}
		else 
		{
			return false;
		}
	}

	public function get_bylaw_by_desc( $desc = null )
	{
		$this->oracle_db->like($this->name_bylaw, strtoupper($desc));
		$query = $this->oracle_db->get($this->table_bylaw);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_bylaw_for_update( $data )
	{
		$this->oracle_db->where('BYLAW_CDE', $data['BYLAW_CDE']);
		$this->oracle_db->where('SECTION_CDE', $data['SECTION_CDE']);

		$query = $this->oracle_db->get($this->table_bylaw);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
	}

	public function check_if_bylaw_exsists( $data )
	{
		$this->oracle_db->where('BYLAW_CDE', $data['BYLAW_CDE']);
		$this->oracle_db->where('SECTION_CDE', $data['SECTION_CDE']);

		if (!empty($data['BYLAW_DSC']))
		{
			$this->oracle_db->where('BYLAW_DSC', $data['BYLAW_DSC']);
		}

		$query = $this->oracle_db->get($this->table_bylaw);

		if ($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function check_if_description_exsists( $description = null )
	{
		$this->oracle_db->where('BYLAW_DSC', $description);
		$query = $this->oracle_db->get($this->table_bylaw);

		if ($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function search_bylaws( $data = null )
	{
		if (!empty($data['section_code'])){ $this->oracle_db->like('SECTION_CDE', strtoupper($data['section_code']));}
		if (!empty($data['bylaw_code'])){ $this->oracle_db->like('BYLAW_CDE', strtoupper($data['bylaw_code']));}
		if (!empty($data['description'])){ $this->oracle_db->like('BYLAW_DSC', strtoupper($data['description']));}

		$query = $this->oracle_db->get($this->table_bylaw);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function get_bylaw_codes( $code )
	{
		$sql = "SELECT UNIQUE(BYLAW_CDE) FROM ACT_SOT_OFFENCE WHERE BYLAW_CDE LIKE '%".strtoupper($code)."%'";
		$query = $this->oracle_db->query($sql);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	public function get_section_codes( $code )
	{
		$sql = "SELECT SECTION_CDE FROM ACT_SOT_OFFENCE WHERE BYLAW_CDE = '".strtoupper($code)."'";
		$query = $this->oracle_db->query($sql);

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

/*-----------------------------------------------------------------------------------
	Bylaw insertion function
------------------------------------------------------------------------------------*/
	public function insert_bylaw( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!$this->check_if_bylaw_exsists($data) && !$this->check_if_description_exsists($data['BYLAW_DSC']))
			{				
				$result = $this->oracle_db->insert($this->table_bylaw, $data);
				return $result;
			}
			else
			{
				$this->session->set_flashdata('error', 'The bylaw already exisit');
				redirect('SOT/offence_bylaws');
			}
		}
	}

/*-----------------------------------------------------------------------------------
	Bylaw update function
------------------------------------------------------------------------------------*/
	public function update_bylaw( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!empty($data))
			{
				if ($data['BYLAW_DSC'] == "")
				{
					$data['BYLAW_DSC'] = null;
				}
				elseif ($data['TYPE_CDE'] == "")
				{
					$data['TYPE_CDE'] = null;
				}
				
				$this->oracle_db->set("BYLAW_CDE", $data['BYLAW_CDE']);
				$this->oracle_db->set("SECTION_CDE", $data['SECTION_CDE']);
				$this->oracle_db->set("BYLAW_DSC", $data['BYLAW_DSC']);
				$this->oracle_db->set("TYPE_CDE", $data['TYPE_CDE']);
		
				$this->oracle_db->where('BYLAW_CDE', $data['BYLAW_CDE']);
				$this->oracle_db->where('SECTION_CDE', $data['SECTION_CDE']);
				$results = $this->oracle_db->update($this->table_bylaw);	

				return $results;
			}
			else
			{
				redirect('SOT/offence_bylaws');
			}
		}
	}
/*-----------------------------------------------------------------------------------
	Bylaw removal function
------------------------------------------------------------------------------------*/
	public function remove_bylaw( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('BYLAW_CDE', $data['BYLAW_CDE']);
			$this->oracle_db->where('SECTION_CDE', $data['SECTION_CDE']);
			$result = $this->oracle_db->delete($this->table_bylaw);

			return $result;
		}
	}

/*-----------------------------------------------------------------------------------
	contact retrieval functions
------------------------------------------------------------------------------------*/
	public function get_contacts()
	{
		$query = $this->oracle_db->get($this->table_contact);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_contact_by_id( $id = null )
	{	
		$this->oracle_db->where($this->id_contact, $id);
		$query = $this->oracle_db->get($this->table_contact);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_contacts_like_id( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->id_contact, $id);
			$query = $this->oracle_db->get($this->table_contact);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
		}
	}

	public function get_contact_by_first_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->first_name_contact, strtoupper($name));
			$query = $this->oracle_db->get($this->table_contact);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function get_contact_by_last_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->last_name_contact, strtoupper($name));
			$query = $this->oracle_db->get($this->table_contact);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function get_contact_by_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->first_name_contact, strtoupper($name['first_name']));
			$this->oracle_db->like($this->last_name_contact, strtoupper($name['last_name']));
			$query = $this->oracle_db->get($this->table_contact);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function get_civic_by_like_id( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like("CIVIC_NUM", $id);
			$query = $this->oracle_db->get('ACT_ADDRESS');

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function get_civic_info( $contact_num, $street_num, $district )
	{
		$this->oracle_db->select("*");
		$this->oracle_db->where("CONTACT_NUM", $contact_num);
		$this->oracle_db->where("STR_CODE", $street_num);
		$this->oracle_db->where("DISTRICT_NUM", $district);
		$query = $this->oracle_db->get("ACT_ADDRESS");

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
	}

	public function get_contact_civic_info( $contact_num )
	{
		$this->oracle_db->where("CONTACT_NUM", $contact_num);
		$query = $this->oracle_db->get("ACT_ADDRESS");

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
	}

	public function get_contact_address( $contact_num )
	{
		$this->oracle_db->where("CONTACT_NUM", $contact_num);
		$query = $this->oracle_db->get("ACT_CONTACT_DSC");

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
	}

	public function get_contact_phone( $contact_num )
	{
		$this->oracle_db->where("CONTACT_NUM", $contact_num);
		$query = $this->oracle_db->get('ACT_CONTPHONE');

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
	}

/*-----------------------------------------------------------------------------------
	contact insertion function
------------------------------------------------------------------------------------*/
	public function insert_contact( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->set("FIRST_NME", $data['FIRST_NME']);
			$this->oracle_db->set("MIDDLE_NME", $data['MIDDLE_NME']);
			$this->oracle_db->set("LAST_NME", $data['LAST_NME']);
			$this->oracle_db->set("NOTES_DSC", $data['NOTES_DSC']);
			$this->oracle_db->set("CONTACT_NUM", $data['CONTACT_NUM']);
			$this->oracle_db->set("ADD_BY", $data['ADD_BY']);
			$this->oracle_db->set("ADD_DTE", $data['ADD_DTE'], false);
			$this->oracle_db->set("BIRTH_DTE", $data['BIRTH_DTE']);


			$results = $this->oracle_db->insert('ACT_CONTACT');

			return $results;
		}
	}

	public function insert_contact_phone( $data )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->set("AREA_CDE", $data['AREA_CDE']);
			$this->oracle_db->set("PHONE_NUM", $data['PHONE_NUM']);
			$this->oracle_db->set("EXT_CDE", $data['EXT_CDE']);
			$this->oracle_db->set("PHONE_FLG", $data['PHONE_FLG']);
			$this->oracle_db->set("FROM_DTE", $data['FROM_DTE']);
			$this->oracle_db->set("TO_DTE", $data['TO_DTE']);
			$this->oracle_db->set("CONTACT_NUM", $data['CONTACT_NUM']);
			$this->oracle_db->set("PHONEREC_NUM", $data['PHONEREC_NUM']);
			$this->oracle_db->set("ADD_BY", $data['ADD_BY']);
			$this->oracle_db->set("ADD_DTE", $data['ADD_DTE'], false);

			$results = $this->oracle_db->insert('ACT_CONTPHONE');

			return $results;
		}
	}

	public function insert_contact_description( $data )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->set("CONTACT_NUM", $data['CONTACT_NUM']);
			$this->oracle_db->set("NAME", $data['NAME']);
			$this->oracle_db->set("CIVIC_ADDR", $data['CIVIC_ADDR']);

			$results = $this->oracle_db->insert('ACT_CONTACT_DSC');

			return $results;
		}
	}

	public function insert_contact_address( $data )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->set("CIVIC_NUM", $data['CIVIC_NUM']);
			$this->oracle_db->set("APT_NUM", $data['APT_NUM']);
			$this->oracle_db->set("DISTRICT_NUM", $data['DISTRICT_NUM']);
			$this->oracle_db->set("ADDRESS_TYP", $data['ADDRESS_TYP']);
			$this->oracle_db->set("MAILCITY_DSC", $data['MAILCITY_DSC']);
			$this->oracle_db->set("COUNTRY_DSC", $data['COUNTRY_DSC']);
			$this->oracle_db->set("CONTACT_NUM", $data['CONTACT_NUM']);
			$this->oracle_db->set("POSTAL_CDE", $data['POSTAL_CDE']);
			$this->oracle_db->set("PROV_DSC", $data['PROV_DSC']);
			$this->oracle_db->set("ADDRESS_NOTES", $data['ADDRESS_NOTES']);
			$this->oracle_db->set("ADDRESS_NUM", $data['ADDRESS_NUM']);
			$this->oracle_db->set("ADD_BY", $data['ADD_BY']);
			$this->oracle_db->set("ADD_DTE", $data['ADD_DTE'], false);
            $this->oracle_db->set("FROM_DTE", $data['FROM_DTE']);
            $this->oracle_db->set("TO_DTE", $data['TO_DTE']);

			$results = $this->oracle_db->insert('ACT_ADDRESS');

			return $results;
		}
	}
/*-----------------------------------------------------------------------------------
	contact update function
------------------------------------------------------------------------------------*/
	public function update_contact( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!empty($data['PRIMARY_FLG']))
			{
				if ($data['PRIMARY_FLG'] == "on") {$data['PRIMARY_FLG'] = "Y";}
				else {$data['PRIMARY_FLG'] = "N";}				
			}

			$this->oracle_db->set("BIRTH_DTE", $data['BIRTH_DTE']);
			$this->oracle_db->set("TITLE_DSC", $data['TITLE_DSC']);
			$this->oracle_db->set("FIRST_NME", $data['FIRST_NME']);
			$this->oracle_db->set("MIDDLE_NME", $data['MIDDLE_NME']);
			$this->oracle_db->set("LAST_NME", $data['LAST_NME']);
			$this->oracle_db->set("NOTES_DSC", $data['NOTES_DSC']);
			$this->oracle_db->set("MOD_BY", $data['MOD_BY']);
			$this->oracle_db->set("MOD_DTE", $data['MOD_DTE'], false);
		
			$this->oracle_db->where($this->id_contact, $data[$this->id_contact]);
			$results = $this->oracle_db->update($this->table_contact);

			return $results;
		}
	}

	public function update_contact_phone( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{

			if (!empty($data['PRIMARY_FLG']))
			{
				if ($data['PRIMARY_FLG'] == "on") {$data['PRIMARY_FLG'] = "Y";}
				$this->oracle_db->set("PRIMARY_FLG", $data['PRIMARY_FLG']);

			}
			else
			{
				$data['PRIMARY_FLG'] == "N";
				$this->oracle_db->set("PRIMARY_FLG", $data['PRIMARY_FLG']);

			}

			$this->oracle_db->set("AREA_CDE", $data['AREA_CDE']);
			$this->oracle_db->set("PHONE_NUM", $data['PHONE_NUM']);
			$this->oracle_db->set("EXT_CDE", $data['EXT_CDE']);
			$this->oracle_db->set("PHONE_FLG", $data['PHONE_FLG']);
			$this->oracle_db->set("FROM_DTE", $data['FROM_DTE']);
			$this->oracle_db->set("TO_DTE", $data['TO_DTE']);
			$this->oracle_db->set("MOD_BY", $data['MOD_BY']);
			$this->oracle_db->set("MOD_DTE", $data['MOD_DTE'], false);
		
			$this->oracle_db->where("PHONEREC_NUM", $data['PHONEREC_NUM']);
			$results = $this->oracle_db->update('ACT_CONTPHONE');

			return $results;
		}
	}

	public function update_contact_civic( $data )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->set("CIVIC_NUM", $data['CIVIC_NUM']);
			$this->oracle_db->set("APT_NUM", $data['APT_NUM']);
			$this->oracle_db->set("DISTRICT_NUM", $data['DISTRICT_NUM']);
			$this->oracle_db->set("ADDRESS_TYP", $data['ADDRESS_TYP']);
			$this->oracle_db->set("MAILADDR1_DSC", $data['MAILADDR1_DSC']);
			$this->oracle_db->set("COUNTRY_DSC", $data['COUNTRY_DSC']);
			$this->oracle_db->set("POSTAL_CDE", $data['POSTAL_CDE']);
			$this->oracle_db->set("PROV_DSC", $data['PROV_DSC']);
			$this->oracle_db->set("FROM_DTE", $data['FROM_DTE']);
			$this->oracle_db->set("TO_DTE", $data['TO_DTE']);
			$this->oracle_db->set("MOD_BY", $data['MOD_BY']);
			$this->oracle_db->set("ADDRESS_NOTES", $data['ADDRESS_NOTES']);
			$this->oracle_db->set("MOD_DTE", $data['MOD_DTE'], false);
		
			$this->oracle_db->where($this->id_contact, $data[$this->id_contact]);
			$results = $this->oracle_db->update('ACT_ADDRESS');

			return $results;
		}
	}

	public function update_contact_description( $data )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->set("CIVIC_ADDR", $data['CIVIC_ADDR']);

			$results = $this->oracle_db->update('ACT_CONTACT_DSC');

			return $results;
		}
	}
/*-----------------------------------------------------------------------------------
	contact removal function
------------------------------------------------------------------------------------*/
	public function remove_contact( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('CONTACT_NUM', $id);
			$result = $this->oracle_db->delete('ACT_CONTACT');
			return $result;
		}
	}

	public function remove_contact_phone( $PHONEREC_NUM = null, $CONTACT_NUM = null )
	{
		if ($PHONEREC_NUM == null && $CONTACT_NUM == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('PHONEREC_NUM', $PHONEREC_NUM);
			$this->oracle_db->where('CONTACT_NUM', $CONTACT_NUM);
			$this->oracle_db->delete('ACT_CONTPHONE');
		}
	}

	public function remove_contact_desc( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('CONTACT_NUM', $id);
			$this->oracle_db->delete('ACT_CONTACT_DSC');
		}
	}

	public function remove_contact_address( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('CONTACT_NUM', $id);
			$this->oracle_db->delete('ACT_ADDRESS');
		}
	}

/*-----------------------------------------------------------------------------------
	employee retrieval functions
------------------------------------------------------------------------------------*/
	public function get_employees( )
	{
		$query = $this->oracle_db->get($this->table_employee);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_employee_by_id( $id = null )
	{
		$this->oracle_db->like($this->id_employee, strtoupper($id));
		$query = $this->oracle_db->get($this->table_employee);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function check_employee_id( $id = null )
	{
		$this->oracle_db->where($this->id_employee, strtoupper($id));
		$query = $this->oracle_db->get($this->table_employee);

		if ($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function get_employee_phone( $employee_number )
	{
		$this->oracle_db->where("EMPL_ID", $employee_number);
		$query = $this->oracle_db->get('ACT_SOT_EMPLPHONE');
		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
	}

	public function get_employee_by_first_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->first_name_employee, strtoupper($name));
			$query = $this->oracle_db->get($this->table_employee);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function get_employee_by_last_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->last_name_employee, strtoupper($name));
			$query = $this->oracle_db->get($this->table_employee);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function get_employee_by_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->first_name_employee, strtoupper($name['first_name']));
			$this->oracle_db->like($this->last_name_employee, strtoupper($name['last_name']));
			$query = $this->oracle_db->get($this->table_employee);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function search_employees( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!empty($data['FIRST_NME'])){$this->oracle_db->like('FIRST_NME', strtoupper($data['FIRST_NME']));}
			if (!empty($data['LAST_NME'])){$this->oracle_db->like('LAST_NME', strtoupper($data['LAST_NME']));}

			$query = $this->oracle_db->get($this->table_employee);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}
/*-----------------------------------------------------------------------------------
	employee insertion function
------------------------------------------------------------------------------------*/
	public function insert_employee( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->set("EMPL_ID", $data['EMPL_ID']);
			$this->oracle_db->set("FIRST_NME", $data['FIRST_NME']);
			$this->oracle_db->set("MIDDLE_NME", $data['MIDDLE_NME']);
			$this->oracle_db->set("LAST_NME", $data['LAST_NME']);
			$this->oracle_db->set("AGENCY_NME", $data['AGENCY_NME']);
			$this->oracle_db->set("EMAIL_DSC", $data['EMAIL_DSC']);
			$this->oracle_db->set("WORK_LOC", $data['WORK_LOC']);
			$this->oracle_db->set("INACTIVE_FLG", $data['INACTIVE_FLG']);
			$this->oracle_db->set("NOTES_DSC", $data['NOTES_DSC']);
			$this->oracle_db->set("ADD_BY", $data['ADD_BY']);
			$this->oracle_db->set("ADD_DTE", $data['ADD_DTE'], false);
			$results = $this->oracle_db->insert($this->table_employee);

			return $results;
		}
	}

	public function insert_employee_phone( $data )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!empty($data['PRIMARY_FLG']))
			{
				if ($data['PRIMARY_FLG'] == "on") {$data['PRIMARY_FLG'] = "Y";}
				else {$data['PRIMARY_FLG'] = "N";}
				
				$this->oracle_db->set("PRIMARY_FLG", $data['PRIMARY_FLG']);

			}
			
			
			$this->oracle_db->set("EMPL_ID", $data['EMPL_ID']);
			$this->oracle_db->set("PHONEREC_NUM", $data['PHONEREC_NUM']);
			$this->oracle_db->set("PHONE_FLG", $data['PHONE_FLG']);
			$this->oracle_db->set("PHONE_NUM", $data['PHONE_NUM']);
			$this->oracle_db->set("ADD_BY", $data['ADD_BY']);
			$this->oracle_db->set("ADD_DTE", $data['ADD_DTE'], false);

			$results = $this->oracle_db->insert('ACT_SOT_EMPLPHONE');

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	employee update function
------------------------------------------------------------------------------------*/
	public function update_employee( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if ($data['INACTIVE_FLG'] == "on") {$data['INACTIVE_FLG'] = "N";}
			else {$data['INACTIVE_FLG'] = "Y";}	

			$this->oracle_db->set("INACTIVE_FLG", $data['INACTIVE_FLG']);
			$this->oracle_db->set("EMPL_ID", $data['EMPL_ID']);
			$this->oracle_db->set("MOD_BY", $data['MOD_BY']);
			$this->oracle_db->set("MOD_DTE", $data['MOD_DTE'], false);
			$this->oracle_db->set("FIRST_NME", $data['FIRST_NME']);
			$this->oracle_db->set("MIDDLE_NME", $data['MIDDLE_NME']);
			$this->oracle_db->set("LAST_NME", $data['LAST_NME']);
			$this->oracle_db->set("AGENCY_NME", $data['AGENCY_NME']);
			$this->oracle_db->set("EMAIL_DSC", $data['EMAIL_DSC']);
			$this->oracle_db->set("WORK_LOC", $data['WORK_LOC']);
			$this->oracle_db->set("NOTES_DSC", $data['NOTES_DSC']);
			$this->oracle_db->set("INACTIVE_FLG", $data['INACTIVE_FLG']);
		
			$this->oracle_db->where($this->id_employee, $data[$this->id_employee]);
			$results = $this->oracle_db->update('ACT_SOT_EMPLOYEE');

			return $results;
		}
	}

	public function update_employee_phone( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!empty($data['PRIMARY_FLG']))
			{
				if ($data['PRIMARY_FLG'] == "on") {$data['PRIMARY_FLG'] = "Y";}
				else {$data['PRIMARY_FLG'] = "N";}	
				$this->oracle_db->set("PRIMARY_FLG", $data['PRIMARY_FLG']);
			}

			$this->oracle_db->set("PHONE_NUM", $data['PHONE_NUM']);
			$this->oracle_db->set("PHONE_FLG", $data['PHONE_FLG']);
			$this->oracle_db->set("MOD_BY", $data['MOD_BY']);
			$this->oracle_db->set("MOD_DTE", $data['MOD_DTE'], false);
		
			$this->oracle_db->where('EMPL_ID', $data['EMPL_ID']);
			$this->oracle_db->where('PHONEREC_NUM', $data['PHONEREC_NUM']);
			$results = $this->oracle_db->update('ACT_SOT_EMPLPHONE');

			return $results;
		}
	}
/*-----------------------------------------------------------------------------------
	employee removal function
------------------------------------------------------------------------------------*/
	public function remove_employee( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_employee, $id);
			$results = $this->oracle_db->delete($this->table_employee);

			return $results;
		}
	}

	public function remove_employee_phone( $phone_id = null )
	{
		if ($phone_id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('PHONEREC_NUM', $phone_id);
			$results = $this->oracle_db->delete('ACT_SOT_EMPLPHONE');

			return $results;
		}
	}
}

/* End of file SOT_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/SOT_model.php */