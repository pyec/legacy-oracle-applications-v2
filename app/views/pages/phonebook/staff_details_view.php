<div class="row">
	<div class="col-6">
		<h2>Contact Details</h2>
	</div>
</div>
	<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-sm-4">
						<label for="first_name">First Name</label>
						<input type="text" name="first_name" class="form-control" title="First Name" value="<?php if(isset($employee[0]['name'][1])){echo $employee[0]['name'][1];}  ?>" readonly />
					</div>
					<div class="col-sm-4">
						<label for="last_name">Last Name</label>
						<input type="text" name="last_name" class="form-control" title="Last Name" value="<?php if(isset($employee[0]['name'][0])){echo $employee[0]['name'][0];}  ?>" readonly />
					</div>
					<div class="col-sm-4">
						<label for="title">Title</label>
						<input type="text" name="title" class="form-control" title="Title" value="<?php if(isset($employee[0]['title'])){echo $employee[0]['title'];} ?>" readonly />
					</div>
				</div>
			</div>
		</div>	
	<div class="form-group">
			<div class="row">
				<div class="col-md-12">
					<div class="col-sm-4">
						<label for="email_address">Email Address</label>
						<input type="text" name="email_address" class="form-control" title="Email Address" value="<?php if(isset($employee[0]['mail'])){echo $employee[0]['mail'];} ?>" readonly />
					</div>
					<div class="col-sm-4">
						<label for="section">Section</label>
						<input type="text" name="section" class="form-control" title="Section" value="<?php if(isset($employee[0]['department'])){echo $employee[0]['department'];} ?>" readonly />
					</div>
					<div class="col-sm-4">
						<label for="location">Location</label>
						<input type="text" name="location" class="form-control" title="Location" value="<?php if(isset($employee[0]['physicaldeliveryofficename'])){echo $employee[0]['physicaldeliveryofficename'];} ?>" readonly />
					</div>
				</div>
			</div>
		</div>	

	<legend>Location</legend>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<div class="col-sm-4">
					<label for="civic_address">Civic Address</label>
					<input type="text" name="civic_address" class="form-control" title="Civic Address" value="<?php if(isset($employee[0]['streetaddress'])){echo $employee[0]['streetaddress'];} ?>" readonly />
				</div>
				<div class="col-sm-4">
					<label for="floor_suite">Floor/Suite</label>
					<input type="text" name="floor_suite" class="form-control" title="Floor/Suite" value="" readonly />
				</div>
			</div>
		</div>
	</div>

	<legend>Phone</legend>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<div class="col-sm-4">
					<label for="work_phone">Work Phone</label>
					<input type="text" name="work_phone" class="form-control" title="Work Phone" value="<?php if(isset($employee[0]['telephonenumber'])){echo $employee[0]['telephonenumber'];}  ?>" readonly />
				</div>
				<div class="col-sm-4">
					<label for="cell_phone">Cell Phone</label>
					<input type="text" name="cell_phone" class="form-control" title="Cell Phone" value="<?php if(isset($employee[0]['mobile'])){echo $employee[0]['mobile'];} ?>" readonly />
				</div>
				<?php if (!empty($employee[0]['fax'])): ?>
					<div class="col-sm-4">
					<label for="fax">Fax</label>
					<input type="text" name="fax" class="form-control" title="Fax" value="<?php if(isset($employee[0]['fax'])){echo $employee[0]['fax'];} ?>" readonly />
				</div>
				<?php endif ?>
			</div>
		</div>
	</div>

	<legend></legend>
	<div class="form-group">
		<div class="row">
			<div class="col-md-12">
				<div class="col-sm-4">
					<a href="<?php echo base_url(); ?>phonebook" class="btn btn-md btn-default return_phonebook">Back to phonebook</a>
				</div>
			</div>
		</div>
	</div>
</div>