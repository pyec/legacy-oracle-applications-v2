<?php 
// determine if the user is an admin 
	$is_admin = false;
	$read_only = true;
    $is_readwrite = false;

	$groups = $this->session->userdata('groups');

	foreach ($groups as $group) 
	{
		if ($group == 'AppHRMAppsEngineeringDrawingLocationReadOnly')
        //if ($group == 'AppHRMAppsEngineeringDrawingLocationReadOnly' || $group == 'AppHRMAppsAdmin')
		{
			$read_only = true;
		}

        if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite')
        //if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite' || $group == 'AppHRMAppsAdmin')
        {
            $is_readwrite = true;
            $read_only = false;
        }

		if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
        //if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin')
		{
			$is_admin = true;
			$is_readwrite = false;
			$read_only = false;
		}
	}
 ?>
		<nav class="navbar navbar-default navbar-fixed-top app_nav" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="nav_items">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle app_header" data-toggle="collapse" data-target=".navbar-ex2-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
				
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex2-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url(); ?>EDL">EDL Lookup</a></li>
							<?php if ($is_admin): ?>
								<li><a href="<?php echo base_url(); ?>EDL/table_maintenance">Table Maintenance</a></li>
							<?php endif ?>
							<!-- <li><a href="<?php echo base_url(); ?>EDL/plan_index_query">Plan Index Query</a></li> -->
						</ul>
					</div><!-- /.navbar-collapse -->
				</div>
			</div>
		</nav>


