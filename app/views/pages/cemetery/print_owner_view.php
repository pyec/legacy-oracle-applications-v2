<div class="row" style="margin-bottom: 2em">
    <div class="col-sm-6 col-xs-6">
        <img src="<?php echo base_url(); ?>/assets/img/logo.png" width="245" height="60" border="0" alt="HALIFAX" title="HALIFAX">
    </div>
	<div class="col-sm-6 col-xs-6">
		<h4 class="text-center">HRM Cemetery System</h4>
		<h5 class="text-center">Date : <?= date('d-m-Y h:i:s') ?></h5>
	</div>
</div>

<br>
<!-- Owner info -->
<div class="row">
	<div class="col-xs-4 col-sm-4">
		<p><strong>Cemetery :</strong>  <?= $cemetery[0]['CEM_NAME'] ?></p>
		<p><strong>Owner :</strong>  <?= $owner[0]['CEM_LNAME'].', '.$owner[0]['CEM_FNAME'].' '.$owner[0]['CEM_MNAME'] ?></p>
		<p><strong>Owner Info :</strong>  <?= $owner[0]['CEM_OWNERS'] ?></p>
		<p><strong>Lot No :</strong> 	 <?= $owner[0]['CEM_LOTNBR'] ?></p>
		<p><strong>Lot Size :</strong>  <?= $owner[0]['CEM_SIZE'] ?></p>
		<p><strong>Purchase Date :</strong>  <?= $owner[0]['CEM_PURDTE'] ?></p>
		<p><strong>Partial Purchase Date :</strong>  <?= $owner[0]['CEM_INCOMPURDTE'] ?></p>
	</div>
	<div class="col-xs-4 col-sm-4">
		<p><strong>No. Monuments :</strong>  <?= $owner[0]['CEM_NBRMON'] ?></p>
		<p><strong>Supplier :</strong>  <?= $owner[0]['CEM_MONVEND'] ?></p>
		<p><strong>Perpetual Care :</strong>  <?= $owner[0]['CEM_PRPCARE'] ?></p>
		<p><strong>Comments :</strong>  <?= $owner[0]['CEM_CMNT'] ?></p>
		<p><strong>Special Comments :</strong>   <?= $owner[0]['CEM_SPC_CMNT'] ?></p>
		<p><strong>No. Graves :</strong>   <?= $owner[0]['CEM_NBRGR'] ?></p>
		<p><strong>Year :</strong>   <?= $owner[0]['CEM_OWNYR'] ?></p>
	</div>
	<div class="col-xs-4 col-sm-4">
		<p><strong>Purchase Amount :</strong>   <?= $owner[0]['CEM_PURAMT'] ?></p>
		<p><strong>Monument Base Size :</strong>   <?= $owner[0]['CEM_MONSIZE'] ?></p>
		<p><strong>Location :</strong>   <?= $owner[0]['CEM_SCTN'] ?></p>
		<p><strong>Sheet :</strong>   <?= $owner[0]['CEM_SHEET'] ?></p>
		<p><strong>Charge No. :</strong>   <?= $owner[0]['CEM_CHGNBR'] ?></p>
		<p><strong>Marker For :</strong>   <?= $owner[0]['CEM_MARKER'] ?></p>
	</div>
</div>
<!-- interred Info -->
<!-- <hr> -->
<?php foreach ($interred as $value): ?>
	<hr>
	<div class="row" style="margin-bottom: 25px;">
		<div class="col-xs-4 col-sm-4">
			<p><strong>Cemetery :</strong> <?= $value['CEM_NAME'] ?></p>
			<p><strong>Interred :</strong> <?= $value['INT_LNAME'].', '.$value['INT_FNAME'].' '.$value['INT_MNAME'] ?></p>
			<p><strong>Date :</strong> <?= $value['INT_DTE'] ?></p>
			<p><strong>Age :</strong> <?= $value['INT_AGE'] ?></p>
		</div>
		<div class="col-xs-4 col-sm-4">
			<p><strong>Type :</strong> <?= $value['INT_TYP'] ?></p>
			<p><strong>Marker :</strong> <?= $value['INT_MARKER'] ?></p>
			<p><strong>Location :</strong> <?= $value['INT_LOC'] ?></p>
			<p><strong>Comments :</strong> <?= $value['INT_CMNT'] ?></p>
			<p><strong>Special Comments :</strong>  <?= $value['INT_SPC_CMNT'] ?></p>
		</div>
		<div class="col-xs-4 col-sm-4">
			<p><strong>Partial Interred Date :</strong>  <?= $value['INT_INCOMPDTE'] ?></p>
		</div>
	</div>
	<!-- <hr> -->
<?php endforeach ?>