<div class="row">
	<div class="col-md-12">
		<h2>Add Cemetery Name</h2>
		<p class="lead">This form will allow you to add a new cemetery name to the system.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>cemetery/add_cemetery_name" method="POST" >
			<div class="form-group">
				<div class="col-sm-6">
					<label for="CEM_NAME">Cemetery Name</label><span class="text-danger">*</span>
					<input type="text" name="CEM_NAME" class="form-control" />
				</div>
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_cemetery_name pull-right" value="Add Name"/>
					<a href="<?php echo base_url(); ?>cemetery/cemetery_names" class="btn btn-md btn-default return">Back to Cemetery Names</a>
				</div>
			</div>
		</form>
	</div>
</div>