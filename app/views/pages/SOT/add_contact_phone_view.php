<div class="row">
	<div class="col-md-6">
		<h2>Add Phone Record</h2>
		<p>Select a contact to view more information about them.</p>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>

<form action="<?php echo base_url(); ?>SOT/add_contact_phone" method="POST">
	<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-sm-1">
					<label for="AREA_CDE">Area</label>
					<input type="text" name="AREA_CDE" class="form-control" title="Area Code" value="<?php set_value('AREA_CDE') ?>" maxlength="3" />
				</div>
				<div class="col-sm-2">
					<label for="PHONE_NUM">Number</label>
					<input type="text" name="PHONE_NUM" class="form-control" title="Number" value="<?php set_value('PHONE_NUM') ?>" maxlength="7" />
				</div>
				<div class="col-sm-1">
					<label for="EXT_CDE">Ext</label>
					<input type="text" name="EXT_CDE" class="form-control" title="Extention" value="<?php set_value('EXT_CDE') ?>" maxlength="4" />
				</div>
				<div class="col-sm-1">
					<label for="PHONE_FLG">Type</label>
					<input type="text" name="PHONE_FLG" class="form-control" title="Type" value="<?php set_value('PHONE_FLG') ?>" maxlength="1" />
				</div>
				<div class="col-sm-1">
					<label for="PRIMARY_FLG">Primary?</label>
					<input type="checkbox" name="PRIMARY_FLG" title="Primary?" value="<?php set_value('PRIMARY_FLG') ?>" />
				</div>
				<div class="col-sm-3">
					<label for="FROM_DTE">From</label>
					<input type="text" name="FROM_DTE" class="form-control date_picker" title="From" value="<?php set_value('FROM_DTE') ?>" readonly />
				</div>
				<div class="col-sm-3">
					<label for="TO_DTE">To</label>
					<input type="text" name="TO_DTE" class="form-control date_picker" title="To" value="<?php set_value('TO_DTE') ?>" readonly />
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12 no_label_fix">
			<input type="hidden" name="CONTACT_NUM"   value="<?= $contact_id ?>" />
			<input type="submit" class="btn btn-md btn-primary pull-right" value="Add phone" />
			<a href="<?php echo base_url(); ?>SOT/update_contact/<?= $contact_id ?>" class="btn btn-md btn-default">Back to Contact</a>
		</div>
	</div>
</form>