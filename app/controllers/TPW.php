<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : PWT Employee Training Tracking 

Description :  This system allows users to add new courses to the PWT training 
			schedule and then tie emoloyees to the courses.

  Functions : index(), add_course(), delete_course(), update_course()

=========================================================================================================================*/
class TPW extends CI_Controller 
{

	public function index()
	{
		// $data['content'] = "pages/TPW/home_view";
		$data['content'] = "pages/TPW/employee_courses_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
employee_course_update
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function employee_courses()
	{
		// Load Model
		$this->load->model('TPW_model');
		// Get a list of the employees to select from for an update.
		$data['content'] = "pages/TPW/employee_courses_view";
		$this->load->view('master', $data);
	}

	public function view_employee()
	{
		// Load Model
		$this->load->model('TPW_model');

		$id = $this->uri->segment(3);

		if (!empty($id))
		{
			$data['employee'] = $this->TPW_model->get_employee_by_id($id);
			$data['courses'] = $this->TPW_model->get_employee_courses($id);

			// loop through the courses and query for their names
			if (!empty($data['courses']))
			{
				for ($i=0; $i < count($data['courses']); $i++) 
				{ 
					$data['courses'][$i]['COURSE_NME'] = $this->TPW_model->get_course_by_id($data['courses'][$i]['COURSE_NUM'])['COURSE_NME'];
				}
			}
		
			$data['content'] = 'pages/TPW/update_employee_courses_view';
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
employee_course_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function employee_course_search()
	{
		// Load model
		$this->load->model('TPW_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);
		if (!empty($search_array))
		{
			// Validation
			$this->form_validation->set_rules('EMPL_ID', 'Employee Number', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('FIRST_NME', 'First Name', 'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('POSITION_NME', 'Position', 'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('LAST_NME', 'Last Name', 'trim|max_length[50]|xss_clean');

			if ($this->form_validation->run() == true)
			{
				$data['employees'] = $this->TPW_model->search_employees($search_array);
			}
			else
			{
				redirect('TPW/employee_courses');
			}
		}
		else
		{
			redirect('TPW/employee_courses');
		}
		if (!empty($data['employees'])){$data['employees'] = array_slice($data['employees'], 0, 500);}

		$data['content'] = "pages/TPW/employee_courses_view";
		$this->load->view('master', $data);
	}

	public function ajax_courses()
	{
		// Load model
		$this->load->model('TPW_model');

		$name = $this->input->post('name');

		$results = $this->TPW_model->get_courses_by_name($name);

		echo json_encode($results);
	}

	public function ajax_course_by_id()
	{
		// Load model
		$this->load->model('TPW_model');

		$id = $this->input->post('id');

		$results = $this->TPW_model->get_course_by_id($id);

		echo json_encode($results);
	}

	public function ajax_employees()
	{
		// Load model
		$this->load->model('TPW_model');

		$data = $this->input->post(null);

		$results = $this->TPW_model->search_employees($data);

		echo json_encode($results);
	}

	public function ajax_employee_by_id()
	{
		// Load model
		$this->load->model('TPW_model');

		$id = $this->input->post('id');

		$results = $this->TPW_model->get_employee_by_id($id);

		echo json_encode($results);
	}


	public function add_employee_course()
	{
		// Load model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['EMPL_ID'] = $this->uri->segment(3);
			$data['content'] = 'pages/TPW/add_employee_course_view';
			$this->load->view('master', $data);
		}
		else
		{
			$post['ADDBY'] = $this->session->userdata('samaccountname');
			$post['ADDDTTM'] = date('y-m-d');

			$this->form_validation->set_rules('COURSE_DTE', 'Course Date', 'required');
			$this->form_validation->set_rules('COURSE_COMMENTS', 'Comments', 'max_length[1000]');

			if ($this->form_validation->run())
			{
				$post['RECERT_FLG'] = (!empty($post['RECERT_FLG']) && $post['RECERT_FLG'] == 'on' ? 'Y':'N');
				$post['PASS_FLG'] = (!empty($post['PASS_FLG']) &&$post['PASS_FLG'] == 'on' ? 'Y':'N');
				unset($post['RECERT_PERIOD']);
				if ($this->TPW_model->insert_employee_course($post))
				{
					$this->session->set_flashdata('message', 'The employees course was successfully added.');
					redirect('TPW/view_employee/'.$post['EMPL_ID']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error, plase try again.');	
					redirect('TPW/view_employee/'.$post['EMPL_ID']);	
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('TPW/view_employee/'.$post['EMPL_ID']);
				
			}
		}
	}

	public function update_employee_course()
	{
		// Load model
		$this->load->model('TPW_model');

		$EMPL_ID = $this->uri->segment(3);
		$COURSE_NUM = $this->uri->segment(4);
		$post = $this->input->post(null, true);

		if (empty($post))
		{
			$data['course'] = $this->TPW_model->get_employee_course($EMPL_ID, $COURSE_NUM);
			$data['content'] = 'pages/TPW/update_employee_course_view';
			$this->load->view('master', $data);
		}
		else
		{
			$post['MODBY'] = $this->session->userdata('samaccountname');
			$post['MODDTTM'] = date('y-m-d');
			$post['RECERT_FLG'] = (!empty($post['RECERT_FLG']) && $post['RECERT_FLG'] == 'on' ? 'Y':'N');
			$post['PASS_FLG'] = (!empty($post['PASS_FLG']) &&$post['PASS_FLG'] == 'on' ? 'Y':'N');

			$this->form_validation->set_rules('COURSE_DTE', 'Course Date', 'required');
			$this->form_validation->set_rules('COURSE_COMMENTS', 'Comments', 'max_length[1000]');

			if ($this->form_validation->run())
			{
				if ($this->TPW_model->update_employee_course($post))
				{
						$this->session->set_flashdata('message', 'The employees course was successfully updated.');
						redirect('TPW/update_employee_course/'.$post['EMPL_ID'].'/'.$post['COURSE_NUM']);
				}
				else
				{
						$this->session->set_flashdata('error', 'There was an error, plase try again.');	
						redirect('TPW/update_employee_course/'.$post['EMPL_ID'].'/'.$post['COURSE_NUM']);	
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('TPW/update_employee_course/'.$post['EMPL_ID'].'/'.$post['COURSE_NUM']);
			}
		}
	}

	public function delete_employee_course()
	{
		//Load Model
		$this->load->model('TPW_model');

		$id = $this->input->post('EMPL_ID');
		$course_id = $this->input->post('COURSE_NUM');

		if ($this->TPW_model->remove_employee_course($id, $course_id))
		{

			// Redirect with a success message.
			$this->session->set_flashdata('message', 'course: '.$id.' has been removed from employee.');
			redirect('TPW/view_employee/'.$id);
		}
		else
		{
			$this->session->set_flashdata('error', 'course: '.$id.' could not be deleted.');
			redirect('TPW/view_employee/'.$id);
		}
	}


/*           
============================================================================
course_list_update
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function course_list()
	{
		// Load Model
		$this->load->model('TPW_model');
		// Get a list of the courses to select from for an update.
		$data['content'] = "pages/TPW/course_list_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
course_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function search_courses()
	{
		// Load model
		$this->load->model('TPW_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		if (!empty($search_array))
		{
			// Validation
			$this->form_validation->set_rules('course_number', 'Course Number', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('course_name', 'Course Name', 'trim|max_length[50]|xss_clean');

			if ($this->form_validation->run() == true)
			{
				$data['courses'] = $this->TPW_model->search_courses($search_array);
				if (!empty($data['courses'])){$data['courses'] = array_slice($data['courses'], 0, 500);}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('TPW/course_list');
			}
			
		}

		$data['content'] = "pages/TPW/course_list_view";
		$this->load->view('master', $data);
	}

	public function add_course()
	{
		// Load Model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			$this->form_validation->set_rules('COURSE_NUM', 'Course Number', 'required|max_length[10]');
			$this->form_validation->set_rules('COURSE_NME', 'Course Name', 'required|max_length[50]');
			$this->form_validation->set_rules('RECERT_PERIOD', 'Recert Period', 'max_length[2]');

			if ($this->form_validation->run() == true)
			{
				$post['ADDBY'] = $this->session->userdata('samaccountname');
				$post['ADDDTTM'] = date('y-m-d');
				$post['RECERT_FLG'] = (!empty($post['RECERT_FLG']) && $post['RECERT_FLG'] == 'on' ? 'Y':'N');
				$post['COURSE_NME'] = strtoupper($post['COURSE_NME']);

				if (!$this->TPW_model->does_course_exist($post['COURSE_NUM']))
				{
					if ($this->TPW_model->insert_course($post))
					{
						$this->session->set_flashdata('message', 'Course: '.$post['COURSE_NME'].' has been added.');
						redirect('TPW/course_list');
					}
					else
					{
						$this->session->set_flashdata('error', 'The course record could not be inserted, please try again.');
						redirect('TPW/add_course');
					}				
				}
				else
				{
					$this->session->set_flashdata('error', 'This course number already exsits.');
					redirect('TPW/add_course');
				}	
			}
			else
			{
				$data['content'] = 'pages/TPW/add_course_view';
				$this->load->view('master', $data, FALSE);
			}
		}
		else
		{
			$data['content'] = 'pages/TPW/add_course_view';
			$this->load->view('master', $data, FALSE);
		}
	}

	public function update_course()
	{

		// Load Model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);
		$id = $this->uri->segment(3);


		if (!empty($post))
		{
			// Validate an dinsert data
			$this->form_validation->set_rules('COURSE_NUM', 'Course Number', 'required|max_length[10]');
			$this->form_validation->set_rules('COURSE_NME', 'Course Name', 'required|max_length[50]');
			$this->form_validation->set_rules('RECERT_PERIOD', 'Recert Period', 'max_length[2]');

			if ($this->form_validation->run() == true)
			{
				// Add the rest of the required fields.
				$post['MODBY'] = $this->session->userdata('samaccountname');
				$post['MODDTTM'] = date('y-m-d');

				// Insert the record
				if ($this->TPW_model->update_course($post))
				{
					$this->session->set_flashdata('message', 'Course code: '.$post['COURSE_NUM'].' has been updated.');
					redirect('TPW/update_course/'.$post['COURSE_NUM']);
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('TPW/update_course/'.$post['COURSE_NUM']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('TPW/update_course/'.$post['COURSE_NUM']);
			}
		}
		else
		{

			$data['course'] = $this->TPW_model->get_course_by_id($id);
			$data['content'] = 'pages/TPW/update_course_view';
			$this->load->view('master', $data, FALSE);
		}
	}

	public function delete_course()
	{
		//Load Model
		$this->load->model('TPW_model');

		$id = $this->input->post('COURSE_NUM');

		if ($this->TPW_model->remove_course($id))
		{

			// Redirect with a success message.
			$this->session->set_flashdata('message', 'course: '.$id.' has been deleted.');
			redirect('TPW/course_list');
		}
		else
		{
			$this->session->set_flashdata('error', 'course: '.$id.' could not be deleted.');
			redirect('TPW/course_list');
		}
	}

/*           
============================================================================
employee_update
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function employees()
	{
		// Load Model
		$this->load->model('TPW_model');
		// Get a list of the employees to select from for an update.
		$data['content'] = "pages/TPW/employees_view";
		$this->load->view('master', $data);
	}

	public function employee_search()
	{
		// Load model
		$this->load->model('TPW_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('EMPL_ID', 'Employee Number', 'trim|max_length[10]|xss_clean');
		$this->form_validation->set_rules('FIRST_NME', 'First Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('LAST_NME', 'Last Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('POSITION_NME', 'Position', 'trim|max_length[50]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			$data['employees'] = $this->TPW_model->search_employees($search_array);
		}
		else
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('TPW/employees');
		}
		
		if (!empty($data['employees'])){$data['employees'] = array_slice($data['employees'], 0, 500);}
		$data['content'] = "pages/TPW/employees_view";

		$this->load->view('master', $data);
	}

	public function add_employee()
	{
		// Load Model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			$this->form_validation->set_rules('EMPL_ID', 'Employee ID', 'required|max_length[10]');
			$this->form_validation->set_rules('LAST_NME', 'Last Name', 'required|max_length[40]');
			$this->form_validation->set_rules('FIRST_NME', 'First Name', 'required|max_length[40]');
			$this->form_validation->set_rules('MIDDLE_NME', 'Middle Initial', 'max_length[3]');
			$this->form_validation->set_rules('POSITION_NME', 'Position', 'max_length[40]');
			$this->form_validation->set_rules('SECTION_NME', 'Section Name', 'max_length[40]');
			$this->form_validation->set_rules('AREA_CDE', 'Area/District', 'max_length[75]');
			$this->form_validation->set_rules('SUPER_ID', 'Supervisor ID', 'max_length[75]|numeric');

			$post['ADDBY'] = $this->session->userdata('samaccountname');
			$post['ADDDTTM'] = date('y-m-d');

			if ($this->form_validation->run() == true)
			{
				if (!$this->TPW_model->does_emp_id_exist($post['EMPL_ID']))
				{
					if ($this->TPW_model->insert_employee($post))
					{
						$this->session->set_flashdata('message', 'Employee ID: '.$post['EMPL_ID'].' has been added.');
						redirect('TPW/update_employee/'.$post['EMPL_ID']);
					}
					else
					{
						$this->session->set_flashdata('error', 'The employee record could not be inserted, please try again.');
						redirect('TPW/add_employee');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'The employee ID already exists, please try again.');
					redirect('TPW/add_employee');
				}				
			}
			else
			{
				$data['endorsements'] = $this->TPW_model->get_endorsements();
				$data['conditions'] = $this->TPW_model->get_conditions();
				$data['content'] = 'pages/TPW/add_employee_view';
				$this->load->view('master', $data, FALSE);
			}

		}
		else
		{
			$data['endorsements'] = $this->TPW_model->get_endorsements();
			$data['conditions'] = $this->TPW_model->get_conditions();
			$data['content'] = 'pages/TPW/add_employee_view';
			$this->load->view('master', $data, FALSE);
		}
	}

	public function update_employee()
	{

		// Load Model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);
		$id = $this->uri->segment(3);


		if (!empty($post))
		{
			// Validate an dinsert data
			$this->form_validation->set_rules('EMPL_ID', 'Employee ID', 'required|max_length[10]');
			$this->form_validation->set_rules('LAST_NME', 'Last Name', 'required|max_length[40]');
			$this->form_validation->set_rules('FIRST_NME', 'First Name', 'required|max_length[40]');
			$this->form_validation->set_rules('MIDDLE_NME', 'Middle Initial', 'max_length[3]');
			$this->form_validation->set_rules('POSITION_NME', 'Position', 'max_length[40]');
			$this->form_validation->set_rules('SECTION_NME', 'Section Name', 'max_length[40]');
			$this->form_validation->set_rules('AREA_CDE', 'Area/District', 'max_length[75]');
			$this->form_validation->set_rules('SUPER_ID', 'Supervisor ID', 'max_length[75]|numeric');

			if ($this->form_validation->run() == true)
			{

				// Add the rest of the required fields.
				$post['MODBY'] = $this->session->userdata('samaccountname');
				$post['MODDTTM'] = date('y-m-d');

				// Insert the record
				if ($this->TPW_model->update_employee($post))
				{
					$this->session->set_flashdata('message', 'The employee has been updated.');
					redirect('TPW/update_employee/'.$post['EMPL_ID']);
				}
				else
				{
					$this->session->set_flashdata('error', 'The employee could not be updated.');
                    redirect('TPW/update_employee/'.$post['ORG_EMPL_ID']);
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('TPW/update_employee/'.$post['EMPL_ID']);
			}
		}
		else
		{
			$data['endorsements'] = $this->TPW_model->get_endorsements();
			$data['conditions'] = $this->TPW_model->get_conditions();
			$data['employee'] = $this->TPW_model->get_employee_by_id($id);
			$data['supervisor'] = $this->TPW_model->get_employee_by_id($data['employee']['SUPER_ID']);
			$data['content'] = 'pages/TPW/update_employee_view';
			$this->load->view('master', $data, FALSE);
		}
	}

	public function delete_employee()
	{
		//Load Model
		$this->load->model('TPW_model');

		$id = $this->input->post('EMPL_ID');

		if ($this->TPW_model->remove_employee($id))
		{

			// Redirect with a success message.
			$this->session->set_flashdata('message', 'Employee: '.$id.' has been deleted.');
			redirect('TPW/employees');
		}
		else
		{
			$this->session->set_flashdata('error', 'Employee: '.$id.' could not be deleted.');
			redirect('TPW/employees');
		}
	}

/*           
============================================================================
endorsement
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function endorsement()
	{
		// Load Model
		$this->load->model('TPW_model');
		// Get a list of the endorsements to select from for an update.
		$data['endorsements'] = $this->TPW_model->get_endorsements();
		if (!empty($data['endorsements'])){$data['endorsements'] = array_slice($data['endorsements'], 0, 25);}
		$data['content'] = "pages/TPW/endorsement_view";
		$this->load->view('master', $data);
	}

	public function add_endorsement()
	{
		// Load Model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			$this->form_validation->set_rules('ENDORSEMENT_CDE', 'Endorsement Code', 'required|max_length[1]');
			$this->form_validation->set_rules('ENDORSEMENT_DSC', 'Description', 'required|max_length[75]');

			if ($this->form_validation->run() == true)
			{
				$post['ADDBY'] = $this->session->userdata('samaccountname');
				$post['ADDDTTM'] = date('y-m-d');
				$post['ENDORSEMENT_DSC'] = strtoupper($post['ENDORSEMENT_DSC']);

				if ($this->TPW_model->insert_endorsement($post))
				{
					$this->session->set_flashdata('message', 'Endorsement with code: '.$post['ENDORSEMENT_CDE'].' was added.');
					redirect('TPW/endorsement');
				}
				else
				{
					$this->session->set_flashdata('error', 'Endorsement with code: '.$post['CONDITION_CDE'].' could not be inserted, please try again.');
					redirect('TPW/endorsement');
				}	
			}
			else
			{
				$data['content'] = "pages/TPW/add_endorsement_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			$data['content'] = "pages/TPW/add_endorsement_view";
			$this->load->view('master', $data);
		}
	} 

	public function update_endorsement()
	{
		// Load Model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			// Validate an dinsert data
			$this->form_validation->set_rules('ENDORSEMENT_CDE', 'Endorsement Code', 'required|max_length[1]');
			$this->form_validation->set_rules('ENDORSEMENT_DSC', 'Endorsement Name', 'required|max_length[75]');

			if ($this->form_validation->run() == true)
			{
				// Add the rest of the required fields.
				$post['MODBY'] = $this->session->userdata('samaccountname');
				$post['MODDTTM'] = date('y-m-d');
				$post['ENDORSEMENT_DSC'] = strtoupper($post['ENDORSEMENT_DSC']);
				
				// Insert the record
				if ($this->TPW_model->update_endorsement($post))
				{
					$this->session->set_flashdata('message', 'Endorsement code: '.$post['ENDORSEMENT_CDE'].' has been updated.');
					redirect('TPW/endorsement');
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('TPW/endorsement');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('TPW/endorsement');
			}
		}
		else
		{
			$data['endorsement'] = $this->TPW_model->get_endorsement_by_id($this->uri->segment(3));
			$data['content'] = "pages/TPW/update_endorsement_view";
			$this->load->view('master', $data);
		}
	}

	public function delete_endorsement()
	{
		//Load Model
		$this->load->model('TPW_model');

		$id = $this->input->post('ENDORSEMENT_CDE');

		if ($this->TPW_model->remove_endorsement($id))
		{

			// Redirect with a success message.
			$this->session->set_flashdata('message', 'Endorsement: '.$id.' has been deleted.');
			redirect('TPW/endorsement');
		}
		else
		{
			$this->session->set_flashdata('message', 'Endorsement: '.$id.' could not be deleted.');
			redirect('TPW/endorsement');
		}
	}

/*           
============================================================================
licence_conditions
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function licence_conditions()
	{
		// Load Model
		$this->load->model('TPW_model');
		// Get a list of the conditions to select from for an update.
		$data['conditions'] = $this->TPW_model->get_conditions();
		$data['content'] = "pages/TPW/conditions_view";
		$this->load->view('master', $data);
	}

	public function add_condition()
	{
		// Load Model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			// Validate an dinsert data
			$this->form_validation->set_rules('CONDITION_CDE', 'Condition Code', 'required|max_length[3]|numeric');
			$this->form_validation->set_rules('CONDITION_DSC', 'Condition name', 'required|max_length[75]');

			// Check to see if that cond. code already exists
			if ($this->form_validation->run() == true)
			{

				if (!$this->TPW_model->does_condition_exist($post['CONDITION_CDE']))
				{
					// Add the rest of the required fields.
					$name = explode('\\', $this->session->userdata('rdn'));
					$post['ADDBY'] = $name[1];
					$post['ADDDTTM'] = date('y-m-d');
					$post['CONDITION_DSC'] = strtoupper($post['CONDITION_DSC']);

					// Insert the record
					if ($this->TPW_model->insert_condition($post))
					{
						$this->session->set_flashdata('message', 'Licence condition code: '.$post['CONDITION_CDE'].' has been added.');
						redirect('TPW/licence_conditions');
					}
					else
					{
						$this->session->set_flashdata('error', 'There was an error adding the record, please try again.');
						redirect('TPW/licence_conditions');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'A licence condition with code: '.$post['CONDITION_CDE'].' already exsits.');
					redirect('TPW/licence_conditions');
				}
			}
			else
			{
				$data['content'] = "pages/TPW/add_condition_view";
				$this->load->view('master', $data);
			}
			
		}
		else
		{
			$data['content'] = "pages/TPW/add_condition_view";
			$this->load->view('master', $data);
		}
	}

	public function update_condition()
	{
		// Load Model
		$this->load->model('TPW_model');

		$post = $this->input->post(null, true);

		if (!empty($post))
		{
			// Validate an dinsert data
			$this->form_validation->set_rules('CONDITION_CDE', 'Condition Code', 'required|max_length[3]');
			$this->form_validation->set_rules('CONDITION_DSC', 'Condition name', 'required|max_length[75]');

			if ($this->form_validation->run() == true)
			{
				// Add the rest of the required fields.
				$post['MODBY'] = $this->session->userdata('samaccountname');
				$post['MODDTTM'] = date('y-m-d');
				$post['CONDITION_DSC'] = strtoupper($post['CONDITION_DSC']);

				// Insert the record
				if ($this->TPW_model->update_condition($post))
				{
					$this->session->set_flashdata('message', 'Licence condition code: '.$post['CONDITION_CDE'].' has been updated.');
					redirect('TPW/licence_conditions');
				}
				else
				{
					$this->session->set_flashdata('error', 'There was an error updating the record, please try again.');
					redirect('TPW/licence_conditions');
				}
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('TPW/licence_conditions');
			}
		}
		else
		{
			$data['condition'] = $this->TPW_model->get_condition_by_id($this->uri->segment(3));
			$data['content'] = "pages/TPW/update_condition_view";
			$this->load->view('master', $data);
		}
	}
	
	public function delete_condition()
	{
		//Load Model
		$this->load->model('TPW_model');

		$id = $this->input->post('CONDITION_CDE');

		if ($this->TPW_model->remove_condition($id))
		{

			// Redirect with a success message.
			$this->session->set_flashdata('message', 'Condition: '.$id.' has been deleted.');
			redirect('TPW/licence_conditions');
		}
		else
		{
			$this->session->set_flashdata('error', 'Condition: '.$id.' could not be deleted.');
			redirect('TPW/licence_conditions');
		}
		
	}

}
/* End of file TPW.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/TPW.php */