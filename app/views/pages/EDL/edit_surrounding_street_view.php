<?php
// determine if the user is an admin -- redirect if not
$is_admin = false;
$is_readwrite = false;

$groups = $this->session->userdata('groups');

foreach ($groups as $group)
{
    if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite')
    {
        $is_readwrite = true;
    }

    if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
    {
        $is_admin = true;
    }
}

if (!($is_admin) && !($is_readwrite)) {
    redirect('EDL/');
}
?>

<div class="row">
	<div class="col-md-12">
		<h2>Manage Surrounding Streets</h2>
		<p class="lead">Use this form to add/remove a surrounding street to the system</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<?php if (!empty($streets)): ?>
	<div class="form-group">
		<div class="row">

                <div class="col-sm-3">
                    <label for="STREET">Name</label>
                </div>
				<div class="col-sm-1">
					<label for="STREET_TYP">Type</label>
				</div>
                <div class="col-sm-2">
                    <label for="STREET_CDE">Number</label>
                </div>
				<div class="col-sm-4">
					<label for="COMMUNITY">Community</label>
				</div>
				<div class="col-sm-1">
					<label for="FILE_ID">Action</label>
				</div>
		</div>
	</div>

		<?php foreach ($streets as $street): ?>
			<form action="<?php echo base_url(); ?>EDL/delete_surrounding_street" method="POST">
                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-3">
                            <input type="text" name="STREET" class="form-control" title="Street" value="<?= $street['STREET'] ?>" readonly />
                        </div>
                        <div class="col-sm-1">
                            <input type="text" name="STREET_TYP" class="form-control" title="Street Type" value="<?= $street['STREET_TYP'] ?>" readonly />
                        </div>
                        <div class="col-sm-2">
                            <input type="text" name="STREET_CDE" class="form-control" title="Street Code" value="<?= $street['STREET_CDE'] ?>" readonly  />
                        </div>
                        <div class="col-sm-4">
                            <input type="text" name="COMMUNITY" class="form-control" title="Community" value="<?= $street['COMMUNITY'] ?>" readonly />
                        </div>
                        <div class="col-sm-1">
                            <input type="hidden" name="FILE_ID" value="<?= $FILE_ID ?>" />
                            <button type="submit" class="btn btn-sm btn-danger delete_surrounding" title="Delete">Delete</button>
                        </div>
				    </div>
                </div>
			</form>
		<?php endforeach ?>	

<?php endif ?>
<div class="form-group">
	<div class="row">
	<form action="<?php echo base_url(); ?>EDL/add_surrounding_street" method="POST">
			<div class="col-sm-3">
				<label for="STREET">Name</label>
				<input type="text" name="STREET_ADD" id="STREET_ADD" class="form-control STREET" title="Street" readonly />
			</div>
			<div class="col-sm-1">
				<label for="STREET_TYP">Type</label>
				<input type="text" name="STREET_TYPE_ADD" id="STREET_TYPE_ADD" class="form-control STREET_TYP" title="Street Type" readonly />
			</div>
            <div class="col-sm-2">
                <label for="STREET_CDE">Number</label>
                <input type="text" name="STREET_CODE_ADD" id="STREET_CODE_ADD" class="form-control STREET_CDE" title="Street Code"  readonly />
            </div>
			<div class="col-sm-4">
				<label for="COMMUNITY">Community</label>
				<input type="text" name="COMMUNITY_ADD" id="COMMUNITY_ADD" class="form-control COMMUNITY" title="Community" readonly />
			</div>
			<div class="col-sm-1">
				<input type="hidden" name="FILE_ID" value="<?= $FILE_ID ?>" />
				<button type="submit" class="btn btn-sm btn-success no_label_fix" title="Add">Add</button>
			</div>
	</div>
</div>

<div class="form-group">
	<div class="row">
		<div class="col-sm-1">
			<button type="button" value="add_surrounding" class="btn btn-sm btn-primary no_label_fix modal_btn" data-toggle="modal" data-target="#myModal" >Select Street</button>
		</div>
		<div class="col-sm-10 no_label_fix">
			<p class="text-muted">(Use the street look up tool to select a street by clicking "Select Street")</p>
		</div>
	</div>
</div>
	<legend></legend>
<div class="form-group">
	<div class="row">
		<div class="col-sm-12 ">
			<a href="<?php echo base_url(); ?>EDL/" class="btn btn-md btn-default ">Back to EDL Lookup</a>
			<a class="btn btn-md btn-primary pull-right" href="<?php echo base_url(); ?>EDL/update_location/<?= $FILE_ID ?>">Back to Record</a>
		</div>
	</div>
</div>
</form>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title">Street Look Up</h4>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="col-sm-5">
									<div class="modal_search">
										<label for="search_box">Street Name</label>
										<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
										<p class="text-muted">(Select the name of the record you would like to use)</p>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<table class="table table-bordered">
							<thead>
								<th>Name</th>
							</thead>
							<tbody class="modal_tbody">
								<tr>
									
								</tr>
							</tbody>	
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		
