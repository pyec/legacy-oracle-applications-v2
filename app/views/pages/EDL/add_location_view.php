<?php
// determine if the user is an admin -- redirect if not
$is_admin = false;
$is_readwrite = false;

$groups = $this->session->userdata('groups');

foreach ($groups as $group)
{
    if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite')
    {
        $is_readwrite = true;
    }

    if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
    {
        $is_admin = true;
    }
}

if (!($is_admin) && !($is_readwrite)) {
    redirect('EDL/');
}
?>

<div class="row">
	<div class="col-md-6">
		<h2 style="padding-bottom: 1em">Add Drawing Location</h2>
<!--		<p class="lead">Use this form to add a drawing and location</p>-->
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php elseif($this->session->userdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->userdata('error'); ?></p>
		<?php $this->session->unset_userdata('error'); ?>
	</div>
<?php endif ?>
<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>EDL/add_location" method="POST">

		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="FILE_ID">File ID</label>
					<input type="text" name="FILE_ID" class="form-control FILE_ID" title="File ID" maxlength="9" value="<?php echo set_value('FILE_ID') ?>"/>
				</div>
			
				<div class="col-sm-2">
					<label for="PROJ_NUM">Project #</label>
					<input type="text" name="PROJ_NUM" class="form-control PROJ_NUM" title="Project Number" maxlength="8" placeholder="99-99999" value="<?php echo set_value('PROJ_NUM') ?>"/>
				</div>
				<div class="col-sm-2">
					<label for="TENDER_NUM">Tender #</label>
					<input type="text" name="TENDER_NUM" class="form-control TENDER_NUM" title="Tender Number" placeholder="YYYY-999" maxlength="8" value="<?php echo set_value('TENDER_NUM') ?>"/>
				</div>
				<div class="col-sm-3">
					<label for="FROM_PLAN">From Plan</label>
					<input type="text" name="FROM_PLAN" class="form-control" title="From Plan" maxlength="12" value="<?php echo set_value('FROM_PLAN') ?>"/>
				</div>
				<div class="col-sm-3">
					<label for="TO_PLAN">To Plan</label>
					<input type="text" name="TO_PLAN" class="form-control" title="To Plan" maxlength="12" value="<?php echo set_value('TO_PLAN') ?>"/>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<button type="button" class="modal_btn btn btn-primary btn-sm no_label_fix" value="location" data-toggle="modal" data-target="#myModal">Select</button>
				</div>
				<div class="col-sm-3">
					<label for="street_name">Name</label>
					<input type="text" name="street_name" class="form-control street_name" title="Street Name" maxlength="" readonly value="<?= $_POST['street_name'] ?>" />
				</div>
				<div class="col-sm-1">
					<label for="street_type">Type</label>
					<input tpye="text" name="street_type" class="form-control street_type" title="Street Type" maxlength="" readonly value="<?= $_POST['street_type'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="STREET_NUM">Number</label>
					<input type="text" name="STREET_NUM" class="form-control street_number" title="Street Number" maxlength="5" readonly value="<?= $_POST['STREET_NUM'] ?>"/>
					<input type="hidden" name="STREET_NUM" class="street_number" value="<?= $_POST['STREET_NUM'] ?>"/>
				</div>
				<div class="col-sm-4">
					<label for="area_name">Community</label>
					<input type="text" name="area_name" class="form-control area" title="Area Name" maxlength="" readonly value="<?= $_POST['area_name'] ?>"/>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="CIVIC_NUM">Civic #</label>
					<input type="text" name="CIVIC_NUM" class="form-control" title="Civic Number" maxlength="5" value="<?php echo set_value('CIVIC_NUM') ?>"/>
				</div>
                <div class="col-sm-2">
                    <label for="CIVIC_NUM">Ext</label>
                    <input type="text" name="EXT" class="form-control" title="Civic Number" maxlength="4" value="<?php echo set_value('EXT') ?>" />
               </div>
				<div class="col-sm-1">
					<label for="SIDE">Side</label>
					<input tpye="text" name="SIDE" class="form-control" title="Side" maxlength="3" value="<?php echo set_value('SIDE') ?>" />
				</div>
				<div class="col-sm-5">
					<label for="LOCATION">Location #</label>
					<select name="LOCATION" class="form-control" title="Location Number">
						<option value="">Select..</option>
						<?php foreach ($locations as $loc): ?>
                        <?php if ($_POST['LOCATION'] == $loc['LOCATION_CODE']): ?>
                            <option selected value="<?= $loc['LOCATION_CODE'] ?>"><?= $loc['LOCATION_CODE'].' - '.$loc['LOCATION_DSC'] ?></option>
                        <?php else : ?>
                            <option value="<?= $loc['LOCATION_CODE'] ?>"><?= $loc['LOCATION_CODE'].' - '.$loc['LOCATION_DSC'] ?></option>
                        <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
			</div>
		</div>





        <div>
		<legend>Surrounding Streets</legend>
            <p class="text-muted">You can add three surrounding streets initially and then add more though the update screen.</p>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<button type="button" class="modal_btn btn btn-primary btn-sm no_label_fix" value="surrounding-0" data-toggle="modal" data-target="#myModal">Select</button>
					<input type="hidden" name="FILE_ID-0" value="" />
				</div>
				<div class="col-sm-3">
						<label for="STREET">Name</label>
					<input type="text" id="STREET-0" name="STREET-0" class="form-control 0-STREET" title="Street" value="<?= $_POST['STREET-0'] ?>" readonly />
				</div>
				<div class="col-sm-1">
						<label for="STREET_TYP">Type</label>
					<input type="text" id="STREET_TYPE-0" name="STREET_TYPE-0" class="form-control 0-STREET_TYP" title="Street Type" value="<?= $_POST['STREET_TYPE-0'] ?>" readonly />
				</div>
                <div class="col-sm-2">
                    <label for="STREET_CDE">Number</label>
                    <input type="text" id="STREET_CODE-0" name="STREET_CODE-0" class="form-control 0-STREET_CDE" title="Street Code" value="<?= $_POST['STREET_CODE-0'] ?>" readonly  />
                </div>
				<div class="col-sm-4">
						<label for="COMMUNITY">Community</label>
					<input type="text" id="COMMUNITY-0" name="COMMUNITY-0" class="form-control 0-COMMUNITY" title="Community" value="<?= $_POST['COMMUNITY-0'] ?>" readonly />
				</div>
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Odd L</label>-->
<!--					<input type="text" name="0-ODDLOW" class="form-control 0-ODDLOW" title="Odd Low" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDHI">Odd H</label>-->
<!--					<input type="text" name="0-ODDHI" class="form-control 0-ODDHI" title="Odd High" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Even L</label>-->
<!--					<input type="text" name="0-ODDLOW" class="form-control 0-ODDLOW" title="Even Low" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Even H</label>-->
<!--					<input type="text" name="0-ODDLOW" class="form-control 0-ODDLOW" title="Even High" value="" readonly />-->
<!--				</div>-->
			</div>
        </div>
        <div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<button type="button" class="modal_btn btn btn-primary btn-sm no_label_fix" value="surrounding-1" data-toggle="modal" data-target="#myModal">Select</button>
					<input type="hidden" name="FILE_ID-1" value="" />
				</div>
				<div class="col-sm-3">
						<label for="STREET">Name</label>
					<input type="text" id="STREET-1" name="STREET-1" class="form-control 1-STREET" title="Street" value="<?= $_POST['STREET-1'] ?>" readonly />
				</div>
				<div class="col-sm-1">
						<label for="STREET_TYP">Type</label>
					<input type="text" id="STREET_TYPE-1" name="STREET_TYPE-1" class="form-control 1-STREET_TYP" title="Street Type" value="<?= $_POST['STREET_TYPE-1'] ?>" readonly />
				</div>
                <div class="col-sm-2">
                    <label for="STREET_CDE">Code</label>
                    <input type="text" id="STREET_CODE-1" name="STREET_CODE-1" class="form-control 1-STREET_CDE" title="Street Code" value="<?= $_POST['STREET_CODE-1'] ?>" readonly  />
                </div>
				<div class="col-sm-4">
						<label for="COMMUNITY">Community</label>
					<input type="text" id="COMMUNITY-1" name="COMMUNITY-1" class="form-control 1-COMMUNITY" title="Community" value="<?= $_POST['COMMUNITY-1'] ?>" readonly />
				</div>
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Odd L</label>-->
<!--					<input type="text" name="1-ODDLOW" class="form-control 1-ODDLOW" title="Odd Low" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDHI">Odd H</label>-->
<!--					<input type="text" name="1-ODDHI" class="form-control 1-ODDHI" title="Odd High" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Even L</label>-->
<!--					<input type="text" name="1-ODDLOW" class="form-control 1-ODDLOW" title="Even Low" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Even H</label>-->
<!--					<input type="text" name="1-ODDLOW" class="form-control 1-ODDLOW" title="Even High" value="" readonly />-->
<!--				</div>-->
<!--			</div>-->

            </div>
        </div>
        <div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<button type="button" class="modal_btn btn btn-primary btn-sm no_label_fix" value="surrounding-2" data-toggle="modal" data-target="#myModal">Select</button>
					<input type="hidden" name="FILE_ID-2" value="" />
				</div>
				<div class="col-sm-3">
						<label for="STREET">Name</label>
					<input type="text" id="STREET-2" name="STREET-2" class="form-control 2-STREET" title="Street" value="<?= $_POST['STREET-2'] ?>" readonly />
				</div>
				<div class="col-sm-1">
						<label for="STREET_TYP">Type</label>
					<input type="text" id="STREET_TYPE-2" name="STREET_TYPE-2" class="form-control 2-STREET_TYP" title="Street Type" value="<?= $_POST['STREET_TYPE-2'] ?>" readonly />
				</div>
                <div class="col-sm-2">
                    <label for="STREET_CDE">Code</label>
                    <input type="text" id="STREET_CODE-2" name="STREET_CODE-2" class="form-control 2-STREET_CDE" title="Street Code" value="<?= $_POST['STREET_CODE-2'] ?>" readonly  />
                </div>
				<div class="col-sm-4">
						<label for="COMMUNITY">Community</label>
					<input type="text" id="COMMUNITY-2" name="COMMUNITY-2" class="form-control 2-COMMUNITY" title="Community" value="<?= $_POST['COMMUNITY-2'] ?>" readonly />
				</div>
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Odd L</label>-->
<!--					<input type="text" name="2-ODDLOW" class="form-control 2-ODDLOW" title="Odd Low" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDHI">Odd H</label>-->
<!--					<input type="text" name="2-ODDHI" class="form-control 2-ODDHI" title="Odd High" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Even L</label>-->
<!--					<input type="text" name="2-ODDLOW" class="form-control 2-ODDLOW" title="Even Low" value="" readonly />-->
<!--				</div>-->
<!--				<div class="col-sm-1">-->
<!--						<label for="ODDLOW">Even H</label>-->
<!--					<input type="text" name="2-ODDLOW" class="form-control 2-ODDLOW" title="Even High" value="" readonly />-->
<!--				</div>-->
			</div>
		</div>
        </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-5">
                        <label for="ORIG_CDE">Originator</label>
                        <select name="ORIG_CDE" class="form-control" title="Originator" maxlength="4" >
                            <option value="">Select..</option>
                            <?php foreach ($originators as $orig): ?>
                                <?php if ($_POST['ORIG_CDE'] != null): ?>
                                    <?php if ($_POST['ORIG_CDE'] == $orig['ORIGINATOR_CODE']): ?>
                                        <option selected value="<?= $orig['ORIGINATOR_CODE'] ?>"><?= $orig['ORIGINATOR_CODE'].' - '.$orig['ORIGINATOR_DSC'] ?></option>
                                    <?php else: ?>
                                        <option value="<?= $orig['ORIGINATOR_CODE'] ?>"><?= $orig['ORIGINATOR_CODE'].' - '.$orig['ORIGINATOR_DSC'] ?></option>
                                    <?php endif ?>
                                <?php else: ?>
                                    <?php if ($orig['ORIGINATOR_CODE'] == 'HDS') : ?>
                                        <option selected value="<?= $orig['ORIGINATOR_CODE'] ?>"><?= $orig['ORIGINATOR_CODE'].' - '.$orig['ORIGINATOR_DSC'] ?></option>
                                    <?php else: ?>
                                        <option value="<?= $orig['ORIGINATOR_CODE'] ?>"><?= $orig['ORIGINATOR_CODE'].' - '.$orig['ORIGINATOR_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-sm-5">
                        <label for="PROJTYP_CDE">Project Type</label>
                        <select name="PROJTYP_CDE" class="form-control" title="Project">
                            <option value="">Select..</option>
                            <?php foreach ($projects as $project): ?>
                                <?php if ($_POST['PROJTYP_CDE'] == $project['PROJTYP_CODE']): ?>
                                    <option selected value="<?= $project['PROJTYP_CODE'] ?>"><?= $project['PROJTYP_CODE'].' - '.$project['PROJTYP_DSC'] ?></option>
                                <?php else : ?>
                                    <option value="<?= $project['PROJTYP_CODE'] ?>"><?= $project['PROJTYP_CODE'].' - '.$project['PROJTYP_DSC'] ?></option>
                                <?php endif ?>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            </div>


            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <label for="NOTES">Title/Description</label>
                        <textarea name="NOTES" class="form-control" title="Title/Description" maxlength="500"><?= set_value('NOTES') ?></textarea>
                    </div>
                </div>
            </div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="SURV_METH">Survey Method</label>
					<select name="SURV_METH" class="form-control" title="Survey Method">
						<option value="">Select..</option>
						<?php foreach ($survey_methods as $method): ?>
                            <?php if ($_POST['SURV_METH'] == $method['SURVMETH_CODE']): ?>
                                <option selected value="<?= $method['SURVMETH_CODE'] ?>"><?= $method['SURVMETH_CODE'].' - '.$method['SURVMETH_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $method['SURVMETH_CODE'] ?>"><?= $method['SURVMETH_CODE'].' - '.$method['SURVMETH_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="SURVEYOR">Surveyor</label>
					<select name="SURVEYOR" class="form-control" title="Surveyor">
						<option value="">Select..</option>
						<?php foreach ($surveyors as $surveyor): ?>
                            <?php if ($_POST['SURVEYOR'] == $surveyor['SURVEYOR_CODE']): ?>
                                <option selected value="<?= $surveyor['SURVEYOR_CODE'] ?>"><?= $surveyor['SURVEYOR_CODE'].' - '.$surveyor['SURVEYOR_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $surveyor['SURVEYOR_CODE'] ?>"><?= $surveyor['SURVEYOR_CODE'].' - '.$surveyor['SURVEYOR_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="RELFILE_ID">Related File ID</label>
					<input type="text" name="RELFILE_ID" class="form-control" title="Related File ID" maxlength="9" value="<?php echo set_value('RELFILE_ID') ?>"/>
				</div>
				<div class="col-sm-3">
					<label for="SW_VER">Software Version</label>
					<select name="SW_VER" class="form-control" title="Software Version"> 
						<option value="">Select..</option>
						<?php foreach ($software_versions as $version): ?>
                            <?php if ($_POST['SW_VER'] == $version['SWVER_CODE']): ?>
                                <option selected value="<?= $version['SWVER_CODE'] ?>"><?= $version['SWVER_CODE'].' - '.$version['SWVER_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $version['SWVER_CODE'] ?>"><?= $version['SWVER_CODE'].' - '.$version['SWVER_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="DEPT">Department</label>
					<select name="DEPT" class="form-control" title="Department">
						<option value="">Select..</option>
						<?php foreach ($departments as $dpt): ?>
                            <?php if ($_POST['DEPT'] == $dpt['DEPT_CODE']): ?>
                                <option selected value="<?= $dpt['DEPT_CODE'] ?>"><?= $dpt['DEPT_CODE'] ?></option>
                            <?php else : ?>
                                <option value="<?= $dpt['DEPT_CODE'] ?>"><?= $dpt['DEPT_CODE'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="E_CLASS_NUM">Electronic Class #</label>
					<select name="E_CLASS_NUM" class="form-control" title="Electronic Class Number">
						<option value="">Select..</option>
						<?php foreach ($electronic_class_numbers as $e_num): ?>
                        <?php if ($_POST['E_CLASS_NUM'] == $e_num['CLASS_CODE']): ?>
                                <option selected value="<?= $e_num['CLASS_CODE'] ?>"><?= $e_num['CLASS_CODE'] ?></option>
                            <?php else : ?>
                                <option value="<?= $e_num['CLASS_CODE'] ?>"><?= $e_num['CLASS_CODE'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="P_CLASS_NUM">Paper Class #</label>
					<select name="P_CLASS_NUM" class="form-control" title="Paper Class Number">
						<option value="">Select..</option>
						<?php foreach ($paper_class_numbers as $p_num): ?>
                            <?php if ($_POST['P_CLASS_NUM'] == $p_num['CLASS_CODE']): ?>
                                <option selected value="<?= $p_num['CLASS_CODE'] ?>"><?= $p_num['CLASS_CODE'] ?></option>
                            <?php else : ?>
                                <option value="<?= $p_num['CLASS_CODE'] ?>"><?= $p_num['CLASS_CODE'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
                <div class="col-sm-2">
                    <label for="SIGNED">Signed Date</label>
                    <input type="text" name="SIGNED" class="form-control date_picker_new" title="Signed Date" readonly value="<?= $_POST['SIGNED'] ?>"/>
                </div>
				<div class="col-sm-3">
					<label for="date_name">Date Note</label>
					<input tpye="text" name="ARCHIVE_DATE" class="form-control" title="Date Note" maxlength="50" value="<?= set_value('ARCHIVE_DATE') ?>" />
				</div>
                <div class="col-sm-2">
                    <label for="MICROFILMED">Microfilmed Date</label>
                    <input type="text" name="MICROFILMED" class="form-control date_picker_new" title="Microfilmed Date" readonly value="<?= $_POST['MICROFILMED'] ?>"/>
                </div>
                <div class="col-sm-3">
                    <label for="ROLL_NUM">Roll #</label>
                    <input type="text" name="ROLL_NUM" class="form-control" title="Roll Number" maxlength="10" value="<?php echo set_value('ROLL_NUM') ?>"/>
                </div>
<!--                <div class="col-sm-2">-->
<!--                    --><?php //if (!$read_only): ?>
<!--                        <button type="button" class="btn btn-sm btn-primary no_label_fix clear_dates">Clear Dates</button>-->
<!--                    --><?php //endif ?>
<!--                </div>-->
			</div>
		</div>

		<legend>Locations</legend>
        <p class="text-muted">You can add three file locations initially and then add more though the update screen</p>
            <div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="PROCESS">Process</label>
					<select name="PROCESS-3" class="form-control" title="Process">
						<option value="">Select..</option>
						<?php foreach ($process as $pro): ?>
                            <?php if ($_POST['PROCESS-3'] == $pro['PROCESS_CODE']): ?>
                                <option selected value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="DEVICE">Device</label>
					<select name="DEVICE-3" class="form-control" title="Device">
						<option value="">Select..</option>
						<?php foreach ($devices as $device): ?>
                            <?php if ($_POST['DEVICE-3'] == $device['DEVICE_CODE']): ?>
                                <option selected value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="STATUS">Status</label>
					<select name="STATUS-3" class="form-control" title="Status">
						<option value="">Select..</option>
						<?php foreach ($statuses as $status): ?>
                            <?php if ($_POST['STATUS-3'] == $status['STATUS_CODE']): ?>
                                <option selected value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select> 
				</div>
				<div class="col-sm-3">
					<label for="DRAWN">Drawn By</label>
					<select name="DRAWN-3" class="form-control" title="Drawn By">
						<option value="">Select..</option>
						<?php foreach ($drawn_by as $drawn): ?>
                            <?php if ($_POST['DRAWN-3'] == $drawn['DRAWN_CODE']): ?>
                                <option selected value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select> 
				</div>
			</div>
        </div>
        <div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="DATETIME">Date</label>
					<input type="text" name="DATETIME-3" class="form-control date_picker_new" title="Date" maxlength="" readonly value="<?= $_POST['DATETIME-3'] ?>"/>
				</div>
				<div class="col-sm-4">
					<label for="FILE_LOC">File Location</label>
					<input type="text" name="FILE_LOC-3" class="form-control" title="File Location" maxlength="100" value="<?= $_POST['FILE_LOC-3'] ?>"/>
				</div>
				<div class="col-sm-6">
					<label for="PHYSICAL_DESC">Physical Description</label>
					<input type="text" name="PHYSICAL_DESC-3" class="form-control" title="Physical Description" maxlength="255" value="<?= $_POST['PHYSICAL_DESC-3'] ?>"/>
				</div>
			</div>
		</div>
            <hr style="margin-top: 30px; ">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="PROCESS">Process</label>
					<select name="PROCESS-4" class="form-control" title="Process">
						<option value="">Select..</option>
						<?php foreach ($process as $pro): ?>
                            <?php if ($_POST['PROCESS-4'] == $pro['PROCESS_CODE']): ?>
                                <option selected value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="DEVICE">Device</label>
					<select name="DEVICE-4" class="form-control" title="Device">
						<option value="">Select..</option>
						<?php foreach ($devices as $device): ?>
                            <?php if ($_POST['DEVICE-4'] == $device['DEVICE_CODE']): ?>
                                <option selected value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="STATUS">Status</label>
					<select name="STATUS-4" class="form-control" title="Status">
						<option value="">Select..</option>
						<?php foreach ($statuses as $status): ?>
                            <?php if ($_POST['STATUS-4'] == $status['STATUS_CODE']): ?>
                                <option selected value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select> 
				</div>
				<div class="col-sm-3">
					<label for="DRAWN">Drawn By</label>
					<select name="DRAWN-4" class="form-control" title="Drawn By">
						<option value="">Select..</option>
						<?php foreach ($drawn_by as $drawn): ?>
                            <?php if ($_POST['DRAWN-4'] == $drawn['DRAWN_CODE']): ?>
                                <option selected value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select> 
				</div>
			</div>
        </div>
        <div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="DATETIME">Date</label>
					<input type="text" name="DATETIME-4" class="form-control date_picker_new" title="Date" maxlength="" readonly value="<?= $_POST['DATETIME-4'] ?>"/>
				</div>
				<div class="col-sm-4">
					<label for="FILE_LOC">File Location</label>
					<input type="text" name="FILE_LOC-4" class="form-control" title="File Location" maxlength="100" value="<?= $_POST['FILE_LOC-4'] ?>"/>
				</div>
				<div class="col-sm-6">
					<label for="PHYSICAL_DESC">Physical Description</label>
					<input type="text" name="PHYSICAL_DESC-4" class="form-control" title="Physical Description" maxlength="255" value="<?= $_POST['PHYSICAL_DESC-4'] ?>"/>
				</div>
			</div>
		</div>
            <hr style="margin-top: 30px; ">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="PROCESS">Process</label>
					<select name="PROCESS-5" class="form-control" title="Process">
						<option value="">Select..</option>
						<?php foreach ($process as $pro): ?>
                            <?php if ($_POST['PROCESS-5'] == $pro['PROCESS_CODE']): ?>
                                <option selected value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $pro['PROCESS_CODE'] ?>"><?= $pro['PROCESS_CODE'].' - '.$pro['PROCESS_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="DEVICE">Device</label>
					<select name="DEVICE-5" class="form-control" title="Device">
						<option value="">Select..</option>
						<?php foreach ($devices as $device): ?>
                            <?php if ($_POST['DEVICE-5'] == $device['DEVICE_CODE']): ?>
                                <option selected value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $device['DEVICE_CODE'] ?>"><?= $device['DEVICE_CODE'].' - '.$device['DEVICE_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="STATUS">Status</label>
					<select name="STATUS-5" class="form-control" title="Status">
						<option value="">Select..</option>
						<?php foreach ($statuses as $status): ?>
                            <?php if ($_POST['STATUS-5'] == $status['STATUS_CODE']): ?>
                                <option selected value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $status['STATUS_CODE'] ?>"><?= $status['STATUS_CODE'].' - '.$status['STATUS_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select> 
				</div>
				<div class="col-sm-3">
					<label for="DRAWN">Drawn By</label>
					<select name="DRAWN-5" class="form-control" title="Drawn By">
						<option value="">Select..</option>
						<?php foreach ($drawn_by as $drawn): ?>
                            <?php if ($_POST['DRAWN-5'] == $drawn['DRAWN_CODE']): ?>
                                <option selected value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $drawn['DRAWN_CODE'] ?>"><?= $drawn['DRAWN_CODE'].' - '.$drawn['DRAWN_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select> 
				</div>
			</div>
        </div>
        <div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="DATETIME">Date</label>
					<input type="text" name="DATETIME-5" class="form-control date_picker_new" title="Date" maxlength="" readonly value="<?= $_POST['DATETIME-5'] ?>"/>
				</div>
				<div class="col-sm-4">
					<label for="FILE_LOC">File Location</label>
					<input type="text" name="FILE_LOC-5" class="form-control" title="File Location" maxlength="100" value="<?= $_POST['FILE_LOC-5'] ?>"/>
				</div>
				<div class="col-sm-6">
					<label for="PHYSICAL_DESC">Physical Description</label>
					<input type="text" name="PHYSICAL_DESC-5" class="form-control" title="Physical Description" maxlength="255" value="<?= $_POST['PHYSICAL_DESC-5'] ?>"/>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_interred pull-right" value="Add Record"/>
					<a href="<?php echo base_url(); ?>EDL/" class="btn btn-md btn-default return_interred">Back to EDL Lookup</a>
				</div>
			</div>
		</div>

		</form>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title">Street Look Up</h4>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="col-sm-5">
									<div class="modal_search">
										<label for="search_box">Street Name</label>
										<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" placeholder="Street Name" />
										<p class="text-muted">(Select the name of the record you would like to use)</p>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<table class="table table-bordered">
							<thead>
								<th>Name</th>
							</thead>
							<tbody class="modal_tbody">
								<tr>
									
								</tr>
							</tbody>	
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->


	</div>
</div>