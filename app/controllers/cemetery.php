<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Cemetery System

Description : This is the controller for the cemetery system. This system allows
			users to CRUD on the owners of cemetery plots in HRM, the interred 
			burried there and the  cemeteries them selves.

  Functions : index(), 
			  owners(), 
			  add_owner(), 
			  delete_owner(), 
			  update_owner(),
  			  interred(), 
  			  add_interred(), 
  			  update_interred(), 
  			  delete_interred(),
  			  cemetery_names(),
              cemeteryNamesAndNumbers(),
              cemeteryInterred(),
  			  add_cemetery_name(), 
  			  delete_cemetery_name(),
  			  update_cemetery_name(),
              ajax_get_owners()

=========================================================================================================================*/
class Cemetery extends CI_Controller 
{

	public function index()
	{
		$data['content'] = "pages/cemetery/home_view";
		$this->load->view('master', $data);
	}

	
/*           
============================================================================
owners
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/

	public function owners()
	{
		//Load Model
		$this->load->model('cemetery_model');
		
		// get cemeteries for search drop down
		$data['cemeteries'] = $this->cemetery_model->get_cemeteries();
		$data['content'] = "pages/cemetery/owners_view";
		$this->load->view('master', $data);
	}

	public function ajax_owners_by_id()
	{
		// Load Model
		$this->load->model('cemetery_model');
		$id = $this->input->post('id');

		// get the owner results by ID
		$results = $this->cemetery_model->get_owner_like_id($id);

		// Echo them back to the browser in JSON format
		echo json_encode($results);	
	}

	public function ajax_owners_by_name()
	{
		// Load Model
		$this->load->model('cemetery_model');

		$post = $this->input->post(null, true);

		$results = $this->cemetery_model->search_owners($post);

		echo json_encode($results);
	}

/*           
============================================================================
owner_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/

	public function owner_search()
	{
		// Load model
		$this->load->model('cemetery_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation rules
		$this->form_validation->set_rules('cemetery', 'Cemetery', 'trim|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('middle_name', 'Middle Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[50]|xss_clean');
        $this->form_validation->set_rules('location', 'Location', 'trim|max_length[50]|xss_clean');

		// validate the form
		if ($this->form_validation->run() == true)
		{	
			// if the user is searching by reference number.
			if (!empty($search_array))
			{	
				$results = $this->cemetery_model->search_owners($search_array);
			}
			// else just redirect back to the owners function which will load the initial data set
			else
			{
				redirect('cemetery/owners');
			}
		}
		else
		{
			// Validation failed, return errors.	
		}

		// store the results in the data array and load the view.
		$data['owners'] = $results;

		//if (count($data['owners']) > 500){$data['owners'] = array_slice($data['owners'], 0, 500);}

		if (!empty($data['owners']))
		{
			// look up the cemetery name for each record
			for ($i=0; $i < count($data['owners']); $i++) 
			{ 
				$data['owners'][$i]['CEM_NAME'] = $this->cemetery_model->get_cemetery_by_id($data['owners'][$i]['CEM_NBR'])[0]['CEM_NAME'];
			}	
		}

		// get cemeteries for search drop down
		$data['cemeteries'] = $this->cemetery_model->get_cemeteries();
		$data['content'] = "pages/cemetery/owners_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_owner
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/

	public function add_owner()
	{
		//Load Model
		$this->load->model('cemetery_model');

		// Get the post variables 
		$post_data = $this->input->post(null, true);
		

		if (!empty($post_data))
		{
			// Validation rules
            $this->form_validation->set_rules('CEM_NBR', 'Cemetery Name', 'required');
			$this->form_validation->set_rules('CEM_LNAME', 'Last Name', 'trim|max_length[30]|xss_clean|required');
			$this->form_validation->set_rules('CEM_MNAME', 'Middle Name', 'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('CEM_FNAME', 'First Name', 'trim|max_length[20]|xss_clean|required');
			$this->form_validation->set_rules('CEM_TTL', 'Title', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('CEM_OWNERS', 'Owner Information', 'trim|max_length[2000]|xss_clean');
			$this->form_validation->set_rules('CEM_SIZE', 'Size', 'trim|max_length[75]|xss_clean');
			$this->form_validation->set_rules('CEM_NBRGR', 'Number of Graves', 'trim|max_length[4]|numeric|xss_clean');
			$this->form_validation->set_rules('CEM_SCTN', 'Location', 'trim|max_length[4]|xss_clean');
			$this->form_validation->set_rules('CEM_SHEET', 'Sheet', 'trim|max_length[4]|xss_clean');
			$this->form_validation->set_rules('CEM_LOTNBR', 'Lot Number', 'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('CEM_PURDTE', 'Purchase Date', 'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('CEM_PURAMT', 'Purchase Amount', 'trim|max_length[11]|min_length[1]|numeric|xss_clean');
			$this->form_validation->set_rules('CEM_CHGNBR', 'Charge Number', 'trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('CEM_NBRMON', 'Number of monuments', 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('CEM_MONSIZE', 'Monument Size', 'trim|max_length[7]|xss_clean');
			$this->form_validation->set_rules('CEM_MONVEND', 'Supplier', 'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('CEM_OWNYR', 'Year', 'trim|max_length[4]|numeric|xss_clean');
			$this->form_validation->set_rules('CEM_PRPCARE', 'Perpetual Care', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('CEM_MARKER', 'Marker', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('CEM_MICR_ROLL', 'Micro Roll #', 'trim|max_length[4]|numeric|xss_clean');
			$this->form_validation->set_rules('CEM_MICR_FRM', 'Micro Frame #', 'trim|max_length[4]|numeric|xss_clean');
			$this->form_validation->set_rules('CEM_SPC_CMNT', 'Special Comment', 'trim|max_length[2000]|xss_clean');
			$this->form_validation->set_rules('CEM_CMNT', 'Comment', 'trim|max_length[2000]|xss_clean');
			$this->form_validation->set_rules('CEM_INCOMPURDTE', 'Incomplete Purchase Date', 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('CEM_CLDATE', 'Close Date', 'trim|max_length[10]|xss_clean');


			// Validate the form
			if ($this->form_validation->run() == true)
			{	
				// Check flag values and convert them to what the db stores
				if (!empty($post_data['CEM_CLFLAG']))
				{
					$post_data['CEM_CLFLAG'] = ($post_data['CEM_CLFLAG'] == 'on' ? 'Y':'N');
				}
				else
				{
					$post_data['CEM_CLFLAG'] = 'N';
				}

				// Capitalize everything
				foreach ($post_data as $key => $value) 
				{
					$post_data[$key] = strtoupper($value);
				}
				
				// Get the next ID from the owners table for the primary key.
				$post_data['CEM_REFNBR'] = $this->cemetery_model->get_next_val('CEM_OWNER', 'CEM_REFNBR');
				// cast the numbers to type int so they insert into the DB without error.
				$post_data['CEM_NBR'] = (int)$post_data['CEM_NBR'];
				$post_data['CEM_REFNBR'] = (int)$post_data['CEM_REFNBR'];

				// Run the insert statement
				$results = $this->cemetery_model->insert_owner($post_data);

				if ($results)
				{
					$this->session->set_flashdata('message', 'The owner was successfully added.');
                    redirect('cemetery/update_owner/'.$post_data['CEM_REFNBR'].'/'.$post_data['CEM_NBR']);
					//redirect('cemetery/owners');
            }
				else
				{
                    $data['errorMessage'] = 'An error has occurred. The owner was not inserted.';
                    $data['cemeteries'] = $this->cemetery_model->get_cemeteries();
                    $data['content'] = "pages/cemetery/add_owner_view";
                    $this->load->view('master', $data);
				}
			}
			else
			{
				// Validation failed 
				$data['cemeteries'] = $this->cemetery_model->get_cemeteries();
				$data['content'] = "pages/cemetery/add_owner_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			// error: no data in post.
			$data['cemeteries'] = $this->cemetery_model->get_cemeteries();
			$data['content'] = "pages/cemetery/add_owner_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
delete_owner
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/

	public function delete_owner()
	{
		//Load Model
		$this->load->model('cemetery_model');

		//Get the ID to delete
		$id = $this->input->post('CEM_REFNBR', true);

		// Run the delete statement
		if ($this->cemetery_model->remove_owner($id))
		{
			$this->session->set_flashdata('message', 'The owner was deleted successfully.');
			redirect('cemetery/owners');
		}
		else
		{
			// There was an error.
			$this->session->set_flashdata('erroe', 'The owner was not removed, please try again.');
			redirect('cemetery/owners');
		}
	}

	public function delete_owners_interred()
	{
		//Load Model
		$this->load->model('cemetery_model');

		//Get the ID to delete
		$data['CEM_REFNBR'] = $this->uri->segment(3);
		$data['CEM_NBR'] = $this->uri->segment(4);
		$data['INT_REFNBR'] = $this->uri->segment(5);


		// Run the delete statement
		if ($this->cemetery_model->remove_interred($data))
		{
			$this->session->set_flashdata('message', 'The interred was removed successfully.');
			redirect('cemetery/update_owner/'.$data['CEM_REFNBR'].'/'.$data['CEM_NBR']);
		}
		else
		{
			// There was an error.
			$this->session->set_flashdata('error', 'The interred was not removed, please try again.');
			redirect('cemetery/update_owner/'.$data['CEM_REFNBR'].'/'.$data['CEM_NBR']);
		}
	}
	
/*           
============================================================================
update_owner
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_owner()
	{
		//Load Model
		$this->load->model('cemetery_model');

		// Get the IDs to update an 
		$id = $this->uri->segment(3);
		$cem_id = $this->uri->segment(4);
		$post_data = $this->input->post(null, true);

		
		if (!empty($post_data))
		{
			// Validation Rules
            $this->form_validation->set_rules('CEM_NBR', 'Cemetery Name', 'required');
			$this->form_validation->set_rules('CEM_LNAME', 'Last Name', 'trim|max_length[30]|xss_clean|required');
			$this->form_validation->set_rules('CEM_MNAME', 'Middle Name', 'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('CEM_FNAME', 'First Name', 'trim|max_length[20]|xss_clean|required');
			$this->form_validation->set_rules('CEM_TTL', 'Title', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('CEM_OWNERS', 'Owner Information', 'trim|max_length[2000]|xss_clean');
			$this->form_validation->set_rules('CEM_SIZE', 'Size', 'trim|max_length[75]|xss_clean');
			$this->form_validation->set_rules('CEM_NBRGR', 'Number of Graves', 'trim|max_length[4]|xss_clean');
			$this->form_validation->set_rules('CEM_SCTN', 'Location', 'trim|max_length[4]|xss_clean');
			$this->form_validation->set_rules('CEM_SHEET', 'Sheet', 'trim|max_length[4]|xss_clean');
			$this->form_validation->set_rules('CEM_LOTNBR', 'Lot Number', 'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('CEM_PURDTE', 'Purchase Date', 'trim|max_length[50]|xss_clean');
			$this->form_validation->set_rules('CEM_PURAMT', 'Purchase Amount', 'trim|max_length[11]|min_length[1]|numeric|xss_clean');
			$this->form_validation->set_rules('CEM_CHGNBR', 'Charge Number', 'trim|max_length[6]|xss_clean');
			$this->form_validation->set_rules('CEM_NBRMON', 'Number of monuments', 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('CEM_MONSIZE', 'Monument Size', 'trim|max_length[7]|xss_clean');
			$this->form_validation->set_rules('CEM_MONVEND', 'Supplier', 'trim|max_length[30]|xss_clean');
			$this->form_validation->set_rules('CEM_OWNYR', 'Year', 'trim|max_length[4]|numeric|xss_clean');
			$this->form_validation->set_rules('CEM_PRPCARE', 'Perpetual Care', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('CEM_MARKER', 'Marker', 'trim|max_length[4]|xss_clean');
			$this->form_validation->set_rules('CEM_MICR_ROLL', 'Micro Roll #', 'trim|max_length[4]|xss_clean');
			$this->form_validation->set_rules('CEM_MICR_FRM', 'Micro Roll Frame #', 'trim|max_length[4]|xss_clean');
			$this->form_validation->set_rules('CEM_SPC_CMNT', 'Special Comment', 'trim|max_length[2000]|xss_clean');
			$this->form_validation->set_rules('CEM_CMNT', 'Comment', 'trim|max_length[2000]|xss_clean');
			$this->form_validation->set_rules('CEM_INCOMPURDTE', 'Incomplete Purchase Date', 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('CEM_CLDATE', 'Close Date', 'trim|max_length[10]|xss_clean');

			// Validate the form
			if ($this->form_validation->run() == true)
			{
				// make sure the flag has an accepted value.
				if (!empty($post_data['CEM_CLFLAG']))
				{
					$post_data['CEM_CLFLAG'] = ($post_data['CEM_CLFLAG'] == 'on' ? 'Y':'N');
				}
				else
				{
					$post_data['CEM_CLFLAG'] = 'N';
				}

				// Capitalize everything
				foreach ($post_data as $key => $value) 
				{
					$post_data[$key] = strtoupper($value);
				}
				
				// attempt to make an update to the owners data.
				$results = $this->cemetery_model->update_owner($post_data, $id, $cem_id);

				if ($results)
				{
					$this->session->set_flashdata('message', 'The owner was successfully updated.');
					redirect('cemetery/update_owner/'.$id.'/'.$cem_id);
				}
				else
				{
     				// Else the data update failed at the driver.
                    $data['errorMessage'] = 'An error has occured. the owner was not updated.';
                    $data['cemeteries'] = $this->cemetery_model->get_cemeteries();
                    $data['content'] = "pages/cemetery/update_owners_view";
                    $data['owner'][0] = $post_data;
                    $data['interred'] = $this->cemetery_model->get_interred_by_owner_id($id, $post_data['CEM_NBR']);
                    $this->load->view('master', $data);
				}
			}
			else
			{
				//$this->session->set_flashdata('error', validation_errors());
				//redirect('cemetery/update_owner/'.$post_data['CEM_REFNBR'].'/'.$post_data['CEM_NBR']);
                $data['cemeteries'] = $this->cemetery_model->get_cemeteries();
                $data['content'] = "pages/cemetery/update_owners_view";
                $data['owner'][0] = $post_data;
                $data['interred'] = $this->cemetery_model->get_interred_by_owner_id($id, $cem_id);
                $this->load->view('master', $data);
			}
		}
		else
		{
			// else there are no post so get the users data to update.
			$data['owner'] = $this->cemetery_model->get_owner_for_update($id, $cem_id);
			$data['interred'] = $this->cemetery_model->get_interred_by_owner_id($id, $cem_id);

			if (!empty($data['interred']))
			{
				// look up the cemetery name for each record
				for ($i=0; $i < count($data['interred']); $i++) 
				{ 
					$data['interred'][$i]['CEM_NAME'] = $this->cemetery_model->get_cemetery_by_id($data['interred'][$i]['CEM_NBR'])[0]['CEM_NAME'];
				}
			}

			$data['cemeteries'] = $this->cemetery_model->get_cemeteries();

			$data['content'] = "pages/cemetery/update_owners_view";
			$this->load->view('master', $data);
		}

	}

	public function print_owner()
	{
		//Load Model
		$this->load->model('cemetery_model');

		// Get the IDs to update 
		$id = $this->uri->segment(3);
		$cem_id = $this->uri->segment(4);

		// else there are no post so get the users data to update.
		$data['owner'] = $this->cemetery_model->get_owner_for_update($id, $cem_id);
		$data['interred'] = $this->cemetery_model->get_interred_by_owner_id($id, $cem_id);

		if (!empty($data['interred']))
		{
			// look up the cemetery name for each record
			for ($i=0; $i < count($data['interred']); $i++) 
			{ 
				$data['interred'][$i]['CEM_NAME'] = $this->cemetery_model->get_cemetery_by_id($data['interred'][$i]['CEM_NBR'])[0]['CEM_NAME'];
			}
		}

		// echo '<pre>';
		// print_r($data);
		// echo '</pre>';
		$data['cemetery'] = $this->cemetery_model->get_cemetery_by_id($cem_id);
		$data['content'] = "pages/cemetery/print_owner_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
interred
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Sets up the interred page.
============================================================================
*/

	public function interred()
	{
		//Load Model
		$this->load->model('cemetery_model');

		// get cemeteries for search drop down
		$data['cemeteries'] = $this->cemetery_model->get_cemeteries();

		$data['content'] = "pages/cemetery/interred_view";
		$this->load->view('master', $data);
	}
/*           
============================================================================
interred_search
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - Handles validation and passing the search string to the model.
============================================================================
*/

	public function interred_search()
	{
		// Load model
		$this->load->model('cemetery_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('cemetery', 'Cemetery', 'trim|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('middle_name', 'Middle Name', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[50]|xss_clean');
        $this->form_validation->set_rules('location', 'Location', 'trim|max_length[50]|xss_clean');

		// Run the form validation
		if ($this->form_validation->run() == true)
		{
			// if it passes then depending on what inputs have been filled out, search for record on that criteria.
			if (!empty($search_array))
			{
				$results = $this->cemetery_model->search_interred($search_array);
			}
			else
			{
				redirect('cemetery/interred');
			}
		}

		// get cemeteries for search drop down
		$data['cemeteries'] = $this->cemetery_model->get_cemeteries();
		// load the page with the results from the search.
		$data['interred'] = $results;

		//if (count($data['interred']) > 500){$data['interred'] = array_slice($data['interred'], 0, 500);}

		// look up the cemetery name for each record
		if (!empty($data['interred']))
		{
			for ($i=0; $i < count($data['interred']); $i++) 
			{ 
				$data['interred'][$i]['CEM_NAME'] = $this->cemetery_model->get_cemetery_by_id($data['interred'][$i]['CEM_NBR'])[0]['CEM_NAME'];
			}
		}

		$data['content'] = "pages/cemetery/interred_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_interred
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles adding the interred to the table.
============================================================================
*/

	public function add_interred()
	{
		//Load Model
		$this->load->model('cemetery_model');

		$post_data = $this->input->post(null, true);

		// if the post variables aren't empty
		if (!empty($post_data))
		{
			// explode the the id field to capture only the int
			$owner_id = explode(" ", $post_data['CEM_REFNBR']);
			$post_data['CEM_REFNBR'] = $owner_id[0];

			$this->form_validation->set_rules('CEM_NBR', 'Cemetery Reference', 'trim|required|xss_clean');
			$this->form_validation->set_rules('CEM_REFNBR', 'Owner Reference', 'trim|max_length[5]|required|xss_clean');
			$this->form_validation->set_rules('INT_FNAME', 'First Name', 'trim|max_length[30]|xss_clean|required');
			$this->form_validation->set_rules('INT_MNAME', 'Middle Name', 'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('INT_LNAME', 'Last Name', 'trim|max_length[20]|xss_clean|required');
			$this->form_validation->set_rules('INT_TITLE', 'Title', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('INT_DTE', 'Interred Date', 'trim|xss_clean');
			$this->form_validation->set_rules('INT_AGE', 'Age', 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('INT_TYP', 'Type', 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('INT_MARKER', 'Marker', 'trim|max_length[1]|xss_clean');
			$this->form_validation->set_rules('INT_LOC', 'Location', 'trim|max_length[5]|xss_clean');
			$this->form_validation->set_rules('INT_SPC_CMNT', 'Special Comment', 'trim|max_length[256]|xss_clean');
			$this->form_validation->set_rules('INT_CMNT', 'Comment', 'trim|max_length[256]|xss_clean');
			$this->form_validation->set_rules('INT_INCOMPDTE', 'Incomplete Date', 'trim|max_length[15]|xss_clean');

			// Run form validation
			if ($this->form_validation->run() == true)
			{	
				// Capitalize everything
				foreach ($post_data as $key => $value) 
				{
					$post_data[$key] = strtoupper($value);
				}

				// If it passes, prep id fields for db insert.
				$post_data['INT_REFNBR'] = $this->cemetery_model->get_next_val('CEM_INTERRED', 'INT_REFNBR');
				$post_data['CEM_NBR'] = (int)$post_data['CEM_NBR'];
				$post_data['CEM_REFNBR'] = (int)$post_data['CEM_REFNBR'];

				// attempt the insert of data.
				$results = $this->cemetery_model->insert_interred($post_data);

				if ($results)
				{
					// Return a success message.
					$this->session->set_flashdata('message', 'The interred was added to the table.');
					redirect('cemetery/update_interred/'.$post_data['CEM_NBR'].'/'.$post_data['CEM_REFNBR'].'/'.$post_data['INT_REFNBR']);
				}
				else
				{
					// Return an error message.
                    $data['errorMessage'] = 'The interred could not be added, please try again.';
                    $data['cemeteries'] = $this->cemetery_model->get_cemeteries();
                    $data['content'] = "pages/cemetery/add_interred_view";

                    $owner_id = $this->uri->segment(3);
                    $cem_id = $this->uri->segment(4);
                    if (!empty($owner_id)){$data['owner_id'] = $owner_id;}
                    if (!empty($cem_id)){$data['cem_id'] = $cem_id;}
                    // get the owner name
                    // if the owner and cemetery ids are not null
                    if (!empty($owner_id) && !empty($cem_id)) {
                        $ownerInfo = $this->cemetery_model->get_owner_by_id($owner_id,$cem_id);
                        if (isset($ownerInfo))
                        {
                            $ownerName = $ownerInfo[0]['CEM_FNAME'] . ' ' . $ownerInfo[0]['CEM_LNAME'];
                            $data['owner_name'] = $ownerName;
                        }
                    }

                    $this->load->view('master', $data);
				}
			}
			else
			{
				// If the validation failed, send the back to the page where the errors can be displayed.
				$data['cemeteries'] = $this->cemetery_model->get_cemeteries();
				$data['content'] = "pages/cemetery/add_interred_view";

                $owner_id = $this->uri->segment(3);
                $cem_id = $this->uri->segment(4);
                if (!empty($owner_id)){$data['owner_id'] = $owner_id;}
                if (!empty($cem_id)){$data['cem_id'] = $cem_id;}
                // get the owner name
                // if the owner and cemetery ids are not null
                if (!empty($owner_id) && !empty($cem_id)) {
                    $ownerInfo = $this->cemetery_model->get_owner_by_id($owner_id,$cem_id);
                    if (isset($ownerInfo))
                    {
                        $ownerName = $ownerInfo[0]['CEM_FNAME'] . ' ' . $ownerInfo[0]['CEM_LNAME'];
                        $data['owner_name'] = $ownerName;
                    }
                }

				$this->load->view('master', $data);
			}
			
		}
		else
		{
			// error: no data in post.
			$owner_id = $this->uri->segment(3);
			$cem_id = $this->uri->segment(4);
			if (!empty($owner_id)){$data['owner_id'] = $owner_id;}
			if (!empty($cem_id)){$data['cem_id'] = $cem_id;}

			// get the owner name
            // if the owner and cemetery ids are not null
			if (!empty($owner_id) && !empty($cem_id)) {
			    $ownerInfo = $this->cemetery_model->get_owner_by_id($owner_id,$cem_id);
			    if (isset($ownerInfo))
                {
                    $ownerName = $ownerInfo[0]['CEM_FNAME'] . ' ' . $ownerInfo[0]['CEM_LNAME'];
                    $data['owner_name'] = $ownerName;
                }
            }

			$data['cemeteries'] = $this->cemetery_model->get_cemeteries();
			$data['content'] = "pages/cemetery/add_interred_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
delete_interred
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles passing the id to delete to the model.
============================================================================
*/

	public function delete_interred() 
	{
		//Load Model
		$this->load->model('cemetery_model');

		// Grab the post variables.
		$post_data = $this->input->post(null, true);

		// Attempt the removeal query.
		$results = $this->cemetery_model->remove_interred($post_data);

		if ($results)
		{
			$this->session->set_flashdata('message', 'The interred record has been deleted.');
			redirect('cemetery/interred');
		}
		else
		{
			$this->session->set_flashdata('error', 'The interred record coud not be deleted.');
			redirect('cemetery/interred');
		}
	}

	public function update_interred()
	{
		//Load Model
		$this->load->model('cemetery_model');

		// There are a lot of records tied to interred records so we grab the interred reference and 
		$cemetery_number = $this->uri->segment(3);
		$cemetery_reference = $this->uri->segment(4);
		$interred_reference = $this->uri->segment(5);

		// grab the post variables.
		$post_data = $this->input->post(null, true);

		if (!empty($post_data))
		{
			// Validation
			$this->form_validation->set_rules('INT_FNAME', 'First Name', 'trim|max_length[30]|xss_clean|required');
			$this->form_validation->set_rules('INT_MNAME', 'Middle Name', 'trim|max_length[20]|xss_clean');
			$this->form_validation->set_rules('INT_LNAME', 'Last Name', 'trim|max_length[20]|xss_clean|required');
			$this->form_validation->set_rules('INT_TITLE', 'Title', 'trim|max_length[15]|xss_clean');
			$this->form_validation->set_rules('INT_DTE', 'Interred Date', 'trim|xss_clean');
			$this->form_validation->set_rules('INT_AGE', 'Age', 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('INT_TYP', 'Type', 'trim|max_length[10]|xss_clean');
			$this->form_validation->set_rules('INT_MARKER', 'Marker', 'trim|max_length[1]|xss_clean');
			$this->form_validation->set_rules('INT_LOC', 'Location', 'trim|max_length[5]|xss_clean');
			$this->form_validation->set_rules('INT_SPC_CMNT', 'Special Comment', 'trim|max_length[256]|xss_clean');
			$this->form_validation->set_rules('INT_CMNT', 'Comment', 'trim|max_length[256]|xss_clean');
			$this->form_validation->set_rules('INT_INCOMPDTE', 'Incomplete Date', 'trim|max_length[15]|xss_clean');

			if ($this->form_validation->run() == true)
			{
				// Capitalize everything
				foreach ($post_data as $key => $value) 
				{
					$post_data[$key] = strtoupper($value);
				}

				// Cast the id fields to int
				$post_data['CEM_NBR'] = (int)$post_data['CEM_NBR'];
				$post_data['CEM_REFNBR'] = (int)$post_data['CEM_REFNBR'];
				$post_data['INT_REFNBR'] = (int)$post_data['INT_REFNBR'];

				// Unset data that we don't need for the update
				unset($post_data['CEM_NAME']);


				// attempt the update 
				$results = $this->cemetery_model->update_interred($post_data);

				if ($results)
				{
					$this->session->set_flashdata('message', 'The interred record has been updated.');
					redirect('cemetery/update_interred/'.$post_data['CEM_NBR'].'/'.$post_data['CEM_REFNBR'].'/'.$post_data['INT_REFNBR']);
				}
				else
				{
					$this->session->set_flashdata('error', 'The interred record could not be updated.');
					redirect('cemetery/update_interred/'.$post_data['CEM_NBR'].'/'.$post_data['CEM_REFNBR'].'/'.$post_data['INT_REFNBR']);
				}	
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('cemetery/update_interred/'.$post_data['CEM_NBR'].'/'.$post_data['CEM_REFNBR'].'/'.$post_data['INT_REFNBR']);				
			}
		}
		else
		{
			// else the post data is emplty and we need to retireve  interred records for update.
			$data['interred'] = $this->cemetery_model->get_interred_for_update($cemetery_number, $cemetery_reference, $interred_reference);
			$cemetery_id = $data['interred'][0]['CEM_NBR'];
			$data['cemetery'] = $this->cemetery_model->get_cemetery_by_id($cemetery_id);
			$data['cemeteries'] = $this->cemetery_model->get_cemeteries();

			// remove the current cemetery from the list.
			foreach ($data['cemeteries'] as $key => $value) 
			{
				if ($data['cemetery'][0]['CEM_NBR'] == $value['CEM_NBR'])
				{
					unset($data['cemeteries'][$key]);
				}
			}

			$owner = $this->cemetery_model->get_owner_by_id($data['interred'][0]['CEM_REFNBR'], $cemetery_id);
			$data['owner_name'] = $owner[0]['CEM_FNAME'].' '.$owner[0]['CEM_LNAME'];
			
			$data['content'] = 'pages/cemetery/update_interred_view';

			$this->load->view('master', $data);
		}
	}

	public function print_interred()
	{
		//Load Model
		$this->load->model('cemetery_model');

		// There are a lot of records tied to interred records so we grab the interred reference and 
		$cemetery_reference = $this->uri->segment(3);
		$cemetery_number = $this->uri->segment(4);
		$interred_reference = $this->uri->segment(5);

		$data['interred'] = $this->cemetery_model->get_interred_for_update($cemetery_number, $cemetery_reference, $interred_reference);
		$data['cemetery'] = $this->cemetery_model->get_cemetery_by_id($cemetery_number);
		$data['owner'] = $this->cemetery_model->get_owner_by_id($data['interred'][0]['CEM_REFNBR'], $cemetery_number);

		$data['content'] = "pages/cemetery/print_interred_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
cemetery_name
----------------------------------------------------------------------------
NO PARAMS 
----------------------------------------------------------------------------
No return - gets all the cemeteries from that table.
============================================================================
*/

	public function cemetery_names()
	{
		//Load Model
		$this->load->model('cemetery_model');

		$data['cemeteries'] = $this->cemetery_model->get_cemeteries();
		$data['content'] = "pages/cemetery/cemetery_name_view";
		$this->load->view('master', $data);
	}


    /**
 * Returns cemeteries and the number of interred per cemetery
 * @param none
 * @return array
 */
    public function cemeteryNamesAndNumbers()
    {
        //Load Model
        $this->load->model('cemetery_model');

        $data['cemeteries'] = $this->cemetery_model->get_cemeteries_and_numbers();
        $data['content'] = "pages/cemetery/cemetery_name_view";
        $this->load->view('master', $data);
    }

    /**
     * Returns interred information per cemetery
     * @param cemetery id
     * @return array
     */
    public function cemeteryInterred()
    {
        //Load Model
        $this->load->model('cemetery_model');

        $id = $this->uri->segment(3);

        $data['interred'] = $this->cemetery_model->get_interred_per_cemetery($id);

        $data['cemetery'] = $this->cemetery_model->get_cemetery_by_id($id);

        $data['content'] = "pages/cemetery/cemetery_interred_view";
        $this->load->view('master', $data);
    }

/*           
============================================================================
add_cemetery_name
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles passing the data to the model for the insert.
============================================================================
*/

	public function add_cemetery_name()
	{
		//Load Model
		$this->load->model('cemetery_model');

		// Get the post data.
		$post_data = $this->input->post(null, true);

		if (!empty($post_data))
		{
			// Validation
			$this->form_validation->set_rules('CEM_NAME', 'Cemetery Name', 'trim|max_length[30]|xss_clean|required');
			if ($this->form_validation->run() == true)
			{
				$post_data['CEM_NBR'] = $this->cemetery_model->get_next_val('CEM_CEMETERY', 'CEM_NBR');
				$post_data['CEM_NAME'] = strtoupper($post_data['CEM_NAME']);

				// Run the insert query
				$results = $this->cemetery_model->insert_cemetery($post_data);
		
				if ($results)
				{	
					$this->session->set_flashdata('message', 'The cemetery has been added.');
					redirect('cemetery/cemetery_names');
				}
				else
				{
					$this->session->set_flashdata('error', 'The cemetery name could not be added.');
					redirect('cemetery/cemetery_names');
				}
			}
			else
			{
				$data['content'] = "pages/cemetery/add_cemetery_name_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			// error: no data in post.
			$data['content'] = "pages/cemetery/add_cemetery_name_view";
			$this->load->view('master', $data);
		}
		
	}

/*           
============================================================================
delete_cemetery_name
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles passing the id to delete to the model
============================================================================
*/
	public function delete_cemetery_name()
	{
		//Load Model
		$this->load->model('cemetery_model');

		$id = $this->input->post('CEM_NBR');

		$results = $this->cemetery_model->remove_cemetery($id);

		if ($results)
		{
			$this->session->set_flashdata('message', 'The cemetery has been removed.');
			redirect('cemetery/cemetery_names');
		}
		else
		{ 
			$this->session->set_flashdata('error', 'The cemetery could not be deleted.');
			redirect('cemetery/cemetery_names');
		}

	}

    /**
     * Searches for cemetery plot owners by last name.
     *
     * returns array
     */
    public function ajax_get_owners()
    {
        //Load Model
        $this->load->model('cemetery_model');

        $firstName = $this->input->post('firstName');
        $lastName = $this->input->post('lastName');

        $results = $this->cemetery_model->get_like_owners($firstName, $lastName);

        echo json_encode($results);
    }
}

/* End of file cemetery.php */
/* Location: .//C/wamp/www/HRMApplications/app/controllers/cemetery.php */