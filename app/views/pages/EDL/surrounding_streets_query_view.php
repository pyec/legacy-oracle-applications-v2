<div class="row">
	<div class="col-md-12">
		<h2>Surrounding Streets Query</h2>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<form action="<?php echo base_url(); ?>EDL/surrounding_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="file_number">File #</label>
					<input type="text" name="file_number" class="form-control" title="FIle Number" value="<?php echo set_value('file_number') ?>" tabindex="1" placeholder="FIle Number" />
				</div>
				<div class="col-sm-3">
					<label for="street_number">Street #</label>
					<input type="text" name="street_number" class="form-control" title="Street Number" value="<?php echo set_value('street_number') ?>" tabindex="2" placeholder="Street Number" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="edl_files_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
<div class="row">
	<div class="col-md-12 pull-right">
		<?php if (!empty($streets)): ?>
			<?php if (count($streets) == 500): ?>
				<h2 class="count"><?php echo count($streets) ?> records or more</h2>
			<?php else: ?>
				<h2 class="count"><?php echo count($streets) ?> records</h2>
			<?php endif ?>
		<?php endif ?>
		<a href="<?php echo base_url(); ?>EDL/add_plan_index" class="btn btn-success pull-right">Add Record</a>
	</div>
	<div class="col-sm-12">
		<?php if (!empty($streets) && count($streets) == 500): ?>
			<P class="">Refine your search for more specific results.</P>			
		<?php endif ?>
	</div>
</div>
<?php if (!empty($streets)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>File ID</th>
				<th>Street #</th>
				<th>Description</th>
			</thead>
			<tbody>
					<?php foreach ($streets as $street): ?>
						<tr>
							<td><a href="<?php echo base_url(); ?>EDL/update_surrounding_streets/<?= $street['FILE_ID'] ?>"><?= $street['FILE_ID'] ?></a></td>
							<td><?php echo $street['STREET_NUM'] ?></td>
								<td><?php echo ucfirst(strtolower($street['NOTES'])); ?></td>
						</tr>	
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>