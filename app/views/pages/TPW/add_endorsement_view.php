<div class="row">
	<div class="col-md-12">
		<h2>Add Endorsement</h2>
		<p class="lead">This form will allow you to update an edorsement.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?= $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?= $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?= validation_errors(); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?= base_url(); ?>TPW/add_endorsement" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="col-sm-2">
					<label for="ENDORSEMENT_CDE">Endorsement Code</label>
					<input type="text" name="ENDORSEMENT_CDE" class="form-control" title="Endorsement Code" maxlength="3" value="<?php echo set_value('ENDORSEMENT_CDE'); ?>" />
				</div>

				<div class="col-sm-6">
					<label for="ENDORSEMENT_DSC">Description</label>
					<input type="text" name="ENDORSEMENT_DSC" class="form-control" title="Description" maxlength="70" value="<?php echo set_value('ENDORSEMENT_DSC'); ?>" />
				</div>
			
				<div class="col-sm-12"> 
					<input type="submit" class="btn btn-md btn-primary update_course pull-right no_label_fix" value="Add Endorsement" />
					<a href="<?= base_url(); ?>TPW/endorsement" class="btn btn-md btn-default return">Back to Endorsements</a>
				</div>
			</div>
		</div>
	</form>
</div>