<div class="row">
	<div class="col-md-6">
		<h2>Cemetery Names</h2>
		<p class="lead">Information on the Cemeteries of HRM</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($cemeteries)): ?>
			<h2><?php echo count($cemeteries) ?> Cemeteries</h2>
		<?php else: ?>
			<h2>No Cemeteries Found</h2>
		<?php endif ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Cemetery Name</th>
                <th>Number Interred</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
				<?php foreach ($cemeteries as $cem): ?>
					<tr>
                        <td>
                            <a href="<?php echo base_url(); ?>cemetery/cemeteryInterred/<?php echo $cem['CEM_NBR'] ?>">
                                <?php echo $cem['CEM_NAME'] ?>
                            </a>
                        </td>
                        <td><?php echo $cem['NUM_INTERRED'] ?></td>
						<td>
							<form action="<?php echo base_url(); ?>cemetery/delete_cemetery_name" method="post">
								<button type="submit" class="delete_cemetery_name btn btn-sm btn-danger" ><i class="fa fa-minus-square"></i></button>
								<input type="hidden" name="CEM_NBR" value="<?= $cem['CEM_NBR'] ?>" />
							</form>
						</td>
					</tr>	
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin-bottom: 2em; margin-top: 1em">
    <div class="col-md-12">
        <a href="<?php echo base_url(); ?>cemetery/add_cemetery_name" class="btn btn-success pull-right">Add Cemetery</a>
    </div>
</div>