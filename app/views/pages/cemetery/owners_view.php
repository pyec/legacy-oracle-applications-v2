<!-- <div class="row">
	<div class="col-md-6">
		<h2>Owners</h2>
	</div>
	<div class="col-md-6">
		<a href="<?php echo base_url(); ?>cemetery/add_owner" class="btn btn-success pull-right">Add Owner</a>
	</div>
</div> -->
<div class="col-6">
	<h2>Owners</h2>
	<!-- <div class="row">
		<p class="lead">Information on the owners of cemetery plots in HRM.</p>
	</div>
	<div class="col-6">
		<p>Select an owner to view more information about them.</p>
	</div> -->
</div>
 <div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>


	<!-- <legend></legend> -->
<form action="<?php echo base_url(); ?>cemetery/owner_search" method="POST">
	
	<div class="form-group">
		<div class="row">
			<div class="col-sm-3">
				<label for="cemetery">Cemetery</label>
				<select  name="cemetery" class="form-control" title="Cemetery" value="<?php echo set_select('cemetery') ?>" tabindex="1">
					<option value="">All</option>
					<?php foreach ($cemeteries as $cem): ?>
						<option value="<?= $cem['CEM_NBR'] ?>" <?php echo set_select('cemetery', $cem['CEM_NBR']); ?> ><?php echo ucwords(strtolower($cem['CEM_NAME'])) ?></option>
					<?php endforeach ?>
				</select>
			</div>

			<div class="col-sm-3">
				<label for="last_name">Last Name</label>
				<input type="text" name="last_name" class="form-control" title="Last Name" value="<?php echo set_value('last_name') ?>" tabindex="4" placeholder="Last Name" />
			</div>

			<div class="col-sm-3">
				<label for="first_name">First Name</label>
				<input type="text" name="first_name" class="form-control" title="First Name" value="<?php echo set_value('first_name') ?>" tabindex="2" placeholder="First Name" />
			</div>
		
			<div class="col-sm-3">
				<label for="middle_name">Middle Name</label>
				<input type="text" name="middle_name" class="form-control" title="Middle Name" value="<?php echo set_value('middle_name') ?>" tabindex="3" placeholder="Middle Name" />
			</div>
		</div>
    </div>
    <div class="form-group">
		<div class="row">
            <div class="col-sm-3">
                <label for="location">Location</label>
                <input type="text" name="location" class="form-control" title="Location" value="<?php echo set_value('location') ?>" tabindex="3" placeholder="Location"/>
            </div>
        </div>
    </div>
    <div class="form-group">
    <div class="row">
			<div class="col-sm-12"> 
				<button type="submit" class="btn btn-primary btn-md pull-right no_label_fix phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
				<button type="reset" class="btn btn-default btn-md pull-right no_label_fix" tabindex="9" onclick="cem_owners_form_reset()">Clear</button>	
			</div>	
     </div>
    </div>
</form>
	


<div id="dvCount" class="row">
	<div class="col-md-12">
		<?php if (!empty($owners)): ?>
			<h2 class="count"><?php echo count($owners) ?> Result<?= count($owners) == 1 ? '' : 's' ?></h2>
		<?php endif ?>
		<a href="<?php echo base_url(); ?>cemetery/add_owner" class="btn btn-success pull-right">Add Owner</a>
	</div>
	<div class="col-sm-12">
		<?php if (!empty($owners) && count($owners) >= 500): ?>
			<P class="">Refine your search for more specific results.</P>			
		<?php endif ?>
	</div>
</div>

<?php if (!empty($owners)): ?>

<div id="dvResults" class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover no_label_fix">
			<thead>
				<th>Cemetery</th>
				<!-- <th>Last Name</th>
				<th>First Name</th>
				<th>Middle Name</th> -->
				<th>Name</th>
				<th>Graves</th>
                <th>Location</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
				
				<?php foreach ($owners as $owner): ?>
				 	<tr>
				 		<td><?php echo ucwords(strtolower($owner['CEM_NAME'])) ?></td>
				 		<!-- <td><a href="<?php echo base_url(); ?>cemetery/update_owner/<?= $owner['CEM_REFNBR'] ?>/<?php echo $owner['CEM_NBR'] ?>"><?php echo $owner['CEM_LNAME'] ?></a></td>
				 		<td><?php echo $owner['CEM_FNAME'] ?></td>
				 		<td><?php echo $owner['CEM_MNAME'] ?></td> -->
				 		<td>
				 			<a href="<?php echo base_url(); ?>cemetery/update_owner/<?= $owner['CEM_REFNBR'] ?>/<?php echo $owner['CEM_NBR'] ?>">
				 				<?php echo ucwords(strtolower($owner['CEM_LNAME'].', '.$owner['CEM_FNAME'].' '.$owner['CEM_MNAME'])) ?>
				 			</a>
				 		</td>
				 		<td><?php echo $owner['CEM_NBRGR'] ?></td>
                        <td><?php echo $owner['CEM_SCTN'] ?></td>
				 		<td>
				 		<form action="<?php echo base_url(); ?>cemetery/delete_owner" method="POST">
							<!-- <button type="submit" class="btn btn-sm btn-danger delete_owners"><i class="fa fa-minus-square"></i></button> -->
							<button type="submit" class="btn btn-sm btn-danger delete_owners">Delete <i class="fa fa-trash"></i></button>
				 			<input type="hidden" name="CEM_REFNBR" value="<?php echo $owner['CEM_REFNBR'] ?>" />
				 		</form>
				 		</td>
				 	</tr>
				 <?php endforeach ?>

			</tbody>
		</table>
	</div>
</div>
<?php endif ?>