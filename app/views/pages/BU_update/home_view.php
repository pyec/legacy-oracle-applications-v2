<!-- Display messages back to the user if there set. -->
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<!-- Title of app -->
<div class="row">
	<div class="col-md-12">
		<h2>Business Unit Update</h2>
		<p class="lead">Use this form to identify whether a business unit is active or not.</p>		
	</div>
</div>

<form action="<?php echo base_url(); ?>BU_update/add_BU" method="POST">
	<div class="form-group">
		<div class="row">
			<div class="col-md-4">
				<label for="add_business_unit_name">Add Business Unit</label>
				<input type="text" name="add_business_unit_name" class="form-control" title="Business Unit Name" placeholder="Business Unit Name" />
			</div>
			<div class="col-md-1 no_label_fix">
				<input style="" type="submit" class="btn btn-md btn-success" title="Add Business Unit" value="Add Business Unit" />
			</div>
		</div>
	</div>
	
</form>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Unit name</th>
				<th>Active?</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
				<?php foreach ($business_units as $unit): ?>
					<tr>
						<td><a href="<?php echo base_url(); ?>BU_update/update_BU/<?php echo $unit['BUSUNIT_CDE'] ?>"><?php echo $unit['BUSUNIT_DSC'] ?></a></td>
						<td><?php echo $unit['CURRENT_FLG'] ?></td>
						<td>
							<form action="<?php echo base_url(); ?>BU_update/remove_BU" method="post">
								<button type="submit" class="delete_business_unit btn btn-sm btn-danger" >Delete</button>
								<input type="hidden" name="BUSUNIT_CDE" value="<?= $unit['BUSUNIT_CDE'] ?>" />
							</form>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>

<!-- labels for 2 columns -->
<!-- <div class="row">
	<div class="col-md-4 col-md-offset-4">
		<label for="BU_textbox">Business Unit</label>
	</div>
	<div class="col-md-4 col-md-offset-8">
		<label for="BU_checkbox">Active?</label>
	</div> -->
</div>

<!-- for all of the business units echo it's title into one col and it's active flag in the other. -->
<!-- <form action="<?php echo base_url(); ?>BU_update/update_BU" method="POST" >
	<div class="row delete_bu_container">
		<?php foreach ($business_units as $unit): ?>
			<div class="col-md-4 col-md-offset-4">
				<div class="form-group">
					<input type="text" name="<?php echo $unit['BUSUNIT_CDE'] ?>" class="form-control input-md" value="<?php echo $unit['BUSUNIT_DSC'] ?>" />
				</div>
			</div>
			<div class="col-md-4">
				<div style="margin-top: 5px;" class="form-group">
					<input type="checkbox" name="<?php echo $unit['BUSUNIT_CDE'].'_checkbox'; ?>" class="BU_checkbox" <?php if($unit['CURRENT_FLG'] == "Y"){ echo "checked";}; ?> />
				</div>
			</div>
		<?php endforeach ?>
	</div> -->

<!-- Update btn wrapped by legends for visual separation.  -->
<!-- 	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<legend></legend>
		</div>
		<div class="col-md-4 col-md-offset-4">
			<input style="margin-bottom: 10px;" type="submit" class="btn btn-warning" value="Update Business Units" />
		</div>
		<div class="col-md-4 col-md-offset-4">
			<legend style="margin-top: -20px;"></legend>
		</div>
	</div>
</form> -->

<!-- Add business unit form. -->

