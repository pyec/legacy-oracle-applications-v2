<div class="row">
	<div class="col-md-12">
		<h2>Traffic Survey Request</h2>
		<p class="lead">Update</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?php echo base_url(); ?>traffic/update_TSR" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="REQ_NUM">Record #</label>
						<input type="text" name="REQ_NUM" class="form-control" title="Record" maxlength="10" value="<?= $survey['REQ_NUM'] ?>" readonly />
					</div>
					<div class="col-sm-2">
						<label for="CREATE_DATE">Created On</label>
						<input type="text" name="CREATE_DATE" class="form-control date_picker" title="Created On" readonly value="<?= $survey['CREATE_DATE'] ?>" />
					</div>
				</div>
			</div>

			<legend>Intersection</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="MAIN_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="MAIN_CODE" class="form-control no_label_fix main_street_code" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">Street Name</label>
						<input type="text"  class="form-control main_street_name" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">Type</label>
						<input type="text" class="form-control main_street_type" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">Community</label>
						<input type="text" class="form-control main_community" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STRA_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STRA_CODE" class="form-control A_street_code" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">At</label>
						<input type="text"  class="form-control A_street_name" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix A_street_type" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix A_community" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STRA2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STRA2_CODE" class="form-control A2_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix A2_street_name" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix A2_street_type" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix A2_community" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STRA3_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STRA3_CODE" class="form-control A3_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix A3_street_name" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix A3_street_type" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix A3_community" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STRA4_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STRA4_CODE" class="form-control A4_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix A4_street_name" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix A4_street_type" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix A4_community" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>

	<legend>Mid Block</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STRB_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STRB_CODE" class="form-control no_label_fix B_street_code" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix B_street_name" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix B_street_type" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix B_community" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STRB2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STRB2_CODE" class="form-control B2_street_code" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">Between</label>
						<input type="text"  class="form-control B2_street_name" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix B2_street_type" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix B2_community" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>

			<legend>Details</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="X_COORD">X coord</label>
						<input type="text" name="X_COORD" class="form-control" title="X Coordinate" maxlength="10" value="<?= $survey['X_COORD'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="Y_COORD">Y Coord</label>
						<input type="text" name="Y_COORD" class="form-control" title="Y Coordinate" maxlength="10" value="<?= $survey['Y_COORD'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="REQ_DATE">Request Date</label>
						<input type="text" name="REQ_DATE" class="date_picker form-control" title="Request Date" readonly value="<?= $survey['REQ_DATE'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="COMP_DATE">Completion Date</label>
						<input type="text" name="COMP_DATE" class="date_picker form-control" title="Completion Date" readonly value="<?= $survey['COMP_DATE'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="STR_REG">Region</label>
						<select name="STR_REG" class="form-control" title="Region" >
							<option value="E">Eastern</option>
							<option value="C">Central</option>
							<option value="W">Western</option>
							<?php if (!empty($STR_REG)): ?>
								<option value="<?= $survey['STR_REG'] ?>"><?= $survey['STR_REG'] ?></option>
							<?php endif ?>
						</select>

					</div>
					<div class="col-sm-2">
						<label for="PRI_RANK">Priority</label>
						<input type="text" name="PRI_RANK" class="form-control" title="Priority" maxlength="1" value="<?= $survey['PRI_RANK'] ?>" />
					</div>
				</div>
			</div>

			

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<label for="REQ_PERSON">Requested By</label>
						<input type="text" name="REQ_PERSON" class="form-control" title="Requested By" maxlength="10" value="<?= $survey['REQ_PERSON'] ?>" />
					</div>
					<div class="col-sm-6">
						<label for="FOR_PERSON">Forwarded By</label>
						<input type="text" name="FOR_PERSON" class="form-control" title="Forwarded By" maxlength="10" value="<?= $survey['FOR_PERSON'] ?>" />
					</div>
				</div>
			</div>

			

			<div class="form-group">
				<div class="row">
					<div class="col-sm-8">
						<label for="COMMENTS">Comments</label>
						<textarea class="form-control" title="Comments"  name="COMMENTS" maxlength="200"><?= $survey['COMMENTS'] ?></textarea>
					</div>
				</div>
			</div>

			<legend>Requested Surveys</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<label for="CONT_WAR">4 Way Stop Control Warrant</label>
						<input <?= $checked = ($survey['CONT_WAR'] == 'T' ? 'checked':''); ?> type="checkbox" name="CONT_WAR" class="requested_surveys" /><br />
						
						<label for="LTHL">Left Turning Holding Lane</label>
						<input <?= $checked = ($survey['LTHL'] == 'T' ? 'checked':''); ?> type="checkbox" name="LTHL" class="requested_surveys" /><br />
						
						<label for="SIG_WAR">Traffic Signal Warrant</label>
						<input <?= $checked = ($survey['SIG_WAR'] == 'T' ? 'checked':''); ?> type="checkbox" name="SIG_WAR" class="requested_surveys" /><br />
						
						<label for="LTIS">Left Turn Investigation</label>
						<input <?= $checked = ($survey['LTIS'] == 'T' ? 'checked':''); ?> type="checkbox" name="LTIS" class="requested_surveys" /><br />

						<label for="CLASS">Classification</label>
						<input <?= $checked = ($survey['CLASS'] == 'T' ? 'checked':''); ?> type="checkbox" name="CLASS" class="requested_surveys" /><br />
						
						<label for="SVS">Speed & Volume</label>
						<input <?= $checked = ($survey['SVS'] == 'T' ? 'checked':''); ?> type="checkbox" name="SVS" class="requested_surveys" /><br />
					</div>

					<div class="col-sm-6">
						<label for="GAPS">GAPS</label>
						<input <?= $checked = ($survey['GAPS'] == 'T' ? 'checked':''); ?> type="checkbox" name="GAPS" class="requested_surveys" /> <br />

						<label for="PEDS">Pedestrian</label>
						<input <?= $checked = ($survey['PEDS'] == 'T' ? 'checked':''); ?> type="checkbox" name="PEDS" class="requested_surveys" /><br />

						<label for="PEDS">Basic 1 & 2 Way Traffic</label>
						<input <?= $checked = ($survey['PEDS'] == 'T' ? 'checked':''); ?> type="checkbox" name="PEDS" class="requested_surveys" /><br />
						
						<label for="B2WC">Basic 2 Way Traffic</label>
						<input <?= $checked = ($survey['B2WC'] == 'T' ? 'checked':''); ?> type="checkbox" name="B2WC" class="requested_surveys" /><br />
						
						<label for="RSSS">Spot Speed</label>
						<input <?= $checked = ($survey['RSSS'] == 'T' ? 'checked':''); ?> type="checkbox" name="RSSS" class="requested_surveys" /><br />

						<label for="SIG_INT">Half Signal Intersection</label>
						<input <?= $checked = ($survey['SIG_INT'] == 'T' ? 'checked':''); ?> type="checkbox" name="SIG_INT" class="requested_surveys" /><br />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-primary btn-md pull-right" value="Update Survey" />
					</div>
				</div>
			</div>
	</form>

			<legend><?= $attachment_count ?> - Attachment</legend>
			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-6">
						<?php echo form_open_multipart('traffic/do_upload');?>

						<input type="file" name="userfile" size="20" class="form-control" />

                        <br /><br />
						<input type="hidden" name="REC_CODE" value="<?= $survey['REQ_NUM'] ?>" />
						<input type="hidden" name="FORM" value="TSR" />

					</div>
					<div class="col-sm-2">
						<input type="submit" value="Upload" class="btn btn-md btn-primary" />
						</form>
					</div>
					<div class="col-sm-4">
						<input type="button" class="btn btn-md btn-primary open_attachments pull-right" value="Open attachments" id="TSR" />
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<a href="<?php echo base_url(); ?>traffic/delete_attachments/TSR/<?= $survey['REQ_NUM'] ?>" class="btn btn-md btn-default pull-right delete_attachments">Delete Attachments</a>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<a href="<?php echo base_url(); ?>traffic/TSR" class="btn btn-default btn-md">Back to Traffic Surveys</a>
					</div>
				</div>
			</div>
		</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Street Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<div class="modal_search">
								<label for="search_box">Street Name</label>
								<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
								<p class="text-muted">(Select the name of the record you would like to use.)</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Street</th>
					</thead>
					<tbody class="modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->