<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<?php
	$is_admin = false;
	$groups = $this->session->userdata('groups');

	if (!empty($groups))
	{
		foreach ($groups as $group) 
		{
			if ($group == 'AppHRMAppsEmployeeDirectoryAdmin' || $group == 'AppHRMAppsAdmin')
			{
				$is_admin = true;
			}
		}
	}
 ?>

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active" id="phonebook_tab"><a href="#phonebook" aria-controls="phonebook" role="tab" data-toggle="tab">Employee Directory</a></li>
    <li role="presentation" id="assets_tab"><a href="#assets" aria-controls="assets" role="tab" data-toggle="tab">HRM Assets</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="phonebook">
    	<div class="row">
			<div class="col-md-12">
				<h2>Employee Directory</h2>
				<p class="lead">Find a colleague by filling in some known information and clicking search.</p>
			</div>
		</div>

			<form action="<?= base_url(); ?>phonebook/search" method="POST">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-2">
							<label for="first_name">Name</label>
							<input type="text" name="first_name" class="form-control" title="First Name" value="<?php echo set_value('first_name') ?>" tabindex="1" placeholder="First Name" />
						</div>
						<div class="col-sm-2">
							<input type="text" name="last_name" class="form-control no_label_fix" title="Last Name" value="<?php echo set_value('last_name') ?>" tabindex="2" placeholder="Last Name" />
						</div>
						<div class="col-sm-4">
							<label for="position">Position</label>
							<input type="text" name="position" class="form-control" title="Position" value="<?php echo set_value('position') ?>" tabindex="3" placeholder="Position" />
						</div>
						<div class="col-sm-4">
							<label for="section">Business Unit</label>
							<select name="section" class="form-control" title="section" tabindex="4" placeholder="Section">
								<option value="0">Choose..</option>
								<?php foreach ($departments as $department): ?>
									<option vlaue="<?php echo $department ?>" <?php echo set_select('section', $department); ?>><?php echo $department ?></option>
								<?php endforeach ?>
		 					</select>
						</div>
						
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="email">Email</label>
							<input type="text" name="email" class="form-control" title="Email" value="<?php echo set_value('email') ?>" tabindex="5" placeholder="Email" />
						</div>
						<div class="col-sm-4">
							<label for="office_phone">Office Phone</label>
							<input type="text" name="office_phone" class="form-control" title="Office Phone" value="<?php echo set_value('office_phone') ?>" tabindex="6" placeholder="Office Phone" />
						</div>
						<div class="col-sm-4">
							<label for="mobile_phone">Mobile Phone</label>
							<input type="text" name="mobile_phone" class="form-control" title="Mobile Phone" value="<?php echo set_value('mobile_phone') ?>" tabindex="7" placeholder="Mobile Phone" />
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-12"> 
							<button type="submit" class="btn btn-primary btn-md no_label_fix pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
							<button type="reset" class="btn btn-default btn-md no_label_fix pull-right" tabindex="9" onclick="phonebook_form_reset()">Clear</button>	
						</div>	
					</div>
				</div>
			</form>
			<div class="alert alert-warning"><p class="text-center">If information is found to be incorrect please contact the <a href="mailto:ictsd@halifax.ca?subject=Please update this AD Record&body=Describe the record that requires updating and include the email address. ">ICT Service Desk</a> to request a correction of the Active Directory data for that record.</p></div>
			<legend class="phonebook_legend"></legend>


		<?php if (!empty($staff)): ?>
		<div class="row">
			<div class="col-sm-12">
				<h3 class="contact_count"><?php if (!empty($staff)){echo count($staff);} ?>&nbsp; Contacts</h3>
				<!-- <a href="<?php echo base_url(); ?>phonebook/add_staff" class="btn btn-success pull-right" tabindex="10" disabled><i class="fa fa-plus-circle">&nbsp;</i>Add</a> -->
			</div>
		</div>

		<div class="row no_label_fix">
			<div class="col-md-12">
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<th>Name</th>
						<th>Position</th>
						<th>Email</th>
						<th>Section</th>
						<th>Location</th>
						<th>Work Phone</th>
						<th>Cell</th>
					</thead>
					<tbody>
							<?php if ($staff == false): ?>
								<p>No records could be found, try being more specific.</p>
							<?php else: ?>
								<?php foreach ($staff as $value): ?>
									<tr>
										<td><?php if(!empty($value['name'])) {if(!empty($value['samaccountname'])){echo "<a href='staff/".$value['samaccountname']."'>".$value['name']."</a>";}}else {echo "";} ?></td>
										<td><?php if(!empty($value['title'])) {echo $value['title'];}else {echo "";} ?></td>
										<td><?php if(!empty($value['mail'])) {echo '<a href="mailto:'.$value['mail'].'" target="_top">'.$value['mail'].'</a>';}else {echo "";} ?></td>
										<td><?php if(!empty($value['department'])) {echo $value['department'];}else {echo "";} ?></td>
										<td><?php if(!empty($value['physicaldeliveryofficename'])) {echo $value['physicaldeliveryofficename'];}else {echo "";} ?></td>
										<td><?php if(!empty($value['telephonenumber'])) {echo $value['telephonenumber'];}else {echo "";} ?></td>
										<td><?php if(!empty($value['mobile'])) {echo $value['mobile'];}else {echo "";} ?></td>
									</tr>
								<?php endforeach ?>
							<?php endif ?>
					</tbody>
				</table>
			</div>
		</div>
		<?php endif ?>
    </div>
    <div role="tabpanel" class="tab-pane" id="assets">
    	<div class="row">
			<div class="col-md-12">
				<h2>Assets</h2>
				<p class="lead">Find an HRM asset by filling in some known information and clicking search.</p>
				<p>Click a assets name to view more details about it.</p>
			</div>
		</div>
		<form action="<?php echo base_url(); ?>phonebook/search_assets" method="POST">
				<div class="form-group">
					<div class="row">
						<div class="col-sm-4">
							<label for="name">Asset Name</label>
							<input type="text" name="name" class="form-control" title="Asset Name" value="<?php echo set_value('name') ?>" tabindex="1" placeholder="Asset Name" />
						</div>
						<div class="col-sm-4">
							<label for="business_unit">Business Unit</label>
							<select name="business_unit" class="form-control" title="Business Unit" tabindex="2" placeholder="Business Unit">
								<option value="0">Choose..</option>
								<?php foreach ($departments as $department): ?>
									<option vlaue="<?php echo $department ?>" <?php echo set_select('business_unit', $department); ?>><?php echo $department ?></option>
								<?php endforeach ?>
		 					</select>
						</div>
						<div class="col-sm-4">
							<label for="phone_number">Phone Number</label>
							<input type="text" name="phone_number" class="form-control" title="Phone Number" value="<?php echo set_value('phone_number') ?>" tabindex="3" placeholder="Phone Number" />
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-2">
							<label for="civic_number">Civic Number</label>
							<input type="text" name="civic_number" class="form-control" title="Civic Number" value="<?php echo set_value('civic_number') ?>" tabindex="4" placeholder="Civic Number" />
						</div>
						<div class="col-sm-4">
							<label for="street_name">Street Name</label>
							<input type="text" name="street_name" class="form-control" title="Street Name" value="<?php echo set_value('street_name') ?>" tabindex="5" placeholder="Street Name" />
						</div>
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-sm-12"> 
							<button type="submit" class="btn btn-primary btn-md no_label_fix pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
							<button type="reset" class="btn btn-default btn-md no_label_fix pull-right" tabindex="9" onclick="asset_form_reset()">Clear</button>	
						</div>	
					</div>
				</div>
			</form>
			<legend class="phonebook_legend"></legend>

			<?php if ($is_admin): ?>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-12">
							<a href="<?php echo base_url(); ?>phonebook/add_asset" class="btn btn-md btn-success pull-right" >Add Assets</a>
						</div>
					</div>
				</div>	
			<?php endif ?>

			<?php if (!empty($assets)): ?>
			<div class="row">
				<div class="col-sm-12">
					<h3 class="contact_count"><?php if (!empty($assets)){echo count($assets);} ?>&nbsp; Asset</h3>
					<!-- <a href="<?php echo base_url(); ?>phonebook/add_staff" class="btn btn-success pull-right" tabindex="10" disabled><i class="fa fa-plus-circle">&nbsp;</i>Add</a> -->
				</div>
			</div>

			<div class="row no_label_fix">
				<div class="col-md-12">
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<th>Name</th>
							<th>Business Unit</th>
							<th>Phone Number</th>
							<th>Civic Number</th>
							<th>Street Name</th>
						</thead>
						<tbody>
								<?php if ($assets == false): ?>
									<p>No records could be found, try being more specific.</p>
								<?php else: ?>
									<?php foreach ($assets as $value): ?>
										<tr>
											<?php if ($is_admin): ?>
												<td><a href="<?php echo base_url(); ?>phonebook/update_asset/<?= $value['id'] ?>" class=""><?= $value['name'] ?></a></td>
											<?php else: ?>
												<td><a href="<?php echo base_url(); ?>phonebook/view_asset/<?= $value['id'] ?>" class=""><?= $value['name'] ?></a></td>
											<?php endif ?>
											<td><?= $value['business_unit'] ?></td>
											<td><?= $value['phone_number'] ?></td>
											<td><?= $value['civic_number'] ?></td>
											<td><?= $value['street_name'] ?></td>
										</tr>
									<?php endforeach ?>
								<?php endif ?>
						</tbody>
					</table>
				</div>
			</div>
			<?php endif ?>
    </div>
  </div>
</div>


