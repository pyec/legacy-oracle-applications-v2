<div class="row">
	<div class="col-md-6">
		<h2>Sources</h2>
	</div>
</div>

<!-- Display messages back to the user if there set. -->
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<div class="col-sm-12">
	<form action="<?php echo base_url(); ?>MCT/source_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="source_name">Source Name</label>
					<input type="text" name="source_name" class="form-control" title="Source Name" value="<?php echo set_value('source_name') ?>" tabindex="1" placeholder="Source Name" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="mct_call_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<!-- <div class="row"> -->
				<a href="<?php echo base_url(); ?>MCT/add_source_type" class="btn btn-success btn-md pull-right">Add Source</a>	
			<!-- </div> -->
		</div>
	</div>
</div>

<?php if (!empty($source_types)): ?>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<th>Source</th>
					<!-- <th>Delete</th> -->
				</thead>
				<tbody class="tbody">
					<?php foreach ($source_types as $type): ?>
						<tr>
							<td><?php echo ucwords(strtolower($type['MCT_SOURCE_DESC'])) ?></td>
							<!-- <td>
								<form action="<?php echo base_url(); ?>MCT/delete_source" method="post" onsubmit="">
									<button type="submit" href="" class="delete_source_type btn btn-danger btn-sm">Delete</button>
									<input type="hidden" name="MCT_SOURCE_DESC" value="<?php echo $type['MCT_SOURCE_DESC'] ?>" />
								</form>
							</td> -->
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
		</div>
	</div>
<?php endif ?>