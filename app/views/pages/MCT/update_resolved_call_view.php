<div class="row">
	<div class="col-md-6">
		<h2>Update Resolved Correspondence</h2>
	</div>
	<div class="col-sm-6">	
		<a class="btn btn-sm btn-primary pull-right no_label_fix" style="margin-left: 5px;" href="mailto:?subject=Mayor%20Correspondence%20Tracking&body=<?= $email_body ?>">Email Record</a>
		<a class="btn btn-sm btn-primary pull-right no_label_fix" target="_blank" href="<?php echo base_url(); ?>MCT/print_view/<?= $call[0]['MCT_NBR'] ?>/Y">Print</a>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<!-- <div class="row"> -->
	<div class="col=md-12">
		<form action="<?php echo base_url(); ?>MCT/update_resolved/<?= $call[0]['MCT_NBR'] ?>" method="post">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="MCT_NBR">Call ID</label>
						<input type="text" name="MCT_NBR"  class="form-control" title="First Name" maxlength="35" value="<?= $call[0]['MCT_NBR'] ?>" readonly />
					</div>
					<div class="col-sm-3">
						<label for="MCT_USER">Last Updated By</label>
						<input type="text" name="MCT_USER" class="form-control" title="Last Updated By" value="<?= $call[0]['MCT_USER'] ?>"  placeholder="" />
					</div>
				</div>	
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="MCT_SOURCE">Source</label>
						<select name="MCT_SOURCE" class="form-control" title="Call Source">
							<option value="">Sources here...</option>
							<?php foreach ($source_types as $source): ?>
								<?php if ($call[0]['MCT_SOURCE'] == $source['MCT_SOURCE_DESC']): ?>
									<option selected value="<?= $source['MCT_SOURCE_DESC'] ?>"><?= $source['MCT_SOURCE_DESC'] ?></option>	
								<?php else : ?>
									<option value="<?= $source['MCT_SOURCE_DESC'] ?>"><?= $source['MCT_SOURCE_DESC'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-sm-3">
						<label for="MCT_TYPE_DESC">Type</label>
						<select name ="MCT_TYPE_DESC" class="form-control" title="Call Type">
							<option value="">Types here...</option>
							<?php foreach ($call_types as $type): ?>
								<?php if ($call[0]['MCT_TYPE_DESC'] == $type['MCT_TYPE_DESC']): ?>
									<option selected value="<?= $type['MCT_TYPE_DESC'] ?>"><?= $type['MCT_TYPE_DESC'] ?></option>
								<?php else: ?>
									<option value="<?= $type['MCT_TYPE_DESC'] ?>"><?= $type['MCT_TYPE_DESC'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-sm-3">
						<label for="MCT_FST_NME">First Name</label>
						<input type="text" name="MCT_FST_NME"  class="form-control" title="First Name" maxlength="35" value="<?= $call[0]['MCT_FST_NME'] ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_LST_NME">Last Name</label>
						<input type="text" name="MCT_LST_NME" class="form-control" title="Last Name" maxlength="35" value="<?= $call[0]['MCT_LST_NME'] ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="MCT_CVC_NUM">Civic #</label>
						<input type="text" name="MCT_CVC_NUM" class="form-control" title="Civic Number" maxlength="5" value="<?= $call[0]['MCT_CVC_NUM'] ?>" />
					</div>
					<div class="col-sm-1">
						<label for="MCT_CVC_EXT_CDE">Ext.</label>
						<input type="text" name="MCT_CVC_EXT_CDE" class="form-control" title="Extention" maxlength="3" value="<?= $call[0]['MCT_CVC_EXT_CDE'] ?>" />
					</div>
					<div class="col-sm-4">
						<label for="STREET_CDE">Civic Address</label>
						<input type="text" name="STREET_CDE" class="form-control" title="Civic Address" maxlength="5" value="<?= $call[0]['STREET_CDE'] ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_UNIT_DSC">Unit Desc.</label>
						<input type="text" name="MCT_UNIT_DSC" class="form-control" title="Unit Description" maxlength="5" value="<?= $call[0]['MCT_UNIT_DSC'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_UNIT_NUM">Unit #</label>
						<input type="text" name="MCT_UNIT_NUM" class="form-control" title="Unit Number" maxlength="5" value="<?= $call[0]['MCT_UNIT_NUM'] ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="MCT_POSTAL_CDE">Postal Code</label>
						<input type="text" name="MCT_POSTAL_CDE" class="form-control" title="Postal Code" maxlength="7" value="<?= $call[0]['MCT_POSTAL_CDE'] ?>" />
					</div>

					<div class="col-sm-4">
						<label for="MCT_DISTRICT">District</label>
						<select name="MCT_DISTRICT" class="form-control">
							<option value="">Select a District...</option>
							<?php foreach ($districts as $dist): ?>
								<?php if ($call[0]['MCT_DISTRICT'] == $dist['MCT_DISTRICT']): ?>
									<option selected value="<?= $dist['MCT_DISTRICT'] ?>"><?= $dist['MCT_DISTRICT']." - ".$dist['MCT_DIST_DESC'] ?></option>
								<?php else: ?>
									<option value="<?= $dist['MCT_DISTRICT'] ?>"><?= $dist['MCT_DISTRICT']." - ".$dist['MCT_DIST_DESC'] ?></option>
								<?php endif ?>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-sm-2">
						<label for="MCT_FILE_NUM">File #</label>
						<input type="text" name="MCT_FILE_NUM" class="form-control" title="File Number" maxlength="15" value="<?= $call[0]['MCT_FILE_NUM'] ?>" />
					</div>

					<div class="col-sm-2">
						<label for="MCT_PRIORITY">Priority</label>
						<select name="MCT_PRIORITY" class="form-control" title="Priority">
							<option value="0">Low</option>
							<option value="1">Med</option>
							<option value="2">High</option>
						</select>
					</div>
					<div class="col-sm-2">
						<label for="MCT_ENTER_DTE">Entered On</label>
						<input type="text" name="MCT_ENTER_DTE" class="form-control date_picker" title="Entered On" maxlength="" readonly value="<?= $call[0]['MCT_ENTER_DTE'] ?>" />
					</div>
				</div>
			</div>
 

			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="MCT_HPHONE">Home Phone</label>
						<input type="text" name="MCT_HPHONE" class="form-control" title="Home Phone" maxlength="14" value="<?= $call[0]['MCT_HPHONE'] ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_WPHONE">Work Phone</label>
						<input type="text" name="MCT_WPHONE" class="form-control" title="Work Phone" maxlength="14" value="<?= $call[0]['MCT_WPHONE'] ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_CPHONE">Cell Phone</label>
						<input type="text" name="MCT_CPHONE" class="form-control" title="Cell Phone" maxlength="14" value="<?= $call[0]['MCT_CPHONE'] ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_FPHONE">Fax</label>
						<input type="text" name="MCT_FPHONE" class="form-control" title="Fax" maxlength="14" value="<?= $call[0]['MCT_FPHONE'] ?>" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="MCT_DESC">Description</label>
						<textarea name="MCT_DESC" cols="6" class="form-control mct_description" title="Description" maxlength="2000"><?= $call[0]['MCT_DESC'] ?></textarea>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="MCT_REF_NME">Referred To</label>
						<input type="text" name="MCT_REF_NME" class="form-control" title="Referred To" maxlength="40" value="<?= $call[0]['MCT_REF_NME'] ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_REF_DPT">Department</label>
						<input type="text" name="MCT_REF_DPT" class="form-control" title="Department" maxlength="30" value="<?= $call[0]['MCT_REF_DPT'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_REF_PHONE">Phone</label>
						<input type="text" name="MCT_REF_PHONE" class="form-control" title="Phone" maxlength="14" value="<?= $call[0]['MCT_REF_PHONE'] ?>" />
					</div>
					<div class="col-sm-3">
						<label for="MCT_REF_DTE">Referred On</label>
						<input type="text" name="MCT_REF_DTE" class="form-control date_picker" title="Referred On" maxlength="" readonly value="<?= $call[0]['MCT_REF_DTE'] ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="MCT_DUE_DTE">Due On</label>
						<input type="text" name="MCT_DUE_DTE" class="form-control date_picker" title="Due On" maxlength="" readonly value="<?= $call[0]['MCT_DUE_DTE'] ?>" />
					</div>
					<div class="col-sm-2">
						<label for="MCT_RESOLVE_FLG">Resolved?</label>
						<select name="MCT_RESOLVE_FLG" class="form-control">
							<?php if ($call[0]['MCT_RESOLVE_FLG'] == 'N' || empty($call[0]['MCT_RESOLVE_FLG'])): ?>
								<option selected value="N">No</option>
								<option value="Y">Yes</option>
							<?php else : ?>
								<option value="N">No</option>
								<option selected value="Y">Yes</option>
							<?php endif ?>
						</select>
					</div>
				</div>
			</div>

			
			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-md btn-primary pull-right" value="Update" />
						<a href="<?php echo base_url(); ?>MCT/resolved" class="btn btn-md btn-default">Back to Resolved Correspondence</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>