<?php
$BILink = '';

if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'production':
            $BILink = BI_PROD_URL;
            break;
        default:
            $BILink = BI_QA_URL;
    }
}
?>

<div class="row">
	<div class="col-md-9">
		<h2>Update Interred</h2>
		<p class="lead">This form will allow you to update an interred record to the system.</p>
		
	</div>
	<div class="col-sm-3" style="margin-bottom: 1em">
        <a target="_blank" href="<?php echo $BILink ?>/BOE/OpenDocument/opendoc/openDocument.jsp?sIDType=CUID&iDocID=Ae_D7jwgtzBIjI7fYkALJiQ&sIDType=CUID&sType=rpt&sRefresh=N&lsSp%5Fcemnum=<?php echo $interred[0]['CEM_NBR']; ?>&lsSp%5Fownnum=<?php echo $interred[0]['CEM_REFNBR'] ?>&lsSp%5Fparm3=1&lsSp%5Fintnum=<?= $interred[0]['INT_REFNBR'] ?>&lsScomments=A" class="btn btn-md btn-primary add_interred pull-right" style="margin-right: 5px;" >Print Record</a>
	</div>
</div>

 <div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>cemetery/update_interred" method="POST">
			<input type="hidden" name="CEM_NBR"  value="<?php echo $interred[0]['CEM_NBR'] ?>"  />
			<input type="hidden" name="CEM_REFNBR"  value="<?php echo $interred[0]['CEM_REFNBR'] ?>"  />
			<input type="hidden" name="INT_REFNBR"  value="<?php echo $interred[0]['INT_REFNBR'] ?>"  />
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="INT_FNAME">First Name</label>
						<input type="text" name="INT_FNAME" class="form-control" title="First Name" value="<?php echo $interred[0]['INT_FNAME'] ?>" maxlength="20" />
					</div>
					<div class="col-sm-4">
						<label for="INT_MNAME">Middle Name</label>
						<input type="text" name="INT_MNAME" class="form-control" title="Middle Name" value="<?php echo $interred[0]['INT_MNAME'] ?>" maxlength="20" />
					</div>
					<div class="col-sm-4">
						<label for="INT_LNAME">Last Name</label>
						<input type="text" name="INT_LNAME" class="form-control" title="Last Name" value="<?php echo $interred[0]['INT_LNAME'] ?>" maxlength="30" />
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="INT_TITLE">Title</label>
						<input type="text" name="INT_TITLE" class="form-control" title="Title" value="<?php echo $interred[0]['INT_TITLE'] ?>" maxlength="15" />
					</div>
					<div class="col-sm-4">
						<label for="INT_DTE">Interred Date</label>
						<input type="text" name="INT_DTE" class="form-control date_picker_past_cem" title="Interred Date" value="<?php echo $interred[0]['INT_DTE'] ?>" />
					</div>
					<div class="col-sm-4">
						<label for="INT_AGE">Age</label>
						<input type="text" name="INT_AGE" class="form-control" title="Age" value="<?php echo $interred[0]['INT_AGE'] ?>" maxlength="10" />
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="CEM_NAME">Cemetery Name</label>
						<input type="text" name="CEM_NAME" class="form-control" title="Cemetery" value="<?php echo $cemetery[0]['CEM_NAME'] ?>" readonly />
					</div>
					<div class="col-sm-3">
						<label for="CEM_REFNBR">Owner Ref. Number</label>
						<input type="text" name="CEM_REFNBR" class="form-control" title="Owner Reference Number" value="<?php echo $interred[0]['CEM_REFNBR'] ?>" readonly />
					</div>
					<div class="col-sm-3">
						<label for="owner_name">Owner Name</label>
						<input type="text" class="form-control" title="Owner Name" value="<?php echo $owner_name ?>" disabled placeholder="" />
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="INT_REFNBR">Interred Ref. Number</label>
						<input type="text" name="INT_REFNBR" class="form-control" title="Interred Reference Number" value="<?php echo $interred[0]['INT_REFNBR'] ?>" readonly />
					</div>
					<div class="col-sm-2">
						<label for="INT_TYP">Grave Type</label>
						<input type="text" name="INT_TYP" class="form-control" title="Grave Type" value="<?php echo $interred[0]['INT_TYP'] ?>" maxlength="10" />
					</div>
					<div class="col-sm-2">
						<label for="INT_MARKER">Marker</label>
						<input type="text" name="INT_MARKER" class="form-control" title="Marker" value="<?php echo $interred[0]['INT_MARKER'] ?>" maxlength="1" />
					</div>
					<div class="col-sm-2">
						<label for="INT_LOC">Location</label>
						<input type="text" name="INT_LOC" class="form-control" title="Location" value="<?php echo $interred[0]['INT_LOC'] ?>" maxlength="5" />
					</div>
					<div class="col-sm-4">
						<label for="INT_INCOMPDTE">Incomplete Interred Date</label>
						<input type="text" name="INT_INCOMPDTE" class="form-control" title="Incomplete Interred Date" value="<?php echo $interred[0]['INT_INCOMPDTE'] ?>" />
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="INT_CMNT">Comments</label>
						<textarea name="INT_CMNT" class="form-control" title="Comments" maxlength="256"><?php echo $interred[0]['INT_CMNT'] ?></textarea>
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="INT_SPC_CMNT">Special Comments</label>
						<textarea name="INT_SPC_CMNT" class="form-control" title="Special Comments" maxlength="256"><?php echo $interred[0]['INT_SPC_CMNT'] ?></textarea>
					</div>
				</div>
			</div>
            <div class="form-group" style="margin-top: 2em">
                <input type="submit" class="btn btn-md btn-primary pull-right" value="Update Interred" />
                <a href="<?php echo base_url(); ?>cemetery/update_owner/<?= $interred[0]['CEM_REFNBR'] ?>/<?= $interred[0]['CEM_NBR'] ?>"  class="btn btn-md btn-default"> Back to Owner</a>
            </div>
		</form>
	</div>
</div>