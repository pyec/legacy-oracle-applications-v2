<div class="row">
	<div class="col-md-6">
		<h2>Add District Councillor</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>MCT/add_councillor" method="POST">
			<div class="form-group">
				<div class="col-sm-2">
					<label for="MCT_DISTRICT">District #</label>
					<input type="text" name="MCT_DISTRICT" class="form-control" title="District Number" maxlength="2" />
				</div>
				<div class="col-sm-4">
					<label for="MCT_DIST_DESC">District Name</label>
					<input type="text" name="MCT_DIST_DESC" class="form-control" title="District Name" maxlength="100" />
				</div>
				<div class="col-sm-4">
					<label for="MCT_COUNCILLOR">Councillor Name</label>
					<input type="text" name="MCT_COUNCILLOR" class="form-control" title="Councillor Name" maxlength="100" />
				</div>
				<div class="col-sm-2">
					<input type="submit" class="btn btn-primary add_councillor" value="Add Councilor" />
				</div>
			</div>
		</form>
		<legend></legend>

		<div class="form-group">
			 <div class="col-sm-3">
				<a href="<?php echo base_url(); ?>MCT/district_councillors" class="btn btn-default">Back to Districts</a>
			 </div>
		</div>
	</div>
</div>