<div class="row">
	<div class="col-md-12">
		<h2>Add Course</h2>
		<p class="lead">This form will allow you to add a new course to the system.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="row">
				<form action="<?php echo base_url(); ?>TPW/add_employee_course/" method="post" >
				<input type="hidden" name="EMPL_ID" value="<?= $this->uri->segment(3); ?>" />
				<div class="col-sm-2">
					<button type="button" value="course" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Course</button>
				</div>
				<div class="col-sm-2">
					<label for="COURSE_NUM">Course #</label><span class="text-danger">*</span>
					<input type="text" name="COURSE_NUM" class="form-control COURSE_NUM" title="Course Number" disabled />
					<input type="hidden" name="COURSE_NUM" class="form-control COURSE_NUM" />
					<input type="hidden" name="RECERT_PERIOD" class="form-control RECERT_PERIOD" />
				</div>

				<div class="col-sm-2">
					<label for="COURSE_DTE">Course Date</label><span class="text-danger">*</span>
					<input type="text" name="COURSE_DTE" class="form-control date_picker_new COURSE_DTE" title="Course Date" readonly />
				</div>

				<div class="col-sm-2">
					<label for="EXPIRY_DTE">Expiry date</label>
					<input type="text" name="EXPIRY_DTE" class="form-control date_picker_new EXPIRY_DTE" title="Expiry Date" readonly />
<!--					<input type="hidden" name="EXPIRY_DTE" class="form-control date_picker EXPIRY_DTE"  />-->
				</div>
				
				<div class="col-sm-2 no_label_fix">
					<label for="RECERT_FLG">Recert Flag</label>
					<input type="checkbox" name="RECERT_FLG" class="" title="Recert Flag"  />
				</div>

				<div class="col-sm-2 no_label_fix">
					<label for="PASS_FLG">Pass Flag</label>
					<input type="checkbox" name="PASS_FLG" class="" title="Recert Flag"  />
				</div>
			</div>

		</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-8">
						<label for="COURSE_COMMENTS">Comments</label>
						<textarea class="form-control" name="COURSE_COMMENTS" ></textarea>
					</div>
				</div>
			</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_course pull-right" value="Add Course" />
					<a href="<?php echo base_url(); ?>TPW/view_employee/<?= $EMPL_ID ?>" class="btn btn-md btn-default return">Back to Employee Courses</a>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>


<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Course Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<div class="modal_search">
								<label for="course_search_box">Course Name</label>
								<input type="text" name="course_search_box" id="" class="form-control course_search_box" title="Course Name" autofocus maxlength="" />
								<p class="text-muted">(Select the name of the record you would like to use.)</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Number</th>
						<th>Name</th>
					</thead>
					<tbody class="courses_modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->