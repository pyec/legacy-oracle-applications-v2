
<nav class="navbar navbar-default navbar-fixed-top app_nav" role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="nav_items">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle app_header" data-toggle="collapse" data-target=".navbar-ex2-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php echo base_url(); ?>MCT" class="navbar-brand">Mayor Correspondence Tracking</a>
			</div>
		
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse navbar-ex2-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<?php echo base_url(); ?>MCT/add">Add New</a></li>
					<li><a href="<?php echo base_url(); ?>MCT/open">Open</a></li>
					<li><a href="<?php echo base_url(); ?>MCT/resolved">Resolved</a></li>
					<li><a href="<?php echo base_url(); ?>MCT/types">Types</a></li>
					<!-- <li><a href="<?php echo base_url(); ?>MCT/district_councillors">District Councilors</a></li> -->
					<li><a href="<?php echo base_url(); ?>MCT/source_types">Sources</a></li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</div>
</nav>

