<div class="row">
	<div class="col-md-6">
		<h2>Types</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="col-sm-12">
	<form action="<?php echo base_url(); ?>MCT/type_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="type_name">Type Name</label>
					<input type="text" name="type_name" class="form-control" title="Type Name" value="<?php echo set_value('type_name') ?>" tabindex="1" placeholder="Type Name" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="mct_call_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
				<a href="<?php echo base_url(); ?>MCT/add_type" class="btn btn-success btn-md pull-right">Add Type</a>
		</div>
	</div>
</div>

<?php if (!empty($call_types)): ?>
    <div class="row">
        <div class="col-md-12">
            <!-- Echo out a list of call types in text boxes that can be edited -->
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <th>Name</th>
                <!-- <th>Delete</th> -->
                </thead>
                <tbody class="tbody">
                <?php foreach ($call_types as $type): ?>
                    <tr>
                        <td><?php echo $type['MCT_TYPE_DESC'] ?></td>
                        <!-- <td>
										<form action="<?php echo base_url(); ?>MCT/delete_type" method="POST" >
										<button class="btn btn-danger btn-sm delete_type">Delete</button>
										<input type="hidden" name="MCT_TYPE_DESC" value="<?php echo $type['MCT_TYPE_DESC'] ?>" />
										</form>
									</td> -->
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
<?php endif ?>