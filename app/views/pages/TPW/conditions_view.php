<div class="row">
	<div class="col-md-12">
		<h2>License Condition Update</h2>
		<p class="lead">Select an condition to view/update their information.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<!-- <div class="row"> -->
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<a href="<?php echo base_url(); ?>/TPW/add_condition" class="btn btn-success btn-md pull-right no_label_fix" title="Add">Add Condition</a>
				</div>
			</div>
		</div>
	<legend class="phonebook_legend"></legend>
	<div class="row">
		<div class="col-md-12">
			<?php if (!empty($conditions)): ?>
				<h2 class="count"><?= count($conditions) ?> conditions</h2>
			<?php endif ?>
		</div>
	</div>
<div class="row no_label_fix">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Condition</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
				<?php foreach ($conditions as $con): ?>
					<tr>
						<td><a href="<?= base_url(); ?>TPW/update_condition/<?= $con['CONDITION_CDE'] ?>"><?= $con['CONDITION_CDE'].' - '.$con['CONDITION_DSC'] ?></a></td>
						<td>
							<form action="<?php echo base_url(); ?>TPW/delete_condition" method="POST" >
								<button type="submit" class="btn btn-sm btn-danger delete_condition"><i class="fa fa-minus-square"></i></button>
								<input type="hidden" name="CONDITION_CDE" value="<?= $con['CONDITION_CDE'] ?>" />
							</form>
						</td>	
					</tr>
				<?php endforeach ?>
			</tbody>	
		</table>
	</div>
</div>