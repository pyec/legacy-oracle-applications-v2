<div class="row">
	<div class="col-md-12">
		<h2>Add Course</h2>
		<p class="lead">This form will allow you to add a new course to the system.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="row">
			<form action="<?php echo base_url(); ?>TPW/update_course" method="post" >
				<div class="col-sm-2">
					<label for="COURSE_NUM">Course #</label>
					<input type="text" name="COURSE_NUM" class="form-control" title="Course Number" value="<?= $course['COURSE_NUM'] ?>" disabled />
					<input type="hidden" name="COURSE_NUM" value="<?= $course['COURSE_NUM'] ?>" />
				</div>

				<div class="col-sm-4">
					<label for="COURSE_NME">Course Name</label>
					<input type="text" name="COURSE_NME" class="form-control" title="Course Name" value="<?= $course['COURSE_NME'] ?>" />
				</div>
				
				<div class="col-sm-2 no_label_fix">
					<label for="RECERT_FLG">Recert Required</label>
					<input type="checkbox" name="RECERT_FLG" class="" title="Recert Flag" <?php echo $checked = ($course['RECERT_FLG'] == 'Y' ? 'checked':''); ?> />
				</div>

				<div class="col-sm-2">
					<label for="RECERT_PERIOD">Recert Period (yrs)</label>
					<input type="text" name="RECERT_PERIOD" class="form-control" title="Recert Period" value="<?= $course['RECERT_PERIOD'] ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_course pull-right" value="Update Course" />
					<a href="<?php echo base_url(); ?>TPW/course_list" class="btn btn-md btn-default return">Back to Courses</a>
				</div>
			</form>
			</div>
		</div>
	</div>
</div>