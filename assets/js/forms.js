// Google Analytics
// var _gaq = _gaq || [];
// _gaq.push(['_setAccount', 'UA-4006696-1']);
// _gaq.push(['_setDomainName', 'halifax.ca']);
// _gaq.push(['_trackPageview']);

// (function() {
// var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
// ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
// var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
// })();


// jQuery UI date picker
// http://xdsoft.net/jqplugins/datetimepicker/
$(function() {
	$('.date_time_picker_past').datetimepicker({
		maxDate: 0,
		allowTimes:[
			'00:00', '00:30', 
			'01:00', '01:30', 
			'02:00', '02:30', 
			'03:00', '03:30', 
			'04:00', '04:30', 
			'05:00', '05:30', 
			'06:00', '06:30', 
			'07:00', '07:30', 
			'08:00', '08:30', 
			'09:00', '09:30', 
			'10:00', '10:30', 
			'11:00', '11:30', 
			'12:00', '12:30', 
			'13:00', '13:30', 
			'14:00', '14:30', 
			'15:00', '15:30', 
			'16:00', '16:30', 
			'17:00', '17:30', 
			'18:00', '18:30', 
			'19:00', '19:30', 
			'20:00', '20:30', 
			'21:00', '21:30',
			'22:00', '22:30',
			'23:00', '23:30' 
		]
	});

	$('.date_time_picker_future').datetimepicker({
		minDate: 0,
		allowTimes:[
			'00:00', '00:30', 
			'01:00', '01:30', 
			'02:00', '02:30', 
			'03:00', '03:30', 
			'04:00', '04:30', 
			'05:00', '05:30', 
			'06:00', '06:30', 
			'07:00', '07:30', 
			'08:00', '08:30', 
			'09:00', '09:30', 
			'10:00', '10:30', 
			'11:00', '11:30', 
			'12:00', '12:30', 
			'13:00', '13:30', 
			'14:00', '14:30', 
			'15:00', '15:30', 
			'16:00', '16:30', 
			'17:00', '17:30', 
			'18:00', '18:30', 
			'19:00', '19:30', 
			'20:00', '20:30', 
			'21:00', '21:30',
			'22:00', '22:30',
			'23:00', '23:30' 
		]
	});

	$('.date_time_picker').datetimepicker({
		allowTimes:[
			'00:00', '00:30', 
			'01:00', '01:30', 
			'02:00', '02:30', 
			'03:00', '03:30', 
			'04:00', '04:30', 
			'05:00', '05:30', 
			'06:00', '06:30', 
			'07:00', '07:30', 
			'08:00', '08:30', 
			'09:00', '09:30', 
			'10:00', '10:30', 
			'11:00', '11:30', 
			'12:00', '12:30', 
			'13:00', '13:30', 
			'14:00', '14:30', 
			'15:00', '15:30', 
			'16:00', '16:30', 
			'17:00', '17:30', 
			'18:00', '18:30', 
			'19:00', '19:30', 
			'20:00', '20:30', 
			'21:00', '21:30',
			'22:00', '22:30',
			'23:00', '23:30' 
		]
	});

	$('.time_picker').datetimepicker({
		datepicker:false,
		format:'h:i',
		allowTimes:[
			'00:00', '00:30', 
			'01:00', '01:30', 
			'02:00', '02:30', 
			'03:00', '03:30', 
			'04:00', '04:30', 
			'05:00', '05:30', 
			'06:00', '06:30', 
			'07:00', '07:30', 
			'08:00', '08:30', 
			'09:00', '09:30', 
			'10:00', '10:30', 
			'11:00', '11:30', 
			'12:00', '12:30', 
			'13:00', '13:30', 
			'14:00', '14:30', 
			'15:00', '15:30', 
			'16:00', '16:30', 
			'17:00', '17:30', 
			'18:00', '18:30', 
			'19:00', '19:30', 
			'20:00', '20:30', 
			'21:00', '21:30',
			'22:00', '22:30',
			'23:00', '23:30' 
		]
	});

    $('.date_picker_past').datetimepicker({
        maxDate:0,
        lang: 'oracle',
        timepicker:false,
        format:'d-M-y'
    });

    $('.date_picker_past_cem').datepicker({
        dateFormat:'d-M-yy',
        changeMonth: true,
        changeYear: true,
        maxDate: 0,
        yearRange: '1700:'+(new Date).getFullYear()
    });

	$('.date_picker_future').datetimepicker({
		minDate:0,
		lang: 'oracle',
		timepicker:false,
		format:'d-M-y'
	});

	$('.date_picker').datetimepicker({
		lang: 'oracle',
		timepicker:false,
		format:'d-M-y',
		yearStart: 1700
	});

    $('.date_picker_new').datepicker({
        dateFormat:'d-M-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1900:+100'
    });

    $('.date_picker_cem').datepicker({
        dateFormat:'d-M-yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '1700:'+(new Date).getFullYear()
    });

	$('.sot_date_picker').datetimepicker({
		lang: 'oracle',
		timepicker:false,
		format:'y-M-d',
		yearStart: 1700
	});
	// make sure the content lines up wit the select state.
	if ($('.mailing_address_flag').value == "Yes") 
	{
		hide_content($('.mailing_address_region'));
	}

	if ($('.property_owner_flag').value == "No") 
	{
		hide_content($('.property_owner_region'));
	}

	if ($('.consent_flag').value == 'No') 
	{
		hide_content($('.consent_region'));
	}

	if ($('.reoffering_flag').value == 'No') 
	{
		hide_content($('.reoffering_region'));
	}

	if ($('.requires_dispatch_flag').value == 'No') 
	{
		hide_content($('.requires_dispatch_message'));
		show_content($('.requires_dispatch_region'));
	}

	$('.TENDER_NUM').mask("0000-000", {clearIfNotMatch: true});
    $('.PROJ_NUM').mask("00-00000", {clearIfNotMatch: true});
  });

/*           
============================================================================
DOM Manipulation functions
----------------------------------------------------------------------------
parent_element - DOM object
----------------------------------------------------------------------------
NO RETURN
============================================================================
*/

function toggle_content(parent_element)
{
	parent_element.toggle();
}

function hide_content(parent_element)
{
	parent_element.hide("slow");
}

function show_content(parent_element)
{
	parent_element.show("slow");
}

$('.mailing_address_toggle').change(function(){

	var mailing_address_region = $('.mailing_address_region');
	if (this.value == 'No') 
	{
		show_content(mailing_address_region);
	}
	else if(this.value == "Yes")
	{
		hide_content(mailing_address_region);
	}
});

$('.property_owner_flag').change(function(){

	var property_owner_region = $('.property_owner_region');
	if (this.value == 'No') 
	{
		hide_content(property_owner_region);
	}
	else if(this.value == "Yes")
	{
		show_content(property_owner_region);
	}
});

$('.reoffering_flag').change(function(){

	var property_owner_region = $('.reoffering_region');
	if (this.value == 'No') 
	{
		hide_content(property_owner_region);
	}
	else if(this.value == "Yes")
	{
		show_content(property_owner_region);
	}
});

$('.consent_flag').change(function(){

	var consent_region = $('.consent_region');
	if (this.value == 'No') 
	{
		hide_content(consent_region);
	}
	else if(this.value == "Yes")
	{
		show_content(consent_region);
	}
});

$('.previous_service_flag').change(function(){

	var previous_service_region = $('.previous_service_region');
	if (this.value == 'No') 
	{
		hide_content(previous_service_region);
	}
	else if(this.value == "Yes")
	{
		show_content(previous_service_region);
	}
});

$('.mailing_address_flag').change(function(){

	var mailing_address_region = $('.mailing_address_region');
	if (this.value == 'No') 
	{
		show_content(mailing_address_region);
	}
	else if(this.value == "Yes")
	{
		hide_content(mailing_address_region);
	}
});

$('.requires_dispatch_flag').change(function(){

	var requires_dispatch_region = $('.requires_dispatch_region');
	if (this.value == 'No') 
	{
		show_content(requires_dispatch_region);
		hide_content($('.requires_dispatch_message'));
	}
	else if(this.value == "Yes")
	{
		show_content($('.requires_dispatch_message'));
		hide_content(requires_dispatch_region);
	}
});
// EDL file ID conformation
$('.FILE_ID').change(function(e){

    // Get the url for the AJAX call.
    var url = window.location.href;
    url = url.split("/");

    // Create a base url to call
    var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

    var fileIdExists = false;

    $.ajax({
        url: BASE_URL+'EDL/ajax_file_id',
        type: 'POST',
        dataType: 'JSON',
        async: false,
        data: {id: this.value},
    })
    .success(function(json) {
		if (json !== false) {
            fileIdExists = true;
		}
    });

	if (fileIdExists) {
		alert('A record with this file ID already exists');
        e.preventDefault();
        this.focus();
	}
    else {
        if (!confirm('Are you sure the File ID : ' + this.value + ' is correct?')) {
            e.preventDefault();
            this.focus();
        }
    }
});



// CKEditor
CKEDITOR.replace('NOTES_DSC');

  





