<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : TPW Employee Training Tracking model

Description : This is the data layer for the PWT ETT system.

  Functions : get_courses( $offset = 0 ),
			get_course_by_id( $id = null ),
			get_course_by_name( $name = null ),
			insert_course( $data = null ),
			update_course( $id = null ),
			remove_course( $id = null )
=========================================================================================================================*/
class TPW_model extends CI_Model 
{
	//Properies
	public $table_courses = null;
	public $id_courses = null;
	public $name_courses = null;

	public $table_conditions = null;
	public $id_conditions = null;
	public $name_conditions = null;

	public $table_emp_courses = null;
	public $id_emp_courses = null;

	public $table_employee = null;
	public $id_employee = null;
	public $first_name_employee = null;
	public $middle_name_employee = null;
	public $last_name_employee = null;

	public $table_endorsements = null;
	public $id_endorsements = null;
	public $name_endorsements = null;

	public $oracle_db = null; // The reference to the database

	public function __construct()
	{
		parent::__construct();
		//Load Dependencies
		$this->table_courses = 'ETT_COURSES';
		$this->id_courses = 'COURSE_NUM';
		$this->name_courses = 'COURSE_NME';

		$this->table_conditions = 'ETT_CONDITION';
		$this->id_conditions = 'CONDITION_CDE';
		$this->name_conditions = 'CONDITION_DSC';

		$this->table_emp_courses = 'ETT_EMPCOURSE';
		$this->id_emp_courses = 'EMPL_ID';

		$this->table_employee = 'ETT_EMPLOYEE';
		$this->id_employee = 'EMPL_ID';
		$this->first_name_employee = 'FIRST_NME';
		$this->middle_name_employee = 'MIDDLE_NME';
		$this->last_name_employee = 'LST_NME';

		$this->table_endorsements = 'ETT_ENDORSEMENT';
		$this->id_endorsements = 'ENDORSEMENT_CDE';
		
		$this->oracle_db = $this->load->database('oracle_PWT', TRUE);
	}

	public function get_next_val( $table_name, $id )
	{
		// Query for the max value in the id field.
		$query = $this->oracle_db->query("SELECT MAX(".$id.") FROM ".$table_name);

		// In one expression return the result sets value, cast it to an integer 
		// and store it in the variable $id.
		$id = (int)$query->result_array()[0]["MAX(".$id.")"];

		// Increment by 1 and return the id so we can add a new record to the table.
		return ++$id;
	}

	public function get_employee_courses($id = null)
	{
		$this->oracle_db->where($this->id_emp_courses, $id);
		$this->oracle_db->order_by('COURSE_DTE', 'desc');
		$query = $this->oracle_db->get($this->table_emp_courses);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_employee_course($EMPL_ID = null, $COURSE_NUM = null)
	{
		$this->oracle_db->where('EMPL_ID', $EMPL_ID);
		$this->oracle_db->where('COURSE_NUM', $COURSE_NUM);
		$query = $this->oracle_db->get($this->table_emp_courses);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}

	public function insert_employee_course($data = null)
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['ADDDTTM'] = "to_date('".$data['ADDDTTM']."','dd-mm-YYYY')";
			$data['COURSE_DTE'] = "to_date('".$data['COURSE_DTE']."','dd-mm-YYYY')";
			$data['EXPIRY_DTE'] = "to_date('".$data['EXPIRY_DTE']."','dd-mm-YYYY')";
			$this->oracle_db->set("ADDDTTM", $data['ADDDTTM'], false);
			$this->oracle_db->set("COURSE_DTE", $data['COURSE_DTE'], false);
			$this->oracle_db->set("EXPIRY_DTE", $data['EXPIRY_DTE'], false);
			unset($data['ADDDTTM']);
			unset($data['COURSE_DTE']);
			unset($data['EXPIRY_DTE']);
			$results = $this->oracle_db->insert('ETT_EMPCOURSE', $data);
			return $results;
		}
	}

	public function update_employee_course( $data )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['MODDTTM'] = "to_date('".$data['MODDTTM']."','dd-mm-YYYY')";
			$data['COURSE_DTE'] = "to_date('".$data['COURSE_DTE']."','dd-mm-YYYY')";
			$data['EXPIRY_DTE'] = "to_date('".$data['EXPIRY_DTE']."','dd-mm-YYYY')";
			$this->oracle_db->set("MODDTTM", $data['MODDTTM'], false);
			$this->oracle_db->set("COURSE_DTE", $data['COURSE_DTE'], false);
			$this->oracle_db->set("EXPIRY_DTE", $data['EXPIRY_DTE'], false);
			unset($data['MODDTTM']);
			unset($data['COURSE_DTE']);
			unset($data['EXPIRY_DTE']);


			$this->oracle_db->where($this->id_emp_courses, $data[$this->id_emp_courses]);
			$results = $this->oracle_db->update($this->table_emp_courses, $data);

			return $results;
		}
	}

	public function remove_employee_course( $id = null, $course_id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('EMPL_ID', $id);
			$this->oracle_db->where('COURSE_NUM', $course_id);
			$results = $this->oracle_db->delete($this->table_emp_courses);

			return $results;
		}
	}


/*-----------------------------------------------------------------------------------
	PWT ETT retrieval functions
------------------------------------------------------------------------------------*/
	public function get_courses( $params = false )
	{
		if (!$params)
		{
			$query = $this->oracle_db->get($this->table_courses);

			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}	
		}
		else
		{
			$this->oracle_db->like($this->id_courses, $params['course_number']);
			$this->oracle_db->like($this->name_courses, strtoupper($params['course_name']));
			$query = $this->oracle_db->get($this->table_courses);

			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}	
		}
	}

	public function get_course_by_name( $id = null )
	{
		$this->oracle_db->where($this->id_courses, $id);
		$query = $this->oracle_db->get($this->table_courses);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_courses_like_name( $id = null )
	{
		$this->oracle_db->like($this->name_courses, $id);
		$query = $this->oracle_db->get($this->table_courses);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_course_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_courses, $id);
		$query = $this->oracle_db->get($this->table_courses);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_courses_by_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->like($this->name_courses, strtoupper($name));
			$query = $this->oracle_db->get($this->table_courses);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function search_courses($search_array = null)
	{
		if ($search_array == null)
		{
			return $this->get_courses();
		}
		else
		{
			if (!empty($search_array['COURSE_NUM']))
				$this->oracle_db->like('COURSE_NUM', strtoupper($search_array['COURSE_NUM']));

			if (!empty($search_array['COURSE_NME']))
				$this->oracle_db->like('COURSE_NME', strtoupper($search_array['COURSE_NME']));


			$query = $this->oracle_db->get($this->table_courses);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
		}
	}

	public function does_course_exist( $course_number = null )
	{
		if ($course_number == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('COURSE_NUM', $course_number);
			$query = $this->oracle_db->get($this->table_courses);

			if ($query->num_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	/*-----------------------------------------------------------------------------------
	PWT ETT insertion function
------------------------------------------------------------------------------------*/
	public function insert_course( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['ADDDTTM'] = "to_date('".$data['ADDDTTM']."','YYYY-mm-dd')";
			$this->oracle_db->set("ADDDTTM", $data['ADDDTTM'], false);
			unset($data['ADDDTTM']);
			$results = $this->oracle_db->insert($this->table_courses, $data);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	PWT ETT update function
------------------------------------------------------------------------------------*/
	public function update_course( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['MODDTTM'] = "to_date('".$data['MODDTTM']."','YYYY-mm-dd')";
			$this->oracle_db->set("MODDTTM", $data['MODDTTM'], false);
			$data['RECERT_FLG'] = (!empty($data['RECERT_FLG']) ? 'Y':'N');
			unset($data['MODDTTM']);
			$this->oracle_db->where($this->id_courses, $data[$this->id_courses]);
			$results = $this->oracle_db->update($this->table_courses, $data);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	PWT ETT removal function
------------------------------------------------------------------------------------*/
	public function remove_course( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_courses, $id);
			$results = $this->oracle_db->delete($this->table_courses);

			return $results;
		}
	}



	public function get_employees()
	{
		$query = $this->oracle_db->get($this->table_employee);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function search_employees($search_array = null)
	{
		if ($search_array == null)
		{
			return false;
		}
		else
		{
			if (!empty($search_array['EMPL_ID']))
				$this->oracle_db->like('EMPL_ID', $search_array['EMPL_ID']);

			if (!empty($search_array['FIRST_NME']))
				$this->oracle_db->like('FIRST_NME', strtoupper($search_array['FIRST_NME']));

			if (!empty($search_array['LAST_NME']))
				$this->oracle_db->like('LAST_NME', strtoupper($search_array['LAST_NME']));

			if (!empty($search_array['POSITION_NME']))
				$this->oracle_db->like('POSITION_NME', strtoupper($search_array['POSITION_NME']));

			$query = $this->oracle_db->get($this->table_employee);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
		}
	}

	public function get_employee_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_employee, $id);
		$query = $this->oracle_db->get($this->table_employee);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_employee_by_name( $name = null )
	{

		if (!empty($name['first_name'])){$this->oracle_db->like($this->first_name_employee, strtoupper($name['first_name']));}
		if (!empty($name['middle_name'])){$this->oracle_db->like($this->middle_name_employee, strtoupper($name['middle_name']));}
		if (!empty($name['last_name'])){$this->oracle_db->like($this->last_name_employee, strtoupper($name['last_name']));}
			
		$query = $this->oracle_db->get($this->table_employee);

		if ($query->num_rows() > 0)
		{ 
			return $results;
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

/*-----------------------------------------------------------------------------------
	PWT ETT insertion function
------------------------------------------------------------------------------------*/
	public function insert_employee( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			foreach ($data as $key => $value) 
			{
				if (is_string($value))
				{
					$data[$key] = strtoupper($value);
				}
			}

			// $data['EMPL_ID'] = $this->get_next_val($this->table_employee, 'EMPL_ID');
			$data['ADDDTTM'] = "to_date('".$data['ADDDTTM']."','YYYY-mm-dd')";
			$this->oracle_db->set("ADDDTTM", $data['ADDDTTM'], false);
			unset($data['ADDDTTM']);

			$results = $this->oracle_db->insert($this->table_employee, $data);
			return $results;
		}
	}

	public function does_emp_id_exist( $id = null )
	{
		$this->oracle_db->where($this->id_employee, $id);
		$query = $this->oracle_db->get($this->table_employee);

		if ($query->num_rows() > 0)
		{ 
			return true;
		}
		else 
		{
			return false;
		}
	}

/*-----------------------------------------------------------------------------------
	PWT ETT update function
------------------------------------------------------------------------------------*/
	public function update_employee( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			foreach ($data as $key => $value) 
			{
				if (is_string($value))
				{
					$data[$key] = strtoupper($value);
				}
			}
			
			$data['MODDTTM'] = "to_date('".$data['MODDTTM']."','YYYY-mm-dd')";
			$this->oracle_db->set("MODDTTM", $data['MODDTTM'], false);
			unset($data['MODDTTM']);
			$this->oracle_db->where($this->id_employee, $data[$this->id_employee]);
			$results = $this->oracle_db->update($this->table_employee, $data);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	PWT ETT removal function
------------------------------------------------------------------------------------*/
	public function remove_employee( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_employee, $id);
			$results = $this->oracle_db->delete($this->table_employee);
			return $results;
		}
	}
	



	public function get_endorsements()
	{
		$this->oracle_db->order_by('ENDORSEMENT_CDE', 'asc');
		$query = $this->oracle_db->get($this->table_endorsements);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_endorsement_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_endorsements, $id);
		$query = $this->oracle_db->get($this->table_endorsements);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}

	/*-----------------------------------------------------------------------------------
	endorsement insertion function
------------------------------------------------------------------------------------*/
	public function insert_endorsement( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['ADDDTTM'] = "to_date('".$data['ADDDTTM']."','YYYY-mm-dd')";

			$this->oracle_db->set("ENDORSEMENT_CDE", $data['ENDORSEMENT_CDE']);
			$this->oracle_db->set("ENDORSEMENT_DSC", $data['ENDORSEMENT_DSC']);
			$this->oracle_db->set("ADDBY", $data['ADDBY']);
			$this->oracle_db->set("ADDDTTM", $data['ADDDTTM'], false);
			$results = $this->oracle_db->insert($this->table_endorsements);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	endorsement update function
------------------------------------------------------------------------------------*/
	public function update_endorsement( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['MODDTTM'] = "to_date('".$data['MODDTTM']."','YYYY-mm-dd')";

			$this->oracle_db->set("ENDORSEMENT_CDE", $data['ENDORSEMENT_CDE']);
			$this->oracle_db->set("ENDORSEMENT_DSC", $data['ENDORSEMENT_DSC']);
			$this->oracle_db->set("MODBY", $data['MODBY']);
			$this->oracle_db->set("MODDTTM", $data['MODDTTM'], false);

			$this->oracle_db->where($this->id_endorsements, $data[$this->id_endorsements]);
			$results = $this->oracle_db->update($this->table_endorsements);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	endorsement removal function
------------------------------------------------------------------------------------*/
	public function remove_endorsement( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_endorsements, $id);
			$results = $this->oracle_db->delete($this->table_endorsements);
			
			return $results;
		}
	}



	public function get_conditions()
	{
		$this->oracle_db->order_by('CONDITION_CDE', 'asc');
		$query = $this->oracle_db->get($this->table_conditions);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_condition_by_id( $id = null )
	{
		$this->oracle_db->where('CONDITION_CDE', $id);
		$query = $this->oracle_db->get($this->table_conditions);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}

	public function does_condition_exist( $condition_code = null )
	{
		if ($condition_code == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('CONDITION_CDE', $condition_code);
			$query = $this->oracle_db->get('ETT_CONDITION');

			if ($query->num_rows() > 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}

/*-----------------------------------------------------------------------------------
	Condition insertion function
------------------------------------------------------------------------------------*/
	public function insert_condition( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['ADDDTTM'] = "to_date('".$data['ADDDTTM']."','YYYY-mm-dd')";

			$this->oracle_db->set("CONDITION_CDE", $data['CONDITION_CDE']);
			$this->oracle_db->set("CONDITION_DSC", $data['CONDITION_DSC']);
			$this->oracle_db->set("ADDBY", $data['ADDBY']);
			$this->oracle_db->set("ADDDTTM", $data['ADDDTTM'], false);
			$results = $this->oracle_db->insert($this->table_conditions);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Condition update function
------------------------------------------------------------------------------------*/
	public function update_condition( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['MODDTTM'] = "to_date('".$data['MODDTTM']."','YYYY-mm-dd')";

			$this->oracle_db->set("CONDITION_CDE", $data['CONDITION_CDE']);
			$this->oracle_db->set("CONDITION_DSC", $data['CONDITION_DSC']);
			$this->oracle_db->set("MODBY", $data['MODBY']);
			$this->oracle_db->set("MODDTTM", $data['MODDTTM'], false);

			$this->oracle_db->where($this->id_conditions, $data[$this->id_conditions]);
			$results = $this->oracle_db->update($this->table_conditions);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Condition removal function
------------------------------------------------------------------------------------*/
	public function remove_condition( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_conditions, $id);
			$results = $this->oracle_db->delete($this->table_conditions);
			
			return $results;
		}
	}

}

/* End of file PWT_ETT_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/PWT_ETT_model.php */