<div class="row">
	<div class="col-md-12">
		<h2>Update summary offence ticket</h2>
		<p class="lead">This form will allow you to update the SOT.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php elseif (validation_errors()): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo validation_errors(); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>


<legend>Summary Offence Ticket</legend>
<div class="row">
	<div class="col-md-12">
	<form action="<?php echo base_url(); ?>SOT/update_SOT" method="post" >
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="SOT_NUM">SOT #</label>
					<input type="text" name="SOT_NUM" class="form-control" title="Issue Number" value="<?= $ticket['SOT_NUM'] ?>" disabled />
					<input type="hidden" name="SOT_NUM" value="<?= $ticket['SOT_NUM'] ?>"  />
				</div>
				<div class="col-sm-3">
					<label for="ISSUE_DTE">Issue Date</label>
					<input type="text" name="ISSUE_DTE" class="form-control date_picker" title="Issue Date" value="<?= $ticket['ISSUE_DTE'] ?>" readonly />
				</div>
				<div class="col-sm-3">
					<label for="CASE_NUM">Case Number</label>
					<input type="text" name="CASE_NUM" class="form-control" title="Case Number" value="<?= $ticket['CASE_NUM'] ?>" />
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="BYLAW_CDE">Bylaw Code</label>
					<input readonly type="text" name="bylaw" class="form-control" title="Bylaw Code" value="<?= $ticket['BYLAW_CDE'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="SECTION_CDE" >Section Code</label>
					<input readonly type="text" name="section" class="form-control" title="Section Code" value="<?= $ticket['SECTION_CDE'] ?>" />
				</div>
				<div class="col-sm-6">
					<label for="BYLAW_DSC">Bylaw</label>
					<textarea readonly class="form-control" title="Bylaw" maxlength="1000"><?= $ticket['BYLAW_DSC'] ?></textarea>
				</div>
			</div>
		</div>
		<legend>Update Bylaw - Use this section to pick a new bylaw for this ticket.</legend>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="BYLAW_CDE">Bylaw Code</label>
					<select name="BYLAW_CDE" class="form-control AJAX-BYLAW_CDE">
						<option value="">Select..</option>
						<?php foreach ($bylaws as $value): ?>
							<option value="<?= $value['BYLAW_CDE'] ?>"><?= $value['BYLAW_CDE'] ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-2">
					<label for="SECTION_CDE" onchange="get_desc(this)">Section Code</label>
					<select name="SECTION_CDE" class="form-control AJAX-SECTION_CDE" value="<?php echo set_value('SECTION_CDE'); ?>">
						<option value="">Select a Bylaw..</option>
					</select>
				</div>
				<div class="col-sm-6">
					<label for="bylaw_description">Bylaw</label>
					<textarea readonly name="bylaw_description" class="form-control AJAX-BYLAW_DSC" title="Bylaw" maxlength="1000"><?php echo set_value('bylaw_description'); ?></textarea>
				</div>
			</div>
		</div>

		<hr>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="ISSUE_ID">Officer</label>
					<input readonly type="text" name="ISSUE_ID" class="form-control" title="Officer" value="<?= $ticket['ISSUE_ID'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="business_unit">Business Unit/Resp</label>
					<input type="text" name="business_unit" class="form-control" title="Business Unit" value="<?= $employee['AGENCY_NME'] ?>" readonly  />
				</div>
				<div class="col-sm-4">
					<label for="system_name">System Name</label>
					<select name="SYSTEM_NME" class="form-control">
						<?php if (!empty($ticket['SYSTEM_NME'])): ?>
							<option value="<?= $ticket['SYSTEM_NME'] ?>"><?= $ticket['SYSTEM_NME'] ?></option>
							<option value="HANSEN">Hansen</option>
							<option value="BCT">BCT</option>
							<option value="ACS">ACS</option>
							<option value="FDM">FDM</option>
							<option value="OTHER">Other</option>
						<?php else :  ?>
							<option value="null">Select a system</option>
							<option value="HANSEN">Hansen</option>
							<option value="BCT">BCT</option>
							<option value="ACS">ACS</option>
							<option value="FDM">FDM</option>
							<option value="OTHER">Other</option>
						<?php endif ?>					
					</select>
				</div>
			</div>
		</div>

		<legend>Contact Address</legend>
		<!-- Get civic info by id -->
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="CONTACT_NUM">Contact #</label>
					<input type="text" name="CONTACT_NUM" class="form-control" title="Contact Number" value="<?= $ticket['CONTACT_NUM'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="title">Title</label>
					<input type="text" name="title" class="form-control" title="Title" value="<?= $contact['TITLE_DSC']?>" readonly placeholder="" />
				</div>
				<div class="col-sm-3">
					<label for="first_name">First Name</label>
					<input type="text" name="first_name" class="form-control" title="First Name" value="<?= $contact['FIRST_NME']?>" readonly placeholder="" />
				</div>
				<div class="col-sm-3">
					<label for="last_name">Last Name</label>
					<input type="text" name="last_name" class="form-control" title="Name" value="<?= $contact['LAST_NME']?>" readonly placeholder="" />
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<label for="civic_address">Civic Address</label>
					<input readonly type="text" name="civic_address" class="form-control" title="Civic Address" value="<?= $SOT_civic['MAILADDR1_DSC'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="city">City</label>
					<input readonly type="text" name="city" class="form-control" title="City" value="<?= $SOT_civic['MAILCITY_DSC'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="potal_code">Postal Code</label>
					<input readonly type="text" name="potal_code" class="form-control" title="Postal Code" value="<?= $SOT_civic['POSTAL_CDE'] ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="provence">Prov.</label>
					<input readonly type="text" name="provence" class="form-control" title="Province" value="<?= $SOT_civic['PROV_DSC'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="country">Country</label>
					<input readonly type="text" name="country" class="form-control" title="Country" value="<?= $SOT_civic['COUNTRY_DSC'] ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<?php if (!empty($contact_phone)): ?>
					<?php foreach ($contact_phone as $contact): ?>
						<?php 
							switch ($contact['PHONE_FLG']) {
								case 'H':
									?>
									<div class="col-sm-3">
										<label for="home_phone">Home Phone</label>
										<input readonly type="text" name="home_phone" class="form-control" title="Home Phone" value="(<?= $contact_phone[0]['AREA_CDE'] ?>) <?= $contact_phone[0]['PHONE_NUM'] ?>" />
									</div>
									<?php
										break;

									case 'W':
									?>
									<div class="col-sm-3">
										<label for="work_phone">Work Phone</label>
										<input readonly type="text" name="work_phone" class="form-control" title="Work Phone" value="(<?= $contact_phone[0]['AREA_CDE'] ?>) <?= $contact_phone[0]['PHONE_NUM'] ?>" />
									</div>
									<?php
										break;

									case 'F':
									?>
									<div class="col-sm-3">
										<label for="fax">Fax</label>
										<input readonly type="text" name="fax" class="form-control" title="Fax" value="(<?= $contact_phone[0]['AREA_CDE'] ?>) <?= $contact_phone[0]['PHONE_NUM'] ?>"/>
									</div>
									<?php
										break;

									case 'O':
									?>
									<div class="col-sm-3">
										<label for="other">Other</label>
										<input readonly type="text" name="other" class="form-control" title="Other" value="(<?= $contact_phone[0]['AREA_CDE'] ?>) <?= $contact_phone[0]['PHONE_NUM'] ?>"/>
									</div>
									<?php
										break;

									case 'P':
									?>
									<div class="col-sm-3">
										<label for="pager">Pager</label>
										<input readonly type="text" name="pager" class="form-control" title="Pager" value="(<?= $contact_phone[0]['AREA_CDE'] ?>) <?= $contact_phone[0]['PHONE_NUM'] ?>"/>
									</div>
									<?php
										break;
								default:
									break;
							}
						 ?>
					<?php endforeach ?>
				<?php endif ?>
			</div>
		</div>

		<legend>Summary Offence Information</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<!-- civic info by id -->
					<label for="address_of_summary_offence">Civic Address of Offence</label>
					<input readonly type="text" name="address_of_summary_offence" class="form-control" title="Civic Address Of Offence" value="<?= $SOT_civic['MAILADDR1_DSC'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="APT_NUM">Apt.</label>
					<input type="text" name="APT_NUM" class="form-control" title="Apartment" value="<?= $ticket['APT_NUM'] ?>" />
				</div>
				<div class="col-sm-3">
					<label for="DISTRICT_NUM">District</label>
					<input type="text" name="DISTRICT_NUM" class="form-control" title="District" value="<?= $ticket['DISTRICT_NUM'] ?>" />
				</div>
			</div>
		</div>
		

		<div class="form-group">
			<div class="row">
				<div class="col-sm-7">
					<label for="OFFENCELOC_DSC">Offence Location</label>
					<input type="text" name="OFFENCELOC_DSC" class="form-control" title="Office Location" value="<?= $ticket['OFFENCELOC_DSC'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="ARRAIGN_DTE">Arraign Date</label>
					<input type="text" name="ARRAIGN_DTE" class="form-control date_picker" title="Arrain Date" value="<?= $ticket['ARRAIGN_DTE'] ?>" readonly />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="COURT_DTE">Trial Date</label>
					<input type="text" name="COURT_DTE" class="form-control date_picker" title="Trial Date" value="<?= $ticket['COURT_DTE'] ?>" readonly />
				</div>
				<div class="col-sm-4">
					<label for="OUTCOME_DSC">Outcome</label>
					<select name="OUTCOME_DSC" class="form-control" title="Outcome" >
						<?php if (!empty($ticket['OUTCOME_DSC'])): ?>
							<option value="<?= $ticket['OUTCOME_DSC'] ?>"><?= $ticket['OUTCOME_DSC'] ?></option>
							<option value="GUILTY">Guilty</option>
							<option value="NOT GUILTY">Not Guilty</option>
							<option value="DROP CHARGE">Drop Charge</option>
							<option value="VOL PYMNT">Vol. Payment</option>
							<option value="IN ABSENCE">In Absence</option>
							<option value="WITHDRAWAL">Withdrawn</option>
							<option value="CHANGE OF PLEA">Change of Plea</option>
						<?php else : ?>
							<option value="">Please select..</option>
							<option value="GUILTY">Guilty</option>
							<option value="NOT GUILTY">Not Guilty</option>
							<option value="DROP CHARGE">Drop Charge</option>
							<option value="VOL PYMNT">Vol. Payment</option>
							<option value="IN ABSENCE">In Absence</option>
							<option value="WITHDRAWAL">Withdrawn</option>
							<option value="CHANGE OF PLEA">Change of Plea</option>
						<?php endif ?>
					</select>
				</div>
				<div class="col-sm-4">
					<label for="FINE_AMT">Fine Amount</label>
					<input type="text" name="FINE_AMT" class="form-control" title="Fine Amount" value="<?= $ticket['FINE_AMT'] ?>"  />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<label for="NOTE_DSC">Notes</label>
					<textarea name="NOTES_DSC" class="form-control NOTES_DSC" title="Notes" ><?= $ticket['NOTES_DSC'] ?></textarea>
				</div>
			</div>
		</div>
		<div class="hidden_inputs">
			<input type="hidden" name="ADD_BY" value="<?= $ticket['ADD_BY'] ?>" />
			<input type="hidden" name="ADD_DTE" value="<?= $ticket['ADD_DTE'] ?>" />
			<input type="hidden" name="BYLAW" value="<?= $ticket['BYLAW_CDE'] ?>" />
			<input type="hidden" name="SECTION" value="<?= $ticket['SECTION_CDE'] ?>" />
			<input type="hidden" name="TYPE_CDE" value="<?= $ticket['TYPE_CDE'] ?>" />
			<input type="hidden" name="BUSUNIT_CDE" value="<?= $ticket['BUSUNIT_CDE'] ?>" />
			<input type="hidden" name="REF_NUM" value="<?= $ticket['REF_NUM'] ?>" />
			<input type="hidden" name="CIV_ID" value="<?= $ticket['CIV_ID'] ?>" />
			<input type="hidden" name="CIVIC_NUM" value="<?= $ticket['CIVIC_NUM'] ?>" />
			<input type="hidden" name="STR_CODE" value="<?= $ticket['STR_CODE'] ?>" />
			<input type="hidden" name="CIVIC_EXT" value="<?= $ticket['CIVIC_EXT'] ?>" />
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_SOT pull-right" value="Update SOT" />
					<a href="<?php echo base_url(); ?>SOT/summary_offence_tickets" class="btn btn-md btn-default return_SOT">Back to SOT</a>
				</div>
			</div>
		</div>
	</form>
	</div>
</div>