<div class="row">
	<div class="col-md-12">
		<h2>24 Hour Automated traffic Recorder Studies</h2>
	</div>
</div>

<!-- Error reporting -->
<?php if (validation_errors()): ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<form action="<?php echo base_url(); ?>traffic/add_ATR" method="POST" >
	<div class="col-md-12">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="REC_CODE">Record #</label>
					<input type="text" name="REC_CODE" class="form-control" title="Record Number" maxlength="10" value="<?php echo set_value('REC_CODE') ?>" />
				</div>
				<div class="col-sm-2">
					<label for="ONE_WAY" class="no_label_fix">One Way Count</label>
					<input type="checkbox" name="ONE_WAY" class="" />
				</div>
				<div class="col-sm-2">
					<label for="TWO_WAY" class="no_label_fix">Two Way Count</label>
					<input type="checkbox" name="TWO_WAY" class="" />
				</div>
				<div class="col-sm-2">
					<label for="SP_SURVEY" class="no_label_fix">Special Survey</label>
					<input type="checkbox" name="SP_SURVEY" class="" />
				</div>
				<div class="col-sm-2">
					<label for="ISIN_IND_PARK" class="no_label_fix">Industrial Park</label>
					<input type="checkbox" name="ISIN_IND_PARK" class="" />
				</div>
				<div class="col-sm-2">
					<label for="CREATE_DATE">Created On</label>
					<input type="text" name="CREATE_DATE" class="form-control date_picker" title="Created On" readonly value="<?php echo set_value('CREATE_DATE') ?>" />
				</div>
			</div>
		</div>

		<legend>Mid Block</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<button type="button" value="STR_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
				</div>
				<div class="col-sm-1">
					<!-- <label for="STR_CODE">Street Code</label> -->
					<input type="hidden" name="STR_CODE" class="form-control street_code" readonly value="<?php echo set_value('STR_CODE') ?>" />
				</div>
				<div class="col-sm-3">
					<label for="">Street Name</label>
					<input type="text"  class="form-control  street_name" readonly />
				</div>
				<div class="col-sm-3">
					<label for="">Type</label>
					<input type="text" class="form-control  street_type" readonly />
				</div>
				<div class="col-sm-3">
					<label for="">Community</label>
					<input type="text" class="form-control  community" readonly />
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<button type="button" value="STR2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
				</div>
				<div class="col-sm-1">
					<!-- <label for="STR_CODE">Between</label> -->
					<input type="hidden" name="STR2_CODE" class="form-control 2_street_code" readonly value="<?php echo set_value('STR2_CODE') ?>" />
				</div>
				<div class="col-sm-3">
					<label for="">Between</label>
					<input type="text"  class="form-control 2_street_name" readonly />
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control no_label_fix 2_street_type" readonly />
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control no_label_fix 2_community" readonly />
				</div>
			</div>
		</div>
	
		<div class="form-group">
			<div class="row">
				<div class="col-sm-1">
					<button type="button" value="STR3_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
				</div>
				<div class="col-sm-1">
					<input type="hidden" name="STR3_CODE" class="form-control 3_street_code no_label_fix" readonly value="<?php echo set_value('STR3_CODE') ?>" />
				</div>
				<div class="col-sm-3">
					<input type="text"  class="form-control no_label_fix 3_street_name" readonly />
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control no_label_fix 3_street_type" readonly />
				</div>
				<div class="col-sm-3">
					<input type="text" class="form-control no_label_fix 3_community" readonly />
				</div>
			</div>
		</div>

		<legend>Location</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="SURVEY_DATE">Survey Date</label>
					<input type="text" name="SURVEY_DATE" class="form-control date_picker" title="Survey Date" readonly value="<?php echo set_value('SURVEY_DATE') ?>" />
				</div>
				<div class="col-sm-4">
					<label for="CIV_ADDR">Civic #</label>
					<input type="text" name="CIV_ADDR" class="form-control" title="Civic Number" maxlength="5" value="<?php echo set_value('CIV_ADDR') ?>" />
				</div>
				<div class="col-sm-4">
					<label for="STR_REG">Region</label>
					<select name="STR_REG" class="form-control" title="Region" maxlength="3" >
						<option value="">select..</option>
						<option value="EST">Eastern</option>
						<option value="WST">Western</option>
						<option value="CNT">Central</option>
					</select>
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<label for="X_COORD">GPS X</label>
					<input type="text" name="X_COORD" class="form-control" title="GPS X" maxlength="10" value="<?php echo set_value('X_COORD') ?>" />
				</div>
				<div class="col-sm-6">
					<label for="Y_COORD">GPS Y</label>
					<input type="text" name="Y_COORD" class="form-control" title="GPS Y" maxlength="10" value="<?php echo set_value('Y_COORD') ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<label for="COMMENT_BOX">Comments</label>
					<textarea name="COMMENT_BOX" class="form-control" title="Comments" maxlength="200" ><?php echo set_value('COMMENT_BOX') ?></textarea>
				</div>
			</div>
		</div>

		<legend>Details</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="SURVEY_TYPE">Survey Classification</label>
					<select name="SURVEY_TYPE" class="form-control" title="Survey Classification" maxlength="2" >
						<option value="">select..</option>
						<option value="BV">Basic Volume</option>
						<option value="SV">Speed & Volume</option>
						<option value="CV">Class & Volume</option>
						<option value="GV">Gap & Volume</option>
					</select>
				</div>
				<div class="col-sm-4">
					<label for="COUNTER_TYPE">Counter Type</label>
					<select name="COUNTER_TYPE" class="form-control" title="Counter Type" maxlength="2" >
						<option value="">select..</option>
						<option value="RT">Road Tube</option>
						<option value="MV" >Miovision</option>
						<option value="RD">RADAR</option>
					</select>
				</div>
				<div class="col-sm-2">
					<label for="SERIAL_NUM">Counter Number</label>
					<input type="text" name="SERIAL_NUM" class="form-control" title="Counter Number" maxlength="15" value="<?php echo set_value('SERIAL_NUM') ?>" />
				</div>
				<div class="col-sm-2">
					<label for="SURVEYOR">Surveyor</label>
					<input type="text" name="SURVEYOR" class="form-control pull-right surveyor_input" title="Surveyor" maxlength="3" />
				</div>
			</div>
		</div>

		<legend>Data</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="AAWT1">AAWT One Way</label>
					<input type="text" name="AAWT1" class="form-control" title="AAWT One Way" maxlength="6" value="<?php echo set_value('AAWT1') ?>" />
				</div>
				<div class="col-sm-3">
					<label for="AAWT1_DIR">Direction</label>
					<select name="AAWT1_DIR" class="form-control" title="Direction" maxlength="2" >
						<option value="">select..</option>
						<option value="SB">SB</option>
						<option value="EB">EB</option>
						<option value="WB">WB</option>
						<option value="NE">NE</option>
						<option value="NW">NW</option>
						<option value="SE">SE</option>
						<option value="SW">SW</option>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="AAWT1OP">AAWT One Way OP</label>
					<input type="text" name="AAWT1OP" class="form-control" title="AAWT One Way" maxlength="6" value="<?php echo set_value('AAWT1OP') ?>" />
				</div>
				<div class="col-sm-3">
					<label for="AAWT1OP_DIR">Direction</label>
					<select name="AAWT1OP_DIR" class="form-control" title="Direction" maxlength="2" >
						<option value="">select..</option>
						<option value="SB">SB</option>
						<option value="EB">EB</option>
						<option value="WB">WB</option>
						<option value="NE">NE</option>
						<option value="NW">NW</option>
						<option value="SE">SE</option>
						<option value="SW">SW</option>
					</select>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="AAWT2">AAWT Two Way</label>
					<input type="text" name="AAWT2" class="form-control" title="AWTT Two way" maxlength="6" value="<?php echo set_value('AAWT2') ?>" />
				</div>
			</div>
		</div> 

		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-primary btn-md pull-right" value="Add Survey" />
					<a href="<?php echo base_url(); ?>traffic/ATR" class="btn btn-default btn-md">Back to Survey</a>
				</div>
			</div>
		</div>
	</div>
	</form>
</div>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title">Street Look Up</h4>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="col-sm-5">
									<div class="modal_search">
										<label for="search_box">Street Name</label>
										<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
										<p class="text-muted">(Select the name of the record you would like to use.)</p>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<table class="table table-bordered">
							<thead>
								<th>Street</th>
							</thead>
							<tbody class="modal_tbody">
								<tr>
									
								</tr>
							</tbody>	
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->