<div class="row">
	<div class="col-md-12">
		<h2>File Attachments</h2>

	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<legend>24 Hour Automated Traffic Recorder Studies</legend>
		<div class="form-group">
			<div class="col-sm-4">
				<label for="file_name">File Name</label>
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<label for="request_id">Request #</label>
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<label for="attachment">Attachments</label>
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>

		<legend>Traffic Signal Warrant</legend>
		<div class="form-group">
			<div class="col-sm-4">
				<label for="file_name">File Name</label>
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<label for="request_id">Request #</label>
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<label for="attachment">Attachments</label>
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>	

		 <legend>Radar Spot Speed Studies</legend>
		<div class="form-group">
			<div class="col-sm-4">
				<label for="file_name">File Name</label>
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<label for="request_id">Request #</label>
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<label for="attachment">Attachments</label>
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>

		<legend>Pedestrian Studies</legend>
		<div class="form-group">
			<div class="col-sm-4">
				<label for="file_name">File Name</label>
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<label for="request_id">Request #</label>
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<label for="attachment">Attachments</label>
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>

		<legend>Manual Turning Movement Traffic Counts</legend>
		<div class="form-group">
			<div class="col-sm-4">
				<label for="file_name">File Name</label>
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<label for="request_id">Request #</label>
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<label for="attachment">Attachments</label>
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>

		<legend>Left Turn Investigation</legend>
		<div class="form-group">
			<div class="col-sm-4">
				<label for="file_name">File Name</label>
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<label for="request_id">Request #</label>
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<label for="attachment">Attachments</label>
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-4">
				<input type="text" name="file_name" class="form-control" title="File Name" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="request_id" class="form-control" title="Request Id" />
			</div>
			<div class="col-sm-4">
				<input type="text" name="attachment" class="form-control" title="Attachment" />
			</div>
		</div>
	</div>
</div>