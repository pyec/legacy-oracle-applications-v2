// Add Summary Offence Tickets 
// AJAX calls.
//-------------------------------
/*
	Here we  need to tie employees, contacts and bylaws
	to the SOT insert, so we're going to use AJAX calls 
	to retrieve the contact, emplyoyee and bylaw and autofill 
	the form.
*/
// Employee lookup
//------------------------------------------------------------------------------------------------
// I'm going to fully comment the first AJAX routine only, 
$('.AJAX-ISSUE_ID').keyup(function(event){

	// Get the current url from the window obj.
	var url = window.location.href;
	url = url.split("/");

	var value = this.id;

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var first_name = $('input[name="officer_first_search_box"]').val();
	var last_name = $('input[name="officer_last_search_box"]').val();

	// this will execute an ajax request with the parameters you set.
	$.ajax({
			url: BASE_URL+'SOT/ajax_employee',
			type: 'POST',
			dataType: 'JSON',
			data: {FIRST_NME: first_name, LAST_NME: last_name},
		})
	//on success the request is to
		.success(function(json) {
			var content = "";
			for (var i = 0; i < json.length; i++) 
			{
				content += "<tr>";
				content += '<td><button class="emp_modal_link" data-dismiss="modal" value="'+value+'" id="'+json[i]['EMPL_ID']+'">'+json[i]['EMPL_ID']+' - '+json[i]['FIRST_NME']+' '+json[i]['LAST_NME']+'</button></td>';
				content += "</tr>";
			};
			
			$('.modal_tbody').html(content);
		})
		// on fail the request is to
		.fail(function() {
		});
});

$('.modal_tbody').delegate('.emp_modal_link', 'click', function(event) {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var search_string = this.id;
	
	form_pos = this.value;

	$.ajax({
		url: BASE_URL+'SOT/ajax_employee',
			type: 'POST',
			dataType: 'JSON',
			data: {string: search_string},
		})
		.success(function(json) {
			for (var i = json.length - 1; i >= 0; i--) {
				if (json[i]['EMPL_ID'] == search_string) 
				{
					$('.ISSUE_ID').val(search_string);
				$('.officer_name').val(json[i]['FIRST_NME']+' '+json[i]['LAST_NME']);
				$('.AGENCY_NME').val(json[i]['AGENCY_NME']);
				}
			};
		});
});


// Bylaw lookup
//-----------------------------------------------------------------------------------------------
$('.AJAX-BYLAW_CDE').keyup(function(event){

	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = $('.AJAX-BYLAW_CDE').val();
	$.ajax({
		url: BASE_URL+'SOT/ajax_bylaw_codes',
			type: 'POST',
			dataType: 'JSON',
			data: {string: search_string},
		})
		.success(function(json) {
			var content = "";
			for (var i = 0; i < json.length; i++) 
			{
				content += "<tr>";
				content += '<td><button class="emp_modal_link" data-dismiss="modal" value="'+value+'" id="'+json[i]['EMPL_ID']+'">'+json[i]['EMPL_ID']+' - '+json[i]['FIRST_NME']+' '+json[i]['LAST_NME']+'</button></td>';
				content += "</tr>";
			};
			
			$('.modal_tbody').html(content);
			
		})
		.fail(function() {
			var source = ['No Results...'];
			$('.AJAX-BYLAW_CDE').autocomplete({
				source: source,
				maxLength: 5
			});
		});
});

$('.AJAX-BYLAW_CDE').change(function(e){
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = this.value;
	$.ajax({
		url: BASE_URL+'SOT/ajax_section_codes',
			type: 'POST',
			dataType: 'JSON',
			data: {string: search_string},
		})
		.success(function(json) {
			var html = '<option value="">Select a Code.</option>';
			for (var i = json.length - 1; i >= 0; i--) {
				html += '<option value="'+json[i]['SECTION_CDE']+'">'+json[i]['SECTION_CDE']+'</option>';
			};
			$('.AJAX-SECTION_CDE').html(html);

		})
		.fail(function() {
		});
});

$('.AJAX-SECTION_CDE').on("change", function(event) {

	var section_code = this.value;
	var bylaw_code = $('.AJAX-BYLAW_CDE').val();
	$('.AJAX-BYLAW_DSC').val(' ');
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	$.ajax({
		url: BASE_URL+'SOT/get_bylaw_description',
			type: 'POST',
			dataType: 'JSON',
			data: {bylaw_code: bylaw_code, section_code: section_code},
		})
		.success(function(json) {
			if (json) {
				for (var i = json.length - 1; i >= 0; i--) {
					$('.AJAX-BYLAW_DSC').val(json[i]['BYLAW_DSC']);
				};
			};
		})
		.fail(function() {
		});
});

// Contact lookup
//-----------------------------------------------------------------------------------------------

$('.AJAX-CONTACT_NUM').keyup(function(event){

	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var value = this.value;

	// Get the search string from the input box.
	var search_string = $('.AJAX-CONTACT_NUM').val();
	$.ajax({
		url: BASE_URL+'SOT/ajax_contact',
			type: 'POST',
			dataType: 'JSON',
			data: {string: search_string},
		})
		.success(function(json) {
			var content = "";
			for (var i = 0; i < json.length; i++) 
			{
				content += "<tr>";
				content += '<td><button class="con_modal_link" data-dismiss="modal" value="'+value+'" id="'+json[i]['CONTACT_NUM']+'">'+json[i]['CONTACT_NUM']+' - '+json[i]['FIRST_NME']+' '+json[i]['LAST_NME']+'</button></td>';
				content += "</tr>";
			};
			
			$('.modal_tbody').html(content);
		})
		.fail(function() {
		});
});

$('.modal_tbody').delegate('.con_modal_link', 'click', function(event) {

	var contact_id = this.id;

	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// AJAX call for the contact record.
	$.ajax({
		url: BASE_URL+'SOT/ajax_contact_by_id',
			type: 'POST',
			dataType: 'JSON',
			data: {id: contact_id},
		})
		.success(function(json) {
			for (var i = json.length - 1; i >= 0; i--) {
				$("input[name='CONTACT_NUM']").val(json[i]['CONTACT_NUM']);
				$("input[name='TITLE_DSC']").val(json[i]['TITLE_DSC']);
				$("input[name='FIRST_NME']").val(json[i]['FIRST_NME']);
				$("input[name='LAST_NME']").val(json[i]['LAST_NME']);
			};
			// Call for civic info on success of privious call.
			$.ajax({
				url: BASE_URL+'SOT/ajax_contact_civic_info',
				type: 'POST',
				dataType: 'JSON',
				data: {id: contact_id},
			})
			.success(function(json) {
				if (json) {
					for (var i = json.length - 1; i >= 0; i--) {
						$("input[name='COUNTRY_DSC']").val(json[i]['COUNTRY_DSC']);
						$("input[name='PROV_DSC']").val(json[i]['PROV_DSC']);
						$("input[name='POSTAL_CDE']").val(json[i]['POSTAL_CDE']);
					};
				};
				// Call for contact address info on success of privious call.
				$.ajax({
					url: BASE_URL+'SOT/ajax_contact_address',
					type: 'POST',
					dataType: 'JSON',
					data: {id: contact_id},
				})
				.success(function(json) {
					if (json) {
						for (var i = json.length - 1; i >= 0; i--) {
							$("input[name='CIVIC_ADDR']").val(json[i]['CIVIC_ADDR']);
							$("input[name='MAIL_ADDR']").val(json[i]['MAIL_ADDR']);
						};
					};
					// Call for contact phone info on success of privious call.
					$.ajax({
						url: BASE_URL+'SOT/ajax_contact_phone',
						type: 'POST',
						dataType: 'JSON',
						data: {id: contact_id},
					})
					.done(function(json) {
						// BOOM thats 4 consecutive AJAX calls. New record.
						if (json) {
							for (var i = json.length - 1; i >= 0; i--) {
								$("input[name='AREA_CDE']").val(json[i]['AREA_CDE']);
								$("input[name='PHONE_NUM']").val(json[i]['PHONE_NUM']);
								$("input[name='EXT_CDE']").val(json[i]['EXT_CDE']);
								$("input[name='PHONE_FLG']").val(json[i]['PHONE_FLG']);
							};
						};
					})
					.fail(function() {
					});	
				})
				.fail(function() {
				});
			})
			.fail(function() {
			});
		})
		.fail(function() {	
		});
});

// Civic Location lookup
//-----------------------------------------------------------------------------------------------
$('.AJAX-CIVIC_NUM').keyup(function(event){

	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = $('.AJAX-CIVIC_NUM').val();

	$.ajax({
		url: BASE_URL+'SOT/ajax_civic',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
		.success(function(json) {
			var source = [];

			if (json.length > 0) {
				for (var i = json.length - 1; i >= 0; i--) {
					source.push(json[i]['MAILADDR1_DSC']+' - '+json[i]['ADD_BY']);
				};
			};
			$('.AJAX-CIVIC_NUM').autocomplete({
				source: source,
				maxLength: 5
			});
			
		})
		.fail(function() {
			var source = ['No Results...'];
			$('.AJAX-CIVIC_NUM').autocomplete({
				source: source,
				maxLength: 5
			});
		});
});

$('.AJAX-CIVIC_NUM').on('autocompleteselect', function(event, ui){
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var id = this.value.split(' ');
	id = id[0];

	$.ajax({
		url: BASE_URL+'SOT/ajax_civic',
			type: 'POST',
			dataType: 'JSON',
			data: {id: id},
		})
		.success(function(json) {
			if (json) {
				for (var i = json.length - 1; i >= 0; i--) {
					$("input[name='CIVIC_NUM']").val(json[i]['CIVIC_NUM']);
					$("input[name='CIVIC_ADDRESS']").val(json[i]['MAILADDR1_DSC']);
				};
			};
			
		})
		.fail(function() {

		});
});

//CEMETERY AJAX CALLS 
//--------------------------------------------------------------------------------------------------------------------
$('.AJAX-CEM_REFNBR').keyup(function(event){

	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = $('.AJAX-CEM_REFNBR').val();

	$.ajax({
		url: BASE_URL+'cemetery/ajax_owners_by_id',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
		.success(function(json) {
			var source = [];

			if (json) {
				for (var i = json.length - 1; i >= 0; i--) {
					source.push(json[i]['CEM_REFNBR']+' - '+json[i]['CEM_FNAME']+' '+json[i]['CEM_LNAME']);
				};
			};
			$('.AJAX-CEM_REFNBR').autocomplete({
				source: source,
				maxLength: 5
			});
			
		})
		.fail(function() {
			var source = ['No Results...'];
			$('.AJAX-CEM_REFNBR').autocomplete({
				source: source,
				maxLength: 5
			});
		});
});

$('.AJAX-CEM_REFNBR').blur(function(event) {
	var id = this.value.split(' ');
	id = id[0];
	this.value = id;
});

// var delay = (function(){
//     var timer = 0;
//     return function(callback, ms){
//         clearTimeout (timer);
//         timer = setTimeout(callback, ms);
//     };
// })();
//
//
// $('.owner_first_name_search_box').keyup(function() {
//     delay(function(){
//         searchCemeteryOwners();
//     }, 100 );  // 100 ms delay
// });
//
// $('.owner_last_name_search_box').keyup(function() {
//     delay(function(){
//         searchCemeteryOwners();
//     }, 100 );  // 100 ms delay
// });
//
// function searchCemeteryOwners() {
//
//     var ownerFirstName = $('#owner_first_name')[0].value;
//     var ownerLastName = $('#owner_last_name')[0].value;
//
//     if (ownerFirstName.length < 3 && ownerLastName.length < 3)
//     {
//         $('.cem_modal_tbody').html('');
//         return false;
//     }
//
//     // Get the url for the AJAX call.
//     var url = window.location.href;
//     url = url.split("/");
//
//     // Create a base url to call
//     var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';
//
//     $.ajax({
//         url: BASE_URL+'cemetery/ajax_get_owners',
//         type: 'POST',
//         dataType: 'json',
//         data: {firstName: $('#owner_first_name')[0].value, lastName: $('#owner_last_name')[0].value}
//     })
//         .done(function(json) {
//             var content = "";
//             var ownerName = "";
//             var ownerNumber = "";
//
//             for (var i = 0; i < json.length; i++)
//             {
//                 var firstName = json[i]['CEM_FNAME'];
//                 var middleName = json[i]['CEM_MNAME'];
//                 var lastName = json[i]['CEM_LNAME'];
//
//                 if (middleName != null) {
//                     ownerName = firstName + ' ' + middleName + ' ' + lastName;
//                 }else{
//                     ownerName = firstName + ' ' + lastName;
//                 }
//
//                 ownerNumber = json[i]['CEM_REFNBR'];
//
//                 content += '<tr>';
//                 content += '<td><button class="cem_modal_link" data-dismiss="modal" value="'+json[i]['CEM_REFNBR']+'" id="'+json[i]['CEM_REFNBR']+'">'+ownerName+'</button></td>';
//                 content += '</tr>';
//             };
//
//
//             $('.cem_modal_tbody').html(content);
//         });
// }

$('.cem_modal_tbody').delegate('.cem_modal_link', 'click', function(event) {
    //// Get the url for the AJAX call.
    //var url = window.location.href;
    //url = url.split("/");

    //// Create a base url to call
    //var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

    var ownerId = this.id;
    var ownerName = this.innerHTML;

    $('input[name="CEM_REFNBR"]').val(ownerId);
    $('input[name="OwnerName"]').val(ownerName);
});

//EDL AJAX CALLS 
//--------------------------------------------------------------------------------------------------------------------

$('.modal_btn').click(function() {
	$('.search_box').val('');
	$('.modal_tbody').html('');
	$('.search_box').attr('id', this.value);
	$('.course_search_box').attr('id', this.value);

});


$('.search_box').keyup(function(event){

	var value = this.id;
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = $('.search_box').val();

	$.ajax({
		url: BASE_URL+'EDL/ajax_street_name',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
		.success(function(json) {
			var content = "";
			for (var i = 0; i < json.length; i++) 
			{
				content += '<tr>';
				content += '<td><button class="modal_link" data-dismiss="modal" value="'+value+'" id="'+json[i]['STREET_CDE']+'">'+json[i]['STREET_CDE']+' - '+json[i]['STREET']+' '+json[i]['STREET_TYP']+', '+json[i]['COMMUNITY']+'</button></td>';
				content += '</tr>';
			};

		
			$('.modal_tbody').html(content);
		})
		.fail(function() {
			
		});	
});

// This ajax call will return a series street records for the lookup
$('.modal_tbody').delegate('.modal_link', 'click', function(event) {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var search_string = this.id;
	
	form_pos = this.value;

	$.ajax({
		url: BASE_URL+'EDL/ajax_street_id',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
		.success(function(json) {
			switch(form_pos)
			{
				case "search_street":
                    $('#STREET_CODE-0').val(json['STREET_CDE']);
                    $('#STREET-0').val(json['STREET']);
                    $('#STREET_TYPE-0').val(json['STREET_TYP']);
                    $('#COMMUNITY-0').val(json['COMMUNITY']);
					break;

                case "add_surrounding":
                    $('#STREET_CODE_ADD').val(json['STREET_CDE']);
                    $('#STREET_ADD').val(json['STREET']);
                    $('#STREET_TYPE_ADD').val(json['STREET_TYP']);
                    $('#COMMUNITY_ADD').val(json['COMMUNITY']);
                    break;

                case 'search_surrounding_1' :
                    $('#STREET_CODE-1').val(json['STREET_CDE']);
                    $('#STREET-1').val(json['STREET']);
                    $('#STREET_TYPE-1').val(json['STREET_TYP']);
                    $('#COMMUNITY-1').val(json['COMMUNITY']);
                    // $('.ODDLOW').val(json['ODDLOW']);
                    // $('.ODDHI').val(json['ODDHI']);
                    // $('.EVENLOW').val(json['EVENLOW']);
                    // $('.EVENHI').val(json['EVENHI']);
                	break;

                case 'search_surrounding_2' :
                    $('#STREET_CODE-2').val(json['STREET_CDE']);
                    $('#STREET-2').val(json['STREET']);
                    $('#STREET_TYPE-2').val(json['STREET_TYP']);
                    $('#COMMUNITY-2').val(json['COMMUNITY']);
                    // $('.ODDLOW').val(json['ODDLOW']);
                    // $('.ODDHI').val(json['ODDHI']);
                    // $('.EVENLOW').val(json['EVENLOW']);
                    // $('.EVENHI').val(json['EVENHI']);
                    break;

				case 'street':
					$('input[name="STR_NAME"]').val(json['STREET']);
					$('input[name="STR_TYPE"]').val(json['STREET_TYP']);
					$('input[name="STR_NUMBER"]').val(json['STREET_CDE']);
					break;

				case 'limit_from' :
					$('input[name="LIMIT_FROM"]').val(json['STREET']);
					$('input[name="LIM_FRM_T"]').val(json['STREET_TYP']);
					$('input[name="L_F_NUMBER"]').val(json['STREET_CDE']);
					break;

				case 'limit_to' :
					$('input[name="LIMIT_TO"]').val(json['STREET']);
					$('input[name="LIM_TO_T"]').val(json['STREET_TYP']);
					$('input[name="L_T_NUMBER"]').val(json['STREET_CDE']);
					break;

				case 'location' :
					$('.street_name').val(json['STREET']);
					$('.street_type').val(json['STREET_TYP']);
					$('.street_number').val(json['STREET_CDE']);
					$('.area').val(json['COMMUNITY']);
					break;

				case 'surrounding' :
					$('.STREET_CDE').val(json['STREET_CDE']);
					$('.STREET').val(json['STREET']);
					$('.STREET_TYP').val(json['STREET_TYP']);
					$('.COMMUNITY').val(json['COMMUNITY']);
					$('.ODDLOW').val(json['ODDLOW']);
					$('.ODDHI').val(json['ODDHI']);
					$('.EVENLOW').val(json['EVENLOW']);
					$('.EVENHI').val(json['EVENHI']);
				break;

				case 'STR_CODE' :
					$('.street_name').val(json['STREET']);
					$('.street_type').val(json['STREET_TYP']);
					$('.street_code').val(json['STREET_CDE']);
					$('.community').val(json['COMMUNITY']);
				break;

                case 'surrounding-0' :
                    $('#STREET-0').val(json['STREET']);
                    $('#STREET_TYPE-0').val(json['STREET_TYP']);
                    $('#STREET_CODE-0').val(json['STREET_CDE']);
                    $('#COMMUNITY-0').val(json['COMMUNITY']);
                    break;

                case 'surrounding-1' :
                    $('#STREET-1').val(json['STREET']);
                    $('#STREET_TYPE-1').val(json['STREET_TYP']);
                    $('#STREET_CODE-1').val(json['STREET_CDE']);
                    $('#COMMUNITY-1').val(json['COMMUNITY']);
                    break;

                case 'surrounding-2' :
                    $('#STREET-2').val(json['STREET']);
                    $('#STREET_TYPE-2').val(json['STREET_TYP']);
                    $('#STREET_CODE-2').val(json['STREET_CDE']);
                    $('#COMMUNITY-2').val(json['COMMUNITY']);
                    break;

				case 'STR2_CODE' :
					$('.2_street_name').val(json['STREET']);
					$('.2_street_type').val(json['STREET_TYP']);
					$('.2_street_code').val(json['STREET_CDE']);
					$('.2_community').val(json['COMMUNITY']);
					break;

				case 'STR3_CODE' :
					$('.3_street_name').val(json['STREET']);
					$('.3_street_type').val(json['STREET_TYP']);
					$('.3_street_code').val(json['STREET_CDE']);
					$('.3_community').val(json['COMMUNITY']);
					break;
				
				case 'STR4_CODE' :
					$('.4_street_name').val(json['STREET']);
					$('.4_street_type').val(json['STREET_TYP']);
					$('.4_street_code').val(json['STREET_CDE']);
					$('.4_community').val(json['COMMUNITY']);
					break;

				case 'STR5_CODE' :
					$('.5_street_name').val(json['STREET']);
					$('.5_street_type').val(json['STREET_TYP']);
					$('.5_street_code').val(json['STREET_CDE']);
					$('.5_community').val(json['COMMUNITY']);
					break;


				case 'STRA_CODE' :
					$('.A_street_name').val(json['STREET']);
					$('.A_street_type').val(json['STREET_TYP']);
					$('.A_street_code').val(json['STREET_CDE']);
					$('.A_community').val(json['COMMUNITY']);
					break;

				case 'STRA2_CODE' :
					$('.A2_street_name').val(json['STREET']);
					$('.A2_street_type').val(json['STREET_TYP']);
					$('.A2_street_code').val(json['STREET_CDE']);
					$('.A2_community').val(json['COMMUNITY']);
					break;

				case 'STRA3_CODE' :
					$('.A3_street_name').val(json['STREET']);
					$('.A3_street_type').val(json['STREET_TYP']);
					$('.A3_street_code').val(json['STREET_CDE']);
					$('.A3_community').val(json['COMMUNITY']);
					break;

				case 'STRA4_CODE' :
					$('.A4_street_name').val(json['STREET']);
					$('.A4_street_type').val(json['STREET_TYP']);
					$('.A4_street_code').val(json['STREET_CDE']);
					$('.A4_community').val(json['COMMUNITY']);
					break;

				case 'STRA5_CODE' :
					$('.A5_street_name').val(json['STREET']);
					$('.A5_street_type').val(json['STREET_TYP']);
					$('.A5_street_code').val(json['STREET_CDE']);
					$('.A5_community').val(json['COMMUNITY']);
					break;

				case 'STRB_CODE' :
					$('.B_street_name').val(json['STREET']);
					$('.B_street_type').val(json['STREET_TYP']);
					$('.B_street_code').val(json['STREET_CDE']);
					$('.B_community').val(json['COMMUNITY']);
					break;

				case 'STRB2_CODE' :
					$('.B2_street_name').val(json['STREET']);
					$('.B2_street_type').val(json['STREET_TYP']);
					$('.B2_street_code').val(json['STREET_CDE']);
					$('.B2_community').val(json['COMMUNITY']);
					break;

				case 'STRB3_CODE' :
					$('.B3_street_name').val(json['STREET']);
					$('.B3_street_type').val(json['STREET_TYP']);
					$('.B3_street_code').val(json['STREET_CDE']);
					$('.B3_community').val(json['COMMUNITY']);
					break;
				case 'MAIN_CODE' :
					$('.main_street_name').val(json['STREET']);
					$('.main_street_type').val(json['STREET_TYP']);
					$('.main_street_code').val(json['STREET_CDE']);
					$('.main_community').val(json['COMMUNITY']);
					break;

				default:
				break;
			}
		});
});

$('.course_search_box').keyup(function(event){
	var value = this.id;
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = $('.course_search_box').val();

	$.ajax({
		url: BASE_URL+'TPW/ajax_courses',
			type: 'POST',
			dataType: 'JSON',
			data: {name: search_string},
		})
		.success(function(json) {
			var content = "";
			for (var i = 0; i < json.length; i++) 
			{
				content += '<tr>';
				content += '<td><button class="modal_link" data-dismiss="modal" value="'+value+'" id="'+json[i]['COURSE_NUM']+'">'+json[i]['COURSE_NUM']+'</button></td>';
				content += '<td>'+json[i]['COURSE_NME']+'</td>';
				content += '</tr>';
			};

		
			$('.courses_modal_tbody').html(content);
		})
		.fail(function() {
			
		});	
});
// This ajax call will return a series street records for the lookup
$('.courses_modal_tbody').delegate('.modal_link', 'click', function(event) {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var search_string = this.id;
	
	form_pos = this.value;

	$.ajax({
		url: BASE_URL+'TPW/ajax_course_by_id',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
		.success(function(json) {
			switch(form_pos)
			{
				case 'course':
					$('input[name="COURSE_NUM"]').val(json['COURSE_NUM']);
					$('.RECERT_PERIOD').val(json['RECERT_PERIOD']);
				break;
			}
		});
});


$('.AJAX-STREET_NAME').keyup(function(event){
	var value = this.id;
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = $('.AJAX-STREET_NAME').val();

	$.ajax({
		url: BASE_URL+'SOT/ajax_streets',
			type: 'POST',
			dataType: 'JSON',
			data: {name: search_string},
		})
		.success(function(json) {
			var content = "";
			for (var i = 0; i < json.length; i++) 
			{
				content += '<tr>';
				content += '<td><button class="modal_link" data-dismiss="modal" value="'+value+'" id="'+json[i]['STR_CODE']+'">'+json[i]['STR_CODE']+' - '+json[i]['STNAME']+' - '+json[i]['CITY']+'</button></td>';
				content += '</tr>';
			};

		
			$('.street_modal_tbody').html(content);
		})
		.fail(function() {
			
		});	
});


// This ajax call will return a series street records for the lookup
$('.street_modal_tbody').delegate('.modal_link', 'click', function(event) {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var search_string = this.id;
	
	form_pos = this.value;

	$.ajax({
		url: BASE_URL+'SOT/ajax_street_by_id',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
		.success(function(json) {
			$('.CIVIC_ADDRESS').val(json[0]['STNAME']+' '+json[0]['SUFFIX']+', '+json[0]['CITY']);
		});
});



$('.COURSE_DTE').blur(function(e){
	var RECERT_PERIOD = Number($('.RECERT_PERIOD').val());
	var class_date = $('.COURSE_DTE').val();

	if (RECERT_PERIOD != '') 
	{
		class_date = class_date.split('-');

		var year = Number(class_date[2]);
		class_date[2] = RECERT_PERIOD + year;

		var new_date = class_date[0]+'-'+class_date[1]+'-'+class_date[2];

		$('.EXPIRY_DTE').val(new_date);	
	}
});


$('.supervisor_first_search_box, .supervisor_last_search_box').keyup(function(event){
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var FIRST_NME = $('.supervisor_first_search_box').val();
	var LAST_NME = $('.supervisor_last_search_box').val();

    $.ajax({
        url: BASE_URL+'TPW/ajax_employees',
        type: 'POST',
        dataType: 'json',
        data: {FIRST_NME: FIRST_NME, LAST_NME: LAST_NME},
        success: function(json) {

            var content = "";
            for (var i = 0; i < json.length; i++)
            {
                content += '<tr>';
                content += '<td><button class="modal_link" data-dismiss="modal" value="" id="'+json[i]['EMPL_ID']+'">'+json[i]['EMPL_ID']+' - '+json[i]['FIRST_NME']+' '+json[i]['LAST_NME']+'</button></td>';
                content += '<td>'+json[i]['POSITION_NME']+'</td>';
                content += '</tr>';
            };


            $('.supervisor_modal_tbody').html(content);
        },
        fail: function() {

        },
        error: function(jqXHR, exception) {
            var msg = '';
            if (jqXHR.status === 0) {
                msg = 'Not connect.\n Verify Network.';
            } else if (jqXHR.status == 404) {
                msg = 'Requested page not found. [404]';
            } else if (jqXHR.status == 500) {
                msg = 'Internal Server Error [500].';
            } else if (exception === 'parsererror') {
                msg = 'Requested JSON parse failed.';
            } else if (exception === 'timeout') {
                msg = 'Time out error.';
            } else if (exception === 'abort') {
                msg = 'Ajax request aborted.';
            } else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
            alert(msg);
        }
    });
});
// This ajax call will return a series street records for the lookup
$('.supervisor_modal_tbody').delegate('.modal_link', 'click', function(event) {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var search_id = this.id;

	$.ajax({
		url: BASE_URL+'TPW/ajax_employee_by_id',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_id},
		})
		.success(function(json) {
			$('.SUPER_ID').val(json['EMPL_ID']);
			$('.SUPER_NAME').val(json['FIRST_NME']+' '+json['LAST_NME']);
			$('.POSITION_NME').val(json['POSITION_NME']);
		});
});

$( "#btnNoSuper" ).click(function() {
    $('.SUPER_ID').val('');
    $('.SUPER_NAME').val('');
    $('.POSITION_NME').val('');

    $('#myModal').modal('toggle');
});



$('select[name="LOCATION"]').change(function(e) {	
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var id = this.value;

	$.ajax({
		url: BASE_URL+'EDL/location_by_id',
		type: 'POST',
		dataType: 'JSON',
		data: {id: id},
	})
	.done(function(json) {
		$('input[name="location_name"]').val(json[0]['LOCATION_AREA']);
	});
});

$('select[name="ORIG_CDE"]').change(function(e) {	
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var id = this.value;

	$.ajax({
		url: BASE_URL+'EDL/originator_by_id',
		type: 'POST',
		dataType: 'JSON',
		data: {id: id},
	})
	.done(function(json) {
		$('input[name="originator_full_name"]').val(json[0]['ORIGINATOR_DSC']);
	});
});

$('select[name="PROJTYP_CDE"]').change(function(e) {	
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var id = this.value;

	$.ajax({
		url: BASE_URL+'EDL/project_by_id',
		type: 'POST',
		dataType: 'JSON',
		data: {id: id},
	})
	.done(function(json) {
		$('input[name="project_full_name"]').val(json[0]['PROJTYP_DSC']);
	});
});

$('.surveyor_list').change(function(event) {
	$('.surveyor_input').val(this.value);
});

/*-------------------------------------------------------------------------------------
	HRIS Training tracking ajax
--------------------------------------------------------------------------------------*/
$('.HRIS_modal_btn').click(function(event) {
	$('.emp_number_search_box').val('');
	$('.first_name_search_box').val('');
	$('.emp_number_search_box').val('');
	$('.modal_tbody').html('');
});

$('.emp_number_search_box').keyup(function(event) {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");
	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var id = this.value;
	$.ajax({
		url: BASE_URL+'HRIS_ETT/ajax_get_employees_by_id',
		type: 'POST',
		dataType: 'JSON',
		data: {id: id},
	})
	.done(function(json) {
		var content = "";

		for (var i = 0; i < json.length; i++) 
		{
			content += '<tr>';
			content += '<form action="'+BASE_URL+'/HRIS_ETT/add_course_employee/" method="post">';
			content += '<td><a href="'+BASE_URL+'/HRIS_ETT/add_course_employee/'+url[6]+'/'+url[7]+'/'+json[i]['EMP_NO']+'" class="emp_modal_link" value="'+json[i]['EMP_NO']+'">'+json[i]['EMP_NO']+'</a></td>';
			content += '<td>'+json[i]['GIVEN_NAME1']+'</td>';
			content += '<td>'+json[i]['SURNAME']+'</td>';
			content += '</form>';
			content += '</tr>';
		};


		$('.modal_tbody').html(content);
	});

});

$('.first_name_search_box').keyup(function(event) {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';
    var firstName = $('#first_name_search_box').val();
    var lastName = $('#last_name_search_box').val();

	$.ajax({
		url: BASE_URL+'HRIS_ETT/ajax_get_employees_by_first_name',
		type: 'POST',
		dataType: 'json',
		data: {lastName: lastName, firstName: firstName},
		})
		.done(function(json) {
			var content = "";

			for (var i = 0; i < json.length; i++) 
			{
				content += '<tr>';
				content += '<form action="'+BASE_URL+'/HRIS_ETT/add_course_employee/" method="post">';
				content += '<td><a href="'+BASE_URL+'/HRIS_ETT/add_course_employee/'+url[6]+'/'+url[7]+'/'+json[i]['EMP_NO']+'" class="emp_modal_link" value="'+json[i]['EMP_NO']+'">'+json[i]['EMP_NO']+'</a></td>';
				content += '<td>'+json[i]['GIVEN_NAME1']+'</td>';
				content += '<td>'+json[i]['SURNAME']+'</td>';
				content += '</form>';
				content += '</tr>';
			};

		
			$('.modal_tbody').html(content);
		});
});

$('.last_name_search_box').keyup(function(event) {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';
	var firstName = $('#first_name_search_box').val();
	var lastName = $('#last_name_search_box').val();

	$.ajax({
		url: BASE_URL+'HRIS_ETT/ajax_get_employees_by_last_name',
		type: 'POST',
		dataType: 'json',
		data: {lastName: lastName, firstName: firstName},
		})
		.done(function(json) {
			var content = "";

			for (var i = 0; i < json.length; i++) 
			{
				content += '<tr>';
				content += '<form action="'+BASE_URL+'/HRIS_ETT/add_course_employee/" method="post">';
				content += '<td><a href="'+BASE_URL+'/HRIS_ETT/add_course_employee/'+url[6]+'/'+url[7]+'/'+json[i]['EMP_NO']+'" class="emp_modal_link" value="'+json[i]['EMP_NO']+'">'+json[i]['EMP_NO']+'</a></td>';
				content += '<td>'+json[i]['GIVEN_NAME1']+'</td>';
				content += '<td>'+json[i]['SURNAME']+'</td>';
				content += '</form>';
				content += '</tr>';
			};

		
			$('.modal_tbody').html(content);
		});
});

$('.HRIS_modal_clear').click(function(event) {
	$('.emp_number_search_box').val('');
	$('.first_name_search_box').val('');
	$('.last_name_search_box').val('');
});



//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Traffic ajax calls - open record attachments

$('.open_attachments').click(function() {
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var form = this.id;
	
	// perform a series of ajax calls in a loop to determine how many files are attached to the record.
	var count = 0;
	var loop = true;





	$.ajax({
	url: BASE_URL+'traffic/ajax_does_survey_have_attachments',
		type: 'POST',
		dataType: 'json',
		data: {id: url[6], form: form},
	})
	.success(function(count) {

		for (var i = 0; i <= count; i++) 
		{
			if (i == 0) 
			{
				window.open('/HRMApplications/assets/uploads/Traffic_Survey_System/'+form+'/'+url[6]+'_.pdf', '_blank', 'scrollbar');
			}
			else
			{
				window.open('/HRMApplications/assets/uploads/Traffic_Survey_System/'+form+'/'+url[6]+'_'+i+'.pdf', '_blank', 'scrollbar');
			}
			
		};

		if (count == -1) 
		{
			alert('No files');
		}
	})
	.fail(function() {
		
	});	


});

// MCT STREET LOOKUP
$('.MCT_search_box').keyup(function(event){
	var value = this.id;
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = $('.MCT_search_box').val();

	$.ajax({
		url: BASE_URL+'EDL/ajax_street_name',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
		.success(function(json) {
			var content = "";
			for (var i = 0; i < json.length; i++) 
			{
				content += '<tr>';
				content += '<td><button class="MCT_modal_link" data-dismiss="modal" value="'+value+'" id="'+json[i]['STREET_CDE']+'">'+json[i]['STREET_CDE']+' - '+json[i]['STREET']+' '+json[i]['STREET_TYP']+', '+json[i]['COMMUNITY']+'</button></td>';
				content += '</tr>';
			};

		
			$('.modal_tbody').html(content);
		})
		.fail(function() {
			
		});	
});

$('.modal_tbody').delegate('.MCT_modal_link', 'click', function(event) {

	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var search_string = this.id;
	
	form_pos = this.value;

	$.ajax({
		url: BASE_URL+'EDL/ajax_street_id',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
	.success(function(json) {

		$('.street_name').val(json['STREET']+' '+json['STREET_TYP']);
		$('.street_code').val(json['STREET_CDE']);

		var civic_number = $('.MCT_CVC_NUM').val();
		var civic_name = json['STREET'];

		if (civic_number != '') 
		{
			distrct_lookup(civic_number, civic_name);
		}
	});
});

$('.MCT_CVC_NUM').blur(function(e){
	var civic_number = $('.MCT_CVC_NUM').val();
	var civic_name = $('.street_name').val();

	if (civic_name != '' && civic_number != '') 
	{
		civic_name = civic_name.split(' ');

		distrct_lookup(civic_number, civic_name[0]);
	}
});

//District Number lookup

function distrct_lookup(civic_number, civic_name)
{
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	$.ajax({
		url: BASE_URL+'MCT/ajax_district_lookup',
		type: 'POST',
		dataType: 'JSON',
		data: {civic_number: civic_number, civic_name: civic_name},
	})
	.success(function(json) {
		$('.MCT_DISTRICT').val(json);
	});
}

// Phonebook asset street lookup

// MCT STREET LOOKUP
$('.ASSET_search_box').keyup(function(event){
	var value = this.id;
	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	// Get the search string from the input box.
	var search_string = $('.ASSET_search_box').val();

	$.ajax({
		url: BASE_URL+'EDL/ajax_street_name',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
		.success(function(json) {
			var content = "";
			for (var i = 0; i < json.length; i++) 
			{
				content += '<tr>';
				content += '<td><button class="ASSET_modal_link" data-dismiss="modal" value="'+value+'" id="'+json[i]['STREET_CDE']+'">'+json[i]['STREET_CDE']+' - '+json[i]['STREET']+' '+json[i]['STREET_TYP']+', '+json[i]['COMMUNITY']+'</button></td>';
				content += '</tr>';
			};

		
			$('.modal_tbody').html(content);
		})
		.fail(function() {
			
		});	
});

$('.modal_tbody').delegate('.ASSET_modal_link', 'click', function(event) {

	// Get the url for the AJAX call.
	var url = window.location.href;
	url = url.split("/");

	// Create a base url to call
	var BASE_URL = url[0]+'//'+url[2]+'/'+url[3]+'/';

	var search_string = this.id;
	
	form_pos = this.value;

	$.ajax({
		url: BASE_URL+'EDL/ajax_street_id',
			type: 'POST',
			dataType: 'JSON',
			data: {id: search_string},
		})
	.success(function(json) {
		$('.street_name').val(json['STREET']+' '+json['STREET_TYP']);
		$('.address_id').val(json['STREET_CDE']);
	});
});