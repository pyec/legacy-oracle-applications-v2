<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Phone Book Model

Description : This is the data layer for the phone book.

  Functions : get_employees( $offset = 0 ),
			get_employee_by_id( $id = null ),
			get_employee_by_name( $name = null ),
			insert_employee( $data = null ),
			update_employee( $id = null ),
			remove_employee( $id = null )

=========================================================================================================================*/
class Phonebook_model extends CI_Model 
{
	//Properties
	public $asset_db = null;


	//Constructor
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies

		$this->assets_db = $this->load->database('mssql_asset_db', TRUE);
	}

/*-----------------------------------------------------------------------------------
	Employee retrieval functions
------------------------------------------------------------------------------------*/
	public function get_employees( $offset = 0 )
	{
		$query = $this->db->get($this->table_employees, $offset, $limit);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_employee_by_id( $id = null )
	{
		$this->db->where($this->id_employees, $id);
		$query = $this->db->get($this->table_employees);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function get_employee_by_first_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->db->where($this->first_name_employees, $name);
			$query = $this->db->get($this->table_employees);

			if ($query->num_rows() > 0)
			{
				return $query->row_result();
			}
			else
			{
				return false;
			}
		}
	}

	public function get_employee_by_last_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$this->db->where($this->last_name_employees, $name);
			$query = $this->db->get($this->table_employees);

			if ($query->num_rows() > 0)
			{
				return $query->row_result();
			}
			else
			{
				return false;
			}
		}
	}

/*-----------------------------------------------------------------------------------
	Employee insertion function
------------------------------------------------------------------------------------*/
	public function insert_employee( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->db->insert($this->table_employees, $data);
		}
	}

/*-----------------------------------------------------------------------------------
	Employee update function
------------------------------------------------------------------------------------*/
	public function update_employee( $id = null )
	{
		if ($data = null)
		{
			return false;
		}
		else
		{
			$this->db->where($this->id_employees, $data[$this->id_employees]);
			$this->db->update($this->table_employees, $data);
		}
	}

/*-----------------------------------------------------------------------------------
	Employee removal function
------------------------------------------------------------------------------------*/
	public function remove_employee( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->db->where($this->id_employees, $id);
			$this->db->delete($this->table_employees);
		}
	}


	public function search_assets( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!empty($data['name'])){$this->assets_db->like('name', $data['name']);}
			if (!empty($data['business_unit'])){$this->assets_db->like('business_unit', $data['business_unit']);}
			if (!empty($data['phone_number'])){$this->assets_db->like('phone_number', $data['phone_number']);}
			if (!empty($data['civic_number'])){$this->assets_db->like('civic_number', $data['civic_number']);}
			if (!empty($data['street_name'])){$this->assets_db->like('street_name', $data['street_name']);}

			$query = $this->assets_db->get('hrm_assets');

			if ($query->num_rows() > 0)
			{
				return $query->result_array();	
			}
			else
			{
				return false;
			}
		}
	}

	public function get_asset_by_id( $id = null )
	{
		if ($id != null)
		{
			$this->assets_db->where('id', $id);
			$query = $this->assets_db->get('hrm_assets');

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

	public function add_asset( $data = null )
	{
		if ($data != null)
		{
			$results = $this->assets_db->insert('hrm_assets', $data);

			return $results;
		}
		else
		{
			return false;
		}
	}

	public function update_asset( $data = null )
	{
		if ($data != null)
		{
			$this->assets_db->where('id', $data['id']);
			unset($data['id']);
			$results = $this->assets_db->update('hrm_assets', $data);

			return $results;
		}
		else
		{
			return false;
		}
	}
}

/* End of file phonebook_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/phonebook_model.php */