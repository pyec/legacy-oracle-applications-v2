<div class="row">
	<div class="col-md-6">
		<h2>Add Call Types</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>MCT/add_type" method="POST">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label  for="call_type_name">Call Type Name</label>
						<input type="text" name="MCT_TYPE_DESC" class="form-control" />
					</div>
					<div class="col-sm-4">
						<input type="submit" class="btn btn-success btn-md no_label_fix" value="Add Call Type" />
					</div>
				</div>
			</div>
		</form>
	</div>	
</div>