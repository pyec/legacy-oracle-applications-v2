
 <div class="row">
	<div class="col-md-12">
		<h2>Update Employee courses</h2>
		<p class="lead">This form will allow you to update an employees courses.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<div class="row">
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="EMPL_ID">Employee ID</label>
						<input type="text" name="EMPL_ID" class="form-control" title="Condition Code" maxlength="10" value="<?= $employee['EMPL_ID'] ?>" disabled  />
						<input type="hidden" name="EMPL_ID" value="<?= $employee['EMPL_ID'] ?>"   />
					</div>

					<div class="col-sm-3">
						<label for="FIRST_NME">First Name</label>
						<input type="text" name="FIRST_NME" class="form-control" title="First Name" maxlength="40" value="<?= $employee['FIRST_NME'] ?>" disabled />
						<input type="hidden" name="FIRST_NME" value="<?= $employee['FIRST_NME'] ?>" />
					</div>

					<div class="col-sm-3">
						<label for="MIDDLE_NME">Middle Initial</label>
						<input type="text" name="MIDDLE_NME" class="form-control" title="Middle Name" maxlength="3" value="<?= $employee['MIDDLE_NME'] ?>" disabled />
						<input type="hidden" name="MIDDLE_NME" value="<?= $employee['MIDDLE_NME'] ?>" />
					</div>

					<div class="col-sm-3">
						<label for="LAST_NME">Last Name</label>
						<input type="text" name="LAST_NME" class="form-control" title="Last Name" maxlength="40" value="<?= $employee['LAST_NME'] ?>" disabled />
						<input type="hidden" name="LAST_NME" value="<?= $employee['LAST_NME'] ?>" />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="POSITION_NME">Position Name</label>
						<input type="text" name="POSITION_NME" class="form-control" title="Position Name" maxlength="40" value="<?= $employee['POSITION_NME'] ?>" disabled  />
						<input type="hidden" name="POSITION_NME" value="<?= $employee['POSITION_NME'] ?>"   />
					</div>

					<div class="col-sm-3">
						<label for="SECTION_NME">Section Name</label>
						<input type="text" name="SECTION_NME" class="form-control" title="Section Name" maxlength="40" value="<?= $employee['SECTION_NME'] ?>" disabled />
						<input type="hidden" name="SECTION_NME" value="<?= $employee['SECTION_NME'] ?>"  />
					</div>

					<div class="col-sm-2">
						<label for="AREA_CDE">Area/District</label>
						<input type="text" name="AREA_CDE" class="form-control" title="Area/District" maxlength="10" value="<?= $employee['AREA_CDE'] ?>" disabled />
						<input type="hidden" name="AREA_CDE" value="<?= $employee['AREA_CDE'] ?>"  />
					</div>

					<div class="col-sm-3">
						<label for="SUPER_ID">Supervisors id</label>
						<input type="text" name="SUPER_ID" class="form-control" title="Supervisor ID" maxlength="70" value="<?= $employee['SUPER_ID'] ?>" disabled />
						<input type="hidden" name="SUPER_ID" value="<?= $employee['SUPER_ID'] ?>"  />
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="DRIVELIC_FLG">Drivers Licence on File?</label>
						<input type="text" name="DRIVELIC_FLG" class="form-control" value="<?php echo $value = ($employee['DRIVELIC_FLG'] == 'Y' ? 'Yes':'No'); ?>" disabled placeholder="" />
						<input type="hidden" name="DRIVELIC_FLG" value="<?php echo $value = ($employee['DRIVELIC_FLG'] == 'Y' ? 'Yes':'No'); ?>"  />
					</div>
					<div class="col-sm-2">
						<label for="DRIVEABS_FLG">Drivers abstract on File?</label>
						<input type="text" name="DRIVEABS_FLG" class="form-control" value="<?php echo $value = ($employee['DRIVEABS_FLG'] == 'Y' ? 'Yes':'No'); ?>" disabled placeholder="" />
						<input type="hidden" name="DRIVEABS_FLG" value="<?php echo $value = ($employee['DRIVEABS_FLG'] == 'Y' ? 'Yes':'No'); ?>" />
					</div>
				</div>
			</div>

			<div class="row">
			<legend>Courses</legend>
				<div class="col-md-12">
					<?php 
						// $expired = 0;
						// $current_date = date('d-M-y');

						// if (!empty($courses))
						// {
						// 	foreach ($courses as $course) 
						// 	{
						// 		if (!empty($course['EXPIRY_DTE']))
						// 		{
						// 			$exploded_date = explode('-', $course['EXPIRY_DTE']);
						// 			$exploded_current_date = explode('-', $current_date);

						// 			if ($exploded_date[2] >= $exploded_current_date[2])
						// 			{
						// 				if ($exploded_date[1] >= $exploded_current_date[1])
						// 				{
						// 					if ($exploded_date[0] < $exploded_current_date[0])
						// 					{
						// 						$expired++;
						// 					}
						// 				}
						// 				else
						// 				{
						// 					$expired++;
						// 				}
						// 			}
						// 			else
						// 			{
						// 				$expired++;
						// 			}
						// 		}
						// 	}
						// }
						
					 ?>
					<?php if (!empty($courses)): ?>
						<h2 class="count"><?= count($courses) ?> courses total</h2>
					<?php else: ?>
						<h2 class="count">No Courses Found</h2>
					<?php endif ?>
					<a href="<?= base_url(); ?>TPW/add_employee_course/<?= $employee['EMPL_ID'] ?>" class="btn btn-md btn-success pull-right">Add Course</a>
				</div>
			</div>
			
			<br>

		<?php if (!empty($courses)): ?>
			<div class="form-group">
				<div class="row">
					<table class="table table-bordered ">
						<thead>
							<th>Course Number</th>
							<th>Name</th>
							<th style="width: 110px;">Course Date</th>
							<th>Recert Flg</th>
							<th style="width: 110px;">Expiry Date</th>
							<th>Pass Flg</th>
							<th>Comments</th>
							<th>Delete</th>
						</thead>
						<tbody class="tbody">
								<?php foreach ($courses as $course): ?>
									<tr>
										<td><a href="<?php echo base_url(); ?>TPW/update_employee_course/<?= $employee['EMPL_ID'] ?>/<?= $course['COURSE_NUM'] ?>"><?= $course['COURSE_NUM'] ?></a></td>
										<td><?= $course['COURSE_NME'] ?></td>
										<td style="width: 110px;"><?= $course['COURSE_DTE'] ?></td>
										<td><?= $course['RECERT_FLG'] ?></td>
										<td style="width: 110px;"><?= $course['EXPIRY_DTE'] ?></td>
										<td><?= $course['PASS_FLG'] ?></td>
										<td><?= $course['COURSE_COMMENTS'] ?></td>
										<td>
											<form action="<?= base_url(); ?>TPW/delete_employee_course/" method="POST" >
												<button type="submit" class="btn btn-sm btn-danger delete_course"><i class="fa fa-minus-square"></i></button>
												<input type="hidden" name="COURSE_NUM" value="<?= $course['COURSE_NUM'] ?>" />
												<input type="hidden" name="EMPL_ID" value="<?= $employee['EMPL_ID'] ?>" />
											</form>
										</td>
									</tr>
								<?php endforeach ?>
						</tbody>	
					</table>
				</div>
			</div>
			<?php endif ?>
		</div>
	<a href="<?php echo base_url(); ?>TPW/employee_courses" class="btn btn-default btn-md">Back to Employee Lookup</a>
</div>