<!-- App title -->
<div class="row">
	<div class="col-md-6">
		<h2>Street File Update</h2>	
	</div>
</div>

<!-- Error reporting-->
<div class="row">
	<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php elseif (validation_errors()): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo validation_errors(); ?></p>
		</div>
	<?php endif ?>

	<!-- Search form -->
	<form action="<?php echo base_url(); ?>street_file/search" method="POST">
		<div class="form-group">
			<div class="col-sm-3">
				<label for="code">Street Code</label>
				<input type="text" name="code" class="form-control code" title="Street Code" value="<?php echo set_value('code') ?>" tabindex="1" placeholder="Street Code" />
			</div>
			<div class="col-sm-3">
				<label for="communities">Community</label>
				<select  name="communities" class="form-control community" title="Community" tabindex="2" >
					<option value=" " <?php echo set_select('communities', '', true); ?>>All</option>
					<?php foreach ($communities as $comm): ?>
						<option value="<?= strtoupper($comm['CITY']) ?>" <?php echo set_select('communities', strtoupper($comm['CITY'])); ?>><?= ucwords(strtolower($comm['CITY'])) ?></option>
					<?php endforeach ?>
				</select>
			</div>
			<div class="col-sm-3">
				<label for="name">Street Name</label>
				<input type="text" name="name" class="form-control name" title="Street Name" value="<?php echo set_value('name') ?>" tabindex="3" placeholder="Street Name" />
			</div>
			<div class="col-sm-3">
				<button type="submit" class="btn btn-primary btn-md no_label_fix phonebook_search_submit street_file_search_submit pull-right" tabindex="4"><i class="fa fa-search">&nbsp;</i>Search</button>
				<button type="reset" class="btn btn-default btn-md no_label_fix pull-right" tabindex="9" onclick="street_file_form_reset()">Clear</button>		
			</div>
		</div>
	</form>
</div>

<legend class="phonebook_legend"></legend>
<div class="row">
	<!-- Display the count of streets in a given search -->
	<div class="col-md-3">
		<?php if (!empty($streets)): ?>
			<h2 class="count"><?php echo count($streets) ?> Streets</h2>
		<?php endif ?>
	</div>
	<!-- Add record button -->
	<div class="col-md-9 col-md-offset-3">
		<a class="btn btn-success btn-md pull-right" href="<?php echo base_url(); ?>street_file/street">Add Street</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<!-- Results table -->
		<?php if (!empty($streets)): ?>
			<table class="table table-bordered table-striped table-hover">
				<thead>
					<th>Street Code</th>
					<th>Name</th>
					<th>Type</th>
					<th>Community</th>
				</thead>
				<tbody>
					
						<?php foreach ($streets as $street): ?>
							<tr>
								<td><a href="<?php echo base_url(); ?>street_file/street/<?php echo $street['STR_CODE'] ?>"><?php echo $street['STR_CODE'] ?></a></td>
								<td><?php echo ucwords(strtolower($street['STNAME'])) ?></td>
								<td><?php echo ucwords(strtolower($street['SUFFIX'])) ?></td>
								<td><?php echo ucwords(strtolower($street['CITY'])) ?></td>
							</tr>
						<?php endforeach ?>
				</tbody>
			</table>
		<?php endif ?>
		
		<?php if (!empty($pag_links)): ?>
			<div class="col-sm-12">
				<?php echo $pag_links ?>
			</div>
		<?php endif ?>
	</div>
</div>