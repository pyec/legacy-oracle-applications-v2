<?php 
// determine if the user is an admin 
	$is_admin = false;
	$read_only = true;
	$is_readwrite = false;

	$groups = $this->session->userdata('groups');

	foreach ($groups as $group) 
	{
		//if ($group == 'AppHRMAppsEngineeringDrawingLocationReadOnly' || $group == 'AppHRMAppsAdmin' )
        if ($group == 'AppHRMAppsEngineeringDrawingLocationReadOnly' )
		{
			$read_only = true;
		}

        if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite')
        //if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite' || $group == 'AppHRMAppsAdmin')
        {
            $is_readwrite = true;
            $read_only = false;
        }

		if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
        //if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin')
		{
			$is_admin = true;
            $is_readwrite = false;
			$read_only = false;
		}
	}
 ?>
<div class="row">
	<div class="col-md-6">
		<h2 style="padding-bottom: 1em">Update Drawing Location</h2>
<!--		<p class="lead">Use this form to update a drawing and location in the system.</p>-->
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p>
            <?php echo $this->session->flashdata('message'); ?>
            <?php if(strrpos($this->session->flashdata('message'),"added") != false): ?>
                <a style="margin-left:1em" href="<?php echo base_url(); ?>EDL/" class="btn btn-success btn-sm pull-right">Back to EDL Lookup</a>
                <a href="<?php echo base_url(); ?>EDL/add_location" class="btn btn-success btn-sm pull-right">Add Record</a>
            <?php endif ?>
            <?php if(strrpos($this->session->flashdata('message'),"updated") != false): ?>
                <a href="<?php echo base_url(); ?>EDL/" class="btn btn-success btn-sm pull-right">Back to EDL Lookup</a>
            <?php endif ?>
        </p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
        <p><?php echo validation_errors(); ?></p>
    </div>
<?php elseif($this->session->userdata('error')) : ?>
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
        <p><?php echo $this->session->userdata('error'); ?></p>
        <?php $this->session->unset_userdata('error'); ?>
    </div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
		<form action="<?php echo base_url(); ?>EDL/update_location/<?= $file[0]['FILE_ID'] ?>" method="POST">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="FILE_ID">File ID</label>
						<?php if ($is_admin): ?>
							<input type="text" name="FILE_ID" class="form-control" title="File ID" maxlength="9" value="<?= $file[0]['FILE_ID'] ?>" />
						<?php else: ?>
							<input type="text" name="FILE_ID" class="form-control readOnlyInputField" title="File ID" maxlength="9" value="<?= $file[0]['FILE_ID'] ?>" readonly />
							<input type="hidden" name="FILE_ID" value="<?= $file[0]['FILE_ID'] ?>" />
						<?php endif ?>
					</div>
				
					<div class="col-sm-2">
						<label for="PROJ_NUM">Project #</label>
						<?php if ($read_only): ?>
							<input readonly type="text" name="PROJ_NUM" class="form-control PROJ_NUM readOnlyInputField" title="Project Number" placeholder="99-99999" maxlength="8" value="<?= $file[0]['PROJ_NUM'] ?>" />
						<?php else: ?>
							<input type="text" name="PROJ_NUM" class="form-control PROJ_NUM" title="Project Number" placeholder="99-99999" maxlength="8" value="<?= $file[0]['PROJ_NUM'] ?>" />
						<?php endif ?>
					</div>
					<div class="col-sm-2">
						<label for="TENDER_NUM">Tender #</label>
						<?php if ($read_only): ?>
						<input readonly type="text" name="TENDER_NUM" class="form-control readOnlyInputField" title="Tender Number" maxlength="8" placeholder="YYYY-999" value="<?= $file[0]['TENDER_NUM'] ?>" />
						<?php else: ?>
						<input type="text" name="TENDER_NUM" class="form-control TENDER_NUM" title="Tender Number" maxlength="8" placeholder="YYYY-999" value="<?= $file[0]['TENDER_NUM'] ?>" />
						<?php endif ?>
					</div>
					<div class="col-sm-3">
						<label for="FROM_PLAN">From Plan</label>
						<?php if ($read_only): ?>
							<input readonly type="text" name="FROM_PLAN" class="form-control readOnlyInputField" title="From Plan" maxlength="12" value="<?= $file[0]['FROM_PLAN'] ?>" />
						<?php else: ?>
							<input type="text" name="FROM_PLAN" class="form-control" title="From Plan" maxlength="12" value="<?= $file[0]['FROM_PLAN'] ?>" />
						<?php endif ?>
					</div>
					<div class="col-sm-3">
						<label for="TO_PLAN">To Plan</label>
						<?php if ($read_only): ?>
							<input readonly type="text" name="TO_PLAN" class="form-control readOnlyInputField" title="To Plan" maxlength="12" value="<?= $file[0]['TO_PLAN'] ?>" />
						<?php else: ?>
							<input type="text" name="TO_PLAN" class="form-control" title="To Plan" maxlength="12" value="<?= $file[0]['TO_PLAN'] ?>" />
						<?php endif ?>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
                    <?php if (!$read_only): ?>
					<div class="col-sm-1">
						<button type="button" class="modal_btn btn btn-primary btn-sm no_label_fix" value="location" data-toggle="modal" data-target="#myModal">Select</button>
					</div>
                    <?php endif ?>
					<div class="col-sm-3">
						<label for="street_name">Street Name</label>
						<?php if ($read_only): ?>
							<input readonly type="text" class="form-control street_name readOnlyInputField" title="Street Name" maxlength="" value="<?= $streets['STREET'] ?>" />
						<?php else: ?>
							<input type="text" class="form-control street_name" title="Street Name" maxlength="" readonly value="<?= $streets['STREET'] ?>" />
						<?php endif ?>
					</div>
					<div class="col-sm-1">
						<label for="street_type">Type</label>
						<?php if ($read_only): ?>
							<input readonly tpye="text" class="form-control street_type readOnlyInputField" title="Street Type" maxlength="" value="<?= $streets['STREET_TYP'] ?>" />
						<?php else: ?>
							<input tpye="text" class="form-control street_type" title="Street Type" maxlength="" readonly value="<?= $streets['STREET_TYP'] ?>" />
						<?php endif ?>
					</div>
					<div class="col-sm-2">
						<label for="STREET_NUM">Number</label>
						<?php if ($read_only): ?>
							<input readonly type="text" name="STREET_NUM" class="form-control street_number readOnlyInputField" title="Street Number" maxlength="5" value="<?= $streets['STREET_CDE'] ?>" />
						<?php else: ?>
							<input type="text" name="STREET_NUM" class="form-control street_number" title="Street Number" maxlength="5" readonly value="<?= $streets['STREET_CDE'] ?>" />
						<?php endif ?>
					</div>
					<div class="col-sm-4">
						<label for="area_name">Community</label>
						<?php if ($read_only): ?>
							<input readonly type="text" class="form-control area readOnlyInputField" title="Area Name" maxlength=""  value="<?= $streets['COMMUNITY'] ?>" />
						<?php else: ?>
							<input type="text" class="form-control area" title="Area Name" maxlength="" readonly value="<?= $streets['COMMUNITY'] ?>" />
						<?php endif ?>
					</div>
				</div>
			</div>
            <div class="form-group">
                <div class="row">

                    <div class="col-sm-2">
                        <label for="CIVIC_NUM">Civic #</label>
                        <?php if ($read_only): ?>
                            <input readonly type="text" name="CIVIC_NUM" class="form-control readOnlyInputField" title="Civic Number" maxlength="5" value="<?= $file[0]['CIVIC_NUM'] ?>" />
                        <?php else: ?>
                            <input type="text" name="CIVIC_NUM" class="form-control" title="Civic Number" maxlength="5" value="<?= $file[0]['CIVIC_NUM'] ?>" />
                        <?php endif ?>
                    </div>
                    <div class="col-sm-2">
                        <label for="CIVIC_NUM">Ext</label>
                        <?php if ($read_only): ?>
                            <input readonly type="text" name="EXT" class="form-control readOnlyInputField" title="Civic Number" maxlength="4" value="<?= $file[0]['EXT'] ?>" />
                        <?php else: ?>
                            <input type="text" name="EXT" class="form-control" title="Civic Number" maxlength="4" value="<?= $file[0]['EXT'] ?>" />
                        <?php endif ?>
                    </div>
                    <div class="col-sm-1">
                        <label for="SIDE">Side</label>
                        <?php if ($read_only): ?>
                            <input readonly tpye="text" name="SIDE" class="form-control readOnlyInputField" title="Side" maxlength="3" value="<?= $file[0]['SIDE'] ?>" />
                        <?php else: ?>
                            <input tpye="text" name="SIDE" class="form-control" title="Side" maxlength="3" value="<?= $file[0]['SIDE'] ?>" />
                        <?php endif ?>
                    </div>
                    <div class="col-sm-5">
                        <label for="LOCATION">Location #</label>
                        <?php if ($read_only): ?>
                            <select name="LOCATION" class="form-control readOnlyInputField" disabled title="Location Number">
                                <option value="">Select..</option>
                                <?php foreach ($locations as $loc): ?>
                                    <?php if ($file[0]['LOCATION'] == $loc['LOCATION_CODE']): ?>
                                        <option selected value="<?= $loc['LOCATION_CODE'] ?>"><?= $loc['LOCATION_CODE'].' - '.$loc['LOCATION_DSC'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $loc['LOCATION_CODE'] ?>"><?= $loc['LOCATION_CODE'].' - '.$loc['LOCATION_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php else: ?>
                            <select name="LOCATION" class="form-control" title="Location Number">
                                <option value="">Select..</option>
                                <?php foreach ($locations as $loc): ?>
                                    <?php if ($file[0]['LOCATION'] == $loc['LOCATION_CODE']): ?>
                                        <option selected value="<?= $loc['LOCATION_CODE'] ?>"><?= $loc['LOCATION_CODE'].' - '.$loc['LOCATION_DSC'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $loc['LOCATION_CODE'] ?>"><?= $loc['LOCATION_CODE'].' - '.$loc['LOCATION_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php endif ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-2">
                        <label for="LAST_UPDATE_BY">Last Updated By</label>
                        <?php if ($read_only): ?>
                            <input readonly type="text" name="LAST_UPDATE_BY" class="form-control readOnlyInputField" title="Last Updated By" value="<?= $file[0]['LAST_UPDATE_BY'] ?>"  />
                        <?php else: ?>
                            <input type="text" name="LAST_UPDATE_BY" class="form-control" title="Last Updated By" value="<?= $file[0]['LAST_UPDATE_BY'] ?>" readonly />
                        <?php endif ?>
                    </div>
                    <div class="col-sm-2">
                        <label for="LAST_UPDATE">Date Last Updated</label>
                        <?php if ($read_only): ?>
                            <input readonly type="text" name="LAST_UPDATE" class="form-control readOnlyInputField" title="Last Updated By" value="<?= $file[0]['LAST_UPDATE'] ?>" />
                        <?php else: ?>
                            <input type="text" name="LAST_UPDATE" class="form-control" title="Last Updated By" value="<?= $file[0]['LAST_UPDATE'] ?>" readonly />
                        <?php endif ?>
                    </div>
                </div>
            </div>

			<legend>Surrounding Streets</legend>
            <div>
<!--			--><?php //if (!$read_only): ?>
<!--				<a href="--><?php //echo base_url(); ?><!--EDL/update_surrounding_streets/--><?//= $file[0]['FILE_ID'] ?><!--" class="btn btn-sm btn-primary pull-right">Modify Surrounding Streets</a>-->
<!--			--><?php //endif ?>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-3">
							<label for="street_name">Name</label>
						</div>
						<div class="col-sm-1">
							<label for="street_type">Type</label>
						</div>
						<div class="col-sm-2">
							<label for="STREET_NUM">Number</label>
						</div>
						<div class="col-sm-4">
							<label for="area_name">Community</label>
						</div>
					</div>
				</div>
				<?php if (!empty($surrounding_streets)): ?>
					<?php foreach ($surrounding_streets as $street): ?>
						<div class="form-group">
							<div class="row">
								<div class="col-sm-3">
                                    <?php if ($read_only): ?>
									    <input type="text" class="form-control readOnlyInputField" title="Street Name" maxlength="" readonly value="<?= $street['STREET'] ?>" />
                                    <?php else: ?>
                                        <input type="text" class="form-control" title="Street Name" maxlength="" readonly value="<?= $street['STREET'] ?>" />
                                    <?php endif ?>
								</div>
								<div class="col-sm-1">
                                    <?php if ($read_only): ?>
									    <input tpye="text" class="form-control readOnlyInputField" title="Street Type" maxlength="" readonly value="<?= $street['STREET_TYP'] ?>" />
                                    <?php else: ?>
                                        <input tpye="text" class="form-control " title="Street Type" maxlength="" readonly value="<?= $street['STREET_TYP'] ?>" />
                                    <?php endif ?>
								</div>
								<div class="col-sm-2">
                                    <?php if ($read_only): ?>
									    <input type="text" class="form-control readOnlyInputField" title="Street Number" maxlength="5" readonly value="<?= $street['STREET_CDE'] ?>" />
                                    <?php else: ?>
                                        <input type="text" class="form-control" title="Street Number" maxlength="5" readonly value="<?= $street['STREET_CDE'] ?>" />
                                    <?php endif ?>
								</div>
								<div class="col-sm-4">
                                    <?php if ($read_only): ?>
									    <input type="text" class="form-control readOnlyInputField" title="Area Name" maxlength="" readonly value="<?= $street['COMMUNITY'] ?>" />
                                    <?php else: ?>
                                        <input type="text" class="form-control" title="Area Name" maxlength="" readonly value="<?= $street['COMMUNITY'] ?>" />
                                    <?php endif ?>
								</div>
							</div>
						</div>
					<?php endforeach ?>
				<?php endif ?>
                <?php if (!$read_only): ?>
                    <a href="<?php echo base_url(); ?>EDL/update_surrounding_streets/<?= $file[0]['FILE_ID'] ?>" class="btn btn-sm btn-primary pull-right">Modify Surrounding Streets</a>
                <?php endif ?>
                <hr style="margin-top: 4em">
            </div>





			<div class="form-group">
				<div class="row">
					<div class="col-sm-5">
						<label for="ORIG_CDE">Originator</label>
						<?php if ($read_only): ?>
                            <select name="ORIG_CDE" class="form-control readOnlyInputField" disabled title="Originator" maxlength="4" >
                                <option value="">Select..</option>
                                <?php foreach ($originators as $orig): ?>
                                    <?php if ($file[0]['ORIG_CDE'] == $orig['ORIGINATOR_CODE']): ?>
                                        <option selected value="<?= $orig['ORIGINATOR_CODE'] ?>"><?= $orig['ORIGINATOR_CODE'].' - '.$orig['ORIGINATOR_DSC'] ?></option>
                                    <?php endif ?>
                                    <option value="<?= $orig['ORIGINATOR_CODE'] ?>"><?= $orig['ORIGINATOR_CODE'].' - '.$orig['ORIGINATOR_DSC'] ?></option>
                                <?php endforeach ?>
                            </select>
						<?php else: ?>
							<select name="ORIG_CDE" class="form-control" title="Originator" maxlength="4" >
								<option value="">Select..</option>
								<?php foreach ($originators as $orig): ?>
									<?php if ($file[0]['ORIG_CDE'] == $orig['ORIGINATOR_CODE']): ?>
										<option selected value="<?= $orig['ORIGINATOR_CODE'] ?>"><?= $orig['ORIGINATOR_CODE'].' - '.$orig['ORIGINATOR_DSC'] ?></option>
									<?php endif ?>
										<option value="<?= $orig['ORIGINATOR_CODE'] ?>"><?= $orig['ORIGINATOR_CODE'].' - '.$orig['ORIGINATOR_DSC'] ?></option>
								<?php endforeach ?>
							</select>
						<?php endif ?>
					</div>
					<div class="col-sm-5">
						<label for="PROJTYP_CDE">Project Type</label>
						<?php if ($read_only): ?>
                            <select name="PROJTYP_CDE" class="form-control readOnlyInputField" disabled title="Project">
                                <option value="">Select..</option>
                                <?php foreach ($projects as $project): ?>
                                    <?php if ($file[0]['PROJTYP_CDE'] == $project['PROJTYP_CODE']): ?>
                                        <option selected value="<?= $project['PROJTYP_CODE'] ?>"><?= $project['PROJTYP_CODE'].' - '.$project['PROJTYP_DSC'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $project['PROJTYP_CODE'] ?>"><?= $project['PROJTYP_CODE'].' - '.$project['PROJTYP_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
						<?php else: ?>
							<select name="PROJTYP_CDE" class="form-control" title="Project">
								<option value="">Select..</option>
								<?php foreach ($projects as $project): ?>
									<?php if ($file[0]['PROJTYP_CDE'] == $project['PROJTYP_CODE']): ?>
										<option selected value="<?= $project['PROJTYP_CODE'] ?>"><?= $project['PROJTYP_CODE'].' - '.$project['PROJTYP_DSC'] ?></option>
									<?php else : ?>
										<option value="<?= $project['PROJTYP_CODE'] ?>"><?= $project['PROJTYP_CODE'].' - '.$project['PROJTYP_DSC'] ?></option>
									<?php endif ?>
								<?php endforeach ?>
							</select>
						<?php endif ?>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="NOTES">Title/Description</label>
						<?php if ($read_only): ?>
						<textarea readonly name="NOTES" class="form-control readOnlyInputField" title="Title/Description" maxlength="500"><?= $file[0]['NOTES'] ?></textarea>
						<?php else: ?>
						<textarea name="NOTES" class="form-control" title="Title/Description" maxlength="500"><?= $file[0]['NOTES'] ?></textarea>
						<?php endif ?>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
                    <div class="col-sm-3">
                        <label for="SURV_METH">Survey Method</label>
                        <?php if ($read_only): ?>
                            <select name="SURV_METH" class="form-control readOnlyInputField" disabled title="Survey Method">
                                <option value="">Select..</option>
                                <?php foreach ($survey_methods as $survey_method): ?>
                                    <?php if ($file[0]['SURV_METH'] == $survey_method['SURVMETH_CODE']): ?>
                                        <option selected value="<?= $survey_method['SURVMETH_CODE'] ?>"><?= $survey_method['SURVMETH_CODE'].' - '.$survey_method['SURVMETH_DSC'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $survey_method['SURVMETH_CODE'] ?>"><?= $survey_method['SURVMETH_CODE'].' - '.$survey_method['SURVMETH_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php else: ?>
                            <select name="SURV_METH" class="form-control" title="Survey Method">
                                <option value="">Select..</option>
                                <?php foreach ($survey_methods as $survey_method): ?>
                                    <?php if ($file[0]['SURV_METH'] == $survey_method['SURVMETH_CODE']): ?>
                                        <option selected value="<?= $survey_method['SURVMETH_CODE'] ?>"><?= $survey_method['SURVMETH_CODE'].' - '.$survey_method['SURVMETH_DSC'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $survey_method['SURVMETH_CODE'] ?>"><?= $survey_method['SURVMETH_CODE'].' - '.$survey_method['SURVMETH_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php endif ?>
                    </div>
					<div class="col-sm-3">
						<label for="SURVEYOR">Surveyor</label>
						<?php if ($read_only): ?>
                            <select name="SURVEYOR" class="form-control readOnlyInputField" disabled title="Surveyor">
                                <option value="">Select..</option>
                                <?php foreach ($surveyors as $surveyor): ?>
                                    <?php if ($file[0]['SURVEYOR'] == $surveyor['SURVEYOR_CODE']): ?>
                                        <option selected value="<?= $surveyor['SURVEYOR_CODE'] ?>"><?= $surveyor['SURVEYOR_CODE'].' - '.$surveyor['SURVEYOR_DSC'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $surveyor['SURVEYOR_CODE'] ?>"><?= $surveyor['SURVEYOR_CODE'].' - '.$surveyor['SURVEYOR_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
						<?php else: ?>
							<select name="SURVEYOR" class="form-control" title="Surveyor">
								<option value="">Select..</option>
								<?php foreach ($surveyors as $surveyor): ?>
									<?php if ($file[0]['SURVEYOR'] == $surveyor['SURVEYOR_CODE']): ?>
										<option selected value="<?= $surveyor['SURVEYOR_CODE'] ?>"><?= $surveyor['SURVEYOR_CODE'].' - '.$surveyor['SURVEYOR_DSC'] ?></option>
									<?php else : ?>
										<option value="<?= $surveyor['SURVEYOR_CODE'] ?>"><?= $surveyor['SURVEYOR_CODE'].' - '.$surveyor['SURVEYOR_DSC'] ?></option>
									<?php endif ?>
								<?php endforeach ?>
							</select>
						<?php endif ?>
					</div>
					<div class="col-sm-3">
						<label for="RELFILE_ID">Related File ID</label>
						<?php if ($read_only): ?>
							<input readonly type="text" name="RELFILE_ID" class="form-control readOnlyInputField" title="Related File ID" maxlength="9" value="<?= $file[0]['RELFILE_ID'] ?>" />
						<?php else: ?>
							<input type="text" name="RELFILE_ID" class="form-control" title="Related File ID" maxlength="9" value="<?= $file[0]['RELFILE_ID'] ?>" />
						<?php endif ?>
					</div>
                    <div class="col-sm-3">
                        <label for="SW_VER">Software Version</label>
                        <?php if ($read_only): ?>
                            <select name="SW_VER" class="form-control readOnlyInputField" disabled title="Software Version">
                                <option value="">Select..</option>
                                <?php foreach ($software_versions as $version): ?>
                                    <?php if ($file[0]['SW_VER'] == $version['SWVER_CODE']): ?>
                                        <option selected value="<?= $version['SWVER_CODE'] ?>"><?= $version['SWVER_CODE'].' - '.$version['SWVER_DSC'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $version['SWVER_CODE'] ?>"><?= $version['SWVER_CODE'].' - '.$version['SWVER_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php else: ?>
                            <select name="SW_VER" class="form-control" title="Software Version">
                                <option value="">Select..</option>
                                <?php foreach ($software_versions as $version): ?>
                                    <?php if ($file[0]['SW_VER'] == $version['SWVER_CODE']): ?>
                                        <option selected value="<?= $version['SWVER_CODE'] ?>"><?= $version['SWVER_CODE'].' - '.$version['SWVER_DSC'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $version['SWVER_CODE'] ?>"><?= $version['SWVER_CODE'].' - '.$version['SWVER_DSC'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php endif ?>
                    </div>
				</div>
			</div>


            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3">
                        <label for="DEPT">Department</label>
                        <?php if ($read_only): ?>
                            <select name="DEPT" class="form-control readOnlyInputField" disabled title="Department">
                                <option value="">Select..</option>
                                <?php foreach ($departments as $dpt): ?>
                                    <?php if ($file[0]['DEPT'] == $dpt['DEPT_CODE']): ?>
                                        <option selected value="<?= $dpt['DEPT_CODE'] ?>"><?= $dpt['DEPT_CODE'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $dpt['DEPT_CODE'] ?>"><?= $dpt['DEPT_CODE'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php else: ?>
                            <select name="DEPT" class="form-control" title="Department">
                                <option value="">Select..</option>
                                <?php foreach ($departments as $dpt): ?>
                                    <?php if ($file[0]['DEPT'] == $dpt['DEPT_CODE']): ?>
                                        <option selected value="<?= $dpt['DEPT_CODE'] ?>"><?= $dpt['DEPT_CODE'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $dpt['DEPT_CODE'] ?>"><?= $dpt['DEPT_CODE'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php endif ?>
                    </div>
                    <div class="col-sm-3">
                        <label for="E_CLASS_NUM">Electronic Class #</label>
                        <?php if ($read_only): ?>
                            <select name="E_CLASS_NUM" class="form-control readOnlyInputField" disabled title="Electronic Class #">
                                <option value="">Select..</option>
                                <?php foreach ($electronic_class_numbers as $e_num): ?>
                                    <?php if ($file[0]['E_CLASS_NUM'] == $e_num['CLASS_CODE']): ?>
                                        <option selected value="<?= $e_num['CLASS_CODE'] ?>"><?= $e_num['CLASS_CODE'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $e_num['CLASS_CODE'] ?>"><?= $e_num['CLASS_CODE'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php else: ?>
                            <select name="E_CLASS_NUM" class="form-control" title="Electronic Class #">
                                <option value="">Select..</option>
                                <?php foreach ($electronic_class_numbers as $e_num): ?>
                                    <?php if ($file[0]['E_CLASS_NUM'] == $e_num['CLASS_CODE']): ?>
                                        <option selected value="<?= $e_num['CLASS_CODE'] ?>"><?= $e_num['CLASS_CODE'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $e_num['CLASS_CODE'] ?>"><?= $e_num['CLASS_CODE'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php endif ?>
                    </div>
                    <div class="col-sm-3">
                        <label for="P_CLASS_NUM">Paper Class #</label>
                        <?php if ($read_only): ?>
                            <select name="P_CLASS_NUM" class="form-control readOnlyInputField" disabled title="Paper Class #">
                                <option value="">Select..</option>
                                <?php foreach ($paper_class_numbers as $p_num): ?>
                                    <?php if ($file[0]['P_CLASS_NUM'] == $p_num['CLASS_CODE']): ?>
                                        <option selected value="<?= $p_num['CLASS_CODE'] ?>"><?= $p_num['CLASS_CODE'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $p_num['CLASS_CODE'] ?>"><?= $p_num['CLASS_CODE'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php else: ?>
                            <select name="P_CLASS_NUM" class="form-control" title="Paper Class #">
                                <option value="">Select..</option>
                                <?php foreach ($paper_class_numbers as $p_num): ?>
                                    <?php if ($file[0]['P_CLASS_NUM'] == $p_num['CLASS_CODE']): ?>
                                        <option selected value="<?= $p_num['CLASS_CODE'] ?>"><?= $p_num['CLASS_CODE'] ?></option>
                                    <?php else : ?>
                                        <option value="<?= $p_num['CLASS_CODE'] ?>"><?= $p_num['CLASS_CODE'] ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        <?php endif ?>
                    </div>
                </div>
            </div>

			<div class="form-group">
				<div class="row">
                    <div class="col-sm-2">
                        <label for="SIGNED">Signed Date</label>
                        <?php if ($read_only): ?>
                            <input readonly type="text" name="SIGNED" class="form-control date_picker_new readOnlyInputField" title="Signed Date" disabled value="<?= $file[0]['SIGNED'] ?>" />
                        <?php else: ?>
                            <input type="text" name="SIGNED" class="form-control date_picker_new" title="Signed Date" readonly value="<?= $file[0]['SIGNED'] ?>" />
                        <?php endif ?>
                    </div>
					<div class="col-sm-3">
						<label for="date_name">Date Note</label>
						<?php if ($read_only): ?>
							<input readonly type="text" name="ARCHIVE_DATE" class="form-control readOnlyInputField" title="Date Note" maxlength="50" value="<?= $file[0]['ARCHIVE_DATE'] ?>" />
						<?php else: ?>
							<input tpye="text" name="ARCHIVE_DATE" class="form-control" title="Date Note" maxlength="50" value="<?= $file[0]['ARCHIVE_DATE'] ?>" />
						<?php endif ?>
					</div>
                    <div class="col-sm-2">
                        <label for="MICROFILMED">Microfilmed Date</label>
                        <?php if ($read_only): ?>
                            <input readonly type="text" name="MICROFILMED" class="form-control date_picker_new readOnlyInputField" title="Microfilmed Date"  value="<?= $file[0]['MICROFILMED'] ?>" />
                        <?php else: ?>
                            <input type="text" name="MICROFILMED" class="form-control date_picker_new" title="Microfilmed Date" readonly value="<?= $file[0]['MICROFILMED'] ?>" />
                        <?php endif ?>
                    </div>
					<div class="col-sm-3">
						<label for="ROLL_NUM">Roll #</label>
						<?php if ($read_only): ?>
							<input readonly type="text" name="ROLL_NUM" class="form-control readOnlyInputField" title="Roll Number" maxlength="10" value="<?= $file[0]['ROLL_NUM'] ?>" />
						<?php else: ?>
							<input type="text" name="ROLL_NUM" class="form-control" title="Roll Number" maxlength="10" value="<?= $file[0]['ROLL_NUM'] ?>" />
						<?php endif ?>
					</div>
<!--					<div class="col-sm-2">-->
<!--                        --><?php //if (!$read_only): ?>
<!--                            <button type="button" class="btn btn-sm btn-primary no_label_fix clear_dates">Clear Dates</button>-->
<!--                        --><?php //endif ?>
<!--                    </div>-->
				</div>
			</div>


			<?php 
			$location_counter = 0;
			 ?>
			 <?php if (!empty($locs)): ?>
    				<legend>Locations</legend>
			 	<?php foreach ($locs as $loc): ?>
			 		<?php if ($location_counter > 0): ?>
			 			<hr style="margin-top: 30px; ">
			 		<?php endif ?>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-3">
								<label for="street_name">Process</label>
								<input type="text" name="" class="form-control" title="" value="<?= $loc['PROCESS'] ?>" disabled />
							</div>
							<div class="col-sm-3">
								<label for="street_type">Device</label>
								<input type="text" name="" class="form-control" title="" value="<?= $loc['DEVICE'] ?>" disabled />
							</div>
							<div class="col-sm-3">
								<label for="area_name">Status</label>
								<input type="text" name="" class="form-control" title="" value="<?= $loc['STATUS'] ?>" disabled />
							</div>
							<div class="col-sm-3">
								<label for="area_name">Drawn By</label>
								<input type="text" name="" class="form-control" title="" value="<?= $loc['DRAWN'] ?>" disabled />
							</div>
						</div>
                    </div>
                     <div class="form-group">
						<div class="row">
							<div class="col-sm-2">
								<label for="area_name">Date</label>
								<input type="text" name="<?= $location_counter ?>-DATETIME" class="form-control date_picker_new" title="Date" maxlength="" value="<?= $loc['DATETIME'] ?>" disabled />
							</div>
							<div class="col-sm-4">
								<label for="STREET_NUM">File Location</label>
								<input type="text" name="<?= $location_counter ?>-FILE_LOC" class="form-control" title="File Location" maxlength="" value="<?= $loc['FILE_LOC'] ?>" disabled />
							</div>
							<div class="col-sm-6">
								<label for="PHYSICAL_DESC">Physical Description</label>
								<input type="text" name="<?= $location_counter ?>-PHYSICAL_DESC" class="form-control" title="Physical Description" maxlength="" value="<?= $loc['PHYSICAL_DESC'] ?>" disabled />
							</div>
						</div>
					</div>
					<?php $location_counter++; ?>
				<?php endforeach ?>
			<?php else: ?>
				<?php if (!$read_only): ?>	
					<?php if (!$read_only): ?>
						<legend>Locations</legend>
					<?php endif ?>
				<?php endif ?>
			 <?php endif ?>
            <?php if (!$read_only): ?>
                <a href="<?php echo base_url(); ?>EDL/update_file_locations/<?= $file[0]['FILE_ID'] ?>" style="margin-top: 1em" class="btn btn-sm btn-primary pull-right">Modify Locations</a>
            <?php endif ?>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-12">
                        <?php if (!$read_only): ?>
                            <input type="submit" class="btn btn-md btn-primary add_interred pull-right" value="Update Record" />
                            <a href="<?php echo base_url(); ?>EDL/" class="btn btn-md btn-default return_interred">Back to EDL Lookup</a>
                            <?php if ($is_admin): ?>
                                <a href="<?php echo base_url(); ?>EDL/delete_drawing/<?= $file[0]['FILE_ID'] ?>" class="btn btn-danger btn-md add_interred delete_drawing" style="margin-left: 1.5em">Delete Drawing</a>
                            <?php endif ?>
                        <?php else: ?>
                            <a href="<?php echo base_url(); ?>EDL/" class="btn btn-md btn-default return_interred">Back to EDL Lookup</a>
                        <?php endif ?>

                    </div>
                </div>
            </div>
		</form>

			<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<h4 class="modal-title">Street Look Up</h4>
						</div>
						<div class="modal-body">
							<div class="container">
								<div class="row">
									<div class="col-sm-5">
										<div class="modal_search">
											<label for="search_box">Street Name</label>
											<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
											<p class="text-muted">(Select the name of the record you would like to use.)</p>
										</div>
									</div>
								</div>
							</div>
							<hr>
							<table class="table table-bordered">
								<thead>
									<th>Name</th>
								</thead>
								<tbody class="modal_tbody">
									<tr>
										
									</tr>
								</tbody>	
							</table>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
	</div>
</div>