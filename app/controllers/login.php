<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================

	   Name : Login System

Description : This is the login controller. We will be authenticating users with 
			Active Directory.

  Functions : index(), 
  			validate(), 
  			logout()

=========================================================================================================================*/
class Login extends CI_Controller 
{

	public function index()
	{
		if ($this->session->userdata('samaccountname'))
		{
			redirect('welcome');
		}
		$data['content'] = 'pages/login_view';
		$this->load->view('master', $data);
	}

/*-----------------------------------------------------------------------------------
	Validate check pass the credentials from the login form against the LDAP server,
	if valid, an array of user information will be returned to be stored in session.
	if not, we return the user to the login screen with an error message.
------------------------------------------------------------------------------------*/
	public function validate()
	{
		// Get the user credentials though post
		$credentials = $this->input->post(null, true);

		// modify they user name to add the domain so the user doesn't have to type it.
		$user_name = "hrm\\".$credentials['user_name'];

		// ENVIRONMENT is set in index.php at root of application
		if(ENVIRONMENT == 'production') 
		{
            $host_name = PROD_LDAP_HOST_NAME; // msrodc01.hrm.halifax.ca
            $base = PROD_LDAP_BASE;
		}
		// dev environment
		else
		{
            $host_name = DEV_LDAP_HOST_NAME; 	// IP: 172.25.210.95
            $base = DEV_LDAP_BASE;
		}
		
		// Set up the config array for the LDAP library
		$config = array(
            'port' 		=> LDAP_PORT,
			'host_name' => $host_name, 
			'base' 		=> $base, 
			'rdn' 		=> $user_name, 
			'password' 	=> $credentials['password'], 
			);

		// Load LDAP and pass it the configuration array.
		$this->load->library('ldap', $config);

		// Call the validate user function and pass it the filter, in this instance we want to filter by account name (i.e. the user name the typed in the login form)
		$user_data = $this->ldap->validate_user("(samaccountname=".$credentials['user_name'].")", DEV_LDAP_BASE);

		// if the user is not validated try searching in the Halifax Water OU
		if (!$user_data)
		{
            // Load LDAP and pass it the configuration array.
            $this->load->library('ldap', $config);

            // Call the validate user function and pass it the filter, in this instance we want to filter by account name (i.e. the user name the typed in the login form)
            $user_data = $this->ldap->validate_user("(samaccountname=".$credentials['user_name'].")", DEV_HW_LDAP_BASE);

            if (!$user_data) {
                $this->session->set_flashdata('error', 'The credentials you provided are incorrect.');
                redirect('login');
            }
		}
		
		// Once we know the user is valid, we need to separate what groups they're a part of.
		$user_groups = null;
		for ($i=0; $i < $user_data['member_of']['count']; $i++) 
		{ 
			// store the full group string in a variable.
			$group = $user_data['member_of'][$i];
			// split the string on commas 
			$group_chunks = explode(',', $group);

			/*
				Once the string has been split on commas we can always assume that the group name is in the first chunk.
				so we're going to split the first chunk on '=' this will give us the group name as it appears in AD, this is what we
				will store in session so that when they get redirected to the application we have a list of apps that they have access to. 
			*/

			// this line splits the string in two on the '=' sign (app=name), normally $group_name
			$group_name = explode('=', $group_chunks[0])[1];

			/*
				IMPORTANT - Session isn't large enough to store all groups that a user belongs to.
				so we do one last check to see if the group name belongs to an app from this portal. 
				if it does then we store it to session, if not skip it becuase we don't have room in sesison.
			*/
			if (strrpos($group_name, 'HRMApps'))
			{
				$user_groups[] = $group_name;
			}
			
		}

		unset($user_data['member_of']);

		$user_data['groups'] = $user_groups;
		$user_data['rdn'] = $user_name;

		// Encrypt the users password when storing it in session;
		$user_data['password'] = $this->encrypt->encode($credentials['password']);

        // split first and last name
        $name_chunks = explode(',', $user_data['name']);
        $user_data['first_name'] = $name_chunks[1];
        $user_data['last_name'] = $name_chunks[0];
	
		// Other wise store the user data in session and redirect the user to the home controller,
		// with the session set the user will now gain access to the web site.
		$this->session->set_userdata($user_data);

	

		$this->load->library('email');
				// DEBUG VIA EMAIL
		//=============================================
		// $output = "<html><body>\n";
		// $output .=  "<p>".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."</p>";
		// $output .=  "<h1>".$use_data['name']."</h1>";

		// // User array
		// $output .=  "<h2>User Data Array</h2>";
		// foreach ($user_data as $key => $value) {
		//     if(is_array($value))
		//     {
		//     	$output .=  "<p>$key</p>\n<ul>\n";
		//     	foreach ($value as $key2 => $value2) {
		//     		// if (strpos($value2,'AppAccountabilityReporting') !== false) 
		//     			$output .=  "<li>$key2: $value2</li>\n";
		//     	}
		//     	$output .=  "</ul>\n";
		//     }
		//     else $output .=  "<p>$key: $value</p>\n";
		// }

		// // Data array
		// $output .=  "<h2>Data Array</h2>";
		// foreach ($data as $key => $value) {
		//     if(is_array($value))
		//     {
		//     	$output .=  "<p>$key</p>\n<ul>\n";
		//     	foreach ($value as $key2 => $value2) {
		//     		// if (strpos($value2,'AppAccountabilityReporting') !== false) 
		//     			$output .=  "<li>$key2: $value2</li>\n";
		//     	}
		//     	$output .=  "</ul>\n";
		//     }
		//     else $output .=  "<p>$key: $value</p>\n";
		// }
		// print_r($this->session->all_userdata());
		// $output .= "</body></html>";

		// $config['charset']  = 'utf-8';			// added
		// $config['mailtype'] = 'html';
		// $this->email->set_newline( "\r\n" );	// added
		// $this->email->set_crlf( "\r\n" );		// added
		// $this->email->initialize($config);
		// $this->email->clear();					// added

		// $this->email->from('no-reply@halifax.ca', 'Halifax Regional Municipality');
		// $this->email->to('pompilg@halifax.ca, morinb@halifax.ca'); 
		// $this->email->subject('HRM Application Debuging');
		// $this->email->message($output);	
		// $this->email->send();
		//=============================================

		redirect('welcome');
	}

/*-----------------------------------------------------------------------------------
	This function destroys the session and redirects the user to the login controller.
	With no session set the user is no longer identifiable and must login again.
------------------------------------------------------------------------------------*/
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('login');
	}
}

/* End of file login.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/login.php */