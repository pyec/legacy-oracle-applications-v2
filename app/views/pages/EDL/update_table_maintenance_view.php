<?php
// determine if the user is an admin -- redirect if not
$is_admin = false;

$groups = $this->session->userdata('groups');

foreach ($groups as $group)
{
    if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
    {
        $is_admin = true;
    }
}

if (!($is_admin)) {
    redirect('EDL/');
}
?>

<div class="row">
	<div class="col-md-12">
		<?php $header = str_replace('_', ' ', $title); ?>
		<h2><?= ucwords($header) ?> Update</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<form action="<?php echo base_url(); ?>EDL/update_table_maintenance" method="POST">
	<div class="row">
		<div class="col-md-5">
			<?php foreach ($data as $key => $value): ?>
				<div class="form-group">
					<?php $label = str_replace('_', ' ', $key) ?>
					<label for="<?= $key ?>"><?php echo ucfirst($label) ?></label>
					<input type="text" name="<?= $key ?>" class="form-control" value="<?= $value ?>"  />
				</div>
			<?php endforeach ?>
			<input type="hidden" name="table_name" value="<?= $title ?>"  />
		</div>
	</div>

	<div class="row">
		<div class="col-md-3">
			<input type="submit" class="btn btn-primary btn-sm" value="Update" />
		</div>
	</div>
</form>