<div class="jumbotron">
	<h1>Applications</h1>
	<p>Please select an application from your app list in the top right of the screen to begin. </p>
</div>
<?php  ?>
<div class="row home_items">
	<div class="col-md-12">
		<img style="height: 250px;" class="user_shillouette pull-left" src="<?php echo base_url(); ?>assets/img/male-silhouette.jpg">
		<div class="jumbotron user_info col-sm-offset-3">
			<div class="user_form">
				<div class="col-sm-4">
					<label for="name">Name</label>
					<input type="text" name="name" class="form-control" title="name" value="<?php echo $this->session->userdata('name'); ?>" readonly />
					<label for="email">Email</label>
					<input type="text" name="email" class="form-control" title="email" value="<?php echo $this->session->userdata('mail'); ?>" readonly />
				</div>
				<div class="col-sm-4">
					<label for="position">Position</label>
					<input type="text" name="position" class="form-control" title="Position" value="<?php echo $this->session->userdata('title'); ?>" readonly  />
					<label for="section">Section</label>
					<input type="text" name="section" class="form-control" title="Section" value="<?php echo $this->session->userdata('department'); ?>" readonly />
				</div>
				<div class="col-sm-4">
					<label for="telephone">Work Phone</label>
					<input type="text" name="telephone" class="form-control" title="Work Phone" value="<?php echo $this->session->userdata('telephone'); ?>" readonly  />
					<label for="mobile">Mobile</label>
					<input type="text" name="mobile" class="form-control" title="Mobile" value="<?php echo $this->session->userdata('mobile'); ?>" readonly />
				</div>
			</div>
		</div>
	
				<p class="contact_service_desk pull-right">Information incorrect? Contact the <a href="mailto:ICTSD@halifax.ca?subject=Active%20Directory%20Information%20Update">Service Desk</a>.</p>			

	</div>
</div>
