<div class="row">
	<div class="col-md-6">
		<h2>Summary Offence Ticket Lookup</h2>
		<p>Select a ticket to view more information about it.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>

<!-- <div class="row"> -->
	<form action="<?php echo base_url(); ?>SOT/ticket_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="ticket_number">Ticket #</label>
					<input type="text" name="ticket_number" class="form-control" title="Ticket Number" value="<?php echo set_value('ticket_number') ?>" tabindex="1" placeholder="Ticket Number" />
				</div>
				<div class="col-sm-3">
					<label for="officer">Officer</label>
					<input type="text" name="officer" class="form-control" title="Ticket Number" value="<?php echo set_value('officer') ?>" tabindex="2" placeholder="Officer Name" />
				</div>
				<div class="col-sm-3">
					<label for="location">Location</label>
					<input type="text" name="location" class="form-control" title="Ticket Number" value="<?php echo set_value('location') ?>" tabindex="3" placeholder="Location Name" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="sot_tickets_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
	
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($tickets)): ?>
			<h2 class="count"><?php echo count($tickets) ?> tickets</h2>
		<?php endif ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<a href="<?php echo base_url(); ?>SOT/add_SOT" class="btn btn-success">Add SOT</a>
		</div>
	</div>
</div>

<?php if (!empty($tickets)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered">
			<thead>
				<th>Ticket #</th>
				<th>Issue Date</th>
				<th>Officer</th>
				<th>Location</th>
				<th>Outcome</th>
				<th>Delete</th>
			</thead>

			<tbody class="tbody">
					<?php foreach ($tickets as $ticket): ?>
						<tr>
							<td><a href="<?php echo base_url(); ?>SOT/update_SOT/<?= $ticket['SOT_NUM'] ?>"><?php echo $ticket['SOT_NUM'] ?></a></td>
							<td><?php echo $ticket['ISSUE_DTE'] ?></td>
							<td><?php echo $ticket['ISSUE_ID'] ?></td>
							<td><?php echo ucfirst(strtolower($ticket['OFFENCELOC_DSC'])); ?></td>
							<td><?php echo ucwords(strtolower($ticket['OUTCOME_DSC'])); ?></td>
							<td>
								<form action="<?php echo base_url(); ?>SOT/delete_SOT" method="post">
									<button onclick="" type="submit" class="delete_SOT btn btn-sm btn-danger" ><i class="fa fa-minus-square"></i></button>
									<input type="hidden" name="SOT_NUM" value="<?= $ticket['SOT_NUM'] ?>" />
								</form>
							</td>
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php elseif (isset($tickets)) : ?>
    <?php if ($tickets == false): ?>
	    <p>There are no search results.</p>
    <?php endif ?>
<?php endif ?>