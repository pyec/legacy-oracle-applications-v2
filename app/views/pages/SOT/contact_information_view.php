<div class="row">
	<div class="col-md-6">
		<h2>Contact Information</h2>
		<p>Select a contact to view more information about them.</p>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<?php if ($this->session->flashdata('message')): ?>
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('message'); ?></p>
		</div>
	<?php elseif ($this->session->flashdata('error')): ?>
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<p><?php echo $this->session->flashdata('error'); ?></p>
		</div>
	<?php endif ?>
	</div>
</div>
<!-- <div class="row"> -->
	<form action="<?php echo base_url(); ?>SOT/contact_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="contact_number">Contact #</label>
					<input type="text" name="contact_number" class="form-control" title="Contact Number" value="<?php echo set_value('contact_number') ?>" tabindex="1" placeholder="Contact Number" />
				</div>
				<div class="col-sm-3">
					<label for="last_name">Last Name</label>
					<input type="text" name="last_name" class="form-control" title="Last Name" value="<?php echo set_value('last_name') ?>" tabindex="1" placeholder="Last Name" />
				</div>
				<div class="col-sm-3">
					<label for="first_name">First Name</label>
					<input type="text" name="first_name" class="form-control" title="First Name" value="<?php echo set_value('first_name') ?>" tabindex="1" placeholder="First Name" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="sot_contact_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
	
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($contacts)): ?>
			<h2 class="count"><?php echo count($contacts) ?> contacts</h2>
		<?php endif ?>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="pull-right">
			<a href="<?php echo base_url(); ?>SOT/add_contact" class="btn btn-success">Add Contact</a>
		</div>
	</div>
</div>

<?php if (!empty($contacts)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Contact #</th>
				<th>Name</th>
				<th>Birthdate</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
					<?php foreach ($contacts as $contact): ?>
						<tr>
							<td><a href="<?php echo base_url(); ?>SOT/update_contact/<?= $contact['CONTACT_NUM'] ?>"><?php echo $contact['CONTACT_NUM'] ?></a></td>
							<td><?php echo ucwords(strtolower($contact['LAST_NME'].', '.$contact['FIRST_NME'])); ?></td>
							<td><?php echo $contact['BIRTH_DTE'] ?></td>
							<td>
								<form action="<?php echo base_url(); ?>SOT/delete_contact" method="post">
									<button type="submit" class="delete_contact btn btn-sm btn-danger" ><i class="fa fa-minus-square"></i></button>
									<input type="hidden" name="CONTACT_NUM" value="<?= $contact['CONTACT_NUM'] ?>" />
								</form>
							</td>
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>