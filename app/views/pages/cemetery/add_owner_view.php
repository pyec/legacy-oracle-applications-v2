<div class="row">
	<div class="col-md-12">
		<h2>Add Owner</h2>
		<!-- <p class="lead">This form will allow you to add a new owner to the system.</p> -->
	</div>
</div>

 <div class="row">
     <div class="col-md-12">
         <?php if ($this->session->flashdata('message')): ?>
             <div class="alert alert-success alert-dismissable">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 <p><?php echo $this->session->flashdata('message'); ?></p>
             </div>
         <?php elseif ($this->session->flashdata('error')): ?>
             <div class="alert alert-danger alert-dismissable">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 <p><?php echo $this->session->flashdata('error'); ?></p>
             </div>
         <?php elseif (validation_errors()): ?>
             <div class="alert alert-danger alert-dismissable">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                 <p><?php echo validation_errors(); ?></p>
             </div>
         <?php endif ?>
        <?php if ($errorMessage): ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <p><?php echo $errorMessage; ?></p>
            </div>
        <?php endif ?>
	</div>
</div>

<div class="row">
	<form action="<?php echo base_url(); ?>cemetery/add_owner" method="POST" >
		<div class="col-md-12">
			<!-- <div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="CEM_NBR">Cemetery Name</label>
						<select name="CEM_NBR" class="form-control">
							<option value="" >Select a cemetery</option>
							<?php foreach ($cemeteries as $cem): ?>
								<option value="<?php echo $cem['CEM_NBR'] ?>" <?php echo set_select('CEM_NBR', $cem['CEM_NBR']); ?> ><?php echo $cem['CEM_NAME'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
				</div>
			</div> -->

			<!-- <legend></legend> -->

			<div class="form-group">
							
				<div class="row">
					<!-- <div class="col-sm-3">
						<label for="CEM_TTL">Title</label>
						<input type="text" name="CEM_TTL" class="form-control" title="Title" maxlength="15" value="<?php echo set_value('CEM_TTL'); ?>" />	
					</div> -->
					<div class="col-sm-3">
						<label for="CEM_NBR">Cemetery Name</label>
						<select name="CEM_NBR" class="form-control">
							<option value="" >Select a cemetery</option>
							<?php foreach ($cemeteries as $cem): ?>
								<option value="<?php echo $cem['CEM_NBR'] ?>" <?php echo set_select('CEM_NBR', $cem['CEM_NBR']); ?> ><?php echo $cem['CEM_NAME'] ?></option>
							<?php endforeach ?>
						</select>
					</div>
					<div class="col-sm-3" title="Last Name">
						<label for="CEM_LNAME">Last Name</label><span class="text-danger">*</span>
						<input type="text" name="CEM_LNAME" class="form-control" maxlength="30" value="<?php echo set_value('CEM_LNAME'); ?>" />	
					</div>
					<div class="col-sm-3" title="First Name">
						<label for="CEM_FNAME">First Name</label><span class="text-danger">*</span>
						<input type="text" name="CEM_FNAME" class="form-control" maxlength="20" value="<?php echo set_value('CEM_FNAME'); ?>" />	
					</div>
					<div class="col-sm-3" title="Middle Name">
						<label for="CEM_MNAME">Middle Name</label>
						<input type="text" name="CEM_MNAME" class="form-control" maxlength="20" value="<?php echo set_value('CEM_MNAME'); ?>" />	
					</div>
				</div>

			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12" title="Owner Information">
						<label for="CEM_OWNERS">Owner Information</label>
						<textarea row="5" type="text" name="CEM_OWNERS" class="form-control"><?php echo set_value('CEM_OWNERS'); ?></textarea>	
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-md-3" title="Lot Number">
						<label for="CEM_LOTNBR">Lot Number</label>
						<input type="text" name="CEM_LOTNBR" class="form-control" maxlength="30" value="<?php echo set_value('CEM_LOTNBR'); ?>" />	
					</div>
					<div class="col-md-1" title="Lot Size">
						<label for="CEM_SIZE">Size</label>
						<input type="text" name="CEM_SIZE" class="form-control" maxlength="75" value="<?php echo set_value('CEM_SIZE'); ?>" />	
					</div>
					<div class="col-md-1" title="Nubmer of Graves">
						<label for="CEM_NBRGR">Graves</label>
						<input type="text" name="CEM_NBRGR" class="form-control" maxlength="4" value="<?php echo set_value('CEM_NBRGR'); ?>" />	
					</div>
					<div class="col-md-3" title="Location">
						<label for="CEM_SCTN">Location</label>
						<input type="text" name="CEM_SCTN" class="form-control" maxlength="4" value="<?php echo set_value('CEM_SCTN'); ?>" />	
					</div>
					<div class="col-md-1" title="Sheet">
						<label for="CEM_SHEET">Sheet</label>
						<input type="text" name="CEM_SHEET" class="form-control" maxlength="4" value="<?php echo set_value('CEM_SHEET'); ?>" />	
					</div>
					<div class="col-md-3" title="Incomplete Purchase Date">
						<label for="CEM_INCOMPURDTE">Incomplete Purchase Date</label>
						<input type="text" name="CEM_INCOMPURDTE" class="form-control" maxlength="" value="<?php echo set_value('CEM_INCOMPURDTE'); ?>" />	
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-md-3" title="Purchase Date">
						<label for="CEM_PURDTE">Purchase Date</label>
						<!-- <input type="text" name="CEM_PURDTE" class="form-control date_picker" title="Purchase Date" readonly maxlength="" value="<?php echo set_value('CEM_PURDTE'); ?>" />	 -->
						<input type="text" name="CEM_PURDTE" class="form-control date_picker_cem" maxlength="" value="<?php echo set_value('CEM_PURDTE'); ?>"  readonly/>
					</div>
					<div class="col-md-2" title="Purchase Amount">
						<label for="CEM_PURAMT">Purchase Amount</label>
						<input type="text" name="CEM_PURAMT" class="form-control" maxlength="11" value="<?php echo set_value('CEM_PURAMT'); ?>" />	
					</div>
					<div class="col-md-2" title="Charge Number">
						<label for="CEM_CHGNBR">Charge Number</label>
						<input type="text" name="CEM_CHGNBR" class="form-control" maxlength="6" value="<?php echo set_value('CEM_CHGNBR'); ?>" />	
					</div>
					<div class="col-md-2" title="Number of Monuments">
						<label for="CEM_NBRMON">Monuments</label>
						<input type="text" name="CEM_NBRMON" class="form-control" maxlength="10" value="<?php echo set_value('CEM_NBRMON'); ?>" />	
					</div>
					<div class="col-md-2" title="Monument Size">
						<label for="CEM_MONSIZE">Monument Size</label>
						<input type="text" name="CEM_MONSIZE" class="form-control" maxlength="7" value="<?php echo set_value('CEM_MONSIZE'); ?>" />	
					</div>
					<div class="col-md-1" title="Marker">
						<label for="CEM_MARKER">Marker</label>
						<input type="text" name="CEM_MARKER" class="form-control" maxlength="15" value="<?php echo set_value('CEM_MARKER'); ?>" />	
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<!-- <div class="col-sm-3">
						<label for="CEM_CLDATE">Close Date</label>
						<input type="text" name="CEM_CLDATE" class="form-control date_picker" title="Close Date" readonly maxlength="" value="<?php echo set_value('CEM_CLDATE'); ?>" />	
					</div>
					<div class="col-sm-2 no_label_fix">
						<label for="CEM_CLFLAG">Close Flag</label>
						<input type="checkbox" name="CEM_CLFLAG" class="" title="Close Flag" maxlength="" value="<?php echo set_value('CEM_CLFLAG'); ?>" />
					</div> -->
					<div class="col-sm-5">
						<label for="CEM_MONVEND">Supplier</label>
						<input type="text" name="CEM_MONVEND" class="form-control" title="Supplier" maxlength="30" value="<?php echo set_value('CEM_MONVEND'); ?>" />	
					</div>
					<div class="col-sm-2 col-md-1">
						<label for="CEM_OWNYR">Year</label>
						<input type="text" name="CEM_OWNYR" class="form-control" title="Year" maxlength="4" value="<?php echo set_value('CEM_OWNYR'); ?>" />	
					</div>
					<div class="col-sm-2">
						<label for="CEM_PRPCARE">Perpetual Care</label>
						<select name="CEM_PRPCARE" class="form-control" title="Perpetual Care">
							<option value="Y" <?php echo set_select('CEM_PRPCARE', 'Y'); ?> >Yes</option>
							<option value="N" <?php echo set_select('CEM_PRPCARE', 'N'); ?> >No</option>
						</select>	
					</div>
				</div>
			</div>

			<!--  
			<legend></legend>

			<div class="form-group">
				<div class="col-sm-1">
					<label for="CEM_MARKER">Marker</label>
					<input type="text" name="CEM_MARKER" class="form-control" title="Marker" maxlength="15" value="<?php echo set_value('CEM_MARKER'); ?>" />	
				</div>
				<div class="col-sm-3">
					<label for="CEM_MICR_ROLL">Micro Roll</label>
					<input type="text" name="CEM_MICR_ROLL" class="form-control" title="Micro Roll" maxlength="4" value="<?php echo set_value('CEM_MICR_ROLL'); ?>" />
				</div>
				<div class="col-sm-3">
					<label for="CEM_MICR_FRM">Micro Frame</label>
					<input type="text" name="CEM_MICR_FRM" class="form-control" title="Micro Frame" maxlength="4" value="<?php echo set_value('CEM_MICR_FRM'); ?>" />
				</div>
			</div>
			 -->

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="CEM_CMNT">Comments</label>
						<textarea name="CEM_CMNT" class="form-control" title="Comments" maxlength="256"><?php echo set_value('CEM_CMNT'); ?></textarea>
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<label for="CEM_SPC_CMNT">Special Comments</label>
						<textarea name="CEM_SPC_CMNT" class="form-control" title="Special Comments" maxlength="256"><?php echo set_value('CEM_SPC_CMNT'); ?></textarea>
					</div>
				</div>
			</div>

			<!-- <legend></legend> -->

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-md btn-primary pull-right" value="Add Owner" />
						<a href="<?php echo base_url(); ?>cemetery/owners" class="btn btn-md btn-default">Back to Owners</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>