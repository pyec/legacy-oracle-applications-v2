<?php 
// determine if the user is an admin 
	$is_admin = false;
	$read_only = false;

	$groups = $this->session->userdata('groups');

	foreach ($groups as $group) 
	{
		if ($group == 'AppHRMAppsEngineeringDrawingLocationReadOnly')
		{
			$read_only = true;
		}

		if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
		{
			$is_admin = true;
			$read_only = false;
		}
	}
 ?>
<div class="row">
	<div class="col-md-6">
		<h2>Plan Index Query</h2>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<!-- <div class="row"> -->
	<form action="<?php echo base_url(); ?>EDL/plan_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="plan_number">Plan #</label>
					<input type="text" name="plan_number" class="form-control" title="Reference Number" value="<?php echo set_value('plan_number') ?>" tabindex="1" placeholder="Reference Number" />
				</div>
			</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right no_label_fix phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right no_label_fix" tabindex="9" onclick="plan_index_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
</div>

<div class="row">
	<div class="col-md-12 pull-right">
		<?php if (!empty($plans)): ?>
			<?php if (count($plans) == 500): ?>
				<h2 class="count"><?php echo count($plans) ?> records or more</h2>
			<?php else: ?>
				<h2 class="count"><?php echo count($plans) ?> records</h2>
			<?php endif ?>
		<?php else: ?>
			<h2 class="count">No records matched the search criteria</h2>
		<?php endif ?>

		<?php if (!$read_only): ?>
			<a href="<?php echo base_url(); ?>EDL/add_plan_index" class="btn btn-success pull-right">Add Record</a>
		<?php endif ?>
	</div>
	<div class="col-sm-12">
		<?php if (!empty($plans) && count($plans) == 500): ?>
			<?php if ($read_only): ?>
				<P style="margin-top: 30px" class="">Refine your search for more specific results.</P>			
			<?php else: ?>
				<P class="">Refine your search for more specific results.</P>			
			<?php endif ?>
		<?php endif ?>
	</div>
</div>

<?php if (!empty($plans)): ?>
<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Plan #</th>
				<th>Street #</th>
				<th>Street Name</th>
				<th>Description</th>
				<?php if ($is_admin): ?>
					<th>Action</th>
				<?php endif ?>
			</thead>
			<tbody class="tbody" >
					<?php foreach ($plans as $plan): ?>
						<tr>
							<td><a href="<?php echo base_url(); ?>EDL/update_plan_index/<?php echo $plan['PLAN_NUM'] ?>"><?php echo $plan['PLAN_NUM'] ?></a></td>
							<td><?php echo $plan['STR_NUMBER'] ?></td>
							<td><?php echo $plan['STR_NAME'] ?></td>
							<td><?php echo $plan['DESC1'] ?></td>
							<?php if ($is_admin): ?>
								<td>
									<form action="<?php echo base_url(); ?>EDL/delete_plan_index" method="POST">
										<button type="submit" class="btn btn-sm btn-danger delete_plan">Delete</button>
										<input type="hidden" name="PLAN_NUM" value="<?php echo $plan['PLAN_NUM'] ?>" />
									</form>
								</td>
							<?php endif ?>
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>