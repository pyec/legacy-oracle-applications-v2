

<?php
// determine if the user is an admin 
	$is_admin = false;
	$read_only = true;
    $is_readwrite = false;

	$groups = $this->session->userdata('groups');

	foreach ($groups as $group) 
	{

		//if ($group == 'AppHRMAppsEngineeringDrawingLocationReadOnly')
        if ($group == 'AppHRMAppsEngineeringDrawingLocationReadOnly')
		{
			$read_only = true;
		}

        if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite')
        //if ($group == 'AppHRMAppsEngineeringDrawingLocationReadWrite' || $group == 'AppHRMAppsAdmin')
        {
            $is_readwrite = true;
            $read_only = false;
        }

		if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin' || $group == 'AppHRMAppsAdmin')
        //if ($group == 'AppHRMAppsEngineeringDrawingLocationAdmin')
		{
			$is_admin = true;
            $is_readwrite = false;
			$read_only = false;
		}
	}
 ?>
<div class="row">
	<div class="col-md-6">
		<h2>Engineering Drawing Location Lookup</h2>
		<p>Select a file to view more info about it</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<!-- <div class="row"> -->
	<form action="<?php echo base_url(); ?>EDL/file_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="file_number">File ID</label>
					<input type="text" name="file_number" class="form-control" title="FIle Number" value="<?php echo $this->session->userdata('file_number'); ?>" tabindex="1" placeholder="File ID" />
				</div>

				<div class="col-sm-4">
					<label for="project_type">Project Type</label>
					<select class="form-control" id="project_type" name="project_type">
						<option value="" <?php set_select('project_type','', true) ?>>Select..</option>
						<?php foreach ($project_types as $value): ?>
                            <?php if ($value['PROJTYP_CODE'] == $this->session->userdata('project_type')): ?>
                                <option selected value="<?= $value['PROJTYP_CODE'] ?>"><?= $value['PROJTYP_CODE'].' - '.$value['PROJTYP_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $value['PROJTYP_CODE'] ?>"><?= $value['PROJTYP_CODE'].' - '.$value['PROJTYP_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-3">
					<label for="originator">Originator</label>
					<select class="form-control" name="originator" id="originator">
						<option value="">Select..</option>
						<?php foreach ($originators as $value): ?>
                            <?php if ($value['ORIGINATOR_CODE'] == $this->session->userdata('originator')): ?>
                                <option selected value="<?= $value['ORIGINATOR_CODE'] ?>"><?= $value['ORIGINATOR_CODE'].' - '.$value['ORIGINATOR_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $value['ORIGINATOR_CODE'] ?>"><?= $value['ORIGINATOR_CODE'].' - '.$value['ORIGINATOR_DSC'] ?></option>
                            <?php endif ?>
						<?php endforeach ?>
					</select>
				</div>
                <div class="col-sm-3">
                    <label for="originator">Surveyor</label>
                    <select class="form-control" name="surveyor" id="surveyor">
                        <option value="">Select..</option>
                        <?php foreach ($surveyors as $value): ?>
                            <?php if ($value['SURVEYOR_CODE'] == $this->session->userdata('surveyor')): ?>
                                <option selected value="<?= $value['SURVEYOR_CODE'] ?>"><?= $value['SURVEYOR_CODE'].' - '.$value['SURVEYOR_DSC'] ?></option>
                            <?php else : ?>
                                <option value="<?= $value['SURVEYOR_CODE'] ?>"><?= $value['SURVEYOR_CODE'].' - '.$value['SURVEYOR_DSC'] ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    </select>
                </div>

			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="signed_date">Signed Date</label>
					<input readonly type="text" name="signed_date" class="form-control date_picker_new" title="Signed Date" placeholder="Signed Date" value="<?php echo $this->session->userdata('signed_date') ?>" />
				</div>
				<div class="col-sm-2">
					<label for="tender_number">Tender #</label>
					<input type="text" name="tender_number" class="form-control" title="Tender Number" placeholder="Tender #" value="<?php echo $this->session->userdata('tender_number') ?>" />
				</div>
				<div class="col-sm-5">
					<label for="Description">Description</label>
					<input type="text" name="description" id="description" class="form-control" title="Description"  placeholder="Description" value="<?php echo $this->session->userdata('description') ?>"/>
				</div>
			</div>
		</div>
        <div class="form-group">
            <div class="row">
                <div class="col-sm-3">
                    <label for="from_plan">From Plan</label>
                    <input type="text" name="from_plan" class="form-control" title="From Plan" placeholder="From Plan" value="<?php echo $this->session->userdata('from_plan') ?>" />
                </div>
                <div class="col-sm-3">
                    <label for="from_plan">To Plan</label>
                    <input type="text" name="to_plan" class="form-control" title="To Plan" placeholder="To Plan" value="<?php echo $this->session->userdata('to_plan') ?>" />
                </div>
            </div>
        </div>
		<legend>Street</legend>
		<div class="form-group">
			<div class="row">

					<div class="col-sm-1">
					</div>
					<div class="col-sm-2">
						<label for="STREET">Name</label>
					</div>
					<div class="col-sm-1">
						<label for="STREET_TYP">Type</label>
					</div>
                    <div class="col-sm-2">
                        <label for="STREET_CDE">Number</label>
                    </div>
					<div class="col-sm-2">
						<label for="COMMUNITY">Community</label>
					</div>
				</div>
		</div>
		<div class="form-group">
			<div class="row">
					<div class="col-sm-1">
						<button type="button" class="modal_btn btn btn-primary btn-sm" value="search_street" data-toggle="modal" data-target="#myModal">Select</button>
					</div>
					<div class="col-sm-2">
						<input type="text" name="STREET-0" id="STREET-0" class="form-control 0-STREET" title="Street" value="<?php echo $this->session->userdata('STREET-0') ?>" readonly />
					</div>
					<div class="col-sm-1">
						<input type="text" name="STREET_TYPE-0" id="STREET_TYPE-0" class="form-control 0-STREET_TYP" title="Street Type" value="<?php echo $this->session->userdata('STREET_TYPE-0') ?>" readonly />
					</div>
                    <div class="col-sm-2">
                        <input type="text" name="STREET_CODE-0" id="STREET_CODE-0" class="form-control 0-STREET_CDE" title="Street Code" value="<?php echo $this->session->userdata('STREET_CODE-0') ?>" readonly  />
                    </div>
					<div class="col-sm-2">
						<input type="text" name="COMMUNITY-0" id="COMMUNITY-0" class="form-control 0-COMMUNITY" title="Community" value="<?php echo $this->session->userdata('COMMUNITY-0') ?>" readonly />
					</div>
				</div>
		</div>
		<legend>Surrounding Street</legend>
		<div class="form-group">
			<div class="row">
					<div class="col-sm-1">
					</div>
					<div class="col-sm-2">
						<label for="STREET">Name</label>
					</div>
					<div class="col-sm-1">
						<label for="STREET_TYP">Type</label>
					</div>
                    <div class="col-sm-2">
                        <label for="STREET_CDE">Number</label>
                    </div>
					<div class="col-sm-2">
						<label for="COMMUNITY">Community</label>
					</div>
				</div>
		</div>

		<div class="form-group">
			<div class="row">
					<div class="col-sm-1">
						<button type="button" class="modal_btn btn btn-primary btn-sm" value="search_surrounding_1" data-toggle="modal" data-target="#myModal">Select</button>
					</div>
					<div class="col-sm-2">
						<input type="text" name="STREET-1" id="STREET-1" class="form-control 1-STREET" title="Street" value="<?php echo $this->session->userdata('STREET-1') ?>" readonly />
					</div>
					<div class="col-sm-1">
						<input type="text" name="STREET_TYPE-1" id="STREET_TYPE-1" class="form-control 1-STREET_TYP" title="Street Type" value="<?php echo $this->session->userdata('STREET_TYPE-1') ?>" readonly />
					</div>
                    <div class="col-sm-2">
                        <input type="text" name="STREET_CODE-1" id="STREET_CODE-1" class="form-control 1-STREET_CDE" title="Street Code" value="<?php echo $this->session->userdata('STREET_CODE-1') ?>" readonly  />
                    </div>
					<div class="col-sm-2">
						<input type="text" name="COMMUNITY-1" id="COMMUNITY-1" class="form-control 1-COMMUNITY" title="Community" value="<?php echo $this->session->userdata('COMMUNITY-1') ?>" readonly />
					</div>
				</div>
		</div>
		
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<a href="<?php echo base_url(); ?>EDL" class="btn btn-default btn-md pull-right" style="margin-right: 0.5em">Clear</a>
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($files)): ?>
<!--			--><?php //if (count($files) == 500): ?>
<!--				<h2 class="count">--><?php //echo count($files) ?><!-- records or more</h2>-->
<!--			--><?php //else: ?>
				<h2 class="count"><?php echo $this->session->userdata('searchCount') ?> records</h2>
<!--			--><?php //endif ?>
		<?php elseif ($files !== null): ?>
            <div style="margin-bottom: 5em">
			    <h2 class="count">No records matched the search criteria</h2>
            </div>
		<?php endif ?>

		<?php if (!$read_only): ?>
        <div style="margin-bottom: 3em">
			<a href="<?php echo base_url(); ?>EDL/add_location" class="btn btn-success btn-sm pull-right" >Add Record</a>
		<?php endif ?>
	</div>
	<div class="col-sm-12">
		<?php if (!empty($files) && count($files) == 500): ?>
			<P style="margin-top: 30px;">Refine your search for more specific results.</P>			
		<?php endif ?>
	</div>
</div>

<?php if (!empty($files)): ?>
<div class="row no_label_fix">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover" id="tblSearch" name="tblSearch">
			<thead>
				<th>File ID</th>
                <th width="109px">Street</th>
				<th >Surrounding Street</th>
				<th >Surrounding Street</th>
				<th>Description</th>
                <th width="109px">From Plan</th>
                <th width="109px">To Plan</th>
                <th width="120px">Signed Date</th>
                <th>Date Note</th>
                <th>Project Type</th>
				<!-- <th>Originator</th> -->
			</thead>
			<tbody class="tbody">
					<?php foreach ($files as $file): ?>
						<tr>
							<td nowrap><a href="<?php echo base_url(); ?>EDL/update_location/<?php echo $file['FILE_ID'] ?>"><?php echo $file['FILE_ID'] ?></a></td>
							<td><?php echo $file['STREET'] ?></td>
							<td><?php if(!empty($file['surrounding_street_1'])){echo $file['surrounding_street_1'];} ?></td>
							<td><?php if(!empty($file['surrounding_street_2'])){echo $file['surrounding_street_2'];} ?></td>
							<td><?php echo $file['NOTES'] ?></td>
                            <td><?php echo $file['FROM_PLAN'] ?></td>
                            <td><?php echo $file['TO_PLAN'] ?></td>
                            <td><?php echo $file['SIGNED'] ?></td>
                            <td><?php echo $file['ARCHIVE_DATE'] ?></td>
                            <td data-toggle="tooltip" title="<?php echo $file['PROJTYP_DSC'] ?>"><?php echo $file['PROJTYP_CDE'] ?></td>
							<!-- <td><?php echo $file['ORIG_CDE'] ?></td> -->
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>

    <p><?php echo $links; ?></p>
<?php endif ?>



<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title">Street Look Up</h4>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="col-sm-5">
									<div class="modal_search">
										<label for="search_box">Street Name</label>
										<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" placeholder="Street Name" />
										<p class="text-muted">(Select the name of the record you would like to use)</p>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<table class="table table-bordered">
							<thead>
								<th>Name</th>
							</thead>
							<tbody class="modal_tbody">
								<tr>
									
								</tr>
							</tbody>	
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->