<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
| DATABASE CONNECTIVITY SETTINGS
| -------------------------------------------------------------------
| This file will contain the settings needed to access your database.
|
| For complete instructions please consult the 'Database Connection'
| page of the User Guide.
|
| -------------------------------------------------------------------
| EXPLANATION OF VARIABLES
| -------------------------------------------------------------------
|
|	['hostname'] The hostname of your database server.
|	['username'] The username used to connect to the database
|	['password'] The password used to connect to the database
|	['database'] The name of the database you want to connect to
|	['dbdriver'] The database type. ie: mysql.  Currently supported:
				 mysql, mysqli, postgre, odbc, mssql, sqlite, oci8
|	['dbprefix'] You can add an optional prefix, which will be added
|				 to the table name when using the  Active Record class
|	['pconnect'] TRUE/FALSE - Whether to use a persistent connection
|	['db_debug'] TRUE/FALSE - Whether database errors should be displayed.
|	['cache_on'] TRUE/FALSE - Enables/disables query caching
|	['cachedir'] The path to the folder where cache files should be stored
|	['char_set'] The character set used in communicating with the database
|	['dbcollat'] The character collation used in communicating with the database
|				 NOTE: For MySQL and MySQLi databases, this setting is only used
| 				 as a backup if your server is running PHP < 5.2.3 or MySQL < 5.0.7
|				 (and in table creation queries made with DB Forge).
| 				 There is an incompatibility in PHP with mysql_real_escape_string() which
| 				 can make your site vulnerable to SQL injection if you are using a
| 				 multi-byte character set and are running versions lower than these.
| 				 Sites using Latin-1 or UTF-8 database character set and collation are unaffected.
|	['swap_pre'] A default table prefix that should be swapped with the dbprefix
|	['autoinit'] Whether or not to automatically initialize the database.
|	['stricton'] TRUE/FALSE - forces 'Strict Mode' connections
|							- good for ensuring strict SQL while developing
|
| The $active_group variable lets you choose which connection group to
| make active.  By default there is only one group (the 'default' group).
|
| The $active_record variables lets you determine whether or not to load
| the active record class
*/

$active_group = 'default';
$active_record = TRUE;

$db['default']['hostname'] = '';
$db['default']['username'] = '';
$db['default']['password'] = '';
$db['default']['database'] = '';
$db['default']['dbdriver'] = '';
$db['default']['dbprefix'] = '';
$db['default']['pconnect'] = TRUE;
$db['default']['db_debug'] = TRUE;
$db['default']['cache_on'] = FALSE;
$db['default']['cachedir'] = '';
$db['default']['char_set'] = 'utf8';
$db['default']['dbcollat'] = 'utf8_general_ci';
$db['default']['swap_pre'] = '';
$db['default']['autoinit'] = TRUE;
$db['default']['stricton'] = FALSE;



/*
	This statement checks on the ENVIRONMENT variable and loads the proper db settings
*/
if (ENVIRONMENT == 'production')
{
    // Phonebook Assets
    $db['mssql_asset_db']['hostname'] 	 = 'msesrvc01';

    // Address and District Lookup
    $db['oracle_address_db']['hostname'] = 'LIS';
    $db['oracle_address_db']['username'] = 'app_hrm';
    $db['oracle_address_db']['password'] = 'sPOaq7eC';
    // $db['oracle_address_db']['database'] = 'LIS';
    $db['oracle_address_db']['dbdriver'] = 'oci8';
    // $db['oracle_address_db']['dbprefix'] = 'imsv7.';
    $db['oracle_address_db']['pconnect'] = TRUE;
    $db['oracle_address_db']['db_debug'] = TRUE;
    $db['oracle_address_db']['cache_on'] = FALSE;
    $db['oracle_address_db']['cachedir'] = '';
    $db['oracle_address_db']['char_set'] = 'utf8';
    $db['oracle_address_db']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_address_db']['swap_pre'] = '';
    $db['oracle_address_db']['autoinit'] = TRUE;
    $db['oracle_address_db']['stricton'] = FALSE;

    // Civic Addressing
    $db['oracle_street_file']['hostname'] = 'LIS';
    $db['oracle_street_file']['username'] = 'APP_STREET_FILE_UPDATE';
    $db['oracle_street_file']['password'] = 'TiEjluw0';
    // $db['oracle_street_file']['database'] = 'LIS';  // seems to have no affect on connection due to user schema rights
    $db['oracle_street_file']['dbdriver'] = 'oci8';
    // $db['oracle_street_file']['dbprefix'] = 'imsv7.';
    $db['oracle_street_file']['pconnect'] = TRUE;
    $db['oracle_street_file']['db_debug'] = TRUE;
    $db['oracle_street_file']['cache_on'] = FALSE;
    $db['oracle_street_file']['cachedir'] = '';
    $db['oracle_street_file']['char_set'] = 'utf8';
    $db['oracle_street_file']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_street_file']['swap_pre'] = '';
    $db['oracle_street_file']['autoinit'] = TRUE;
    $db['oracle_street_file']['stricton'] = FALSE;

    // Cemetery
    $db['oracle_cemetery']['hostname'] = 'HRM12';
    $db['oracle_cemetery']['username'] = 'APP_CEMETERY';
    $db['oracle_cemetery']['password'] = 'wRlEfrl3';
    // $db['oracle_cemetery']['database'] = 'HRM11';  // seems to have no affect on connection due to user schema rights
    $db['oracle_cemetery']['port'] = 1511;
    $db['oracle_cemetery']['dbdriver'] = 'oci8';
    $db['oracle_cemetery']['dbprefix'] = '';
    $db['oracle_cemetery']['pconnect'] = TRUE;
    $db['oracle_cemetery']['db_debug'] = TRUE;
    $db['oracle_cemetery']['cache_on'] = FALSE;
    $db['oracle_cemetery']['cachedir'] = '';
    $db['oracle_cemetery']['char_set'] = 'utf8';
    $db['oracle_cemetery']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_cemetery']['swap_pre'] = '';
    $db['oracle_cemetery']['autoinit'] = TRUE;
    $db['oracle_cemetery']['stricton'] = FALSE;

    // HRIS
    $db['oracle_HRIS']['hostname'] = 'HRM12';
    $db['oracle_HRIS']['username'] = 'APP_HRTRAIN';
    $db['oracle_HRIS']['password'] = 'pR4s42SAph2nEpuyu';
    $db['oracle_HRIS']['database'] = 'HRIS';
    $db['oracle_HRIS']['port'] = 1511;
    $db['oracle_HRIS']['dbdriver'] = 'oci8';
    $db['oracle_HRIS']['dbprefix'] = '';
    $db['oracle_HRIS']['pconnect'] = TRUE;
    $db['oracle_HRIS']['db_debug'] = TRUE;
    $db['oracle_HRIS']['cache_on'] = FALSE;
    $db['oracle_HRIS']['cachedir'] = '';
    $db['oracle_HRIS']['char_set'] = 'utf8';
    $db['oracle_HRIS']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_HRIS']['swap_pre'] = '';
    $db['oracle_HRIS']['autoinit'] = TRUE;
    $db['oracle_HRIS']['stricton'] = FALSE;

    // Business Unit Update
    $db['oracle_business_unit']['hostname'] = 'HRM11';
    $db['oracle_business_unit']['username'] = 'APP_BUSINESS_UNIT_UPDATE';
    $db['oracle_business_unit']['password'] = 'jla4WOus';
    // $db['oracle_business_unit']['database'] = 'HRM11';
    $db['oracle_business_unit']['dbdriver'] = 'oci8';
    // $db['oracle_business_unit']['dbprefix'] = 'ifdadm..';
    $db['oracle_business_unit']['port'] = 1511;
    $db['oracle_business_unit']['pconnect'] = TRUE;
    $db['oracle_business_unit']['db_debug'] = TRUE;
    $db['oracle_business_unit']['cache_on'] = FALSE;
    $db['oracle_business_unit']['cachedir'] = '';
    $db['oracle_business_unit']['char_set'] = 'utf8';
    $db['oracle_business_unit']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_business_unit']['swap_pre'] = '';
    $db['oracle_business_unit']['autoinit'] = TRUE;
    $db['oracle_business_unit']['stricton'] = FALSE;

    // TPW
    $db['oracle_PWT']['hostname'] = 'HRM11';
    //$db['oracle_PWT']['username'] = 'APP_TPW';
    $db['oracle_PWT']['username'] = 'app_TWP';
    $db['oracle_PWT']['password'] = 'hoEcri3C';
    // $db['oracle_PWT']['database'] = 'DEVHRM11';  // seems to have no affect on connection due to user schema rights
    $db['oracle_PWT']['dbdriver'] = 'oci8';
    // $db['oracle_PWT']['dbprefix'] = 'ettadm.';
    $db['oracle_PWT']['port'] = 1511;
    $db['oracle_PWT']['pconnect'] = TRUE;
    $db['oracle_PWT']['db_debug'] = FALSE;
    $db['oracle_PWT']['cache_on'] = FALSE;
    $db['oracle_PWT']['cachedir'] = '';
    $db['oracle_PWT']['char_set'] = 'utf8';
    $db['oracle_PWT']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_PWT']['swap_pre'] = '';
    $db['oracle_PWT']['autoinit'] = TRUE;
    $db['oracle_PWT']['stricton'] = FALSE;

    // SOT
    $db['oracle_SOT']['hostname'] = 'HRM11';
    $db['oracle_SOT']['username'] = 'APP_SOT';
    $db['oracle_SOT']['password'] = 'giA0rLuf';
    $db['oracle_SOT']['dbdriver'] = 'oci8';
    $db['oracle_SOT']['port'] = 1511;
    // $db['oracle_SOT']['dbprefix'] = 'actadm.';
    $db['oracle_SOT']['pconnect'] = TRUE;
    $db['oracle_SOT']['db_debug'] = TRUE;
    $db['oracle_SOT']['cache_on'] = FALSE;
    $db['oracle_SOT']['cachedir'] = '';
    $db['oracle_SOT']['char_set'] = 'utf8';
    $db['oracle_SOT']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_SOT']['swap_pre'] = '';
    $db['oracle_SOT']['autoinit'] = TRUE;
    $db['oracle_SOT']['stricton'] = FALSE;

    // Mayor Call Tracking
    $db['oracle_MCT']['hostname'] = 'HRM11';
    $db['oracle_MCT']['username'] = 'APP_MCT';
    $db['oracle_MCT']['password'] = 'sWl2cHlu';
    $db['oracle_MCT']['database'] = 'HRM11';
    $db['oracle_MCT']['dbdriver'] = 'oci8';
    // $db['oracle_MCT']['dbprefix'] = 'mct.';
    $db['oracle_MCT']['port'] = 1511;
    $db['oracle_MCT']['pconnect'] = TRUE;
    $db['oracle_MCT']['db_debug'] = TRUE;
    $db['oracle_MCT']['cache_on'] = FALSE;
    $db['oracle_MCT']['cachedir'] = '';
    $db['oracle_MCT']['char_set'] = 'utf8';
    $db['oracle_MCT']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_MCT']['swap_pre'] = '';
    $db['oracle_MCT']['autoinit'] = TRUE;
    $db['oracle_MCT']['stricton'] = FALSE;

    // EDL
    $db['oracle_EDL']['hostname'] = 'HRM12';
    $db['oracle_EDL']['username'] = 'APP_EDL';
    $db['oracle_EDL']['password'] = 'umcOSTeF4yrht';
    $db['oracle_EDL']['database'] = 'DEVHRM11';
    $db['oracle_EDL']['dbdriver'] = 'oci8';
    // $db['oracle_EDL']['dbprefix'] = 'edladm.';
    $db['oracle_EDL']['port'] = 1511;
    $db['oracle_EDL']['pconnect'] = TRUE;
    $db['oracle_EDL']['db_debug'] = TRUE;
    $db['oracle_EDL']['cache_on'] = FALSE;
    $db['oracle_EDL']['cachedir'] = '';
    $db['oracle_EDL']['char_set'] = 'utf8';
    $db['oracle_EDL']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_EDL']['swap_pre'] = '';
    $db['oracle_EDL']['autoinit'] = TRUE;
    $db['oracle_EDL']['stricton'] = FALSE;

    // Traffic Surveys
    $db['oracle_traffic']['hostname'] = 'HRM11';
    $db['oracle_traffic']['username'] = 'APP_TRAFFIC_VOLUME';
    $db['oracle_traffic']['password'] = 'boU6Ieyo';
    $db['oracle_traffic']['database'] = 'HRM11';
    $db['oracle_traffic']['dbdriver'] = 'oci8';
    $db['oracle_traffic']['dbprefix'] = 'pwtsadm.';
    // $db['oracle_cemetery']['port'] = 1511;
    $db['oracle_traffic']['pconnect'] = TRUE;
    $db['oracle_traffic']['db_debug'] = TRUE;
    $db['oracle_traffic']['cache_on'] = FALSE;
    $db['oracle_traffic']['cachedir'] = '';
    $db['oracle_traffic']['char_set'] = 'utf8';
    $db['oracle_traffic']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_traffic']['swap_pre'] = '';
    $db['oracle_traffic']['autoinit'] = TRUE;
    $db['oracle_traffic']['stricton'] = FALSE;
}
else // Development
{
    // Phonebook Assets
    $db['mssql_asset_db']['hostname'] 	 = 'msesrvc03-dev';

    // Address and District Lookup
    // $db['oracle_address_db']['hostname'] = 'QALIS';
    // $db['oracle_address_db']['username'] = 'imsv7';
    // $db['oracle_address_db']['password'] = 'seven';
    $db['oracle_address_db']['hostname'] = 'DEVHRM11';
    $db['oracle_address_db']['username'] = 'app_hrm';
    $db['oracle_address_db']['password'] = 'rraNepucha7UD';
    // $db['oracle_address_db']['database'] = 'QALIS';
    $db['oracle_address_db']['dbdriver'] = 'oci8';
    // $db['oracle_address_db']['dbprefix'] = 'imsv7.';
    $db['oracle_address_db']['pconnect'] = TRUE;
    $db['oracle_address_db']['db_debug'] = TRUE;
    $db['oracle_address_db']['cache_on'] = FALSE;
    $db['oracle_address_db']['cachedir'] = '';
    $db['oracle_address_db']['char_set'] = 'utf8';
    $db['oracle_address_db']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_address_db']['swap_pre'] = '';
    $db['oracle_address_db']['autoinit'] = TRUE;
    $db['oracle_address_db']['stricton'] = FALSE;

    // Civic Addressing
    $db['oracle_street_file']['hostname'] = 'HRM12DEV';
    $db['oracle_street_file']['username'] = 'APP_STREET_FILE_UPDATE';
    $db['oracle_street_file']['password'] = 'Ha7awRuBastE5';
    // $db['oracle_street_file']['database'] = 'LIS';  // seems to have no affect on connection due to user schema rights
    $db['oracle_street_file']['dbdriver'] = 'oci8';
    // $db['oracle_street_file']['dbprefix'] = 'imsv7.';
    $db['oracle_street_file']['pconnect'] = TRUE;
    $db['oracle_street_file']['db_debug'] = TRUE;
    $db['oracle_street_file']['cache_on'] = FALSE;
    $db['oracle_street_file']['cachedir'] = '';
    $db['oracle_street_file']['char_set'] = 'utf8';
    $db['oracle_street_file']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_street_file']['swap_pre'] = '';
    $db['oracle_street_file']['autoinit'] = TRUE;
    $db['oracle_street_file']['stricton'] = FALSE;

    // Cemetery
    $db['oracle_cemetery']['hostname'] = 'HRM12DEV';
    //$db['oracle_cemetery']['hostname'] = 'DEVHRM11';
    $db['oracle_cemetery']['username'] = 'APP_CEMETERY';
    $db['oracle_cemetery']['password'] = 'GabRAxaheC4uk';
    // $db['oracle_cemetery']['database'] = 'HRM11';  // seems to have no affect on connection due to user schema rights
    $db['oracle_cemetery']['port'] = 1511;
    $db['oracle_cemetery']['dbdriver'] = 'oci8';
    $db['oracle_cemetery']['dbprefix'] = '';
    $db['oracle_cemetery']['pconnect'] = TRUE;
    $db['oracle_cemetery']['db_debug'] = TRUE;
    $db['oracle_cemetery']['cache_on'] = FALSE;
    $db['oracle_cemetery']['cachedir'] = '';
    $db['oracle_cemetery']['char_set'] = 'utf8';
    $db['oracle_cemetery']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_cemetery']['swap_pre'] = '';
    $db['oracle_cemetery']['autoinit'] = TRUE;
    $db['oracle_cemetery']['stricton'] = FALSE;

    // HRIS
    //$db['oracle_HRIS']['hostname'] = 'devhrm11';
    $db['oracle_HRIS']['hostname'] = 'HRM12DEV';
    $db['oracle_HRIS']['username'] = 'APP_HRTRAIN';
    $db['oracle_HRIS']['password'] = 'b4eDresWUY5xU';
    $db['oracle_HRIS']['database'] = 'HRIS';
    $db['oracle_HRIS']['port'] = 1511;
    $db['oracle_HRIS']['dbdriver'] = 'oci8';
    $db['oracle_HRIS']['dbprefix'] = '';
    $db['oracle_HRIS']['pconnect'] = TRUE;
    $db['oracle_HRIS']['db_debug'] = TRUE;
    $db['oracle_HRIS']['cache_on'] = FALSE;
    $db['oracle_HRIS']['cachedir'] = '';
    $db['oracle_HRIS']['char_set'] = 'utf8';
    $db['oracle_HRIS']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_HRIS']['swap_pre'] = '';
    $db['oracle_HRIS']['autoinit'] = TRUE;
    $db['oracle_HRIS']['stricton'] = FALSE;

    // Business Unit Update
    $db['oracle_business_unit']['hostname'] = 'HRM12DEV';
    $db['oracle_business_unit']['username'] = 'APP_BUSINESS_UNIT_UPDATE';
    $db['oracle_business_unit']['password'] = 'fhA5uyavutema';
    // $db['oracle_business_unit']['database'] = 'HRM11';
    $db['oracle_business_unit']['dbdriver'] = 'oci8';
    // $db['oracle_business_unit']['dbprefix'] = 'ifdadm..';
    $db['oracle_business_unit']['port'] = 1511;
    $db['oracle_business_unit']['pconnect'] = TRUE;
    $db['oracle_business_unit']['db_debug'] = TRUE;
    $db['oracle_business_unit']['cache_on'] = FALSE;
    $db['oracle_business_unit']['cachedir'] = '';
    $db['oracle_business_unit']['char_set'] = 'utf8';
    $db['oracle_business_unit']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_business_unit']['swap_pre'] = '';
    $db['oracle_business_unit']['autoinit'] = TRUE;
    $db['oracle_business_unit']['stricton'] = FALSE;

    // TPW
    $db['oracle_PWT']['hostname'] = 'HRM12DEV';
    $db['oracle_PWT']['username'] = 'APP_TPW';
//    $db['oracle_PWT']['hostname'] = 'DEVHRM11';
//    $db['oracle_PWT']['username'] = 'APP_TWP';
    $db['oracle_PWT']['password'] = 'cHuze7reveSuc';
    // $db['oracle_PWT']['database'] = 'DEVHRM11';  // seems to have no affect on connection due to user schema rights
    $db['oracle_PWT']['dbdriver'] = 'oci8';
    // $db['oracle_PWT']['dbprefix'] = 'ettadm.';
    $db['oracle_PWT']['port'] = 1511;
    $db['oracle_PWT']['pconnect'] = TRUE;
    $db['oracle_PWT']['db_debug'] = TRUE;
    $db['oracle_PWT']['cache_on'] = FALSE;
    $db['oracle_PWT']['cachedir'] = '';
    $db['oracle_PWT']['char_set'] = 'utf8';
    $db['oracle_PWT']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_PWT']['swap_pre'] = '';
    $db['oracle_PWT']['autoinit'] = TRUE;
    $db['oracle_PWT']['stricton'] = FALSE;

    // SOT
    $db['oracle_SOT']['hostname'] = 'HRM12DEV';
    $db['oracle_SOT']['username'] = 'APP_SOT';
    $db['oracle_SOT']['password'] = 'WeQadasPe4reT';
    $db['oracle_SOT']['dbdriver'] = 'oci8';
    $db['oracle_SOT']['port'] = 1511;
    // $db['oracle_SOT']['dbprefix'] = 'actadm.';
    $db['oracle_SOT']['pconnect'] = TRUE;
    $db['oracle_SOT']['db_debug'] = TRUE;
    $db['oracle_SOT']['cache_on'] = FALSE;
    $db['oracle_SOT']['cachedir'] = '';
    $db['oracle_SOT']['char_set'] = 'utf8';
    $db['oracle_SOT']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_SOT']['swap_pre'] = '';
    $db['oracle_SOT']['autoinit'] = TRUE;
    $db['oracle_SOT']['stricton'] = FALSE;

    // Mayor Call Tracking
    $db['oracle_MCT']['hostname'] = 'HRM12DEV';
    $db['oracle_MCT']['username'] = 'APP_MCT';
    $db['oracle_MCT']['password'] = 'KUs5efreTufuf';
    $db['oracle_MCT']['database'] = 'HRM11';
    $db['oracle_MCT']['dbdriver'] = 'oci8';
    // $db['oracle_MCT']['dbprefix'] = 'mct.';
    $db['oracle_MCT']['port'] = 1511;
    $db['oracle_MCT']['pconnect'] = TRUE;
    $db['oracle_MCT']['db_debug'] = TRUE;
    $db['oracle_MCT']['cache_on'] = FALSE;
    $db['oracle_MCT']['cachedir'] = '';
    $db['oracle_MCT']['char_set'] = 'utf8';
    $db['oracle_MCT']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_MCT']['swap_pre'] = '';
    $db['oracle_MCT']['autoinit'] = TRUE;
    $db['oracle_MCT']['stricton'] = FALSE;

    // EDL
    $db['oracle_EDL']['hostname'] = 'HRM12DEV';
    $db['oracle_EDL']['username'] = 'APP_EDL';
    $db['oracle_EDL']['password'] = 'thry4FeTSOcmu';
    //$db['oracle_EDL']['hostname'] = 'HRM12qa';
    //$db['oracle_EDL']['username'] = 'EDLADM';
    //$db['oracle_EDL']['password'] = 'N0th1ng';
    //$db['oracle_EDL']['database'] = 'DEVHRM11';
    $db['oracle_EDL']['dbdriver'] = 'oci8';
    // $db['oracle_EDL']['dbprefix'] = 'edladm.';
    $db['oracle_EDL']['port'] = 1511;
    $db['oracle_EDL']['pconnect'] = TRUE;
    $db['oracle_EDL']['db_debug'] = TRUE;
    $db['oracle_EDL']['cache_on'] = FALSE;
    $db['oracle_EDL']['cachedir'] = '';
    $db['oracle_EDL']['char_set'] = 'utf8';
    $db['oracle_EDL']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_EDL']['swap_pre'] = '';
    $db['oracle_EDL']['autoinit'] = TRUE;
    $db['oracle_EDL']['stricton'] = FALSE;

    // Traffic Surveys
    $db['oracle_traffic']['hostname'] = 'HRM12DEV';
    $db['oracle_traffic']['username'] = 'APP_TRAFFIC_VOLUME';
    $db['oracle_traffic']['password'] = 'WeBEBreme4teG';
    $db['oracle_traffic']['database'] = 'HRM11';
    $db['oracle_traffic']['dbdriver'] = 'oci8';
    $db['oracle_traffic']['dbprefix'] = 'PWTSADM.';
    // $db['oracle_cemetery']['port'] = 1511;
    $db['oracle_traffic']['pconnect'] = TRUE;
    $db['oracle_traffic']['db_debug'] = TRUE;
    $db['oracle_traffic']['cache_on'] = FALSE;
    $db['oracle_traffic']['cachedir'] = '';
    $db['oracle_traffic']['char_set'] = 'utf8';
    $db['oracle_traffic']['dbcollat'] = 'utf8_general_ci';
    $db['oracle_traffic']['swap_pre'] = '';
    $db['oracle_traffic']['autoinit'] = TRUE;
    $db['oracle_traffic']['stricton'] = FALSE;

}



/*-----------------------------------------------------------------------------------
	Phonebook Assests DB
------------------------------------------------------------------------------------*/
$db['mssql_asset_db']['username'] = 'sqladmin';
$db['mssql_asset_db']['password'] = 'Hal1fax@2012';
$db['mssql_asset_db']['database'] = 'hrm_apps';
$db['mssql_asset_db']['dbdriver'] = 'sqlsrv';
$db['mssql_asset_db']['dbprefix'] = '';
$db['mssql_asset_db']['pconnect'] = TRUE;
$db['mssql_asset_db']['db_debug'] = TRUE;
$db['mssql_asset_db']['cache_on'] = FALSE;
$db['mssql_asset_db']['cachedir'] = '';
$db['mssql_asset_db']['char_set'] = 'utf8';
$db['mssql_asset_db']['dbcollat'] = 'utf8_general_ci';
$db['mssql_asset_db']['swap_pre'] = '';
$db['mssql_asset_db']['autoinit'] = TRUE;
$db['mssql_asset_db']['stricton'] = FALSE;



/*-----------------------------------------------------------------------------------
	Street File System connection
------------------------------------------------------------------------------------*/
// $db['oracle_street_file']['hostname'] = 'QALIS';	//QA



/*-----------------------------------------------------------------------------------
	Cemetery System connection
------------------------------------------------------------------------------------*/

// $db['oracle_cemetery']['username'] = 'cemadm';
// $db['oracle_cemetery']['password'] = 'plots';

/*-----------------------------------------------------------------------------------
	HRIS System connection
------------------------------------------------------------------------------------*/

// $db['oracle_HRIS']['username'] = 'hris';
// $db['oracle_HRIS']['password'] = 'n0tpr0d';


/*-----------------------------------------------------------------------------------`
	Business Unit Update System connection
------------------------------------------------------------------------------------*/

// $db['oracle_business_unit']['username'] = 'ifdadm';
// $db['oracle_business_unit']['password'] = 'forms';


/*-----------------------------------------------------------------------------------
	TPW System connection
------------------------------------------------------------------------------------*/

// $db['oracle_PWT']['username'] = 'ettadm';
// $db['oracle_PWT']['password'] = 'courses';


/*-----------------------------------------------------------------------------------
	SOT System connection
------------------------------------------------------------------------------------*/

// $db['oracle_SOT']['database'] = 'HRM11';  // seems to have no affect on connection due to user schema rights


/*-----------------------------------------------------------------------------------
	MCT System connection
------------------------------------------------------------------------------------*/

// $db['oracle_MCT']['username'] = 'mct';
// $db['oracle_MCT']['password'] = 'tvor';



/*-----------------------------------------------------------------------------------
	EDL System connection
------------------------------------------------------------------------------------*/

// $db['oracle_EDL']['username'] = 'edladm';
// $db['oracle_EDL']['password'] = 'edlora';



/*-----------------------------------------------------------------------------------
	traffic System connection
------------------------------------------------------------------------------------*/

// $db['oracle_traffic']['username'] = 'pwtsadm';
// $db['oracle_traffic']['password'] = 'traffic';

/* End of file database.php */
/* Location: ./application/config/database.php */