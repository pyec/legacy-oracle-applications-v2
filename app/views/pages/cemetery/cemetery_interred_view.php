

<h2><?php echo $cemetery[0]['CEM_NAME']?></h2>
<?php
    $baseUrl = base_url();
    $baseUrl = $baseUrl . 'cemetery/update_owner/';
?>
<?php if (!empty($interred)): ?>

            <table id="tblResults" class="table table-bordered table-striped">
                <thead>
                <th>Owner</th>
                <th>Interred</th>
                <th>Interred Date</th>
                <th>Location</th>
                <th>Section</th>

                </thead>
                <tbody class="tbody">
                <?php foreach ($interred as $value):
                    $section = $value['SECTION'];
                    $location = $value['CEM_SCTN'];
                    $interredDate = $value['INT_DTE'];
                    $interred = trim($value['INT_LNAME']," \t\n\r\0\x0B") .', '. trim($value['INT_FNAME']," \t\n\r\0\x0B") .' '. trim($value['INT_MNAME']," \t\n\r\0\x0B");

                    $ownerLastName = $value['CEM_LNAME'];
                    $ownerFirstName = $value['CEM_FNAME'];
                    $ownerMiddleName = $value['CEM_MNAME'];

                    $owner = trim($ownerLastName," \t\n\r\0\x0B") . ', ' . trim($ownerFirstName," \t\n\r\0\x0B") . ' ' . trim($ownerMiddleName," \t\n\r\0\x0B");

                    $ownerNameHtml = '<td>' . $owner . '</td>';
                    $ownerRefNum = $value['CEM_REFNBR'];
                    $ownerCemNum = $cemetery[0]['CEM_NBR'];
                    $ownerLink = '<td><a href="' . $baseUrl . $ownerRefNum . '/' . $ownerCemNum . '">' . $owner .'</a></td>';

                    $interredHtml = '<td>' . $interred . '</td>';
                    $interredDateHtml = '<td>' . $interredDate . '</td>';

                    $locationHtml = '<td>' . $location . '</td>';
                    $sectionHtml = '<td>' . $section . '</td>';
                    ?>
                    <tr>
                        <?php
                            echo $ownerLink;
                            echo $interredHtml;
                            echo $interredDateHtml;
                            echo $locationHtml;
                            echo $sectionHtml;
                        ?>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>

<?php endif ?>