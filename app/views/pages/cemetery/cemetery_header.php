<?php
$BILink = '';

if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'production':
            $BILink = BI_PROD_URL;
            break;
        default:
            $BILink = BI_QA_URL;
    }
}
?>
		<nav class="navbar navbar-default navbar-fixed-top app_nav" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="nav_items">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle app_header" data-toggle="collapse" data-target=".navbar-ex2-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a href="<?php echo base_url(); ?>cemetery" class="navbar-brand">Cemetery</a>
					</div>
				
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex2-collapse">
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url(); ?>cemetery/owners">Owners</a></li>
							<li><a href="<?php echo base_url(); ?>cemetery/interred">Interred</a></li>
							<li><a href="<?php echo base_url(); ?>cemetery/cemeteryNamesAndNumbers">Cemetery Names</a></li>
                            <li><a target="_blank" href="<?php echo $BILink ?>/BOE/BI?startFolder=Aa.7ZWthXoNHoYmZb3A3Sqc&isCat=false">Reports</a></li>
						</ul>

						</ul>
					</div><!-- /.navbar-collapse -->
				</div>
			</div>
		</nav>


