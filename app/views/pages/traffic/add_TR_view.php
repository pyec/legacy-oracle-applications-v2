<div class="row">
	<div class="col-md-12">
		<h2>Add Traffic Regulation</h2>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?php echo base_url(); ?>traffic/add_TR" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="REG_NUM">Regulation #</label>
						<input type="text" name="REG_NUM" class="form-control" title="Record Number" maxlength="7" />
					</div>
					<div class="col-sm-3">
						<label for="STR_SIDE">Direction</label>
						<select name="STR_SIDE" class="form-control" title="Direction">
							<option value="BO">Both</option>
							<option value="NS">N / S</option>
							<option value="ES">E / S</option>
							<option value="SS">S / S</option>
							<option value="WS">W / S</option>
						</select>
					</div>
					<div class="col-sm-3">
						<label for="CIV_ADDR">Civic Address</label> 
						<input type="text" name="CIV_ADDR" class="form-control" title="Civic Address" maxlength="5" />
					</div>
				</div>
			</div>

			

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="SIGNS" class="no_label_fix">Signs</label>
						<input type="checkbox" name="SIGNS" class="" title="Signs" />
					</div>
					<div class="col-sm-2">
						<label for="SIGNALS" class="no_label_fix">Signal</label>
						<input type="checkbox" name="SIGNALS" class="" title="Signal" />
					</div>
					<div class="col-sm-2">
						<label for="TRANSIT" class="no_label_fix">Transit</label>
						<input type="checkbox" name="TRANSIT" class="" title="Transit" />
					</div>
					<div class="col-sm-2">
						<label for="MARKINGS" class="no_label_fix">Markings</label>
						<input type="checkbox" name="MARKINGS" class="" title="Markings" />
					</div>
					<div class="col-sm-2">
						<label for="METERS" class="no_label_fix">Meters</label>
						<input type="checkbox" name="METERS" class="" title="Meters" />
					</div>
					<div class="col-sm-2">
						<label for="TAXI" class="no_label_fix">Taxi</label>
						<input type="checkbox" name="TAXI" class="" title="Taxi" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="OTHER">Other</label>
						<input type="checkbox" name="OTHER" class="" title="Other" />
					</div>
				</div>
			</div>

			<legend>Intersection</legend>
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="MAIN_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="MAIN_CODE" class="form-control no_label_fix main_street_code" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET_CDE'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<label for="">Street Name</label>
								<input type="text"  class="form-control main_street_name" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<label for="">Type</label>
								<input type="text" class="form-control main_street_type" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<label for="">Community</label>
								<input type="text" class="form-control main_community" readonly value="<?php echo $value = (!empty($streets[0]) ? $streets[0]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STRA_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STRA_CODE" class="form-control A_street_code" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET_CDE'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<label for="">At</label>
								<input type="text"  class="form-control A_street_name" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix A_street_type" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix A_community" readonly value="<?php echo $value = (!empty($streets[1]) ? $streets[1]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STRA2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STRA2_CODE" class="form-control A2_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_CDE'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix A2_street_name" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix A2_street_type" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix A2_community" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STRA3_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STRA3_CODE" class="form-control A3_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET_CDE'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix A3_street_name" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix A3_street_type" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix A3_community" readonly value="<?php echo $value = (!empty($streets[3]) ? $streets[3]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>
					
					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STRA4_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STRA4_CODE" class="form-control A4_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET_CDE'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix A4_street_name" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix A4_street_type" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix A4_community" readonly value="<?php echo $value = (!empty($streets[4]) ? $streets[4]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>

			<legend>Mid Block</legend>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STRB_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STRB_CODE" class="form-control no_label_fix B_street_code" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['STREET_CDE'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text"  class="form-control no_label_fix B_street_name" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix B_street_type" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix B_community" readonly value="<?php echo $value = (!empty($streets[5]) ? $streets[5]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>

					<div class="form-group">
						<div class="row">
							<div class="col-sm-1">
								<button type="button" value="STRB2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
							</div>
							<div class="col-sm-1">
								<input type="hidden" name="STRB2_CODE" class="form-control B2_street_code" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['STREET_CDE'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<label for="">Between</label>
								<input type="text"  class="form-control B2_street_name" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['STREET'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix B2_street_type" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['STREET_TYP'] : ''); ?>" />
							</div>
							<div class="col-sm-3">
								<input type="text" class="form-control no_label_fix B2_community" readonly value="<?php echo $value = (!empty($streets[6]) ? $streets[6]['COMMUNITY'] : ''); ?>" />
							</div>
						</div>
					</div>

			<legend>Additional Information</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="ISSUE_DATE">Issue Date</label>
						<input type="text" name="ISSUE_DATE" class="form-control date_picker" title="Issue Date" readonly />
						<!-- Can I only have one date box? -->
					</div>
					<div class="col-sm-2">
						<label for="C_DATE">Completion Date</label>
						<input type="text" name="C_DATE" class="form-control date_picker" title="Completion Date" readonly />
					</div>
					<div class="col-sm-2">
						<label for="STR_REG">Region</label>
						<select name="STR_REG" class="form-control" title="Region" maxlength="3" >
							<option value="">select..</option>
							<option value="EST">Eastern</option>
							<option value="WST">Western</option>
							<option value="CNT">Central</option>
						</select>
					</div>
					<div class="col-sm-6">
						<label for="COMMENTS">Comments</label>
						<textarea name="COMMENTS" class="form-control" title="Comments" maxlength="200"></textarea>
					</div>
				</div>
			</div>

			

			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="pull-right btn btn-primary btn-md" value="Add Survey" />
						<a href="<?php echo base_url(); ?>traffic/TR" class="btn btn-default btn-md">Back to Traffic Regulations</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Street Look Up</h4>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="col-sm-5">
							<div class="modal_search">
								<label for="search_box">Street Name</label>
								<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
								<p class="text-muted">(Select the name of the record you would like to use.)</p>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Name</th>
					</thead>
					<tbody class="modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->