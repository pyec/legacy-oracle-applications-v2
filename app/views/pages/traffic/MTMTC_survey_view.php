<div class="row">
	<div class="col-md-12">
		<h2>Manual Turning Movement Traffic Counts Survey</h2>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>
<div class="row">
	<form action="<?php echo base_url(); ?>traffic/add_MTMTC" method="POST">
		<div class="col-md-12">
			<div class="form-group">
				<div class="row">
					<div class="col-sm-2">
						<label for="REC_CODE">Record #</label>
						<input type="text" name="REC_CODE" class="form-control" title="Record Number" maxlength="10" value="<?php echo set_value('REC_CODE') ?>" />
					</div>
					<div class="col-sm-2">
						<label for="S_AM" class="no_label_fix">AM</label>
						<input type="checkbox" name="S_AM" class="" />
					</div>
					<div class="col-sm-2">
						<label for="S_PM" class="no_label_fix">PM</label>
						<input type="checkbox" name="S_PM" class="" />
					</div>
					<div class="col-sm-3">
						<label for="S_OP" class="no_label_fix" title="Off Peak" >OP</label>
						<input type="checkbox" name="S_OP" class="" title="Off Peak" />
					</div>
					<div class="col-sm-2">
						<label for="CREATE_DATE">Created On</label>
						<input type="text" name="CREATE_DATE" class="form-control date_picker" title="Created On" readonly value="<?php echo set_value('CREATE_DATE') ?>" />
					</div>
				</div>
			</div>

			<legend>Intersection</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR_CODE" class="form-control street_code" readonly value="" value="<?php echo set_value('STR_CODE') ?>" />
					</div>
					<div class="col-sm-3">
						<label for="text">Street Name</label>
						<input type="text"  class="form-control street_name" readonly value="" />
					</div>
					<div class="col-sm-3">
						<label for="hidden">Type</label>
						<input type="text" class="form-control street_type" readonly value="" />
					</div>
					<div class="col-sm-3">
						<label for="hidden">Community</label>
						<input type="text" class="form-control community" readonly value="" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR2_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR2_CODE" class="form-control 2_street_code" readonly value="<?php echo set_value('STR2_CODE') ?>" />
					</div>
					<div class="col-sm-3">
						<label for="">At</label>
						<input type="text"  class="form-control  2_street_name" readonly value="" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 2_street_type" readonly value="" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 2_community" readonly value="" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR3_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR3_CODE" class="form-control 3_street_code no_label_fix" readonly value="<?php echo set_value('STR3_CODE') ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix 3_street_name" readonly value="" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 3_street_type" readonly value="" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 3_community" readonly value="" />
					</div>
				</div>
			</div>	

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR4_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR4_CODE" class="form-control 4_street_code no_label_fix" readonly value="<?php echo set_value('REC_CODE') ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix 4_street_name" readonly value="" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 4_street_type" readonly value="" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 4_community" readonly value="" />
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-1">
						<button type="button" value="STR5_CODE" class="btn btn-primary btn-sm no_label_fix modal_btn" data-toggle="modal" data-target="#myModal">Select Street</button>
					</div>
					<div class="col-sm-1">
						<input type="hidden" name="STR5_CODE" class="form-control 5_street_code no_label_fix" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_CDE'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text"  class="form-control no_label_fix 5_street_name" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 5_street_type" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['STREET_TYP'] : ''); ?>" />
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control no_label_fix 5_community" readonly value="<?php echo $value = (!empty($streets[2]) ? $streets[2]['COMMUNITY'] : ''); ?>" />
					</div>
				</div>
			</div>

			<legend>Location</legend>

			<div class="form-group">
				<div class="row">
					<div class="col-sm-4">
						<label for="SURVEY_DATE">Survey Date</label>
						<input type="text" name="SURVEY_DATE" class="form-control date_picker" title="Survey Date" readonly value="<?php echo set_value('SURVEY_DATE') ?>"  />
						<!-- Can I only have one date box? -->
					</div>
					<div class="col-sm-2">
						<label for="STR_REG">Region</label>
						<select name="STR_REG" class="form-control" title="Region" maxlength="3" >
							<option value="">select..</option>
							<option value="EST">Eastern</option>
							<option value="WST">Western</option>
							<option value="CNT">Central</option>
						</select>
					</div>
					<div class="col-sm-3">
						<label for="X_COORD">GPS X</label>
						<input type="text" name="X_COORD" class="form-control X_COORD" title="GPS X" maxlength="10" value="<?php echo set_value('X_COORD') ?>" />
					</div>
					<div class="col-sm-3">
						<label for="Y_COORD">GPS Y</label>
						<input type="text" name="Y_COORD" class="form-control Y_COORD" title="GPS Y" maxlength="10" value="<?php echo set_value('Y_COORD') ?>" />
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-3">
						<label for="PEAK_HV">Peak Hour Volume</label>
						<input type="text" name="PEAK_HV" class="form-control" title="Peak Hour Volume" maxlength="5" value="<?php echo set_value('PEAK_HV') ?>" />
					</div>

					<div class="col-sm-2">
						<label for="SURVEYOR">Surveyor</label>
						
							<input type="text" name="SURVEYOR" class="form-control pull-right surveyor_input" title="Surveyor" maxlength="3" />
					
					</div>

					<div class="col-sm-6">
						<label for="COMMENT_BOX">Comments</label>
						<textarea name="COMMENT_BOX" class="form-control" title="Comments" maxlength="200" ><?php echo set_value('COMMENT_BOX') ?></textarea>
					</div>
				</div>
			</div>


			<div class="form-group">
				<div class="row">
					<div class="col-sm-12">
						<input type="submit" class="btn btn-primary btn-md pull-right" value="Add Survey" />
						<a href="<?php echo base_url(); ?>traffic/MTMTC" class="btn btn-default btn-md">Back to MTMTC</a>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<h4 class="modal-title">Street Look Up</h4>
					</div>
					<div class="modal-body">
						<div class="container">
							<div class="row">
								<div class="col-sm-5">
									<div class="modal_search">
										<label for="search_box">Street Name</label>
										<input type="text" name="search_box" id="" class="form-control search_box" title="Street Name" autofocus maxlength="" />
										<p class="text-muted">(Select the name of the record you would like to use.)</p>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<table class="table table-bordered">
							<thead>
								<th>Street</th>
							</thead>
							<tbody class="modal_tbody">
								<tr>
									
								</tr>
							</tbody>	
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->