<div class="row">
	<div class="col-md-6">
		<h2>HRIS Course dates</h2>
		<p class="lead">For course number <?= $course_no ?></p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>
<div class="form-group">
	<div class="row">
		<div class="col-sm-12">
			<a href="<?= base_url(); ?>HRIS_ETT/add_course_date/<?= $course_no ?>" class="btn btn-success pull-right">Add Course Date</a>
		</div>
	</div>
</div>
<div class="row">
	<a href=""></a>
	<div class="col-sm-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>Schedule</th>
				<th>Start date</th>
				<th>End date</th>
				<th>Cost</th>
				<th>days</th>
				<th>hours</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
				<?php if (!empty($course_dates)): ?>
					<?php foreach ($course_dates as $course): ?>
						<tr>
							<td><a href="<?php echo base_url(); ?>HRIS_ETT/update_course_date/<?= $course_no.'/'.$course['COURSE_SCHED'] ?>"><?php echo $course['COURSE_SCHED'] ?></a></td>
							<td><?php echo $course['COURSE_START'] ?></td>
							<td><?php echo $course['COURSE_END'] ?></td>
							<td><?php echo $course['COST_COURSE'] ?></td>
							<td><?php echo $course['DURATION_DAYS'] ?></td>
							<td><?php echo $course['DURATION_HRS'] ?></td>
							<td>
								<form action="<?php echo base_url(); ?>HRIS_ETT/delete_course_date" method="POST" >
									<button type="submit" class="btn btn-sm btn-danger delete_date">Delete</button>
									<input type="hidden" name="COURSE_SCHED" value="<?= $course['COURSE_SCHED'] ?>" />
									<input type="hidden" name="COURSE_NO" value="<?= $course_no ?>" />
								</form>
							</td>
						</tr>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>
	</div>
    <div class="form-group">
        <div class="col-sm-12">
            <a href="<?php echo base_url(); ?>HRIS_ETT" class="btn btn-md btn-default return">Back to Courses</a>
        </div>
    </div>
</div>