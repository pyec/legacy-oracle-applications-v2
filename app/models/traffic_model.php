<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Traffic Signal System Model

Description : This is the data layer for the Traffic Signal system.

  Functions : get_ATR_studies(  ),
			get_ATR_study_by_id( $id = null ),
  			insert_ATR_study( $data = mull ),
  			update_ATR_study( $data = null ),
  			remove_ATR_study( $id = null ),
  			get_LTI_studies(  ),
			get_LTI_study_by_id( $id = null ),
  			insert_LTI_study( $data = mull ),
  			update_LTI_study( $data = null ),
  			remove_LTI_study( $id = null ),
  			get_MTMTC_studies(  ),
			get_MTMTC_study_by_id( $id = null ),
  			insert_MTMTC_study( $data = mull ),
  			update_MTMTC_study( $data = null ),
  			remove_MTMTC_study( $id = null ),
  			get_PS_studies(  ),
			get_PS_study_by_id( $id = null ),
  			insert_PS_study( $data = mull ),
  			update_PS_study( $data = null ),
  			remove_PS_study( $id = null ),
  			get_RSS_studies(  ),
			get_RSS_study_by_id( $id = null ),
  			insert_RSS_study( $data = mull ),
  			update_RSS_study( $data = null ),
  			remove_RSS_study( $id = null ),
  			get_TSW_studies(  ),
			get_TSW_study_by_id( $id = null ),
  			insert_TSW_study( $data = mull ),
  			update_TSW_study( $data = null ),
  			remove_TSW_study( $id = null ),
  			get_TSR_studies(  ),
			get_TSR_study_by_id( $id = null ),
  			insert_TSR_study( $data = mull ),
  			update_TSR_study( $data = null ),
  			remove_TSR_study( $id = null ),
  			get_TR_studies(  ),
			get_TR_study_by_id( $id = null ),
  			insert_TR_study( $data = mull ),
  			update_TR_study( $data = null ),
  			remove_TR_study( $id = null ),
=========================================================================================================================*/
class Traffic_model extends CI_Model 
{
	//Properies
	public $table_ATR = null;
	public $id_ATR = null;
	public $table_ATR_files = null;

	public $table_LTI = null;
	public $id_LTI = null;
	public $table_LTI_files = null;

	public $table_MTMTC = null;
	public $id_MTMTC = null;
	public $table_MTMTC_files = null;

	public $table_PS = null;
	public $id_PS = null;
	public $table_PS_files = null;

	public $table_RSS = null;
	public $id_RSS = null;
	public $table_RSS_files = null;

	public $table_TSW = null;
	public $id_TSW = null;
	public $table_TSW_files = null;

	public $table_TSR = null;
	public $id_TSR = null;
	public $table_TSR_files = null;

	public $table_TR = null;
	public $id_TR = null;
	public $table_TR_files = null;

	public $oracle_db = null; // The reference to the database

	//Constructor
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies

		$this->table_ATR = 'ATRS';
		$this->id_ATR = 'REC_CODE';
		$this->table_ATR_files = "ATRS_FILES";

		$this->table_LTI = 'LTIS';
		$this->id_LTI = 'REC_CODE';
		$this->table_LTI_files = "LTIS_FILES";

		$this->table_MTMTC = 'MTMT';
		$this->id_MTMTC = 'REC_CODE';
		$this->table_MTMTC_files = "MTMT_FILES";

		$this->table_PS = 'PEDS';
		$this->id_PS = 'REC_CODE';
		$this->table_PS_files = "PEDS_FILES";

		$this->table_RSS = 'RSSS';
		$this->id_RSS = 'REC_CODE';
		$this->table_RSS_files = "RSSS_FILES";

		$this->table_TSW = 'TSWS';
		$this->id_TSW = 'REC_CODE';
		$this->table_TSW_files = "TSWS_FILES";

		$this->table_TSR = 'SPREQ';
		$this->id_TSR = 'REQ_NUM';
		$this->table_TSR_files = "SPREQ_FILES";

		$this->table_TR = 'TREG';
		$this->id_TR = 'REG_NUM';
		$this->table_TR_files = "TREG_FILES";

		$this->oracle_db = $this->load->database('oracle_traffic', TRUE);

        $this->oracle_db->query("alter session set nls_date_format = 'DD-MON-YYYY'");
	}

	public function get_surveyor_list($table_name)
	{
		$this->oracle_db->distinct();
		$this->oracle_db->select('SURVEYOR');
		$query = $this->oracle_db->get($table_name);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	
/*-----------------------------------------------------------------------------------
	24 Hour Automated Traffic Recorder Study functions
------------------------------------------------------------------------------------*/
	public function get_ATR_studies()
	{
		$query = $this->oracle_db->get($this->table_ATR);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_ATR_study_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_ATR, $id);
		$query = $this->oracle_db->get($this->table_ATR);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}
	public function search_ATR_like_id($id = null)
	{
		$this->oracle_db->like($this->id_ATR, $id);
		$query = $this->oracle_db->get($this->table_ATR);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function insert_ATR_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_ATR, $data);

			return $results;
		}
	}
	public function update_ATR_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
		
			$this->oracle_db->where($this->id_ATR, $data[$this->id_ATR]);

			$results = $this->oracle_db->update($this->table_ATR, $data);

			return $results;
		}
	}
	public function remove_ATR_study( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_ATR, $id);
			$results = $this->oracle_db->delete($this->table_ATR);
			return $results;
		}
	}

	public function get_ATR_attachment( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('REC_CODE', $id);
			$results = $this->oracle_db->get('ATRS_FILES_BLOB');

			if ($results->num_rows() > 0)
			{
				return $results->row_array();
			}
			else
			{
				return false;
			}
		}
	}

/*-----------------------------------------------------------------------------------
	Left Turn Investigation Functions 
------------------------------------------------------------------------------------*/
	public function get_LTI_studies()
	{
		$query = $this->oracle_db->get($this->table_LTI);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_LTI_study_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_LTI, $id);
		$query = $this->oracle_db->get($this->table_LTI);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}
	public function search_LTI_like_id($id = null)
	{
		$this->oracle_db->like($this->id_LTI, $id);
		$query = $this->oracle_db->get($this->table_LTI);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function insert_LTI_study( $data = mull )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_LTI, $data);
			return $results;
		}
	}
	public function update_LTI_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_LTI, $data[$this->id_LTI]);
			$results = $this->oracle_db->update($this->table_LTI, $data);

			return $results;
		}
	}
	public function remove_LTI_study( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_LTI, $id);
			$results = $this->oracle_db->delete($this->table_LTI);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Manual Turning Movement Traffic Count functions 
------------------------------------------------------------------------------------*/
	public function get_MTMTC_studies(  )
	{
		$query = $this->oracle_db->get($this->table_MTMTC);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_MTMTC_by_streets( $street_1 = null, $street_2 = null )
	{
		$this->oracle_db->where('STR_CODE', $street_1);
		$this->oracle_db->where('STR2_CODE', $street_2);

		$query = $this->oracle_db->get($this->table_MTMTC);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_MTMTC_study_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_MTMTC, $id);
		$query = $this->oracle_db->get($this->table_MTMTC);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}
	public function search_MTMTC_study_like_id( $id = null )
	{
		$this->oracle_db->like($this->id_MTMTC, $id);
		$query = $this->oracle_db->get($this->table_MTMTC);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function insert_MTMTC_study( $data = mull )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_MTMTC, $data);

			return $results;
		}
	}
	public function update_MTMTC_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_MTMTC, $data[$this->id_MTMTC]);
			unset($data[$this->id_MTMTC]);
			$results = $this->oracle_db->update($this->table_MTMTC, $data);
			return $results;
		}
	}
	public function remove_MTMTC_study( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_MTMTC, $id);
			$results = $this->oracle_db->delete($this->table_MTMTC);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Pedestrian Study functions
------------------------------------------------------------------------------------*/
	public function get_PS_studies(  )
	{
		$query = $this->oracle_db->get($this->table_PS);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_PS_by_streets( $street_1 = null, $street_2 = null )
	{
		$this->oracle_db->where('STR_CODE', $street_1);
		$this->oracle_db->where('STR2_CODE', $street_2);

		$query = $this->oracle_db->get($this->table_PS);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_PS_study_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_PS, $id);
		$query = $this->oracle_db->get($this->table_PS);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}
	public function search_PS_study_like_id( $id = null )
	{
		$this->oracle_db->like($this->id_PS, $id);
		$query = $this->oracle_db->get($this->table_PS);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function insert_PS_study( $data = mull )
	{	
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_PS, $data);
			return $results;
		}
	}
	public function update_PS_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_PS, $data[$this->id_PS]);
			$results = $this->oracle_db->update($this->table_PS, $data);
			return $results;
		}
	}
	public function remove_PS_study( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_PS, $id);
			$results = $this->oracle_db->delete($this->table_PS);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Radar Spot Speed Study functions
------------------------------------------------------------------------------------*/
	public function get_RSS_studies(  )
	{
		$query = $this->oracle_db->get($this->table_RSS);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_RSS_study_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_RSS, $id);
		$query = $this->oracle_db->get($this->table_RSS);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}
	public function search_RSS_study_like_id( $id = null )
	{
		$this->oracle_db->like($this->id_RSS, $id);
		$query = $this->oracle_db->get($this->table_RSS);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function insert_RSS_study( $data = mull )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_RSS, $data);
			return $results;
		}
	}
	public function update_RSS_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_RSS, $data[$this->id_RSS]);
			$results = $this->oracle_db->update($this->table_RSS, $data);
			return $results;
		}
	}
	public function remove_RSS_study( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_RSS, $id);
			$results = $this->oracle_db->delete($this->table_RSS);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Traffic Signal Warrant functions
------------------------------------------------------------------------------------*/
	public function get_TSW_studies(  )
	{
		$query = $this->oracle_db->get($this->table_TSW);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_TSW_study_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_TSW, $id);
		$query = $this->oracle_db->get($this->table_TSW);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}
	public function search_TSW_study_like_id( $id = null )
	{
		$this->oracle_db->like($this->id_TSW, $id);
		$query = $this->oracle_db->get($this->table_TSW);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function insert_TSW_study( $data = mull )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_TSW, $data);
			return $results;
		}
	}
	public function update_TSW_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_TSW, $data[$this->id_TSW]);
			$results = $this->oracle_db->update($this->table_TSW, $data);
			return $results;
		}
	}
	public function remove_TSW_study( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_TSW, $id);
			$results = $this->oracle_db->delete($this->table_TSW);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Traffic Survey Request functions
------------------------------------------------------------------------------------*/
	public function get_TSR_studies(  )
	{
		$query = $this->oracle_db->get($this->table_TSR);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_TSR_study_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_TSR, $id);
		$query = $this->oracle_db->get($this->table_TSR);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}
	public function search_TSR_study_like_id( $id = null )
	{
		$this->oracle_db->like($this->id_TSR, $id);
		$query = $this->oracle_db->get($this->table_TSR);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function insert_TSR_study( $data = mull )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_TSR, $data);
			return $results;
		}
	}
	public function update_TSR_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_TSR, $data[$this->id_TSR]);
			$results = $this->oracle_db->update($this->table_TSR, $data);
			return $results;
		}
	}
	public function remove_TSR_study( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_TSR, $id);
			$results = $this->oracle_db->delete($this->table_TSR);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Traffic Regulation functions
------------------------------------------------------------------------------------*/
	public function get_TR_studies(  )
	{
		$query = $this->oracle_db->get($this->table_TR);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function get_TR_study_by_id( $id = null )
	{
		$this->oracle_db->where($this->id_TR, $id);
		$query = $this->oracle_db->get($this->table_TR);

		if ($query->num_rows() > 0)
		{ 
			return $query->row_array();
		}
		else 
		{
			return false;
		}
	}
	public function search_TR_study_like_id( $id = null )
	{
		$this->oracle_db->like($this->id_TR, $id);
		$query = $this->oracle_db->get($this->table_TR);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}
	public function insert_TR_study( $data = mull )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_TR, $data);
			return $results;
		}
	}
	public function update_TR_study( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_TR, $data[$this->id_TR]);
			$results = $this->oracle_db->update($this->table_TR, $data);
			return $results;
		}
	}
	public function remove_TR_study( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_TR, $id);
			$results = $this->oracle_db->delete($this->table_TR);
			return $results;
		}
	}
}

/* End of file traffic_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/traffic_model.php */