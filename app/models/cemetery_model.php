<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Cemetery Model

Description : This is the data layer for the Cemetery system.

  Functions : get_owners(), 
  			get_owner_by_id($owner_id = null), 
  			get_owner_by_first_name($first_name),
  			get_owner_by_sur_name($sur_name), 
  			insert_owner($owner_data), 
  			update_owner($owner_data),
			remove_owner($owner_id), 
			get_interred(), 
			get_interred_by_id($interred_id = null),
			get_interred_by_first_name($first_name),
			insert_interred($interred_data),
			update_interred($interred_data), 
			removed_interred($interred_id), 
			get_cemeteries(),
            get_cemeteries_and_numbers(),
            get_interred_per_cemetery(),
			get_cemetery_by_name($cemetery_name),
			insert_cemetery($cemetery_id),
			update_cemetery($cemetery_data),
			remove_cemetery($cemetery_id),
            get_cemeteries,
            get_like_owners()



=========================================================================================================================*/
class Cemetery_model extends CI_Model 
{
	//Properties
	public $table_owners = null;
	public $id_owners = null;
	public $first_name_owners = null;
	public $middle_name_owners = null;
	public $last_name_owners = null;

	public $table_interred = null;
	public $id_interred = null;
	public $first_name_interred = null;

	public $table_cemetery = null;
	public $id_cemetery = null;
	public $name_cemetery = null;

	public $oracle_db = null; // The reference to the database

	//Constructor
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies

		$this->table_owners = 'CEM_OWNER';
		$this->id_owners = 'CEM_REFNBR';
		$this->first_name_owners = 'CEM_FNAME';
		$this->middle_name_owners = 'CEM_MNAME';
		$this->last_name_owners = 'CEM_LNAME';

		$this->table_interred = 'CEM_INTERRED';
		$this->id_interred = 'INT_REFNBR';
		$this->first_name_interred = 'INT_FNAME';
		$this->middle_name_interred = 'INT_MNAME';
		$this->last_name_interred = 'INT_LNAME';

		$this->table_cemetery = 'CEM_CEMETERY';
		$this->id_cemetery = 'CEM_NBR';
		$this->name_cemetery = 'CEM_NAME';

		$this->oracle_db = $this->load->database('oracle_cemetery', TRUE);

        $this->oracle_db->query("alter session set nls_date_format = 'DD-MON-YYYY'");
	}

	public function get_next_val( $table_name, $id )
	{
		// Query for the max value in the id field.
		$query = $this->oracle_db->query("SELECT MAX(".$id.") FROM ".$table_name);

		// In one expression return the result sets value, cast it to an integer 
		// and store it in the variable $id.
		$id = (int)$query->result_array()[0]["MAX(".$id.")"];

		// Increment by 1 and return the id so we can add a new record to the table.
		return ++$id;
	}


/*-----------------------------------------------------------------------------------
	Owner retrieval functions
------------------------------------------------------------------------------------*/
	public function get_owners( )
	{
		$query = $this->oracle_db->get($this->table_owners);

		$this->oracle_db->order_by('CEM_LNAME', 'asc');
		$this->oracle_db->order_by('CEM_FNAME', 'asc');
		$this->oracle_db->order_by('CEM_NBRGR', 'asc');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function search_owners($data)
	{
		if ($data != null)
		{
			if (!empty($data['cemetery'])) {$this->oracle_db->where('CEM_NBR', $data['cemetery']);}
			if (!empty($data['first_name'])) {$this->oracle_db->like('CEM_FNAME', strtoupper($data['first_name']), 'after');}
			if (!empty($data['last_name'])) {$this->oracle_db->like('CEM_LNAME', strtoupper($data['last_name']), 'after');}
			if (!empty($data['middle_name'])) {$this->oracle_db->like('CEM_MNAME', strtoupper($data['middle_name']), 'after');}
            if (!empty($data['location'])) {$this->oracle_db->like('CEM_SCTN', strtoupper($data['location']), 'after');}

			$this->oracle_db->order_by('CEM_LNAME', 'asc');
			$this->oracle_db->order_by('CEM_FNAME', 'asc');
			$this->oracle_db->order_by('CEM_NBRGR', 'asc');

 			$query = $this->oracle_db->get($this->table_owners);

 			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}

		}
	}

	public function get_owner_by_id( $id = null, $cem_id = null ) 
	{
		if ($id != null)
		{
			$this->oracle_db->where($this->id_owners, $id);
			$this->oracle_db->where('CEM_NBR', $cem_id);
			$query = $this->oracle_db->get($this->table_owners);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function get_owner_like_id( $id = null ) 
	{
		if ($id != null)
		{
			$this->oracle_db->like($this->id_owners, $id);
			$query = $this->oracle_db->get($this->table_owners);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function get_owner_by_name( $name = null )
	{
		if ($name != null)
		{
			if (!empty($name['first_name'])){$this->oracle_db->like($this->first_name_owners, strtoupper($name['first_name']));}
			if (!empty($name['middle_name'])){$this->oracle_db->like($this->middle_name_owners, strtoupper($name['middle_name']));}
			if (!empty($name['last_name'])){$this->oracle_db->like($this->last_name_owners, strtoupper($name['last_name']));}
			
			$query = $this->oracle_db->get($this->table_owners);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function get_owner_for_update( $id, $cem_id )
	{
		if ($id != null)
		{
			$this->oracle_db->where($this->id_owners, $id);
			$this->oracle_db->where('CEM_NBR', $cem_id);
			$query = $this->oracle_db->get($this->table_owners);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function get_interred_by_owner_id( $id = null, $cem_id = null )
	{
		if ($id != null)
		{
			$this->oracle_db->where($this->id_owners, $id);
			$this->oracle_db->where('CEM_NBR', $cem_id);
			$query = $this->oracle_db->get($this->table_interred);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

/*-----------------------------------------------------------------------------------
	Owner insertion function
------------------------------------------------------------------------------------*/
	public function insert_owner( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!empty($data['CEM_FNAME'])){$data['CEM_FNAME'] = strtoupper($data['CEM_FNAME']);}
			if (!empty($data['CEM_MNAME'])){$data['CEM_MNAME'] = strtoupper($data['CEM_MNAME']);}
			if (!empty($data['CEM_LNAME'])){$data['CEM_LNAME'] = strtoupper($data['CEM_LNAME']);}
			
			$results = $this->oracle_db->insert($this->table_owners, $data);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Owner update function
------------------------------------------------------------------------------------*/
	public function update_owner( $data = null, $id = null, $cem_id = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_owners, $id);
			$this->oracle_db->where('CEM_NBR', $cem_id);

			// unset($data['CEM_REFNBR']);

			$results = $this->oracle_db->update($this->table_owners, $data);


			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Owner Removal function
------------------------------------------------------------------------------------*/
	public function remove_owner( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('CEM_REFNBR', $id);
			$results = $this->oracle_db->delete($this->table_owners);
			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Interred retrieval functions
------------------------------------------------------------------------------------*/
	public function get_interred() 
	{
		$query = $this->oracle_db->get('CEM_INTERRED');

		$this->oracle_db->order_by('INT_LNAME', 'asc');
		$this->oracle_db->order_by('INT_FNAME', 'asc');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function search_interred($data)
	{
		if ($data != null)
		{
		    if (!empty($data['cemetery']) && (!empty($data['first_name']))
                || (!empty($data['last_name'])) || (!empty($data['middle_name'])) || (!empty($data['location']))) {

                if (!empty($data['cemetery'])) {$this->oracle_db->where('CEM_NBR', $data['cemetery']);}
                if (!empty($data['first_name'])) {$this->oracle_db->like('INT_FNAME', strtoupper($data['first_name']), 'after');}
                if (!empty($data['last_name'])) {$this->oracle_db->like('INT_LNAME', strtoupper($data['last_name']), 'after');}
                if (!empty($data['middle_name'])) {$this->oracle_db->like('INT_MNAME', strtoupper($data['middle_name']), 'after');}
                if (!empty($data['location'])) {$this->oracle_db->like('INT_LOC', strtoupper($data['location']), 'after');}

                $this->oracle_db->order_by('INT_LNAME', 'asc');
                $this->oracle_db->order_by('INT_FNAME', 'asc');

                $query = $this->oracle_db->get($this->table_interred);

                if ($query->num_rows() > 0)
                {
                    return $query->result_array();
                }
                else
                {
                    return false;
                }
			}
		}
	}

	public function get_interred_by_id( $id = null ) 
	{
		if ($id != null)
		{
			$this->oracle_db->where($this->id_interred, $id);
			$query = $this->oracle_db->get($this->table_interred);

			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function get_interred_for_update($cemetery_number = null, $cemetery_reference = null, $interred_reference = null)
	{
		if ($cemetery_number != null && $cemetery_reference != null && $interred_reference != null)
		{
			$this->oracle_db->where('CEM_NBR', $cemetery_number);
			$this->oracle_db->where('CEM_REFNBR', $cemetery_reference);
			$this->oracle_db->where('INT_REFNBR', $interred_reference);
			$query = $this->oracle_db->get('CEM_INTERRED');
			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

	public function get_interred_by_name( $name = null )
	{
		if ($name != null)
		{
			if (!empty($name['first_name'])){$this->oracle_db->like($this->first_name_interred, strtoupper($name['first_name']));}
			if (!empty($name['middle_name'])){$this->oracle_db->like($this->middle_name_interred, strtoupper($name['middle_name']));}
			if (!empty($name['last_name'])){$this->oracle_db->like($this->last_name_interred, strtoupper($name['last_name']));}
			
			$query = $this->oracle_db->get($this->table_interred);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}

/*-----------------------------------------------------------------------------------
	Interred insertion function
------------------------------------------------------------------------------------*/
	public function insert_interred( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			if (!empty($data['INT_FNAME'])){$data['INT_FNAME'] = strtoupper($data['INT_FNAME']);}
			if (!empty($data['INT_MNAME'])){$data['INT_MNAME'] = strtoupper($data['INT_MNAME']);}
			if (!empty($data['INT_LNAME'])){$data['INT_LNAME'] = strtoupper($data['INT_LNAME']);}

			$results = $this->oracle_db->insert($this->table_interred, $data);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Interred update function
------------------------------------------------------------------------------------*/
	public function update_interred( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{	

			$this->oracle_db->where('INT_REFNBR', $data['INT_REFNBR']);
			$this->oracle_db->where('CEM_NBR', $data['CEM_NBR']);
			$this->oracle_db->where('CEM_REFNBR', $data['CEM_REFNBR']);

			unset($data['CEM_NBR']);
			unset($data['CEM_REFNBR']);
			unset($data['INT_REFNBR']);

			$results = $this->oracle_db->update($this->table_interred, $data);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Interred Removal function
------------------------------------------------------------------------------------*/
	public function remove_interred( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('INT_REFNBR', $data['INT_REFNBR']);
			$this->oracle_db->where('CEM_NBR', $data['CEM_NBR']);
			$this->oracle_db->where('CEM_REFNBR', $data['CEM_REFNBR']);
			$results = $this->oracle_db->delete($this->table_interred);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Cemetery retrieval functions
------------------------------------------------------------------------------------*/
	public function get_cemeteries( )
	{
		$query = $this->oracle_db->get($this->table_cemetery);

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

    /**
     * Returns cemeteries and the number of interred per cemetery
     * @param none
     * @return array
     */
    public function get_cemeteries_and_numbers( )
    {
        $query = $this->oracle_db->query('SELECT COUNT(cem_int.cem_nbr) AS NUM_INTERRED, cem.cem_name, cem_int.cem_nbr 
                                          FROM cem_interred cem_int 
                                          JOIN cem_cemetery cem ON cem.cem_nbr = cem_int.cem_nbr
                                          JOIN cem_owner cem_own on cem_int.cem_refnbr = cem_own.cem_refnbr
                                          AND cem_int.cem_nbr = cem_own.cem_nbr 
                                          GROUP BY cem.cem_name, cem_int.cem_nbr 
                                          ORDER BY cem.cem_name');

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    /**
     * Returns information on interred per cemetery
     * @param $cemeteryId - cemetery id used in search
     * @return array
     */
    public function get_interred_per_cemetery($cemeteryId )
    {
        $query = $this->oracle_db->query("SELECT cem_int.int_loc AS SECTION, cem.cem_name, cem_int.int_fname, cem_int.int_mname, cem_int.int_lname,
                                          cem_int.int_dte, cem_own.cem_fname, cem_own.cem_mname, cem_own.cem_lname,
                                          cem_own.cem_refnbr, cem_own.cem_sctn
                                          FROM cem_interred cem_int
                                          JOIN cem_owner cem_own on cem_int.cem_refnbr = cem_own.cem_refnbr
                                          AND cem_int.cem_nbr = cem_own.cem_nbr
                                          JOIN cem_cemetery cem on cem_int.cem_nbr = cem.cem_nbr
                                          WHERE cem_int.cem_nbr = " . $cemeteryId . "
                                          ORDER BY cem_own.cem_lname, cem_own.cem_fname, cem_own.cem_mname,
                                          cem_int.int_lname, cem_int.int_fname, cem_int.int_fname");

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

	public function get_cemetery_by_id( $id )
	{
		$this->oracle_db->where('CEM_NBR', $id);
		$query = $this->oracle_db->get('CEM_CEMETERY');

		if ($query->num_rows() > 0)
		{
			return $query->result_array();
		}
	}

	public function get_cemetery_by_name( $cemetery_name = null )
	{
		if ($cemetery_name == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->name_cemetery, $cemetery_name);
			$query = $this->oracle_db->get($this->table_cemetery);

			if ($query->num_rows() > 0)
			{
				return $query->result_array();
			}
			else
			{
				return false;
			}
		}
	}

/*-----------------------------------------------------------------------------------
	Cemetery insertion function
------------------------------------------------------------------------------------*/
	public function insert_cemetery( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$results = $this->oracle_db->insert($this->table_cemetery, $data);

			return $results;
		}
	}

/*-----------------------------------------------------------------------------------
	Cemetery update function
------------------------------------------------------------------------------------*/
	public function update_cemetery( $data = null )
	{
		if ($data = null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_cemetery, $data[$this->id_cemetery]);
			$this->oracle_db->update($this->table_cemetery, $data);
		}
	}

/*-----------------------------------------------------------------------------------
	Cemetery removal function
------------------------------------------------------------------------------------*/
	public function remove_cemetery( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->id_cemetery, $id);
			$results = $this->oracle_db->delete($this->table_cemetery);
			return $results;
		}
	}

    /**
     * Searches database for plot owners based on last name.
     * @param $firstName
     * @param $lastName
     * @return bool
     */
    public function get_like_owners($firstName, $lastName)
    {
        if ($firstName == null && $lastName == null )
        {
            return false;
        }
        else
        {
            $this->oracle_db->like('CEM_FNAME', strtoupper($firstName));
            $this->oracle_db->like('CEM_LNAME', strtoupper($lastName));
            $this->oracle_db->limit(20);
            $this->oracle_db->order_by('CEM_LNAME', 'asc');
            $this->oracle_db->order_by('CEM_FNAME', 'asc');
            $this->oracle_db->order_by('CEM_NBRGR', 'asc');
            $query = $this->oracle_db->get($this->table_owners);


            return $query->result_array();
        }
    }
}

/* End of file cemetery_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/cemetery_model.php */