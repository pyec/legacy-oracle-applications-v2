<div class="row">
	<div class="col-md-6">
		<h2>District Councilors</h2>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
		<p>Select an Counciller to view more information about them.</p>
	</div>

	<div class="col-md-12">
		<a href="<?php echo base_url(); ?>MCT/add_councillor" class="btn btn-success pull-right">Add Councilor</a>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>District #</th>
				<th>District Name</th>
				<th>Councillor Name</th>
				<th>Delete</th>
			</thead>
			<tbody class="tbody">
				<?php foreach ($district_councillors as $councillor): ?>
					<tr>
						<td><?php echo $councillor['MCT_DISTRICT'] ?></td>
						<td><a href="<?php echo base_url(); ?>MCT/update_councillor/<?= $councillor['MCT_DISTRICT'] ?>"><?php echo ucwords(strtolower($councillor['MCT_DIST_DESC'])) ?></a></td>
						<td><?php echo $councillor['MCT_COUNCILLOR'] ?></td>
						<td>
							<form action="<?php echo base_url(); ?>MCT/delete_councillor" method="POST">
								<button type="submit" class="delete_councillor btn btn-danger btn-sm">Delete</button>
								<input type="hidden" name="MCT_DISTRICT" value="<?php echo $councillor['MCT_DISTRICT'] ?>" />
							</form>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>