<div class="row">
	<div class="col-md-6">
		<h2>Resolved Correspondence</h2>
		<p>Select a record to update/view more information.</p>
	</div>
</div>

<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php endif ?>

<!-- <div class="row"> -->
	<form action="<?php echo base_url(); ?>MCT/resolved_call_search" method="POST">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="call_number">ID</label>
					<input type="text" name="call_number" class="form-control" title="ID" value="<?php echo set_value('call_number') ?>" tabindex="1" placeholder="ID" />
				</div>
				<div class="col-sm-3">
					<label for="first_name">First Name</label>
					<input type="text" name="first_name" class="form-control" title="First Name" value="<?php echo set_value('first_name') ?>" tabindex="2" placeholder="First Name" />
				</div>
				<div class="col-sm-3">
					<label for="last_name">Last Name</label>
					<input type="text" name="last_name" class="form-control" title="Last Name" value="<?php echo set_value('last_name') ?>" tabindex="3" placeholder="Last Name" />
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-sm-12"> 
					<button type="submit" class="btn btn-primary btn-md pull-right phonebook_search_submit" tabindex="8"><i class="fa fa-search">&nbsp;</i>Search</button>
					<button type="reset" class="btn btn-default btn-md pull-right" tabindex="9" onclick="mct_call_form_reset()">Clear</button>	
				</div>	
			</div>
		</div>
	</form>
	<legend class="phonebook_legend"></legend>
	
<div class="row">
	<div class="col-md-12">
		<?php if (!empty($resolved_calls)): ?>
			<?php if (count($resolved_calls) == 500): ?>
				<h2 class="count"><?php echo count($resolved_calls) ?> resolved or more</h2>
			<?php else: ?>
				<h2 class="count"><?php echo count($resolved_calls) ?> resolved</h2>
			<?php endif ?>
		<?php endif ?>
	</div>
	<div class="col-sm-12" style="margin-top: 30px;">
		<?php if (!empty($resolved_calls) && count($resolved_calls) == 500): ?>
			<P class="">Refine your search for more specific results.</P>			
		<?php endif ?>
	</div>
</div>
<?php if (!empty($resolved_calls)): ?>
<div class="row no_label_fix">
	<div class="col-md-12">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<th>ID</th>
				<th>Source</th>
				<th>Type</th>
				<th>First Name</th>
				<th>Last Name</th>
				<th>Resolved</th>
				<th>Action</th>
			</thead>
			<tbody class="tbody">
					<?php foreach ($resolved_calls as $call): ?>
						<tr>
							<td><a href="<?php echo base_url(); ?>MCT/update_resolved/<?= $call['MCT_NBR'] ?>"><?= $call['MCT_NBR'] ?></a></td>
							<td><?php echo $call['MCT_SOURCE'] ?></td>
							<td><?php echo $call['MCT_TYPE_DESC'] ?></td>
							<td><?php echo $call['MCT_FST_NME'] ?></td>
							<td><?php echo $call['MCT_LST_NME'] ?></td>
							<td><?php if($call['MCT_RESOLVE_FLG'] == 'Y') {echo "Yes";} else {echo "No";} ?></td>
							<td>
								<form action="<?php echo base_url(); ?>MCT/delete_resolved" method="POST">
									<button type="submit" class="btn btn-sm btn-danger delete_call">Delete</button>
									<input type="hidden" name="MCT_NBR" value="<?= $call['MCT_NBR'] ?>" />
								</form>
							</td>
						</tr>
					<?php endforeach ?>
			</tbody>
		</table>
	</div>
</div>
<?php endif ?>

