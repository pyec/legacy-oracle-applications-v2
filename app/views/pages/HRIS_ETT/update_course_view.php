<div class="row">
	<div class="col-md-12">
		<h2>Update Course Date</h2>
		<p class="lead">This form will allow you to update a scheduled course and the employees attending.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>
<?php endif ?>


<legend>Course</legend>
<form action="<?php echo base_url(); ?>HRIS_ETT/update_course_date/<?= $course[0]['COURSE_NO'] ?>/<?= $schedule[0]['COURSE_SCHED'] ?>" method="POST">
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="COURSE_CLASS">Course Category</label>
					<input type="text" name="COURSE_CLASS" class="form-control" title="Course Category" value="<?= $course[0]['COURSE_CLASS'] ?>" disabled />
					<input type="hidden" name="COURSE_CLASS"  value="<?= $course[0]['COURSE_CLASS'] ?>"  />
				</div>

				<div class="col-sm-2">
					<label for="COURSE_NO">Course Number</label>
					<input type="text" name="COURSE_NO" class="form-control" title="Course Number" value="<?= $course[0]['COURSE_NO'] ?>" disabled />
					<input type="hidden" name="COURSE_NO"  value="<?= $course[0]['COURSE_NO'] ?>"  />
				</div>

				<div class="col-sm-4">
					<label for="COURSE_TITLE">Course Name</label>
					<input type="text" name="COURSE_TITLE" class="form-control" title="Course Name" value="<?= $course[0]['COURSE_TITLE'] ?>" disabled />
				</div>	

				<div class="col-sm-2">
					<label for="COST_COURSE">Course Cost</label>
					<input type="text" name="COST_COURSE" class="form-control" title="Course Cost" value="<?= $schedule[0]['COST_COURSE'] ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="SCHOOL_LOCN">Location</label>
					<input type="text" name="SCHOOL_LOCN" class="form-control" title="Location" value="<?= $schedule[0]['SCHOOL_LOCN'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="DURATION_DAYS">Duration (days)</label>
					<input type="text" name="DURATION_DAYS" class="form-control" title="Duration in days" value="<?= $schedule[0]['DURATION_DAYS'] ?>" />
				</div>
				<div class="col-sm-2">
					<label for="DURATION_HRS">Duration (Hours)</label>
					<input type="text" name="DURATION_HRS" class="form-control" title="Duration in Hours" value="<?= $schedule[0]['DURATION_HRS'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="INSTRUCTOR">Instructor</label>
					<input type="text" name="INSTRUCTOR" class="form-control" title="Instructor" value="<?= $schedule[0]['INSTRUCTOR'] ?>" />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-2">
					<label for="COURSE_SCHED">Course Schedule</label>
					<input type="text" name="COURSE_SCHED" class="form-control" title="Course Schedule" value="<?= $schedule[0]['COURSE_SCHED'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="COURSE_START">Course Start Date</label>
					<input type="text" name="COURSE_START" class="form-control date_picker_new" title="Course Start Date" readonly value="<?= $schedule[0]['COURSE_START'] ?>" />
				</div>
				<div class="col-sm-4">
					<label for="COURSE_END">Course End Date</label>
					<input type="text" name="COURSE_END" class="form-control date_picker_new" title="Course End Date" readonly value="<?= $schedule[0]['COURSE_END'] ?>" />
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_course pull-right" value="Update Schedule" />
				</div>
			</div>
		</div>
</form>
		<legend>Employees</legend>
		<div class="">
			<?php if (!empty($employees)): ?>
				<table class="table table-bordered table-striped table-hover">
					<thead>
						<th>Emp #</th>
						<th>Name</th>
						<th>Business Unit</th>
                        <th>Cost Center</th>
						<th>Status</th>
<!--						<th>Date Complete</th>-->
						<th>Update</th>
						<th>Remove</th>
					</thead>
					<tbody class="tbody">
						<?php foreach ($employees as $emp): ?>
							<tr>
								<form action="<?php echo base_url(); ?>HRIS_ETT/update_course_employee/<?= $course[0]['COURSE_NO'] ?>/<?= $schedule[0]['COURSE_SCHED'] ?>" method="POST" >
									<td><?= $emp['EMP_NO'] ?></td>
									<td><?= ucwords(strtolower($emp['GIVEN_NAME1'].' '.$emp['SURNAME'])) ?></td>
									<td><?= ucwords(strtolower($emp['DIVISION_NAME'])) ?></td>
                                    <td><?= $emp['CC_CODE'] ?></td></td>
									<td>
										<select class="form-control" name="COURSE_STATUS">
											<option value="" >Select..</option>
											<option value="O" <?php if ($emp['COURSE_STATUS'] == 'O') echo ' selected="selected"';?>>Open</option>
											<option value="C" <?php if ($emp['COURSE_STATUS'] == 'C') echo ' selected="selected"';?>>Close/Completed</option>
											<option value="EX" <?php if ($emp['COURSE_STATUS'] == 'EX') echo ' selected="selected"';?>>Excused</option>
											<option value="NS" <?php if ($emp['COURSE_STATUS'] == 'NS') echo ' selected="selected"';?>>No Show</option>
											<option value="NC" <?php if ($emp['COURSE_STATUS'] == 'NC') echo ' selected="selected"';?>>Not Completed</option>
											<option value="CA" <?php if ($emp['COURSE_STATUS'] == 'CA') echo ' selected="selected"';?>>Cancelled</option>
											<option value="NR" <?php if ($emp['COURSE_STATUS'] == 'NR') echo ' selected="selected"';?>>Not Registered</option>
<!--											--><?php //if (!empty($emp['COURSE_STATUS'])): ?>
<!--												<option selected value="--><?//= $emp['COURSE_STATUS'] ?><!--">--><?//= $emp['COURSE_STATUS'] ?><!--</option>-->
<!--											--><?php //endif ?>
										</select>
									</td>
<!--									<td><input type="text" class="form-control date_picker_new" title="" value="--><?//= $schedule[0]['COURSE_END'] ?><!--" readonly name="DATE_COMPLETED" /></td>-->
									<input type="hidden" name="EMP_NO"  value="<?= $emp['EMP_NO'] ?>" />
                                    <input type="hidden" name="DATE_COMPLETED"  value="<?= $emp['DATE_COMPLETED'] ?>" />
									<td><button type="submit" class="btn btn-warning btn-sm" title="update"><i class="fa fa-plus-square"></i></button></td>
								</form>
								<td>
									<form action="<?php echo base_url(); ?>/HRIS_ETT/remove_course_employee" method="post">
										<button type="submit" class="btn btn-danger btn-sm delete_emp" title="Remove"><i class="fa fa-minus-square"></i></button>
										<input type="hidden" name="COURSE_NO" value="<?= $emp['COURSE_NO'] ?>"  />
										<input type="hidden" name="COURSE_SCHED" value="<?= $emp['COURSE_SCHED'] ?>"  />
										<input type="hidden" name="EMP_NO" value="<?= $emp['EMP_NO'] ?>"  />
									</form>
								</td>
							</tr>
						<?php endforeach ?>
					</tbody>	
				</table>
				
				<button type="button" class="btn btn-primary HRIS_modal_btn" data-toggle="modal" data-target="#HRIS_myModal" value="employee">Add Employee</button>
				<a href="<?php echo base_url(); ?>HRIS_ETT/update_to_complete/<?= $course[0]['COURSE_NO'] ?>/<?= $schedule[0]['COURSE_SCHED'] ?>" class="btn btn-primary pull-right update_to_complete" value="">Update All To Complete</a>
				
			<?php else : ?>
				<button type="button" class="btn btn-primary HRIS_modal_btn" data-toggle="modal" data-target="#HRIS_myModal" value="employee">Add Employee</button>
			<?php endif ?>
			
		</div>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<a href="<?php echo base_url(); ?>HRIS_ETT/update_course/<?= $course[0]['COURSE_NO'] ?>" class="btn btn-md btn-default return">Back to Course Schedules</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="HRIS_myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Employee Look Up</h4>
				<p class="text-muted">Start typing to search by ID, first or last name. you can only search by one criteria at a time.</p>
			</div>
			<div class="modal-body">
				<div class="container">
					<div class="row">
						<div class="modal_search">
							<div class="col-sm-2">
								<label for="search_box">Employee #</label>
								<input type="text" name="emp_number_search_box" id="" class="form-control emp_number_search_box" title="Employee Number" autofocus maxlength="" />
							</div>
							<div class="col-sm-2">
								<label for="first_name_search_box">First Name</label>
								<input type="text" name="first_name_search_box" id="first_name_search_box" class="form-control first_name_search_box" title="First Name" />
							</div>
							<div class="col-sm-2">
								<label for="last_name_search_box">Last Name</label>
								<input type="text" name="last_name_search_box" id="last_name_search_box" class="form-control last_name_search_box" title="Last Name" />
							</div>
							<div class="col-sm-1">
								<button type="reset" class="btn btn-sm btn-default no_label_fix HRIS_modal_clear">Clear</button>
							</div>
						</div>
					</div>
				</div>
				<hr>
				<table class="table table-bordered">
					<thead>
						<th>Employee #</th>
						<th>First Name</th>
						<th>Last Name</th>
					</thead>
					<tbody class="modal_tbody">
						<tr>
							
						</tr>
					</tbody>	
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->