<!-- Page title -->
<div class="row">
	<div class="col-md-12">
		<h2>Add Street File</h2>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<!-- Error reporting -->
		<?php if (validation_errors()): ?>
			<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<p><?php echo validation_errors(); ?></p>
			</div>
		<?php endif ?>
		<!-- Beginning of the input fields row 1 -->
		<form action="<?php echo base_url(); ?>street_file/add" method="POST" class="">
		<div class="form-group">
			<div class="row">
				<div class="col-sm-4">
					<label for="STNAME">Street Name</label>
					<input type="text" name="STNAME" class="form-control add_street_name" title="Street Name" value="<?php echo set_value('STNAME') ?>" />
				</div>
				<div class="col-sm-2">
					<label for="SUFFIX">Street Type</label>
					<select name="SUFFIX" class="form-control add_suffix" title="Street Type" value="<?php echo set_value('SUFFIX')?>">
						<option value="">Street Types..</option>
						<?php foreach ($street_types as $type): ?>
							<option value="<?php echo $type['SUFFIX'] ?>" <?php echo set_select('SUFFIX', $type['SUFFIX']); ?>><?php echo $type['SUFFIX'] ?></option>
						<?php endforeach ?>
					</select>
				</div>
				<div class="col-sm-4">
					<label for="CITY">Community</label>
					<input type="text" name="CITY" class="form-control add_community_name" title="Community" value="<?php echo set_value('CITY') ?>" />
				</div>
				<div class="col-sm-2">
					<label for="PROVINCE">Province</label>
					<input maxlenghth="2" type="text"class="form-control" title="Province" value="NS" disabled />
					<input maxlenghth="2" type="hidden" name="PROVINCE" title="Province" value="NS"/>
				</div>
			</div>
		</div>

		<legend>Additional Information</legend>

		<div class="form-group">
			<div class="row">
				<div class="col-sm-3">
					<label for="LSTNOLO">Odd Low</label>
					<input type="text" name="LSTNOLO" class="form-control" title="Odd Low" value="<?php echo set_value('LSTNOLO') ?>" />
				</div>
				<div class="col-sm-3">
					<label for="LSTNOHI">Odd High</label>
					<input type="text" name="LSTNOHI" class="form-control" title="Odd High" value="<?php echo set_value('LSTNOHI') ?>" />
				</div>
				<div class="col-sm-3">
					<label for="RSTNOLO">Even Low</label>
					<input type="text" name="RSTNOLO" class="form-control" title="Even Low" value="<?php echo set_value('RSTNOLO') ?>" />
				</div>
				<div class="col-sm-3">
					<label for="RSTNOHI">Even High</label>
					<input type="text" name="RSTNOHI" class="form-control" title="Even High" value="<?php echo set_value('RSTNOHI') ?>" />
				</div>
			</div>
		</div>

		<!-- <legend></legend>
 -->		<!-- input fields row 5 -->
		<div class="form-group">
			<div class="row">
				<div class="col-sm-6">
					<label for="SDE_STNAME">Official Street Name</label>
					<input type="text" name="SDE_STNAME" class="form-control official_street_name" title="Official Street Name" value="<?php echo set_value('SDE_STNAME') ?>" disabled/>
					<input type="hidden" name="SDE_STNAME" class="form-control official_street_name" title="Official Street Name" value="<?php echo set_value('SDE_STNAME') ?>" />
				</div>
				<div class="col-sm-6">
					<label for="SDE_CITY">Official Community Name</label>
					<input type="text" name="SDE_CITY" class="form-control official_community_name" title="Official Community Name" value="<?php echo set_value('SDE_CITY') ?>" disabled/>
					<input type="hidden" name="SDE_CITY" class="form-control official_community_name" title="Official Community Name" value="<?php echo set_value('SDE_CITY') ?>" />
				</div>
			</div>
		</div>

		<!-- <legend></legend>
 -->		<!-- Submit and back buttons  -->
		<div class="form-group">
			<div class="col-sm-12">
				<div class="row">
					<input type="submit" class="btn btn-primary btn-md pull-right" value="Add Street" />
					<a class="btn btn-default btn-md" href="<?php echo base_url(); ?>street_file">Back to Street File</a>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>