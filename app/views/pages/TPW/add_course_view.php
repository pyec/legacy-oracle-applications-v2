<div class="row">
	<div class="col-md-12">
		<h2>Add Course</h2>
		<p class="lead">This form will allow you to add a new course to the system.</p>
	</div>
</div>
<?php if ($this->session->flashdata('message')): ?>
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('message'); ?></p>
	</div>
<?php elseif($this->session->flashdata('error')) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo $this->session->flashdata('error'); ?></p>
	</div>
<?php elseif(validation_errors()) : ?>
	<div class="alert alert-danger alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" >&times;</button>
		<p><?php echo validation_errors(); ?></p>
	</div>	
<?php endif ?>
<div class="row">
	<div class="col-md-12">
		<div class="form-group">
			<div class="row">
				<form action="<?php echo base_url(); ?>TPW/add_course" method="POST">
				<div class="col-sm-2">
					<label for="COURSE_NUM">Course #</label><span class="text-danger">*</span>
					<input type="text" name="COURSE_NUM" class="form-control" title="Course Number"  />
				</div>

				<div class="col-sm-4">
					<label for="COURSE_NME">Course Name</label><span class="text-danger">*</span>
					<input type="text" name="COURSE_NME" class="form-control" title="Course Name"  />
				</div>
				
				<div class="col-sm-2 no_label_fix">
					<label for="RECERT_FLG">Recert Required</label>
					<input type="checkbox" name="RECERT_FLG" class="" title="Recert Flag"  />
				</div>

				<div class="col-sm-2">
					<label for="RECERT_PERIOD">Recert Period (yrs)</label>
					<input type="text" name="RECERT_PERIOD" class="form-control" title="Recert Period"  />
				</div>
			</div>
		</div>


		<div class="form-group">
			<div class="row">
				<div class="col-sm-12">
					<input type="submit" class="btn btn-md btn-primary add_course pull-right" value="Add Course" />
					<a href="<?php echo base_url(); ?>TPW/course_list" class="btn btn-md btn-default return">Back to Courses</a>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>