<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================

	   Name : Welcome

Description : This is the "landing" controller, where users will be directed once auth.

  Functions : index()

=========================================================================================================================*/
class Welcome extends CI_Controller 
{
/*-----------------------------------------------------------------------------------
	This is the main entry point to HRM APPS
------------------------------------------------------------------------------------*/
	public function index()
	{

		redirect('phonebook');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */