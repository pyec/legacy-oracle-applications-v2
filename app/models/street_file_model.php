<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Street File Update System Model

Description : This is the data layer for the Street File System responsible for 
				for updating the information on file for each street in HRM.

  Functions : get_street_files( $offset = 0 ),
  			get_street_file_types( ),
  			get_street_file_by_name( $name = null ),
  			insert_street_file( $data = null ),
  			update_street_file( $data = null ),
  			remove_street_file( $id = null ), 

=========================================================================================================================*/
class Street_file_model extends CI_Model 
{
	//Properies - These are identifiers to the actually table/field names
	public $street_file_table_name = null;
	public $street_file_id_name = null;
	public $oracle_db = null; // The reference to the database

	//Constructor
	public function __construct()
	{
		parent::__construct();
		//Load Dependencies

		// Initialize properties
		$this->street_file_table_name = "ADDRST2";
		$this->street_file_id_name = 'STR_CODE';
		$this->oracle_db = $this->load->database('oracle_street_file', TRUE);
	}

/*           
============================================================================
get_next_val
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
int - The next id in the table.
============================================================================
*/

	public function get_next_val()
	{
		// Query for the max value in the id field.
		$query = $this->oracle_db->query("SELECT MAX(STR_CODE) FROM addrst2");

		// In one expression return the result sets value, cast it to an integer 
		// and store it in the variable $id.
		$id = (int)$query->result_array()[0]["MAX(STR_CODE)"];

		// Increment by 1 and return the id so we can add a new record to the table.
		return ++$id;
	}
	
/*           
============================================================================
get_street_files( )
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
Return values :
$query->result_array() - (array) - The result set of records.
false - (boolean) - There were no results to return.
============================================================================
*/
	
	public function get_street_files( )
	{
		// Oracle doesn't work with CI's active records so we hav to write the sql ourselves.
		// $query = $this->oracle_db->query('SELECT * FROM '.$this->street_file_table_name.' ORDER BY STNAME ASC');
		$query = $this->oracle_db->get($this->street_file_table_name);
		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

/*           
============================================================================
get_street_file_types( )
----------------------------------------------------------------------------
N/A
----------------------------------------------------------------------------
Return values :
$query->result_array() - (array) - The result set of records.
false - (boolean) - There were no results to return.
============================================================================
*/	
	public function get_street_file_types() 
	{
		$query = $this->oracle_db->query('SELECT DISTINCT SUFFIX FROM '.$this->street_file_table_name.' ORDER BY SUFFIX ASC');

		if ($query->num_rows() > 0)
		{ 
			return $query->result_array();
		}
		else 
		{
			return false;
		}
	}

	public function search_streets( $data = null )
	{
		if ($data != null)
		{
			// $this->oracle_db->group_by(array('STNAME'));
			if (!empty($data['communities'])){ $this->oracle_db->like('CITY', $data['communities']);}
			if (!empty($data['name'])){ $this->oracle_db->like('STNAME', strtoupper($data['name']));}
			if (!empty($data['code'])){ $this->oracle_db->like('STR_CODE', $data['code']);}
			$query = $this->oracle_db->get($this->street_file_table_name);

			if ($query->num_rows() > 0)
			{ 
				return $query->result_array();
			}
			else 
			{
				return false;
			}
		}
	}

/*           
============================================================================
get_street_file_by_name( $name = null )
----------------------------------------------------------------------------
(String) $name - The name of the street.
----------------------------------------------------------------------------
Return values :
$query->result_array() - (array) - The result set of records.
false - (boolean) - There were no results to return.
============================================================================
*/
	public function get_street_file_by_name( $name = null )
	{
		if ($name == null)
		{
			return false;
		}
		else
		{
			$search_string = strtoupper($name);

			$this->oracle_db->like("STNAME", $search_string);
			$query = $this->oracle_db->get($this->street_file_table_name);

			if ($query->num_rows() > 0)
			{
				return $query->result_array;
			}
			else
			{
				return false;
			}
		}
	}

/*           
============================================================================
get_street_file_by_id( $id = null )
----------------------------------------------------------------------------
(int) $id - The id of the street.
----------------------------------------------------------------------------
Return values :
$query->result_array() - (array) - The result set of records.
false - (boolean) - There were no results to return.
============================================================================
*/
	public function get_street_file_by_id( $id = null )
	{
		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where('STR_CODE', $id);
			$query = $this->oracle_db->get($this->street_file_table_name);

			if ($query->num_rows() > 0)
			{
				return $query->result_array;
			}
			else
			{
				return false;
			}
		}
	}

	public function get_cities()
	{
		$this->oracle_db->distinct();
		$this->oracle_db->select('CITY');
		$this->oracle_db->where('EXP_FLAG', null);
		$this->oracle_db->order_by('CITY', 'asc');
		$query = $this->oracle_db->get($this->street_file_table_name);

		if ($query->num_rows() > 0)
		{
			return $query->result_array;
		}
		else
		{
			return false;
		}
	}

/*           
============================================================================
insert_street_file( $data = null )
----------------------------------------------------------------------------
(array[string,int]) $data - The data of the street to insert.
----------------------------------------------------------------------------
Return values :
true - (boolean) - The query was successful.
false - (boolean) - The query was unsuccessful.
============================================================================
*/
	public function insert_street_file( $data = null )
	{
		if ($data == null)
		{
			// return false;
		}
		else
		{
			$next_id = $this->get_next_val();
			$data['ADDDTTM'] = "to_date('".$data['ADDDTTM']."','YY-mm-dd')";
			$data['MODDTTM'] = "to_date('".$data['MODDTTM']."','YY-mm-dd')";

			$this->oracle_db->set("ADDDTTM", $data['ADDDTTM'], false);
			$this->oracle_db->set("MODDTTM", $data['MODDTTM'], false);
			unset($data['ADDDTTM']);
			unset($data['MODDTTM']);

			$data['STNAME'] = strtoupper($data['STNAME']);
			$data['POSTCODE'] = ' ';
			$data['POSTDIR'] = ' ';
			$data['PREDIR'] = ' ';

			// Concat the street name and street type for the offical street name
			

			$results = $this->oracle_db->insert($this->street_file_table_name, $data);

			return $results;

		}
	}

/*           
============================================================================
update_street_file( $data = null )
----------------------------------------------------------------------------
(array[string,int]) $data - The data of the street to update.
----------------------------------------------------------------------------
Return values :
true - (boolean) - The query was successful.
false - (boolean) - The query was unsuccessful.
============================================================================
*/
	public function update_street_file( $data = null )
	{
		if ($data == null)
		{
			return false;
		}
		else
		{
			$data['MODDTTM'] = "to_date('".$data['MODDTTM']."','YYYY-mm-dd')";

			$this->oracle_db->where('STR_CODE', $data['STR_CODE']);
			$this->oracle_db->set("MODDTTM", $data['MODDTTM'], false);
			unset($data['MODDTTM']);
			$results = $this->oracle_db->update($this->street_file_table_name, $data);
			return $results;
		}
	}


/*           
============================================================================
remove_street_file( $id = null )
----------------------------------------------------------------------------
(int) $id - The id of the street to delete.
----------------------------------------------------------------------------
Return values :
true - (boolean) - The query was successful.
false - (boolean) - The query was unsuccessful.
============================================================================
*/
	public function remove_street_file( $id = null ) 
	{

		if ($id == null)
		{
			return false;
		}
		else
		{
			$this->oracle_db->where($this->street_file_id_name, $id);
			$results = $this->oracle_db->delete($this->street_file_table_name);
			return results;
		}
	}

}

/* End of file street_file_model.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/models/street_file_model.php */