<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*========================================================================================================================
	   Name : Summary Offence Ticket System

Description : This system tracks all of the summary offence tickets issued in HRM 
			as well as offence bylaws, contact information of offenders and employee
			information.
  Functions : index
			summary_offence_tickets
			ticket_search
			add_SOT
			ajax_employee
			ajax_bylaw_codes
			ajax_section_codes
			ajax_contact
			ajax_contact_by_id
			ajax_contact_civic_info
			ajax_contact_address
			ajax_contact_phone
			ajax_civic
			get_bylaw_description
			update_SOT
			delete_SOT
			offence_bylaws
			bylaw_search
			add_bylaw
			update_bylaw
			remove_bylaw
			contact_information
			contact_search
			add_contact
			add_contact_phone
			update_contact
			delete_contact
			employee_information
			employee_searc
			add_employee
			add_employee_phone
			update_employee
			delete_employee
			
			
MODIFICATION HISTORY:
20160321		PRSC		Increased NOTES_DESC to 999 from 15 to allow larger data entry.
			
			
			
=========================================================================================================================*/
class SOT extends CI_Controller 
{

	function __construct()
	{
		parent::__construct();
		error_reporting(E_ERROR | E_PARSE | E_NOTICE);
	}

	public function index()
	{
		$this->load->model('SOT_model');
		// $data['content'] = "pages/SOT/home_view";
		$data['content'] = "pages/SOT/SOT_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
summary_offence_tickets
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function summary_offence_tickets()
	{
		//Load Model
		$this->load->model('SOT_model');

		$data['content'] = "pages/SOT/SOT_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
ticket_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function ticket_search()
	{
		// Load model
		$this->load->model('SOT_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('ticket_number', 'Ticket Number', 'trim|max_length[8]|xss_clean|numeric');
		$this->form_validation->set_rules('officer', 'Officer Name', 'trim|xss_clean');
		$this->form_validation->set_rules('location', 'Location', 'trim|xss_clean');

		// If validation is successfull.
		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['ticket_number']))
			{
				// Get the ticket's information by ticket number.
				if (!empty($data['tickets'])){$data['tickets'] = array_slice($data['tickets'], 0, 500);}
				$data['tickets'] = $this->SOT_model->get_SOT_like_id($search_array['ticket_number']);
				$data['content'] = "pages/SOT/SOT_view";
				$this->load->view('master', $data);
			}
			elseif (!empty($search_array['officer'])) 
			{
				if (!empty($data['tickets'])){$data['tickets'] = array_slice($data['tickets'], 0, 500);}
				$data['tickets'] = $this->SOT_model->get_SOT_like_officer_name(strtoupper($search_array['officer']));
				$data['content'] = "pages/SOT/SOT_view";
				$this->load->view('master', $data);
			}
			elseif (!empty($search_array['location'])) 
			{
				if (!empty($data['tickets'])){$data['tickets'] = array_slice($data['tickets'], 0, 500);}
				$data['tickets'] = $this->SOT_model->get_SOT_like_location_name(strtoupper($search_array['location']));
				$data['content'] = "pages/SOT/SOT_view";
				$this->load->view('master', $data);
			}
			else
			{
				// Else set the flash data to an error message.
				$this->session->set_flashdata('error', validation_errors());
				redirect('SOT/summary_offence_tickets');
			}
		}
		else
		{
			redirect('SOT/summary_offence_tickets');
		}
	}

/*           
============================================================================
add_SOT
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_SOT()
	{
		//Load Model
		$this->load->model('SOT_model');

		$post_data = $this->input->post(null, true);

		// If there are post variables present.
		if (!empty($post_data))
		{
			// Get the current date.
			$date = date('d-m-Y H:i:s');
			// Get the add by name from the session variables.
			$post_data['ADD_BY'] = $this->session->userdata('samaccountname'); 
			// Wrap the date vaiables in oracles to_date() function.
			$post_data['ADD_DTE'] = "to_date('".$date."','dd-mm-YY HH24:MI:SS')";
			$post_data['ARRAIGN_DTE'] = "to_date('".$post_data['ARRAIGN_DTE']."','dd-mm-YY HH24:MI:SS')";
			$post_data['COURT_DTE'] = "to_date('".$post_data['COURT_DTE']."','dd-mm-YY HH24:MI:SS')";
			$post_data['ISSUE_DTE'] = "to_date('".$post_data['ISSUE_DTE']."','dd-mm-YY HH24:MI:SS')";

			// Get the next key for the record insert.
			// $post_data['SOT_NUM'] = $this->SOT_model->get_next_val('ACT_SOT', 'SOT_NUM');


			// Unset the variables that aren't needed for the insert.
			unset($post_data['officer_name']);
			unset($post_data['AGENCY_NME']);
			unset($post_data['bylaw_description']);
			unset($post_data['CIVIC_ADDR']);
			unset($post_data['TITLE_DSC']);
			unset($post_data['FIRST_NME']);
			unset($post_data['LAST_NME']); 
			unset($post_data['MAIL_ADDR']);
			unset($post_data['COUNTRY_DSC']);
			unset($post_data['PROV_DSC']);
			unset($post_data['AREA_CDE']);
			unset($post_data['PHONE_NUM']);
			unset($post_data['EXT_CDE']);
			unset($post_data['PHONE_FLG']);
			// unset($post_data['CIVIC_ADDRESS']);
			unset($post_data['POSTAL_CDE']);
			// unset($post_data['CASE_NUM']);
			
			// Validation
			$this->form_validation->set_rules('SOT_NUM', 'SOT #', 'trim|xss_clean|required|max_length[8]');
			$this->form_validation->set_rules('ISSUE_DTE', 'Issue Date', 'trim|xss_clean|required');
			$this->form_validation->set_rules('ISSUE_ID', 'Officer', 'trim|xss_clean|max_length[10]|required');
			$this->form_validation->set_rules('CASE_NUM', 'Case Number', 'trim|xss_clean|max_length[6]');
			$this->form_validation->set_rules('CONTACT_NUM', 'Contact Number', 'trim|xss_clean|max_length[10]');
			$this->form_validation->set_rules('OUTCOME_DSC', 'Outcome Description', 'trim|xss_clean|max_length[15]');
			$this->form_validation->set_rules('FINE_AMT', 'Fine Amount', 'trim|xss_clean|max_length[6]|min_length[2]');
			$this->form_validation->set_rules('OFFENCELOC_DSC', 'Offence Location', 'trim|xss_clean|max_length[30]');
			$this->form_validation->set_rules('APT_NUM', 'Apartment Number', 'trim|xss_clean|max_length[5]');
			$this->form_validation->set_rules('DISTRICT_NUM', 'District Number', 'trim|xss_clean|max_length[4]');

			if ($this->form_validation->run() == true)
			{
				if (!$this->SOT_model->get_SOT_by_id($post_data['SOT_NUM']))
				{
					$post_data['CIVIC_ADDRESS'] = $post_data['CIVIC_NUM'].' '.$post_data['CIVIC_ADDRESS'];
					
					// If the validation is successful.
					// Run the insert and store the result (boolean) in results.
					$results = $this->SOT_model->insert_SOT($post_data);

					// If true (i.e. successful insert)
					if ($results)
					{
						// Set a success message to display to the user.
						$this->session->set_flashdata('message', 'Ticket has been successfully added.');
						redirect('SOT/update_SOT/'.$post_data['SOT_NUM']);
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'Ticket could not be added because the SOT # '.$post_data['SOT_NUM'].' already exists.');
					redirect('SOT/add_SOT');
				}
			}
			else
			{
				// else if the insert was a fail, set and error message.
				$data['bylaws'] = $this->SOT_model->get_bylaws();
				$data['content'] = "pages/SOT/add_SOT_view";
				$this->load->view('master', $data);
			}

		}
		else
		{
			// error: no data in post.
			$data['bylaws'] = $this->SOT_model->get_bylaws();
			$data['content'] = "pages/SOT/add_SOT_view";
			$this->load->view('master', $data);
		}
	}

/*
	For the inert form we need to perform a number of AJAX calls for data that will be
	linked to the ticket, the following functions are the server side routines of those
	AJAX calls.

	I'll comments the first function and the rest of them will be doing similar things.
*/
	public function ajax_employee()
	{
		//Load model
		$this->load->model('SOT_model');
		// Grab the post variables from the AJAX call. (i.e. Variables from javascript)
		$data['FIRST_NME'] = $this->input->post('FIRST_NME', true);
		$data['LAST_NME'] = $this->input->post('LAST_NME', true);
		
		// Return employee matches based on the search string.
		$matches = $this->SOT_model->search_employees($data);

		// If the results aren't empty.
		if (!empty($matches))
		{
			// Encode the array to a JSON object and echo it back to the browser.
			// Javascript will then capture the JSON in the .success(json) callback.
			echo json_encode($matches);
		}
		else
		{
			// If there aren't any results encode a error message to send back to the browser.
			echo json_encode('No Results.');
		}
	}

	public function ajax_bylaw_codes()
	{
		// Load model
		$this->load->model('SOT_model');
		$string = $this->input->post('string', true);

		$matches = $this->SOT_model->get_bylaw_codes($string);

		if (!empty($matches))
		{
			echo json_encode($matches);
		}
		else
		{
			echo json_encode('No Results.');
		}
	}

	public function ajax_section_codes()
	{
		// Load model
		$this->load->model('SOT_model');
		$string = $this->input->post('string', true);

		$matches = $this->SOT_model->get_section_codes($string);
		
		if (!empty($matches))
		{
			echo json_encode($matches);
		}
		else
		{
			echo json_encode('No Results.');
		}
	}

	public function ajax_contact()
	{
		// Load model
		$this->load->model('SOT_model');
		$last_name = $this->input->post('string', true);

		$contact = $this->SOT_model->get_contact_by_last_name($last_name);

		echo json_encode($contact);
	}

	public function ajax_contact_by_id()
	{
		// Load model
		$this->load->model('SOT_model');
		$contact_id = $this->input->post('id', true);

		$contact = $this->SOT_model->get_contact_by_id($contact_id);

		echo json_encode($contact);
	}

	public function ajax_contact_civic_info()
	{
		// Load model
		$this->load->model('SOT_model');
		$contact_id = $this->input->post('id', true);

		$contact = $this->SOT_model->get_contact_civic_info($contact_id);

		echo json_encode($contact);
	}

	public function ajax_contact_address()
	{
		// Load model
		$this->load->model('SOT_model');
		$contact_id = $this->input->post('id', true);

		$contact = $this->SOT_model->get_contact_address($contact_id);

		echo json_encode($contact);
	}

	public function ajax_contact_phone()
	{
		// Load model
		$this->load->model('SOT_model');
		$contact_id = $this->input->post('id', true);

		$contact = $this->SOT_model->get_contact_phone($contact_id);

		echo json_encode($contact);
	}

	public function ajax_civic()
	{
		// Load model
		$this->load->model('SOT_model');
		$civic_id = $this->input->post('id', true);
		$civic = $this->SOT_model->get_civic_by_like_id($civic_id);

		echo json_encode($civic);
	}

	public function get_bylaw_description()
	{
		// Load model
		$this->load->model('SOT_model');
		$data['BYLAW_CDE'] = $this->input->post('bylaw_code', true);
		$data['SECTION_CDE'] = $this->input->post('section_code', true);

		// using the get_bylaw_for_update function becuase it achieves the 
		// exactly result that we're looking for.

		$results = $this->SOT_model->get_bylaw_for_update($data);

		echo json_encode($results);
	}

	public function ajax_streets()
	{
		// Load model
		$this->load->model('SOT_model');
		$street_name = $this->input->post('name');

		$results = $this->SOT_model->get_streets_by_name($street_name);

		echo json_encode($results);
	}

	public function ajax_street_by_id()
	{
		// Load model
		$this->load->model('SOT_model');
		$id = $this->input->post('id');

		$results = $this->SOT_model->get_streets_by_id($id);

		echo json_encode($results);
	}

	// END OF AJAX FUNCITONS -----------------------------------------------

/*           
============================================================================
update_SOT
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_SOT()
	{
		//Load Model
		$this->load->model('SOT_model');
		$this->load->model('business_unit_update_model');

		// Get the post variables if there are any.
		$post_data = $this->input->post(null, true);
		// Get the ticket_id from the url.
		$ticket_id = $this->uri->segment(3);

		/*
			If there are post variables then we know there was a form submission
			so well go head and prep the data for an update query.

			else we know all we have to do is display the record to the user and
			wait for a form submission.
		*/
		if (!empty($post_data))
		{

			/*
				When we display the record to the user for a modification we display
				not only the record from act_sot but all it's affiliated records.

				Because we only need the values pertaining to the act_sot record
				we need to filter out all the other values there that don't belong.

				We do this by our name attributes coming through POST. The values
				we want to insert are in uppercase to match the field names in the 
				DB, everything we don't need is in undercase so we have something 
				to check on.

				Start by loop over each POST variable.
			*/
			foreach ($post_data as $key => $value) 
			{	
				// Underscores will mess up an uppcase check so strip them out.
				$field = explode("_", $key);

				// If the field is not uppercase.
				if (!ctype_upper($field[0]))
				{
					// unset the field from the post variables.
					$test[$key] = $value;
					unset($post_data[$key]);
				}

			}
			// Get the mod by value from the session variables.
			$post_data['MOD_BY'] = $this->session->userdata('samaccountname');

			$date = date('d-m-y H:i:s');
			$post_data['MOD_DTE'] = "to_date('".$date."','dd-mm-YY HH24:MI:SS')";
			// $post_data['ARRAIGN_DTE'] = "to_date('".$post_data['ARRAIGN_DTE']."','dd-mm-YY HH24:MI:SS')";
			// $post_data['COURT_DTE'] = "to_date('".$post_data['COURT_DTE']."','dd-mm-YY HH24:MI:SS')";
			// $post_data['ISSUE_DTE'] = "to_date('".$post_data['ISSUE_DTE']."','dd-mm-YY HH24:MI:SS')";

			$sot_num = $post_data['SOT_NUM'];

			// if the bylaw code sections are empty remove them from the post so that the record does get updated with null values
			if (empty($post_data['BYLAW_CDE']))
			{
				$post_data['BYLAW_CDE'] = $post_data['BYLAW'];
				$post_data['SECTION_CDE'] = $post_data['SECTION'];
			}

			unset($post_data['BYLAW']);
			unset($post_data['SECTION']);

			// Validation
			$this->form_validation->set_rules('ISSUE_DTE', 'Issue Date', 'trim|xss_clean|required');
			$this->form_validation->set_rules('ISSUE_ID', 'Officer', 'trim|xss_clean|max_length[10]|required');
			$this->form_validation->set_rules('CASE_NUM', 'Case Number', 'trim|xss_clean|max_length[6]');

			if ($this->form_validation->run() == true)
			{	
				$this->SOT_model->update_SOT($post_data);
				
				$this->session->set_flashdata('message', 'Ticket #'.$sot_num.' has been updated.');
				redirect('SOT/update_SOT/'.$sot_num);
			}
			else
			{
				// Get SOT data.
				$data['ticket'] = $this->SOT_model->get_SOT_by_id($sot_num);
				// Load BLOB back into the array to be displayed.
				if (!empty($data['ticket']))
				{
					// load the note description from the CLOB.
					if (!empty($data['ticket'][0]['NOTES_DSC']))
					{
						$data['ticket'][0]['NOTES_DSC'] = $data['ticket'][0]['NOTES_DSC']->load();	
					}

					$data['ticket'] = $data['ticket'][0];

					// Get Contacts information.
					$data['contact'] = $this->SOT_model->get_contact_by_id($data['ticket']['CONTACT_NUM']);
					$data['contact'] = $data['contact'][0];
					$data['contact_phone'] = $this->SOT_model->get_contact_phone($data['ticket']['CONTACT_NUM']);

					// Get Bylaw information.
					// $data['bylaw'] = $this->SOT_model->get_bylaw_by_section($data['ticket']['SECTION_CDE']);
					// $data['bylaw'] = $data['bylaw'][0];
					$data['ticket']['BYLAW_DSC'] = $this->SOT_model->get_bylaw_description_by_codes($data['ticket']['BYLAW_CDE'], $data['ticket']['SECTION_CDE']);
					$data['SOT_civic'] = $this->SOT_model->get_contact_civic_info($data['ticket']['CONTACT_NUM']);
					$data['SOT_civic'] = $data['SOT_civic'][0];

					// Get employee information.
					$data['employee'] = $this->SOT_model->get_employee_by_id($data['ticket']['ISSUE_ID']);
					$data['employee'] = $data['employee'][0];

					$data['content'] = "pages/SOT/update_SOT_view";

					$this->load->view('master', $data);	
				}
			}	

		}
		else
		{
			// Get SOT data.
			$data['ticket'] = $this->SOT_model->get_SOT_by_id($ticket_id);
			// Load BLOB back into the array to be displayed.
			if (!empty($data['ticket']))
			{
				// load the note description from the CLOB.
				if (!empty($data['ticket'][0]['NOTES_DSC']))
				{
					$data['ticket'][0]['NOTES_DSC'] = $data['ticket'][0]['NOTES_DSC']->load();	
				}
				$data['ticket'] = $data['ticket'][0];

				// Get Contacts information.
				$data['contact'] = $this->SOT_model->get_contact_by_id($data['ticket']['CONTACT_NUM']);
				$data['contact'] = $data['contact'][0];
				$data['contact_phone'] = $this->SOT_model->get_contact_phone($data['ticket']['CONTACT_NUM']);

				// Get Bylaw information.
				$data['bylaw'] = $this->SOT_model->get_bylaw_by_section($data['ticket']['SECTION_CDE']);
				$data['bylaw'] = $data['bylaw'][0];
				$data['ticket']['BYLAW_DSC'] = $this->SOT_model->get_bylaw_description_by_codes($data['ticket']['BYLAW_CDE'], $data['ticket']['SECTION_CDE']);
				$data['SOT_civic'] = $this->SOT_model->get_contact_civic_info($data['ticket']['CONTACT_NUM']);
				$data['SOT_civic'] = $data['SOT_civic'][0];

				// Get employee information.
				$data['employee'] = $this->SOT_model->get_employee_by_id($data['ticket']['ISSUE_ID']);
				$data['employee'] = $data['employee'][0];

				$data['bylaws'] = $this->SOT_model->get_bylaws();

				$data['content'] = "pages/SOT/update_SOT_view";

			
				$this->load->view('master', $data);	
			}
			else
			{
				$this->session->set_flashdata('error', 'No ticket could be found.');
				redirect('SOT/summary_offence_tickets');
			}
		}
	}

/*           
============================================================================
delete_SOT
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - Handles passing the id to delete to the model to run the query.
============================================================================
*/
	public function delete_SOT()
	{
		// Load Model
		$this->load->model('SOT_model');

		$post_data = $this->input->post('SOT_NUM');

		if ($this->SOT_model->remove_SOT($post_data))
		{
			$this->session->set_flashdata('message', 'The SOT: '.$post_data.' has been successfully removed.');
			redirect('SOT/summary_offence_tickets');
		}
		else
		{ 
			$this->session->set_flashdata('error', 'An error has occurred, the SOT could not be removed.');
			redirect('SOT/summary_offence_tickets');
		}
	}

/*           
============================================================================
offence_bylaws
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function offence_bylaws()
	{
		//Load Model
		$this->load->model('SOT_model');

		$data['content'] = "pages/SOT/bylaw_offence_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
bylaw_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function bylaw_search()
	{
		// Load model
		$this->load->model('SOT_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('bylaw_code', 'Bylaw Code', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('section_code', 'Section Code', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('description', 'Bylaw Description', 'trim|max_length[1000]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array))
			{
				$data['bylaws'] = $this->SOT_model->search_bylaws($search_array);
			}
			else
			{
				redirect('SOT/offence_bylaws');
			}
		}
		else
		{
			redirect('SOT/offence_bylaws');
		}

		if (!empty($data['bylaws'])){$data['bylaws'] = array_slice($data['bylaws'], 0, 500);}
		$data['content'] = "pages/SOT/bylaw_offence_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_offence_bylaw
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_bylaw()
	{
		//Load Model
		$this->load->model('SOT_model');

		$post_data = $this->input->post(null, true);

		
		if (!empty($post_data))
		{
			$this->form_validation->set_rules('SECTION_CDE', 'Seciton Code', 'trim|max_length[15]|xss_clean|required');
			$this->form_validation->set_rules('BYLAW_CDE', 'Bylaw Code', 'trim|max_length[12]|xss_clean|required');
			$this->form_validation->set_rules('BYLAW_DSC', 'Description', 'trim|max_length[1000]|xss_clean');
			$this->form_validation->set_rules('TYPE_CDE', 'Type Code', 'trim|max_length[4]|xss_clean');

			if ($this->form_validation->run())
			{

				foreach ($post_data as $key => $value) 
				{
					$post_data[$key] = strtoupper($value);
				}

				if ($this->SOT_model->insert_bylaw($post_data))
				{
					// Return a success message.
					$this->session->set_flashdata('message', 'The bylaw has been successfully added.');

					redirect('SOT/offence_bylaws');
				}
				else
				{
					// Return an error message.
					$this->session->set_flashdata('error', 'The bylaw could not be added to the database, plase try again.');

					redirect('SOT/add_bylaw');
				}	
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('SOT/add_bylaw');
			}
		}
		else
		{
			// error: no data in post.
			$data['content'] = "pages/SOT/add_offence_bylaw_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
update_offence_bylaw
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_bylaw()
	{
		//Load Model
		$this->load->model('SOT_model');

		$post_data = $this->input->post(null, true);

		// Because the model processes the fields the same way we need to tack on a "H-" to
		// the name attribute of the hidden fields so we can determine weather or not 
		// we're getting ready to update a bylaw or we're submitting updated info to the db.
		if (!empty($post_data['H-SECTION_CDE']) && !empty($post_data['H-BYLAW_CDE']) && empty($post_data['BYLAW_DSC']))
		{
			// If we're getting ready to update remove the hidden flag from the name.
			foreach ($post_data as $key => $value) 
			{
				$new_key = explode("-", $key);
				unset($post_data[$key]);
				$post_data[$new_key[1]] = $value; 
			}
			
			$data['bylaw'] = $this->SOT_model->get_bylaw_for_update($post_data);
			$data['content'] = "pages/SOT/update_bylaw_view";
			$this->load->view('master', $data);
		}
		elseif (!empty($post_data['SECTION_CDE']) && !empty($post_data['BYLAW_CDE']))
		{

			if ($this->SOT_model->update_bylaw($post_data))
			{
				$this->session->set_flashdata('message', 'Bylaw updated successfully');
				redirect('SOT/offence_bylaws');
			}
			else
			{
				$this->session->set_flashdata('error', 'The bylaw could not be updated, please try again.');
				redirect('SOT/offence_bylaws');
			}
		}
	}

/*           
============================================================================
delete_offence_bylaw
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the id to delete to the model to run the query.
============================================================================
*/
	public function remove_bylaw()
	{
		// Load Model
		$this->load->model('SOT_model');

		$post_data = $this->input->post(null, true);

		if ($this->SOT_model->remove_bylaw($post_data))
		{
			$this->session->set_flashdata('message', 'The bylaw has been successfully removed.');
			redirect('SOT/offence_bylaws');
		}
		else
		{ 
			$this->session->set_flashdata('error', 'An error has occurred, the bylaw could not be removed.');
			redirect('SOT/offence_bylaws');
		}
	}

/*           
============================================================================
contact_information
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function contact_information()
	{
		//Load Model
		$this->load->model('SOT_model');

		$data['content'] = "pages/SOT/contact_information_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
contact_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function contact_search()
	{
		// Load model
		$this->load->model('SOT_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('contact_number', 'Contact Number', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[15]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['contact_number']))
			{
				$data['contacts'] = $this->SOT_model->get_contacts_like_id($search_array['contact_number']);
			}
			else if (empty($search_array['last_name']) && !empty($search_array['first_name']))
			{
				$data['contacts'] = $this->SOT_model->get_contact_by_first_name($search_array['first_name']);
			}
			else if (empty($search_array['first_name']) && !empty($search_array['last_name']))
			{
				$data['contacts'] = $this->SOT_model->get_contact_by_last_name($search_array['last_name']);
			}
			else if (!empty($search_array['first_name']) && !empty($search_array['last_name']))
			{
				$data['contacts'] = $this->SOT_model->get_contact_by_name($search_array);
			}
			else
			{
				redirect('SOT/contact_information');
			}
		}
		else
		{
			redirect('SOT/contact_information');
		}

		if (!empty($data['contacts'])){$data['contacts'] = array_slice($data['contacts'], 0, 500);}

		$data['content'] = "pages/SOT/contact_information_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_contact
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.

MODIFICATION HISTORY:
20160321		PRSC		Increased NOTES_DESC to 999 from 15 to allow larger data entry.

============================================================================
*/
	public function add_contact()
	{
		//Load Model
		$this->load->model('SOT_model');

		$post_data = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('TITLE_DSC', 'Title', 'trim|max_length[5]|xss_clean');
		$this->form_validation->set_rules('FIRST_NME', 'First Name', 'trim|max_length[30]|xss_clean');
		$this->form_validation->set_rules('MIDDLE_NME', 'Middle Name', 'trim|max_length[3]|xss_clean');
		$this->form_validation->set_rules('LAST_NME', 'Last Name', 'trim|max_length[30]|xss_clean|required');
		$this->form_validation->set_rules('EMAIL_DSC', 'Email Address', 'trim|max_length[30]|xss_clean');
		$this->form_validation->set_rules('FROM_DTE', 'From Date', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('TO_DTE', 'To Date', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('NOTES_DSC', 'Notes Desc', 'trim|max_length[999]|xss_clean');
		$this->form_validation->set_rules('D-CIVIC_ADDR', 'Civic Address', 'trim|max_length[100]|xss_clean');
		$this->form_validation->set_rules('D-MAIL_ADDR', 'Mail Address', 'trim|max_length[100]|xss_clean');
		$this->form_validation->set_rules('A-CIVIC_NUM', 'Civic Number', 'trim|max_length[6]|xss_clean');
		$this->form_validation->set_rules('A-APT_NUM', 'Apartment Number', 'trim|max_length[5]|xss_clean');
		$this->form_validation->set_rules('A-DISTRICT_NUM', 'District Number', 'trim|max_length[4]|xss_clean');
		$this->form_validation->set_rules('A-ADDRESS_TYP', 'Address Type', 'trim|max_length[1]|xss_clean');
		$this->form_validation->set_rules('A-MAILCITY_DSC', 'Mailing City', 'trim|max_length[25]|xss_clean');
		$this->form_validation->set_rules('A-COUNTRY_DSC', 'Country', 'trim|max_length[25]|xss_clean');
		$this->form_validation->set_rules('A-POSTAL_CDE', 'Postal Code', 'trim|max_length[7]|xss_clean');
		$this->form_validation->set_rules('A-PROV_DSC', 'Province', 'trim|max_length[2]|xss_clean');
		$this->form_validation->set_rules('A-ADDRESS_NOTES', 'Address Notes', 'trim|max_length[200]|xss_clean');
		$this->form_validation->set_rules('P-AREA_CDE', 'Area Code', 'trim|max_length[3]|xss_clean');
		$this->form_validation->set_rules('P-PHONE_NUM', 'Phone Number', 'trim|max_length[7]|xss_clean');
		$this->form_validation->set_rules('P-EXT_CDE', 'Exit code', 'trim|max_length[4]|xss_clean');
		$this->form_validation->set_rules('P-PHONE_FLG', 'Phone Flag', 'trim|max_length[1]|xss_clean');
		$this->form_validation->set_rules('P-FROM_DTE', 'From Date', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('P-TO_DTE', 'To Date', 'trim|max_length[15]|xss_clean');

		if (!empty($post_data))
		{
			// If post data has been submitted then run the validation.
			if ($this->form_validation->run() == false)
			{	
				// Set the error message and send them back tot he add screen.
				$data['content'] = "pages/SOT/add_contact_view";
				$this->load->view('master', $data);
			}
			else
			{
				// Loop over the post variables and group them in separate arrays 
				// using our name attribute prefix.
				foreach ($post_data as $key => $value) 
				{
					// explode the post variable keys, if the value belongs to a table 
					// other than the contact table it will be prefixed with a letter.
					$exploded_key = explode('-', $key);

					// if the prefix is 'A' we know it belongs to the address table.
					if ($exploded_key[0] == 'A')
					{
						$address[$exploded_key[1]] = $value;
					}
					// contact description table
					elseif ($exploded_key[0] == 'D')
					{
						$contact_desc[$exploded_key[1]] = $value;
					}
					// contact phone table
					elseif ($exploded_key[0] == 'P')
					{
						$phone[$exploded_key[1]] = $value;
					}
					// everything else gets inserted into the contact.
					else
					{
						$contact[$key] = $value;
					}

				}

				// Once everything is separated properly we need to fetch the new
				// contact number and add it to each array.
				$new_id = $this->SOT_model->get_next_val('ACT_CONTACT', 'CONTACT_NUM');
				$contact['CONTACT_NUM'] = $new_id;
				$contact_desc['CONTACT_NUM'] = $new_id;
				$address['CONTACT_NUM'] = $new_id;
				$phone['CONTACT_NUM'] = $new_id;

				$date = date('d-m-Y H:i:s');

				$contact['FIRST_NME'] = strtoupper($contact['FIRST_NME']);
				$contact['MIDDLE_NME'] = strtoupper($contact['MIDDLE_NME']);
				$contact['LAST_NME'] = strtoupper($contact['LAST_NME']);
				$contact['TITLE_DSC'] = strtoupper($contact['TITLE_DSC']);
				$contact['AGENCY_NME'] = strtoupper($contact['AGENCY_NME']);

				$contact['ADD_BY'] = $this->session->userdata('samaccountname'); 
				$contact['ADD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";
				

				// Fetch the new address id, add by and add date then add it to the address array.
				$address['ADDRESS_NUM'] = $this->SOT_model->get_next_val('ACT_ADDRESS', 'ADDRESS_NUM');
				$address['ADD_BY'] = $this->session->userdata('samaccountname');
				$address['ADD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";
//				$address['FROM_DTE'] = "to_date('".$address['FROM_DTE']."','dd-mm-YY')";
//				$address['TO_DTE'] = "to_date('".$address['TO_DTE']."','dd-mm-YY')";
                $address['FROM_DTE'] = $address['FROM_DTE'];
                $address['TO_DTE'] = $address['TO_DTE'];

				// Fetch the same for the phone array.
				$phone['PHONEREC_NUM'] = $this->SOT_model->get_next_val('ACT_CONTPHONE', 'PHONEREC_NUM');
				$phone['ADD_BY'] = $this->session->userdata('samaccountname');
				$phone['ADD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";
				
				$contact_desc['NAME'] = $contact['FIRST_NME'].' '.$contact['LAST_NME'];

				// Attempt to insert the contacts basic data.
				$contact_results = $this->SOT_model->insert_contact($contact);

				if ($contact_results)
				{
					// if that is successful the attempt to insert the contacts description data.
					$contact_desc_results = $this->SOT_model->insert_contact_description($contact_desc);

					if ($contact_desc_results)
					{
						// If that is successful, go head and insert the address and phone data.
						$address_results = $this->SOT_model->insert_contact_address($address);
						$phone_results = $this->SOT_model->insert_contact_phone($phone);

						if ($address_results && $phone_results)
						{
							// If everything is inserted correctly then set a success message and 
							// redirect them to the update page of that inserted contact for them to varify.
							$this->session->set_flashdata('message', 'The contact has been successfully inserted.');
							redirect('SOT/update_contact/'.$contact['CONTACT_NUM']);
						}
						else
						{
							// delete the contact and description records and get them to try again.
							$this->SOT_model->delete_contact($contact['CONTACT_NUM']);
							$this->SOT_model->delete_contact_desciption($contact_desc['CONTACT_NUM']);
							$this->session->set_flashdata('error', 'An error occured while inserting the contacts record. Please try again.');
							redirect('SOT/add_contact');

						}
					}
					else
					{
						// There was an error inserting the contacts description
						// delete the contact record and redirect them to try again
						$this->SOT_model->delete_contact($contact['CONTACT_NUM']);
						$this->session->set_flashdata('error', 'An error occured while inserting some of the contact');
						redirect('SOT/add_contact');
					}
				}
				else
				{
					// Error - failed to add the contact
					$this->session->set_flashdata('error', 'The contact could not be inserted. Please try again.');
					redirect('SOT/add_contact');
				}
			}
		}
		else
		{
			// error: no data in post.
			$data['content'] = "pages/SOT/add_contact_view";
			$this->load->view('master', $data);
		}	
	}

	public function add_contact_phone()
	{	
		//Load model
		$this->load->model('SOT_model');
		$data['contact_id'] = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('AREA_CDE', 'Area Code', 'trim|max_length[3]|xss_clean|numeric');
		$this->form_validation->set_rules('PHONE_NUM', 'Phone Number', 'trim|max_length[7]|xss_clean|numeric|required');
		$this->form_validation->set_rules('EXT_CDE', 'Extention', 'trim|max_length[4]|xss_clean|numeric');
		$this->form_validation->set_rules('PHONE_FLG', 'Phone Type', 'trim|max_length[1]|xss_clean|alpha');

		// if we're submitting data to be inserted
		if (!empty($post))
			{
				// RUn the validation we set up above.
				if ($this->form_validation->run() == true)
				{
					// set up the post variables for insert
					$post['PHONEREC_NUM'] = $this->SOT_model->get_next_val("ACT_CONTPHONE", "PHONEREC_NUM");
					$post['ADD_BY'] = $this->session->userdata('samaccountname');
					$post['ADD_DTE'] = date('d-m-Y H:i:s');
					$post['ADD_DTE'] = "to_date('".$post['ADD_DTE']."','dd-mm-YYYY HH24:MI:SS')";

					// call the model for the insert and pass it the post data.
					// upon success/fail the model will return true/false.
					$results = $this->SOT_model->insert_contact_phone($post);

					// if the query was a success.
					if ($results)
					{
						// set a message for the user and then redirect back to the contacts record.
						$this->session->set_flashdata('message', 'The phone record has been added to the contact.');
						redirect('SOT/update_contact/'.$post['CONTACT_NUM']);
					}		
				}
				else
				{	
					// If the validation failed, return an error message to the user and redirect them
					// back to the add phone form.
					$this->session->set_flashdata('error', validation_errors());
					redirect('SOT/add_contact_phone/'.$post['CONTACT_NUM']);
				}
			}
			else
			{
				$data['contact'] = $this->SOT_model->get_contact_by_id($data['contact_id']);
				$data['content'] = "pages/SOT/add_contact_phone_view";
				$this->load->view('master', $data);
			}
		
	}

/*           
============================================================================
update_contact
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_contact()
	{
		//Load Model
		$this->load->model('SOT_model');

		//Get contact id from the URL 
		$id = $this->uri->segment(3);

		// Grab the post wheather there is post or not
		$post = $this->input->post(null, true);

		// If the post is not empty then we know that we have submitted values to update 
		// rather than fetching the values to display to the user for review.
		if (!empty($post))
		{	
			// Separating post values to update multiple tables 
			//-------------------------------
			/*
				Because the collection of contact data comes from multiple tables
				
			*/
			foreach ($post as $key => $value) 
			{
				$exploded_key = explode("-", $key);
				if (is_numeric($exploded_key[0]))
				{
					unset($post[$key]);
					$phones[$exploded_key[0]][$exploded_key[1]] = $value;
				}
				elseif($exploded_key[0] == 'C')
				{
					unset($post[$key]);
					$civic[$exploded_key[1]] = $value;
				}
			}
		
			// Set up the mod_by and mod_date for the update query.
			$date = date('d-m-Y H:i:s');
			$post['MOD_BY'] = $this->session->userdata('samaccountname');
			$civic['MOD_BY'] = $this->session->userdata('samaccountname');
			$post['MOD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";
			$civic['MOD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";

			// The contacts civic information needs a key to be updated.
			$civic['CONTACT_NUM'] = $post['CONTACT_NUM'];

			if(!empty($post['FIRST_NME'])){$post['FIRST_NME'] = strtoupper($post['FIRST_NME']);}
			if(!empty($post['MIDDLE_NME'])){$post['MIDDLE_NME'] = strtoupper($post['MIDDLE_NME']);}
			if(!empty($post['LAST_NME'])){$post['LAST_NME'] = strtoupper($post['LAST_NME']);}
			if(!empty($post['TITLE_DSC'])){$post['TITLE_DSC'] = strtoupper($post['TITLE_DSC']);}
			if(!empty($post['AGENCY_NME'])){$post['AGENCY_NME'] = strtoupper($post['AGENCY_NME']);}


			if (!empty($phones))
			{
				// For each phone record add the mod date and mod by values.
				foreach ($phones as $phone) 
				{	
					$phone['MOD_BY'] = $this->session->userdata('samaccountname');
					$phone['MOD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";			

					$this->SOT_model->update_contact_phone($phone);
				}	
			}

			$contact_results = $this->SOT_model->update_contact($post);
			$civic_results = $this->SOT_model->update_contact_civic($civic);
			$this->SOT_model->update_contact_description($civic);

			if (!empty($phone_results))
			{
				if ($contact_results && $phone_results && $civic_results)
				{
					$this->session->set_flashdata('message', 'The contact has been successfully updated.');
					redirect('SOT/update_contact/'.$post['CONTACT_NUM']);	
				}	
			}
			else
			{
				if ($contact_results && $civic_results)
				{
					$this->session->set_flashdata('message', 'The contact has been successfully updated.');
					redirect('SOT/update_contact/'.$post['CONTACT_NUM']);	
				}
			}
		}
		//Get all of the data pertaining to the CONTACT_NUM.
		else
		{
			$data['contact'] = $this->SOT_model->get_contact_by_id($id);
			$data['phone_numbers'] = $this->SOT_model->get_contact_phone($id);
			$data['civic'] = $this->SOT_model->get_contact_civic_info($id);
			$data['address'] = $this->SOT_model->get_contact_address($id);

			$data['content'] = "pages/SOT/update_contact_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
delete_contact
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - hadles passign the id to delete to the model to run the query.
============================================================================
*/
	public function delete_contact()
	{
		//Load Model
		$this->load->model('SOT_model');

		$id = $this->input->post('CONTACT_NUM');

		// Retrieve the users phone records
		$contact_phones = $this->SOT_model->get_contact_phone($id);

		// delete the phone records
		if (!empty($contact_phones))
		{
			foreach ($contact_phones as $phone) 
			{
				$this->SOT_model->remove_contact_phone($phone['PHONEREC_NUM'], $id);
			}
		}

		// Next delete the contact decription record
		$this->SOT_model->remove_contact_desc($id);
		$this->SOT_model->remove_contact_address($id);

		$delete_results = $this->SOT_model->remove_contact($id);

		if ($delete_results)
		{
			$this->session->set_flashdata('message', 'The contact was successfully deleted.');
			redirect('SOT/contact_information');
		}
		else
		{
			$this->session->set_flashdata('error', 'An error occured while deleteing the contact, please try again.');
			redirect('SOT/contact_information');
		}		
	}

/*           
============================================================================
employee_information
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - this function sets up the data for the view.
============================================================================
*/
	public function employee_information()
	{
		//Load Model
		$this->load->model('SOT_model');

		$data['content'] = "pages/SOT/employee_information_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
employee_search
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - Handles validation and setting up the search string.
============================================================================
*/
	public function employee_search()
	{
		// Load model
		$this->load->model('SOT_model');

		// Get search params from the form.
		$search_array = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('employee_number', 'Employee Number', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('first_name', 'First Name', 'trim|max_length[25]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|max_length[25]|xss_clean');

		if ($this->form_validation->run() == true)
		{
			if (!empty($search_array['employee_number']))
			{
				$data['employees'] = $this->SOT_model->get_employee_by_id($search_array['employee_number']);
			}
			else if (empty($search_array['last_name']) && !empty($search_array['first_name']))
			{
				$data['employees'] = $this->SOT_model->get_employee_by_first_name($search_array['first_name']);
			}
			else if (empty($search_array['first_name']) && !empty($search_array['last_name']))
			{
				$data['employees'] = $this->SOT_model->get_employee_by_last_name($search_array['last_name']);
			}
			else if (!empty($search_array['first_name']) && !empty($search_array['last_name']))
			{
				$data['employees'] = $this->SOT_model->get_employee_by_name($search_array);
			}
			else
			{
				redirect('SOT/employee_information');
			}
		}
		else
		{
			redirect('SOT/employee_information');
		}
		if (!empty($data['employees'])){$data['employees'] = array_slice($data['employees'], 0, 500);}

		$data['content'] = "pages/SOT/employee_information_view";
		$this->load->view('master', $data);
	}

/*           
============================================================================
add_employee
----------------------------------------------------------------------------
NO PARAMS
----------------------------------------------------------------------------
No return - handles adding owner record to the table.
============================================================================
*/
	public function add_employee()
	{
		//Load Model
		$this->load->model('SOT_model');

		$post_data = $this->input->post(null, true);

		$this->form_validation->set_rules('EMPL_ID', 'Employee Id', 'trim|max_length[10]|xss_clean|required');
		$this->form_validation->set_rules('FIRST_NME', 'First Name', 'trim|max_length[30]|xss_clean|required');
		$this->form_validation->set_rules('MIDDLE_NME', 'Middle Name', 'trim|max_length[3]|xss_clean');
		$this->form_validation->set_rules('LAST_NME', 'Last Name', 'trim|max_length[30]|xss_clean|required');
		$this->form_validation->set_rules('AGENCY_NME', 'Agency Name', 'trim|max_length[40]|xss_clean');
		$this->form_validation->set_rules('EMAIL_DSC', 'Email', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('WORK_LOC', 'Work Location', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('NOTES_DSC', 'Notes Description', 'trim|max_length[1000]|xss_clean');

		if (!empty($post_data))
		{
			if ($this->form_validation->run() == true)
			{
				// add the added date
				$date = date('d-m-y H:i:s');
				$post_data['ADD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";

				// add the added by 
				$post_data['ADD_BY'] = $this->session->userdata('samaccountname');
				
				// Separate the employee values from the phone record values.
				foreach ($post_data as $key => $value) 
				{
					$exploded_key = explode('-', $key);

					if ($exploded_key[0] == "P")
					{
						$phone[$exploded_key[1]] = $value;
						unset($post_data[$key]);
					}
				}

				// set the inactive flag value.
				if (!empty($post_data['INACTIVE_FLG']))
				{
					$post_data['INACTIVE_FLG'] = 'N';
				}
				else
				{
					$post_data['INACTIVE_FLG'] = 'Y';
				}

				// check to see if the Employee id already exists 
				$exists = $this->SOT_model->check_employee_id($post_data['EMPL_ID']);
				if (!$exists)
				{
					// Convert all of the values other than the email to upper case.
					foreach ($post_data as $key => $value) 
					{
						if ($key == 'EMAIL_DSC')
						{
							continue;
						}
						else
						{						
							$post_data[$key] = strtoupper($value);
						}
					}

					// attempt to insert the employee record.
					$results = $this->SOT_model->insert_employee($post_data);

					// if the employee was succfully added.
					if ($results)
					{
						// if the phone array is not empty.
						if (!empty($phone['PHONE_NUM']))
						{
							$phone['PHONEREC_NUM'] = $this->SOT_model->get_next_val('ACT_SOT_EMPLPHONE', 'PHONEREC_NUM');
							$phone['ADD_BY'] = $this->session->userdata('samaccountname');
							$phone['ADD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";
							$phone['EMPL_ID'] = $post_data['EMPL_ID'];

							if (!empty($phone['PRIMARY_FLG']))
								{$phone['PRIMARY_FLG'] = 'Y';}
							else
								{$phone['PRIMARY_FLG'] = 'N';}

							// attempt to insert the employee's phone record.
							$phone_results = $this->SOT_model->insert_employee_phone($phone);

							if ($phone_results)
							{
								// everything was successfully inserted.
								$this->session->set_flashdata('message', 'The employee and their phone record has been successfully inserted.');
								redirect('SOT/employee_information');
							}
							else
							{
								// The employee recored was inserted but the phone insert failed.
								$this->session->set_flashdata('message', 'The Employee record was successfully inserted however the phone record could not be.');
								redirect('SOT/update_employee/'.$post_data['EMPL_ID']);
							}
						}
						else
						{
							// no phone record but the the employee record was successfully inserted.
							$this->session->set_flashdata('message', 'The employee record was inserted successfully.');
							redirect('SOT/update_employee/'.$post_data['EMPL_ID']);
						}
					}
					else
					{
						// error
						$this->session->set_flashdata('error', 'The employee record could not be inserted at this time, please try again.');
						redirect('SOT/add_employee');
					}
				}
				else
				{
					$this->session->set_flashdata('error', 'The employee id already exists due to a simillar name, try something more unique.');
					redirect('SOT/add_employee');
				}	
			}
			else
			{
				//There were form errors
				$data['content'] = "pages/SOT/add_employee_view";
				$this->load->view('master', $data);
			}
		}
		else
		{
			// error: no data in post.
			$data['content'] = "pages/SOT/add_employee_view";
			$this->load->view('master', $data);
		}
	}

	public function add_employee_phone()
	{	
		//Load model
		$this->load->model('SOT_model');

		// if there arent post vaiables then post will be null
		$post = $this->input->post(null, true);

		// Validation
		$this->form_validation->set_rules('PHONE_NUM', 'Phone NUmber', 'trim|max_length[15]|xss_clean|numeric|required');
		$this->form_validation->set_rules('PHONE_FLG', 'Phone Flag', 'trim|max_length[1]|xss_clean|');

		// if we're submitting data to be inserted
		if (!empty($post))
			{
				// Run the validation we set up above.
				if ($this->form_validation->run() == true)
				{
					// set up the post variables for insert
					$post['PHONEREC_NUM'] = $this->SOT_model->get_next_val("ACT_SOT_EMPLPHONE", "PHONEREC_NUM");
					$post['ADD_BY'] = $this->session->userdata('samaccountname');
					$post['ADD_DTE'] = date('d-m-y H:i:s');
					$post['ADD_DTE'] = "to_date('".$post['ADD_DTE']."','dd-mm-YYYY HH24:MI:SS')";

					// call the model for the insert and pass it the post data.
					// upon success/fail the model will return true/false.
					$results = $this->SOT_model->insert_employee_phone($post);

					// if the query was a success.
					if ($results)
					{
						// set a message for the user and then redirect back to the contacts record.
						$this->session->set_flashdata('message', 'The phone record has been added to the contact.');
						redirect('SOT/update_employee/'.$post['EMPL_ID']);
					}		
				}
				else
				{	
					// If the validation failed, return an error message to the user and redirect them
					// back to the add phone form.
					$this->session->set_flashdata('error', validation_errors());
					redirect('SOT/add_employee_phone/'.$post['EMPL_ID']);
				}
			}
			else
			{
				$data['EMPL_ID'] = $this->uri->segment(3);
				$data['contact'] = $this->SOT_model->get_employee_by_id($data['EMPL_ID']);
				$data['content'] = "pages/SOT/add_employee_phone_view";
				$this->load->view('master', $data);
			}
		
	}

/*           
============================================================================
update_employee
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - handles passing the data to model to update the specified record.
============================================================================
*/
	public function update_employee()
	{
		//Load Model
		$this->load->model('SOT_model');

		$id = $this->uri->segment(3);
		$post = $this->input->post(null, true);

		$this->form_validation->set_rules('FIRST_NME', 'First Name', 'trim|max_length[30]|xss_clean');
		$this->form_validation->set_rules('MIDDLE_NME', 'Middle Name', 'trim|max_length[3]|xss_clean');
		$this->form_validation->set_rules('LAST_NME', 'Last Name', 'trim|max_length[30]|xss_clean');
		$this->form_validation->set_rules('AGENCY_NME', 'Agency Name', 'trim|max_length[40]|xss_clean');
		$this->form_validation->set_rules('EMAIL_DSC', 'Email', 'trim|max_length[50]|xss_clean');
		$this->form_validation->set_rules('WORK_LOC', 'Work Location', 'trim|max_length[15]|xss_clean');
		$this->form_validation->set_rules('NOTES_DSC', 'Notes Description', 'trim|max_length[1000]|xss_clean');

		if (!empty($post))
		{
			if ($this->form_validation->run() == true)
			{
				// Set up the mod_by and mod_date for the update query.
				$date = date('d-m-Y H:i:s');
				$post['MOD_BY'] = $this->session->userdata('samaccountname');
				$post['MOD_DTE'] = "to_date('".$date."','dd-mm-YYYY HH24:MI:SS')";

				// set up the employees phone data to be updated
				foreach ($post as $key => $value) 
				{
					$exploded_key = explode('-', $key);

					if (is_numeric($exploded_key[0]))
					{
						$phones[$exploded_key[0]][$exploded_key[1]] = $value;
						unset($post[$key]);
					}
				}

				if (!empty($phones))
				{
					// loop through each phone array and update the employees phone records
					foreach ($phones as $phone) 
					{
						$phone['EMPL_ID'] = $post['EMPL_ID'];
						$phone['MOD_BY'] = $post['MOD_BY'];
						$phone['MOD_DTE'] = $post['MOD_DTE'];
						$this->SOT_model->update_employee_phone($phone);
					}
				}

				$results = $this->SOT_model->update_employee($post);

				if ($results)
				{
					$this->session->set_flashdata('message', 'The employee has been updated.');
					redirect('SOT/update_employee/'.$post['EMPL_ID']);
				}
				else
				{
					$this->session->set_flashdata('error', 'The employee could not be updated.');
					redirect('SOT/update_employee/'.$post['EMPL_ID']);
				}	
			}
			else
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('SOT/update_employee/'.$post['EMPL_ID']);
			}
		}
		else
		{
			$data['employee'] = $this->SOT_model->get_employee_by_id($id);
			$data['phone_numbers'] = $this->SOT_model->get_employee_phone($id);
			$data['content'] = "pages/SOT/update_employee_view";
			$this->load->view('master', $data);
		}
	}

/*           
============================================================================
delete_employee
----------------------------------------------------------------------------
NO PARAM
----------------------------------------------------------------------------
No return - Handles passing the id to delete to the model to run the query.
============================================================================
*/
	public function delete_employee()
	{
		// Load Model
		$this->load->model('SOT_model');

		$post_data = $this->input->post(null, true);

		// Get all phone records tied to the employee.
		$phones = $this->SOT_model->get_employee_phone($post_data['EMPL_ID']);

		if (!empty($phones))
		{
			foreach ($phones as $phone) 
			{
				$this->SOT_model->remove_employee_phone($phone['PHONEREC_NUM']);
			}
		}

		if ($this->SOT_model->remove_employee($post_data['EMPL_ID']))
		{
			$this->session->set_flashdata('message', 'The employee has been successfully removed.');
			redirect('SOT/employee_information');
		}
		else
		{ 
			$this->session->set_flashdata('error', 'An error has occurred, the employee could not be removed.');
			redirect('SOT/employee_information');
		}
	}

}

/* End of file SOT.php */
/* Location: .//Applications/MAMP/htdocs/HRMApplications/app/controllers/SOT.php */